import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

/** Problem #62 = ChessBoard squares : Programmer's School */
public class ChessBoardSquares {
	public static void main(String args[]){
		new ChessBoardSquares().run();
	}
	
	void run(){
		Locale.setDefault(Locale.US);
		try(
			Scanner in = new Scanner(System.in);
			PrintWriter out = new PrintWriter(System.out)){
				solve(in, out);
		}
	}

	void solve(Scanner in, PrintWriter out){
		String square = in.next();
		int letter = square.charAt(0);
		int number = Integer.valueOf(square.substring(1));
		
		// we use fact that black and white squares are alternating 
		// and the integer value of consecutive characters are sequential
		// (e.g, Ascii value of A = 65; B = 66). 
		out.println( (letter + number) % 2 == 0 ? "BLACK" : "WHITE");
	}
}
