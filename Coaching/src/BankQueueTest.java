import static org.junit.Assert.*;

import org.junit.Test;

public class BankQueueTest {

	@Test
	public void test() {
		assertEquals("0 0", BankQueue.queueWaitTime(1));
		assertEquals("1 35", BankQueue.queueWaitTime(20));
		assertEquals("NO", BankQueue.queueWaitTime(235));
		assertEquals("NO", BankQueue.queueWaitTime(249));
	}

}
