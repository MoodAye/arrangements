import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;


/** Programmers school problem # 86 */
public class Queens {
	public static void main(String args[]) {
		new Queens().run();
	}
	
	void run() {
		Locale.setDefault(Locale.US);
		try(
			Scanner in = new Scanner(System.in);
			PrintWriter out = new PrintWriter(System.out)){
			solve(in, out);
		}
	}

	// n = 3; output = 9 - 6 + 2 = 5
	void solve(Scanner in, PrintWriter out){
		int n = in.nextInt();
		//To get maximum number of queens; start with square that
		//won't be attacked by any queen - best would be a corner 
		//square.  This means that you cannot have any queen on the 
		//row; column and diagnol occupied by this square.  This leads 
		//formula below.
		out.println(n*n - 3*n + 2);
	}
}

