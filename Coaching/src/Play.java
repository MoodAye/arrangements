import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.stream.IntStream;

public class Play {
	public static void main(String[] args) {
		int start = IntStream.rangeClosed(1, 3)
				.reduce(1, (a,b) -> 10 * a + b);
		
		System.out.println(start);
		
		//1 = initial value; a - partial result; b = next value
		/*
		 * therefore
		 * 1, 10*1 + 1 = 11; 10*11 + 2 = 112;  10*112 + 3 = 1123
		 
		 *  
		 *  if we add be then we expect
		 *  11+1 = 12, 120 + 12 = 132; 1320 + 132 = 1452
		 */
	}
}


class Node{
	int v;
}
