import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

/** Programmer's School - Problem 511 */
public class BankQueue {

	void solve(Scanner in, PrintWriter out) {
		int position = in.nextInt();
		out.println(queueWaitTime(position));
	}
	
	public static String queueWaitTime(int position){
		if (position > ((12 * 60) / 5) + 1 ){
			return "NO";
		}
		
		int hr = ((position - 1) * 5) / 60;
		int min = ((position - 1) * 5) % 60;
		
		return Integer.toString(hr) + " " + Integer.toString(min);
	}

	public static void main(String[] args) {
			new BankQueue().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
