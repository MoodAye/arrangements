package com.moodaye.playground;

public class QuickFind {
	int[] forest;
	
	public QuickFind(int n) {
		forest = new int[n];
		for(int i = 0; i < n; i++) {
			forest[i] = i;
		}
	}
	
	public boolean connected(int p, int q) {
		return find(p) == find(q);
	}

	// return id of group
	public int find(int p) {
		return forest[p];
	}
	
	// makes them same group
	public void union(int p, int q) {
		int gp = find(p);
		int gq = find(q);
		for(int i : forest) {
			if(find(i) == gp) { 
				forest[i] = gq;
			}
		}
	}
	
}
