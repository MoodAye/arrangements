package com.moodaye.playground.crackingTheCodingInterview.moderate;

/** 16.9 
 *  Implement multiply, substract, divide using only
 *  add operator 
 *	
 *  ~15min to implement multiply 
 */
public class Operations {
	
	public static void main(String[] args) {
		System.out.println(multiply(4,8));
		System.out.println(multiply(-4,8));
		System.out.println(multiply(-4,-8));
		System.out.println(multiply(0,-8));
		System.out.println(multiply(9,0));
	}

	/**
	 * returns positive a
	 * @param a 
	 * @return a if a is positive or -1 * a if a is negative
	 */
	public static int makePositive(int a) {
		if(a >= 0) {
			return a;
		}
		
		int cnt = 0;
		while (a != 0) {
			a += 1;
			cnt++;
		}
		return cnt;
	}
	
	/**
	 * Computes a * b
	 * We'll simply add a to a b times.
	 * If b is negative then
	 * 	if a is positive - no issues
	 * 	if a is also negative then we need a way to return a positive
	 * @param a
	 * @param b
	 * @return
	 */
	public static int multiply(int a, int b) {
		if(a < 0 && b < 0) {
			a = makePositive(a);
			b = makePositive(b);
		}
		
		int p = 0;
		for(int i = 0; i < b; i++) {
			p += a;
		}
		
		return p;
	}
}
