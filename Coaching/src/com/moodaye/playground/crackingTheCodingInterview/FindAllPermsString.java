package com.moodaye.playground.crackingTheCodingInterview;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


//Given a unique string - find all permutations of its letters
public class FindAllPermsString {

	public static void main(String[] args) {
		Iterable<String> perms = FindAllPermsString.getPerms("Rajiv");
		for(String str : perms)
			System.out.println(str);
	}
	
	public static Iterable<String> getPerms(String str){
		Set<String> list = new HashSet<String>();
		if(str.length() == 1)
			return list;
	
		Deque<String> stack = new ArrayDeque<>();
		stack.push(str.substring(0,1));
	
		while(stack.size() > 0){
			String nextStr = stack.pop();
			if(nextStr.length() == str.length()){
				list.add(nextStr);
				continue;
			}
			List<String> perms = createNextPerms(nextStr, str);
			for( String perm : perms){
				stack.push(perm);
			}
		}
		return list;
	}
	
	private static List<String> createNextPerms(String nextStr, String str){
		List<String> list = new ArrayList<String>();
		StringBuilder sb = new StringBuilder();
		
		for(int i = 0; i < nextStr.length()+1; i++){
			sb.append(nextStr.substring(0,i));
			sb.append(str.substring(nextStr.length(), nextStr.length()+1));
			sb.append(nextStr.substring(i));
			list.add(sb.toString());
			sb = new StringBuilder();
		}
		return list;
	}
}
