package com.moodaye.playground.probPatterns.slidingWindow;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

// Problem  lc 53
// 9:37am - 10:04
public class MaxSumSubarray {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int[] nums = new int[n];
		for (int i = 0; i < nums.length; i++) {
			nums[i] = in.nextInt();
		}
		out.println(maxSubArray(nums));
		out.println(maxSubArray2(nums));
	}
	
	// complexity is 
	public int maxSubArray(int[] nums) {
		int max = Integer.MIN_VALUE;

		for (int w = 1; w <= nums.length; w++) {
			for (int left = 0; left <= nums.length - w; left++) {
				int curr = 0;
				for(int right = left; right - left < w; right++) {
					curr += nums[right];
				}
				max = Math.max(curr, max);
			}
		}
		return max;
	}
	
	/*
	 * At any point - the max contiguous subarray is
	 * max of max at prev i plus current i; prev max; global max
	 * e.g.,
	 * 
	 * 1000, -10, 2000  ==> answer is 2990
	 */
	public int maxSubArray2(int[] nums) {
		int gmax = nums[0];
		int pmax = nums[0];
		
		for(int i = 0; i < nums.length; i++) {
			pmax = Math.max(pmax + nums[i], nums[i]);
			gmax = Math.max(gmax, pmax);
		}
		return gmax;
	}

	public static void main(String[] args) {
		new MaxSumSubarray().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
