package com.moodaye.playground.probPatterns.slidingWindow;

import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Locale;
import java.util.Scanner;

// Problem lc 340
public class LongestSubStringAtLeastKDistinctChar {
	void solve(Scanner in, PrintWriter out) {
		String s = in.next();
		int k = in.nextInt();
		out.println(lengthOfLongestSubstringKDistinctBf(s, k));
	}
	
	/*
	 * Brute force: Iterate through windows of increasing size
	 */
	public int lengthOfLongestSubstringKDistinctBf(String s, int k) {
		if(k == 0) {
			return 0;
		}
		
		char[] a = s.toCharArray();
		
		int maxLen = 1;
		for(int window = 2; window <= a.length; window++) {
			for(int left = 0; left < a.length - window + 1; left++) {
				int right = left + window - 1;
				if(countDistinctChars(a, left, right) <= k) {
					maxLen = window;
					break;
				}
			}
//			if(maxLen != window) {
//				break;
//			}
		}
		return maxLen;
	}
	
	public long countDistinctChars(char[] a, int left, int right) {
		int[] cnts = new int[256];
		for(int i = left; i <= right; i++) {
			cnts[a[i]]++;
		}
		return Arrays.stream(cnts).filter((x) -> x > 0).count();
	}

	public static void main(String[] args) {
		new LongestSubStringAtLeastKDistinctChar().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
