package com.moodaye.playground.probPatterns.topologicalSort;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * For each node: 
 * 	* if visited - continue
 *  * if not visited add node to queue
 *  * mark as visited
 *  * add neighbors to a stack 
 * 
 * Topological sort order is now held in the queue
 * 
 * 1/10/2022
 * 	- Thought through this - think this will work
 *  - Next step - code Graph structure - then complete code
 *  
 *  1/11/2022
 *   - Had to review briefly the Graph data structure.
 *   - Figure out graph and then ts logic
 *   
 *  1/12/2022
 *   - spent time trying to figure out graph 
 *   - now stuck on the actual algorithm for the ts
 *   - NS: test cases 
 *   
 *  1/14/2022
 *   - Seems like i missed yesterday 
 *   - So signature will be 
 *          static String topoSort(List<Vertex>[]) 
 */
public class TopologicalSort {
	public static void main(String[] args) {
		
		Scanner in = new Scanner(System.in);
		int v =  in.nextInt();
		int e =  in.nextInt();
		
		Digraph g = new Digraph(v);
		
		for(int i = 0; i < g.e; i++) {
			g.addEdge(in.nextInt(), in.nextInt());
		}
	}
	
	public static String topoSort(Digraph g) {
		Deque<Integer> ts = new ArrayDeque<>();
		int[] parent = new int[g.v];
		
		for(int i = 0; i < g.v; i++) {
			List<Integer> adj = g.adj[i];
			for(int j = 0; j < adj.size(); j++) {
				
			}
		}
		
		return null;
	}

	/**
	 * Given list of vertices - checks whether list is in 
	 * topological order
	 * 
	 * @param topo List to check
	 * @param g Digraph
	 * @return true if the input list is in topological order. 
	 *         Else return false.
	 */
	public static boolean isTopo(List<Integer> topo, Digraph g) {
		
		
		return true;
	}

}

/**
 * Directed Graph 
 *  - Vertices are zero indexed
 *
 */
class Digraph {
	int v; // count of vertices
	int e; // count of edges
	List<Integer>[] adj;
	
	public Digraph(int V) {
		this.v = v;
		this.e = 0;
		adj = (List<Integer>[]) new List[v];
		for(int i = 0; i < v; i++) {
			adj[i] = new ArrayList<>();
		}
	}
	
	public void addEdge(int v, int w) {
		adj[v].add(w);
		e++;
	}
}

