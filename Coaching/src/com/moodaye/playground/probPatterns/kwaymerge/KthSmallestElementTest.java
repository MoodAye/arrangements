package com.moodaye.playground.probPatterns.kwaymerge;

import static org.junit.Assert.*;
import org.junit.Test;
import static com.moodaye.playground.probPatterns.kwaymerge.KthSmallestElement.*;

public class KthSmallestElementTest {

	@Test
	public void testBinarySearchSolution1() {
		int[] a = {10, 20, 30, 40};
		int[] b = {1, 12, 33, 34};
		
		assertEquals(10, binarySearchSolution(a, b, 2));
		assertEquals(10, binarySearchSolution(b, a, 2));
		assertEquals(1, binarySearchSolution(b, a, 1));
		assertEquals(1, binarySearchSolution(a, b, 1));
		assertEquals(12, binarySearchSolution(b, a, 3));
		assertEquals(12, binarySearchSolution(a, b, 3));
		assertEquals(20, binarySearchSolution(b, a, 4));
		assertEquals(20, binarySearchSolution(a, b, 4));
		assertEquals(30, binarySearchSolution(b, a, 5));
		assertEquals(30, binarySearchSolution(a, b, 5));
		assertEquals(33, binarySearchSolution(b, a, 6));
		assertEquals(33, binarySearchSolution(a, b, 6));
		assertEquals(34, binarySearchSolution(b, a, 7));
		assertEquals(34, binarySearchSolution(a, b, 7));
		assertEquals(40, binarySearchSolution(b, a, 8));
		assertEquals(40, binarySearchSolution(a, b, 8));
		
		
		assertEquals(10, joinAndSortSolution(a, b, 2));
		assertEquals(10, joinAndSortSolution(b, a, 2));
		assertEquals(1, joinAndSortSolution(b, a, 1));
		assertEquals(1, joinAndSortSolution(a, b, 1));
		assertEquals(12, joinAndSortSolution(b, a, 3));
		assertEquals(12, joinAndSortSolution(a, b, 3));
		assertEquals(20, joinAndSortSolution(b, a, 4));
		assertEquals(20, joinAndSortSolution(a, b, 4));
		assertEquals(30, joinAndSortSolution(b, a, 5));
		assertEquals(30, joinAndSortSolution(a, b, 5));
		assertEquals(33, joinAndSortSolution(b, a, 6));
		assertEquals(33, joinAndSortSolution(a, b, 6));
		assertEquals(34, joinAndSortSolution(b, a, 7));
		assertEquals(34, joinAndSortSolution(a, b, 7));
		assertEquals(40, joinAndSortSolution(b, a, 8));
		assertEquals(40, joinAndSortSolution(a, b, 8));
		
		
		assertEquals(10, mergeSolution(a, b, 2));
		assertEquals(10, mergeSolution(b, a, 2));
		assertEquals(1, mergeSolution(b, a, 1));
		assertEquals(1, mergeSolution(a, b, 1));
		assertEquals(12, mergeSolution(b, a, 3));
		assertEquals(12, mergeSolution(a, b, 3));
		assertEquals(20, mergeSolution(b, a, 4));
		assertEquals(20, mergeSolution(a, b, 4));
		assertEquals(30, mergeSolution(b, a, 5));
		assertEquals(30, mergeSolution(a, b, 5));
		assertEquals(33, mergeSolution(b, a, 6));
		assertEquals(33, mergeSolution(a, b, 6));
		assertEquals(34, mergeSolution(b, a, 7));
		assertEquals(34, mergeSolution(a, b, 7));
		assertEquals(40, mergeSolution(b, a, 8));
		assertEquals(40, mergeSolution(a, b, 8));
	}

	@Test
	public void testBinarySearchSolution2() {
		int[] a = {10, 20, 30};
		int[] b = {1, 12, 33, 34};
		
		assertEquals(10, binarySearchSolution(a, b, 2));
		assertEquals(10, binarySearchSolution(b, a, 2));
		assertEquals(1, binarySearchSolution(b, a, 1));
		assertEquals(1, binarySearchSolution(a, b, 1));
		assertEquals(12, binarySearchSolution(b, a, 3));
		assertEquals(12, binarySearchSolution(a, b, 3));
		assertEquals(20, binarySearchSolution(b, a, 4));
		assertEquals(20, binarySearchSolution(a, b, 4));
		assertEquals(30, binarySearchSolution(b, a, 5));
		assertEquals(30, binarySearchSolution(a, b, 5));
		assertEquals(33, binarySearchSolution(b, a, 6));
		assertEquals(33, binarySearchSolution(a, b, 6));
		assertEquals(34, binarySearchSolution(b, a, 7));
		assertEquals(34, binarySearchSolution(a, b, 7));
		
		
		assertEquals(10, joinAndSortSolution(a, b, 2));
		assertEquals(10, joinAndSortSolution(b, a, 2));
		assertEquals(1, joinAndSortSolution(b, a, 1));
		assertEquals(1, joinAndSortSolution(a, b, 1));
		assertEquals(12, joinAndSortSolution(b, a, 3));
		assertEquals(12, joinAndSortSolution(a, b, 3));
		assertEquals(20, joinAndSortSolution(b, a, 4));
		assertEquals(20, joinAndSortSolution(a, b, 4));
		assertEquals(30, joinAndSortSolution(b, a, 5));
		assertEquals(30, joinAndSortSolution(a, b, 5));
		assertEquals(33, joinAndSortSolution(b, a, 6));
		assertEquals(33, joinAndSortSolution(a, b, 6));
		assertEquals(34, joinAndSortSolution(b, a, 7));
		assertEquals(34, joinAndSortSolution(a, b, 7));
		
		
		assertEquals(10, mergeSolution(a, b, 2));
		assertEquals(10, mergeSolution(b, a, 2));
		assertEquals(1, mergeSolution(b, a, 1));
		assertEquals(1, mergeSolution(a, b, 1));
		assertEquals(12, mergeSolution(b, a, 3));
		assertEquals(12, mergeSolution(a, b, 3));
		assertEquals(20, mergeSolution(b, a, 4));
		assertEquals(20, mergeSolution(a, b, 4));
		assertEquals(30, mergeSolution(b, a, 5));
		assertEquals(30, mergeSolution(a, b, 5));
		assertEquals(33, mergeSolution(b, a, 6));
		assertEquals(33, mergeSolution(a, b, 6));
		assertEquals(34, mergeSolution(b, a, 7));
		assertEquals(34, mergeSolution(a, b, 7));
	}
	
	@Test
	public void testBinarySearchSolution3() {
		int[] a = {10};
		int[] b = {1};
		
		assertEquals(10, binarySearchSolution(a, b, 2));
		assertEquals(10, binarySearchSolution(b, a, 2));
		assertEquals(1, binarySearchSolution(a, b, 1));
		assertEquals(1, binarySearchSolution(b, a, 1));
		
		assertEquals(10, joinAndSortSolution(a, b, 2));
		assertEquals(10, joinAndSortSolution(b, a, 2));
		assertEquals(1, joinAndSortSolution(a, b, 1));
		assertEquals(1, joinAndSortSolution(b, a, 1));
		
		assertEquals(10, mergeSolution(a, b, 2));
		assertEquals(10, mergeSolution(b, a, 2));
		assertEquals(1, mergeSolution(a, b, 1));
		assertEquals(1, mergeSolution(b, a, 1));
	}
	
	@Test
	public void testBinarySearchSolution4() {
		int[] a = {10, 20};
		int[] b = {1};
		
		assertEquals(10, binarySearchSolution(a, b, 2));
		assertEquals(10, binarySearchSolution(b, a, 2));
		assertEquals(1, binarySearchSolution(a, b, 1));
		assertEquals(1, binarySearchSolution(b, a, 1));
		assertEquals(20, binarySearchSolution(a, b, 3));
		assertEquals(20, binarySearchSolution(b, a, 3));
		
		assertEquals(10, joinAndSortSolution(a, b, 2));
		assertEquals(10, joinAndSortSolution(b, a, 2));
		assertEquals(1, joinAndSortSolution(a, b, 1));
		assertEquals(1, joinAndSortSolution(b, a, 1));
		assertEquals(20, joinAndSortSolution(a, b, 3));
		assertEquals(20, joinAndSortSolution(b, a, 3));
		
		assertEquals(10, mergeSolution(a, b, 2));
		assertEquals(10, mergeSolution(b, a, 2));
		assertEquals(1, mergeSolution(a, b, 1));
		assertEquals(1, mergeSolution(b, a, 1));
		assertEquals(20, mergeSolution(a, b, 3));
		assertEquals(20, mergeSolution(b, a, 3));
	}
	
	@Test
	public void testBinarySearchSolution5() {
		int[] a = {10, 20, 30, 40, 50, 60};
		int[] b = {54};
		
		assertEquals(10, binarySearchSolution(a, b, 1));
		assertEquals(10, binarySearchSolution(b, a, 1));
		assertEquals(20, binarySearchSolution(a, b, 2));
		assertEquals(20, binarySearchSolution(b, a, 2));
		assertEquals(54, binarySearchSolution(a, b, 6));
		assertEquals(54, binarySearchSolution(b, a, 6));
		assertEquals(60, binarySearchSolution(a, b, 7));
		assertEquals(60, binarySearchSolution(b, a, 7));
		
		assertEquals(10, joinAndSortSolution(a, b, 1));
		assertEquals(10, joinAndSortSolution(b, a, 1));
		assertEquals(20, joinAndSortSolution(a, b, 2));
		assertEquals(20, joinAndSortSolution(b, a, 2));
		assertEquals(54, joinAndSortSolution(a, b, 6));
		assertEquals(54, joinAndSortSolution(b, a, 6));
		assertEquals(60, joinAndSortSolution(a, b, 7));
		assertEquals(60, joinAndSortSolution(b, a, 7));
		
		assertEquals(10, mergeSolution(a, b, 1));
		assertEquals(10, mergeSolution(b, a, 1));
		assertEquals(20, mergeSolution(a, b, 2));
		assertEquals(20, mergeSolution(b, a, 2));
		assertEquals(54, mergeSolution(a, b, 6));
		assertEquals(54, mergeSolution(b, a, 6));
		assertEquals(60, mergeSolution(a, b, 7));
		assertEquals(60, mergeSolution(b, a, 7));
	}
	
	@Test
	public void testBinarySearchSolution6() {
		int[] a = {10};
		int[] b = {10};
		
		assertEquals(10, binarySearchSolution(a, b, 2));
		assertEquals(10, binarySearchSolution(b, a, 2));
		assertEquals(10, binarySearchSolution(a, b, 1));
		assertEquals(10, binarySearchSolution(b, a, 1));
		
		assertEquals(10, joinAndSortSolution(a, b, 2));
		assertEquals(10, joinAndSortSolution(b, a, 2));
		assertEquals(10, joinAndSortSolution(a, b, 1));
		assertEquals(10, joinAndSortSolution(b, a, 1));
		
		assertEquals(10, mergeSolution(a, b, 2));
		assertEquals(10, mergeSolution(b, a, 2));
		assertEquals(10, mergeSolution(a, b, 1));
		assertEquals(10, mergeSolution(b, a, 1));
	}
	
	@Test
	public void testBinarySearchSolution7() {
		int[] a = {10, 10, 10, 10, 10};
		int[] b = {10};
		
		assertEquals(10, binarySearchSolution(a, b, 2));
		assertEquals(10, binarySearchSolution(b, a, 2));
		assertEquals(10, binarySearchSolution(a, b, 1));
		assertEquals(10, binarySearchSolution(b, a, 1));
		assertEquals(10, binarySearchSolution(a, b, 5));
		assertEquals(10, binarySearchSolution(b, a, 5));
		assertEquals(10, binarySearchSolution(a, b, 6));
		assertEquals(10, binarySearchSolution(b, a, 6));
		
		assertEquals(10, joinAndSortSolution(a, b, 2));
		assertEquals(10, joinAndSortSolution(b, a, 2));
		assertEquals(10, joinAndSortSolution(a, b, 1));
		assertEquals(10, joinAndSortSolution(b, a, 1));
		assertEquals(10, joinAndSortSolution(a, b, 5));
		assertEquals(10, joinAndSortSolution(b, a, 5));
		assertEquals(10, joinAndSortSolution(a, b, 6));
		assertEquals(10, joinAndSortSolution(b, a, 6));
		
		assertEquals(10, mergeSolution(a, b, 2));
		assertEquals(10, mergeSolution(b, a, 2));
		assertEquals(10, mergeSolution(a, b, 1));
		assertEquals(10, mergeSolution(b, a, 1));
		assertEquals(10, mergeSolution(a, b, 5));
		assertEquals(10, mergeSolution(b, a, 5));
		assertEquals(10, mergeSolution(a, b, 6));
		assertEquals(10, mergeSolution(b, a, 6));
	}
}
