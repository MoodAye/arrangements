package com.moodaye.playground.probPatterns.kwaymerge;

import java.util.Arrays;
import java.util.stream.IntStream;

/**
 * 1/4/2022: 
 * - setup the boiler plate.   Added comments to methods (solution options).
 * - conceptual understanding of approach.
 * https://www.baeldung.com/java-kth-smallest-element-in-sorted-arrays 1. Join
 * and Sort the 2 Arrays O(nlogn) 2. Merge (2 pointers) O(n) 3. Binary Search
 * 
 * 1/5/2022:
 * - Unit Tests
 * - Core Algo for binarySearch solution
 * 
 * 1/6/2022
 * 	- Wrote code for code algo for bs solution
 * 	- Wrote tests
 * 	- Easy tests are passing - that is good
 * 	- At least one test failing - i understand the core issue
 * 
 * 1/7/2022
 * 	- Got past issue where index is exceeding length of array
 *    e.g., assertEquals(33, binarySearchSolution(a, b, 6, 0, 0));
 *  - Next step: Write more tests - i expect more issues around edge cases
 *  
 * 1/8/2022
 * 	- Found another failing test where we are finding the largest element in the 2 arrays
 *    Here - the start index was outside the max index - so we now have a 
 *    base case that addresses this.
 *    - Next step:  Is there less code we can write to address this?
 *    	Address failing test case (test with an odd number of total elements)
 *    
 * 1/9/2022
 *   - Fixed issue with tests.  Now all tests are passing.  Satisfied that i have tested
 *     edge cases
 *   - Implemented join and sort solution
 *   - Completed test cases for join and sort
 *   - Completed merge solution and test cases
 */
public class KthSmallestElement {

	/**
	 * O(nlogn) performance
	 * O(n) space
	 */
	public static int joinAndSortSolution(int[] a, int[] b, int k) {
		int[] combined = IntStream.concat(Arrays.stream(a), Arrays.stream(b)).sorted().toArray();
		return combined[k - 1];
	}
	
	/**
	 *	O(n)  performance
	 *	O(n)  space
	 */
	public static int mergeSolution(int[] a, int[] b, int k) {
		int[] c = new int[a.length + b.length];
		
		int ax = 0;
		int bx = 0;
		
		while(ax < a.length || bx < b.length) {
			if(ax == a.length) {
				c[ax + bx] = b[bx++];
			}
			else if(bx == b.length) {
				c[ax + bx] = a[ax++];
			}
			else if(a[ax] <= b[bx]) {
				c[ax + bx] = a[ax++];
			}
			else {
				c[ax + bx] = b[bx++];
			}
		}
		return c[k - 1];
	}
	
	/**
	 * O(logn) complexity
	 * O(1) space
	 */
	public static int binarySearchSolution(int[] a, int[] b, int k) {
		return binarySearchSolution(a, b, k, 0, 0);
	}

	public static int binarySearchSolution(int[] a, int[] b, int k, int ax, int bx) {
		if(ax == a.length) {
			return b[bx + k - 1];
		}
		
		if(bx == b.length) {
			return a[ax + k - 1];
		}
		
		if(k == 1) {
			return a[ax] <= b[bx] ? a[ax] : b[bx];
		}
		
		int ka = (k + 1) / 2;
		int kb = k / 2;
		
		if(ax + ka - 1 >= a.length) {
			int temp = ka;
			ka = a.length - ax;
			kb += temp - ka;
		}
		
		if(bx + kb - 1 >= b.length) {
			int temp = kb;
			kb = b.length - bx;
			ka += temp - kb;
		}
		
		
		if(a[ax + ka - 1] >= b[bx + kb - 1]) {
			bx += kb;
			k = k - kb;
		}
		else {
			ax += ka;
			k = k - ka;
		}
		
		return binarySearchSolution(a, b, k, ax, bx);
	}

}
