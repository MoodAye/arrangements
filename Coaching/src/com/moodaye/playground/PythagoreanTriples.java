package com.moodaye.playground;

import java.util.stream.IntStream;
import java.util.stream.Stream;

public class PythagoreanTriples {
	private final static int[] i = {1,2,3};
	private final static int j = 2;
	
	
	
	public static void main(String[] args) throws Exception{
		IntStream.rangeClosed(1,1000)
			.boxed()  // need to make this stream of objects (Integer)
			.flatMap(a -> IntStream.rangeClosed(a, 1000) // combine 2 streams
			.filter(b -> Math.sqrt(a * a + b * b) % 1 == 0)
			.mapToObj(b -> new int[] {a, b, (int) Math.sqrt(a * a + b * b)})) // map alone would keep this Integer? But why is b needed  is it not already a local variable
			.limit(5)
			.forEach(a -> System.out.printf("%d %d %d%n", a[0], a[1], a[2]));
		
		PythagoreanTriples.i[0]= 10;
	}
}