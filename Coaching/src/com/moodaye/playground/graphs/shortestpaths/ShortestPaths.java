package com.moodaye.playground.graphs.shortestpaths;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Scanner;
import java.util.Set;

import javax.print.PrintService;

import com.moodaye.playground.graphs.Edge;
import com.moodaye.playground.graphs.Graph;
import com.moodaye.playground.graphs.Vertex;

/**
 * TODO: 
 * 
 * 1. Print out predecessors
 * 2. Demonstrate -ve weights not working Djikstra; working with Bellman
 * 3. Demonstrate cycle with Djiktra; with Bellman
 * 4. Deomonstrate -ve cycle detection with Bellman
 * 5. Simple optimization for Bellman with example where this helps
 *
 */
public class ShortestPaths {

	public static void main(String[] args) {
		try (Scanner in = new Scanner(System.in)) {

			int vCnt = in.nextInt();
			Set<Vertex> vs = new HashSet<>();
			for (int i = 0; i < vCnt; i++) {
				vs.add(new Vertex(in.next()));
			}

			int eCnt = in.nextInt();
			Set<Edge> es = new HashSet<>();
			for (int i = 0; i < eCnt; i++) {
				String src = in.next();
				Vertex vSrc = vs.stream().filter((v) -> v.getName().equals(src)).findAny().orElse(null);

				String trg = in.next();
				Vertex vTrg = vs.stream().filter((v) -> v.getName().equals(trg)).findAny().orElse(null);

				es.add(new Edge(vSrc, vTrg, in.nextDouble()));
			}

			Graph g = new Graph(vs, es);
			String source = in.next();

			djikstra(g, source);
			System.out.println("djiktra's algorithm");
			printSs(g);
			
			g.reset();
			System.out.println();
			
			bellmanFord(g, source);
			System.out.println("bellmanFord algorithm");
			printSs(g);
		}
	}
	
	private static void printSs(Graph g) {
			for (Vertex v : g.getVertices()) {
				System.out.println(v.getPr() != null ? v.getPr().getName() : ""  
						+ "--->" + v.getName() + "    :" + v.getSrcDist());
			}
	}
	
	public static List<Vertex> bellmanFord(Graph g, String src){
		Vertex vs = g.getVertices().stream().filter((v) -> v.getName().equals(src)).findAny().orElse(null);
		vs.setSrcDist(0);
		
		List<Vertex> ss = new ArrayList<>();
		
		for(int i = 0; i < g.getVertices().size() - 1; i++) {
			for(Edge e : g.getEdges()) {
				
				Vertex s = e.getSource();
				Vertex t = e.getTarget();
				double sd = s.getSrcDist() + e.getW();
				
				if(sd < t.getSrcDist()) {
					t.setSrcDist(sd);
					t.setPr(s);
				}
			}
		}
		return ss;
	}

	public static List<Vertex> djikstra(Graph g, String src) {
		Vertex vs = g.getVertices().stream().filter((v) -> v.getName().equals(src)).findAny().orElse(null);
		vs.setSrcDist(0);

		List<Vertex> ss = new ArrayList<>();
		PriorityQueue<Vertex> pq = new PriorityQueue<>();
//		Queue<Vertex> pq = new ArrayDeque<>();
		pq.add(vs);
		while (!pq.isEmpty()) {

			Vertex minV = pq.poll();
			ss.add(minV);
			
			List<Edge> edges = minV.getAdjList();

			for (Edge e : edges) {

				Vertex vSrc = e.getSource();
				Vertex vTrg = e.getTarget();

				if (vSrc.getSrcDist() + e.getW() < vTrg.getSrcDist()) {

					vTrg.setSrcDist(vSrc.getSrcDist() + e.getW());
					vTrg.setPr(vTrg);
					if (!pq.contains(vTrg)) {
						pq.add(vTrg);
					}

				}
			}
		}

		return ss;
	}
}


/* Test 1
4
A B C D
9

A C 7
A D 1
B D 7
B A 9
C A 6
C B 3
C D 9
D C 2
D B 10


A
*/

/*
8
A B C D E F G H
14
A F 86
B E 25
B H 1000
B C 44
C A 61
C B 7
E H 59
H E 39
H B 29
H D 69
G B 62
F D 98
F H 43
H A 6
A

Result:
A:0.0
F:86.0
H:129.0
B:158.0
E:168.0
D:184.0
C:202.0
*/

/*
 * Trying to demo why we need priority queue
 * Here - if you use queue = node C gets updated with incorrect value
  5
  A B C D E
  5
  A B 100
  A C 1
  C D 1
  D B 1
  B E 1
  A
 */

/*
 * Trying to demo why we need priority queue
 * Here - if you use queue = node C gets updated with incorrect value
  5
  A B C D E
  5
  A B 10
  A C 20  
  C D -1
  D E -1
  E B -100
  
  Result:
  A B -82  (Djikstra will give ?)
  A C 20 
  A D 19  
  A E 18
  
  
 */