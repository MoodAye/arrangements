package com.moodaye.playground.graphs.trees.bst;

/**
 * Represents a node of tree supporting typical 
 * needs of problem solutions
 * @author Rajiv
 *
 */
public class TreeNode {
	TreeNode left;
	TreeNode right;
	int val;
	
	public TreeNode() { }
	public TreeNode(int val) {this.val = val;}
	
	public TreeNode(TreeNode left, TreeNode right, int val) {
		this.left = left;
		this.right = right;
		this.val = val;
	}
}
