package com.moodaye.playground.graphs.trees.bst;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

/** not yet solved */
public class LongestUnivaluePath {
	int maxLen;

	public int longestUnivaluePath(TreeNode root) {
		if (root == null) {
			return 0;
		}

		int leftPath = longestUnivaluePath(root.left);
		if(root.left.val != root.val) {
			leftPath = 0;
		}
		
		int rightPath = longestUnivaluePath(root.right);
		if(root.right.val != root.val) {
			rightPath = 0;
		}
		
		int path = leftPath + rightPath + 1;
		maxLen = Math.max(maxLen, leftPath + rightPath + 1);

		return Math.max(leftPath + 1, rightPath + 1);
	}

	void solve(Scanner in, PrintWriter out) {
		String strLine = in.next();
		String[] tree = strLine.substring(1, strLine.length() - 1).split(",");
		TreeNode root = BstFactory.buildTree(tree);
		longestUnivaluePath(root);
		out.println(maxLen);
	}

	public static void main(String[] args) {
		new LongestUnivaluePath().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}

}
