package com.moodaye.playground.graphs.trees.bst;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

public class BstTraversal {
	
	/** root --> left --> right */
	public void inOrderTraversal(TreeNode root, PrintWriter out) {
		if (root == null) {
			return;
		}
		out.printf("%d, ", root.val);
		inOrderTraversal(root.left, out);
		inOrderTraversal(root.right, out);
	}

	/** left --> root --> right */
	public void preOrderTraversal(TreeNode root, PrintWriter out) {
		if (root == null) {
			return;
		}
		inOrderTraversal(root.left, out);
		out.printf("%d, ", root.val);
		inOrderTraversal(root.right, out);
	}
	
	/** left --> right --> root */
	public void postOrderTraversal(TreeNode root, PrintWriter out) {
		if (root == null) {
			return;
		}
		inOrderTraversal(root.left, out);
		out.printf("%d, ", root.val);
		inOrderTraversal(root.right, out);
	}

	void solve(Scanner in, PrintWriter out) {
		String strLine = in.next();

		String[] tree = strLine.substring(1, strLine.length() - 1).split(",");
		TreeNode root = BstFactory.buildTree(tree);
		inOrderTraversal(root, out);
		preOrderTraversal(root, out);
		postOrderTraversal(root, out);
	}

	public static void main(String[] args) {
		new BstTraversal().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}

}
