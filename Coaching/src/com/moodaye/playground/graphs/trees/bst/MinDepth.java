package com.moodaye.playground.graphs.trees.bst;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

/** not yet solved */
public class MinDepth {

	public int minDepth(TreeNode root) {
		return 0;
	}

	void solve(Scanner in, PrintWriter out) {
		String strLine = in.next();
		String[] tree = strLine.substring(1, strLine.length() - 1).split(",");
		TreeNode root = BstFactory.buildTree(tree);
		out.println(minDepth(root));
	}

	public static void main(String[] args) {
		new MinDepth().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}

}
