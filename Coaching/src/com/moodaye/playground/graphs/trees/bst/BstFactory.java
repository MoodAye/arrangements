package com.moodaye.playground.graphs.trees.bst;

public class BstFactory {

	public static TreeNode buildTree(String[] tree) {
		if(null == tree || tree.length == 0 || tree[0].equals("null")) {
			return null;
		}
		
		TreeNode root = new TreeNode(Integer.valueOf(tree[0]));

		for (int i = 1; i < tree.length; i++) {
			insertNode(root, tree[i]);
		}

		return root;
	}

	/** Duplicates are ignored */
	private static TreeNode insertNode(TreeNode root, String sval) {
		if (sval.equals("null")) {
			return null;
		}
		
		if(root == null) {
			int val = Integer.valueOf(sval);
			root = new TreeNode(val);
		}
		
		return root;
	}
}
