package com.moodaye.playground.graphs;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Vertex implements Comparable<Vertex>{
	private String name;
	
	/** predecessor  - in shortest paths - for example */
	private Vertex pr;
	
	private double srcDist;
	
	private List<Edge> adjList;

	public Vertex(String name) {
		super();
		this.name = name;
		this.adjList = new ArrayList<>();
		this.srcDist = Double.POSITIVE_INFINITY;
	}
	
	public String getName() {
		return name;
	}

	public Vertex getPr() {
		return pr;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setPr(Vertex pr) {
		this.pr = pr;
	}
	
	public List<Edge> getAdjList(){
		return adjList;
	}
	
	public void setAdjList(List<Edge> adjList) {
		this.adjList = adjList;
	}
	
	public double getSrcDist() {
		return srcDist;
	}
	
	public void setSrcDist(double srcDist) {
		this.srcDist = srcDist;
	} 

	@Override
	public int hashCode() {
		return name.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof Vertex)) {
			return false;
		}
		Vertex other = (Vertex) obj;
		return Objects.equals(name, other.name);
	}

	@Override
	public String toString() {
		return "Vertex [name=" + name + "]";
	}

	@Override
	public int compareTo(Vertex o) {
		return Double.compare(srcDist, o.getSrcDist());
	}
}
	
