package com.moodaye.playground.graphs;

import java.util.List;
import java.util.Objects;
import java.util.Set;

public class Graph {
	Set<Vertex> vertices;
	
	Set<Edge> edges;

	public Graph(Set<Vertex> vertices, Set<Edge> edges) {
		super();
		this.vertices = vertices;
		this.edges = edges;
		for(Edge e : edges) {
			e.getSource().getAdjList().add(e);
		}
	}

	/** Reinitialized the vertices to allow another computation */
	public void reset() {
	 for(Vertex v : vertices)	{
		 v.setSrcDist(Double.POSITIVE_INFINITY);
		 v.setPr(null);
	 }
	}

	/**
	 * @return the vertices
	 */
	public Set<Vertex> getVertices() {
		return vertices;
	}

	/**
	 * @return the edges
	 */
	public Set<Edge> getEdges() {
		return edges;
	}

	/**
	 * @param vertices the vertices to set
	 */
	public void setVertices(Set<Vertex> vertices) {
		this.vertices = vertices;
	}

	/**
	 * @param edges the edges to set
	 */
	public void setEdges(Set<Edge> edges) {
		this.edges = edges;
	}

	@Override
	public int hashCode() {
		return Objects.hash(edges, vertices);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof Graph)) {
			return false;
		}
		Graph other = (Graph) obj;
		return Objects.equals(edges, other.edges) && Objects.equals(vertices, other.vertices);
	}
	
}
