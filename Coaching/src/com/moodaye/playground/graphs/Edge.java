package com.moodaye.playground.graphs;

import java.util.Objects;

public class Edge {
	
	Vertex source;
	Vertex target;
	
	/** weight */
	double w;
	
	public Edge(Vertex source, Vertex target, double w) {
		super();
		this.source = source;
		this.target = target;
		this.w = w;
	}
	
	public double getW() {
		return w;
	}
	
	public void setW(double w) {
		this.w = w;
	}
	
	public Vertex getSource() {
		return source;
	}
	
	public Vertex getTarget() {
		return target;
	}
	
	public void setSource(Vertex source) {
		this.source = source;
	}
	
	public void setTarget(Vertex target) {
		this.target = target;
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(source, target);
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof Edge)) {
			return false;
		}
		Edge other = (Edge) obj;
		return Objects.equals(source, other.source) && Objects.equals(target, other.target);
	}
	
	@Override
	public String toString() {
		return "Edge [source=" + source + ", target=" + target + "]";
	}
}
