package com.moodaye.playground.compete.hack48;

import java.util.Scanner;

/*
 We define the following conditions for n robots numbered from 0 to n-1:

The robots are initially spaced apart on an infinitely long straight line. 
Some of them begin moving simultaneously at the same constant speed. 
Two robots moving in the same direction never collide.

Two robots moving toward each other will crash, break down, and stay at that point forever. 
This counts as 2 collisions. A robot moving toward a non-moving robot will crash into it, 
break down, and stay at that point forever. This counts as 1 collision.

To clarify, whenever a moving robot crashes onto another robot, it will stay at that point 
and become a non-moving robot at that location.

We describe their movement instructions in a string, s, consisting of the letters l, r, and d. 
The ith character in denotes the  robot's instruction according to the following rules:

l: Move left indefinitely until a collision occurs.
r: Move right indefinitely until a collision occurs.
d: Do not move at all.

Complete the function below so that it returns the total number of collisions that eventually occur.

Input Format

The first line contains an integer, q, denoting the total number of queries (i.e., calls to the function). 
Each of the  subsequent lines describes a query in the form of a string, s, consisting of the letters l, r, and d.

Constraints
1 <= q <= 100
1 <= n <= 10^5 , where n is the length of s.
Each s consists of the letters l, r, and d only.

Output Format
Return an integer denoting the total number of collisions that eventually occur after movement begins. 

Sample Input
5
r
lrrl
rrrll
rrdlldrr
rrrdllrllrrl

Sample Output
0
3
5
4
11
 
 */

public class WalkingRobots {
	static int howManyCollisions(String s){
		int collisions = 0;
		boolean d = false;
		int r = 0;
		for ( int i = 0; i < s.length(); i++ ){
			if ( s.charAt(i) == 'r'){
				r++;
				d = false;
				continue;
			}
			if ( s.charAt(i) == 'd'){
				d = true;
				collisions += r;
				r = 0;
				continue;
			}
			if (s.charAt(i) == 'l'){
				if(d) collisions++;
				else if (r > 0){
					collisions += r + 1;
					r = 0;
					d = true;
				}
			}
		}
		return collisions;
	}
	
	 public static void main(String[] args) {
	        Scanner in = new Scanner(System.in);
	        int q = in.nextInt();
	        for(int a0 = 0; a0 < q; a0++){
	            String s = in.next();
	            // Returns the number of times moving robots collide.
	            int result = howManyCollisions(s);
	            System.out.println(result);
	        }
	        in.close();
	    }
}
