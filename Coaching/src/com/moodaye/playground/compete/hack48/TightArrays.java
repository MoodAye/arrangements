package com.moodaye.playground.compete.hack48;

/**

Max Score = 15; Difficulty = Easy

We call an array of integers tight if every pair of adjacent integers in the array has an 
absolute difference <= 1. For example, the array [3,4,4,3,2,1,2,3,4,4,5,5] is tight, but the array 
[1,2,4,3,3] is not. 

Given a,b, and c, complete the function below by returning the length of the shortest tight 
array such that the first element is , the last element is , and the array contains .
Input Format
Three space-separated integers describing the respective values of a,b, and c.
Constraints
1<=a,b,c<=100
Output Format
Return a single integer denoting the length of the shortest tight array such that the first element is a, 
the last element is c, and the array contains the element b.

Sample input = 5,7,11
Sample output = 7
Answer array = [5,6,7,8,9,10,11]

Sample input = 3, 1 , 2
Sample output = 4
Answer array = [3,2,1,2]

Sample input = 5, 5 , 6
Sample output = 2
Answer array = [5,6]
*/
public class TightArrays {
	static int shortestTightArray(int a, int b, int c){
		return Math.abs(a-b) + Math.abs(b-c) + 1;
	}
}
