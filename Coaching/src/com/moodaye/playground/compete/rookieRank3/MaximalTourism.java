package com.moodaye.playground.compete.rookieRank3;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.Scanner;

public class MaximalTourism {
	Graph G;
	int maxCities;
	boolean[] marked;
	
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int m = in.nextInt();
        int[][] route = new int[m][2];
        for(int route_i=0; route_i < m; route_i++){
            for(int route_j=0; route_j < 2; route_j++){
                route[route_i][route_j] = in.nextInt();
            }
        }	
        
        in.close();
        
        Graph g = new Graph(n+1);
        for(int i =0; i < m; i++ )
        	g.addEdge(route[i][0], route[i][1]);
        
        System.out.println(new MaximalTourism(g).maxCities);
	}
	
	public MaximalTourism(Graph G){
		this.G = G;
		marked = new boolean[G.V()];
		computeMaxCities();
	}
	
	private void computeMaxCities(){
		Queue<Integer> queue = new ArrayDeque<>();
		for (int v = 0; v < G.V(); v++){
			if(marked[v]) continue;
			queue.add(v);
			int max = 0;
			while(queue.size() != 0){
				int w = queue.remove();
				if (marked[w]) continue;
				marked[w] = true;
				max++;
				Iterable<Integer> iter = G.adj(w);
				for(int x : iter) queue.add(x);
			}
			if (max > maxCities) maxCities = max;
		}	
	
	}
	
	public int getMaxCities(){
		return maxCities;
	}
	
	
	static class Graph{
		private final int V;
		private int E;
		private List<Integer>[] adj;
		
		Graph(int V){
			this.V = V;
			this.E = 0;
			adj = (List<Integer>[]) new ArrayList[V];
			for(int i =0; i<V; i++){
				adj[i] = new ArrayList<>();
			}
		}
		
		int V(){return V;}
		int E(){return E;}
		void addEdge(int v, int w){
			adj[v].add(w);
			adj[w].add(v);
			E++;
		}
		Iterable<Integer> adj(int v){
			return adj[v];
		}
	}
}
