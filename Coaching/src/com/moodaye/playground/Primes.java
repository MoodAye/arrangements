package com.moodaye.playground;

import java.util.stream.IntStream;

public class Primes {
	public static void main(String[] args) {
		isPrime(10);
	}
	
	public static boolean isPrime(int x) {
		IntStream.range(1, x).filter(b -> x % b == 0).forEach(System.out::println);
		return true;
	}
}
