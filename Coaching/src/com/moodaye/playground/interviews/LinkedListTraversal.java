package com.moodaye.playground.interviews;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

// Problem 
public class LinkedListTraversal {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		Node2 root = new Node2();
		root.val = in.nextInt();
		Node2 next = root;
		for(int i = 1; i < n; i++) {
			next.next = new Node2();
			next.next.val = in.nextInt();
			next = next.next;
		}
//		printLinkedList(root);
		out.println(computeMaxOfOppEnds(root));
	}
	
	/*
	 * computes the max of the sum of the first + last node; 
	 * second and second to last node; etc.
	 */
	private int computeMaxOfOppEnds(Node2 root) {
		int len = 0;
		Node2 next = root;
		while(next != null) {
			len++;
			next = next.next;
		}
		
		int max = Integer.MIN_VALUE;
		
		for(int i = 0; i < len / 2; i++) {
			// first node
			Node2 first = root;
			for(int j = 0; j < i; j++) {
				first = first.next;
			}
			int val1 = first.val;
			
			// second node
			Node2 second = root;
			for(int j = 0; j < len - i - 1; j++) {
				second = second.next;
			}
			int val2 = second.val;
			
			max = Math.max(max, val1 + val2);
		}
		return max;
	}

	private void printLinkedList(Node2 root) {
		Node2 next = root;
		while(next != null) {
			System.out.print(next.val + "-->");
			next = next.next;
		}
	}
		

	public static void main(String[] args) {
		new LinkedListTraversal().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}

class Node2 {
	int val;
	Node2 next;
}
