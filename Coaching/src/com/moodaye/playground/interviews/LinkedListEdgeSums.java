package com.moodaye.playground.interviews;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

/**
 * Amazon Interview Question
 * 	Given an instruction document - a linked list of pages
 * 	Customer reads first and last pages every day
 *  Then the next day customer reads 2nd and second to last page
 *  The val of the linked list node is size of page
 *  We need to find the max size of pages read in a day by the customer
 *  
 *  Need to do this in O(1) time
 *  
 *  What i plan to do is to 
 *  	1. Count nodes in linked list (=cnt)
 *      2. Move the nodes so that pages read in a single day are next to each other
 *      	2a. First node moves (cnt - 2 node)
 *          2b. Second node moves (cnt - 4 nodes)
 *          2c. Continue until cnt - x <= 0;
 *      3. Then add adjacent pages to get size read daily
 *      	3a. If cnt is odd - then the page page is read alone
 *      
 *  Test Cases
 *  	1. 5 1 4 5 1 2    (ans = 9)
 *  	2. 5 1 5 1 2     (ans = 7)
 *      3. 5 1 100 1 2    (ans = 100)
 *      4. 10             (ans = 10)
 * 		5. 1 2 			  (ans = 3)
 */
public class LinkedListEdgeSums {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		if(n == 0) {
			out.println(0);
		}
		Node head = new Node(in.nextInt());
		Node next = head;
		for (int i = 1; i < n; i++) {
			next.next = new Node(in.nextInt());
			next = next.next;
		}
		
		out.println(getMaxPagesRead(head));
	}
	
	private void printList(Node head) {
		while(head != null) {
			System.out.print(head.val + "-->");
			head = head.next;
		}
		System.out.println();
	}
	
	private int getMaxPagesRead(Node head) {
		
		// count nodes in list
		int cnt = 0;
		Node next = head;
		while(next != null) {
			cnt++;
			next = next.next;
		}
		
		// move nodes 
		for(int i = 1; cnt - 2 * i > 0; i++) {
			Node curr = head;
			Node prev = head;
			head = head.next;
			int steps = cnt - 2 * i;
			for(int j = 1; j <= steps; j++) {
				Node target = curr.next;
				Node temp = target.next;
				prev.next = target; 
				target.next = curr;
				curr.next = temp;
				prev = target;
//				printList(head);
			}
		}
		
		// compute max
		int max = 0;
		int start = 1;
		Node curr = head;
		if(cnt % 2 == 1) {
			max = head.val;
			start = 2;
			curr = head.next;
		}
		
		while(curr != null) {
			max = Math.max(max, curr.val + curr.next.val);
			curr = curr.next.next;
		}
		
		return max;
	}

	public static void main(String[] args) {
		new LinkedListEdgeSums().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}


class Node{
	Node next;
	int val;
	public Node(int val) {
		this.val = val;
	}
	
	public Node(int val, Node next) {
		this.val = val;
		this.next = next;
	}
	
	public Node() { }
}
