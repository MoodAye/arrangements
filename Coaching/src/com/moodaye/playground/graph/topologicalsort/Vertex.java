package com.moodaye.playground.graph.topologicalsort;

import java.util.ArrayList;
import java.util.List;

public class Vertex {
	private String data;
	private boolean visited;
	private List<Vertex> neighbors;
	
	public Vertex(String data) {
		this.data = data;
		neighbors = new ArrayList<>();
	}
	
	public void addNeighbor(Vertex v) {
		this.neighbors.add(v);
	}

	
	/**
	 * @return the visited
	 */
	public boolean isVisited() {
		return visited;
	}


	/**
	 * @param visited the visited to set
	 */
	public void setVisited(boolean visited) {
		this.visited = visited;
	}


	/**
	 * @return the data
	 */
	public String getData() {
		return data;
	}

	/**
	 * @return the neighbors
	 */
	public List<Vertex> getNeighbors() {
		return neighbors;
	}

	/**
	 * @param data the data to set
	 */
	public void setData(String data) {
		this.data = data;
	}

	/**
	 * @param neighbors the neighbors to set
	 */
	public void setNeighbors(List<Vertex> neighbors) {
		this.neighbors = neighbors;
	}
	
	public String toString() {
		return data;
	}
	
}
