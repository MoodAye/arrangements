package com.moodaye.playground.graph.topologicalsort;

import java.util.Stack;

public class TopologicalSort {
	private Stack<Vertex> stack;
	
	public TopologicalSort() {
		stack = new Stack<>();
	}
	
	public void dfs(Vertex vertex) {
		vertex.setVisited(true);
		
		for(Vertex v : vertex.getNeighbors()) {
			if(! v.isVisited()) {
				dfs(v);
			}
		}
		stack.push(vertex);
	}
	
	public Stack getStack() {
		return this.stack;
	}
	
}
