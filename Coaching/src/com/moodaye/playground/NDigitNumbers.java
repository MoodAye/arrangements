package com.moodaye.playground;

import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Locale;
import java.util.Scanner;

// Problem 
public class NDigitNumbers {
	static String smallest;
	static boolean smallestFound;

	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int[] digits = new int[n];
		if (n == 1) {
			out.printf("%d %d%n", 10, 0);
			return;
		}
		int cnt = count(digits, 0, 0);
		out.printf("%d %s%n", cnt, smallest);
	}

	private int sum(int[] a) {
		return Arrays.stream(a).sum();
	}

	private int prod(int[] a) {
		int c = Arrays.stream(a).reduce(1, (f, g) -> f * g);
		return c;
	}

	// need to fix the duplicate writing of numbers ending in 0
	private int count(int[] a, int idx, int cnt) {
		if (idx == a.length) {
			while (--idx >= 0 && a[idx] == 9)
				a[idx] = 1;
			return cnt;
		}
		for (int i = 1; i <= 9; i++) {
			a[idx] = i;
			int sum = sum(a);
			int prod = prod(a);
			if (prod != 0 && sum < prod) {
				for (int j = idx; j < a.length; j++) {
					a[j] = 1;
				}
				break;
			}

//			Arrays.stream(a).forEach(System.out::print);
//			System.out.println();
			if (sum == prod && idx == a.length - 1) {
				cnt++;
				if (!smallestFound) {
					StringBuilder sb = new StringBuilder();
					Arrays.stream(a).forEach(x -> sb.append(x));
					smallest = sb.toString();
					smallestFound = true;
				}
			}
			cnt = count(a, idx + 1, cnt);
		}
		for (int j = idx; j < a.length; j++) {
			a[j] = 1;
		}
		return cnt;
	}

	public static void main(String[] args) {
		new NDigitNumbers().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
