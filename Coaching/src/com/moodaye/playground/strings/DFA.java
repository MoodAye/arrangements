package com.moodaye.playground.strings;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class DFA {
	private static int resetIndex;
	
	public static void main(String[] args) {
		System.out.println(dfa("aaaaaa","aa"));
		System.out.println(dfaAll("aaaaaa","aa"));
		System.out.println(dfaAll("ababab", "b"));
	}
	
	public static int dfa(String text, String pattern){
		Iterable<Integer> indexes = dfaAll(text, pattern);
		Iterator<Integer> it = indexes.iterator();
		if(it.hasNext()){
			return it.next();
		}
		return -1;
	}
	
	public static Iterable<Integer> dfaAll(String text, String pattern){
		int[][] dfa = computeDfa(pattern);
		int M = pattern.length();
		
		List<Integer> indexes = new ArrayList<Integer>();
		
		for(int i = 0, j = 0; i<text.length(); i++){
			j = dfa[text.charAt(i)][j];
			if (j == M){
				indexes.add(i-M+1);
				j = resetIndex;
			}
		}
		return indexes;
	}
	
	private static int[][] computeDfa(String pattern){
		int M = pattern.length();
		int[][] dfa = new int[256][M];
		dfa[pattern.charAt(0)][0] = 1;
		int X = 0;
		for(int j=1; j<M; j++){
			for(int c=0; c<256; c++)
				dfa[c][j] = dfa[c][X];
			dfa[pattern.charAt(j)][j] = j+1;
			X = dfa[pattern.charAt(X)][j];
		}
		if(X != pattern.length()) resetIndex = X;
		else resetIndex = X - 1;
		return dfa;
	}

	public static int rotationN(String text, String rotText){
		if (text.equals(rotText)) return 0;
		if (text.length() != rotText.length()) return -1;
		
		for(int i=1; i < text.length(); i++ ){
			int index1 = dfa(text,rotText.substring(0,i));
			int index2 = dfa(text,rotText.substring(i,rotText.length()));
			if(index1 == -1) return -1;
			if (index2 == 0){
				if(text.equals(rotText.substring(i,rotText.length()) + rotText.substring(0,i))){
					return i;
				}
			}
		}
		return -1;
	}
	
	
}

class Funwords{
	public static List<String> prefixes(String T, String X){
		List<String> prefixes = null;
		
		for (int i = 0; i < X.length(); i++){
			int index = DFA.dfa(T, X.substring(0, i+1));
			if (index < 0) return null;
			
			if (index == 0){
				if (i == X.length() -1){
					prefixes = new ArrayList<String>();
					prefixes.add(X);
					return prefixes;
				} else {
					prefixes = prefixes(T, X.substring(i + 1));
					if (prefixes == null)
						continue;
					else {
						prefixes.add(X.substring(0, i + 1));
						return prefixes;
					}
				}
			}
		}
		return prefixes;
	}
}
