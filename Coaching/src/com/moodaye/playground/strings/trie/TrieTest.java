package com.moodaye.playground.strings.trie;

import static org.junit.Assert.*;

import org.junit.Test;

public class TrieTest {

	@Test
	public void testInsert() {
		Trie trie = new Trie();
		
		trie.insert("Rajiv");
		trie.insert("Reena");
		trie.insert("Kavita");
		trie.insert("Tasha");
		trie.insert("Ronnie");
		
		assertTrue(trie.search("Reena"));
		assertTrue(trie.search("Rajiv"));
		assertTrue(trie.search("Kavita"));
		assertTrue(trie.search("Ronnie"));
		assertTrue(trie.search("Tasha"));
		assertTrue(trie.search("Reena"));
		assertTrue(trie.search("Rajiv"));
		assertTrue(trie.search("Kavita"));
		assertTrue(trie.search("Ronnie"));
		assertTrue(trie.search("Tasha"));
		
		assertFalse(trie.search("Alisha"));
		assertFalse(trie.search("RajivReena"));
		assertFalse(trie.search("Raji"));
		assertFalse(trie.search("Rajiva"));
		
		assertTrue(trie.startsWith("Ree"));
		assertFalse(trie.startsWith("B"));
		assertTrue(trie.startsWith("Rajiv"));
	}

}
