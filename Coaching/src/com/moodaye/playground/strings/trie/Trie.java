package com.moodaye.playground.strings.trie;


/**
 *	12/28/2021
 * Practice implementing Trie
 * Based on Udemy Faang course
 * Next Steps:
 * 	- Use HashMap - and its method - computeIfAbsent - using a lambda
 *
 */
public class Trie {
	private Node root = new Node();

	public void insert(String word) {
		Node next = root;
		int len = word.length();
		for(int i = 0; i < len; i++) {
			char nc = word.charAt(i);
			if(next.dic[nc] == null) {
				next.dic[nc] = new Node();
			}
			next = next.dic[nc];
		}
		next.isWord = true;
	}
	
	public boolean search(String word) {
		Node next = root;
		int len = word.length();
		for(int i = 0; i < len; i++) {
			char nc = word.charAt(i);
			if(next.dic[nc] == null) {
				return false;
			}
			next = next.dic[nc];
		}
		return next.isWord;
	}
	
	public boolean startsWith(String word) {
		Node next = root;
		int len = word.length();
		for(int i = 0; i < len; i++) {
			char nc = word.charAt(i);
			if(next.dic[nc] == null) {
				return false;
			}
			next = next.dic[nc];
		}
		return true;
	}
}


class Node{
	
	/** For now - include 127 so we don't have to do wierd math 
	 * Since A - Z are from 15 - 90 and a - b are from 90 - ...
	 */
	Node[] dic = new Node[127];
	boolean isWord;
	
}
