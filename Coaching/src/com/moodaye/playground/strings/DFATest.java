package com.moodaye.playground.strings;
import static org.junit.Assert.*;

import java.util.Iterator;
import java.util.List;

import org.junit.Test;

import junit.framework.AssertionFailedError;

public class DFATest {

	@Test
	public void testDfa() {
		assertEquals(-1,DFA.dfa("abc","cbb"));
		assertEquals(1,DFA.dfa("ababac","ba"));
		assertEquals(0,DFA.dfa("aaaaa","a"));
		assertEquals(0,DFA.dfa("aaaaa","aa"));
	}
	
	@Test
	public void testDfaAll(){
		Iterator<Integer> iter = DFA.dfaAll("ababac", "ab").iterator();
		assertEquals(iter.next(), new Integer(0));
		assertEquals(iter.next(), new Integer(2));
		assertEquals(iter.hasNext(), false);
		
		iter = DFA.dfaAll("aaa", "a").iterator();
		assertEquals(iter.next(), new Integer(0));
		assertEquals(iter.next(), new Integer(1));
		assertEquals(iter.next(), new Integer(2));
		assertEquals(iter.hasNext(), false);
		
		iter = DFA.dfaAll("aaa", "ab").iterator();
		assertEquals(iter.hasNext(), false);
	}
	
	@Test
	public void testRot(){
		assertEquals(0, DFA.rotationN("abc","abc"));
		assertEquals(2, DFA.rotationN("abc","bca"));
		assertEquals(1, DFA.rotationN("abc","cab"));
		assertEquals(-1, DFA.rotationN("abc","cbb"));
		assertEquals(4, DFA.rotationN("abracadabra","abraabracad"));
		assertEquals(7, DFA.rotationN("abracadabra","cadabraabra"));
	}
	
	@Test
	public void testPrefixes(){
		List<String> list = Funwords.prefixes("Rajiv", "RaRajiv");
		assertEquals(2, list.size());
		assertEquals("Rajiv", list.get(0));
		assertEquals("Ra", list.get(1));
		
		List<String> list2 = Funwords.prefixes("Rajiv", "Rajiv");
		assertEquals(1, list2.size());
		assertEquals("Rajiv", list.get(0));
		
		List<String> list3 = Funwords.prefixes("abracadabra", "abraacadabra");
		assertNull(list3);
		
		List<String> list4 = Funwords.prefixes("aaaa", "aaaa");
//		assertEquals(1, list4.size());
	}

}
