kmp:

basic construction:

we construct prefix-function.

string s

prefix[i] - for (prefix of size i of string s)=p
is answers what is the length of the longest prefix of p that is equal to suffix of p and has length strictly less than p

        a b r a c a d a b r a
i       1 2 3 4 5 6 7 8 9 10 11
prefix  0 0 0 1 0 1 0 1 2 3 4


1. How to efficiently fill this structure.
2. How to use it for search.


During the search we maintain size of maximum prefix of string s that ends in the given position in string t in which we search

prefix = 4  compare 5-th character of s to the next character of t.
if it matches then we just increase prefix size.

    a b a b d a b r a \ c
    1 2 1 2 0 1 2 3 4 \ 5

next letter equals -> we increase prefix.

prefix = 4 compare 5-th ch of s to next ch of t, they do not match
-> we shrink prefix 4->1 according to prefix function.
decrease to 1. Compare 2nd ch of s to next ch of t, they match -> increase prefix. get 2.

    a b a b d a b r a \ b
    1 2 1 2 0 1 2 3 4 \ 2

prefix function 'knows' that for prefix of size 4 its own prefix equal to its suffix has size 1.

prefix = 4 compare 5-th ch of s to next ch of t, they do not match
-> we shrink prefix 4->1 according to prefix function.
decrease to 1. Compare 2nd ch of s to next ch of t, they do not match
-> we shrink prefix 1->0 according to prefix function.
Compare 1st ch of s to next ch of t, they match -> increase prefix.

    a b a b d a b r a \ a
    1 2 1 2 0 1 2 3 4 \ 1

prefix = 4 compare 5-th ch of s to next ch of t, they do not match
-> we shrink prefix 4->1 according to prefix function.
decrease to 1. Compare 2nd ch of s to next ch of t, they do not match
-> we shrink prefix 1->0 according to prefix function.
Compare 1st ch of s to next ch of t, they do match -> prefix is 0.

    a b a b d a b r a \ d
    1 2 1 2 0 1 2 3 4 \ 0

1. How to efficiently fill this structure.

same way as in search.

for first char -> 0

 a b r a c a d a b r a
 0

second char -> we can either 

    ******
  --------
abababababc
--------
******
  ******

 a b a c a b a // c
 0 0 1 0 1 2 3 // 4

 a b a c a b a // b
 0 0 1 0 1 2 3 // 2

 a b a c a b a // a
 0 0 1 0 1 2 3 // 1

 a b a c a b a // d
 0 0 1 0 1 2 3 // 0

int kmp(const std::vector<int> &where, const std::vector<int> &what) {
	std::vector<int> prefix(what.size() + 1, 0); // prefix[len] -> len
	int len = 0;
	for (int i = 1; i < (int)what.size(); i++) { // i = last element
		while (true) {
			if (what[i] == what[len]) { // can add 1 element
				len++;
				break;
			}
			if (len == 0) {
				break;
			}
			len = prefix[len];
		}
		prefix[i + 1] = len; // [len = last + 1]
	}
	len = 0;
	for (int i = 0; i < (int)where.size(); i++) {
		while (true) {
			if (where[i] == what[len]) {
				len++;
				break;
			}
			if (len == 0) {
				break;
			}
			len = prefix[len];
		}
		if (len == (int)what.size()) {
			return i + 1 - len; // position of the first matching
		}
		// len = prefix[len]; if we need to find more
	}
	return -1; // not found
}
