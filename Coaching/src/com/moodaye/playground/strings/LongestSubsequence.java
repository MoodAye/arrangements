package com.moodaye.playground.strings;

import java.util.List;

public class LongestSubsequence {

	/**
	 * Traverse grid to find transition and retunr the char appended to strings (sequences)
	 * E.,g given sequences cream, racerman the vals are (via dp)
	 * 
	 *   |  0  r  a  c  e  r  m  a  n
	 * ------------------------------
	 * 0 |  0  0  0  0  0  0  0  0  0
	 * c |  0  0  0 *1  1  1  1  1  1 
	 * r |  0 *1  1  1  1 *2  2  2  2 
	 * e |  0  1  1  1 *2  2  2  2  2 
	 * a |  0  1 *2  2  2  2  2 *3  3
	 * m |  0  1  2  2  2  2 *3  3 *3* 
	 * 
	 * The * cells are transition points. 
	 * The ** cell is the starting point
	 * We can find the transition points by finding the max cells in the outermost column.
	 * Then traversing the row looking for a transition of the max value from both above and left
	 * 
	 * Once we find the next transition point - we note the character - append to the strings and
	 * recurse with the subgrid
	 * 
	 * A transition cell with a value of 1 ends the traversal
	 * 
	 * 
	 * 2nd recurse
	 *   |  0  r  a  c  e  r  m 
	 * ------------------------
	 * 0 |  0  0  0  0  0  0  0
	 * c |  0  0  0 *1  1  1  1 
	 * r |  0 *1  1  1  1 *2  2 
	 * e |  0  1  1  1 *2  2  2 
	 * a |  0  1 *2  2  2  2  2
	 * m |  0  1  2  2  2  2 *3
	 * 
	 * 
	 * 3rd recurse
	 *   |  0  r  a
	 * ------------
	 * 0 |  0  0  0
	 * c |  0  0  0 
	 * r |  0 *1  1 
	 * e |  0  1  1 
	 * a |  0  1 *2 
	 * m |  0  1  2 
	 * 
	 * 4th recurse - End
	 *   |  0  r
	 * ---------
	 * 0 |  0  0
	 * c |  0  0 
	 * r |  0 *1
	 * 
	 * 
	 * Next recurse start
	 * 
	 *   |  0  r  a  c  e  r  m  a
	 * ---------------------------
	 * 0 |  0  0  0  0  0  0  0  0
	 * c |  0  0  0 *1  1  1  1  1
	 * r |  0 *1  1  1  1 *2  2  2
	 * e |  0  1  1  1 *2  2  2  2
	 * a |  0  1 *2  2  2  2  2 *3*
	 * 
	 * 
	 *   |  0  r  a  c  e
	 * ------------------
	 * 0 |  0  0  0  0  0
	 * c |  0  0  0 *1  1
	 * r |  0 *1  1  1  1
//	 * e |  0  1  1  1 *2*
	 * 
	 *3rd recurse
	 *   |  0  r
	 * ---------
	 * 0 |  0  0
	 * c |  0  0
	 * r |  0 *1*
	 * 
	 * 4th (backtrack)
	 *   |  0  r  a  c
	 * ---------------
	 * 0 |  0  0  0  0
	 * c |  0  0  0 *1*
	 * 
	 *
	 * 
	 * @param grid
	 * @param vals
	 * @return
	 */
	private List<String> nextChange(char[] grid, int[] vals, int startX, int startY, List<String> seq){
		//find max value of 
		
		
		return null;
	}
}
