package com.moodaye.playground;

import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/* write a program that beeps every 5 seconds for 2 minutes.
 *  so we will created a runnable - use a scheduledExecutor class
 *  Then create another thread that uses the future for the prior thread
 *  to cancel it after 2 minutes.
 */
public class Conc1 {
	public static void main(String[] args) {
		ScheduledExecutorService ses =  Executors.newScheduledThreadPool(1);
		Future<?> f =  ses.scheduleAtFixedRate(new Runnable() {

			@Override
			public void run() {
				System.out.println("Beep!!!");
			}
			
		}, 0, 5, TimeUnit.SECONDS);
		
		ScheduledExecutorService ses2 = Executors.newSingleThreadScheduledExecutor();
		ses2.schedule(new Runnable() {

			@Override
			public void run() {
				System.out.println("Cancelling dawg");
				f.cancel(true);
			}
			
			
		}, 2, TimeUnit.MINUTES);
	}
	
}
	
