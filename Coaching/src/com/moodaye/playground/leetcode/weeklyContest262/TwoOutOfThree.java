package com.moodaye.playground.leetcode.weeklyContest262;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;
import java.util.Set;

// Problem  10:42am - 11:02am
public class TwoOutOfThree {
	void solve(Scanner in, PrintWriter out) {
		int n1 = in.nextInt();
		int[] nums1 = new int[n1];
		for (int i = 0; i < n1; i++) {
			nums1[i] = in.nextInt();
		}
		
		int n2 = in.nextInt();
		int[] nums2 = new int[n2];
		for (int i = 0; i < n2; i++) {
			nums2[i] = in.nextInt();
		}
		
		int n3 = in.nextInt();
		int[] nums3 = new int[n3];
		for (int i = 0; i < n3; i++) {
			nums3[i] = in.nextInt();
		}
		
		Set<Integer> tt = new HashSet<>();
		
		extractTt(nums1, nums2, nums3, tt);
		extractTt(nums2, nums1, nums3, tt);
		extractTt(nums3, nums2, nums1, tt);
		
		
	}
	
	public void extractTt(int[] nums1, int[] nums2, int[] nums3, Set<Integer> tt) {
		for(int i = 0; i < nums1.length ; i++) {
			int next = nums1[i];
			
			for(int j = 0; j < nums2.length ; j++) {
				if(nums2[j] == next) {
					tt.add(next);
				}
			}
			
			for(int j = 0; j < nums3.length ; j++) {
				if(nums3[j] == next) {
					tt.add(next);
				}
			}
		}
		
	}

	public static void main(String[] args) {
		new TwoOutOfThree().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
