package com.moodaye.playground.leetcode.easy;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.List;

public class BSTPostOrderTraversal {

  public static class TreeNode {
      public int val;
      public TreeNode left;
      public TreeNode right;
      TreeNode() {}
      public TreeNode(int val) { this.val = val; }
      TreeNode(int val, TreeNode left, TreeNode right) {
          this.val = val;
          this.left = left;
          this.right = right;
      }
  }
 
    public List<Integer> postorderTraversal(TreeNode root) {
        List<Integer> list = new ArrayList<>();
        return postorderTraversal(root, list);
    }
    
    public List<Integer> postorderTraversal(TreeNode root, List<Integer> list) {
        if(root == null){
            return list;
        }
 
        list.addAll(postorderTraversal(root.left) );
        list.addAll(postorderTraversal(root.right));
        list.add(root.val);
        
        return list;
    }
    
    // iterative
        public List<Integer> postorderTraversal2(TreeNode root) {
        List<Integer> list = new ArrayList<>();
            
        if(root == null){
            return list;
        }
 
        Deque<TreeNode> stack = new ArrayDeque<>();
        stack.push(root);
        while(!stack.isEmpty()){
            TreeNode next = stack.pop();
            list.addAll(postorderTraversal(next.left) );
            list.addAll(postorderTraversal(next.right));
            list.add(next.val);
        }
            
        return list;
    }
}
