package com.moodaye.playground.leetcode.easy;

// 4 ways:
// 1. bf 2. ht 3. 2p  4. reverse ll
public class IntersectionTwoLinkedLists {

	/** Brute Force */
	public ListNode getIntersectionNodeBf(ListNode headA, ListNode headB) {
		ListNode intersect = headA;
		while (headB != null) {
			while (intersect != null) {
				if (intersect == headB) {
					return intersect;
				}
				intersect = intersect.next;
			}
			headB = headB.next;
		}
		return null;
	}
}
