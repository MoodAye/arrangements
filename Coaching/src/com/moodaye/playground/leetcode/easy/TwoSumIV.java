package com.moodaye.playground.leetcode.easy;

import com.moodaye.playground.leetcode.easy.BSTPostOrderTraversal.TreeNode;

// lc 653
public class TwoSumIV {
	 public boolean findTarget(TreeNode root, int k) {
		 if(root == null) return false;
		 
		 if(k - root.val < root.val) {
			if(findNode(root.left, k - root.val)) return true;
		 }
		 else if(k - root.val > root.val) {
			if(findNode(root.right, k - root.val)) return true;
		 }
		 
		 return findTarget(root.left, k) || findTarget(root.right, k);
	}
	 
	 private boolean findNode(TreeNode root, int k) {
		 if(root == null) return false;
		 if(root.val == k) return true;
		 return findNode(root.left, k) || findNode(root.right, k);
	 }
}
