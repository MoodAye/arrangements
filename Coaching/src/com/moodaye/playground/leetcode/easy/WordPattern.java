package com.moodaye.playground.leetcode.easy;

import java.util.HashMap;
import java.util.Map;

public class WordPattern {
	public boolean wordPattern(String pattern, String s) {
		
		String[] words = s.split(" ");
		if(pattern.length() != words.length) {
			return false;
		}
		
		Map<Character, String> cmap = new HashMap<>();
		Map<String, Character> smap = new HashMap<>();
		
		for(int i = 0; i < words.length; i++) {
			// is letter in map
			char nextC = pattern.charAt(i);
			
			if(cmap.containsKey(nextC)) {
				String word = cmap.get(nextC);
				if(!word.equals(words[i])) {
					return false;
				}
			}
			else {
				cmap.put(nextC, words[i]);
			}
			
			if(smap.containsKey(words[i])) {
				Character c = smap.get(words[i]);
				if(c != nextC) {
					return false;
				}
			}
			else {
				smap.put(words[i], nextC);
			}
		}
		
		return true;
	}
}
