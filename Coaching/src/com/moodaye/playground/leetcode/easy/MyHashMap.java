package com.moodaye.playground.leetcode.easy;

import java.util.Arrays;

public class MyHashMap {
	
	int[] hashMap;
	
	/** empty map - supports keys 0 to size of array */
	public MyHashMap() {
		hashMap = new int[1_000_001];
		Arrays.fill(hashMap, -1);
	}
	
	/** if key exists - updates the class */
	public void put(int key, int value) {
		hashMap[key] = value;
	}
	
	/** -1 returned if key does not exist */
	public int get(int key) {
		return hashMap[key];
	}
	
	/** removes key */
	public void remove(int key) {
		hashMap[key] = -1;
	}
}
