package com.moodaye.playground.leetcode.easy;

import java.util.ArrayDeque;
import java.util.Deque;

import com.moodaye.playground.leetcode.easy.BSTPostOrderTraversal.TreeNode;

// lc
public class MaximumDepthBinaryTree {
	
	   public int maxDepth(TreeNode root) {
		   if(root == null) return 0;
		   int maxDepth = 0;
		   return maxDepth(root, 1);
	    }
	   
	   public int maxDepth(TreeNode root, int depthToHere) {
		   if(root.left == null && root.right == null) {
			   return depthToHere;
		   }
		   
		   int maxLeft = 0;
		   if(root.left != null) {
			  maxLeft = maxDepth(root.left, depthToHere + 1);
		   }
		   
		   int maxRight = 0;
		   if(root.right != null) {
			  maxRight = maxDepth(root.right, depthToHere + 1);
		   }
		   
		   return Math.max(maxLeft, maxRight);
	   }
	   
}
