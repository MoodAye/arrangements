package com.moodaye.playground.leetcode.easy;

// lc 807pm
public class RemoreNthNodeFromEndOfList {
	public ListNode removeNthFromEnd(ListNode head, int n) {
		int cnt = 1;
		ListNode ptr = head;

		while (ptr.next != null) {
			ptr = ptr.next;
			cnt++;
		}
		
		int rem = cnt - n + 1;
		cnt = 1;
		ptr = head;
		ListNode prev = ptr;
		
		while(cnt < rem) {
			prev = ptr;
			ptr = ptr.next;
			cnt++;
		}
		
		prev.next = ptr.next;
		
		return head;
	}
}
