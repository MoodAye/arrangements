package com.moodaye.playground.leetcode.easy;

// lc 839pm - 842pm
public class RemoveDuplicatesFromSortedList {
	public ListNode deleteDuplicates(ListNode head) {
		if (head == null || head.next == null)
			return head;

		ListNode ptr = head.next;
		ListNode prev = head;

		while (ptr != null) {
			if (ptr.val == prev.val) {
				prev.next = ptr.next;
			} else {
				prev = ptr;
			}
			ptr = ptr.next;
		}
		return head;
	}
}
