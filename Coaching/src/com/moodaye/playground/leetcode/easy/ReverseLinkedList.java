package com.moodaye.playground.leetcode.easy;

import java.util.Scanner;

// LC problem 732pm - 747pm
public class ReverseLinkedList {
	public ListNode reverseList(ListNode head) {
		if(head == null) return head;
		ListNode ptr = head.next;
		
		while(ptr != null) {
			ListNode temp = ptr;
			ptr = ptr.next;
			temp.next = head;
			head = temp;
		}
		return head;
	}
	
	public ListNode reverseListRecursive(ListNode head) {
		return reverse(null, head);
	}
	
	public ListNode reverse(ListNode head, ListNode next) {
		if(next == null) return head;
		ListNode temp = next.next;
		next.next = head;
		return reverse(next, temp);
	}
}
