package com.moodaye.playground.leetcode.easy;

import java.util.ArrayDeque;
import java.util.Deque;

import com.moodaye.playground.leetcode.easy.BSTPostOrderTraversal.TreeNode;

// lc101 145am
public class SymmtetricTree {
	public boolean isSymmetric(TreeNode root) {
		if(root == null) return true;
		
		Deque<TreeNode> leftStk = new ArrayDeque<>();
		Deque<TreeNode> rightStk = new ArrayDeque<>();
		
		leftStk.push(root);
		rightStk.push(root);
		
		while(!leftStk.isEmpty()) {
			if(rightStk.isEmpty()) return false;
			
			TreeNode left = leftStk.pop();
			TreeNode right = rightStk.pop();
			
			if(left.val != right.val) return false;
		
			if(left.left == null && right.right != null) return false;
			if(left.left != null && right.right == null) return false;
			if(left.right == null && right.left != null) return false;
			if(left.right != null && right.left == null) return false;
			
			if(left.left != null) {
			leftStk.push(left.left);
			rightStk.push(right.right);
			}
			if(left.right != null) {
			leftStk.push(left.right);
			rightStk.push(right.left);
			}
		}
		
		
		return true;
	}
}
