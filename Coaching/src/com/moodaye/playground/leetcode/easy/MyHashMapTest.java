package com.moodaye.playground.leetcode.easy;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class MyHashMapTest {

	@Test
	void test() {
		MyHashMap obj = new MyHashMap();
		  obj.put(1,2);
		  int param_2 = obj.get(1);
		  assertEquals(2, param_2);
		  obj.remove(1);
		  assertEquals(-1, obj.get(1));
	}
}
