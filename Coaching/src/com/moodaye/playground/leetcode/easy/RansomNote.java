package com.moodaye.playground.leetcode.easy;

//lc 11pm 1104pm
public class RansomNote {
	public boolean canConstruct(String ransomNote, String magazine) {
		if (ransomNote.length() > magazine.length())
			return false;

		int ransomCntr[] = new int[26];
		int magazineCntr[] = new int[26];

		for (char c : ransomNote.toCharArray()) {
			ransomCntr[c - 'a']++;
		}
		for (char c : magazine.toCharArray()) {
			magazineCntr[c - 'a']++;
		}

		for (int i = 0; i < 26; i++) {
			if (magazineCntr[i] < ransomCntr[i])
				return false;
		}
		return true;
	}
}
