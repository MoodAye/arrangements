package com.moodaye.playground.leetcode.easy;

import java.util.ArrayDeque;
import java.util.Deque;

// lc 20 10:43 - 10:52
public class ValidParentheses {
	public static boolean isValid(String s) {
		char[] cs = s.toCharArray();
		Deque<Character> stack = new ArrayDeque<>();
		for(int i = 0; i < cs.length; i++) {
			if(cs[i] == ')' || cs[i] == ']' || cs[i] == '}') {
				if(stack.isEmpty()) return false;
				char par = stack.pop();
				if(cs[i] == ')' && par != '(') return false;
				if(cs[i] == '}' && par != '{') return false;
				if(cs[i] == ']' && par != '[') return false;
			}
			else {
				stack.push(cs[i]);
			}
		}
		return stack.isEmpty();
	}
}
