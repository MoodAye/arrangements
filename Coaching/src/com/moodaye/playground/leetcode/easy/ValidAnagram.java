package com.moodaye.playground.leetcode.easy;

import java.util.Arrays;

// lc 1105pm 1107pm
public class ValidAnagram {
	public boolean isAnagram(String s, String t) {
		if(s.length() != t.length()) return false;
		char[] cs = s.toCharArray();
		char[] ct = t.toCharArray();
		Arrays.sort(cs);
		Arrays.sort(ct);
		
		for(int i = 0; i < cs.length; i++) {
			if(cs[i] != ct[i]) return false;
		}
		return true;
	}
}
