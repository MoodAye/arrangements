package com.moodaye.playground.leetcode.easy;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class MergeSortedArrayTest {

	@Test
	void test1() {
		int[] a = {10,20,0,0};
		int[] b = {4,12};
		int[] expected = {4,10,12,20};
		new MergeSortedArray().merge(a, 2, b, 2);
		assertArrayEquals(expected, a);
	}
	
	@Test
	void test2() {
		int[] a = {10,0};
		int[] b = {4};
		int[] expected = {4,10};
		new MergeSortedArray().merge(a, 1, b, 1);
		assertArrayEquals(expected, a);
	}
	
	@Test
	void test3() {
		int[] a = {0};
		int[] b = {4};
		int[] expected = {4};
		new MergeSortedArray().merge(a, 0, b, 1);
		assertArrayEquals(expected, a);
	}
	@Test
	void test4() {
		int[] a = {10};
		int[] b = {};
		int[] expected = {10};
		new MergeSortedArray().merge(a, 1, b, 0);
		assertArrayEquals(expected, a);
	}
	
	@Test
	void test5() {
		int[] a = {10,10,0};
		int[] b = {10};
		int[] expected = {10,10,10};
		new MergeSortedArray().merge(a, 2, b, 1);
		assertArrayEquals(expected, a);
	}

}