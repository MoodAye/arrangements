package com.moodaye.playground.leetcode.easy;

import static org.junit.Assert.assertNull;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

class MergeTwoLinkedListsTest {

	@Test
	void test1() {
		ListNode list1 = new ListNode(10);
		ListNode next = new ListNode(20);
		list1.next = next;
		next.next = new ListNode(30);
		next = next.next;
		next.next = new ListNode(40);

		ListNode list2 = new ListNode(11);
		next = new ListNode(12);
		list2.next = next;
		next.next = new ListNode(31);
		next = next.next;
		next.next = new ListNode(50);
		
		MergeTwoLinkedLists mt = new MergeTwoLinkedLists();
		ListNode head = mt.mergeTwoLists(list2, list1);

		assertEquals(10, head.val);
		next = head.next;
		assertEquals(11, next.val);
		next = next.next;
		assertEquals(12, next.val);
		next = next.next;
		assertEquals(20, next.val);
		next = next.next;
		assertEquals(30, next.val);
		next = next.next;
		assertEquals(31, next.val);
		next = next.next;
		assertEquals(40, next.val);
		next = next.next;
		assertEquals(50, next.val);
		next = next.next;
		assertNull(next);
	}
	
	@Test
	void test2() {
		ListNode list1 = null;
		ListNode list2 = new ListNode(11);
		ListNode next = new ListNode(12);
		list2.next = next;
		next.next = new ListNode(31);
		next = next.next;
		next.next = new ListNode(50);
		
		MergeTwoLinkedLists mt = new MergeTwoLinkedLists();
		ListNode head = mt.mergeTwoLists(list1, list2);
		
		assertEquals(11, head.val);
		next = head.next;
		assertEquals(12, next.val);
		next = next.next;
		assertEquals(31, next.val);
		next = next.next;
		assertEquals(50, next.val);
		next = next.next;
		assertNull(next);
	}
	
	@Test
	void test3() {
		ListNode list2 = null;
		ListNode list1 = new ListNode(11);
		ListNode next = new ListNode(12);
		list1.next = next;
		next.next = new ListNode(31);
		next = next.next;
		next.next = new ListNode(50);
		
		MergeTwoLinkedLists mt = new MergeTwoLinkedLists();
		ListNode head = mt.mergeTwoLists(list1, list2);
		
		assertEquals(11, head.val);
		next = head.next;
		assertEquals(12, next.val);
		next = next.next;
		assertEquals(31, next.val);
		next = next.next;
		assertEquals(50, next.val);
		next = next.next;
		assertNull(next);
	}

	@Test
	void test4() {
		ListNode list1 = new ListNode(1);
		ListNode next = new ListNode(2);
		list1.next = next;
		next.next = new ListNode(4);

		ListNode list2 = new ListNode(1);
		next = new ListNode(3);
		list2.next = next;
		next.next = new ListNode(4);
		
		MergeTwoLinkedLists mt = new MergeTwoLinkedLists();
		ListNode head = mt.mergeTwoLists(list2, list1);

		assertEquals(1, head.val);
		next = head.next;
		assertEquals(1, next.val);
		next = next.next;
		assertEquals(2, next.val);
		next = next.next;
		assertEquals(3, next.val);
		next = next.next;
		assertEquals(4, next.val);
		next = next.next;
		assertEquals(4, next.val);
		next = next.next;
		assertNull(next);
	}
	
	@Test
	void test5() {
		ListNode list1 = new ListNode(-10);
		ListNode next = new ListNode(-10);
		list1.next = next;
		next.next = new ListNode(-9);
		next = next.next;
		next.next = new ListNode(-4);
		next = next.next;
		next.next = new ListNode(1);
		next = next.next;
		next.next = new ListNode(6);
		next = next.next;
		next.next = new ListNode(6);

		ListNode list2 = new ListNode(-7);
		
		MergeTwoLinkedLists mt = new MergeTwoLinkedLists();
		ListNode head = mt.mergeTwoLists(list2, list1);

		assertEquals(-10, head.val);
		next = head.next;
		assertEquals(-10, next.val);
		next = next.next;
		assertEquals(-9, next.val);
		next = next.next;
		assertEquals(-7, next.val);
		next = next.next;
		assertEquals(-4, next.val);
		next = next.next;
		assertEquals(1, next.val);
		next = next.next;
		assertEquals(6, next.val);
		next = next.next;
		assertEquals(6, next.val);
		next = next.next;
		assertNull(next);
	}
}
