package com.moodaye.playground.leetcode.easy;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class MajorityElementTest {

	@Test
	void test() {
		int[] a = {1,1,2};
		MajorityElement me = new MajorityElement();
		assertEquals(1, me.majorityElement(a));
		
		int[] b = {2,2,1,1,1,2,2};
		assertEquals(2, me.majorityElement(b));
	}

}
