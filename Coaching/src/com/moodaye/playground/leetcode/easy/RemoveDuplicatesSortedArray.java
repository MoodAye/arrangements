package com.moodaye.playground.leetcode.easy;

import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Locale;
import java.util.Scanner;

// Problem 
public class RemoveDuplicatesSortedArray {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int[] nums = new int[n];
		for (int i = 0; i < nums.length; i++) {
			nums[i] = in.nextInt();
		}
		System.out.println();
		System.out.println(removeDuplicates2(nums));
		Arrays.stream(nums).forEach((a) -> {
			System.out.print(a + " ");
		});

	}

	public int removeDuplicates2(int[] a) {
		int p1 = 0;
		int p2 = 0;

		for (p2 = 0; p2 < a.length; p2++) {
			if (a[p1] == a[p2]) continue;
			++p1;
			if (p1 != p2) exch(a, p1, p2);
		}
		return p1 + 1;
	}

	public int removeDuplicates(int[] a) {
		int p1 = 0;
		int p2 = 0;
		while (p2 < a.length) {
			while (a[p1] == a[p2]) {
				p2++;
				if (p2 == a.length) {
					break;
				}
			}
			++p1;
			if (p2 == a.length) {
				break;
			}
			if (p1 != p2)
				exch(a, p1, p2);
			p2++;
		}
		return p1 + 1;
	}

	public void exch(int[] a, int i, int j) {
		if(i >= a.length || j >= a.length) return;
		int temp = a[i];
		a[i] = a[j];
		a[j] = temp;
	}

	public static void main(String[] args) {
		new RemoveDuplicatesSortedArray().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
