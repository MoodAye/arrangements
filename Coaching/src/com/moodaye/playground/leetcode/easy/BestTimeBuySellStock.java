package com.moodaye.playground.leetcode.easy;

// lc 121 826am - 833am
public class BestTimeBuySellStock {
	  public int maxProfit(int[] prices) {
		  int currMin = Integer.MAX_VALUE;
		  int max = 0;
		  
		  for(int i = 0; i < prices.length; i++) {
			  max = Math.max(max, prices[i] - currMin);
			  currMin = Math.max(currMin, prices[i]);
		  }
		  
		  return max;
	    }
}
