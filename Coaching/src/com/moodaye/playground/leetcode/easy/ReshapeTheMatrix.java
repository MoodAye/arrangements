package com.moodaye.playground.leetcode.easy;

// lc 816am - 823am
public class ReshapeTheMatrix {
	public int[][] matrixReshape(int[][] mat, int r, int c) {
		int m = mat.length;
		int n = mat[0].length;
		
		if(r * c != m * n) return mat;
		
		int[][] reshape = new int[r][c];
		int cr = 0;
		int cc = 0;
	
		/*
		 *  1 2
		 *  3 4 ---->  1 2 3 4
		 *  m = 2      r = 1 
		 *  n = 2      c = 4
		 *  
		 *  take element from left and populate right
		 * 
		 */
		
		for(int i = 0; i < m; i++) {
			for(int j = 0; j < n; j++) {
				reshape[cr][cc] = mat[i][j];
				cc++;
				if(cc == c) {
					cc = 0;
					cr++;
				}
			}
		}
		
		return reshape;
	}
}
