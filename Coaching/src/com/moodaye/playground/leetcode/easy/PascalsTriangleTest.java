package com.moodaye.playground.leetcode.easy;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import org.junit.jupiter.api.Test;

class PascalsTriangleTest {

	@Test
	void test() {
		PascalsTriangle pt = new PascalsTriangle();
		
		List<List<Integer>> one = pt.generate(1);
		List<List<Integer>> two = pt.generate(2);
		List<List<Integer>> three = pt.generate(3);
		List<List<Integer>> four = pt.generate(4);
		
		assertEquals(1, one.size());
		assertEquals(2, two.size());
		assertEquals(3, three.size());
		assertEquals(4, four.size());
		
		int[] expected = {1,3,3,1};
		int[] afour3 = new int[4];
		for(int i = 0; i < 4; i++) {
			afour3[i] = four.get(3).get(i);
		}
		
		assertArrayEquals(expected, afour3);
		
		
	}

}
