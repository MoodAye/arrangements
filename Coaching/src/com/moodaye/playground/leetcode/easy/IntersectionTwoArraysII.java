package com.moodaye.playground.leetcode.easy;

import java.util.HashMap;
import java.util.Map;

public class IntersectionTwoArraysII {
	
	public int[] intersect(int[] nums1, int[] nums2) {
		Map<Integer, Integer> map = new HashMap<>();
		for(int i = 0; i < nums1.length; i++) {
			map.merge(nums1[i], 1, (a,b) -> a + b);
		}
		
		for(int i = 0; i < nums2.length; i++) {
			if(map.containsKey(nums2[i])) {
				int val = map.get(nums2[i]);
				if(val - 1 > 0) {
					map.put(nums2[i], val - 1);
				}
				else {
					map.remove(nums2[i]);
				}
			}
		}
		
		int n = map.values().stream().reduce(Integer::sum).get();
		int[] ans  = new int[n];
		int idx = 0;
		for(int key: map.keySet()) {
			int count = map.get(key);
			while(count-- > 0) ans[idx++] = key;
		}
		
		return ans;
    }
}
