package com.moodaye.playground.leetcode.easy;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

import org.junit.jupiter.api.Test;

class MoveZeroesTest {

	@Test
	void test1() {
		int[] a = {0,1,0,3,12};
		int[] expected = {1,3,12,0,0};
		new MoveZeroes().moveZeroes(a);
		assertArrayEquals(expected, a);
	}

	@Test
	void test2() {
		int[] a = {0,0};
		int[] expected = {0,0};
		new MoveZeroes().moveZeroes(a);
		assertArrayEquals(expected, a);
	}
	
	@Test
	void test3() {
		int[] a = {0};
		int[] expected = {0};
		new MoveZeroes().moveZeroes(a);
		assertArrayEquals(expected, a);
	}
	
	@Test
	void test4() {
		int[] a = {0,0,1,1};
		int[] expected = {1,1,0,0};
		new MoveZeroes().moveZeroes(a);
		assertArrayEquals(expected, a);
	}
}
