package com.moodaye.playground.leetcode.easy;

import java.util.HashMap;
import java.util.Map;

public class MajorityElement {
	public int majorityElement(int[] nums) {
		Map<Integer, Integer> cntr = new HashMap<>();

		for (int n : nums) {
			cntr.merge(n, 1, (a, b) -> a + 1);
		}

		int ans = 0;
		for (int key : cntr.keySet()) {
			if (cntr.get(key) > nums.length / 2)
				ans = key;
		}

		return ans;
	}
}
