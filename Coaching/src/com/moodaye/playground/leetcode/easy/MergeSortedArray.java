package com.moodaye.playground.leetcode.easy;

public class MergeSortedArray {
	public void merge(int[] nums1, int m, int[] nums2, int n) {
		int ptr1 = m - 1;
		int ptr2 = n - 1;
		int ptr3 = m + n - 1;
		for (int i = 0; i < m + n; i++) {
			if (ptr2 < 0 || (ptr1 >= 0 && nums1[ptr1] >= nums2[ptr2])) {
				nums1[ptr3--] = nums1[ptr1--];
			} else {
				nums1[ptr3--] = nums2[ptr2--];
			}
		}
	}
}
