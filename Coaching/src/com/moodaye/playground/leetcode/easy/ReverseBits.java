package com.moodaye.playground.leetcode.easy;

public class ReverseBits {
	public int reverseBits(int n) {
		int left = 31;
		int right = 0;

		while (left > right) {
			int leftMask = 1 << left--;
			int rightMask = 1 << right++;

			int leftDigit = (n & leftMask) == 0 ? 0 : 1;
			int rightDigit = (n & rightMask) == 0 ? 0 : 1;

			if (leftDigit != rightDigit) {
				n ^= leftMask;
				n ^= rightMask;
			}
		}
		return n;
	}
}
