package com.moodaye.playground.leetcode.easy;

import java.util.ArrayDeque;
import java.util.Deque;

// lc 825pm - 845pm
public class FloodFill {
	public int[][] floodFill(int[][] image, int sr, int sc, int newColor) {
		
		Deque<Point> stack = new ArrayDeque<>();
		stack.push(new Point(sr, sc));
		int match = image[sr][sc];
		
		while(!stack.isEmpty()) {
			Point next = stack.pop();
			image[next.r][next.c] = -newColor;
			// cell to right
			if(next.c != image[0].length - 1) {
				if(image[next.r][next.c + 1] == match) {
					stack.push(new Point(next.r, next.c + 1));
				}
			}
			// cell to bottom
			if(next.r != image.length - 1) {
				if(image[next.r + 1][next.c] == match) {
					stack.push(new Point(next.r + 1, next.c));
				}
			}
			// cell to left
			if(next.r != 0) {
				if(image[next.r - 1][next.c] == match) {
					stack.push(new Point(next.r - 1, next.c));
				}
			}
			// cell to top
			if(next.c != 0) {
				if(image[next.r][next.c - 1] == match) {
					stack.push(new Point(next.r, next.c - 1));
				}
			}
		}
		
		for(int r = 0; r < image.length; r++) {
			for(int c = 0; c < image[0].length; c++) {
				if(image[r][c] < 0) {
					image[r][c] *= -1;
				}
			}
		}
		
		return image;
	}
	
	static class Point{
		int r;
		int c;
		Point(int r, int c){
			this.r = r;
			this.c =c ;
		}
	}
}