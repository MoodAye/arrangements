package com.moodaye.playground.leetcode.easy;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class AddStringsTest {

	@Test
	void test1() {
		AddStrings as1 = new AddStrings();
		assertEquals("2", as1.addStrings("1", "1"));
		assertEquals("15", as1.addStrings("7", "8"));
		assertEquals("134", as1.addStrings("11", "123"));
		assertEquals("533", as1.addStrings("456", "77"));
		assertEquals("0", as1.addStrings("0", "0"));
		assertEquals("19998", as1.addStrings("9999", "9999"));
		
	}

}
