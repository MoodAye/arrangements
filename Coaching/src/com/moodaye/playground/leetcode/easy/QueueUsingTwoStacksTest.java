package com.moodaye.playground.leetcode.easy;

import static org.junit.Assert.assertFalse;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class QueueUsingTwoStacksTest {

	@Test
	void test1() {
		QueueUsingTwoStacks q = new QueueUsingTwoStacks();
		q.push(1);
		q.push(2);
		assertEquals(1, q.peek());
		assertEquals(1, q.pop());
		assertFalse(q.empty());
		assertEquals(2,q.pop());
		assertTrue(q.empty());
	}
	
	@Test
	void test2() {
		QueueUsingTwoStacks q = new QueueUsingTwoStacks();
		q.push(1);
		q.push(2);
		q.push(3);
		q.push(4);
		assertEquals(1, q.pop());
		
	}

}
