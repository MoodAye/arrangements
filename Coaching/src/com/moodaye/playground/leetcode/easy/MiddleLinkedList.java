package com.moodaye.playground.leetcode.easy;

// 755 - 8:03
public class MiddleLinkedList {
	  public ListNode middleNode(ListNode head) {
		  ListNode slow = head;
		  ListNode fast = head;
		  
		  while(fast.next == null || fast.next.next == null) {
			  slow = slow.next;
			  fast = fast.next.next;
		  }
		  
		  if(fast.next != null) slow = slow.next; 
		  return slow;
	  }
}
