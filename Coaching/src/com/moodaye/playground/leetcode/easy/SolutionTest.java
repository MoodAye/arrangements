package com.moodaye.playground.leetcode.easy;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class SolutionTest {

	@Test
	void test() {
		Solution soln = new Solution();
		assertTrue(soln.isValid("()"));
		assertFalse(soln.isValid(")"));
	}

}
