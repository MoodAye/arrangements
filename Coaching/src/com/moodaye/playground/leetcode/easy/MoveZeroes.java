package com.moodaye.playground.leetcode.easy;

public class MoveZeroes {
	// optimal - 2 pointers
	public void moveZeroes(int[] nums) {
		for(int lastZero = 0, curr = 0; curr < nums.length; curr++) {
			if(nums[curr] != 0) {
				swap(nums, lastZero, curr);
				lastZero++;
			}
		}
	}
	
	public void moveZeroesSubOptimal(int[] nums) {
		// 0 1 0 3 12
		for (int i = 0; i < nums.length - 1; i++) {
			if (nums[i] != 0)
				continue;
			for (int j = i + 1; j < nums.length; j++) {
				if (nums[j] != 0) {
					swap(nums, i, j);
					break;
				}
			}
		}
	}

	public void swap(int[] nums, int i, int j) {
		int temp = nums[i];
		nums[i] = nums[j];
		nums[j] = temp;
	}
}
