package com.moodaye.playground.leetcode.easy;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PascalsTriangle {
	public List<List<Integer>> generate(int numRows) {
        List<List<Integer>> pt = new ArrayList<>();
        
        pt.add(List.of(1));
        if(numRows == 1) return pt;
        
        pt.add(List.of(1,1));
        if(numRows == 2) return pt;
        
        for(int i = 2; i < numRows; i++) {
        	List<Integer> currRow= new ArrayList<>();
        	currRow.add(1);
        	List<Integer> prevRow = pt.get(i - 1);
        	for(int j = 1; j < prevRow.size(); j++) {
        		int sum = prevRow.get(j - 1) + prevRow.get(j);
        		currRow.add(sum);
        	}
        	currRow.add(1);
        	pt.add(currRow);
        }
        return pt;
    }
}
