package com.moodaye.playground.leetcode.easy;

import java.util.HashMap;
import java.util.Map;

// lc 350   838am - 850am
public class IntersectionOfTwoArrays {
	public int[] intersect(int[] nums1, int[] nums2) {
		Map<Integer, Integer> map1 = new HashMap<>();
		Map<Integer, Integer> map2 = new HashMap<>();
		Map<Integer, Integer> mapRes = new HashMap<>();

		for (int i : nums1)
			map1.merge(i, 1, (a, b) -> a + 1);
		for (int i : nums2)
			map2.merge(i, 1, (a, b) -> a + 1);

		int len = 0;
		for (int key : map1.keySet()) {
			if (map2.containsKey(key)) {
				int cnt = Math.min(map1.get(key), map2.get(key));
				mapRes.put(key, cnt);
				len += cnt;
			}
		}

		int[] result = new int[len];

		int idx = 0;
		for (int key : mapRes.keySet()) {
			int cnt = mapRes.get(key);
			while (cnt-- > 0) {
				result[idx] = key;
				idx++;
			}
		}
		return result;
	}
}
