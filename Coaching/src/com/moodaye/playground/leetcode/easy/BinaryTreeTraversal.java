package com.moodaye.playground.leetcode.easy;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.List;

import com.moodaye.playground.leetcode.easy.BSTPostOrderTraversal.TreeNode;

// lc - 12:15am
public class BinaryTreeTraversal {
	public List<Integer> preorderTraversal(TreeNode root) {
		if (root == null) {
			return List.of();
		}

		return preorderTraversal(root, new ArrayList<Integer>());
	}

	public List<Integer> preorderTraversal(TreeNode root, List<Integer> prot) {
		if (root == null) {
			return prot;
		}

		Deque<TreeNode> stack = new ArrayDeque<>();
		stack.push(root);

		while (!stack.isEmpty()) {
			TreeNode next = stack.pop();
			prot.add(next.val);
			if(next.left != null) stack.push(next.left);
			if(next.right != null) stack.push(next.right);
		}

		return prot;
	}

	public List<Integer> preorderTraversal2(TreeNode root, List<Integer> prot) {
		if (root == null) {
			return prot;
		}

		prot.add(root.val);
		prot = preorderTraversal(root.left, prot);
		prot = preorderTraversal(root.right, prot);

		return prot;
	}

	public List<Integer> inorderTraversal(TreeNode root) {
		if (root == null) {
			return List.of();
		}

		return inorderTraversal(root, new ArrayList<Integer>());
	}

	public List<Integer> inorderTraversal(TreeNode root, List<Integer> prot) {
		if (root == null) {
			return prot;
		}

		prot = inorderTraversal(root.left, prot);
		prot = inorderTraversal(root.right, prot);
		prot.add(root.val);

		return prot;
	}

	public List<Integer> postorderTraversal(TreeNode root) {
		if (root == null) {
			return List.of();
		}

		return postorderTraversal(root, new ArrayList<Integer>());
	}

	public List<Integer> postorderTraversal(TreeNode root, List<Integer> prot) {
		if (root == null) {
			return prot;
		}

		prot = postorderTraversal(root.left, prot);
		prot = postorderTraversal(root.right, prot);
		prot.add(root.val);

		return prot;
	}
}
