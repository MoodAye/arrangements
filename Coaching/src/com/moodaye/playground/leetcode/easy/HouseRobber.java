package com.moodaye.playground.leetcode.easy;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

// Problem lc 808am - 825am = 17min
public class HouseRobber {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int[] nums = new int[n];
		for (int i = 0; i < nums.length; i++) {
			nums[i] = in.nextInt();
		}
		out.println(rob(nums));
	}
	
	public int rob(int[] nums) {
		if(nums.length == 0) return 0;
		if(nums.length == 1) return nums[0];
		if(nums.length == 2) return Math.max(nums[0], nums[1]);
		if(nums.length == 3) return Math.max(nums[0] + nums[2], nums[1]);
		
		nums[2] += nums[0];
		int max = Math.max(nums[2], nums[1]);
		for(int i = 3; i < nums.length; i++) {
			nums[i] += Math.max(nums[i - 3], nums[i - 2]);
			max = Math.max(nums[i], max);
		}
		return max;
	}

	public static void main(String[] args) {
		new HouseRobber().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
