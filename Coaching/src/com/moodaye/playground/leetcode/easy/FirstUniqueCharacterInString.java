package com.moodaye.playground.leetcode.easy;

import java.util.Arrays;

// lc387 1052pm - 1059pm
public class FirstUniqueCharacterInString {
	public int firstUniqChar(String s) {
		char[] cs = s.toCharArray();
		int[] cntr = new int[26];
		
		for(int i = 0; i < cs.length; i++) {
			cntr[cs[i] - 'a']++;
		}
		
		for(int i = 0; i < cs.length; i++) {
			if(cntr[cs[i] - 'a'] == 1) {
				return i;
			}
		}
		
		return -1;
	}
}
