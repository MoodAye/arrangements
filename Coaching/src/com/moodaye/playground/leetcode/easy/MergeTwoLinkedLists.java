package com.moodaye.playground.leetcode.easy;

// lc 910pm
public class MergeTwoLinkedLists {
	public ListNode mergeTwoLists(ListNode list1, ListNode list2) {
		if(list1 == null) {
			return list2;
		}
		if(list2 == null) {
			return list1;
		}
		
		if(list1.val > list2.val) {
			ListNode temp = list1;
			list1 = list2;
			list2 = temp;
		}
		
		ListNode prev1 = list1;
		ListNode next2 = list2.next;
		ListNode p1 = list1;
		ListNode p2 = list2;
		
		while(true) {
			
			if(p2 == null) {
				break;
			}
			
			while(p1 != null && p2.val >= p1.val) {
				prev1 = p1;
				p1 = p1.next;
			}
			
			prev1.next = p2;
			p2.next = p1;
			prev1 = p2;
			p1 = p2.next;
			p2 = next2;
			
			if(p2 == null) {
				break;
			}
			
			next2 = next2.next;
		}
		
		return list1;
	}
}
