package com.moodaye.playground.leetcode.easy;

import java.util.ArrayDeque;
import java.util.Deque;

public class QueueUsingTwoStacks {
	Deque<Integer> in = new ArrayDeque<>();
	Deque<Integer> out = new ArrayDeque<>();

	public void push(int x) {
		while (!out.isEmpty()) {
			in.push(out.pop());
		}
		in.push(x);
		while (!in.isEmpty()) {
			out.push(in.pop());
		}
	}

	public int pop() {
		while (!in.isEmpty()) {
			out.push(in.pop());
		}
		return out.pop();
	}

	public int peek() {
		while (!in.isEmpty()) {
			out.push(in.pop());
		}
		return out.peek();
	}

	public boolean empty() {
		return in.isEmpty() && out.isEmpty();
	}
}
