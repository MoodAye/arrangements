package com.moodaye.playground.leetcode.easy;

// lc
public class AddStrings {
	   public String addStrings(String num1, String num2) {
	        char[] c1 = num1.toCharArray();
	        char[] c2 = num2.toCharArray();

	        if(num1.length() < num2.length()){
	            c2 = c1;
	            c1 = num2.toCharArray();
	        }
	        
	        int carry = 0;
	        
	        StringBuilder sb = new StringBuilder();
	        
	        for(int i = 0; c2.length - i > 0; i++) {
	        	
	        	int idx1 = c1.length - i - 1;
	        	int idx2 = c2.length - i - 1;
	        	
	            int digit = (c1[idx1] - '0' + c2[idx2] - '0' + carry) % 10;
	            carry = (c1[idx1] - '0' + c2[idx2] - '0' + carry) / 10;
	            
	            sb.append(digit);
	        }
	        
	        for(int i = 0; c1.length - c2.length - i > 0; i++) {
	        
	        	int idx1 = c1.length - c2.length - i - 1;
	        	
	            int digit = (c1[idx1] - '0' + carry) % 10;
	            carry = (c1[idx1] - '0' + carry) / 10;
	            
	            sb.append(digit);
	        }
	        
	        if(carry == 1){
	            sb.append(carry);
	        }
	        
	        return sb.reverse().toString();
	    }
}
