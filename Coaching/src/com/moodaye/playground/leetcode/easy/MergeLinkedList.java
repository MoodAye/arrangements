package com.moodaye.playground.leetcode.easy;

//lc 21
public class MergeLinkedList {

	public ListNode mergeTwoLists(ListNode list1, ListNode list2) {
		if (list1 == null)
			return list2;
		if (list2 == null)
			return list1;

		ListNode p1 = list1;
		ListNode p2 = list2;
		ListNode prev1 = null;
		ListNode prev2 = null;

		ListNode head = null;

		if (list1.val <= list2.val) {
			head = list1;
		} else {
			head = list2;
		}

		while (true) {
			if (p1 == null || p2 == null)
				break;
			
			while (p2.val <= p1.val) {
				prev2 = p2;
				p2 = p2.next;
			}

			while (p1.val <= p2.val) {
				prev1 = p1;
				p1 = p1.next;
			}
			
			prev2.next = p1;
		}
		return head;
	}
}