package com.moodaye.playground.leetcode.easy;

// lc35 10min
public class SearchInsertPosition {
    public int searchInsert(int[] nums, int target) {
    	int left = 0;
    	int right = nums.length - 1;
    	
    	while(left <= right) {
    		int mid = left + (right - left) / 2;
    		if(target == nums[mid]) return mid;
    		if(target < nums[mid]) {
    			right = mid - 1;
    		}
    		else {
//    			if(left == mid) return left + 1;
    			left = mid + 1;
    		}
    	}
    	return left;
    }
}
/**
 * 1,3,5,6    5
 * 0,3  mid = 1   5 > 3
 * 1,3  mid = 2  return 2
 * 3,3  mid = 3  return 6 
 */

