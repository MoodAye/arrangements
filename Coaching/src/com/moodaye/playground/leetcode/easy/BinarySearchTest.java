package com.moodaye.playground.leetcode.easy;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class BinarySearchTest {

	@Test
	void test() {
		int a[] = {1,2};
		assertEquals(0, new BinarySearch().search(a, 1));
		assertEquals(1, new BinarySearch().search(a, 2));
		assertEquals(-1, new BinarySearch().search(a, 0));
		assertEquals(-1, new BinarySearch().search(a, 5));
	}

}
