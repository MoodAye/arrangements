package com.moodaye.playground.leetcode.easy;

import java.util.ArrayDeque;
import java.util.Deque;

// 857pm = 925pm
public class PopulateNextRightPointersInEachNode {
	// go tree order traversal?

	// add left to queue; add right to queue
	// empty queue into stack
	// empty stack-- adding to queue
	public Node connect(Node root) {
		if (root == null || root.left == null) {
			return root;
		}

		Deque<Node> que = new ArrayDeque<>();
		que.add(root.left);
		que.add(root.right);

		Deque<Node> stk = new ArrayDeque<>();

		while (!que.isEmpty()) {
			// move queue to stack
			while (!que.isEmpty()) {
				stk.push(que.remove());
			}

			Deque<Node> stk2 = new ArrayDeque<>();

			// pop stack - the first node points to null
			Node right = stk.pop();
			if (right.right != null) {
				stk2.push(right.right);
				stk2.push(right.left);
			}

			while (!stk.isEmpty()) {
				Node left = stk.pop();
				left.next = right;
				right = left;

				if (left.right != null) {
					stk2.push(left.right);
					stk2.push(left.left);
				}
			}
			
			while(!stk2.isEmpty()) {
				que.add(stk2.pop());
			}
		}

		return root;
	}
}

class Node {

	public int val;
	public Node left;
	public Node right;
	public Node next;

	public Node() {
	}

	public Node(int _val) {
		val = _val;
	}

	public Node(int _val, Node _left, Node _right, Node _next) {
		val = _val;
		left = _left;
		right = _right;
		next = _next;
	}
}
