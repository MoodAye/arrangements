package com.moodaye.playground.leetcode.easy;

// lc 278  
public class FirstBadVersion {
	public int firstBadVersion(int n) {
		int left = 1;
		int right = n;
		
		while(left < right) {
			int mid = left + (right - left) / 2;
			if(isBadVersion(mid)) {
				right = mid;
			}
			else {
				if(left == mid) return left + 1;
				left = mid;
			}
		}
		return left;
    }
	
	
	public boolean isBadVersion(int d) {
		return true;
	}
}
