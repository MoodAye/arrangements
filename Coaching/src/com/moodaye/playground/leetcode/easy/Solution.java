package com.moodaye.playground.leetcode.easy;

import java.util.ArrayDeque;
import java.util.Deque;

//423pm
public class Solution {
	public boolean isValid(String s) {
		char[] cs = s.toCharArray();
		Deque<Character> stack = new ArrayDeque<>();
		
		for (int i = 0; i < cs.length; i++) {
			if (cs[i] == '(' || cs[i] == '{' || cs[i] == '[') {
				stack.push(cs[i]);
			} else {
				if (stack.isEmpty()) {
					return false;
				}
				char next = stack.pop();
				if (!
						(
								(next == '(' && cs[i] == ')') || 
								(next == '{' && cs[i] == '}') || 
								(next == '[' && cs[i] == ']')
						)
					) {
					return false;
				}
			}
		}
		return stack.isEmpty();
	}
}
