package com.moodaye.playground.leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Dishes should be sorted.
 * 
 * Either a dish is included or not.
 * 
 * No reason not to include a dish with 0 or +ve ratings 
 * 
 * Only reason to include dish with -ve ratings is to 
 * increase the multiplier for the +ve dishes 
 * 
 * With each -ve dish added - the sum of the other dishes increases
 * by their sum
 * e.g., sum(zero -ve dishes) = 2,4+4,6+6+6.  Now add -ve dish = sum goes to 
 * sum(zero -ve dishes) * 2 - negative dish
 * 
 * Step 1: Added zero and +ve dishes.  This is baseCount.
 * Step 2: Sort -ve dishes - low to high
 * Step 2: Add -ve dishes one by one - low to high until sum stops increasing
 * 	- each time we add - we double the base count and add the -ve dish to the base count
 *
 */
public class ReducingDishes {
	
	public static void main(String[] args) {
		List<int[]> tests = new ArrayList<>();
		List<Integer> ans = new ArrayList<>();
		
		int[] test1 = {-1, -8, 0, 5, -9};
		tests.add(test1);
		ans.add(14);
		
		int[] test2 = {4, 3, 2};
		tests.add(test2);
		ans.add(20);
		
		int[] test3 = {-1, -4, -5};
		tests.add(test3);
		ans.add(0);
		
		int[] test4 = {-2, 5, -1, 0, 3, -3};
		tests.add(test4);
		ans.add(35);
		
		int[] test5 = {-1};
		tests.add(test5);
		ans.add(0);
		
		for(int i = 0; i < tests.size(); i++) {
			if(ans.get(i) != maxSatisfaction(tests.get(i))){
				System.out.printf("test %d failed with ans %d%n", i, maxSatisfaction(tests.get(i)));
			}
		}
		
	}
	
	public static int maxSatisfaction(int[] satisfaction) {
		
		int baseCount = Arrays.stream(satisfaction).filter((i) -> i >= 0).sum();
		
		Arrays.sort(satisfaction);
		
		
		int posStart = 0; // where +ve start
		for(int i = 0; i < satisfaction.length; i++) {
			if(satisfaction[i] >= 0) {
				posStart = i;
				break;
			}
		}
		
		// do the initial count
		int sum = 0;
		int cnt = 1;
		for(int i = posStart; i < satisfaction.length; i++) {
			sum += satisfaction[i] * cnt++;
		}
		
		// add in the -ve numbers if it makes sense
		for(int i = 0; i < posStart; i++) {
			baseCount = baseCount * 2 + satisfaction[i];
			if(baseCount > 0) {
				sum += baseCount;
			}
			else {
				break;
			}
		}
		
		
		return sum;
	}
}
