package com.moodaye.playground.leetcode.contest1304;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

// Problem 1306
// Accepted first try - time take = 34min
public class JumpGameIII {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int start = in.nextInt();
		int[] v = new int[n];
		for(int i = 0; i < n; i++) {
			v[i] = in.nextInt();
		}
		out.println(canReach(v, start));
	}
	
	boolean canReach(int[] arr, int start) {
		boolean[] hasBeenATarget = new boolean[arr.length];
		for(int i = 0; i < arr.length; i++) {
			if(arr[i] == 0) {
				if(canReach(arr, start, i, hasBeenATarget)) {
					return true;
				}
			}
		}
		return false;
	}
	
	boolean canReach(int[] arr, int start, int target, boolean[] hasBeenATarget) {
		if(hasBeenATarget[target]) {
			return false;
		}
		
		hasBeenATarget[target] = true;
		
		for(int i = 0; i < arr.length; i++) {
			if(i + arr[i] == target || i - arr[i] == target) {
				if(i == start) {
					return true;
				}
				if(canReach(arr, start, i, hasBeenATarget)) {
					return true;
				}
			}
		}
		return false;
	}

	public static void main(String[] args) {
		new JumpGameIII().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
