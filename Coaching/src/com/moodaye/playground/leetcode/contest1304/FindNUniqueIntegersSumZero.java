package com.moodaye.playground.leetcode.contest1304;

import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Locale;
import java.util.Scanner;
import java.util.stream.IntStream;
import java.util.stream.Stream;

// Problem 1304 LeetCode
// time = 19min
public class FindNUniqueIntegersSumZero {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int[] v = sumZero(n);
		out.println();
		for (int i : v) {
			out.print(i + " ");
		}
		out.println();
		sumZero2(n);
	}

	/* lambda to write out unique integers */
	public void sumZero2(int n) {
		System.out.println();
		IntStream.rangeClosed(0, n / 2).forEach( x -> {
			if(x == 0) {
				if(n % 2 == 1) {
					System.out.print(0 + " ");
				}
			}
			else {
				System.out.print(x + " " + -x + " ");
			}
		});
		System.out.println();
	}

	public int[] sumZero(int n) {
		int[] v = new int[n];
		int idx = 0;
		for (int i = -n / 2; i <= n / 2; i++) {
			if (i == 0 && n % 2 == 0) {
				continue;
			}
			v[idx++] = i;
		}
		return v;
	}

	public static void main(String[] args) {
		new FindNUniqueIntegersSumZero().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
