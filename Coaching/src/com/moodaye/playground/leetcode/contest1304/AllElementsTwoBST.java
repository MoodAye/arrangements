package com.moodaye.playground.leetcode.contest1304;

import java.io.PrintWriter;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Deque;
import java.util.List;
import java.util.Locale;
import java.util.Queue;
import java.util.Scanner;

// Problem 1305 Leetcode
// Got Accepted first try - but need to practice creating bsts and printing them etc.
// Time = 37 minutes
public class AllElementsTwoBST {
	void solve(Scanner in, PrintWriter out) {
		int n1 = in.nextInt();
		int n2 = in.nextInt();
		
		TreeNode root1 = makeTree(n1, in);
		TreeNode root2 = makeTree(n2, in);
		
		List<Integer> list = getAllElements(root1, root2);
		out.println(list.toString());
	}
	
	TreeNode makeTree(int n, Scanner in) {
		if(n == 0) {
			return null;
		}
		
		TreeNode root = new TreeNode(in.nextInt());
		n--;
		
		Queue<TreeNode> q = new ArrayDeque<>();
		q.add(root);
		
		while(n > 0) {
			TreeNode next = q.remove();
			next.left = new TreeNode(in.nextInt());
			n--;
			q.add(next.left);
			if(n == 0) {
				break;
			}
			next.right = new TreeNode(in.nextInt());
			n--;
			q.add(next.right);
		}
		return root;
	}

	public void getAllElements(TreeNode root, List<Integer> list) {
		if(root == null) {
			return;
		}
		list.add(root.val);
		getAllElements(root.left, list);
		getAllElements(root.right, list);
	}

	public List<Integer> getAllElements(TreeNode root1, TreeNode root2) {
		List<Integer> list = new ArrayList<>();
		getAllElements(root1, list);
		getAllElements(root2, list);
		Collections.sort(list);
		return list;
	}

	public static void main(String[] args) {
		new AllElementsTwoBST().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}

class TreeNode {
	int val;
	TreeNode left;
	TreeNode right;

	TreeNode(int x) {
		val = x;
	}
}
