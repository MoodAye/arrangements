package com.moodaye.playground.leetcode.June2020Challenge;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

// Problem LeetCode June 2020 Challenge
public class TwoCityScheduling {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int[][] nums = new int[2 * n][2];
		for (int i = 0; i < 2 * n; i++) {
			nums[i][0] = in.nextInt();
			nums[i][1] = in.nextInt();
		}
		out.println(twoCitySchedCost(nums));
	}

	public int twoCitySchedCost(int[][] costs) {
		return twoCitySchedCost(costs, 0, 0, 0, 0, costs.length / 2, Integer.MAX_VALUE);
	}

	public int twoCitySchedCost(int[][] cost, int idx, int costThusFar, int num1, int num2, int n, int minSum) {
		if (num1 == n && num2 == n) {
			minSum = Math.min(costThusFar, minSum);
			return costThusFar;
		}
		
		if(num1 > n || num2 > n) {
			return Integer.MAX_VALUE;
		}

		int newCost1 = costThusFar + cost[idx][1];
		if (newCost1 < minSum) {
			if (num1 != n) {
				minSum = twoCitySchedCost(cost, idx + 1, newCost1, num1 + 1, num2, n, minSum);
			}
		}

		int newCost2 = costThusFar + cost[idx][0]; if (newCost2 < minSum) {
			if (num2 != n) {
				minSum = twoCitySchedCost(cost, idx + 1, newCost2, num1, num2 + 1, n, minSum);
			}
		}

		return minSum;
	}

	public static void main(String[] args) {
		new TwoCityScheduling().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
