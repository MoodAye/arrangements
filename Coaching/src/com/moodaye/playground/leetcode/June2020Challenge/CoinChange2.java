package com.moodaye.playground.leetcode.June2020Challenge;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

// Problem LeetCode June 2020 Challenge
public class CoinChange2 {

	void solve(Scanner in, PrintWriter out) {
		int amount = in.nextInt();
		int n = in.nextInt();
		int[] coins = new int[n];
		for (int i = 0; i < coins.length; i++) {
			coins[i] = in.nextInt();
		}

		out.println(change(amount, coins));
		out.println(recursiveChange(amount, coins, 0));
	}

	public int change(int amount, int[] coins) {
		int[] cnt = new int[amount + 1];
		cnt[0] = 1;
		for (int coin : coins) {
			for (int j = coin; j <= amount; j++) {
				cnt[j] += cnt[j - coin];
			}
		}
		return cnt[amount];
	}

	public int recursiveChange(int amount, int[] coins, int i) {
		if (amount < 0) {
			return 0;
		}
			
		if (amount == 0) {
			return 1;
		}
		
		if(i == coins.length && amount > 0) {
			return 0;
		}
		
		
		return recursiveChange(amount - coins[i], coins, i) + recursiveChange(amount, coins, i + 1);
	}

	public static void main(String[] args) {
		new CoinChange2().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
