package com.moodaye.playground.leetcode.June2020Challenge;

import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.Locale;
import java.util.Queue;
import java.util.Scanner;

// Problem LeetCode June challenge
public class InvertBinaryTree {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		String[] nodes = new String[n];
		for (int i = 0; i < nodes.length; i++) {
			nodes[i] = in.next();
		}
		out.println(invertTree(buildTree(nodes)));
	}
	
	public TreeNode invertTree(TreeNode root) {
		if (root == null) {
			return root;
		}
		
		Queue<TreeNode> queue = new LinkedList<>();
		queue.add(root);
		while(queue.size() > 0) {
			TreeNode next =  queue.remove();
			if(next == null) {
				continue;
			}
			TreeNode left = next.left;
			next.left = next.right;
			next.right = left;
			queue.add(next.left);
			queue.add(next.right);
		}
		
		return root;
	}

	public static void main(String[] args) {
		new InvertBinaryTree().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}

	static class TreeNode {
		int val;
		TreeNode left;
		TreeNode right;

		TreeNode() {
		}

		TreeNode(int val) {
			this.val = val;
		}

		TreeNode(int val, TreeNode left, TreeNode right) {
			this.val = val;
			this.left = left;
			this.right = right;
		}

	}

	private static TreeNode buildTree(String[] nodes) {
		TreeNode root = new TreeNode(Integer.valueOf(nodes[0]));
		Queue<TreeNode> queue = new LinkedList<>();
		queue.add(root);
		int idx = 1;
		while (queue.size() != 0) {
			TreeNode next = queue.remove();
			if (idx < nodes.length) {
				if (!nodes[idx].equals("null")) {
					TreeNode left = new TreeNode(Integer.valueOf(nodes[idx]));
					queue.add(left);
					next.left = left;
				}
				idx++;
			}
			if (idx < nodes.length) {
				if (!nodes[idx].equals("null")) {
					TreeNode right = new TreeNode(Integer.valueOf(nodes[idx]));
					queue.add(right);
					next.right = right;
				}
				idx++;
			}
		}
		return root;
	}
}
