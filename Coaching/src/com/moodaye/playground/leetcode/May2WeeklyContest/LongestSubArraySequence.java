package com.moodaye.playground.leetcode.May2WeeklyContest;

import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Locale;
import java.util.Scanner;

// Problem 
// TLE  ... consider array with all elements = 1 and limit = 10.  
// size of array is 10^5
public class LongestSubArraySequence {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int limit = in.nextInt();
		int[] nums = new int[n];
		for (int i = 0; i < nums.length; i++) {
			nums[i] = in.nextInt();
		}
		nums = new int[100_000];
		Arrays.fill(nums, 1);
		limit = 10;
		out.println(longestSubarray(nums, limit));
	}

	/*
	 * 4 4 8 2 4 7 2
	 * 
	 * 6 5 10 1 2 4 7 2 4
	 * 
	 * WA 3 9 2 4 2 3
	 */

	/*
	 * Approach: DP for sub-sequence length from 2 - length of array track the min
	 * and max for the length of the sub-array ending with the current element
	 */
	public int longestSubarray(int[] nums, int limit) {
		int[] min = nums.clone();
		int[] max = nums.clone();
		int[] pmin = nums.clone();
		int[] pmax = nums.clone();
		int maxLen = 1;
		for (int len = 2; len < nums.length; len++) {
			for (int i = len - 1; i < nums.length; i++) {
				min[i] = Math.min(min[i], pmin[i - 1]);
				max[i] = Math.max(max[i], pmax[i - 1]);
				if (max[i] - min[i] <= limit) {
					maxLen = len;
				}
			}
			if (maxLen != len) {
				break;
			}
			pmin = min.clone();
			pmax = max.clone();
		}
		return maxLen;
	}

	public int longestSubarray2(int[] nums, int limit) {
		int min = nums[0];
		int max = nums[0];
		int pmin = nums[0];
		int pmax = nums[0];
		int len = 1;
		for (int i = 1; i < nums.length; len++) {
			min = Math.min(min, nums[i]);
			max = Math.min(min, nums[i]);
		}
		return len;
	}

	public static void main(String[] args) {
		new LongestSubArraySequence().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
