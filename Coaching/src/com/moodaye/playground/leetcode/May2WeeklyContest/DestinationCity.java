package com.moodaye.playground.leetcode.May2WeeklyContest;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;
import java.util.Set;

// Problem 
public class DestinationCity {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		List<List<String>> paths = new LinkedList<>();
		for(int i = 0; i < n; i++) {
			List<String> list = new ArrayList<>(2);
			list.add(in.next());
			list.add(in.next());
			paths.add(list);
		}
		out.println(destCity(paths));
	}

	public String destCity(List<List<String>> paths) {
		Set<String> origin = new HashSet<>();
		Set<String> dest = new HashSet<>();
		for (List<String> path : paths) {
			origin.add(path.get(0));
			dest.add(path.get(1));
		}
		String ans = null;
		for (String d : dest) {
			if (!origin.contains(d)) {
				ans = d;
				break;
			}
		}
		return ans;
	}

	public static void main(String[] args) {
		new DestinationCity().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
