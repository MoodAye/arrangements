package com.moodaye.playground.leetcode.May2WeeklyContest;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

// Problem 
public class KLengthApart {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int k = in.nextInt();
		int[] nums = new int[n];
		for (int i = 0; i < nums.length; i++) {
			nums[i] = in.nextInt();
		}
		out.println(kLengthApart(nums, k));
	}

	/*
6 2 
1 0 0 1 0 1
false
		
8 2
1 0 0 0 1 0 0 1
true
	
5 0
1 1 1 1 1 
true

4 1
0 1 0 1
true



		
	 */
	public boolean kLengthApart(int[] nums, int k) {
		int prev1 = -1;
		for (int i = 0; i < nums.length; i++) {
			if (nums[i] == 1) {
				if (prev1 != -1 && i - prev1 - 1 < k) {
						return false;
				}
				prev1 = i;
			}
		}
		return true;
	}

	public static void main(String[] args) {
		new KLengthApart().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
