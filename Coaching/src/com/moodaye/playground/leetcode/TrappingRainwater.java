package com.moodaye.playground.leetcode;

import java.util.Arrays;
import java.util.Random;

/** Leetcode Problem 42 - Hard */
public class TrappingRainwater {
	
	public static void main(String[] args) {
		for(int i = 0; i < 1000000000; i++) {
			int[] heights = new Random().ints(10,0,5).toArray();
			if(trapBf(heights) != trapOptimized(heights)) {
				printProblem(heights);
			}
		}
	}

	/**
	 * Water at any point is equal to the min of the max 
	 * walls to either side of the point minus the height of the 
	 * wall at that point.  If the difference is negative - then 
	 * the water at that point is zero.
	 *
	 * To determine the water at a point - we scan left and then we scan right 
	 * to determine the max
	 * 
	 * @param height
	 * @return
	 */
	public static int trapBf(int[] height) {
		int water = 0;
	
		// no water collects at edges - therefore start and end values of i
		for(int i = 1; i < height.length - 1; i++) {
			
			int pLeft = i - 1;
			int maxLeft = 0;
			while(pLeft >= 0) {
				maxLeft = Math.max(maxLeft, height[pLeft--]);
			}
			
			int pRight = i + 1;
			int maxRight = 0;
			while(pRight < height.length) {
				maxRight = Math.max(maxRight, height[pRight++]);
			}
			
			int max = Math.min(maxLeft, maxRight);
			if(max > height[i]) {
				water += (max - height[i]);
			}
			
		}
		return water;
	}

	
	/**
	 * The water height at any point can be determined using a
	 * 2 pointer method - which will not require nested loops.
	 * 
	 * We need to determine the min of the left max and right max walls 
	 * that a point is between.
	 * 
	 * We use a left pointer to track the max to the left and right pointer
	 * to track the max to the right. 
	 *
	 * Left pointer starts at the left and right at the right most point. 
	 * They track maxLeft and maxRight, respectively.
	 *
	 * The left pointer will always know the max height to its left.
	 * The right pointer will always know the max height to its right. 
	 * 
	 * At any point - determine which pointer has a lower max height.
	 * Determine the water height at this pointer's location.  We can do this because
	 * we know the min height of max left and right walls to this point.
	 * 
	 * @param height
	 * @return
	 */
	public static int trapOptimized(int[] height) {
		
		int water = 0;
		
		int pLeft = 0;
		int maxLeft = height[pLeft];
		int pRight = height.length - 1;
		int maxRight = height[pRight];
		
		while(pLeft < pRight) {
			
			if(maxLeft <= maxRight) {
				pLeft++;
				if(maxLeft > height[pLeft]) {
					water += maxLeft - height[pLeft];
				}
				else {
					maxLeft = height[pLeft];
				}
			}
			else {
				pRight--;
				if(maxRight > height[pRight]) {
					water += maxRight - height[pRight];
				}
				else {
					maxRight = height[pRight];
				}
			}
		}
		
		return water;
	}
	
	private static void printProblem(int[] heights) {
		heights = heights.clone();
		int max = Arrays.stream(heights).max().getAsInt();
		while(max >= 0) {
			for(int i = 0; i < heights.length; i++) {
				if(heights[i] == max) {
					heights[i]--;
				}
				else {
				}
			}
			max--;
		}
	}
	
	private int[] randomProblem() {
		Random rand = new Random();
		return rand.ints().limit(10).toArray();
	}
	

}
