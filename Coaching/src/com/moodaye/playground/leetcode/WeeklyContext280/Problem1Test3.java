package com.moodaye.playground.leetcode.WeeklyContext280;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class Problem1Test3 {

	@Test
	void test() {
		Problem3 p3 = new Problem3();
		int[] t1 = { 4, 1, 6, 5 };
		assertEquals(4, p3.minimumRemoval(t1));

		int[] t2 = { 2, 10, 3, 2 };
		assertEquals(7, p3.minimumRemoval(t2));

		int[] t3 = { 2 };
		assertEquals(0, p3.minimumRemoval(t3));

		int[] t4 = { 2, 100 };
		assertEquals(2, p3.minimumRemoval(t4));

		int[] t5 = { 0, 0, 0, 0, 0, 10 };
		assertEquals(0, p3.minimumRemoval(t5));
	}

}
