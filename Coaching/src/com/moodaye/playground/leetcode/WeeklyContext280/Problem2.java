package com.moodaye.playground.leetcode.WeeklyContext280;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

public class Problem2 {

	public int minimumOperations(int[] nums) {
		Map<Integer, Integer> opsByIdx = new TreeMap<>();
		for (int i = 0; i < nums.length; i += 2) {
			int cnt = 0;
			for (int j = 0; j < nums.length; j += 2) {
				if (nums[i] == nums[j])
					continue;
				else
					cnt++;
			}
			opsByIdx.put(i, cnt);
		}

		for (int i = 1; i < nums.length; i += 2) {
			int cnt = 0;
			for (int j = 1; j < nums.length; j += 2) {
				if (nums[i] == nums[j])
					continue;
				else
					cnt++;
			}
			opsByIdx.put(i, cnt);
		}

		int minEvenIdx = -1;
		int minEvenCnt = 0;
		int minOddIdx = -1;
		int minOddCnt = 0;
		return 0;
	}

}
