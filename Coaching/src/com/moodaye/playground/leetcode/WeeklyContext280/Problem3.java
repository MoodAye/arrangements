package com.moodaye.playground.leetcode.WeeklyContext280;

import java.util.Arrays;

public class Problem3 {
	public long minimumRemoval(int[] b) {
		
		  long[] beans = new long[b.length]; for(int i = 0; i < b.length; i++) {
		  beans[i] = 1L * b[i]; }
		 
		
		int len = beans.length;
		Arrays.sort(beans);
		long sum = Arrays.stream(beans).sum();
		long penalty = 0L;
		long minBeans = (1L) * (sum - (beans[0] * len));
		
		for(int i = 1; i < beans.length; i++) {
			penalty += beans[i - 1];
			sum -=  beans[i - 1];
			long currMin = penalty + sum - (1L * beans[i] * (len - i));
			minBeans = Math.min(minBeans, currMin);
		}
		
		return minBeans;
	}
	
}
