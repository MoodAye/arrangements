package com.moodaye.playground.leetcode.May2020Challenge;

import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Scanner;

// Problem 
public class MajorityElement {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int[] nums = new int[n];
		for (int i = 0; i < nums.length; i++) {
			nums[i] = in.nextInt();
		}
		out.println(majorityElement(nums));
	}
	
	public int majorityElement(int[] nums) {
		Map<Integer, Integer> cnts = new HashMap<>();
		int maj = 0;
		for(int i : nums) {
			if(cnts.merge(i, 1, Integer::sum) >= (nums.length + 1)/ 2) {
				maj = i;
				break;
			}
		}
		return maj;
	}

	public static void main(String[] args) {
		new MajorityElement().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
