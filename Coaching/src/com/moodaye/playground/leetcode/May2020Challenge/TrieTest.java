package com.moodaye.playground.leetcode.May2020Challenge;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class TrieTest {
	static Trie trie;
	static Trie basic;
	
	@Before
	public void dothisbefore() {
		trie = new Trie();
		trie.insert("rajiv");
		trie.insert("kavita");
		trie.insert("tasha");
		trie.insert("ronnie");
		
		basic = new Trie();
		basic.insert("r");
		basic.insert("k");
		basic.insert("t");
		basic.insert("r");
	}
	
	@Test
	public void test() {
		assertTrue(basic.search("r"));
		assertTrue(trie.search("rajiv"));
	}
	
	@Test
	public void test2() {
		Trie trie2 = new Trie();
		trie2.insert("apple");
		assertTrue(trie2.search("apple"));
		assertTrue(!trie2.search("app"));
		assertTrue(trie2.startsWith("app"));
	}

}
