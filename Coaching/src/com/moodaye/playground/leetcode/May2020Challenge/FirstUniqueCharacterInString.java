package com.moodaye.playground.leetcode.May2020Challenge;

import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Scanner;

// Problem LeetCode May 2020 Challenge
public class FirstUniqueCharacterInString {
	void solve(Scanner in, PrintWriter out) {
		String s = in.next();
		out.println(firstUniqueChar(s));
	}
	
	public int firstUniqueChar(String s) {
		Map<Character, Integer> cnts = new HashMap<>();
		
		for(char c : s.toCharArray()) {
			cnts.merge(c, 1, Integer::sum);
		}
		
		for(int i = 0; i < s.length(); i++) {
			if(cnts.get(s.charAt(i)) == 1){
				return i;
			}
		}
		
		return -1;
	}

	public static void main(String[] args) {
		new FirstUniqueCharacterInString().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
