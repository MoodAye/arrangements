package com.moodaye.playground.leetcode.May2020Challenge;

import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Scanner;
import java.util.TreeMap;

// Problem LeetCode May 2020
public class KClosestPointsToOrigin {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int k = in.nextInt();
		int[][] nums = new int[n][2];
		for (int i = 0; i < nums.length; i++) {
			nums[i][0] = in.nextInt();
			nums[i][1] = in.nextInt();
		}

		Arrays.stream(kClosest(nums, k)).forEach(x -> out.printf("[%d,%d] ", x[0], x[1]));
	}
	
	static class Point implements Comparable<Point>{
		int x;
		int y;
		int d;
		Point(int x, int y){
			this.x = x;
			this.y = y;
			d = x * x + y * y;
		}
		@Override
		public int compareTo(Point o) {
			return d - o.d;
		}
	}

	public int[][] kClosest(int[][] points, int k) {
		PriorityQueue<Point> pq = new PriorityQueue<>();
		
		Point[] ps = new Point[points.length];

		for (int i = 0; i < points.length; i++) {
			Point p = new Point(points[i][0], points[i][1]);
			pq.add(p);
			ps[i] = p;
		}
		
		Arrays.sort(ps);
		
		int[][] kcpq = new int[k][2];
		int[][] kc = new int[k][2];

		for (int i = 0; i < k; i++) {
			Point p = pq.poll();
			kcpq[i][0] = p.x;
			kcpq[i][1] = p.y;
			kc[i][0] = p.x;
			kc[i][1] = p.y;
		}

//		return kcpq;
		return kc;
	}

	public static void main(String[] args) {
		new KClosestPointsToOrigin().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
