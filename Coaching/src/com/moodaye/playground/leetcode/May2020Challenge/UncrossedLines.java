package com.moodaye.playground.leetcode.May2020Challenge;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

// Problem LeetCode May2020 Challenge
public class UncrossedLines {
	void solve(Scanner in, PrintWriter out) {
		int n1 = in.nextInt();
		int n2 = in.nextInt();
		int[] nums1 = new int[n1];
		int[] nums2 = new int[n2];
		for (int i = 0; i < n1; i++) {
			nums1[i] = in.nextInt();
		}
		for (int i = 0; i < n2; i++) {
			nums2[i] = in.nextInt();
		}
		out.println(maxUncrossedLines(nums1, nums2));
	}
	
	/*
	6 5
	1 3 7 1 7 5
	1 9 2 5 1 
	2

	2 5
	3 3
	1 3 1 2 1	
	1
	
	 * 
	 */
	public int maxUncrossedLines(int[] a, int[] b) {
		int[][] max = new int[a.length][b.length];
		
		for (int i = 0; i < a.length; i++) {
			for (int j = 0; j < b.length; j++) {
				max[i][j] = a[i] == b[j] ? 1 : 0;
				if(i == 0 && j == 0) {
					continue;
				}
				else if(i == 0) {
					max[i][j] = Math.max(max[i][j], max[i][j - 1]);
				}
				else if (j == 0) {
//					max[i][j] = max[i][j] + max[i - 1][j];
					max[i][j] = Math.max(max[i][j], max[i - 1][j]);
				}
				else {
					max[i][j] = Math.max(max[i][j] + max[i - 1][j - 1], Math.max(max[i][j - 1], max[i - 1][j]));
				}
			}
		}
		return max[a.length - 1][b.length - 1];
	}

	public static void main(String[] args) {
		new UncrossedLines().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
