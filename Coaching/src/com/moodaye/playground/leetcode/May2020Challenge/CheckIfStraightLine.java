package com.moodaye.playground.leetcode.May2020Challenge;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

enum P {
	X, Y
}

// Problem LeetCode May 2020 Challenge
/*
 * Test: 5 -3 -2 -1 -2 2 -2 -2 -2 0 -2 True
 */
public class CheckIfStraightLine {

	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int[][] nums = new int[n][2];
		for (int i = 0; i < nums.length; i++) {
			nums[i][0] = in.nextInt();
			nums[i][1] = in.nextInt();
		}
		out.println(checkStraightLine(nums));
	}

	public boolean checkStraightLine(int[][] coordinates) {
		if (coordinates.length <= 2) {
			return true;
		}

		int dx = coordinates[0][0] - coordinates[1][0];
		int dy = coordinates[0][1] - coordinates[1][1];

		for (int i = 2; i < coordinates.length; i++) {
			int dxi = coordinates[0][0] - coordinates[i][0];
			int dyi = coordinates[0][1] - coordinates[i][1];
			if(dx * dyi != dxi * dy) {
				return false;
			}
		}
		return true;
	}

	public static void main(String[] args) {
		new CheckIfStraightLine().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
