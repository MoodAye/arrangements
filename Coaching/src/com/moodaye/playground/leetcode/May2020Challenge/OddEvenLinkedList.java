package com.moodaye.playground.leetcode.May2020Challenge;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

// Problem LeetCode May 2020 Challenge
public class OddEvenLinkedList {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int[] nums = new int[n];
		for (int i = 0; i < nums.length; i++) {
			nums[i] = in.nextInt();
		}
		
		ListNode head = new ListNode(nums[0]);
		ListNode curr = head;
		for(int i = 1; i < n; i++){
			ListNode next = new ListNode(nums[i]);
			curr.next = next;
			curr = next;
		}
		
		printList(head, out);
		out.println();
		printList(oddEvenList(head), out);
	}
	
	public void printList(ListNode head, PrintWriter out) {
		ListNode next = head;
		while(next != null) {
			out.print(next.val + " ");
			next = next.next;
		}
	}
	
	// (1) curr -> (2) next -> (3) next.next
	// (1) curr ->  (3) next.next -> (2) next
	// (1) -> (3) -> curr 
	public ListNode oddEvenList(ListNode head) {
		if(head == null) {
			return head;
		}
		ListNode lastOdd = head;
		ListNode firstEven = head.next;
		ListNode lastEven = head.next;
		while(lastEven != null && lastEven.next != null) {
			lastOdd.next = lastEven.next;
			lastEven.next = lastOdd.next.next;
			lastOdd = lastOdd.next;
			lastOdd.next = firstEven;
			lastEven = lastEven.next;
		}
		return head;
	}

	public static void main(String[] args) {
		new OddEvenLinkedList().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}

	static class ListNode {
		int val;
		ListNode next;

		ListNode() {
		}

		ListNode(int val) {
			this.val = val;
		}

		ListNode(int val, ListNode next) {
			this.val = val;
			this.next = next;
		}
	}
}
