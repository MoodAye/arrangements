package com.moodaye.playground.leetcode.May2020Challenge;

import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Locale;
import java.util.Scanner;

/* Problem LeetCode May2020 Challenge
   6
  -1 20 20 20 -70 10
 */
public class MaxSumCircularSubarray {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int[] nums = new int[n];
		for (int i = 0; i < nums.length; i++) {
			nums[i] = in.nextInt();
		}
		out.println(maxSubarraySumCircular2(nums));
	}

	public static int maxSubarraySumCircular2(int[] a) {
		int sum = a[0];
        int maxSum = a[0];
        int sumNeg = -a[0];
        int maxSumNeg = -a[0];
        int arrSum = a[0];
        for (int i = 1; i < a.length; i++) {
        	arrSum += a[i];
            sum += a[i];
            sum = Math.max(sum, a[i]);
            maxSum = Math.max(sum, maxSum);
            sumNeg -= a[i];
            sumNeg = Math.max(sumNeg, -a[i]);
            maxSumNeg = Math.max(sumNeg, maxSumNeg);
        }
        if(maxSum < 0) return maxSum;
		return Integer.max(maxSum, arrSum + maxSumNeg);
	}

	public static void main(String[] args) {
		new MaxSumCircularSubarray().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
