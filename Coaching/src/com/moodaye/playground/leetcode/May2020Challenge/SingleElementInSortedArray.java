package com.moodaye.playground.leetcode.May2020Challenge;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

// Problem LeetCode May2020 Challenge
public class SingleElementInSortedArray {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int[] nums = new int[n];
		for (int i = 0; i < nums.length; i++) {
			nums[i] = in.nextInt();
		}
		out.println(singleNonDuplicate(nums));
	}

	public int singleNonDuplicate(int[] nums) {
		int left = 0;
		int right = nums.length - 1;
		
		while(left != right) {
			int mid = (left + right) / 2;
			if(nums[mid] == nums[mid + 1]) {
				if((right + mid + 2) % 2 == 0) {
					left = mid + 2;
				}
				else {
					right = mid - 1;
				}
			}
			else if(nums[mid] == nums[mid - 1]){
				if((left + mid - 2) % 2 == 0) {
					right = mid - 2;
				}
				else {
					left = mid + 1;
				}
			}
			else {
				return nums[mid];
			}
		}
		return nums[left];
	}
	

	public static void main(String[] args) {
		new SingleElementInSortedArray().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
