package com.moodaye.playground.leetcode.May2020Challenge;

import java.io.PrintWriter;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Scanner;

// Problem LeetCode May 2020 Challenge
public class ContiguousArray {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int[] nums = new int[n];
		for (int i = 0; i < nums.length; i++) {
			nums[i] = in.nextInt();
		}

		out.println(findMaxLength(nums));
	}

	/*
	 * 0 1 1 0 1 1 1 0 0 1 1 2 2 2 2 2
	 * 
	 */

	/*
	 * If you match the prior element - then len = 1 + len(i - 2) Else - you add 1
	 * to prior element len if you have a match with (i - 2 * len - 1). Else equal
	 * to prior element if equals to element at 1 - 2*len Else 0.
	 */
	public int findMaxLength(int[] nums) {
		if(nums.length == 0) {
			return 0;
		}
		
		int max = 0;
		Map<Integer, Integer> mapSumIdx = new HashMap<>();
		if(nums[0] == 0) {
			nums[0] = -1;
		}
		mapSumIdx.put(nums[0], 0);
		for (int i = 1; i < nums.length; i++) {
			if (nums[i] == 0) {
				nums[i] = nums[i - 1] - 1;
			} else {
				nums[i] = nums[i - 1] + 1;
			}
			if (nums[i] == 0) {
				max = i + 1;
			} else if (mapSumIdx.containsKey(nums[i])) {
				max = Math.max(max, i - mapSumIdx.get(nums[i]));

			} else {
				mapSumIdx.put(nums[i], i);
			}
		}
		return max;
	}

	public static void main(String[] args) {
		new ContiguousArray().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
