package com.moodaye.playground.leetcode.May2020Challenge;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

// Problem LeetCode May Challenge
/*
 * TLE for 2126753390 1702766719
 */
public class FirstBadVersion {
	int firstBadVerion;
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		firstBadVerion = in.nextInt();
		out.println(firstBadVersion(n));
	}

	/*
	 * You have to find first instance of a bad version just ahead of a good version
	 * So we use binary search - if we hit a good version - then we check the next
	 * element. If that is also a good version - then we move right. If we hit a bad
	 * version - we check the prior version. Move left.
	 */
	public int firstBadVersion(int n) {
		if (n == 1) {
			return 1;
		}
		
		int left = 0;
		int right = n - 1;
		int mid = 0;
		while (left < right) {
			mid = left / 2 + right / 2;
			if (isBadVersion(mid)) {
				if (!isBadVersion(mid - 1)) {
					return mid;
				}
				right = mid - 1;
			} else {
				if (isBadVersion(mid + 1)) {
					return mid + 1;
				}
				left = mid + 1;
			}
		}
		return left;
	}

	public boolean isBadVersion(int n) {
		return n >= firstBadVerion;
	}

	public static void main(String[] args) {
		new FirstBadVersion().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
