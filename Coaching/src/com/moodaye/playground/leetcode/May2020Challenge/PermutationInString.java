package com.moodaye.playground.leetcode.May2020Challenge;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

// Problem LeetCode May 2020 Challenge
public class PermutationInString {
	void solve(Scanner in, PrintWriter out) {
		String s1 = in.next();
		String s2 = in.next();
		out.println(checkInclusion(s1, s2));
	}
	
	public boolean checkInclusion(String s1, String s2) {
		char[] cs1 = s1.toCharArray();
		char[] cs2 = s2.toCharArray();
		int len = cs1.length;
		if(len > cs2.length) {
			return false;
		}
		
		int[] cs1Cnts = cnts(cs1, 0, len);
		
		for(int i = 0; i <= cs2.length - len; i++) {
			int[] cs2Cnts = cnts(cs2, i, len);
			boolean permFound = true;
			for(int j = 0; j < 26; j++) {
				if(cs1Cnts[j] != cs2Cnts[j]) {
					permFound = false;
					break;
				}
			}
			if(permFound) {
				return true;
			}
		}
		
		return false;
	}
	
	private int[] cnts(char[] ca, int start, int len) {
		int[] cnt = new int[26];
		
		for(int i = start; i < start + len; i++) {
			cnt[ca[i] - 'a']++;
		}
		return cnt;
	}
	
	public static void main(String[] args) {
		new PermutationInString().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
