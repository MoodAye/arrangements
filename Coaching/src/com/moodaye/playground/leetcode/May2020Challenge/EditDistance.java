package com.moodaye.playground.leetcode.May2020Challenge;

import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Locale;
import java.util.Scanner;

// Problem LeetCode May2020 challenge
/* TLE at dinitrophenylhydrazine benzalphenylhydrazone
 * 
*/
public class EditDistance {
	void solve(Scanner in, PrintWriter out) {
		String w1 = in.next();
		String w2 = in.next();
		out.println( minDistance(w1.toCharArray(), w1.length(), w2.toCharArray(), w2.length()));
	}

	private int minDistance(char[] word1, int idx1, char[] word2, int idx2) {
		int[][] cache = new int[idx1 + 1][idx2 + 1];

		for (int i = 0; i <= idx1; i++) {
			for (int j = 0; j <= idx2; j++) {
				if(i == 0) {
					cache[i][j] = j;
				}
				else if(j == 0) {
					cache[i][j] = i;
				}
				else if (word1[i - 1] == word2[j - 1]) {
					cache[i][j] = 	cache[i - 1][j - 1];
				}
				else {
					cache[i][j] = 1 + Math.min(cache[i - 1][j - 1], 
							Math.min(cache[i][j - 1], cache[i - 1][j]));
				}
			}
		}
		return cache[idx1][idx2];

	}

	// solution with TLE
	private int minDistance2(char[] word1, int idx1, char[] word2, int idx2) {
		if (idx1 == 0) {
			return idx2;
		}
		if (idx2 == 0) {
			return idx1;
		}

		if (word1[idx1 - 1] == word2[idx2 - 1]) {
			return minDistance(word1, idx1 - 1, word2, idx2 - 1);
		}
		int cnt1 = minDistance(word1, idx1, word2, idx2 - 1);
		int cnt2 = minDistance(word1, idx1 - 1, word2, idx2);
		int cnt3 = minDistance(word1, idx1 - 1, word2, idx2 - 1);

		return 1 + Math.min(cnt3, Math.min(cnt1, cnt2));

	}

	public static void main(String[] args) {
		new EditDistance().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
