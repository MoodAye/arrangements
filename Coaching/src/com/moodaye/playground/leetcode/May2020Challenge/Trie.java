package com.moodaye.playground.leetcode.May2020Challenge;

import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Locale;
import java.util.Map;
import java.util.Scanner;

// Problem LeetCode May 2020 Challenge
public class Trie {
	private final char END = '-';
	
	Node root;

	/** Initialize your data structure here. */
	public Trie() {
		root = new Node(END);
	}

	/** Inserts a word into the trie. */
	public void insert(String word) {
		if(search(word)) {
			return;
		}
		
		Node next = root;
		for(char c : word.toCharArray()) {
			if(!next.children.containsKey(c)) {
				Node node = new Node(c);
				next.children.put(c, node);
			}
				next = next.children.get(c);
		}
		// good way to end trie?
		next.children.put(END, new Node(END));
	}

	/** Returns if the word is in the trie. */
	public boolean search(String word) {
		Node next = root;
		for(char c : word.toCharArray()) {
			if(!next.children.containsKey(c)) {
				return false;
			}
			next = next.children.get(c);
		}
		return next.children.containsKey(END);
	}

	/**
	 * Returns if there is any word in the trie that starts with the given prefix.
	 */
	public boolean startsWith(String prefix) {
		Node next = root;
		for(char c : prefix.toCharArray()) {
			if(!next.children.containsKey(c)) {
				return false;
			}
			next = next.children.get(c);
		}
		return true;
	}
	
	class Node{
		char c;
		Map<Character, Node> children;
		Node(char c){
			this.c = c;
			children = new HashMap<>();
		}
	}
} 
class Node{
	char c;
	Map<Character, Node> children;
	Node(char c){
		this.c = c;
		children = new HashMap<>();
	}
}
