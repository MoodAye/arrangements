package com.moodaye.playground.leetcode.May2020Challenge;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

// Problem LeetCode May 2020
public class CountSquareSubmatricesWithAllOnes {
	void solve(Scanner in, PrintWriter out) {
		int m = in.nextInt();
		int n = in.nextInt();
		int[][] matrix = new int[m][n];
		for (int r = 0; r < m; r++) {
			for (int c = 0; c < n; c++) {
				matrix[r][c] = in.nextInt();
			}
		}
		out.println(countSquares(matrix));
	}

	public int countSquares(int[][] matrix) {
		int m = matrix.length;
		int n = matrix[0].length;
		int cnt = 0;
		for (int row = 0; row < m; row++) {
			for (int col = 0; col < n; col++) {
				matrix[row][col] = cnt(matrix, row, col);
				cnt += matrix[row][col];
			}
		}
		return cnt;
	}

	private int cnt(int[][] matrix, int row, int col) {
		if (row == 0 || col == 0) {
			return matrix[row][col];
		}

		if (matrix[row][col] == 0) {
			return 0;
		}

		int cnt = Math.min(matrix[row][col - 1],
				  Math.min(matrix[row - 1][col - 1], matrix[row - 1][col]));

		return cnt + 1;
	}

	public static void main(String[] args) {
		new CountSquareSubmatricesWithAllOnes().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
