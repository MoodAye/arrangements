package com.moodaye.playground.leetcode.May2020Challenge;

import java.io.PrintWriter;
import java.util.Deque;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Locale;
import java.util.Queue;
import java.util.Scanner;
import java.util.Set;

// Problem LeetCode May 2020 
public class KthSmallestElementInBst {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int k = in.nextInt();
		String[] nums = new String[n];
		for (int i = 0; i < nums.length; i++) {
			nums[i] = in.next();
		}

		TreeNode root = new TreeNode(Integer.valueOf(nums[0]));
		Queue<TreeNode> queue = new LinkedList<>();
		queue.add(root);
		int idx = 1;
		while (queue.size() != 0) {
			TreeNode next = queue.remove();
			if (idx < nums.length) {
				if (!nums[idx].equals("null")) {
					TreeNode left = new TreeNode(Integer.valueOf(nums[idx]));
					queue.add(left);
					next.left = left;
				}
				idx++;
			}
			if (idx < nums.length) {
				if (!nums[idx].equals("null")) {
					TreeNode right = new TreeNode(Integer.valueOf(nums[idx]));
					queue.add(right);
					next.right = right;
				}
				idx++;
			}
		}
		out.println(kthSmallest(root, k));
	}

	static class TreeNode {
		int val;
		TreeNode left;
		TreeNode right;

		TreeNode() {
		}

		TreeNode(int val) {
			this.val = val;
		}

		TreeNode(int val, TreeNode left, TreeNode right) {
			this.val = val;
			this.left = left;
			this.right = right;
		}
	}
	
	private int kth;
	private boolean found;
	private int k;

	private void visit(TreeNode node) {
		if(found) {
			return;
		}
		k--;
		if(k == 0) {
			found = true;
			kth = node.val;
		}
	}
		

	public int kthSmallest(TreeNode root, int k) {
		this.k = k;
		inOrderTraversal(root);
		return kth;
	}
	
	private void inOrderTraversal(TreeNode node) {
		if(found) {
			return;
		}
		if(node != null) {
			inOrderTraversal(node.left);
			visit(node);
			inOrderTraversal(node.right);
		}
	}

	public static void main(String[] args) {
		new KthSmallestElementInBst().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
