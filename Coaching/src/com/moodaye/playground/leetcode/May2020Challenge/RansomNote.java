package com.moodaye.playground.leetcode.May2020Challenge;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

// Problem 
public class RansomNote {
	void solve(Scanner in, PrintWriter out) {
		String ransomNote = in.next();
		String magazine = in.next();
		out.println(canConstruct(ransomNote, magazine));
		out.println(canConstruct("", "xx"));
	}

	public boolean canConstruct(String ransomNote, String magazine) {
		if(magazine == null && ransomNote == null) {
			return true;
		}
		if(ransomNote.length() == 0 && magazine.length() == 0) {
			return true;
		}
		if(magazine == null) {
			return false;
		}
		if(ransomNote == null) {
			return false;
		}
		
		int[] letCnt = new int[255];
		for(char c : magazine.toCharArray()) {
			letCnt[c]++;
		}
		for(char c : ransomNote.toCharArray()) {
			if(letCnt[c] == 0) {
				return false;
			}
			letCnt[c]--;
		}
		
		return true;
	}
	
	public static void main(String[] args) {
		new RansomNote().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
