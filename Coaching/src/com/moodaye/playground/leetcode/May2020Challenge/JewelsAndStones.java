package com.moodaye.playground.leetcode.May2020Challenge;

import java.io.PrintWriter;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

// Problem LeetCode May 2020 Challenge
public class JewelsAndStones {
	void solve(Scanner in, PrintWriter out) {
		String j = in.next();
		String s = in.next();
		out.println(numJewelsInStones(j, s));
	}

	public int numJewelsInStones(String J, String S) {
		if(J == null || J.length() == 0 || S == null || S.length() == 0) {
			return 0;
		}
		int cnt = 0;
		Set<Character> set = new HashSet<>();
		for(char c : J.toCharArray())
			set.add(c);
		for(char c : S.toCharArray()) {
			if(set.contains(c)) {
				cnt++;
			}
		}
		return cnt;
	}

	public static void main(String[] args) {
		new JewelsAndStones().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
