package com.moodaye.playground.leetcode.May2020Challenge;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

// Problem LeetCode May 2020 Challenge
public class ValidPerfectSquare {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		out.println(isPerfectSquare(n));
	}

	public boolean isPerfectSquare(int num) {
		// find closest square of 2
		int pow = 30;
		int mask = 1 << pow;
		while((mask & num) == 0) {
			mask >>= 1;
			pow--;
		}
		long  guess = 1 << (pow / 2);
		while(guess * guess <= num) {
			if(guess * guess > Integer.MAX_VALUE) {
				return false;
			}
			if(guess * guess == num) {
				return true;
			}
			guess++;
		}
		return false;
	}

	public static void main(String[] args) {
		new ValidPerfectSquare().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
