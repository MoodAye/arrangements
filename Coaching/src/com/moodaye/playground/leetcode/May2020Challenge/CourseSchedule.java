package com.moodaye.playground.leetcode.May2020Challenge;

import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Deque;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Queue;
import java.util.Scanner;

// Problem  LeetCode May 2020 Challenge
public class CourseSchedule {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int p = in.nextInt();
		int[][] pr = new int[p][2];
		for (int i = 0; i < pr.length; i++) {
			pr[i][0] = in.nextInt();
			pr[i][1] = in.nextInt();
		}

		out.println(canFinish(n, pr));
	}

	public boolean canFinish(int n, int[][] pr) {
		// create graph adj list
		Map<Integer, List<Integer>> adj = new HashMap<>();
		
		boolean ans = true;

		// track nodes with incoming edges for checking circular paths
		boolean[] inE = new boolean[n];

		for (int i = 0; i < pr.length; i++) {
			int p = pr[i][0];
			int v = pr[i][1];

			inE[v] = true;

			if (adj.containsKey(p)) {
				adj.get(p).add(v);
			} else {
				List<Integer> list = new LinkedList<>();
				list.add(v);
				adj.put(p, list);
			}
		}

		// check for circular path starting only with nodes without no in edges
		// run bfs (or dfs?) from these starting points

		List<Integer> noInEdges = new LinkedList<>();
		for (int i = 0; i < n; i++) {
			// if (!inE[i]) {
			noInEdges.add(i);
			// }
		}

		if (noInEdges.size() == 0) {
			return false;
		}

		boolean[] visited = new boolean[n];
		boolean[] onStack = new boolean[n];

		for (int root : noInEdges) {
			if (visited[root]) {
				continue;
			}
			Arrays.fill(visited, false);
			ans =  dfs(root, adj, onStack, visited);
			if(!ans) {
				break;
			}
		}
		return ans;
	}

	private boolean dfs(int node, Map<Integer, List<Integer>> adj, boolean[] onStack, boolean[] visited) {
		if (onStack[node]) {
			return false;
		}
		if (visited[node]) {
			return true;
		}
		visited[node] = true;
		onStack[node] = true;

		List<Integer> connectedNodes = adj.get(node);
		if (connectedNodes != null) {
			for (int cnode : connectedNodes) {
				if(! dfs(cnode, adj, onStack, visited)) {
					return false;
				}
			}
		}
		onStack[node] = false;
		return true;
	}

	public static void main(String[] args) {
		new CourseSchedule().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
