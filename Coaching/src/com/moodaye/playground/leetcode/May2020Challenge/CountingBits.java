package com.moodaye.playground.leetcode.May2020Challenge;

import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Locale;
import java.util.Scanner;

// Problem LeetCode Challenge May 2020
public class CountingBits {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		Arrays.stream(countBits(n)).forEach(x -> out.printf("%d, ", x));
	}
	
	public int[] countBits(int n) {
		int[] cnt = new int[n + 1];
		int idx = 1;
		for(int i = 1; i <= n; i++) {
			// check for power of 2
			if(((i - 1) & i) == 0) {
				cnt[i] = 1;
				idx = 1;
			}
			else {
				cnt[i] = 1 + cnt[idx++];
			}
		}
		
		return cnt;
	}

	public static void main(String[] args) {
		new CountingBits().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
