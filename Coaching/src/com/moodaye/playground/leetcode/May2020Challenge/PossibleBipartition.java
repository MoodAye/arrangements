package com.moodaye.playground.leetcode.May2020Challenge;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Queue;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;

// Problem LeetCode 2020 May Challenge
public class PossibleBipartition {
	void solve(Scanner in, PrintWriter out) {
		int limit = in.nextInt();
		int n = in.nextInt();
		int[][] nums = new int[n][2];
		for (int i = 0; i < nums.length; i++) {
			nums[i][0] = in.nextInt();
			nums[i][1] = in.nextInt();
		}
		out.println(possibleBipartition(limit, nums));
	}

	private void adjListAdd(Map<Integer, List<Integer>> adj, int t, int v) {
		List<Integer> list = null;
		if (!adj.containsKey(t)) {
			list = new ArrayList<>();
		} else {
			list = adj.get(t);
		}
		list.add(v);
		adj.put(t, list);
	}

	public boolean possibleBipartition(int N, int[][] dislikes) {
		final int RED = 10;
		final int BLUE = -10;
		boolean[] visited = new boolean[N + 1];
		int[] color = new int[N + 1];

		Map<Integer, List<Integer>> adj = new HashMap<>();
		for (int i = 0; i < dislikes.length; i++) {
			int t = dislikes[i][0];
			int v = dislikes[i][1];
			adjListAdd(adj, t, v);
			adjListAdd(adj, v, t);
		}

		// bfs
		Queue<Integer> queue = new LinkedList<Integer>();
		for (int i : adj.keySet()) {
			if (!visited[i]) {
				queue.add(i);
			}
			color[i] = RED;
			int curColor = RED;

			while (queue.size() > 0) {
				int next = queue.remove();
				curColor = color[next] == RED ? BLUE : RED;

				List<Integer> list = adj.get(next);
				if (list != null) {
					for (int j : list) {
						if (color[j] + curColor == 0) {
							return false;
						}
						color[j] = curColor;
						if (!visited[j]) {
							queue.add(j);
						}

					}
				}
				visited[next] = true;
			}
		}
		return true;
	}

	// tle here as well - perhaps build an adjacency list?
	public boolean possibleBipartition3(int N, int[][] dislikes) {
		final int RED = 10;
		final int BLUE = -10;
		boolean[] visited = new boolean[N + 1];
		int[] color = new int[N + 1];

		// set adjacency matrix
		boolean[][] adj = new boolean[N + 1][N + 1];
		for (int i = 0; i < dislikes.length; i++) {
			adj[dislikes[i][0]][dislikes[i][1]] = true;
			adj[dislikes[i][1]][dislikes[i][0]] = true;
		}

		// bfs
		Queue<Integer> queue = new LinkedList<Integer>();
		for (int i = 1; i <= N; i++) {
			if (!visited[i]) {
				queue.add(i);
			}
			color[i] = RED;
			int curColor = RED;

			while (queue.size() > 0) {
				int next = queue.remove();
				curColor = color[next] == RED ? BLUE : RED;

				for (int j = 1; j <= N; j++) {
					if (adj[next][j] && !visited[j]) {
						if (color[j] + curColor == 0) {
							return false;
						}
						color[j] = curColor;
						queue.add(j);
					}
				}
				visited[next] = true;
			}
		}
		return true;
	}

	/*
	 * We do this recursively - trying each option. Each dislike pair can either go
	 * into the first group or second group - we try both. So the number of
	 * permutation is 2^10_000 - so not sure this will work or not lets try this.
	 * 
	 * Use TreeSet - so that search is logN.
	 * 
	 * So to add a pair [x,y] to set1 and set2 we need to know the following: Before
	 * the add: -set1 cannot contain x and y and set2 cannot contain x and y
	 * 
	 * Fitment for add x to set1 and y to set2 -set1 cannot contain y and set2
	 * cannot contain x
	 * 
	 * Fitment for add y to set1 and x to set2 -set1 cannot contain x and set2
	 * cannot contain y
	 * 
	 * Now can a set contain a conflict if we are trying to add an element - that is
	 * a conflict from a prior dislike e.g., we are trying to add x to set1 and y to
	 * set2 but previously we had a dislike x,a and set1 contains a? The key is that
	 * if we had previously seen x,a then we would have added x and a to set1 and
	 * set2 and vice versa.
	 */
	public boolean possibleBipartition2(int N, int[][] dislikes) {
		if (dislikes.length <= 1) {
			return true;
		}

		Set<Integer> set1 = new TreeSet<>();
		Set<Integer> set2 = new TreeSet<>();

		set1.add(dislikes[0][0]);
		set2.add(dislikes[0][1]);

		return checkPerms(1, dislikes, set1, set2);
	}

	private boolean checkPerms(int idx, int[][] dislike, Set<Integer> s1, Set<Integer> s2) {
		if (idx == dislike.length) {
			return true;
		}

		if (s1.contains(dislike[idx][0]) && s1.contains(dislike[idx][1])
				|| (s2.contains(dislike[idx][0]) && s2.contains(dislike[idx][1]))) {
			return false;
		}

		// add items

		Set<Integer> s1Opt2 = new TreeSet<>(s1);
		Set<Integer> s2Opt2 = new TreeSet<>(s2);

		if (!s1.contains(dislike[idx][0]) && !s2.contains(dislike[idx][1])) {
			s1.add(dislike[idx][1]);
			s2.add(dislike[idx][0]);
			if (checkPerms(idx + 1, dislike, s1, s2)) {
				return true;
			}
		}

		if (!s1Opt2.contains(dislike[idx][1]) && !s2Opt2.contains(dislike[idx][0])) {
			s1Opt2.add(dislike[idx][0]);
			s2Opt2.add(dislike[idx][1]);
			if (checkPerms(idx + 1, dislike, s1Opt2, s2Opt2)) {
				return true;
			}
		}

		return false;
	}

	public static void main(String[] args) {
		new PossibleBipartition().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
