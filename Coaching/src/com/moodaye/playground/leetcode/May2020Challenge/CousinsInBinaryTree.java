package com.moodaye.playground.leetcode.May2020Challenge;

import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.Locale;
import java.util.Queue;
import java.util.Scanner;

// Problem LeetCode May 2020 Challenge
public class CousinsInBinaryTree {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int x = in.nextInt();
		int y = in.nextInt();
		String[] nums = new String[n];
		for (int i = 0; i < nums.length; i++) {
			nums[i] = in.next();
		}
		out.println(isCousins(makeBinaryTree(nums), x, y));
	}

	public boolean isCousins(TreeNode root, int x, int y) {
		if(x == y || root == null || root.val == x || root.val == y) {
			return false;
		}
		
		TreeNode parentX = null;
		TreeNode parentY = null;
		int levelX = -1;
		int levelY = -2;
		Queue<TreeNode> q = new LinkedList<>();
		Queue<Integer> qLevel = new LinkedList<>();
		Queue<TreeNode> parent = new LinkedList<>();
		q.add(root);
		qLevel.add(0);
		parent.add(null);
		while(q.size() != 0 && (parentX == null || parentY == null)) {
			TreeNode next = q.remove();
			int level = qLevel.remove();
			TreeNode cParent = parent.remove();
			if(x == next.val) {
				levelX = level;
				parentX = cParent;
			}
			else if(y == next.val) {
				levelY = level;
				parentY = cParent;
			}
			if(next.left != null) {
				q.add(next.left);
				qLevel.add(level + 1);
				parent.add(next);
			}
			if(next.right != null) {
				q.add(next.right);
				qLevel.add(level + 1);
				parent.add(next);
			}
		}
		
		return (parentX.val != parentY.val) && (levelX == levelY);
	}

	public static void main(String[] args) {
		new CousinsInBinaryTree().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}

	static class TreeNode {
		int val;
		TreeNode left;
		TreeNode right;

		TreeNode() {
		}

		TreeNode(int val) {
			this.val = val;
		}

		TreeNode(int val, TreeNode left, TreeNode right) {
			this.val = val;
			this.left = left;
			this.right = right;
		}
	}

	public static TreeNode makeBinaryTree(String[] nums) {
		TreeNode root = new TreeNode(Integer.valueOf(nums[0]));
		Queue<TreeNode> queue = new LinkedList<>();
		queue.add(root);
		int idx = 1;
		while (queue.size() != 0) {
			TreeNode next = queue.remove();
			if (idx < nums.length) {
				if (!nums[idx].equals("null")) {
					TreeNode left = new TreeNode(Integer.valueOf(nums[idx]));
					queue.add(left);
					next.left = left;
				}
				idx++;
			}
			if (idx < nums.length) {
				if (!nums[idx].equals("null")) {
					TreeNode right = new TreeNode(Integer.valueOf(nums[idx]));
					queue.add(right);
					next.right = right;
				}
				idx++;
			}
		}
		return root;
	}
}
