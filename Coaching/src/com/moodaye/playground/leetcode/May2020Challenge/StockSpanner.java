package com.moodaye.playground.leetcode.May2020Challenge;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Scanner;

// Problem LeetCode May 2020 Challenge
public class StockSpanner {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();

		for (int i = 0; i < n; i++) {
			out.print(next(in.nextInt()) + " ");
		}
	}

	Map<Integer, Span> history = new HashMap<>();

	public StockSpanner() {

	}

	public int next(int price) {
		int idx = history.size();
		if (idx == 0) {
			history.put(0, new Span(price,1));
			return 1;
		}
	
		int span = 1;
		int cidx = idx;
		while(idx > 0) {
			Span prev = history.get(idx - 1);
			if(prev.price > price) {
				history.put(cidx, new Span(price, span));
				return span;
			}
			span += prev.span;
			idx -= prev.span;
		}
		history.put(cidx, new Span(price, span));
		return span;
	}

	static class Span {
		int price;
		int span;
		Span(int price, int span){
			this.price = price;
			this.span = span;
		}
	}

	public static void main(String[] args) {
		new StockSpanner().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
