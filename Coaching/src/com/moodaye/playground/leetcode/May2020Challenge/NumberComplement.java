package com.moodaye.playground.leetcode.May2020Challenge;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

// Problem LeetCode May2020 Challenge
public class NumberComplement {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		out.println(findComplement(n));
	}
	
    public int findComplement(int num) {
    	// find first bit from left
    	int mask = 1 << 30;
    	while((num & mask) == 0) {
    		mask >>= 1;
    	}
    	
    	// now flip the bits
    	while(mask > 0) {
    		num ^= mask;
    		mask >>= 1;
    	}
    	return num;
    }


	public static void main(String[] args) {
		new NumberComplement().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
