package com.moodaye.playground.leetcode.May2020Challenge;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

// Problem LeetCode May 2020 Challenge
public class SortCharactersByFrequency {
	void solve(Scanner in, PrintWriter out) {
		String s = in.next();

		out.println(frequencySort(s));
	}

	public String frequencySort(String s) {
		char[] cs = s.toCharArray();

		int[] cnts = new int[256];

		for (char c : cs) {
			cnts[c]++;
		}

		// in place selection sort
		StringBuilder sb = new StringBuilder();
		int max = 0;
		int maxIdx = 0;
		while (true) {
			boolean found = false;
			for (int j = 0; j < cnts.length; j++) {
				if (cnts[j] <= 0) {
					continue;
				}
				found = true;
				if (cnts[j] >= max) {
					max = cnts[j];
					maxIdx = j;
				}
			}
			if (!found) {
				break;
			}
			while (cnts[maxIdx]-- > 0) {
				sb.append((char) maxIdx);
			}
			max = 0;
		}
		return sb.toString();
	}

	public static void main(String[] args) {
		new SortCharactersByFrequency().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
