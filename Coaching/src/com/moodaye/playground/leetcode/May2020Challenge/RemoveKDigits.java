package com.moodaye.playground.leetcode.May2020Challenge;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

// Problem  LeetCode Challenge May 2020
public class RemoveKDigits {
	void solve(Scanner in, PrintWriter out) {
		String num = in.next();
		int k = in.nextInt();
		out.println(removeKdigits(num, k));
	}

	public String removeKdigits(String num, int k) {
		if (num.length() == 0) {
			return "0";
		}
		if (k == 0) {
			if (num.charAt(0) != '0') {
				return num;
			}
			int i = 0;
			while (num.charAt(i) == '0') {
				if (i == num.length() - 1) {
					return "0";
				}
				i++;
			}
			return num.substring(i);
		}

		for (int i = 1; i < num.length(); i++) {
			if (num.charAt(i) < num.charAt(i - 1)) {
				return removeKdigits(num.substring(0, i - 1) + num.substring(i), k - 1);
			}
		}

		return removeKdigits(num.substring(0, num.length() - 1), k - 1);
	}

	public static void main(String[] args) {
		new RemoveKDigits().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
