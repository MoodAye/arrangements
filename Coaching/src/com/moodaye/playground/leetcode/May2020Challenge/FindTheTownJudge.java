package com.moodaye.playground.leetcode.May2020Challenge;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

// Problem LeetCode May 2020 Challenge
public class FindTheTownJudge {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int t = in.nextInt();
		int[][] trust = new int[t][2];
		for (int i = 0; i < t; i++) {
			trust[i][0] = in.nextInt();
			trust[i][1] = in.nextInt();
		}
		out.println(findJudge(n, trust));
	}

	public int findJudge(int n, int[][] trust) {
		int[] cntWhoPersonTrusts = new int[n + 1];
		int[] cntTrustedBy = new int[n + 1];
		for(int i = 0; i < trust.length; i++) {
				cntWhoPersonTrusts[trust[i][0]]++;
				cntTrustedBy[trust[i][1]]++;
		}
		
		for(int i = 1; i <= n; i++) {
			if(cntWhoPersonTrusts[i] == 0) {
				if(cntTrustedBy[i] == n - 1) {
					return i;
				}
			}
		}

		return -1;
	}

	public static void main(String[] args) {
		new FindTheTownJudge().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
