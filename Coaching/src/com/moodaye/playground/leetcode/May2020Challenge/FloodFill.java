package com.moodaye.playground.leetcode.May2020Challenge;

import java.io.PrintWriter;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

// Problem LeetCode May 2020 Challenge
/*
 Tests
 3 3 1 1 2
 1 1 1
 1 1 0
 1 0 1

 2 3 0 0 2
 0 0 0
 0 0 0
 
 * 
 */
public class FloodFill {
	void solve(Scanner in, PrintWriter out) {
		int dr = in.nextInt();
		int dc = in.nextInt();
		int sr = in.nextInt();
		int sc = in.nextInt();
		int newColor = in.nextInt();
		int[][] image = new int[dr][dc];
		for (int r = 0; r < dr; r++) {
			for (int c = 0; c < dc; c++) {
				image[r][c] = in.nextInt();
			}
		}
		printImage(floodFill(image, sr, sc, newColor));
	}

	private void printImage(int[][] image) {
		for (int i = 0; i < image.length; i++) {
			Arrays.stream(image[i]).forEach(x -> System.out.print(x + " "));
			System.out.println();
		}


	}

	public int[][] floodFill(int[][] image, int sr, int sc, int newColor) {
		boolean[][] processed = new boolean[image.length][image[0].length];
		floodFill(image, sr, sc, newColor, image[sr][sc], processed);
		return image;
	}

	public void floodFill(int[][] image, int r, int c, int newColor, int oldColor, boolean[][] processed) {
		if (r >= image.length || r < 0 || c >= image[0].length || c < 0 || processed[r][c]) {
			return;
		}

		processed[r][c] = true;

		if (oldColor != image[r][c]) {
			return;
		}

		image[r][c] = newColor;

			floodFill(image, r - 1, c, newColor, oldColor, processed);
			floodFill(image, r + 1, c, newColor, oldColor, processed);
			floodFill(image, r, c - 1, newColor, oldColor, processed);
			floodFill(image, r, c + 1, newColor, oldColor, processed);
	}

	public static void main(String[] args) {
		new FloodFill().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
