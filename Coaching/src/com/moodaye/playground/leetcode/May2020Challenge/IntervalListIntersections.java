package com.moodaye.playground.leetcode.May2020Challenge;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

// Problem LeetCode May 2020 Challenge
/*
 * 
2 2
0 2 5 10
1 5 8 12

2 1
0 2 5 10
1 5

 */
public class IntervalListIntersections {
	void solve(Scanner in, PrintWriter out) {
		int n1 = in.nextInt();
		int n2 = in.nextInt();
		int[][] nums1 = new int[n1][2];
		int[][] nums2 = new int[n2][2];
		for (int i = 0; i < n1; i++) {
			nums1[i][0] = in.nextInt();
			nums1[i][1] = in.nextInt();
		}
		for (int i = 0; i < n2; i++) {
			nums2[i][0] = in.nextInt();
			nums2[i][1] = in.nextInt();
		}

		int[][] ans = intervalIntersection(nums1, nums2);
		Arrays.stream(ans).forEach((int[] x) -> System.out.printf("[%d,%d] ", x[0], x[1]));
	}

	/* solution from LeetCode - omg - how elegant ... !!! */
	public int[][] intervalIntersectionSolution(int[][] A, int[][] B) {
		List<int[]> ans = new ArrayList();
		int i = 0, j = 0;

		while (i < A.length && j < B.length) {
			// Let's check if A[i] intersects B[j].
			// lo - the startpoint of the intersection
			// hi - the endpoint of the intersection
			int lo = Math.max(A[i][0], B[j][0]);
			int hi = Math.min(A[i][1], B[j][1]);
			if (lo <= hi)
				ans.add(new int[] { lo, hi });

			// Remove the interval with the smallest endpoint
			if (A[i][1] < B[j][1])
				i++;
			else
				j++;
		}

		return ans.toArray(new int[ans.size()][]);
	}

	public int[][] intervalIntersection(int[][] A, int[][] B) {
		if (A.length == 0 || B.length == 0) {
			return new int[0][0];
		}

		List<int[]> list = new ArrayList<>();
		int[] next = null;

		// for each interval in A - find intervals in B
		for (int idA = 0; idA < A.length; idA++) {
			List<Integer> listB = intervalIdx(B, A[idA][0], A[idA][1]);
			for (int idB : listB) {
				next = new int[2];
				next[0] = Math.max(A[idA][0], B[idB][0]);
				next[1] = Math.min(A[idA][1], B[idB][1]);
				list.add(next);
			}
		}

		// TODO - too much code
		int[][] ans = new int[list.size()][2];
		int i = 0;
		for (int[] x : list) {
			ans[i++] = x;
		}
		return ans;
	}

	/*
	 * TODO efficiency to be had here would be to track idx of array since array is
	 * sorted and search is sequential
	 */
	private List<Integer> intervalIdx(int[][] a, int low, int hi) {
		List<Integer> list = new LinkedList<>();
		for (int i = 0; i < a.length; i++) {
			if ((low <= a[i][0] && a[i][0] <= hi) || a[i][0] <= low && low <= a[i][1]) {
				list.add(i);
			}
		}
		return list;
	}

	/* TLE */
	public int[][] intervalIntersection3(int[][] A, int[][] B) {
		if (A.length == 0 || B.length == 0) {
			return new int[0][0];
		}

		int max = Math.max(A[A.length - 1][1], B[B.length - 1][1]);
		int min = Math.max(A[0][0], B[0][0]);

		List<int[]> list = new ArrayList<>();
		int[] next = null;

		for (int i = min; i <= max; i++) {
			int ieA = intervalExists(i, A);
			int ieB = intervalExists(i, B);
			if (ieA != 0 && ieB != 0) {
				if (next == null) {
					next = new int[2];
					next[0] = i;
					list.add(next);
				} else {
					if (ieA == 1 || ieB == 1) {
						next[1] = i - 1;
						next = new int[2];
						next[0] = i;
						list.add(next);
					}
				}
			} else if (next != null) {
				next[1] = i - 1;
				next = null;
			}
		}

		if (next != null) {
			next[1] = max;
		}

		// TODO - too much code
		int[][] ans = new int[list.size()][2];
		int i = 0;
		for (int[] x : list) {
			ans[i++] = x;
		}
		return ans;
	}

	/*
	 * return 0 if does not exist; 1 if exists and is at the start of the interval;
	 * 2 if exists and is not at start
	 */
	private int intervalExists(int k, int[][] a) {
		for (int i = 0; i < a.length; i++) {
			if (a[i][0] <= k && k <= a[i][1]) {
				if (a[i][0] == k) {
					return 1;
				}
				return 2;
			}
		}
		return 0;
	}

	/* MLE */
	public int[][] intervalIntersection2(int[][] A, int[][] B) {
		if (A.length == 0 || B.length == 0) {
			return new int[0][0];
		}

		int max = Math.max(A[A.length - 1][1], B[B.length - 1][1]);

		int[] cnts = new int[max + 1];
		// mark with A intervals - 1 marks the start of an interval.
		// Then increment subsequent elements.
		for (int i = 0; i < A.length; i++) {
			// use -ve ids to distinguish from B ids
			int id = -1;
			for (int j = A[i][0]; j <= A[i][1]; j++) {
				cnts[j] = id--;
			}
		}

		// Mark B intervals as we did for A above and at the same time overlay them
		// with A intervals using min(-Aid, Bid)
		for (int i = 0; i < B.length; i++) {
			int id = 1;
			for (int j = B[i][0]; j <= B[i][1]; j++) {
				cnts[j] = Math.min(-cnts[j], id);
				id++;
			}
		}

		List<int[]> list = new ArrayList<>();
		int[] next = null;

		// if 1 - then start interval
		// if 0 - then close interval or continue
		for (int i = 0; i <= max; i++) {
			if (cnts[i] == 1) {
				if (next != null) {
					next[1] = i - 1;
					list.add(next);
					next = null;
				}
				next = new int[2];
				next[0] = i;
			}
			if (cnts[i] <= 0) {
				if (next != null) {
					next[1] = i - 1;
					list.add(next);
					next = null;
				}
			}
		}

		if (next != null) {
			next[1] = max;
			list.add(next);
			next = null;
		}

		int[][] ans = new int[list.size()][2];
		int i = 0;
		for (int[] x : list) {
			ans[i++] = x;
		}
		return ans;
	}

	public static void main(String[] args) {
		new IntervalListIntersections().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
