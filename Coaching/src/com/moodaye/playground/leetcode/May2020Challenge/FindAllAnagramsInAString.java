package com.moodaye.playground.leetcode.May2020Challenge;

import java.io.PrintWriter;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

// Problem LeetCode May 2020 Challenge
public class FindAllAnagramsInAString {
	void solve(Scanner in, PrintWriter out) {
		String s = in.next();
		String p = in.next();
		out.printf("len s = %d; len p = %d%n", s.length(), p.length());
		out.printf("first index = %d, last index = %d %n", s.indexOf(p), s.lastIndexOf(p));
		out.println(findAnagrams(s, p));
	}

	public List<Integer> findAnagrams(String s, String p) {
		char[] cs = s.toCharArray();
		char[] cp = p.toCharArray();
		int plen = cp.length;

		List<Integer> indexes = new LinkedList<>();

		Map<Character, Integer> slc = new HashMap<Character, Integer>();
		Map<Character, Integer> plc = new HashMap<Character, Integer>();

		for (char c : cp) {
			plc.merge(c, 1, Integer::sum);
		}

		for (int i = 0; i < cs.length; i++) {
			slc.merge(cs[i], 1, Integer::sum);
			
			if (i < (plen - 1)) {
				continue;
			}
			
			int tail = i - plen + 1;
			
			if (mapsAreEquals(slc, plc)) {
				indexes.add(tail);
			}
			
			int v = slc.get(cs[tail]);
			if (v == 1) {
				slc.remove(cs[tail]);
			} else {
				slc.put(cs[tail], v - 1);
			}
		}
		return indexes;
	}

	private boolean mapsAreEquals(Map<Character, Integer> m1, Map<Character, Integer> m2) {
		if (m1.size() != m2.size()) {
			return false;
		}
		Set<Character> m1Keys = m1.keySet();
		for (Character c : m1Keys) {
			if (m1.get(c) != m2.get(c)) {
				return false;
			}
		}
		return true;
	}

	public static void main(String[] args) {
		new FindAllAnagramsInAString().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
