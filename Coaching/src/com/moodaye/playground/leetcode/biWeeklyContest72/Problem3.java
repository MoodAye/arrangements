package com.moodaye.playground.leetcode.biWeeklyContest72;

import java.util.ArrayList;
import java.util.List;

public class Problem3 {

	public List<Long> maximumEvenSplit(long finalSum) {
		List<Long> list = new ArrayList<>();
		if (finalSum <= 1L)
			return list;

		long first = 2L;
		long n = 1L;
		long last = 2L;

		long sum = 2L;

		while (sum < finalSum) {
			n *= 2L;
			last = first + (n - 1L) * 2L;
			sum = (first / 2 + last / 2) * n;
		}

		if (sum == finalSum) {
			for (long i = 1; i <= n; i++) {
				list.add(first);
				first += 2;
			}
			return list;
		}

		// lets get max list whose sum is less than final sum
		long right = n;
		long left = n / 2L;

		while (left < right) {
			long mid = left + (right - left) / 2L;
			last = first + (mid - 1L) * 2L;
			sum = (first / 2 + last / 2) * mid;

			if (sum == finalSum) {
				for (long i = 1; i <= mid; i++) {
					list.add(first);
					first += 2;
				}
				break;
			}

			if (sum > finalSum) {
				right = left + (mid - left) / 2;
			} else {
				if (finalSum - sum < last + 2) {
					for (long i = 1; i < mid; i++) {
						list.add(first);
						first += 2;
					}

					list.add(last + (finalSum - sum));
					break;
				}
				left = mid + (right - mid) / 2;
			}
		}

		return list;
	}

	public List<Long> maximumEvenSplit2(long finalSum) {
		List<Long> list = new ArrayList<>();
		if (finalSum % 2 == 1)
			return list;

		long sum = 0L;
		for (long i = 2; i <= finalSum; i = i + 2L) {
			if (sum + i > finalSum) {
				long diff = finalSum - sum;
				list.add(i - 2 + diff);
				break;
			}
			sum += i;
			list.add(i);
			list.remove(i - 2);
			if (sum == finalSum)
				break;
		}

		return list;
	}
}
