package com.moodaye.playground.leetcode.April2020Challenge;

import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Locale;
import java.util.Scanner;

// Problem LeetCode - Day3
public class MaximumSubarray {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int[] nums = new int[n];
		for(int i = 0; i < n; i++) {
			nums[i] = in.nextInt();
		}
		out.println(maxSubArray(nums));
	}

	public int maxSubArray(int[] nums) {
		for(int i = 1; i < nums.length; i++) {
			nums[i] = Math.max(nums[i], nums[i - 1] + nums[i]);
		}
		return Arrays.stream(nums).max().getAsInt();
	}

	public static void main(String[] args) {
		new MaximumSubarray().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
