package com.moodaye.playground.leetcode.April2020Challenge;

import static org.junit.Assert.*;

import org.junit.Test;

public class LRUCacheTest {

	@Test
	public void test() {
		LRUCache cache = new LRUCache( 2 /* capacity */ );

		cache.put(1, 1);
		cache.put(2, 2);
		
		assertEquals(cache.get(1), 1);       // returns 1
		
		cache.put(3, 3);    // evicts key 2
		assertEquals(cache.get(2), -1);  // returns -1 (not found)       
		
		cache.put(4, 4);    // evicts key 1
		assertEquals(cache.get(1), -1);  // returns -1 (not found)       
		
		assertEquals(cache.get(3), 3);       // returns 3
		assertEquals(cache.get(4), 4);       // returns 4
		
	}
	
	@Test
	public void test2() {
		LRUCache cache = new LRUCache(2);
		assertEquals(cache.get(2), -1);
		
		cache.put(1, 1);
		cache.put(2, 2);
		
		cache.put(3, 3);    
		assertEquals(cache.get(1), -1); 
		assertEquals(cache.get(2), 2);  
		assertEquals(cache.get(3), 3);  
	}
	
	@Test
	public void test3() {
		LRUCache cache = new LRUCache(2);
		cache.put(2,1);    //   2
		cache.put(1,1);    //   1-->2
		cache.put(2,3);    //   2-->1
		cache.put(4,1);    //   4-->2  (1 evicted)
		assertEquals(cache.get(1), -1);
		assertEquals(cache.get(2), 3);
	}
}
