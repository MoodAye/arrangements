package com.moodaye.playground.leetcode.April2020Challenge;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

// Problem LeetCode Day 9
public class BackSpaceStringCompare {
	void solve(Scanner in, PrintWriter out) {
		String s1 = in.next();
		String s2 = in.next();
		out.println(backspaceCompare(s1, s2));
	}

	public boolean backspaceCompare(String S, String T) {
		int idxS = S.length() - 1;
		int idxT = T.length() - 1;
		while (idxS >= 0 && idxT >= 0) {
			idxS = getNext(S, idxS);
			idxT = getNext(T, idxT);
			if (idxS < 0 || idxT < 0) {
				break;
			}
			if (S.charAt(idxS) != T.charAt(idxT)) {
				return false;
			}
			idxS--;
			idxT--;
		}
		return getNext(S, idxS) == getNext(T, idxT);
	}

	public int getNext(String s, int idx) {
		if (idx < 0) {
			return -1;
		}

		int backSpaces = 0;
		while (idx >= 0 && s.charAt(idx) == '#') {
			backSpaces++;
			idx--;
		}
		while ((idx >= 0) && (backSpaces > 0  || s.charAt(idx) == '#')) {
			if(s.charAt(idx--) == '#') {
				backSpaces++;
				continue;
			}
			backSpaces--;
		}
		return idx >= 0 ? idx : -1;
	}

	public static void main(String[] args) {
		new BackSpaceStringCompare().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
