package com.moodaye.playground.leetcode.April2020Challenge;

import java.util.Deque;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Objects;

public class LRUCache {
	Map<Integer, Node> vals = new HashMap<>();
	Deque<Node> queue = new LinkedList<>();
	int capacity;

	public LRUCache(int capacity) {
		this.capacity = capacity;
	}

	public int get(int key) {
		if (vals.containsKey(key)) {
			Node node = vals.get(key);

			// NOT O(1) !!
			queue.remove(node);

			queue.add(node);
			return node.value;
		} else {
			return -1;
		}
	}

	public void put(int key, int value) {
		Node node = new Node(key, value);
		if (!vals.containsKey(key)) {
			if (queue.size() == capacity) {
				Node first = queue.remove();
				vals.remove(first.key);
			}
		}
		else {
			queue.remove(node);
		}
		queue.add(node);
		vals.put(key, node);
	}

	static class Node {
		int key;
		int value;

		Node(int key, int value) {
			this.key = key;
			this.value = value;
		}

		@Override
		public int hashCode() {
			return Objects.hash(key);
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj) {
				return true;
			}
			if (!(obj instanceof Node)) {
				return false;
			}
			Node other = (Node) obj;
			return key == other.key;
		}
	}
}
