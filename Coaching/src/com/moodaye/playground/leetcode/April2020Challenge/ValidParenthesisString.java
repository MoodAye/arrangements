package com.moodaye.playground.leetcode.April2020Challenge;

import java.io.PrintWriter;
import java.util.Deque;
import java.util.LinkedList;
import java.util.Locale;
import java.util.Scanner;

// Problem LeetCode April Challenge
public class ValidParenthesisString {
	void solve(Scanner in, PrintWriter out) {
		while(true) {
			System.out.println(checkValidString(in.next()));
		}
	}
	
	public boolean checkValidString(String s) {
		StringBuilder sb = new StringBuilder();
		for(char c : s.toCharArray()) {
			if(c == '(') sb.append(')');
			else if(c == ')') sb.append('(');
			else sb.append('*');
		}
		return checkValidString2(s) && checkValidString2(sb.reverse().toString());
	}
	
	public boolean checkValidString2(String s) {
		int open = 0; // (
		int wc = 0; // *
		char[] cs = s.toCharArray();
		for(char c : cs) 
			if(c == '(') open++;
			else if(c == '*') wc++;
			else 
				if(open > 0) open--;
				else if(wc > 0) wc--;
				else return false;
		return wc - open >= 0;
	}

	public static void main(String[] args) {
		new ValidParenthesisString().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
