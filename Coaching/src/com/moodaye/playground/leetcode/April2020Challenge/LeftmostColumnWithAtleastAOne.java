package com.moodaye.playground.leetcode.April2020Challenge;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;


// Problem  LeetCode - April 2020 challenge
/* Test Case
 *	2 2 
 *  0 0
 *  1 1  
 *  ok
 *  
 *  Test Case
 *  2 2
 *  0 0 
 *  0 1
 *  fail 
 * 
 * 
 */
public class LeftmostColumnWithAtleastAOne {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int m = in.nextInt();
		int[][] matrix = new int[n][m];
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < m; j++) {
				matrix[i][j] = in.nextInt();
			}
		}

		BinaryMatrix dm = new BinaryMatrix(matrix);
		out.println(leftMostColumnWithOne(dm));
	}

	public int leftMostColumnWithOne(BinaryMatrix binaryMatrix) {
		List<Integer> dim = binaryMatrix.dimensions();
		int n = dim.get(0);
		int m = dim.get(1);
		boolean[] rowsWithOne = new boolean[m];
		Arrays.fill(rowsWithOne, true);

		int left = 0;
		int right = m - 1;
		int leftMostCol = -1;
		int bmCnt = 0;
		while (left <= right) {
			int mid = (left + right) / 2;
			boolean atLeastOneCol = false;
			for (int row = 0; row < n; row++) {
				if (rowsWithOne[row]) {
					bmCnt++;
					if (binaryMatrix.get(row, mid) == 0) {
						rowsWithOne[row] = false;
					} else {
						atLeastOneCol = true;
					}
				}
			}
			
			if (atLeastOneCol) {
				leftMostCol = mid;
				right = mid - 1;
			} else {
				left = mid + 1;
				Arrays.fill(rowsWithOne, true);
			}
		}
		System.out.println("bm count = " + bmCnt);
		return leftMostCol;
	}

	static class BinaryMatrix {
		int[][] matrix;
		List<Integer> dimensions = new ArrayList<>();

		BinaryMatrix(int[][] matrix) {
			this.matrix = matrix;
			this.dimensions.add(matrix.length);
			this.dimensions.add(matrix[0].length);
		}

		List<Integer> dimensions() {
			return dimensions;
		}

		public int get(int x, int y) {
			return matrix[x][y];
		}
	}

	public static void main(String[] args) {
		new LeftmostColumnWithAtleastAOne().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
