package com.moodaye.playground.leetcode.April2020Challenge;
	
import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

// Problem 1 - LeetCode Challenge
public class NonDuplicateElement {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int[] vals = new int[n];
		for(int i = 0; i < n; i++) {
			vals[i] = in.nextInt();
		}
		out.println(nonDup(vals));
		int x = Integer.MAX_VALUE;
	}
	
	private int nonDup(int[] vals) {
		int ans = 0;
		for(int i = 0; i < vals.length; i++) {
			ans ^= vals[i];
		}
		return ans;
	}

	public static void main(String[] args) {
		new NonDuplicateElement().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
	
}
