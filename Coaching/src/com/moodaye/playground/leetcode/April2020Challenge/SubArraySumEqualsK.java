package com.moodaye.playground.leetcode.April2020Challenge;

import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Locale;
import java.util.Scanner;

// Problem LeetCode April Challenge 2020
// Complexity is O(n^2)
// Range of sums: Max = 1_000 * 20_000 = 20_000_000; Min = -20_000_000
public class SubArraySumEqualsK {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int k = in.nextInt();
		int[] nums = new int[n];
		for (int i = 0; i < nums.length; i++) {
			nums[i] = in.nextInt();
		}
		out.println(subarraySum(nums, k));
	}

	public int subarraySum(int[] nums, int k) {
		int cnt = 0;

		int[] sums = new int[nums.length];
		sums = Arrays.copyOf(nums, nums.length);

		// Todo - any way to merge this with loop below?
		for (int i = 0; i < nums.length; i++) {
			if (nums[i] == k) {
				cnt++;
			}
		}
		
		for (int len = 2; len <= nums.length; len++) {
			int priorSum = sums[len - 2];
			for (int i = len - 1; i < nums.length; i++) {
					int temp = sums[i];
					sums[i] = priorSum + nums[i];
					priorSum = temp;
					if (sums[i] == k) {
						cnt++;
					}
			}
		}
		return cnt;
	}

	public static void main(String[] args) {
		new SubArraySumEqualsK().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
