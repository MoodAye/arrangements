package com.moodaye.playground.leetcode.April2020Challenge;

import static org.junit.jupiter.api.Assertions.*;
import com.moodaye.playground.leetcode.easy.BSTPostOrderTraversal.TreeNode;
import static com.moodaye.playground.leetcode.April2020Challenge.PathSum.*;


import org.junit.jupiter.api.Test;

class PathSumTest {

	@Test
	void test() {
		TreeNode root = new TreeNode(1);
		root.left = new TreeNode(2);
		root.right = new TreeNode(3);
		
		assertFalse(hasPathSum(root, 5));
		
		root.right =  null;
		
		assertFalse(hasPathSum(root, 1));
	}
	
//	@Test
	void test2() {
		TreeNode root = new TreeNode(5);
		root.left = new TreeNode(4);
		
		root.left.left = new TreeNode(11);
		root.left.left.left = new TreeNode(7);
		root.left.left.right = new TreeNode(2);

		root.right = new TreeNode(8);
		root.right.left = new TreeNode(13);
		root.right.right = new TreeNode(4);
		root.right.right.right = new TreeNode(1);
		
		assertTrue(hasPathSum(root, 22));
	}

}
