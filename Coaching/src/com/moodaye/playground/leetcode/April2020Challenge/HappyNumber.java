package com.moodaye.playground.leetcode.April2020Challenge;

import java.io.PrintWriter;
import java.util.HashSet;
import java.util.Locale;
import java.util.Scanner;
import java.util.Set;

// Problem 2 - Leet Code April 2020 Challenge
public class HappyNumber {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		out.println(isHappy(n) ? "Yes" : "No");
	}

	public boolean isHappy(int n) {
		Set<Long> occured = new HashSet<>();
		long longn = 1L * n;
		while(!occured.contains(longn)) {
			occured.add(longn);
			longn = getSquares(longn);
			if(longn == 1) {
				return true;
			}
		}
		return false;
	}
	
	private long getSquares(long n) {
		long sq = 0;
		while(n != 0) {
			long next = n % 10;
			sq += next * next;
			n /= 10;
		}
		return sq;
	}

	public static void main(String[] args) {
		new HappyNumber().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
