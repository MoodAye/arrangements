package com.moodaye.playground.leetcode.April2020Challenge;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

// Problem LeetCode April 2020 challenge
public class BitwiseANDofNumbersRange {
	void solve(Scanner in, PrintWriter out) {
		int m = in.nextInt();
		int n = in.nextInt();
		out.println(rangeBitwiseAnd(m, n));
	}
	
	/*
	 Test: 10, 11  Expect 10
	 Test: 5,7 Expect 4
	 * 
	 */
	public int rangeBitwiseAnd(int m, int n) {
		if(m == 0 || (m == n)) {
			return m;
		}
		
		int mask = 1 << 31;
		int bitMLeftpos = 0;
		int bitNLeftpos = 0;
		int pos = 31;
		while((bitMLeftpos == 0 || bitNLeftpos == 0)) {
			mask >>= 1;
			if(((bitMLeftpos == 0) && (m & mask) != 0)) {
				bitMLeftpos = pos;
			}
			if(((bitNLeftpos == 0) && (n & mask) != 0)) {
				bitNLeftpos = pos;
			}
			pos--;
		}
		
		if(bitMLeftpos != bitNLeftpos) {
			return 0;
		}
		
		mask = 1 << (bitNLeftpos - 1);
		while(mask > 0 && (mask & m) == (mask & n)) {
			mask >>= 1;
		}
		
		// set all bit from mask to end 0.
		while(mask > 0) {
			int pmask = ~mask;
			m &= pmask;
			mask >>= 1;
		}
		return m;
	}
	
	
	public static void main(String[] args) {
		new BitwiseANDofNumbersRange().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
