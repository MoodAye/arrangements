package com.moodaye.playground.leetcode.April2020Challenge;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

// Problem LeetCode April 2020 Challenge
public class JumpGame {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int[] nums = new int[n];
		for (int i = 0; i < nums.length; i++) {
			nums[i] = in.nextInt();
		}
		out.println(canJump(nums));
	}
	
	public boolean canJump2(int[] nums) {
		if (nums == null || nums.length == 0) {
			return false;
		}
		
		if(nums.length == 1) {
			return true;
		}
		
		boolean[] reachable = new boolean[nums.length];
		reachable[0] = true;
		
		for (int i = 0; i < nums.length; i++) {
			if(!reachable[i]) continue;
			for(int j = 1; j <= nums[i] && i + j < nums.length; j++) {
				if(i + j == nums.length - 1) {
					return true;
				}
				reachable[j] = true;
			}
		}
		return false;
	}
	
	public boolean canJump(int[] nums) {
		if (nums == null || nums.length == 0) {
			return false;
		}
		if(nums.length == 1) {
			return true;
		}
		
		nums[0] *= -1;
		for (int i = 0; i < nums.length; i++) {
			if (nums[i] >= 0) {
				continue;
			}
			for (int j = 1; j <= -nums[i] && i + j < nums.length; j++) {
				if (i + j == nums.length - 1) {
					return true;
				}
				if (nums[i + j] > 0) {
					nums[i + j] *= -1;
				}
			}
		}

		return false;
	}

	public static void main(String[] args) {
		new JumpGame().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
