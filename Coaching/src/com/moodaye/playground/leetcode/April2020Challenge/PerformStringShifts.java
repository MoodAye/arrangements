package com.moodaye.playground.leetcode.April2020Challenge;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

// Problem LeetCode April 2020 Challenge
public class PerformStringShifts {
	void solve(Scanner in, PrintWriter out) {
		String s = in.next();
		int n = in.nextInt();
		int[][] matrix = new int[n][2];
		for(int i = 0; i < n; i++) {
			matrix[i][0] = in.nextInt();
			matrix[i][1] = in.nextInt();
		}
		out.println(stringShift(s, matrix));
	}
	
    public String stringShift(String s, int[][] shift) {
    	int netShift = 0;
    	for(int i = 0; i < shift.length; i++) {
    		netShift += shift[i][0] == 0 ? -shift[i][1] : shift[i][1];
    	}
    	netShift %= s.length();
    	netShift = (s.length() + netShift) % s.length();
    	s = s + s;
    	return s.substring(s.length() / 2 - netShift,  s.length() - netShift);
    }
    
    
	public static void main(String[] args) {
		new PerformStringShifts().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
