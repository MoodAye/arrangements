package com.moodaye.playground.leetcode.April2020Challenge;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

// Problem LeetCode- Week2 #1
public class MiddleOfTheLinkedList {
	
	static class ListNode{
		int val;
		ListNode next;
		ListNode(int x){
			val = x;
		}
	}

	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		ListNode head = null;
		if(n != 0) {
			head = new ListNode(in.nextInt());
		}
		
		ListNode next = head;
		
		for(int i = 1; i < n; i++) {
			next.next = new ListNode(in.nextInt());
			next = next.next;
		}
		
		ListNode middle = middleNode(head);
		next = middle;
		do {
			out.print(next.val + " ");
			next = next.next;
		}
		while(next != null);
		
	}
	
	public ListNode middleNode(ListNode head) {
		if(head == null) {
			return head;
		}
		ListNode next = head.next;
		
		int cnt = 1;
		while(next != null) {
			cnt++;
			next = next.next;
		}
		
		ListNode middle = head;
		int mid = cnt  / 2;
		while(mid > 0) {
			middle = middle.next;
			mid--;
		}
		
		return middle;
		
	}

	public static void main(String[] args) {
		new MiddleOfTheLinkedList().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
