package com.moodaye.playground.leetcode.April2020Challenge;

import com.moodaye.playground.leetcode.easy.BSTPostOrderTraversal.TreeNode;

// 743am - 746am  lc 226
public class InvertBinaryTree {

	public TreeNode invertTree(TreeNode root) {
		if(root == null) return root;
		
		TreeNode temp = root.left;
		root.left = root.right;
		root.right = temp;
		
		invertTree(root.left);
		invertTree(root.right);
		return root;
	}
}
