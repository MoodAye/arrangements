package com.moodaye.playground.leetcode.April2020Challenge;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

// Problem 
/*
 * 
6
7 1 5 3 6 4
7

5
1 2 3 4 5
4

 */
public class BestTimeToBuyAndSellStockII {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int[] prices = new int[n];
		for (int i = 0; i < n; i++) {
			prices[i] = in.nextInt();
		}
		out.println(maxProfit(prices));
	}

	public int maxProfit(int[] prices) {
		if (prices == null || prices.length < 2) {
			return 0;
		}

		int profit = 0;
		int currDay = 1;
		while (true) {
			// find starting element
			while (prices[currDay] <= prices[currDay - 1]) {
				currDay++;
				if (currDay == prices.length)
					break;
			}

			if (currDay == prices.length)
				break;

			int buyPrice = prices[currDay - 1];

			while (prices[currDay] > prices[currDay - 1]) {
				currDay++;
				if (currDay == prices.length)
					break;
			}

			profit += prices[currDay - 1] - buyPrice;

			if (currDay == prices.length) {
				break;
			}
		}
		return profit;
	}

	public static void main(String[] args) {
		new BestTimeToBuyAndSellStockII().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}

}
