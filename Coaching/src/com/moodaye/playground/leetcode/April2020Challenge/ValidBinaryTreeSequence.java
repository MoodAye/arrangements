package com.moodaye.playground.leetcode.April2020Challenge;

import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.Locale;
import java.util.Queue;
import java.util.Scanner;

/*  Test Cases
11
0 1 0 0 1 0 null null 1 0 0 
4
0 1 0 1
true

11
0 1 0 0 1 0 null null 1  0 0 
3
0 0 1
false

11
0 1 0 0 1 0 null null 1 0 0 
3
0 1 1
false

The solution here assumes that the left node and right node are different - that is an incorrect assumption.
The test below was failing

7
10 9 9 8 7 8 10
3
10 9 10
true
*/

// Problem LeetCode April Challenge 
public class ValidBinaryTreeSequence {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		String[] nums = new String[n];
		for (int i = 0; i < nums.length; i++) {
			nums[i] = in.next();
		}

		TreeNode root = new TreeNode(Integer.valueOf(nums[0]));
		Queue<TreeNode> queue = new LinkedList<>();
		queue.add(root);
		int idx = 1;
		while (queue.size() != 0) {
			TreeNode next = queue.remove();
			if (idx < nums.length) {
				if (!nums[idx].equals("null")) {
					TreeNode left = new TreeNode(Integer.valueOf(nums[idx]));
					queue.add(left);
					next.left = left;
				}
				idx++;
			}
			if (idx < nums.length) {
				if (!nums[idx].equals("null")) {
					TreeNode right = new TreeNode(Integer.valueOf(nums[idx]));
					queue.add(right);
					next.right = right;
				}
				idx++;
			}
		}

		int m = in.nextInt();
		int[] arr = new int[m];
		for (int i = 0; i < m; i++) {
			arr[i] = in.nextInt();
		}
		out.println(isValidSequence(root, arr));
	}

	public boolean isValidSequence(TreeNode root, int[] arr) {
		if (root == null && (arr == null || arr.length == 0)) {
			return true;
		}
		if (root == null) {
			return false;
		}
		if (arr == null || arr.length == 0) {
			return false;
		}
		return isValidSequence(root, arr, 0);
	}

	private boolean isValidSequence(TreeNode root, int[] arr, int startIdx) {
		if (root.val != arr[startIdx]) {
			return false;
		}
		if (startIdx == arr.length - 1) {
			return root.left == null && root.right == null;
		}

		int next = arr[startIdx + 1];
		boolean leftNodeFits = false;
		boolean rightNodeFits = false;
		if (root.left != null && next == root.left.val) {
			leftNodeFits = isValidSequence(root.left, arr, startIdx + 1);
		}
		if (root.right != null && next == root.right.val) {
			rightNodeFits = isValidSequence(root.right, arr, startIdx + 1);
		}

		return (leftNodeFits || rightNodeFits);
	}

	public class TreeNode {
		int val;
		TreeNode left;
		TreeNode right;

		TreeNode() {
		}

		TreeNode(int val) {
			this.val = val;
		}

		TreeNode(int val, TreeNode left, TreeNode right) {
			this.val = val;
			this.left = left;
			this.right = right;
		}
	}

	public static void main(String[] args) {
		new ValidBinaryTreeSequence().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
