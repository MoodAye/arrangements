package com.moodaye.playground.leetcode.April2020Challenge;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

// Problem LeetCode April Challenge
/* test case failing:
  3 5
  5 1 3
  fixed
  
  then failing on 
  6 2 
  3 4 5 6 1 2
  fixed
  
  but failing on the prior test case again.
*/
public class SearchInRotatedArray {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int target = in.nextInt();
		int[] nums = new int[n];
		for (int i = 0; i < nums.length; i++) {
			nums[i] = in.nextInt();
		}
		out.println(search(nums, target));
	}

	public int search(int[] nums, int target, int left, int right) {
		if (left == right) {
			if (nums[left] == target) {
				return left;
			}
			return -1;
		}

		int pivot = (left + right) / 2;
		while (nums[pivot] != target) {
			if (nums[left] <= nums[pivot]) {
				if (nums[left] <= target && target <= nums[pivot]) {
					return search(nums, target, left, pivot);
				} else {
					return search(nums, target, pivot + 1, right);
				}
			} else if (nums[pivot + 1] <= target && target <= nums[right]) {
				return search(nums, target, pivot + 1, right);
			}
			return search(nums, target, left, pivot);
		}
		return pivot;
	}

	public int search(int[] nums, int target) {
		if (nums == null || nums.length == 0) {
			return -1;
		}
		return search(nums, target, 0, nums.length - 1);
	}

	public static void main(String[] args) {
		new SearchInRotatedArray().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
