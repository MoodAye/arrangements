package com.moodaye.playground.leetcode.April2020Challenge;

import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Locale;
import java.util.Scanner;

// Problem LeetCode April 2020 Challenge
public class ProductOfArrayExceptSelf {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int[] nums = new int[n];
		for (int i = 0; i < nums.length; i++) {
			nums[i] = in.nextInt();
		}
		Arrays.stream(productExceptSelf(nums)).forEach(x -> System.out.printf("%d ", x));
	}
	
	public int[] productExceptSelf(int[] nums) {
		int[] left = new int[nums.length];
		Arrays.fill(left, 1);
		for(int i = 1; i < nums.length; i++) {
			left[i] = nums[i - 1] * left[i - 1];
		}
		int temp = 1;
		for(int i = nums.length - 2; i >= 0; i--) {
			temp *= nums[i + 1];
			left[i] *= temp;
		}
		
		return left;
	}

	public static void main(String[] args) {
		new ProductOfArrayExceptSelf().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
