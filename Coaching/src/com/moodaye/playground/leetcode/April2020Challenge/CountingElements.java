package com.moodaye.playground.leetcode.April2020Challenge;

import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Locale;
import java.util.Scanner;

// Problem  - Leet Code - Day 7
public class CountingElements {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int[] nums = new int[n];
		for (int i = 0; i < n; i++) {
			nums[i] = in.nextInt();
		}
		out.println(countElements(nums));
	}
	
	public int countElements(int[] arr) {
		Arrays.sort(arr);
		int cnt = 0;
		int numCnt = 1;
		for(int i = 0; i < arr.length - 1; i++) {
			if(arr[i] + 1 == arr[i + 1]) {
				cnt += numCnt;
				numCnt = 1;
			}
			else if(arr[i] == arr[i + 1]) {
				numCnt++;
			}
			else {
				numCnt = 1;
			}
		}
		return cnt;
	}

	public static void main(String[] args) {
		new CountingElements().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
