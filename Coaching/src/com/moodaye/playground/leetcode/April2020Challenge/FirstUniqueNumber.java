package com.moodaye.playground.leetcode.April2020Challenge;

import java.io.PrintWriter;
import java.util.Deque;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Locale;
import java.util.Map;
import java.util.Scanner;

// Problem LeetCode April 2020 challenge
public class FirstUniqueNumber {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int[] nums = new int[n];
		for (int i = 0; i < nums.length; i++) {
			nums[i] = in.nextInt();
		}
		FirstUnique fu = new FirstUnique(nums);
		while(true) {
			String cmd = in.next();
			if(cmd.equals("quit")) break;
			if(cmd.equals("FirstUnique")) {
				System.out.println(fu.showFirstUnique());
			}
			else if (cmd.equals("add")) {
				fu.add(in.nextInt());
			}
			else {
				System.out.println("what?");
			}
		}
	}

	public static void main(String[] args) {
		new FirstUniqueNumber().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}

/**
 * Your FirstUnique object will be instantiated and called as such: FirstUnique
 * obj = new FirstUnique(nums); int param_1 = obj.showFirstUnique();
 * obj.add(value);
 */
class FirstUnique {
	Deque<Integer> queue = new LinkedList<>();
	Map<Integer, Integer> cnt = new HashMap<Integer, Integer>();

	public FirstUnique(int[] nums) {
		for (int i = 0; i < nums.length; i++) {
			add(nums[i]);
		}
	}

	public int showFirstUnique() {
		while(queue.size() > 0) {
			int next = queue.peek();
			if(cnt.get(next) == 1) {
				return next;
			}
			queue.remove(next);
		}
		return -1;
	}

	public void add(int value) {
		if (!cnt.containsKey(value)) {
			queue.add(value);
		}
		cnt.merge(value, 1, Integer::sum);
	}
}