package com.moodaye.playground.leetcode.April2020Challenge;

import java.io.PrintWriter;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;
import java.util.Stack;

// Problem  Leet Code April challenge
public class DiameterOfBinaryTree {
	void solve(Scanner in, PrintWriter out) {

	}
	
	public int diamterOfBinaryTree(TreeNode root) {
		List<TreeNode> leafNodes = getLeafs(root);
		int maxCnt = 0;
		for(TreeNode leaf : leafNodes) {
			
			
			
			
			
			
		}
		return 1;
	}
	
	public static List<TreeNode> getLeafs(TreeNode root){
		List<TreeNode> leafs = new LinkedList<>();
		Deque<TreeNode> stack = new LinkedList<>();
		stack.push(root);
		while(stack.size() > 0) {
			TreeNode next = stack.pop();
			if(next.left == null && next.right == null) {
				leafs.add(next);
				continue;
			}
			if(next.left != null) {
				stack.push(next.left);
			}
			if(next.right != null) {
				stack.push(next.right);
			}
		}
		return leafs;
	}
	
	static class TreeNode{
		int val;
		TreeNode left;
		TreeNode right;
		TreeNode(int x){
			val = x;
		}
	}

	public static void main(String[] args) {
		new DiameterOfBinaryTree().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
