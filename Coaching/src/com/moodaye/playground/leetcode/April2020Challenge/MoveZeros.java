package com.moodaye.playground.leetcode.April2020Challenge;

import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Locale;
import java.util.Scanner;

// Problem  LeetCode April Challenge - Day 4
public class MoveZeros {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int[] nums = new int[n];
		for(int i = 0; i < n; i++) {
			nums[i] = in.nextInt();
		}
		moveZeroes(nums);
		Arrays.stream(nums).forEach(x -> out.printf("%d ", x));
	}

	public void moveZeroes(int[] nums) {
		int zeroCnt = 0;
		int lastNonZero = 0;
		
		for(int i = 0; i < nums.length; i++){
			if(nums[i] != 0) {
				nums[lastNonZero++] = nums[i];
			}
			else {
				zeroCnt++;
			}
		}
		
		for(int i = nums.length - zeroCnt; i < nums.length; i++) {
			nums[i] = 0;
		}
			
	}

	public static void main(String[] args) {
		new MoveZeros().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
