package com.moodaye.playground.leetcode.April2020Challenge;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

// Problem  Leet Code April 2020 Challenge
public class LastStoneWeight {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int[] w = new int[n];
		for (int i = 0; i < n; i++) {
			w[i] = in.nextInt();
		}
		out.println(lastStoneWeight(w));
	}

	public int lastStoneWeight(int[] stones) {
		int[] buckets = new int[1001];
		for(int w : stones) {
			buckets[w]++;
		}

		int h1 = 0;
		int h2 = 0;
		int i = 1000;
		while (i > 0) {
			if (buckets[i] == 0) {
				i--;
				continue;
			}
			if (h1 == 0) {
				h1 = i;
				buckets[i]--;
				continue;
			} else if (h2 == 0) {
				h2 = i;
				buckets[i]--;
				continue;
			} else {
				int smashed = h1 - h2;
				if (smashed != 0) {
					buckets[smashed]++;
				}
				i = Math.max(smashed, i);
				h1 = 0;
				h2 = 0;
			}
		}
		return h2 == 0 ? h1 : h1 - h2;
	}

	public static void main(String[] args) {
		new LastStoneWeight().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
