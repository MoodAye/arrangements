package com.moodaye.playground.leetcode.April2020Challenge;

import java.io.PrintWriter;
import java.util.Deque;
import java.util.LinkedList;
import java.util.Locale;
import java.util.Queue;
import java.util.Scanner;

// Problem LeetCode April 2020 Challenge
// Test Case - NEED NULL 
//	8,5,1,7,10,12 
//
// Test Case - OK
// 2 2 1
//
// Test Case - OK
// 6 8 4 3 2 1 9
public class ConstructBinaryTreeFromPreorderTraversal {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int[] preorder = new int[n];
		for (int p = 0; p < preorder.length; p++) {
			preorder[p] = in.nextInt();
		}
		TreeNode root = bstFromPreorder(preorder);
		Queue<TreeNode> queue = new LinkedList<>();
		queue.add(root);
		while (queue.size() > 0) {
			TreeNode next = queue.remove();
			out.print(next.val + " ");

			if (next.left != null) {
				queue.add(next.left);
			}
			if (next.right != null) {
				queue.add(next.right);
			}
		}
	}

	static class TreeNode {
		int val;
		TreeNode left;
		TreeNode right;

		TreeNode(int x) {
			val = x;
		}
	}

	public TreeNode bstFromPreorder(int[] preorder) {
		TreeNode root = new TreeNode(preorder[0]);
		bstFromPreorder(preorder, root, 0, preorder.length - 1);
		return root;
	}

	public void bstFromPreorder(int[] preorder, TreeNode root, int left, int right) {
		if (left > preorder.length - 1 || right > preorder.length - 1 || left >= right) {
			return;
		}

		TreeNode nodeLeft = null;
		if (left + 1 < preorder.length && preorder[left + 1] < root.val) {
			nodeLeft = new TreeNode(preorder[left + 1]);
			root.left = nodeLeft;
		}

		// find first element after left that is gt root
		int nodeRightIdx = -1;
		for (int i = left + 1; i <= right; i++) {
			if (preorder[i] > root.val) {
				nodeRightIdx = i;
				break;
			}
		}
		TreeNode nodeRight = null;
		if (nodeRightIdx != -1)
			nodeRight = new TreeNode(preorder[nodeRightIdx]);
		root.right = nodeRight;
		if (nodeLeft != null) {
			if (nodeRightIdx == -1) {
				bstFromPreorder(preorder, nodeLeft, left + 1, right);
			} else {
				bstFromPreorder(preorder, nodeLeft, left + 1, nodeRightIdx - 1);
			}
		}
		if (nodeRight != null) {
			bstFromPreorder(preorder, nodeRight, nodeRightIdx, right);
		}
	}

	public static void main(String[] args) {
		new ConstructBinaryTreeFromPreorderTraversal().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}

}
