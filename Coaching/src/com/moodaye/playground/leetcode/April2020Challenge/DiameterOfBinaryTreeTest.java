package com.moodaye.playground.leetcode.April2020Challenge;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.moodaye.playground.leetcode.April2020Challenge.DiameterOfBinaryTree;
import com.moodaye.playground.leetcode.April2020Challenge.DiameterOfBinaryTree.TreeNode;

public class DiameterOfBinaryTreeTest {
	TreeNode root1;
	TreeNode root2;
	TreeNode root3;
	
	@Before
	public void createTestObjects() {
		root1 = new TreeNode(1);
		root1.left = new TreeNode(2);
		root1.right = new TreeNode(3);
		
		root2 = new TreeNode(100);
		root2.left = new TreeNode(10);
		root2.right = new TreeNode(1000);
		root2.left.left = new TreeNode(1);
		root2.left.right = new TreeNode(20);
		root2.right.left = new TreeNode(500);
		root2.right.right = new TreeNode(1500);
	}

	@Test
	public void test1() {
		DiameterOfBinaryTree dbt = new DiameterOfBinaryTree();
		assertEquals(1, dbt.diamterOfBinaryTree(root1));
	}
	
	@Test
	public void test2() {
		List<TreeNode> leafs = DiameterOfBinaryTree.getLeafs(root1);
		assertEquals(2, leafs.size());
		leafs = DiameterOfBinaryTree.getLeafs(root2);
		assertEquals(4, leafs.size());
	}

}
