package com.moodaye.playground.leetcode.April2020Challenge;

import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.Locale;
import java.util.Queue;
import java.util.Scanner;

// Problem LeetCode April 2020 Challenge
public class BinaryTreeMaximumPath {
	int gMax = Integer.MIN_VALUE;

	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		String[] nums = new String[n];
		for (int i = 0; i < nums.length; i++) {
			nums[i] = in.next();
		}

		TreeNode root = new TreeNode(Integer.valueOf(nums[0]));
		Queue<TreeNode> queue = new LinkedList<>();
		queue.add(root);
		int idx = 1;
		while (queue.size() != 0) {
			TreeNode next = queue.remove();
			if (idx < nums.length) {
				if (!nums[idx].equals("null")) {
					TreeNode left = new TreeNode(Integer.valueOf(nums[idx]));
					queue.add(left);
					next.left = left;
				}
				idx++;
			}
			if (idx < nums.length) {
				if (!nums[idx].equals("null")) {
					TreeNode right = new TreeNode(Integer.valueOf(nums[idx]));
					queue.add(right);
					next.right = right;
				}
				idx++;
			}
		}
		out.println(maxPathSum(root));
//		out.println(gMax);
	}

	public int maxPathSum(TreeNode root) {
		maxPathSum2(root);
		return gMax;

	}

	public int maxPathSum2(TreeNode root) {
		if (root.left == null && root.right == null) {
			gMax = Math.max(root.val, gMax);
			return root.val;
		}
		int leftPathMax = 0;
		int rightPathMax = 0;
		int pMax = root.val;
		if (root.left != null) {
			leftPathMax = maxPathSum2(root.left);
			pMax = Math.max(pMax, root.val + leftPathMax);
		}
		if (root.right != null) {
			rightPathMax = maxPathSum2(root.right);
			pMax = Math.max(pMax, root.val + rightPathMax);
		}

		if (root.left != null && root.right != null) {
			gMax = Math.max(gMax, Math.max(pMax, leftPathMax + rightPathMax + root.val));
		} else {
			gMax = Math.max(gMax, pMax);
		}

		return pMax;
	}

	static class TreeNode {
		int val;
		TreeNode left;
		TreeNode right;

		TreeNode() {
		}

		TreeNode(int val) {
			this.val = val;
		}

		TreeNode(int val, TreeNode left, TreeNode right) {
			this.val = val;
			this.left = left;
			this.right = right;
		}
	}

	public static void main(String[] args) {
		new BinaryTreeMaximumPath().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
