package com.moodaye.playground.leetcode.April2020Challenge;

import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Deque;
import java.util.LinkedList;
import java.util.Locale;
import java.util.Scanner;

// Problem LeetCode Challenge April 2020
/* Approach: Checking from left to right, top to bottom
 *             1. If you have water to left and top - then island count ++
 *             2. If you have land to left or to top - then continue
 *             3. If you have water - then continue;
 * 
 */
/* Test Cases
 *	11110
    11010
    11000
    00000 
    Ans = 1
 * 
 * 11000
   11000
   00100
   00011
   Ans = 3
 */
public class NumberOfIslands {
	void solve(Scanner in, PrintWriter out) {
		int h = in.nextInt();
		String s = in.next();
		char[][] grid = new char[h][s.length()];
		grid[0] = s.toCharArray();
		for (int r = 1; r < h; r++) {
			s = in.next();
			grid[r] = s.toCharArray();
		}
		out.println(numIslands(grid));
	}

	public int numIslands(char[][] grid) {
		int cnt = 0;
		for (int r = 0; r < grid.length; r++) {
			for (int c = 0; c < grid[0].length; c++) {
				// if water to left and top?
				if (grid[r][c] == '1') {
					cnt++;
					mark(grid, r, c);
				}
			}
		}
		return cnt;
	}

	private void mark(char[][] grid, int r, int c) {
		Deque<Point> stack = new LinkedList<>();
		stack.add(new Point(r, c));
		while (stack.size() > 0) {
			Point next = stack.pop();
			if (next.x != 0 && grid[next.x - 1][next.y] == '1') {
				stack.add(new Point(next.x - 1, next.y));
			}
			if (next.y != 0 && grid[next.x][next.y - 1] == '1') {
				stack.add(new Point(next.x, next.y - 1));
			}
			if (next.x != grid.length - 1 && grid[next.x + 1][next.y] == '1') {
				stack.add(new Point(next.x + 1, next.y));
			}
			if (next.y != grid[0].length - 1 && grid[next.x][next.y + 1] == '1') {
				stack.add(new Point(next.x, next.y + 1));
			}
			grid[next.x][next.y] = '0';
		}
	}

	static class Point {
		int x, y;

		public Point(int x, int y) {
			this.x = x;
			this.y = y;
		}
	}

	public static void main(String[] args) {
		new NumberOfIslands().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
