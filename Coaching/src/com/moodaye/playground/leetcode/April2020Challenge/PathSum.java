package com.moodaye.playground.leetcode.April2020Challenge;

import com.moodaye.playground.leetcode.easy.BSTPostOrderTraversal.TreeNode;

// lc 112 752am - 829am
public class PathSum {
	   public static boolean hasPathSum(TreeNode root, int targetSum) {
		    if(root == null) return false;	
			return hasPathSum(root, targetSum, 0);
		}
		
		public static boolean hasPathSum(TreeNode root, int targetSum, int pSum) {
			if (root == null) return false;
	       if (root.left == null && root.right == null) {
				if (pSum + root.val == targetSum) return true;
			}
			return hasPathSum(root.left, targetSum, pSum + root.val) || hasPathSum(root.right, targetSum, pSum + root.val);
		}
}
