package com.moodaye.playground.leetcode.April2020Challenge;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

// Problem LeetCode April 2020 challenge
public class MaximalSquare {
	void solve(Scanner in, PrintWriter out) {
		int r = in.nextInt();
		int c = in.nextInt();
		char[][] matrix = new char[r][c];
		for (int i = 0; i < matrix.length; i++) {
			for(int j = 0; j < matrix[0].length; j++) {
				matrix[i][j] = (char) ('0' + in.nextInt());
			}
		}
		out.println(maximalSquare(matrix));
	}
	
	public int maximalSquare(char[][] matrix) {
		if(matrix == null) {
			return 0;
		}
		int max = 0;
		for(int i = 0; i < matrix.length; i++) {
			for(int j = 0; j < matrix[0].length; j++) {
				if(matrix[i][j] == '0') {
					continue;
				}
				if(i == 0 || j == 0) {
					max = Math.max(max, matrix[i][j] - '0');
					continue;
				}
				int min = Math.min(matrix[i][j - 1], 
						Math.min(matrix[i - 1][j - 1], matrix[i - 1][j]));
				matrix[i][j] = (char)(min + 1);
				max = Math.max(max, matrix[i][j] - '0');
			}
		}
		return max * max;
	}

	public static void main(String[] args) {
		new MaximalSquare().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
