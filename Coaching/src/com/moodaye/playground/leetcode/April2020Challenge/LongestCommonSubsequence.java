package com.moodaye.playground.leetcode.April2020Challenge;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

// Problem LeetCode April 2020 challenge
public class LongestCommonSubsequence {
	void solve(Scanner in, PrintWriter out) {
		String s1 = in.next();
		String s2 = in.next();
		out.println(longestCommonSubsequence(s1, s2));
	}

	public int longestCommonSubsequence(String text1, String text2) {
				int[][] dp = new int[text1.length() + 1][text2.length() + 1];

				for (int i = 1; i <= text1.length(); i++) {
					for (int j = 1; j <= text2.length(); j++) {
						if (text1.charAt(i - 1) == text2.charAt(j - 1)) {
							dp[i][j] = dp[i - 1][j - 1] + 1;
						} else {
							dp[i][j] = Math.max(dp[i - 1][j], dp[i][j - 1]);
						}
					}
				}
				return dp[text1.length()][text2.length()];
	}

	public static void main(String[] args) {
		new LongestCommonSubsequence().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
