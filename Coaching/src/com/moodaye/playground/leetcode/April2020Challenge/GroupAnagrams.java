package com.moodaye.playground.leetcode.April2020Challenge;

import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

// Problem 
public class GroupAnagrams {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		String[] strs = new String[n];
		for(int i = 0; i < n; i++) {
			strs[i] = in.next();
		}
		out.println(groupAnagrams(strs));
	}
	
	public List<List<String>> groupAnagrams(String[] strs){
		Map<String, List<String>> list = new HashMap<>();
		for(String word : strs) {
			char[] wordCh = word.toCharArray();
			Arrays.sort(wordCh);
			String normalizedStr = String.valueOf(wordCh);
			if(list.containsKey(normalizedStr)) {
				list.get(normalizedStr).add(word);
			}
			else {
				List<String> currList = new LinkedList<>();
				currList.add(word);
				list.put(normalizedStr, currList);
			}
		}
		Collection<List<String>> v =  list.values();
		List<List<String>> x = new LinkedList<>();
		for(List<String> li : v) {
			x.add(li);
		}
		return x;
	}

	public static void main(String[] args) {
		new GroupAnagrams().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
