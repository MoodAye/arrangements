package com.moodaye.playground.leetcode.April2020Challenge;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

/* Problem LeetCode April Challenge
 * TLE - expected this - since this is brute force O(n^2)
 * There probably is a dp solution along the following lines:
 * The max for current cell is dependent on prior cell max length
 * and the state of the cell prior to that length.
 */
public class ContiguousArray {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int[] nums = new int[n];
		for (int i = 0; i < nums.length; i++) {
			nums[i] = in.nextInt();
		}
		out.println(findMaxLength2(nums));
		out.println(findMaxLength(nums));
	}

	/*
	 Test Case
	 8 0 1 1 0 1 1 1 0
	 * 
	 */
	public int findMaxLength(int[] nums) {
		int max = 0;
		int cnt0 = 0;
		int cnt1 = 0;
		for (int i = 0; i < nums.length; i++) {
			if(nums[i] == 0) {
				cnt0++;
			}
			else {
				cnt1++;
			}
			max = Math.max(max, Math.min(cnt0, cnt1));
		}
		return max * 2;
	}

	public int findMaxLength2(int[] nums) {
		int max = 0;
		for (int i = 0; i < nums.length - 1; i++) {
			int cnt0 = 0;
			int cnt1 = 0;
			if (nums[i] == 0) {
				cnt0++;
			} else {
				cnt1++;
			}
			for (int j = i + 1; j < nums.length; j++) {
				if (nums[j] == 0) {
					cnt0++;
				} else {
					cnt1++;
				}
				if (cnt0 == cnt1) {
					max = Math.max(max, j - i + 1);
				}
			}
		}
		return max;
	}

	public static void main(String[] args) {
		new ContiguousArray().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
