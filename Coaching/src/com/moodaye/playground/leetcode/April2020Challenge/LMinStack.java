package com.moodaye.playground.leetcode.April2020Challenge;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

// Problem 
public class LMinStack {
	void solve(Scanner in, PrintWriter out) {
		MinStack stack = new MinStack();
		while (true) {
			String command = in.next();
			if (command.equals("quit")) {
				stack.printStack();
				return;
			}
			if (command.equals("push")) {
				stack.push(in.nextInt());
			} else if (command.equals("pop")) {
				stack.pop();
			} else if (command.equals("top")) {
				System.out.println(stack.top());
			} else if (command.equals("min")) {
				System.out.println(stack.getMin());
			}
			stack.printStack();
		}

	}

	public static void main(String[] args) {
		new LMinStack().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}

}

class MinStack {
	Node minNode;
	Node head;

	public void printStack() {
		System.out.print("Insertion order: ");
		Node next = head;
		while (next != null) {
			System.out.print(next.val + "  ");
			next = next.prev;
		}
		System.out.println();

		System.out.print("Sort order: ");
		next = minNode;
		while (next != null) {
			System.out.print(next.val + "  ");
			next = next.nextHigher;
		}
	}

	public void push(int x) {
		Node node = new Node(x);

		// empty?
		if (head == null) {
			head = node;
			minNode = node;
			return;
		}

		// insertion order
		node.prev = head;
		head.next = node;
		head = node;

		// sort order
		Node next = minNode;
		Node prev = minNode;
		while (next != null && x >= next.val) {
			prev = next;
			next = next.nextHigher;
		}

		// at this point - if next is not null the either node is immediately lower than
		// next; or equal to next;
		if (next != null) {
			node.nextHigher = next;
			if (next.nextLower != null) {
				node.nextLower = next.nextLower;
				next.nextLower.nextHigher = node;
			} else {
				minNode = node;
			}
			next.nextLower = node;
		} else {
			// if next is null then node.val has to be >= prev.val and prev is the last node
			// in the sort order
			node.nextLower = prev;
			prev.nextHigher = node;
		}
	}

	public void pop() {
		if (head == null) {
			return;
		}

		if (minNode == head) {
			if (minNode.nextHigher != null) {
				minNode = minNode.nextHigher;
				minNode.nextLower = null;
			} else {
				minNode = null;
			}
		}

		if (head.nextLower != null) {
			head.nextLower.nextHigher = head.nextHigher;
		}
		if (head.nextHigher != null) {
			head.nextHigher.nextLower = head.nextLower;
		}
		head = head.prev;
		if (head != null) {
			head.next = null;
		}
	}

	/* will throw npe if empty */
	public int top() {
		return head.val;
	}

	public int getMin() {
		return minNode.val;
	}

	static class Node {
		Node next;
		Node prev;
		Node nextHigher;
		Node nextLower;
		int val;

		Node(int val) {
			this.val = val;
		}
	}
}