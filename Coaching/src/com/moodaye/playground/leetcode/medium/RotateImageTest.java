package com.moodaye.playground.leetcode.medium;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class RotateImageTest {

	@Test
	void test() {
		RotateImage ri = new RotateImage();
		
		int[][] a = {{1,2,3},{4,5,6},{7,8,9}};
		ri.rotate(a);
		int[][] expected = {{7,4,1}, {8,5,2}, {9,6,3}};
		assertArrayEquals(expected, a);
	}
	
	@Test
	void test2(){
		RotateImage ri = new RotateImage();
		
		int[][] a = {{5,1,9,11},{2,4,8,10},{13,3,6,7},{15,14,12,16}};
		ri.rotate(a);
		int[][] expected = {{15,13,2,5},{14,3,4,1},{12,6,8,9},{16,7,10,11}};
		assertArrayEquals(expected, a);
	}

}
