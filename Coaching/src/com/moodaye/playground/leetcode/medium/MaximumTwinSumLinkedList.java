package com.moodaye.playground.leetcode.medium;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

import com.moodaye.playground.leetcode.hard.ListNode;

// Problem lc 2130
public class MaximumTwinSumLinkedList {
	void solve(Scanner in, PrintWriter out) {
	}
	
	
	public static int pairSum(ListNode head) {
		int len = 1;
		ListNode next = head;
		while(next.next != null) {
			next = next.next;
			len++;
		}
		
		int a[] = new int[len];
		next = head;
		for(int i = 0; i < len; i++) {
			a[i] = next.val;
			next = next.next;
		}
		
		int max = Integer.MIN_VALUE;
		for(int i = 0; i < len/2; i++) {
			max = Math.max(max, a[i] + a[len - i - 1]);
		}
		
		if(len % 2 == 1) {
			max = Math.max(max, a[len / 2]);
		}
		
		return max;
	}

	// this gets tle
	public static int pairSum2(ListNode head) {
		
		int len = 1;
		ListNode next = head;
		while(next.next != null) {
			next = next.next;
			len++;
		}
		
		int max = Integer.MIN_VALUE;
		
		// 1 2 3 4
		for(int i = 0; i <= (len / 2 - 1); i++) {
			
			ListNode p1 = head;
			for(int j = 1; j <= i; j++) {
				p1 = p1.next;
			}
			
			ListNode p2 = head;
			for(int j = 1; j < len - i; j++) {
				p2 = p2.next;
			}
			
			max = Math.max(max, p1.val + p2.val);
		}
		
		// if a single middle node
		if(len % 2 == 1) {
			ListNode mid = head;
			for(int i = 1; i <= len / 2; i++) {
				mid = mid.next;
			}
			max = Math.max(max, mid.val);
		}
		
		return max;
	}

	public static void main(String[] args) {
		new MaximumTwinSumLinkedList().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
