package com.moodaye.playground.leetcode.medium;

public class Test111 {
	
	public static void main(String[] args) {
		int x = Integer.MAX_VALUE;
		int y = (1 << 31) - 1;
		
		if(x == y) System.out.println("EUREKA");
		else System.out.println("FAILED");
		
		System.out.printf("%d%n%d%n", Integer.MAX_VALUE, y);
	}
}
