package com.moodaye.playground.leetcode.medium;

// 20min lc557
public class ReverseWordsInStringIII {
	public String reverseWords(String s) {
		if (s.length() == 1)
			return s;

		char[] cs = s.toCharArray();

		int start = -2;
		int end = -1;

		int i = 0;

		while (i < cs.length) {
			if (cs[i] != ' ') {
				if (start < 0) {
					start = i;
				}
				else if(i == cs.length - 1) {
					doReverse(cs, start, i);
				}
			} else {
				if (start >= 0) {
					end = i - 1;
					doReverse(cs, start, end);
					start = -2;
				}
			}
			i++;
		}

		return String.valueOf(cs);
	}

	public void doReverse(char[] s, int start, int end) {
		if (start > end)
			return;

		char temp = s[start];
		s[start] = s[end];
		s[end] = temp;

		doReverse(s, start + 1, end - 1);
	}
}
