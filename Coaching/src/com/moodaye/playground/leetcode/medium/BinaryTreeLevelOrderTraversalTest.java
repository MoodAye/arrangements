package com.moodaye.playground.leetcode.medium;
import static com.moodaye.playground.leetcode.medium.BinaryTreeLevelOrderTraversal.levelOrder;

import java.util.List;

import org.junit.Test;
import static org.junit.Assert.*;

import com.moodaye.playground.leetcode.medium.BinaryTreeLevelOrderTraversal.TreeNode;

public class BinaryTreeLevelOrderTraversalTest {

	@Test
	public void test() {
		TreeNode root = new TreeNode(3);
		root.left = new TreeNode(9);
		root.right = new TreeNode(20);
		root.right.left = new TreeNode(15);
		root.right.right = new TreeNode(7);
		List<List<Integer>> list = levelOrder(root);
		
		assertEquals(3, list.size());
		assertEquals(1, list.get(0).size());
		assertEquals(2, list.get(1).size());
		assertEquals(2, list.get(2).size());
	}

}
