package com.moodaye.playground.leetcode.medium;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class LongestPalindromeTest {

	@Test
	void test() {
		LongestPalindrome lp = new LongestPalindrome();
		assertEquals(11, lp.longestPalindrome("aabbbccccdd"));
		assertEquals(1, lp.longestPalindrome("a"));
		assertEquals(1, lp.longestPalindrome("abcd"));
		assertEquals(3, lp.longestPalindrome("abb"));
		assertEquals(7, lp.longestPalindrome("iiitttwww")); // itwwwti
		
		assertEquals(7, lp.longestPalindrome("acgiiilnrstttvwwwhe"));
		
		// wiiiw  
	}

}
