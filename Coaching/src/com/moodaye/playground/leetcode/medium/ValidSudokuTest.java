package com.moodaye.playground.leetcode.medium;

import static org.junit.Assert.*;

import org.junit.Test;

import static com.moodaye.playground.leetcode.medium.ValidSudoku.*;

public class ValidSudokuTest {

	@Test
	public void testBasic() {
		char[][] sb1 = new char[9][9];

		sb1[0] = "53..7....".toCharArray();
		sb1[1] = "6..195...".toCharArray();
		sb1[2] = ".98....6.".toCharArray();
		sb1[3] = "8...6...3".toCharArray();
		sb1[4] = "4..8.3..1".toCharArray();
		sb1[5] = "7...2...6".toCharArray();
		sb1[6] = ".6....28.".toCharArray();
		sb1[7] = "...419..5".toCharArray();
		sb1[8] = "....8..79".toCharArray();

		assertTrue(isValidSudoku(sb1));
	}

	@Test
	public void testBasic2() {
		char[][] sb1 = new char[9][9];

		sb1[0] = "53..5....".toCharArray();
		sb1[1] = "6..195...".toCharArray();
		sb1[2] = ".98....6.".toCharArray();
		sb1[3] = "8...6...3".toCharArray();
		sb1[4] = "4..8.3..1".toCharArray();
		sb1[5] = "7...2...6".toCharArray();
		sb1[6] = ".6....28.".toCharArray();
		sb1[7] = "...419..5".toCharArray();
		sb1[8] = "....8..79".toCharArray();

		assertFalse(isValidSudoku(sb1));
	}
	
	@Test
	public void testBasic3() {
		char[][] sb1 = new char[9][9];

		sb1[0] = "83..7....".toCharArray();
		sb1[1] = "6..195...".toCharArray();
		sb1[2] = ".98....6.".toCharArray();
		sb1[3] = "8...6...3".toCharArray();
		sb1[4] = "4..8.3..1".toCharArray();
		sb1[5] = "7...2...6".toCharArray();
		sb1[6] = ".6....28.".toCharArray();
		sb1[7] = "...419..5".toCharArray();
		sb1[8] = "....8..79".toCharArray();
		
		assertFalse(isValidSudoku(sb1));
	}
	
	@Test
	public void testBasic4() {
		char[][] sb1 = new char[9][9];

		sb1[0] = "53..7....".toCharArray();
		sb1[1] = "6..195...".toCharArray();
		sb1[2] = ".98....6.".toCharArray();
		sb1[3] = "8...6.5.3".toCharArray();
		sb1[4] = "4..8.3.51".toCharArray();
		sb1[5] = "7...2...6".toCharArray();
		sb1[6] = ".6....28.".toCharArray();
		sb1[7] = "...419..5".toCharArray();
		sb1[8] = "....8..79".toCharArray();
		
		assertFalse(isValidSudoku(sb1));
	}
}
