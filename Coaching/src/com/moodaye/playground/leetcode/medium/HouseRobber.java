package com.moodaye.playground.leetcode.medium;

//lc
public class HouseRobber {
	   public int rob(int[] nums) {
		   if(nums.length == 1) {
			   return nums[0];
		   }
		   
	       int max = Math.max(nums[0], nums[1]);
		   
	       int prevMax = max;
	       int prevPrevMax = nums[0];
	       
	       for(int i = 2; i < nums.length; i++) {
	    	   int currMax = Math.max(nums[i] + prevPrevMax, prevMax);
	    	   max = Math.max(currMax, max);
	    	   prevPrevMax = prevMax;
	    	   prevMax = currMax;
	       }
	       return max;
	    }
}
