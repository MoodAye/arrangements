package com.moodaye.playground.leetcode.medium;

public class LongestPalindrome {
	public int longestPalindrome(String s) {
		int[] cntr = new int[100];
		for (char c : s.toCharArray()) {
			cntr[c - 'A']++;
		}

		int longest = 0;
		boolean odd = false;

		for (int i = 0; i < cntr.length; i++) {
			longest += cntr[i];
			if(cntr[i] % 2 == 1) {
				odd = true;
				longest--;
			}

		}

		return longest + (odd ? 1 : 0);
	}
}
