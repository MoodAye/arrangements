package com.moodaye.playground.leetcode.medium;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

// Problem lc
public class ValidSudoku {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int[] nums = new int[n];
		for (int i = 0; i < nums.length; i++) {
			nums[i] = in.nextInt();
		}
	}

	public static boolean isValidSudoku(char[][] board) {
		// check rows
		for (int r = 0; r < 9; r++) {
			int[] cntr = new int[256];
			for (int c = 0; c < 9; c++) {
				if ('1' <= board[r][c] && board[r][c] <= '9')
					cntr[board[r][c]]++;
				if (cntr[board[r][c]] > 1) {
					return false;
				}
			}
		}

		// check columns
		for (int c = 0; c < 9; c++) {
			int[] cntr = new int[256];
			for (int r = 0; r < 9; r++) {
				if ('1' <= board[r][c] && board[r][c] <= '9')
					cntr[board[r][c]]++;
				if (cntr[board[r][c]] > 1) {
					return false;
				}
			}

		}

		// check squares
		for (int topLefty = 0; topLefty < 9 - 2; topLefty += 3) {
			for (int topLeftx = 0; topLeftx < 9 - 2; topLeftx += 3) {
				int[] cntr = new int[256];
				for (int i = topLefty; i < topLefty + 3; i++ ) {
					for (int j = topLeftx; j < topLeftx + 3; j++) {
						if ('1' <= board[i][j] && board[i][j] <= '9') {
							cntr[board[i][j]]++;
						}
						if((cntr[board[i][j]]) > 1){
							return false;
						}
					}
				}
			}
		}
		return true;
	}

	public static void main(String[] args) {
		new ValidSudoku().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
