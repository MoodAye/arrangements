package com.moodaye.playground.leetcode.medium;


class MultiplyStrings {
	  public String multiply(String num1, String num2) {
		  if(num1.equals("0") || num2.equals("0")) {
			  return "0";
		  }
		  
		  int zero = 0;
		  String prod = null;
		  
		  String ans = "0";
		  for(int i = num2.length() - 1; i >= 0; i--) {
			  prod = m(num1, num2.charAt(i), zero++);
			  
			  ans = add(ans, prod);
		  }
		  
		  return ans;
	    }
	  
	  public String add(String s1, String s2) {
		  s1 = new StringBuilder().append(s1).reverse().toString();
		  s2 = new StringBuilder().append(s2).reverse().toString();
		  
		  char[] c1 = s1.toCharArray();
		  char[] c2 = s2.toCharArray();
		  
		  if(c1.length < c2.length) {
			  char[] temp = c1;
			  c1 = c2;
			  c2 = temp;
		  }
		  
		  int carry = 0;
		  
		  int i = 0;
		  for(; i < c2.length; i++) {
			  int dsum = c1[i] - '0' + c2[i] - '0' + carry;
			  c1[i] = (char) ('0' + dsum % 10);
			  carry = dsum / 10;
		  }
		  
		  while(i < c1.length && carry > 0) {
			  int dsum = c1[i] - '0' + carry;
			  c1[i] = (char) ('0' + dsum % 10);
			  carry = dsum / 10;
			  i++;
		  }
		  
		  StringBuilder ans = new StringBuilder();
		  ans.append(c1);
		  
		  if(carry > 0) {
			  ans.append(carry);
		  }
		  
		  return ans.reverse().toString();
	  }
	    
	    public String m(String s, char c, int zeros){
	        StringBuilder sb = new StringBuilder();
	        int carry = 0;        

	        while(zeros-- > 0) sb.append("0");
	        
	        for(int i = s.length() - 1; i >= 0; i--){
	            char nextC = s.charAt(i);
	            
	            int prod = (int) (nextC - '0') * (c - '0');
	            prod += carry;
	            
	            sb.append(prod % 10);
	            carry = prod / 10;
	        }
	        
	        if(carry > 0) sb.append(carry);
	        return sb.reverse().toString();
	    }

}
