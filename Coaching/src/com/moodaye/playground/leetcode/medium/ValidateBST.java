package com.moodaye.playground.leetcode.medium;

import com.moodaye.playground.leetcode.easy.BSTPostOrderTraversal.TreeNode;

public class ValidateBST {
	  public boolean isValidBST(TreeNode root) {
	       return isValidBST(root, Integer.MIN_VALUE, Integer.MAX_VALUE);
	   }

	  public boolean isValidBST(TreeNode root, int min, int max) {
		    if(root.val < min) return false;
		  if(root.val > max) return false;
		  
		  boolean left = true;
		  if(root.left != null) {
            if(root.val == Integer.MIN_VALUE) return false;
			  left = isValidBST(root.left, min, root.val - 1);
		  }
		  
		  boolean right = true;
		  if(root.right != null) {
            if(root.val == Integer.MAX_VALUE) return false;
			  right = isValidBST(root.right, root.val + 1, max);
		  }
		  
		  return left && right;
	  }
}
