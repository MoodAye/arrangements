package com.moodaye.playground.leetcode.medium;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class ReverseWordsInStringIIITest {

	@Test
	void testOneWord() {
		ReverseWordsInStringIII rvs = new ReverseWordsInStringIII();
		String s = "This";
		String expected = "sihT";
		
		assertEquals(expected, rvs.reverseWords(s));
		assertEquals("a", rvs.reverseWords("a"));
		assertEquals("ab", rvs.reverseWords("ba"));
		assertEquals("ab ba", rvs.reverseWords("ba ab"));
		assertEquals("ab ba ", rvs.reverseWords("ba ab "));
	}
		
	@Test
	void test() {
		ReverseWordsInStringIII rvs = new ReverseWordsInStringIII();
		String s = "This is a test";
		String expected = "sihT si a tset";
		
		assertEquals(expected, rvs.reverseWords(s));
	}

}
