package com.moodaye.playground.leetcode.medium;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

// 438pm - 451pm
public class GroupAnagrams {
	 public List<List<String>> groupAnagrams(String[] strs) {
		 String[] strsSorted = new String[strs.length];
		 
		 for(int i = 0; i < strs.length; i++) {
			 char[] cs = strs[i].toCharArray();
			 Arrays.sort(cs);
			 strsSorted[i] = String.valueOf(cs);
		 }
		 
		 List<List<String>> ans = new ArrayList<>();
		 boolean[] matched = new boolean[strs.length];
		 
		 for(int i = 0; i < strs.length; i++) {
			 if(matched[i]) {
				 continue;
			 }
			 List<String> item = new ArrayList<>();
			 item.add(strs[i]);
			 matched[i] = true;
			for(int j = i + 1; j < strs.length; j++) {
				if(strsSorted[i].equals(strsSorted[j])) {
					item.add(strs[j]);
					matched[j] = true;
				}
			}
			ans.add(item);
		 }
	 
		 return ans;
	 }
}
