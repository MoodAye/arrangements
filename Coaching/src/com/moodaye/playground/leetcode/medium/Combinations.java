package com.moodaye.playground.leetcode.medium;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

// 1155pm
public class Combinations {
	
	public List<List<Integer>> combineBit(int n, int k){
		List<List<Integer>> ans = new ArrayList<>();
		
		List<Integer> nums = new LinkedList<>();
		
		for(int i = 1; i <= k; i++) {
			nums.add(i);
		}
		nums.add(n + 1);
		
		
		int j = 0;
		while(j < k) {
			ans.add(new LinkedList(nums.subList(0, k)));
			j = 0;
			while((j < k) && nums.get(j) + 1 == nums.get(j + 1)) {
				nums.set(j, j++ + 1);
			}
			nums.set(j, nums.get(j) + 1);
		}
		
		return ans;
	}
	
	
	
	public List<List<Integer>> combineBt(int n, int k) {
		List<List<Integer>> combos = new LinkedList<>();
		combineBt(n, k, 0, 1, new LinkedList<>(), combos);
		return combos;
    }
	
	public void combineBt(int n, int k, int idx, int start, 
			List<Integer> curr, List<List<Integer>> combos) {
		
		if(idx == k) {
			combos.add(new LinkedList<>(curr));
			return;
		}
		
		for(int i = start; i <= idx + 1 + (n - k); i++) {
			curr.add(idx, i);
			combineBt(n, k, idx + 1, i + 1, curr, combos);
			curr.remove(idx);
		}
	}
}
