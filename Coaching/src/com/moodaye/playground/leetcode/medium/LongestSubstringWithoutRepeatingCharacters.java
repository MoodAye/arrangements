package com.moodaye.playground.leetcode.medium;

// 1027am
public class LongestSubstringWithoutRepeatingCharacters {

	public int lengthOfLongestSubstring(String s) {
		if (s == null || s.length() == 0) {
			return 0;
		}

		char[] cs = s.toCharArray();
		int max = 1;
		int prevMax = 1;

		for (int i = 0; i < cs.length; i++) {
			for (int j = i - 1; j >= 0 && j >= i - prevMax ; j--) {
				if (cs[i] == cs[j]) {
					max = Math.max(max, i - j);
					prevMax = i - j;
					break;
				}
				if(j == 0 || j == i - prevMax) {
					max = Math.max(max, i - j + 1);
					prevMax = i - j + 1;
				}
			}
		}

		return max;
	}

}
