package com.moodaye.playground.leetcode.medium;

import java.util.ArrayDeque;
import java.util.Collections;
import java.util.Deque;
import java.util.HashMap;
import java.util.Map;

//lc 845pm - 902pm
public class MaxAreaOfIsland {
	
	int islandNumber = 1;
	
	public int maxAreaOfIsland(int[][] grid) {
		
		for(int r = 0; r < grid.length; r++) {
			for(int c = 0; c < grid[0].length; c++) {
				if(grid[r][c] == 1) {
					labelIsland(grid, r, c, ++islandNumber);
				}
			}
		}
		
		Map<Integer, Integer> cnts = new HashMap<>();
		for(int r = 0; r < grid.length; r++) {
			for(int c = 0; c < grid[0].length; c++) {
				if(grid[r][c] > 1) {
					cnts.merge(grid[r][c], 1, (a,b) -> a + 1);
				}
			}
		}
		
		return cnts.isEmpty() ? 0 : Collections.max(cnts.values());
	}
	
	public void labelIsland(int[][] grid, int r, int c, int id) {
	
		Deque <Point> stack = new ArrayDeque<>();
		grid[r][c] = id;
		stack.push(new Point(r,c));
		
		while(!stack.isEmpty()) {
			Point next = stack.pop();
			
			// visit above
			if(next.x != 0 && grid[next.x - 1][next.y] == 1) {
					grid[next.x - 1][next.y] = id;
					stack.push(new Point(next.x -1, next.y));
			}
			// visit right
			if(next.y != grid[0].length - 1 && grid[next.x][next.y + 1] == 1) {
					grid[next.x][next.y + 1] = id;
					stack.push(new Point(next.x, next.y + 1));
			}
			// visit below
			if(next.x != grid.length - 1 && grid[next.x + 1][next.y] == 1) {
					grid[next.x + 1][next.y] = id;
					stack.push(new Point(next.x + 1, next.y));
			}
			// visit left
			if(next.y != 0 && grid[next.x][next.y - 1] == 1) {
					grid[next.x][next.y - 1] = id;
					stack.push(new Point(next.x, next.y - 1));
			}
		}
	}
	
	
	
	
	static class Point{
		int x;
		int y;
		boolean visited = false;
		int cnt = 1;
		
		Point(int x, int y){
			this.x = x;
			this.y = y;
		}
	}
	
	
}