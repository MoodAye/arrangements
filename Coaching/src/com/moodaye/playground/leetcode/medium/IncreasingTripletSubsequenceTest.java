package com.moodaye.playground.leetcode.medium;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class IncreasingTripletSubsequenceTest {

	@Test
	void test() {
		IncreasingTripletSubsequence its = new IncreasingTripletSubsequence();
		int[] a = {1,2,3};
		assertTrue(its.increasingTriplet(a));
		
		int[] b = {1,4,3};
		assertFalse(its.increasingTriplet(b));
		
		int[] c = {1,2,3,4,5};
		assertTrue(its.increasingTriplet(c));
		
		int[] d = {5,4,3,2,1};
		assertFalse(its.increasingTriplet(d));
		
		int[] e = {2,1,5,0,4,6};
		assertTrue(its.increasingTriplet(e));
		
		int[] f = {2};
		assertFalse(its.increasingTriplet(f));
		
		int[] g = {2, 6};
		assertFalse(its.increasingTriplet(g));
		
		int[] h = {1,1,0,6};
		assertFalse(its.increasingTriplet(h));
		
	}

}
