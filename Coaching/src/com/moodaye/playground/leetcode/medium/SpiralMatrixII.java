package com.moodaye.playground.leetcode.medium;

// 857pm
public class SpiralMatrixII {
	
	 public int[][] generateMatrix(int n) {
		 int[][] matrix = new int[n][n];
		 
		 int next = 1;
		 for(int r = 0; r <= n / 2; r++) {
			 
			 // top row ... 0,[c ...]
			 for(int c = r; c <= n - r - 1; c++) {
				matrix[r][c] = next++; 
			 }
			 
			 // right col - starting with row r + 1
			 for(int rowsGoingDown = r + 1; rowsGoingDown <= n - r - 1; rowsGoingDown++) {
				matrix[rowsGoingDown][n - r - 1] = next++; 
			 }
			 
			 // bottom row
			 for(int c = n - r - 2; c >= r; c--) {
				matrix[n - r - 1][c] = next++; 
			 }
			 
			 // right col
			 for(int rowsGoingUp = n - r - 2; rowsGoingUp > r; rowsGoingUp--) {
				matrix[rowsGoingUp][r] = next++; 
			 }
			 
		 }
		 return matrix;
	 }
}
/*
 * n,n
 * 0,0-------->0,n-1
 * 0,n-1----->n-1,n-1
 * n-1,n-1-->n-1,0
 * n-1,0---->0,0
 * 
 *   10  11  12  13
 *   14  15  16  17
 *   18  19  20  21
 *   22  23  24  25
 *   
 *   
 *   22  18  14  10
 *   23  19  15  11  
 *   24  20  16  12
 *   25  21  17  13
 *   
 * 
 * 
 */
