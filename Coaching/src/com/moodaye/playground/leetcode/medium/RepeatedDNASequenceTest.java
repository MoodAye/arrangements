package com.moodaye.playground.leetcode.medium;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import org.junit.jupiter.api.Test;

class RepeatedDNASequenceTest {

	@Test
	void test() {
		RepeatedDNASequence rds = new RepeatedDNASequence();
		List<String> expected = List.of("AAAAAAAAAA", "BBBBBBBBBB");
//		assertEquals(expected, rds.findRepeatedDnaSequences("AAAAAAAAAAxBBBBBBBBBByAAAAAAAAAAzBBBBBBBBBB"));
		expected = List.of("AAAAAAAAAA");
		assertEquals(expected, rds.findRepeatedDnaSequences("AAAAAAAAAAA"));
	}

}
