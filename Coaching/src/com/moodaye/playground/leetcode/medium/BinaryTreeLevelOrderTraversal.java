package com.moodaye.playground.leetcode.medium;

import java.util.ArrayDeque;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

// lc 102
public class BinaryTreeLevelOrderTraversal {
	static class TreeNode {
		int val;
		TreeNode left;
		TreeNode right;

		TreeNode() {
		}

		TreeNode(int val) {
			this.val = val;
		}

		TreeNode(int val, TreeNode left, TreeNode right) {
			this.val = val;
			this.left = left;
			this.right = right;
		}
	}

	public static List<List<Integer>> levelOrder(TreeNode root) {
		List<List<Integer>> list = new LinkedList<>();
		
		if (root == null)
			return list;
		
		Queue<TreeNode> currLevel = new ArrayDeque<>();
		Queue<TreeNode> nextLevel = new ArrayDeque<>();

		nextLevel.add(root);

		while (!nextLevel.isEmpty()) {
			
			Queue<TreeNode> temp = currLevel;
			currLevel = nextLevel;
			nextLevel = temp;
			
			List<Integer> level = new LinkedList<>();
			
			while (!currLevel.isEmpty()) {
				TreeNode node = currLevel.remove();
				level.add(node.val);
				if (node.left != null) nextLevel.add(node.left);
				if (node.right != null) nextLevel.add(node.right);
			}
			
			list.add(level);
		}

		return list;
	}

}
