package com.moodaye.playground.leetcode.medium;

import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Queue;

// lc 538pm
public class LRUCache {
	int capacity;
	Map<Integer, Integer> cache;
	Queue<Integer> evictQ;

	public LRUCache(int capacity) {
		this.cache = new HashMap<>();
		this.evictQ = new PriorityQueue<>();
		this.capacity = capacity;
	}

	public int get(int key) {
		if(cache.containsKey(key)) {
			return cache.get(key);
		}
		return -1;
	}

	public void put(int key, int value) {
		if(cache.containsKey(key)) {
			cache.put(key, value);
		}
		else if(cache.size() == capacity) {
			evict();
			cache.put(key, value);
		}
		capacity++;
	}
	
	private void evict() {
		int evictKey = evictQ.remove();
		cache.remove(evictKey);
		capacity--;
	}
}
