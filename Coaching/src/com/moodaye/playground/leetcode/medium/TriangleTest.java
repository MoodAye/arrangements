package com.moodaye.playground.leetcode.medium;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;

class TriangleTest {

	@Test
	void test() {
		Triangle t1 = new Triangle();
		List<List<Integer>> list1 = new ArrayList<>();
		list1.add(Arrays.asList(1));
		list1.add(Arrays.asList(2,3));
		list1.add(Arrays.asList(4,5,6));
		
	   	assertEquals(7,t1.minimumTotal(list1));
		
		List<List<Integer>> list2 = new ArrayList<>();
		list2.add(Arrays.asList(1));
		list2.add(Arrays.asList(2,1));
		list2.add(Arrays.asList(4,5,1));
	   	assertEquals(3,t1.minimumTotal(list2));
		
		List<List<Integer>> list3 = new ArrayList<>();
		list3.add(Arrays.asList(1));
		list3.add(Arrays.asList(-1,1));
		list3.add(Arrays.asList(-1,5,1));
	   	assertEquals(-1,t1.minimumTotal(list3));
		
	}

}
