package com.moodaye.playground.leetcode.medium;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import org.junit.jupiter.api.Test;

class PartitionLabelsTest {

	@Test
	void test() {
		PartitionLabels pl = new PartitionLabels();
		
		List<Integer> ans = List.of(1,1,1);
		assertEquals(ans, pl.partitionLabels("abc"));
		
		List<Integer> ans4 = List.of(4);
		assertEquals(ans4, pl.partitionLabels("abca"));
		
		List<Integer> ans2 = List.of(9,7,8);
		assertEquals(ans2, pl.partitionLabels("ababcbacadefegdehijhklij"));
		
		List<Integer> ans3 = List.of(10);
		assertEquals(ans3, pl.partitionLabels("eccbbbbdec"));
		
		List<Integer> ans5 = List.of(1);
		assertEquals(ans5, pl.partitionLabels("e"));
		
		List<Integer> ans6 = List.of(24);
		assertEquals(ans6, pl.partitionLabels("ababcbacadefegdehijhklia"));
	}

}
