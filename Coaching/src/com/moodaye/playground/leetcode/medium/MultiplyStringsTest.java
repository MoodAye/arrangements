package com.moodaye.playground.leetcode.medium;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class MultiplyStringsTest {

	@Test
	void testM() {
		MultiplyStrings ms = new MultiplyStrings();
		
		assertEquals("12", ms.m("6", '2', 0));
		assertEquals("1800", ms.m("600", '3', 0));
		assertEquals("24", ms.m("12", '2', 0));
		assertEquals("240", ms.m("24", '1', 1));
		assertEquals("1107", ms.m("123", '9', 0));
		
	}
	
	@Test
	void testMult1() {
		MultiplyStrings ms = new MultiplyStrings();
		
		assertEquals("12", ms.multiply("6", "2"));
		assertEquals("1800", ms.multiply("600", "3"));
		assertEquals("6", ms.multiply("2", "3"));
		assertEquals("144", ms.multiply("12", "12"));
		assertEquals(String.valueOf(123 * 456), ms.multiply("123", "456"));
		assertEquals("0", ms.multiply("123", "0"));
		assertEquals(String.valueOf(2 * 9), ms.multiply("2", "9"));
		assertEquals(String.valueOf(99 * 99), ms.multiply("99", "99"));
		assertEquals(String.valueOf(12 * 98), ms.multiply("12", "98"));
		assertEquals("121401", ms.multiply("123", "987"));
		assertEquals("121932631112635269", ms.multiply("123456789", "987654321"));
	}
	
	@Test
	void testAdd() {
		MultiplyStrings ms = new MultiplyStrings();
		
		assertEquals("8", ms.add("6", "2"));
		assertEquals("60", ms.add("20", "40"));
		assertEquals("142", ms.add("140", "2"));
		assertEquals(String.valueOf(861+9840), ms.add("861", "9840"));
	}
}