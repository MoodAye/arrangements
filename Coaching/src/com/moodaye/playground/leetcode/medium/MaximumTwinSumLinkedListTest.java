package com.moodaye.playground.leetcode.medium;

import static com.moodaye.playground.leetcode.medium.MaximumTwinSumLinkedList.pairSum;
import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.moodaye.playground.leetcode.hard.ListNode;

public class MaximumTwinSumLinkedListTest {

	@Test
	public void test1() {
		ListNode head = new ListNode(5);
		ListNode n2 = new ListNode(4);
		ListNode n3 = new ListNode(2);
		ListNode n4 = new ListNode(1);
		
		head.next = n2;
		n2.next = n3;
		n3.next = n4;
		
		assertEquals(6, pairSum(head));
	}

	@Test
	public void test2() {
		ListNode head = new ListNode(4);
		ListNode n2 = new ListNode(2);
		ListNode n3 = new ListNode(2);
		ListNode n4 = new ListNode(3);
		
		head.next = n2;
		n2.next = n3;
		n3.next = n4;
		
		assertEquals(7, pairSum(head));
	}
	
	
	@Test
	public void test3() {
		ListNode head = new ListNode(1);
		ListNode n2 = new ListNode(100_000);
		
		head.next = n2;
		
		assertEquals(100_001, pairSum(head));
	}
	
	@Test
	public void test4() {
		ListNode head = new ListNode(5);
		ListNode n2 = new ListNode(40);
		ListNode n3 = new ListNode(2000);
		ListNode n4 = new ListNode(1);
		ListNode n5 = new ListNode(1);
		
		head.next = n2;
		n2.next = n3;
		n3.next = n4;
		n4.next = n5;
		
		assertEquals(2000, pairSum(head));
	}
	
	@Test
	public void test5() {
		ListNode head = new ListNode(5);
		ListNode n2 = new ListNode(40);
		ListNode n3 = new ListNode(2);
		ListNode n4 = new ListNode(1);
		
		head.next = n2;
		n2.next = n3;
		n3.next = n4;
		
		assertEquals(42, pairSum(head));
	}
	
	@Test
	public void test6() {
		ListNode head = new ListNode(5);
		assertEquals(5, pairSum(head));
	}
}
