package com.moodaye.playground.leetcode.medium;

import java.util.ArrayList;
import java.util.List;

public class PartitionLabels {

	public List<Integer> partitionLabels(String s) {
		char[] c = s.toCharArray();
		int[] pnum = new int[s.length()];

		pnum[0] = 1;
		int nextP = 2;

		for (int i = 1; i < s.length(); i++) {
			boolean hasOccured = false;
			for (int j = i - 1; j >= 0; j--) {
				if (c[i] == c[j]) {
					int currP = pnum[j];
					for (int k = j + 1; k <= i; k++) {
						pnum[k] = currP;
					}
					hasOccured = true;
					break;
				}
			}
			if (!hasOccured) {
				pnum[i] = nextP++;
			}
		}

		int startIdx = 0;
		int currPNum = pnum[0];

		List<Integer> ans = new ArrayList<>();

		for (int i = 1; i < pnum.length; i++) {
			if (pnum[i] == currPNum) {
				continue;
			} else {
				ans.add(i - startIdx);
				startIdx = i;
				currPNum = pnum[i];
			}
		}

		ans.add(pnum.length - startIdx);
		return ans;
	}
}
