package com.moodaye.playground.leetcode.medium;

import java.util.Arrays;

//lc 567 1233am - 1:02
public class PermutationInString {
	 public boolean checkInclusion(String s1, String s2) {
		 if(s2.length() < s1.length()) return false;
		 
		 char[] cs1 = s1.toCharArray();
		 char[] cs2 = s2.toCharArray();
		 
		 int[] s1Cntr = new int[26];
		 for(char c : cs1) {
			s1Cntr[c - 'a']++;
		 }
		
		 // compare the first substring of len = s1.len
		 for(int i = 0; i < cs1.length; i++) {
			 s1Cntr[cs2[i] - 'a']--;
		 }
		 
		 if(checkPerm(s1Cntr)) return true;
		 
		 for(int i = cs1.length; i < cs2.length; i++) {
			 s1Cntr[cs2[i] - 'a']--;
			 s1Cntr[cs2[i - cs1.length] - 'a']++;
			 if(checkPerm(s1Cntr)) return true;
		 }
		 
		 return false;
	 }
	 
	 private boolean checkPerm(int[] s1Cntr) {
		 for(int i : s1Cntr) {
			 if(i != 0) {
				 return false;
			 }
		 }
		 return true;
	 }
}
