package com.moodaye.playground.leetcode.medium;

// 
public class Matrix {
	public int[][] updateMatrix(int[][] mat) {
		
		boolean hasChanged = true;
		int currLen = 1;
		
		while(hasChanged) {
			
			hasChanged = false;
			
			for(int r = 0; r < mat.length; r++) {
				for(int c = 0; c < mat[0].length; c++) {
					if(mat[r][c] == currLen){
						mat[r][c] = getLen(mat, r, c, currLen);
					}
				}
			}
			
			
		}
		
		
		
		return mat;
	}
	
	private int getLen(int[][] mat, int r, int c, int currLen) {
		int len = 0;
		int rowCombos[] = new int[2];
		if(r != 0) rowCombos[0] = -1;
		if(r != mat.length) rowCombos[0] = +1;
		
		int colCombos[] = new int[2];
		if(c != 0) colCombos[0] = -1;
		if(c != mat[0].length - 1) colCombos[0] = +1;
		
		return len;
		
	}
}
