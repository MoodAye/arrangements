package com.moodaye.playground.leetcode.medium;

import java.util.Arrays;

// lc 56 945am
public class MergeIntervals {
	public int[][] merge(int[][] intervals) {
		int[][] merged = new int[intervals.length][2];
		Arrays.sort(intervals, (a, b) -> a[0] - b[0]);
		
		merged[0][0] = intervals[0][0];
		merged[0][1] = intervals[0][1];
		int cnt = 1;
		
		for(int[] interval : intervals) {
			if(interval[0] > merged[cnt - 1][1]) {
				merged[cnt][0] = interval[0];
				merged[cnt][1] = interval[1];
				cnt++;
			}
			else if(interval[1] > merged[cnt - 1][1]) {
				merged[cnt - 1][1] = interval[1];
			}
		}
		
		int[][] ans = new int[cnt][2];
		
		for(int i = 0; i < cnt; i++) {
			ans[i][0] = merged[i][0];
			ans[i][1] = merged[i][1];
		}
		return ans;
	}
}
