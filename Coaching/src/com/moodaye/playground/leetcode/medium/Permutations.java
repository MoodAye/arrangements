package com.moodaye.playground.leetcode.medium;

import java.io.PrintWriter;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

// Problem 
public class Permutations {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int[] nums = new int[n];
		for (int i = 0; i < nums.length; i++) {
			nums[i] = in.nextInt();
		}
	}
	
	public List<List<Integer>> permute(int[] nums){
		
		return null;
	}
	
	public static void main(String[] args) {
		new Permutations().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
