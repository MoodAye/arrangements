package com.moodaye.playground.leetcode.medium;

public class IncreasingTripletSubsequence {
	 public boolean increasingTriplet(int[] nums) {
		 int e1 = nums[0];
		 int e2 = 0;
		 boolean isE2 = false;
		 
		 for(int i = 1; i < nums.length; i++) {
			 if(nums[i] == e1) continue;
			 if(isE2 && nums[i] == e2) continue;
			 if(isE2 && nums[i] > e2) {
				 return true;
			 }
			 else if(nums[i] < e1) {
				 e1 = nums[i];
			 }
			 else if(isE2 && nums[i] < e2) {
				 e2 = nums[i];
			 }
			 else if (!isE2) {
				 e2 = nums[i];
				 isE2 = true;
			 }
		 }
		 
		 return false;
	    }
}
