package com.moodaye.playground.leetcode.medium;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class PermutationInStringTest {

	@Test
	void test() {
		String s1 = "ab";
		String s2 = "dbas";
		assertTrue(new PermutationInString().checkInclusion(s1, s2));
	}

}
