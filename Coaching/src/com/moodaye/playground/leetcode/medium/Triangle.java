package com.moodaye.playground.leetcode.medium;

import java.util.List;

//lc
public class Triangle {
	public int minimumTotal(List<List<Integer>> triangle) {
		if(triangle.size() == 1) {
			return (triangle.get(0)).get(0);
		}
		
		List<Integer> prevRow = triangle.get(0);
		List<Integer> currRow = null;
		for(int i = 1; i < triangle.size(); i++) {
			currRow = triangle.get(i);
			for(int j = 0; j < currRow.size(); j++) {
				if(j == 0) {
					currRow.set(j, currRow.get(j) + prevRow.get(0));
				}
				else if(j == currRow.size() - 1) {
					currRow.set(j, currRow.get(j) + prevRow.get(j - 1));
				}
				else {
					currRow.set(j, currRow.get(j) + 
							Math.min(prevRow.get(j - 1), prevRow.get(j)));
				}
			}
			prevRow = currRow;
		}
		
		return currRow.stream().min(Integer::compare).get();
	}
}