package com.moodaye.playground.leetcode.medium;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class LongestSubstringWithoutRepeatingCharactersTest {

	@Test
	void test() {
		LongestSubstringWithoutRepeatingCharacters ls = new LongestSubstringWithoutRepeatingCharacters();
		
		assertEquals(0, ls.lengthOfLongestSubstring(""));
		assertEquals(0, ls.lengthOfLongestSubstring(null));
		assertEquals(1, ls.lengthOfLongestSubstring("a"));
		assertEquals(1, ls.lengthOfLongestSubstring("aa"));
		assertEquals(2, ls.lengthOfLongestSubstring("aab"));
		assertEquals(1, ls.lengthOfLongestSubstring("abb"));
		assertEquals(3, ls.lengthOfLongestSubstring("abcabcbb"));
		assertEquals(1, ls.lengthOfLongestSubstring("bbbbb"));
		assertEquals(3, ls.lengthOfLongestSubstring("pwwkew"));
		assertEquals(2, ls.lengthOfLongestSubstring("ke"));
		assertEquals(6, ls.lengthOfLongestSubstring("pwwkewalr"));
		assertEquals(7, ls.lengthOfLongestSubstring("kewalrpwwkew"));
		assertEquals(6, ls.lengthOfLongestSubstring("pwkewalrawkew"));
	}

}