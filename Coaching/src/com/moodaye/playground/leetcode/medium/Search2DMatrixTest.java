package com.moodaye.playground.leetcode.medium;

import static org.junit.Assert.*;

import org.junit.Test;
import static com.moodaye.playground.leetcode.medium.Search2DMatrix.*;

public class Search2DMatrixTest {

	@Test
	public void testBasic() {
		Search2DMatrix sd = new Search2DMatrix();
		int[][] a = { { 1, 2 }, { 3, 4 }, { 5, 6 } };
		assertTrue(sd.searchMatrix(a, 1));
		assertTrue(sd.searchMatrix(a, 2));
		assertTrue(sd.searchMatrix(a, 3));
		assertTrue(sd.searchMatrix(a, 4));
		assertTrue(sd.searchMatrix(a, 5));
		assertTrue(sd.searchMatrix(a, 6));
		assertFalse(sd.searchMatrix(a, 0));
		assertFalse(sd.searchMatrix(a, 7));
		assertFalse(sd.searchMatrix(a, 8));
	}

	@Test
	public void testBasic2() {
		Search2DMatrix sd = new Search2DMatrix();
		int[][] a = { { 10, 20 }, { 30, 40 }, { 50, 60 } };
		assertTrue(sd.searchMatrix(a, 20));
		assertFalse(sd.searchMatrix(a, 8));
	}

	@Test
	public void testSingleCell() {
		Search2DMatrix sd = new Search2DMatrix();
		int[][] a = { { 10 } };
		assertTrue(sd.searchMatrix(a, 10));
		assertFalse(sd.searchMatrix(a, 20));
		assertFalse(sd.searchMatrix(a, 8));
	}

	@Test
	public void testSingleRow() {
		Search2DMatrix sd = new Search2DMatrix();
		int[][] a = { { 10, 20, 30, 40 } };
		assertTrue(sd.searchMatrix(a, 10));
		assertFalse(sd.searchMatrix(a, 24));
		assertFalse(sd.searchMatrix(a, 8));
		assertFalse(sd.searchMatrix(a, 0));
		assertFalse(sd.searchMatrix(a, 41));
	}

	@Test
	public void testSingleCol() {
		Search2DMatrix sd = new Search2DMatrix();
		int[][] a = { { 10 }, { 20 }, { 30 } };
		assertTrue(sd.searchMatrix(a, 10));
		assertTrue(sd.searchMatrix(a, 20));
		assertFalse(sd.searchMatrix(a, 8));
	}

	@Test
	public void testLeetCode1() {
		Search2DMatrix sd = new Search2DMatrix();
		int[][] a = { { 1, 3, 5, 7 }, { 10, 11, 16, 20 }, { 23, 30, 34, 60 } };
		assertTrue(sd.searchMatrix(a, 3));
	}

	@Test
	public void testLeetCode2() {
		Search2DMatrix sd = new Search2DMatrix();
		int[][] a = { { 1, 3, 5, 7 }, { 10, 11, 16, 20 }, { 23, 30, 34, 50 } };
		assertFalse(sd.searchMatrix(a, 6));

	}
	
	@Test
	public void testLeetCode3() {
		Search2DMatrix sd = new Search2DMatrix();
		int[][] a = { { 1, 4 }, {2, 5}};
		assertFalse(sd.searchMatrix(a, 6));
		assertTrue(sd.searchMatrix(a, 4));
		assertTrue(sd.searchMatrix(a, 2));
	}
}
