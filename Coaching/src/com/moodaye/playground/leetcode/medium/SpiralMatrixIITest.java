package com.moodaye.playground.leetcode.medium;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class SpiralMatrixIITest {

	@Test
	void test() {
		SpiralMatrixII sm = new SpiralMatrixII();
		
		int[][] expected2 = {{1,2},{4,3}};
		assertArrayEquals(expected2, sm.generateMatrix(2));
		
		int[][] expected3 = {{1,2,3},{8,9,4}, {7,6,5}};
		assertArrayEquals(expected3, sm.generateMatrix(3));
		
		int[][] expected4 = {{1,2,3,4},{12,13,14,5},{11,16,15,6},{10,9,8,7}};
		assertArrayEquals(expected4, sm.generateMatrix(4));
	}
}
