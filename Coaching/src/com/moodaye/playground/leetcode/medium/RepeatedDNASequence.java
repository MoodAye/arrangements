package com.moodaye.playground.leetcode.medium;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class RepeatedDNASequence {
	 public List<String> findRepeatedDnaSequences(String s) {
	        Set<String> ansSet = new HashSet<>();
	        Set<String> seen = new HashSet<>();
	        for(int i = 0; i < s.length() - 10 + 1; i++){
	            String sequence = s.substring(i, i + 10);
	            if(seen.contains(sequence)) ansSet.add(sequence);
	            else seen.add(sequence);
	        }
	        return List.copyOf(ansSet);
	    }
}
