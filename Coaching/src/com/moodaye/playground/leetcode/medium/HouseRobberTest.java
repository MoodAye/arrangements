package com.moodaye.playground.leetcode.medium;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class HouseRobberTest {

	@Test
	void test() {
		HouseRobber hr = new HouseRobber();
		int[] a = { 1, 2, 3 };
		assertEquals(4, hr.rob(a));

		int[] b = { 1, 2, 3, 1 };
		assertEquals(4, hr.rob(b));

		int[] c = { 2, 7, 9, 3, 1 };
		assertEquals(12, hr.rob(c));

		int[] d = { 2 };
		assertEquals(2, hr.rob(d));

		int[] e = { 2, 3 };
		assertEquals(3, hr.rob(e));

		int[] f = { 3, 2 };
		assertEquals(3, hr.rob(f));

		int[] g = { 3, 2, 1 };
		assertEquals(4, hr.rob(g));

		int[] h = { 2, 4, 1 };
		assertEquals(4, hr.rob(h));

		int[] i = { 3, 4, 3 };
		assertEquals(6, hr.rob(i));

		int[] j = { 2,1,1,2};
		assertEquals(4, hr.rob(j));
	}
}
