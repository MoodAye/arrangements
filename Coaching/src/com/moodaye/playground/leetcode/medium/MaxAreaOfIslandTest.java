package com.moodaye.playground.leetcode.medium;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class MaxAreaOfIslandTest {

	@Test
	void test() {
		MaxAreaOfIsland mai = new MaxAreaOfIsland();
		
		
		int[][] a = {{1,1},{1,1}};
		assertEquals(4, mai.maxAreaOfIsland(a));
		
		int[][] b = {{1}};
		assertEquals(1, mai.maxAreaOfIsland(b));
		 
	}

}
