package com.moodaye.playground.leetcode.medium;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

// Problem lc
public class AddTwoNumbers {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int[] nums = new int[n];
		for (int i = 0; i < nums.length; i++) {
			nums[i] = in.nextInt();
		}
	}

	public static void main(String[] args) {
		new AddTwoNumbers().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
