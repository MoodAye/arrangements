package com.moodaye.playground.leetcode.medium;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

import org.junit.jupiter.api.Test;

class TwoSumIISortedArrayTest {

	@Test
	void test() {
		int[] a = {2,7,11,15};
		int[] expected = {0,1};
		assertArrayEquals(expected, new TwoSumIISortedArray().twoSum(a, 9));
		
		expected[1] = 3;
		assertArrayEquals(expected, new TwoSumIISortedArray().twoSum(a, 17));
	}

}
