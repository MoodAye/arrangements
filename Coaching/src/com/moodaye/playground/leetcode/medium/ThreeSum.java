package com.moodaye.playground.leetcode.medium;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ThreeSum {
    public List<List<Integer>> threeSum(int[] nums) {
        List<List<Integer>> ans = new ArrayList<>();
        
        if(nums == null || nums.length == 0) {
        	return ans;
        }
        
    	Map<Integer, Integer> cnts = new HashMap<>();
    	
    	for(int i : nums) {
    		cnts.merge(i, 1, (a, b) -> a + 1);
    	}
    	
    	for(int key1 : cnts.keySet()) {
    		int cnt = cnts.get(key1);
    		
    		if(cnt >= 2) {
    			int key2 = 0 - 2 * key1;
    			if(cnts.containsKey(key2)) {
    				ans.add(List.of(key1,key1,key2));
    			}
    		}
    		
    		for(int key2 : cnts.keySet()) {
    			if(key2 <= key1) continue;
    			
    			int key3 = 0 - (key1 + key2);
    			if(key3 <= key2) continue;
    			
    			if(cnts.containsKey(key3)) {
    				ans.add(List.of(key1, key2, key3));
    			}
    		}
    	}
        return ans;
    	
//        int i = 0;
//        int j = 0;
//        int k = 0;
//        for(; i < nums.length; i++)
//            for(j = i + 1; j < nums.length; j++)
//                for(k = j + 1; k < nums.length; k++)
//                    if(nums[i] + nums[j] + nums[k] == 0) 
//                        ans.add(List.of(nums[i],nums[j],nums[k]));
    }
}
