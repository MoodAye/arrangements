package com.moodaye.playground.leetcode.medium;

// 857pm
public class RotateImage {
	
	 public void rotate(int[][] matrix) {
		 int len = matrix.length;
		 if(len == 1) return;
		 
		 for(int r = 0; r < len / 2; r++) {
			 for(int c = r; c < len - r - 1; c++) {
				 
				 // formula for mv is m[r][c] --> m[c][n - r - 1]
				 
				 int temp1 = matrix[c][len - r - 1];
				 matrix[c][len - r - 1] = matrix[r][c];
				 
				 int temp2 = matrix[len - r - 1][len - c - 1];
				 matrix[len - r - 1][len - c - 1] = temp1;
				 
				 temp1 = matrix[len - c - 1][r];
				 matrix[len - c - 1][r] = temp2;
				 
				 matrix[r][c] = temp1;
			 }
		 }
	        
	 }
}
/*
 * n,n
 * 0,0-------->0,n-1
 * 0,n-1----->n-1,n-1
 * n-1,n-1-->n-1,0
 * n-1,0---->0,0
 * 
 *   10  11  12  13
 *   14  15  16  17
 *   18  19  20  21
 *   22  23  24  25
 *   
 *   
 *   22  18  14  10
 *   23  19  15  11  
 *   24  20  16  12
 *   25  21  17  13
 *   
 * 
 * 
 */
