package com.moodaye.playground.leetcode.hard;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

// Problem lc 868
public class Trie {
	Node[] childs;

	public Trie() {
		childs = new Node[26];
	}

	public void insert(String word) {
		char[] cw = word.toCharArray();

		Node[] localChilds = childs;

		for (int i = 0; i < cw.length; i++) {
			
			if (localChilds[cw[i] - 'a'] == null) {
				localChilds[cw[i] - 'a'] = new Node(cw[i]);
			}
			
			if(i == cw.length - 1) {
				localChilds[cw[cw.length - 1] - 'a'].isWord = true;
			}
			
			localChilds = localChilds[cw[i] - 'a'].childs;
			
		}
	}

	public boolean search(String word) {
		char[] cw = word.toCharArray();

		Node[] localChilds = childs;
		boolean isWord = false;
		
		for (int i = 0; i < cw.length; i++) {
			if (localChilds[cw[i] - 'a'] != null) {
				isWord = localChilds[cw[i] - 'a'].isWord;
				localChilds = localChilds[cw[i] - 'a'].childs;
			} else {
				break;
			}
		}
		return isWord;
	}

	public boolean startsWith(String prefix) {
		char[] cw = prefix.toCharArray();

		Node[] localChilds = childs;

		for (int i = 0; i < cw.length; i++) {
			if (localChilds[cw[i] - 'a'] != null) {
				localChilds = localChilds[cw[i] - 'a'].childs;
			} else {
				return false;
			}
		}
		return true;
	}

	void solve(Scanner in, PrintWriter out) {
		while (true) {
			String cmd = in.next();

			switch (cmd.toLowerCase()) {
			case "quitnowdawg":
				out.println("OK - exiting");
				out.flush();
				System.exit(0);
				break;
			case "insert":
				insert(in.next());
				break;
			case "search":
				out.println(search(in.next()));
				break;
			case "startswith":
				out.println(startsWith(in.next()));
				break;
			default: 
				out.println("did not recognize command");
			}
		}
	}

	public static void main(String[] args) {
		new Trie().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}

class Node {
	Node[] childs = new Node[26];
	char c;
	boolean isWord;

	Node(char c) {
		this.c = c;
	}
}
