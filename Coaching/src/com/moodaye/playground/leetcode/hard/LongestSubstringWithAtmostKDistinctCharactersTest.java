package com.moodaye.playground.leetcode.hard;

import static org.junit.Assert.*;

import org.junit.Test;
import static com.moodaye.playground.leetcode.hard.LongestSubstringWithAtmostKDistinctCharacters.*;

public class LongestSubstringWithAtmostKDistinctCharactersTest{

	@Test
	public void testZeroLenString() {
		String s = "";
		int k = 0;
		assertEquals(0, lengthOfLongestSubstringKDistinct(s, k));
	}
	
	@Test
	public void testLCTest1() {
		String s = "eceba";
		int k = 2;
		assertEquals(3, lengthOfLongestSubstringKDistinct(s, k));
	}
	
	@Test
	public void testLCTest2() {
		String s = "aa";
		int k = 2;
		assertEquals(2, lengthOfLongestSubstringKDistinct(s, k));
	}
	
	@Test
	public void testLongSSAtEnd() {
		String s = "ecebaxxyy";
		int k = 2;
		assertEquals(4, lengthOfLongestSubstringKDistinct(s, k));
	}
}
