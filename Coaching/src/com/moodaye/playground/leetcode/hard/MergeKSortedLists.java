package com.moodaye.playground.leetcode.hard;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

// Problem lc
public class MergeKSortedLists {

	public static ListNode mergeKLists(ListNode[] lists) {
		if (lists == null || lists.length == 0) {
			return null;
		}

		ListNode mainHead = lists[0];

		for (int i = 1; i < lists.length; i++) {
			mainHead = mergeLists(mainHead, lists[i]);
		}

		return mainHead;
	}

	public static ListNode mergeLists(ListNode headA, ListNode headB) {
		if (headB == null && headA == null) {
			return null;
		}

		if (headA == null) {
			return headB;
		}

		if (headB == null) {
			return headA;
		}

		ListNode prevA = null;
		ListNode ptrA = headA;

		while (headB != null) {
			ListNode nextB = headB;
			headB = nextB.next;

			while (ptrA != null && ptrA.val < nextB.val) {
				prevA = ptrA;
				ptrA = ptrA.next;
			}

			if (prevA == null) {
				headA = nextB;
				nextB.next = ptrA;
				prevA = nextB;
			} else {
				prevA.next = nextB;
				prevA = nextB;
				nextB.next = ptrA;
			}
		}

		return headA;
	}

	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int[] nums = new int[n];
		for (int i = 0; i < nums.length; i++) {
			nums[i] = in.nextInt();
		}
	}

	public static void main(String[] args) {
		new MergeKSortedLists().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
