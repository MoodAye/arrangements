package com.moodaye.playground.leetcode.hard;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

public class LongestSubstringWithAtmostKDistinctCharacters {
	void solve(Scanner in, PrintWriter out) {
		String s = in.next();
		int k = in.nextInt();
	}
	public static int lengthOfLongestSubstringKDistinct(String s, int k) {
		if(s == null || s.length() == 0 || k == 0) return 0;
	
		char[] sc = s.toCharArray();
		int[] track = new int[256];
		
		track[sc[0]]++;
		int len = 1;
		int maxLen = 1;
		int ptrL = 0;
		int ptrR = 0;
		int uniqCnt = 1;
	
		while(true) {

			ptrR++;
			len++;
			
			if(ptrR == sc.length) break;
			
			if(track[sc[ptrR]] == 0) uniqCnt++;
			
			track[sc[ptrR]]++;
			
			if(uniqCnt <= k) maxLen = Math.max(maxLen, len);
			else {
				while(uniqCnt > k) {
					track[sc[ptrL]]--;
					if(track[sc[ptrL]] == 0) uniqCnt--;
					ptrL++;
					len--;
				}
			}
			
		}
		
		return maxLen;
	}

	public static int lengthOfLongestSubstringKDistinct2(String s, int k) {
		if (k == 0)
			return 0;

		int maxLen = 0;

		int[] tracker = new int[256];
		int distinct = 0;

		int ptrL = 0;
		int ptrR = 0;

		for (int i = 0; i < s.length(); i++) {
			if (tracker[s.charAt(i)] == 0) {
				distinct++;
				tracker[s.charAt(i)]++;
				maxLen++;
				ptrR++;
			} else {
				while (true) {
					while (tracker[s.charAt(ptrL)] == 0) ptrL++;
					tracker[s.charAt(ptrL)]--;
					if (tracker[s.charAt(ptrL)] == 1) {
						distinct--;
						break;
					}
				}
			}
		}

		return maxLen;
	}

	public static void main(String[] args) {
		new LongestSubstringWithAtmostKDistinctCharacters().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}

}
