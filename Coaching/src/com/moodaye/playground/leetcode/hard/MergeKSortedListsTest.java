package com.moodaye.playground.leetcode.hard;

import static org.junit.Assert.*;

import org.junit.Test;

public class MergeKSortedListsTest {

	@Test
	public void test1() {
		ListNode[] lists = new ListNode[3];

		lists[0] = genList(1, 4, 5);
		lists[1] = genList(1, 3, 4);
		lists[2] = genList(2, 6);

		ListNode head = MergeKSortedLists.mergeKLists(lists);
		assertEquals(8, getListSize(head));
		assertTrue(isInAscendingOrder(head));
		
		printList(head);
		
		lists = null;
		head = MergeKSortedLists.mergeKLists(lists);
		assertTrue(head == null);
		
		lists = new ListNode[3];
		assertEquals(0, getListSize(head));
		
	}
	
	@Test
	public void test2() {
		ListNode[] lists = new ListNode[3];

		lists[0] = genList(1, 2, 2);
		lists[1] = genList(1, 1, 2);

		ListNode head = MergeKSortedLists.mergeKLists(lists);
		assertEquals(6, getListSize(head));
		assertTrue(isInAscendingOrder(head));
		
		printList(head);
	}

	public static boolean isInAscendingOrder(ListNode head) {
		if(head == null) {
			return true;
		}
		ListNode next = head.next;
		while(next != null) {
			if(next.val < head.val) {
				return false;
			}
			head = next;
			next = next.next;
		}
		return true;
	}

	public static int getListSize(ListNode head) {
		int cnt = 0;
		while(head != null) {
			cnt++;
			head = head.next;
		}
		return cnt;
	}

	public static ListNode genList(int ... var) {
		ListNode head = new ListNode();
		ListNode next = head;
		
		for(int i = 0; i < var.length; i++) {
			next.val = var[i];
			if(i != var.length - 1) {
				next.next = new ListNode();
				next = next.next;
			}
		}
		return head;
	}
	
	public static void printList(ListNode head) {
    	while(head != null) {
    		System.out.print(head.val + " ");
    		head = head.next;
    	}
    	System.out.println();
    }
}
