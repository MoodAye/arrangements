package com.moodaye.playground.leetcode.hard;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;

// Problem 
// 854am - 9:21
public class WordBreak {
	void solve(Scanner in, PrintWriter out) {
		String s = in.next();
		List<String> wordDict = new ArrayList<>();
	}
	
	// understand sorted list / binary search of list in java (see collections class)
	public static boolean wordBreak(String s, List<String> wordDict) {
		
		boolean[] isWord =  new boolean[s.length()];
		
		for(int i = 0; i < s.length(); i++) {
			for(int j = i; j >= 0; j--) {
				String curr = s.substring(j, i + 1);
				if(wordDict.contains(curr) && (j == 0 || isWord[j - 1])) {
					isWord[i] = true;
					break;
				}
			}
		}
		
		return isWord[s.length() - 1];
	}

	public static void main(String[] args) {
		new WordBreak().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
