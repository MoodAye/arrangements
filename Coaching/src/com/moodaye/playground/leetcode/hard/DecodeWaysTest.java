package com.moodaye.playground.leetcode.hard;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class DecodeWaysTest {

	@Test
	void leetCodeTests() {
		assertEquals(2, new DecodeWays().numDecodings("12"));
		assertEquals(3, new DecodeWays().numDecodings("226"));
		assertEquals(0, new DecodeWays().numDecodings("06"));
		assertEquals(1, new DecodeWays().numDecodings("2101")); // 2(1); 2,1(1) + 21(1); 2,1,0(0) + 21,0 (0) + 2,10(1); 210,1(1) + 21,01 
	}
	
	@Test
	void myTests1() {
		assertEquals(3, new DecodeWays().numDecodings("123"));  
		assertEquals(3, new DecodeWays().numDecodings("12345679"));
		assertEquals(3, new DecodeWays().numDecodings("1230"));
	}
	
	@Test
	void zeroTests() {
		assertEquals(3, new DecodeWays().numDecodings("12304")); 
		assertEquals(3, new DecodeWays().numDecodings("123040"));
		assertEquals(3, new DecodeWays().numDecodings("123041"));
		assertEquals(0, new DecodeWays().numDecodings("10011"));  
		assertEquals(0, new DecodeWays().numDecodings("230"));  
	}
}
