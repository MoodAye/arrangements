package com.moodaye.playground.leetcode.hard;

import java.util.ArrayList;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;

import static com.moodaye.playground.leetcode.hard.WordBreak.*;
import static org.junit.Assert.*;

public class WordBreakTest {
	List<String> dict = new ArrayList<>();
	
	@BeforeClass
	public static void setup() {
	}

	@Test
	public void test1() {
		String s = "leetcode";
		List<String> dict = new ArrayList<>();
		assertFalse(wordBreak(s, dict));
	}
	
	@Test
	public void test2() {
		String s = "leetcode";
		List<String> dict = new ArrayList<>();
		dict.add("leet");
		dict.add("code");
		assertTrue(wordBreak(s, dict));
	}
	

	@Test
	public void test3() {
		String s = "applepenapplie";
		List<String> dict = new ArrayList<>();
		dict.add("apple");
		dict.add("pen");
		assertFalse(wordBreak(s, dict));
	}
	
	@Test
	public void test4() {
		String s = "catsandog";
		List<String> dict = new ArrayList<>();
		dict.add("cats");
		dict.add("dog");
		dict.add("sand");
		dict.add("and");
		dict.add("cat");
		assertFalse(wordBreak(s, dict));
	}

	@Test
	public void test5() {
		String s = "catsanddog";
		List<String> dict = new ArrayList<>();
		dict.add("cats");
		dict.add("dog");
		dict.add("sand");
		dict.add("and");
		dict.add("cat");
		assertTrue(wordBreak(s, dict));
	}
}
