package com.moodaye.playground.leetcode.hard;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class KthSmallestElementInSortedMatrixTest {

	@Test
	void test() {
		int[][] m = {{1,5,9},{10,11,13},{12,13,15}};
		assertEquals(1, new KthSmallestElementInSortedMatrix().kthSmallest(m, 1));
		assertEquals(5, new KthSmallestElementInSortedMatrix().kthSmallest(m, 2));
		assertEquals(9, new KthSmallestElementInSortedMatrix().kthSmallest(m, 3));
		assertEquals(10, new KthSmallestElementInSortedMatrix().kthSmallest(m, 4));
		assertEquals(11, new KthSmallestElementInSortedMatrix().kthSmallest(m, 5));
		assertEquals(12, new KthSmallestElementInSortedMatrix().kthSmallest(m, 6));
		assertEquals(13, new KthSmallestElementInSortedMatrix().kthSmallest(m, 7));
		assertEquals(13, new KthSmallestElementInSortedMatrix().kthSmallest(m, 8));
		assertEquals(15, new KthSmallestElementInSortedMatrix().kthSmallest(m, 9));
	}

}
