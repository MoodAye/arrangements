package com.moodaye.playground.leetcode.hard;

// 129pm - 224pm
public class DecodeWays {
	public int numDecodings(String s) {
		char[] cs = s.toCharArray();
		if(cs[0] == '0') return 0;
		
		int cnt = 1;
		int pCnt = 1;
		int ppCnt = 1;
		
		for(int i = 1; i < s.length(); i++) {
			if(cs[i] == '0') {
				if('1' <= cs[i - 1] && cs[i - 1] <= '2') {
					cnt = ppCnt;
				}
				else {
					cnt = 0;
				}
			}
			else if (cs[i - 1] == '0') {
				cnt = pCnt;
			}
			else if(cs[i - 1] == '1') {
				cnt = pCnt + ppCnt;
			}
			else if(cs[i - 1] == '2') {
				if(cs[i] <= '6') {
					cnt = pCnt + ppCnt; // 1344324 --> 134432,4 + 13443,24
				}
				else {
					cnt = pCnt; // 1344327 --> 134432,7 + 13443,27
				}
			}
			else {
				cnt = pCnt;
			}
			int temp = pCnt;
			pCnt = cnt;
			ppCnt = temp;
		}
		
		return cnt;
	}
}
