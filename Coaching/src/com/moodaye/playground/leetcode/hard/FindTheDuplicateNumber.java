package com.moodaye.playground.leetcode.hard;

import java.util.Arrays;

// lc 843am
public class FindTheDuplicateNumber {
	public static void main(String[] args) {
		int[] nums = { 1, 1 };
		System.out.println(findDuplicate(nums));
	}

	public static int findDuplicate(int[] nums) {
		int ptr = 0;
		int n = nums.length;
		int nCurr = -1;
		while (ptr <= n) {
			nCurr = nums[ptr];
			while (nCurr > 0) {
				int idxReplace = nums[nCurr];
				if (nums[idxReplace] == -1) {
					return idxReplace;
				} else {
					nCurr = nums[idxReplace];
					nums[idxReplace] = nums[idxReplace] > 0 ? -1 : nums[idxReplace] - 1;
				}
			}
			ptr++;
		}
		return 0;
	}

	/*
	 * 4,2,3,3,3 n=4 3,2,3,3,-1 n=4 3,2,3,-1,-1 n=4 0,0,-1,-3,-1 n=4
	 * 
	 * ptr = 0; while(ptr <= n) if(nums[ptr] != 0){
	 * 
	 */
}
