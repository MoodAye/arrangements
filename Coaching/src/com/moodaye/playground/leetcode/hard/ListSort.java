package com.moodaye.playground.leetcode.hard;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

// Problem 
public class ListSort {

    public ListNode sortList(ListNode head) {
        if (head == null || head.next == null)
            return head;
        ListNode mid = getMid(head);
        ListNode left = sortList(head);
        ListNode right = sortList(mid);
        return merge(left, right);
    }

    ListNode merge(ListNode list1, ListNode list2) {
        ListNode dummyHead = new ListNode();
        ListNode tail = dummyHead;
        while (list1 != null && list2 != null) {
            if (list1.val < list2.val) {
                tail.next = list1;
                list1 = list1.next;
                tail = tail.next;
            } else {
                tail.next = list2;
                list2 = list2.next;
                tail = tail.next;
            }
        }
        tail.next = (list1 != null) ? list1 : list2;
        return dummyHead.next;
    }

    ListNode getMid(ListNode head) {
        ListNode midPrev = null;
        while (head != null && head.next != null) {
            midPrev = (midPrev == null) ? head : midPrev.next;
            head = head.next.next;
        }
        ListNode mid = midPrev.next;
        midPrev.next = null;
        return mid;
    }
    
    void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		ListNode head = new ListNode();
		ListNode next = head;
		for (int i = 0; i < n; i++) {
			next.val = in.nextInt();
			if(i != n - 1) {
				next.next = new ListNode();
			}
			next = next.next;
		}
		printList(head);
		sortList(head);
		printList(head);
	}
    
    public void printList(ListNode head) {
    	while(head != null) {
    		System.out.print(head.val + " ");
    		head = head.next;
    	}
    	System.out.println();
    }

	public static void main(String[] args) {
		new ListSort().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
