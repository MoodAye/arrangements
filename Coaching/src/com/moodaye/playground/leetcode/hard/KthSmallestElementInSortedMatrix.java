package com.moodaye.playground.leetcode.hard;

// lc746am
public class KthSmallestElementInSortedMatrix {
    public int kthSmallest(int[][] m, int k) {
    	
    	int r = 0;
    	int c = 0;
    	int cnt = 1;
    	
    	boolean chkPriorRow = false;
    	while(cnt != k) {
    		if(r == m.length - 1) {
    			c++;
    		}
    		else if(c == m[0].length - 1 || m[r + 1][0] < m[r][c + 1]) {
    			c = 0;
    			r++;
    		}
    		else {
    			c++;
    		}
    		cnt++;
    	}
    	return m[r][c];
    }
}
