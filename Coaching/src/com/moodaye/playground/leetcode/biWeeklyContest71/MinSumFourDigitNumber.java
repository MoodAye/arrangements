package com.moodaye.playground.leetcode.biWeeklyContest71;

public class MinSumFourDigitNumber {
    public int minimumSum(int num) {
    	int[] sorted = new int[10];
    	
    	for(int i = 0; i < 4; i++) {
    		sorted[num % 10]++;
    		num /= 10;
    	}
    	
    	int digit = 1;
    	int n1 = 0;
    	int n2 = 0;
    	for(int i = 0; i < 10; i++) {
    		while(sorted[i]-- > 0) {
    			if(digit == 1) {
    				n1 += i * 10;
    			}
    			if(digit == 2) {
    				n2 += i * 10;
    			}
    			if(digit == 3) {
    				n1 += i;
    			}
    			if(digit == 4) {
    				n2 += i;
    			}
    			digit++;
    		}
    	}
    	
    	return n1 + n2;
    }
}
