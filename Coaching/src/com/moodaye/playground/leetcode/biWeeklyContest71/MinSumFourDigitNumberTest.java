package com.moodaye.playground.leetcode.biWeeklyContest71;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class MinSumFourDigitNumberTest {

	@Test
	void test() {
		assertEquals(52, new MinSumFourDigitNumber().minimumSum(2932));
		assertEquals(13, new MinSumFourDigitNumber().minimumSum(4009));
		assertEquals(1, new MinSumFourDigitNumber().minimumSum(1000));
		assertEquals(14 + 23, new MinSumFourDigitNumber().minimumSum(1234));
	}

}
