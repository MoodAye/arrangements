package com.moodaye.playground.leetcode.biWeeklyContest71;

import java.util.ArrayList;

public class PartitionArrayPerPivot {

	public int[] pivotArray(int[] nums, int pivot) {
		ArrayList<Integer> left = new ArrayList<>();
		ArrayList<Integer> right = new ArrayList<>();
		ArrayList<Integer> middle = new ArrayList<>();
		
		for(int i : nums) {
			if(i < pivot) left.add(i);
			else if(i > pivot) right.add(i);
			else middle.add(i);
		}
		left.addAll(middle);
		left.addAll(right);
		for(int i = 0; i < nums.length; i++)
			nums[i] = left.get(i);
		
		return nums;
	}
}
