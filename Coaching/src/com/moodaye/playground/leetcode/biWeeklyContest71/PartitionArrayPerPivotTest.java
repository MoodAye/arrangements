package com.moodaye.playground.leetcode.biWeeklyContest71;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class PartitionArrayPerPivotTest {

	@Test
	void test() {
		int[] a = {9, 12, 5, 10, 14, 3, 10};
		int[] b = new PartitionArrayPerPivot().pivotArray(a, 10);
		int[] c = {9,10};
		int[] d = new PartitionArrayPerPivot().pivotArray(c, 9);

	}

}
