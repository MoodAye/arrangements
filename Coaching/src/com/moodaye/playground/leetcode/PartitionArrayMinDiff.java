package com.moodaye.playground.leetcode;

import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Locale;
import java.util.Scanner;

// Problem 
public class PartitionArrayMinDiff {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int[] nums = new int[n];
		for (int i = 0; i < nums.length; i++) {
			nums[i] = in.nextInt();
		}
//		out.println(partition(nums, 0, 0, 0, 0, 0));
		out.println(partition2(nums));
	}
	
	
	/**
	 * This time we will take the max and put it in 
	 * the first array.
	 * 
	 * Then we take the second highest and put it in 
	 * the second array.  
	 * 
	 * We take the next highest and put it in the second array and
	 * keep putting the next highest until the sum exceeds that of
	 * array 1.
	 * 
	 * Repeat now with the first array.
	 * 
	 * Once the arrays are filled - we are done.
	 * @param nums
	 * @return
	 */
	public int partition2(int[] nums) {
		Arrays.sort(nums);
		
		int len = nums.length;
		
		int[] a1 = new int[len / 2];
		int[] a2 = new int[len / 2];
		
		int idx1 = 0;
		int idx2 = 0;
		
		int sum1 = 0;
		int sum2 = 0;
		
		for(int i = nums.length - 1; i >= 0; i--) {
			if(sum1 < sum2 && idx1 < len / 2) {
				a1[idx1++] = nums[i];
				sum1 += nums[i];
			}
			else {
				a2[idx2++] = nums[i];
				sum2 += nums[i];
			}
		}
		
		return Math.abs(sum1 - sum2);
	}

	/**
	 * Assumes a.length is even
	 *  Runs into TLE (say with 30 numbers)
	 * @param a
	 * @param idx
	 * @param cnt1
	 * @param cnt2
	 * @param sum1
	 * @param sum2
	 * @return
	 */
	public int partition(int[] a, int idx, int cnt1, int cnt2, int sum1, int sum2) {
		if (cnt1 == a.length / 2 && cnt2 == a.length / 2) {
			return Math.abs(sum1 - sum2);
		}

		if (cnt1 > a.length / 2 || cnt2 > a.length / 2) {
			return Integer.MAX_VALUE;
		}
		int diff1 = partition(a, idx + 1, cnt1 + 1, cnt2, sum1 + a[idx], sum2);

		int diff2 = partition(a, idx + 1, cnt1, cnt2 + 1, sum1, sum2 + a[idx]);

		return Math.min(diff1, diff2);
	}

	public static void main(String[] args) {
		new PartitionArrayMinDiff().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
