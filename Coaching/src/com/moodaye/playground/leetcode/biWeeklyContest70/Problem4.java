package com.moodaye.playground.leetcode.biWeeklyContest70;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

// Problem 
public class Problem4 {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int[] nums = new int[n];
		for (int i = 0; i < nums.length; i++) {
			nums[i] = in.nextInt();
		}
	}

	public static void main(String[] args) {
		new Problem4().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
