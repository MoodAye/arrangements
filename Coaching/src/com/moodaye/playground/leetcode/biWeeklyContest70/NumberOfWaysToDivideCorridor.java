package com.moodaye.playground.leetcode.biWeeklyContest70;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;
	
// Problem 
public class NumberOfWaysToDivideCorridor {
	public final static int MOD = 1_000_000_007; 
	
	void solve(Scanner in, PrintWriter out) {
		out.printf("For %s (expected 3) count = %d%n", "SSPPSPS", countArrangements("SSPPSPS".toCharArray()));
		out.printf("For %s (expected 1) count = %d%n", "PPSPSP", countArrangements("PPSPSP".toCharArray()));
		out.printf("For %s (expected 0) count = %d%n", "C", countArrangements("C".toCharArray()));
		out.printf("For %s (expected 0) count = %d%n", "CC", countArrangements("CC".toCharArray()));
		out.printf("For %s (expected 0) count = %d%n", "CCSSSC", countArrangements("CCSSSC".toCharArray()));
		
		out.printf("For %s (expected 15) count = %d%n", "SSPPSPSPPPPSS", countArrangements("SSPPSPSPPPPSS".toCharArray()));
		out.printf("For %s (expected 2) count = %d%n", "PPSPPSSSSSSPSPS", countArrangements("PPSPPSSSSSSSPSPS".toCharArray()));
		
		out.printf("For %s (expected 919999993) count = %d%n", "PPPPPSPPSPPSPPPSPPPPSPPPPSPPPPSPPSPPPSPSPPPSPSPPPSPSPPPSPSPPPPSPPPPSPPPSPPSPPPPSPSPPPPSPSPPPPSPSPPPSPPSPPPPSPSPSS", 
				countArrangements("PPPPPSPPSPPSPPPSPPPPSPPPPSPPPPSPPSPPPSPSPPPSPSPPPSPSPPPSPSPPPPSPPPPSPPPSPPSPPPPSPSPPPPSPSPPPPSPSPPPSPPSPPPPSPSPSS".toCharArray()));
		
		out.printf("For %s (expected 18335643) count = %d%n", "PPPPPPPSPPPSPPPPSPPPSPPPPPSPPPSPPSPPSPPPPPSPSPPPPPSPPSPPPPPSPPSPPSPPPSPPPPSPPPPSPPPPPSPSPPPPSPSPPPSPPPPSPPPPPSPSPPSPPPPSPPSPPSPPSPPPSPPSPSPPSSSS", 
				countArrangements("PPPPPPPSPPPSPPPPSPPPSPPPPPSPPPSPPSPPSPPPPPSPSPPPPPSPPSPPPPPSPPSPPSPPPSPPPPSPPPPSPPPPPSPSPPPPSPSPPPSPPPPSPPPPPSPSPPSPPPPSPPSPPSPPSPPPSPPSPSPPSSSS".toCharArray()));
	}
	
	public int countArrangements(char[] c) {
		int chairs = 0;
		for(int i = 0; i < c.length; i++) {
			if(c[i] == 'S') chairs++;
		}
		
		if(chairs <= 0 || chairs % 2 == 1) return 0;
		if(chairs == 2) return 1;
		return (int) countArrangements(c, 0);
	}
	
	
	public long countArrangements(char[] c, int startIdx) {
		if(startIdx == c.length) return 1;
		
		int firstChairIdx = -1;
		for(int i = startIdx; i < c.length; i++) {
			if(c[i] == 'S') {
				firstChairIdx = i;
				break;
			}
		}
		
		if(firstChairIdx == -1) return 1;
		
		int secondChairIdx = 0;
		for(int i = firstChairIdx + 1; i < c.length; i++) {
			if(c[i] == 'S') {
				secondChairIdx = i;
				break;
			}
		}
		if(startIdx == 0) 
			return countArrangements(c, secondChairIdx + 1) % MOD;
		return (firstChairIdx - startIdx + 1) * countArrangements(c, secondChairIdx + 1) % MOD;
	}

	public static void main(String[] args) {
		new NumberOfWaysToDivideCorridor().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
