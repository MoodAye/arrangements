package com.moodaye.playground.leetcode.biWeeklyContest70;

import java.util.Arrays;

public class ConvertStringToDataStructure {
	
	
	

	// given "[a,b,c,d,e]" return array
	public static int[] getArray(String str) {
		if(str == null || str.length() == 0) {
			return null;
		}
		
		String[] strSplit = str.split(",");
		int[] a = new int[strSplit.length];
		
		if(strSplit.length == 1) {
			a[0] = Integer.valueOf(strSplit[0].substring(1, strSplit[0].length() - 1));
			return a;
		}
		
		a[0] = Integer.valueOf(strSplit[0].substring(1));
		a[a.length - 1] = Integer.valueOf(strSplit[a.length - 1].substring(0, strSplit[a.length - 1].length() - 1));
		
		for(int i = 1; i < a.length - 1; i++) {
			a[i] = Integer.valueOf(strSplit[i]);
		}
		
		return a;
	}
	
	public static void main(String[] args) {
		printArray(ConvertStringToDataStructure.getArray("[1,2,3]"));
		printArray(ConvertStringToDataStructure.getArray("[1]"));
		printArray(ConvertStringToDataStructure.getArray("[1,3]"));
	}
	
	public static void printArray(int[] a) {
		Arrays.stream(a).forEach((i) -> System.out.print(i + " "));
		System.out.println();
	}
}
