package com.moodaye.playground.leetcode.biWeeklyContest70;

import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Locale;
import java.util.Scanner;

// Problem 
public class Problem2 {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int[] nums = new int[n];
		
		for (int i = 0; i < nums.length; i++) {
			nums[i] = in.nextInt();
		}
		
		int upper = in.nextInt();
		int lower = in.nextInt();
		
		out.println(numberOfArrays(nums, upper, lower));
	}
	public int numberOfArrays(int[] differences, int lower, int upper) {
		int cnt = 0;
		
		long currVal = 0L;
		
		// is L needed
		long min = 0L;
		long max = 0L;
		
		for(int i = 0; i < differences.length; i++) {
			currVal += differences[i];
			min = Math.min(min, currVal);
			max = Math.max(max, currVal);
		}
		
		long bottom = lower - min;
		long top = upper - max;
		
		return (int) (top - bottom + 1 < 0 ? 0 : top - bottom + 1L);
	}
	
	public int numberOfArrays2(int[] differences, int lower, int upper) {
		int cnt = 0;
	
		for(long startVal = lower; startVal <= upper; startVal++) {
			
			long[] a = new long[differences.length + 1];
			boolean found = true;
			boolean noMore = false;
			
			
			a[0] = startVal;
			
			for(int idx = 1; idx < a.length; idx++) {
				
				// concern - difference are in int - quto conversion to long?
				a[idx] = a[idx - 1] + differences[idx - 1];
				
				if (a[idx] < lower) {
					found = false;
					break;
				}
				
				if (a[idx] > upper) {
					found = false;
					noMore = true;
					break;
				}
			}
			
			if(found) {
				cnt = upper - (int) Arrays.stream(a).max().getAsLong() + 1;
				break;
			}
		}
		return cnt;
	}

	public static void main(String[] args) {
		new Problem2().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
