package com.moodaye.playground.leetcode.biWeeklyContest70;

import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Locale;
import java.util.Scanner;

// Problem 
public class Problem1 {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int[] nums = new int[n];
		for (int i = 0; i < nums.length; i++) {
			nums[i] = in.nextInt();
		}
		out.println(mc(nums));
		
	}
	
	public int mc(int[] nums) {
		Arrays.sort(nums);
		int sum = 0;
		for(int i = nums.length - 1; i >= 0; i -= 3) {
			if(i == 0) {
				sum += nums[i];
				break;
			}
			sum += nums[i] + nums[i - 1];
		}
		return sum;
	}

	public static void main(String[] args) {
		new Problem1().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
