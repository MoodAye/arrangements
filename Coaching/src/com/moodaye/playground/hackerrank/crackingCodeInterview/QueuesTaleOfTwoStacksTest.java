package com.moodaye.playground.hackerrank.crackingCodeInterview;

import static org.junit.Assert.*;

import org.junit.Test;

import com.moodaye.playground.hackerrank.crackingCodeInterview.QueuesTaleOfTwoStacks.MyQueue;
import com.moodaye.playground.hackerrank.crackingCodeInterview.QueuesTaleOfTwoStacks.Stack;

public class QueuesTaleOfTwoStacksTest {

	@Test
	public void testStack() {
		Stack<Integer> stack = new Stack<>();
		assertEquals(0, stack.size);
		stack.push(1);
		stack.push(2);
		stack.push(3);

		assertEquals(3, stack.size);
		assertEquals(new Integer(3), stack.peek());
		assertEquals(3, stack.size);
		assertEquals(new Integer(3), stack.pop());
		assertEquals(2, stack.size);
		assertEquals(new Integer(2), stack.peek());
		assertEquals(new Integer(2), stack.pop());
		assertEquals(1, stack.size);
		assertEquals(new Integer(1), stack.pop());
		assertNull(stack.pop());
		assertEquals(0, stack.size);
		
		stack.push(1);
		stack.push(2);
		stack.push(3);
		stack.pop();
		stack.pop();
		stack.push(5);
		assertEquals(new Integer(5), stack.peek());
		
	}
	
	@Test
	public void testQueue(){
		MyQueue<Integer> queue = new MyQueue<>();
		
		queue.enqueue(1);
		queue.enqueue(2);
		queue.enqueue(3);
		assertEquals(new Integer(1), queue.dequeue());
		assertEquals(new Integer(2), queue.dequeue());
		assertEquals(new Integer(3), queue.dequeue());
	}

}
