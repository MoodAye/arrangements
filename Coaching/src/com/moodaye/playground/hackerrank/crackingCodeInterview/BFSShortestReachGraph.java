package com.moodaye.playground.hackerrank.crackingCodeInterview;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Scanner;

public class BFSShortestReachGraph {
	
	public static class Graph{
		private final int V;
		private int E;
		private List<Integer>[] adj;
		
		private boolean[] marked;
		private int[] edgesTo;
		
		public Graph(int V){
			this.V = V;
			E = 0;
			adj = (List<Integer>[]) new LinkedList[V];
			for(int i = 0; i < this.V; i++)
				adj[i] = new LinkedList<Integer>();
			marked = new boolean[this.V];
			edgesTo = new int[this.V];
			for ( int i = 0 ; i < V; i++)
				edgesTo[i] = -1;
		}
		
		public Graph (Scanner in){
			this(in.nextInt());
			int _E = in.nextInt();
			for(int i = 0 ; i < _E; i++){
				addEdge(in.nextInt(), in.nextInt());
			}
		}
		
		public void addEdge(int v, int w){
			adj[v].add(w);
			adj[w].add(v);
			E++;
		}
		
		public Iterable<Integer> adj(int v){
			return adj[v];
		}
		
		public int[] shortestReach(int startId){
			Queue<Integer> queue = new LinkedList<>();
			queue.add(startId);
			marked[startId] = true;
			edgesTo[startId] = 0;
			while(queue.size() != 0){
				int v = queue.remove();
				for ( int w : adj(v)){
					if (!marked[w]){
						queue.add(w);
						marked[w] = true;
						edgesTo[w] = edgesTo[v] + 6;
					}
				}
			}
			return edgesTo;
		}
	}
	
	 public static void main(String[] args) {
	        Scanner scanner = new Scanner(System.in);
	      
	        int queries = scanner.nextInt();
	        
	        for (int t = 0; t < queries; t++) {
	            
	            // Create a graph of size n where each edge weight is 6:
	            Graph graph = new Graph(scanner.nextInt());
	            int m = scanner.nextInt();
	            
	            // read and set edges
	            for (int i = 0; i < m; i++) {
	                int u = scanner.nextInt() - 1;
	                int v = scanner.nextInt() - 1;
	                
	                // add each edge to the graph
	                graph.addEdge(u, v);
	            }
	            
	            // Find shortest reach from node s
	            int startId = scanner.nextInt() - 1;
	            int[] distances = graph.shortestReach(startId);
	 
	            for (int i = 0; i < distances.length; i++) {
	                if (i != startId) {
	                    System.out.print(distances[i]);
	                    System.out.print(" ");
	                }
	            }
	            System.out.println();            
	        }
	        
	        scanner.close();
	    }
}
