package com.moodaye.playground.hackerrank.crackingCodeInterview;

import java.util.Scanner;

public class QueuesTaleOfTwoStacks {
	
	public static class MyQueue<T> {
        Stack<T> stackNewestOnTop = new Stack<T>();
        Stack<T> stackOldestOnTop = new Stack<T>();

        public void enqueue(T value) { // Push onto newest stack
        	if(stackOldestOnTop.size == 0){
        		stackOldestOnTop.push(value);
        		moveNewestToOldest();
        	}
        	else{
        		moveOldestToNewest();
        		enqueue(value);
        	}
        }

        public T peek() {
        	if(stackOldestOnTop.size == 0)
        		moveNewestToOldest();
        	return stackOldestOnTop.peek();
        }

        public T dequeue() {
        	if(stackOldestOnTop.size == 0)
        		moveNewestToOldest();
        	return stackOldestOnTop.pop();
        }
        
        private void moveNewestToOldest(){
        	while (stackNewestOnTop.size != 0)
        		stackOldestOnTop.push(stackNewestOnTop.pop());
        }
        
        private void moveOldestToNewest(){
        	while(stackOldestOnTop.size != 0)
        		stackNewestOnTop.push(stackOldestOnTop.pop());
        }
    }
	
	
	public static class Stack<T>{
		Node<T> head;
		int size = 0;
		
		void push(T t){
			if (head == null){
				head = new Node<>(t, null);
			}
			else{
				Node<T> temp = head;
				head = new Node<>(t, temp);
			}
			size++;
		}
		
		T pop(){
			if (head == null) return null;
			Node<T> temp = head;
			head = head.next;
			size--;
			return temp.value;
		}
		
		T peek(){
			return head.value;
		}
	}

	public static class Node<T>{
		T value;
		Node next;
		
		Node(T value, Node next){
			this.value = value;
			this.next = next;
		}
	}
	

    
    public static void main(String[] args) {
        MyQueue<Integer> queue = new MyQueue<Integer>();
        
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();
        
        for (int i = 0; i < n; i++) {
            int operation = scan.nextInt();
            if (operation == 1) { // enqueue
                queue.enqueue(scan.nextInt());
            } else if (operation == 2) { // dequeue
                queue.dequeue();
            } else if (operation == 3) { // print/peek
                System.out.println(queue.peek());
            }
        }
        scan.close();
    }
}
