package com.moodaye.playground.hackerrank.datastructures;

import java.io.PrintWriter;
import java.util.Scanner;

public class WeekOfCode3RangeModularQueries {
	 public static void main(String[] args) {
	        Scanner in = new Scanner(System.in);
	        int n = in.nextInt();
	        int q = in.nextInt();
	        int[] a = new int[n];
	        for(int a_i=0; a_i < n; a_i++){
	            a[a_i] = in.nextInt();
	        }
	        PrintWriter out = new PrintWriter(System.out);
	        for(int a0 = 0; a0 < q; a0++){
	            int left = in.nextInt();
	            int right = in.nextInt();
	            int x = in.nextInt();
	            int y = in.nextInt();
	            int cnt = 0;
	            for(int i=left; i<=right && i<a.length; i++){
	                if (a[i] % x == y){
	                    cnt++;
	                }
	            }
	            out.println(cnt);
	        }
	        out.flush();
	    }
}
