package com.moodaye.playground.hackerrank.datastructures;

import java.util.Scanner;

/**
 * HackerRank - Practice - Data Structures - Arrays - 2D Array - DS
 * 
 * Strategy: The hour glasses are predefined. We add them up individually
 * and then determine the max.
 * 
 * for hour glass starting at i,j ... elements are
 * (i,j)    (i,j+1)      (i,j+2)
 *         (i+1,j+1)
 * (i+2,j) (i+2,j+1)    (i+2,j+2)
 * 
 * 0<=i,j<=3   (i-j <= 2)
 *
 */
public class Arrays2DArrayDS {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
        int arr[][] = new int[6][6];
        for(int arr_i=0; arr_i < 6; arr_i++){
            for(int arr_j=0; arr_j < 6; arr_j++){
                arr[arr_i][arr_j] = in.nextInt();
            }
        }	
       
        int maxSum = Integer.MIN_VALUE;
        for (int i=0; i<4; i++)
        	for (int j=0; j<4; j++){
        		//if(i-j>2) continue;  //this caused test case #6 to fail. Not needed actually?
        		int sum = arr[i][j] + arr[i][j+1] + arr[i][j+2] +
        				              arr[i+1][j+1] + 
        				  arr[i+2][j] + arr[i+2][j+1] + arr[i+2][j+2];
        		if (sum > maxSum) maxSum = sum;
        	}
        
        System.out.println(maxSum);
	}
}
