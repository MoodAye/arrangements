package com.moodaye.playground.hackerrank.datastructures;

import java.util.Scanner;
import java.util.TreeMap;

/**
 * HackerRank - Practice - Data Structures - Stacks - Maximum Element
 * Runing count of time spent ... 3/9/17 = 20min.  Stuck on how to implement max.
 * 
 * Max = 20pts; Diff = E
 * 1    -Push the element x into the stack.
 * 2    -Delete the element present at the top of the stack.
 * 3    -Print the maximum element in the stack.
 * 1<=N<=10^5   N = Number of queries
 * 1<=x<=10^9   x = element of the stack
 * 1<=type<=3   query type
 * 
 * Strategy: Implement simple stack - Node class with link to next element. Client
 * will be presented the head Node of the stack. If head is deleted or new element is
 * pushed - the head reference is adjusted accordingly.
 * 
 * To evaluate the max - a running max element is established in the Node as an attribute.  
 * 
 * **** But - if the head node comprises the max value - we have an issue. We then need to search the stack for the 
 * max value.  So this strategy won't work.
 * 
 * Let's use a stack and maintain a heap structure within the stack so that we can get the max.  
 * We have to delete nodes from the heap as needed.  Actually - need a MaxPQ. 
 * 
 * When we delete an item from the stack - we need to delete the item from the MaxPQ. So we need to implement a delete operation
 * on the PQ that works in LogN time.  Since the underlying structure of the PQ is a heap - finding elements is not straightforward.
 * 
 * The java.util PriorityQueue implementation has linear time for removal of objects.  So this wont work.
 * 
 * The java.util TreeSet implementation guarantees logN time for removal - but does not allow duplicates. 
 * 
 * So we can use a TreeMap with key equal to the number being inserted into the stack and value equal to the count.
 * 
 * 
 * 
 */
public class StacksMaximumElement {

	public static void main(String[] args) {
		MStack stack = new StacksMaximumElement().new MStack();
		Scanner in = new Scanner(System.in);
		int N = in.nextInt();
		for (int i=0; i<N; i++){
			int type = in.nextInt();
			if (type == 1){
				int x = in.nextInt();
				stack.push(x);
			}
			else if(type == 2)
				stack.delete();
			else
				System.out.println(stack.tm.lastKey());
		}
	}
	
	private class MStack{
		Node head;
		TreeMap<Integer, Integer> tm = new TreeMap<>();
		
		void push(int x){
			Node newHead = new Node(x);
			newHead.next = head;
			head = newHead;
			Integer i = tm.get(x);
			if (i == null) tm.put(x, 1);
			else tm.put(x, ++i);
		}
		
		void delete(){
			if (tm.get(head.value) == 1) tm.remove(head.value);
			else {
				int count = tm.get(head.value);
				tm.put(head.value, --count);
			}
			head = head.next;
		}
	}
	
	private class Node{
		int value;  // max value is 10^9  (ok - Integer.MAX_VALUE is about 2*10^9
		Node next;
		
		Node(int value){
			this.value = value;
		}
	}
}
	