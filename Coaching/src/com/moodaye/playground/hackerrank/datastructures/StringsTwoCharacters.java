package com.moodaye.playground.hackerrank.datastructures;

public class StringsTwoCharacters {
	
	public static void main(String args[]){
		maxLengthT4("as");
	}

	/** deoptimized version */
	public static int maxLengthT4(String str){
	
		// pair characters - all combinations (26 x 25)
		char[] a2z = new char[26];
		for (int i = 0; i < 26; i++)
			a2z[i] = (char) ('a' + i);
	
		int maxL = 0;
		for (int i = 0; i < 25; i++){
			for (int j = i+1; j < 26; j++){
				if (i == j) continue;
				String stripS = stripChars(str, a2z[i], a2z[j]);
				if (stripS.length() > 1 && isStringT3(stripS)){
					if (maxL < stripS.length()) 
						maxL = stripS.length();
				}
			}
		}
		return maxL;
	}
	
	/** remove all chars from str except the given 2 */
	public static String stripChars(String s, char c1, char c2){
		char[] carr = s.toCharArray();
		StringBuilder sb = new StringBuilder();
		for(char c : carr){
			if ( c == c1 || c == c2) 
				sb.append(c);
		}
		return sb.toString();
	}
	
	/** 3rd approach to checking whether string is T */
	public static boolean isStringT3(String str){
		if (str == null || str.length() == 1) return false;
		char c1 = str.charAt(0);
		char c2 = str.charAt(1);
		if (c1 == c2) return false;
		for( int i = 2; i < str.length(); i++){
			if ( i % 2 == 0){
				if (str.charAt(i) != c1) return false;
			}
			else{
				if( str.charAt(i) != c2){
					return false;
				}
			}
		}
		return true;
	}

	/** We leverage the fact that for a string to be of type T, 
	 *  we need exactly 2 distinct characters whose counts in the
	 *  string differ by zero or one.  And their occurrence within 
	 *  the string must alternate. 
	 *    
	 */
	public static int maxLengthT3(String str){
		
		//count occurrence of each char
		int[] a2z = new int['z' + 1];
		for(int i = 0; i < str.length(); i++)
			a2z[str.charAt(i)]++;
	
		char[] c = str.toCharArray();
		int maxL = 0;
		for (int i = 0; i < c.length; i++){
			if (a2z[c[i]] == 0) continue;
			for (int j = i + 1; j < c.length; j++){
				if (c[i] == c[j]){
					break; 
				}
				if (a2z[c[i]] == a2z[c[j]]  || (a2z[c[i]] - a2z[c[j]]) == 1){
					int temp = stripAllButTwo(str, c[i], c[j], i);
					maxL = temp > maxL ? temp : maxL;
				}
			}
			a2z[c[i]] = 0;
		}
		return maxL;
	}
	
	
	/** helper method to strip out all characters except 2 types 
	    StartIndex is the first occurrence of c1 or c2 */
	public static int stripAllButTwo(String str, char c1, char c2, int startIndex){
		
		char[] c = str.toCharArray();
		StringBuilder sb = new StringBuilder();
		char prevC = c[startIndex];
		sb.append(prevC);
		
		for (int i = startIndex + 1; i < c.length; i++){
			if (c[i] == prevC) return 0;
			if (c[i] == c1 || c[i] == c2){
				sb.append(c[i]);
				prevC = c[i];
			}
		}
		return sb.length();
	}
	
	
	public static int maxLengthT(String str){
		if (isStringT2(str)) return str.length();
		
		char[] a2z = new char['z' + 1];
		for(int i = 0; i < str.length(); i++)
			a2z[str.charAt(i)]++;

		int maxL = 0;
		for(int i = 0; i < str.length(); i++){
			
			if(a2z[str.charAt(i)] == 0) continue;
			else{
				String strT = removeCharFromString(str, str.charAt(i));
				if(isStringT2(strT) && strT.length() > maxL)
					maxL = strT.length();
				else{
					int tempMax = 0;
					tempMax = maxLengthT(strT);
					maxL = tempMax > maxL ? tempMax : maxL;
				}
				a2z[str.charAt(i)] = 0;
			}
		}
		return maxL;
	}
	
	public static String removeCharFromString(String str, char c){
		char[] strCarr = str.toCharArray();
		
		StringBuilder sb = new StringBuilder();
		for(int i = 0; i < strCarr.length; i++)
			if (strCarr[i] != c) sb.append(strCarr[i]);
			
		return sb.toString();
	}

	/** checks with str is of format abababab */
	public static boolean isStringT(String str){
		if (str == null || str.length() <= 1) return false;
		if (str.length() == 2 && str.charAt(0) != str.charAt(1)) return true;
		
		
		if(str.charAt(0) == str.charAt(str.length()-1) &&
				str.length() % 2 == 1)
			return isStringT(str.substring(0, str.length() - 1));
		
		else if (str.charAt(0) == str.charAt(str.length()-2) &&
				 str.charAt(1) == str.charAt(str.length() -1) &&
				str.length() % 2 == 0)
			return isStringT(str.substring(0, str.length()-2));
		
		else 
			return false;
	}
	
	/** non recursive approach */
	public static boolean isStringT2(String str){
		if (str == null || str.length() <= 1) return false;
		if (str.charAt(0) == str.charAt(1)) return false;
		if (str.length() == 2) return true;
		
		int i = 1;
		boolean isStringT = true;
		for(; i < str.length() / 2; i++){
			if(! (str.charAt(2*i)   == str.charAt(0) && 
				  str.charAt(2*i+1) == str.charAt(1))){
				isStringT = false;
				break;
			}
		}

		if(isStringT && 2*i < str.length()) 
			if(str.charAt(2*i) != str.charAt(0))
				isStringT = false;
		
		return isStringT;
	}
	
	

}
