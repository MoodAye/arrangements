package com.moodaye.playground.hackerrank.datastructures;

import static org.junit.Assert.*;
import static com.moodaye.playground.hackerrank.datastructures.StringsTwoCharacters.*;

import org.junit.Test;

public class StringsTwoCharactersTest {
	
	
	@Test
	public void testIsStringT4(){
		assertEquals(10, maxLengthT4("ababababab"));
		assertEquals(10, maxLengthT4("abcabababab"));
		assertEquals(10, maxLengthT4("abcdefdgabababab"));
		assertEquals(4, maxLengthT4("abcdab"));
		assertEquals(5, maxLengthT4("beabeefeab"));
		assertEquals(0, maxLengthT4("z"));
		assertEquals(2, maxLengthT4("ab"));
		assertEquals(2, maxLengthT4("abxd"));
		assertEquals(6, maxLengthT4("pvmaigytciycvjdhovwiouxxylkxjjyzrcdrbmokyqvsradegswrezhtdyrsyhg"));
		assertEquals(0, maxLengthT4("ezfnjymgqtjnmstbadgdsrxvntnacwljnkgchtjeaoivfcindgxipmrjuqmmcpntpotplodjhijxqpogjmzipygacfdjgmewechuebxvcbnakszzcxkozxwavzgmesrvysonomhvufezislfntgncspthcpneyminpbjildobozfirvcgdratdpmmpkujcywvtzkdytzyfejbytsobvudvutfueveevgrqnxjiwpkrvllsjxmqhotlnpgjxkjnobxfqodlyiqsisdeuwqmntxouzdtisgutdafostmwticvncjwldpknuodmfksusaqpsoosgpiveyxipfklmhypdxpdncpgaswpycoxsuxasqduojpblctcyvyxldcgzevedvxiwinfppkjbtifuuapickknwxxjmjmtxlpfalxdgepmekaxijuphqfafrnezyldokwcnzenhpibktlfuxjfmeqajmvopbhuslnnnlmkmoteceiwbytjhhxqnkuazevswrkaofggfrnapciuoexqogscugzspwuvzkyrdfkhixcaqctfwadewpqksxxvqiigvjjpagvqikuojlwhfyztu"));
		assertEquals(0, maxLengthT4("eeaao"));
	}
	
//	@Test
	public void testIsStringT3(){
		//first test stripAllButTwo
		assertEquals(0, stripAllButTwo("aaaa", 'a', 'b', 0));
		assertEquals(2, stripAllButTwo("ab", 'a', 'b', 0));
		assertEquals(3, stripAllButTwo("aba", 'a', 'b', 0));
		assertEquals(0, stripAllButTwo("abaa", 'a', 'b', 0));
		assertEquals(7, stripAllButTwo("abcdabcdaccccba", 'a', 'b', 0));
		
	//	test maxlengthT3
		assertEquals(10, maxLengthT3("ababababab"));
		assertEquals(10, maxLengthT3("abcabababab"));
		assertEquals(10, maxLengthT3("abcdefdgabababab"));
		assertEquals(4, maxLengthT3("abcdab"));
		assertEquals(5, maxLengthT3("beabeefeab"));
		assertEquals(0, maxLengthT3("z"));
		assertEquals(2, maxLengthT3("ab"));
		assertEquals(2, maxLengthT3("abxd"));
		assertEquals(6, maxLengthT3("pvmaigytciycvjdhovwiouxxylkxjjyzrcdrbmokyqvsradegswrezhtdyrsyhg"));
		assertEquals(0, maxLengthT3("ezfnjymgqtjnmstbadgdsrxvntnacwljnkgchtjeaoivfcindgxipmrjuqmmcpntpotplodjhijxqpogjmzipygacfdjgmewechuebxvcbnakszzcxkozxwavzgmesrvysonomhvufezislfntgncspthcpneyminpbjildobozfirvcgdratdpmmpkujcywvtzkdytzyfejbytsobvudvutfueveevgrqnxjiwpkrvllsjxmqhotlnpgjxkjnobxfqodlyiqsisdeuwqmntxouzdtisgutdafostmwticvncjwldpknuodmfksusaqpsoosgpiveyxipfklmhypdxpdncpgaswpycoxsuxasqduojpblctcyvyxldcgzevedvxiwinfppkjbtifuuapickknwxxjmjmtxlpfalxdgepmekaxijuphqfafrnezyldokwcnzenhpibktlfuxjfmeqajmvopbhuslnnnlmkmoteceiwbytjhhxqnkuazevswrkaofggfrnapciuoexqogscugzspwuvzkyrdfkhixcaqctfwadewpqksxxvqiigvjjpagvqikuojlwhfyztu"));
		assertEquals(0, maxLengthT3("eeaao"));
	}

//	@Test
	public void testIsStringT2() {
		assertEquals(false, isStringT2(null));
		assertEquals(false, isStringT2(""));
		assertEquals(false, isStringT2("a"));
		assertEquals(true, isStringT2("ab"));
		assertEquals(true, isStringT2("aba"));
		assertEquals(true, isStringT2("aba"));
		assertEquals(true, isStringT2("abab"));
		assertEquals(true, isStringT2("ababababa"));
		assertEquals(false, isStringT2("ababc"));
		assertEquals(false, isStringT2("ababacababababa"));
		assertEquals(false, isStringT2("abcdcdcdabababab"));
		assertEquals(false, isStringT2("aaaaa"));
	}
	
//	@Test
	public void testIsStringT() {
		assertEquals(false, isStringT(null));
		assertEquals(false, isStringT(""));
		assertEquals(false, isStringT("a"));
		assertEquals(true, isStringT("ab"));
		assertEquals(true, isStringT("aba"));
		assertEquals(true, isStringT("aba"));
		assertEquals(true, isStringT("abab"));
		assertEquals(true, isStringT("ababababa"));
		assertEquals(false, isStringT("ababc"));
		assertEquals(false, isStringT("ababacababababa"));
		assertEquals(false, isStringT("abcdcdcdabababab"));
	}
	
//	@Test
	public void testRemoveCharFromString(){
		assertEquals("Rajiv", removeCharFromString("Rajivk",'k'));
		assertEquals("pbcdefgkkk", removeCharFromString("pabcdefgakakaka",'a'));
		assertEquals("r", removeCharFromString("r",'a'));
	}
	
//	@Test
	public void testMaxLengthT(){
		assertEquals(10, maxLengthT("ababababab"));
		assertEquals(10, maxLengthT("abcabababab"));
		assertEquals(10, maxLengthT("abcdefdgabababab"));
		assertEquals(4, maxLengthT("abcdab"));
		assertEquals(5, maxLengthT("beabeefeab"));
		assertEquals(0, maxLengthT("z"));
		assertEquals(2, maxLengthT("ab"));
		assertEquals(2, maxLengthT("abxd"));
		assertEquals(6, maxLengthT("pvmaigytciycvjdhovwiouxxylkxjjyzrcdrbmokyqvsradegswrezhtdyrsyhg"));
	}

}
