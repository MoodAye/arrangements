package com.moodaye.playground.hackerrank.datastructures;

public class TreesHuffmanDecoding {
	void decode(String S ,Node root)
    {
       decode(S, root, root, 0);
    }

void decode(String S, Node root, Node nextNode, int index){
   while(index < S.length()){
       char x = S.charAt(index++);
       if (x == '0'){
           if(nextNode.left.left == null){
               System.out.print(nextNode.left.data);
               nextNode = root;
           }
           else
               nextNode = nextNode.left;
       }
       else if (x == '1'){
           if(nextNode.right.right == null){
               System.out.print(nextNode.right.data);
               nextNode = root;
           }
           else
               nextNode = nextNode.right;
       }
       else
           System.out.println("ERROR");
   }
}


}