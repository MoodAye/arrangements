package com.moodaye.playground.hackerrank.datastructures;

import java.util.Scanner;


/**
 *  Max = 30; Difficulty = M
 *
 *	max n = 6;  number of permutations 
 *  is 19 x 5 x 19 x 5 x 19 x 5 x 2 = 2 * 10 ^6
 *  
 *   We can do this a brute force way
 *  
 */
public class WeekOfCode30MelodiousPassword {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();
		
		String[] consonants = {"b","c","d","f","g","h","j","k","l","m","n","p","q","r","s"
					,"t","v","w","x","z"};
		String[] vowels = {"a","e", "i", "o", "u"};
		
		StringBuffer ans = new StringBuffer();
	
		if (n == 1){
		for(int i=0; i<20; i++){
								System.out.println(
									ans.append(consonants[i]).toString());
								ans = new StringBuffer();
							}
		
		for(int i=0; i<5; i++){
								System.out.println(
									ans.append(vowels[i]).toString());
								ans = new StringBuffer();
							}
		}
		
		if (n == 2){
		for(int i=0; i<20; i++)
			for (int j=0; j<5; j++){
								System.out.println(
									ans.append(consonants[i]).
									append(vowels[j]).toString());
								ans = new StringBuffer();
							}
		
		for(int i=0; i<5; i++)
			for (int j=0; j<20; j++){
								System.out.println(
									ans.append(vowels[i]).
									append(consonants[j]).toString());
								ans = new StringBuffer();
							}
		}
		
		if (n == 3){
		for(int i=0; i<20; i++)
			for (int j=0; j<5; j++)
				for(int k=0; k<20; k++){
								System.out.println(
									ans.append(consonants[i]).
									append(vowels[j]).
									append(consonants[k]).toString());
								ans = new StringBuffer();
							}
		
		for(int i=0; i<5; i++)
			for (int j=0; j<20; j++)
				for(int k=0; k<5; k++){
								System.out.println(
									ans.append(vowels[i]).
									append(consonants[j]).
									append(vowels[k]).toString());
								ans = new StringBuffer();
							}
		}
		
		if (n == 4){
		for(int i=0; i<20; i++)
			for (int j=0; j<5; j++)
				for(int k=0; k<20; k++)
					for (int l=0; l<5; l++){
								System.out.println(
									ans.append(consonants[i]).
									append(vowels[j]).
									append(consonants[k]).
									append(vowels[l]).toString());
								ans = new StringBuffer();
							}
		
		for(int i=0; i<5; i++)
			for (int j=0; j<20; j++)
				for(int k=0; k<5; k++)
					for (int l=0; l<20; l++){
								System.out.println(
									ans.append(vowels[i]).
									append(consonants[j]).
									append(vowels[k]).
									append(consonants[l]).toString());
								ans = new StringBuffer();
							}
					}
		if (n == 5){
		for(int i=0; i<20; i++)
			for (int j=0; j<5; j++)
				for(int k=0; k<20; k++)
					for (int l=0; l<5; l++)
						for(int m=0; m<20; m++){
								System.out.println(
									ans.append(consonants[i]).
									append(vowels[j]).
									append(consonants[k]).
									append(vowels[l]).
									append(consonants[m]).toString());
								ans = new StringBuffer();
							}
		
		for(int i=0; i<5; i++)
			for (int j=0; j<20; j++)
				for(int k=0; k<5; k++)
					for (int l=0; l<20; l++)
						for(int m=0; m<5; m++){
								System.out.println(
									ans.append(vowels[i]).
									append(consonants[j]).
									append(vowels[k]).
									append(consonants[l]).
									append(vowels[m]).toString());
								ans = new StringBuffer();
							}
		}
		
		if (n == 6){
		for(int i=0; i<20; i++)
			for (int j=0; j<5; j++)
				for(int k=0; k<20; k++)
					for (int l=0; l<5; l++)
						for(int m=0; m<20; m++)
							for (int o=0; o<5; o++){
								System.out.println(
									ans.append(consonants[i]).
									append(vowels[j]).
									append(consonants[k]).
									append(vowels[l]).
									append(consonants[m]).
									append(vowels[o]).toString());
								ans = new StringBuffer();
							}
		
		for(int i=0; i<5; i++)
			for (int j=0; j<20; j++)
				for(int k=0; k<5; k++)
					for (int l=0; l<20; l++)
						for(int m=0; m<5; m++)
							for (int o=0; o<20; o++){
								System.out.println(
									ans.append(vowels[i]).
									append(consonants[j]).
									append(vowels[k]).
									append(consonants[l]).
									append(vowels[m]).
									append(consonants[o]).toString());
								ans = new StringBuffer();
							}
		}
	}
}
