package com.moodaye.playground.hackerrank.java;

import java.io.File;
import java.io.PrintWriter;
import java.util.Scanner;

public class OneDArray {
	
	void solve(Scanner in, PrintWriter out){
	        int T = in.nextInt();
	        
	        for (int i = 0; i < T; i++){
	            int n = in.nextInt();
	            int m = in.nextInt();
	            int[] arr = new int[n];
	            boolean[] pt = new boolean[n];
	            
	            for (int k = 0; k < n; k++){
	                arr[k] = in.nextInt();
	                pt[k] = false;
	            }
	           
	            if(nextPosition(arr, 0, n, m, pt) == n) out.println("YES");
	            else out.println("NO");
	            
	        }
	        
	    }
	   
	    /** returns the next viable position */
	    private static int nextPosition(int[] arr, int currPos, int n, int m, boolean[] posTracker){
	        if (currPos + 1 > n - 1 || currPos + m > n - 1) return n;
	        int nextPos = -1;
	        
	        posTracker[currPos] = true;
	        
	        //option 1 (+1)
	        if (posTracker[currPos + 1] != true && arr[currPos + 1] != 1) {
	            if(nextPosition(arr, currPos + 1, n, m, posTracker) == n) return n;
	        }
	        
	        //option 2 (+m)
	        if (posTracker[currPos + m] != true && arr[currPos + m] != 1) {
	            if(nextPosition(arr, currPos + m, n, m, posTracker) == n) return n;
	        }
	        
	        //option 3 (-1)
	        if ( (currPos - 1 != -1) && (posTracker[currPos - 1] != true) && (arr[currPos - 1] != 1)) {
	            if (nextPosition(arr, currPos - 1, n, m, posTracker) == n) return n;
	        }
	        
	        return nextPos;
	     }
		
	void run() throws Exception{
		try(
//				Scanner in = new Scanner(new File("C://Users/Rajiv/Desktop/test1.txt"));
				Scanner in = new Scanner(System.in);

				PrintWriter out = new PrintWriter(System.out);
//				PrintWriter out = new PrintWriter(new File("C://Users/Rajiv/Desktop/test1-out.txt"));
			){
			solve(in, out);
		}
	}
	
	public static void main(String[] args) throws Exception {
		new OneDArray().run();
	}
}
