package com.moodaye.playground.hackerrank.java;

import java.io.PrintWriter;
import java.util.BitSet;
import java.util.Scanner;

public class JavaBitSet {
	
	public void solve(Scanner in, PrintWriter out){
		int N = in.nextInt();
		int M = in.nextInt();
		BitSet bs1 = new BitSet();
		BitSet bs2 = new BitSet();
		
		for(int m = 0; m < M; m++){
			String opType = in.next();
			int op1 = in.nextInt();
			int op2 = in.nextInt();
			switch(opType){
				case "AND":
					if (op1 == 1) bs1.and(bs2);
					else          bs2.and(bs1);
					break;
	            case "OR":
	            	if (op1 == 1) bs1.or(bs2);
	            	else          bs2.or(bs1);
	            	break;
	            case "XOR":
	            	if (op1 == 1) bs1.xor(bs2);
	            	else          bs2.xor(bs1);
	            	break;
	            case "FLIP":
	            	if (op1 == 1) bs1.flip(op1);
	            	else          bs2.flip(op2);
	            	break;
	            case "SET":
	            	if (op1 == 1) bs1.set(op2);
	            	else          bs2.set(op2);
	            	break;
	            }
	            System.out.println(bs1.cardinality() + " " + bs2.cardinality());
		}
		
	}
	
	public void solveWithMyBSImplementations(Scanner in, PrintWriter out){
		int N = in.nextInt();
	        int M = in.nextInt();
	        boolean[] bs1 = new boolean[N];
	        boolean[] bs2 = new boolean[N];
	        
	        for(int m = 0; m < M; m++){
	            String opType = in.next();
	            int op1 = in.nextInt();
	            int op2 = in.nextInt();
	            switch(opType){
	                case "AND":
	                    if (op1 == 1) doAnd(bs1,bs2);
	                    else          doAnd(bs2,bs1);
	                    break;
	                case "OR":
	                    if (op1 == 1) doOr(bs1,bs2);
	                    else          doOr(bs2,bs1);
	                    break;
	                case "XOR":
	                    if (op1 == 1) doXor(bs1,bs2);
	                    else          doXor(bs2,bs1);
	                    break;
	                case "FLIP":
	                    if (op1 == 1) doFlip(bs1,op2);
	                    else          doFlip(bs2,op2);
	                    break;
	                case "SET":
	                    if (op1 == 1) doSet(bs1, op2);
	                    else          doSet(bs2, op2);
	                    break;
	            }
	            System.out.println(cntSb(bs1) + " " + cntSb(bs2));
	        }
	}
	
	 public static void main(String[] args) {
		 try(
	        Scanner in = new Scanner(System.in);
			PrintWriter out = new PrintWriter(System.out);
		 ){
			 new JavaBitSet().solve(in, out);
		 }
	        
	    }
	    
	    public static void doAnd(boolean[] bs1, boolean[] bs2){
	        for(int i = 0; i < bs1.length; i++)
	            bs1[i] = (bs1[i] && bs2[i]);
	    }

	    
	    public static void doOr(boolean[] bs1, boolean[] bs2){
	        for(int i = 0; i < bs1.length; i++)
	            bs1[i] = (bs1[i] || bs2[i]);
	    }
	        
	    public static void doXor(boolean[] bs1, boolean[] bs2){
	        for(int i = 0; i < bs1.length; i++)
	            bs1[i] = (bs1[i] ^ bs2[i]);
	    }
	        
	    public static void doFlip(boolean[] bs, int index){
	        bs[index] = !bs[index];
	    }
	        
	    public static void doSet(boolean[] bs, int index){
	        bs[index] = true;
	    }

	    public static int cntSb(boolean[] bs){
	        int cnt = 0;
	        for( boolean b : bs ) if (b) cnt++;
	        return cnt;
	    }
}
