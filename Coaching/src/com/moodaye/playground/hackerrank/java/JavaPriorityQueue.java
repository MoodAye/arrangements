package com.moodaye.playground.hackerrank.java;

import java.util.PriorityQueue;
import java.util.Scanner;

public class JavaPriorityQueue {
	 public static void main(String[] args) {
	      Scanner in = new Scanner(System.in);
	      int totalEvents = Integer.parseInt(in.nextLine());
//	      PriorityQueue<Student> pq = new PriorityQueue<>(1000, new Comparator<Student>(){
	      PriorityQueue<Student> pq = new PriorityQueue<>(1000, (Student s1,Student s2) -> {
	                if (s1.getCgpa() != s2.getCgpa()) {
	                    if (s1.getCgpa() < s2.getCgpa()) return 1;
	                    else return -1;
	                }
	                if (!s1.getFname().equals(s2.getFname())){
	                    return s1.getFname().compareTo(s2.getFname());
	                }
	                return s1.getToken() - s2.getToken();
	            });
	      while(totalEvents>0){
	         String event = in.next();
	        if (event.equals("ENTER")){
	            String fname = in.next();
	            double cgpa = in.nextDouble();
	            int id = in.nextInt();
	            Student s = new Student(id, fname, cgpa);
	            pq.add(s);
	        }
	        else pq.poll();
	         totalEvents--;
	      }
	       
	        if (pq.size() != 0)
	            while(pq.size() > 0)
	                System.out.println(pq.poll().getFname());
	        else
	            System.out.println("EMPTY");
	    }
}

class Student{
	   private int token;
	   private String fname;
	   private double cgpa;
	   public Student(int id, String fname, double cgpa) {
	      super();
	      this.token = id;
	      this.fname = fname;
	      this.cgpa = cgpa;
	   }
	   public int getToken() {
	      return token;
	   }
	   public String getFname() {
	      return fname;
	   }
	   public double getCgpa() {
	      return cgpa;
	   }
	}

