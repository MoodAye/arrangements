package com.moodaye.playground.hackerrank.java;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class FeedZai {
	
	void solve(Scanner in, PrintWriter out){
		String[] input = {"rajiv", "ajivr", "aa"};
		String[] output = friendlyWords(input);
		
		for (String s : output)
			System.out.println(s);
	}
	
	
	
	void run() throws Exception{
		try(
				Scanner in = new Scanner(System.in);
				PrintWriter out = new PrintWriter(System.out);
			){
			solve(in, out);
		}
	}
	
	public static void main(String[] args) throws Exception {
		new FeedZai().run();
	}
	
	static String[] friendlyWords(String[] input){
		
		Map<String, List<String>> hm = new HashMap<>();
		for (int i=0; i<input.length; i++){
			String s = input[i];
			char[] c = s.toCharArray();
			Arrays.sort(c);
			String sortS = String.valueOf(c);
			if (hm.containsKey(sortS)){
				hm.get(sortS).add(s);
			}
			else{
				List<String> al = new ArrayList<>();
				al.add(s);
				hm.put(sortS, al);
			}
		}
		
		
		ArrayList<String> al = new ArrayList<>();
	
		for(List<String> list : hm.values()){
			if (list.size() <= 1) continue;
			StringBuilder sb = new StringBuilder();
			for (String s : list){
				sb.append(s).append(" ");
			}
			al.add(sb.toString());
		}
		
		String[] returnedS = new String[al.size()];
		
		int i = 0;
		for(String s : al)
			returnedS[i] = al.get(i++);
		
		return returnedS;
		
	}
}
