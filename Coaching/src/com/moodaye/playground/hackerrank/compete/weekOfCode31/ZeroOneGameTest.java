package com.moodaye.playground.hackerrank.compete.weekOfCode31;

import static org.junit.Assert.*;

import org.junit.AfterClass;
import org.junit.Test;

public class ZeroOneGameTest {

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Test
	public void testBreakUpSequence() {
		int[] s1 = {0};
		int[] s2 = {0,0};
		int[] s3 = {0,0,0};
		assertEquals(0,ZeroOneGame.breakUpSequence(s1));
		assertEquals(0,ZeroOneGame.breakUpSequence(s2));
		assertEquals(1,ZeroOneGame.breakUpSequence(s3));
		
		int[] s4 = {1};
		int[] s5 = {1,1};
		int[] s6 = {1,1,1};
		assertEquals(0,ZeroOneGame.breakUpSequence(s4));
		assertEquals(0,ZeroOneGame.breakUpSequence(s5));
		assertEquals(0,ZeroOneGame.breakUpSequence(s6));
	
		int[] s7 = {0,1};
		int[] s8 = {1,0};
		int[] s9 = {1,0,1,1,0,1,1};
		assertEquals(0,ZeroOneGame.breakUpSequence(s7));
		assertEquals(0,ZeroOneGame.breakUpSequence(s8));
		assertEquals(0,ZeroOneGame.breakUpSequence(s9));
	}
}
