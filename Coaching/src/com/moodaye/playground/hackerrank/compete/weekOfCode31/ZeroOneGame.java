package com.moodaye.playground.hackerrank.compete.weekOfCode31;


	import java.io.PrintWriter;
	import java.util.Scanner;

	public class ZeroOneGame {

		private static void run(){
			Scanner in = new Scanner(System.in);
			PrintWriter out = new PrintWriter(System.out);
			solve(in, out);
		}
		
		private static void solve(Scanner in, PrintWriter out){
			int g = in.nextInt();
			for (int i = 0; i < g; i++){
				int n = in.nextInt();
				int[] sequence = new int[n];
				for(int j = 0; j < n; j++){
					sequence[j] = in.nextInt();
				}
				//break up array into string of 0 and 1 delimited by 11... 
				int count = breakUpSequence(sequence);
			}
		}
		
		public static int breakUpSequence(int[] sequence){
			if (sequence.length < 3) return 0;
			
			int count = 0;
			int i = 2;
			boolean oneFound = false;
			boolean oneFoundAgain = false;
			boolean firstZero = false;
			boolean secondZero = false;
			
			while ( i < sequence.length ){
				
				if (sequence[i] == 1){
					if ((oneFound == true)){
						i++;
						oneFoundAgain = true;
						continue;
					}
					else{
						oneFound = true;
						i++;
						continue;
					}
				}
				else{
					if(secondZero == true){
						secondZero = false;
						continue;
					}
					if(firstZero == true){
						firstZero = false;
						secondZero = true;
						continue;
					}
					if (oneFound == true){
						if(oneFoundAgain != true){
							count++;
						}
						else{
							firstZero = true;
						}
					}
					count++;
					i++;
				}
			}
			return count;
		}
		
		
		public static void main(String args[]){
			new ZeroOneGame().run();
		}
	}
