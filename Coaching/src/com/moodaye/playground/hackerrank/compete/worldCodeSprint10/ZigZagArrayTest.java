package com.moodaye.playground.hackerrank.compete.worldCodeSprint10;

import static org.junit.Assert.*;

import org.junit.Test;

public class ZigZagArrayTest {

	@Test
	public void test() {
		int[] a = {1,2,0};
		assertEquals(0, ZigZagArray.minimumDeletions(a));
		
		int[] b = {1,2,3,0};
		assertEquals(1, ZigZagArray.minimumDeletions(b));
		
		int[] c = {4,2,6,3,10,1};
		assertEquals(0, ZigZagArray.minimumDeletions(c));
		
		int[] d = {5,2,3,6,1};
		assertEquals(1, ZigZagArray.minimumDeletions(d));
	}

}
