package com.moodaye.playground.hackerrank.algorithms.search;

import java.util.Iterator;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

public class MinimumLoss {
	
	public static void main2(String[] args) {
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();
		long[] p = new long[n];
		for (int i = 0; i < n; i++)
			p[i] = in.nextLong();
	
		long min = Long.MAX_VALUE;
		for(int i = 0; i < n-1; i++){
			for (int j = i+1; j<n; j++){
				long diff = p[i] - p[j];
				if(diff > 0 && diff < min)
					min = diff;
			}
		}
		System.out.println(min);
	}
	
	public static void main(String args[]){
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();
		TreeMap<Long, Integer> tm = new TreeMap<>();
		for (int i = 0; i < n; i++)
			tm.put(in.nextLong(), i);
		
		int i = 0;
	
		long min = Long.MAX_VALUE;
		long prevKey = 0;
		int prevIndex = 0;
		long currKey = 0;
		int currIndex = 0;
		
		for(Map.Entry<Long, Integer> entry 	: tm.entrySet()){
			if (i == 0){
				prevKey = entry.getKey();
				prevIndex = entry.getValue();
				i++;
				continue;
			}
			currKey = entry.getKey();
			currIndex = entry.getValue();
			long diff = currKey - prevKey;
			if(currIndex < prevIndex && diff < min){
				min = diff;
			}
			prevKey = currKey;
			prevIndex = currIndex;
		}
		System.out.println(min);
	}
}