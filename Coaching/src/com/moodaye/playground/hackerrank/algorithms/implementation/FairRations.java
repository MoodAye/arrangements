package com.moodaye.playground.hackerrank.algorithms.implementation;

import java.util.Scanner;

public class FairRations {

	 public static void main(String[] args) {
	        Scanner in = new Scanner(System.in);
	        int N = in.nextInt();
	        int B[] = new int[N];
	        for(int B_i=0; B_i < N; B_i++){
	            B[B_i] = in.nextInt();
	        }
	        int loafCount = 0;
	        for (int i = 0; i < B.length - 1; i++){
	           if (B[i] % 2 == 0) continue;
	           else{
	               B[i]++; 
	               B[i+1]++; 
	               loafCount += 2;
	           }
	        }
	        if (B[B.length - 1] % 2 == 1) System.out.println("NO");
	        else System.out.println(loafCount);
	    }
}
