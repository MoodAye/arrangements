package com.moodaye.playground.hackerrank.algorithms.implementation;

import java.util.Scanner;

public class CatsAndMouse {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int q = in.nextInt();
        for(int a0 = 0; a0 < q; a0++){
            int x = in.nextInt();
            int y = in.nextInt();
            int z = in.nextInt();
            
            int distAC = (z - x) * (z - x);
            int distBC = (z - y) * (z - y);
            if ( distAC > distBC) System.out.println("Cat B");
            else if ( distAC < distBC) System.out.println("Cat A");
            else System.out.println("Mouse C");
        }
    }
}
