package com.moodaye.playground;

public class QuickUnion {
	int[] forest;
	
	public QuickUnion(int n) {
		forest = new int[n];
		for(int i = 0; i < n; i++) {
			forest[i] = i;
		}
	}
	
	private int root(int p) {
		while(forest[p] != p) {
			p = forest[p];
		}
		return p;
	}
	
	public boolean connected(int p, int q) {
		return find(p) == find(q);
	}

	// return id of group
	public int find(int p) {
		return root(p);
	}
	
	// makes them same group
	public void union(int p, int q) {
		int gp = find(p);
		int gq = find(q);
		forest[gq] = gp;
	}
	
}
