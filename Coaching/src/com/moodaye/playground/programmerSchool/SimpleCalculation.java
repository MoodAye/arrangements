package com.moodaye.playground.programmerSchool;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

/** Programmer's School Problem #59 */
/** Based on remainder method --- see http://www.oxfordmathcenter.com/drupal7/node/18 */
/** time complexity = O(n/k);  space complexity = constant */
public class SimpleCalculation {

	void solve(Scanner in, PrintWriter out) {
		int base10 = in.nextInt();
		int newBase = in.nextInt();
		long sum = 0L;
		long prod = 1L;
		
		while ( base10 > 0 ) {
			int rem = base10 % newBase;
			sum += rem;
			prod *= rem;
			base10 = base10 / newBase;
		} 
		out.println(prod - sum);
	}

	public static void main(String[] args) {
		new SimpleCalculation().run();
	}
	
	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}