package com.moodaye.playground.programmerSchool.Y202001;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;

// Problem 273
public class StrikingOut {
	void solve(Scanner in, PrintWriter out) {
		String n = in.next();
//		out.println(method1(n.toCharArray()));
		out.println(method2(n));
	}
	
	/* simply check if any number from 100 to 999
	 * is part of the string!
	 */
	public static int method2(String n) {
		int cnt = 0;
		for(int i = 100; i < 1000; i++) {
			String regex = "[0-9]*" + i / 100 + "[0-9]*" + (i % 100) / 10 + "[0-9]*" 
							+ (i % 10) + "[0-9]*";
			if(n.matches(regex)) {
				cnt++;
			}
		}
		return cnt;
	}

	public static int method1(char[] n) {
		Set<Integer> list = new TreeSet<>();

		for (int first = 0; first < n.length - 2; first++) {
			if (n[first] == '0') {
				continue;
			}
			for (int second = first + 1; second < n.length - 1; second++) {
				for (int third = second + 1; third < n.length; third++) {
					int number = (n[first] - '0') * 100 + (n[second] - '0') * 10 + (n[third] - '0');
					list.add(number);
				}
			}
		}
		return list.size();
	}

	public static void main(String[] args) {
		new StrikingOut().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
