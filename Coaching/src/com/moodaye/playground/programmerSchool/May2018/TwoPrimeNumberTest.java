package com.moodaye.playground.programmerSchool.May2018;

import static org.junit.Assert.*;
import static com.moodaye.playground.programmerSchool.May2018.TwoPrimeNumber.*;

import org.junit.AfterClass;
import org.junit.Test;
public class TwoPrimeNumberTest {

	@Test
	public void testReverse() {
		assertEquals(23L, reverse(32L));
		assertEquals(1L, reverse(1L));
		assertEquals(12345L, reverse(54321L));
		assertEquals(123456789123L, reverse(321987654321L));
	}
	
	@Test
	public void testArePrime(){
		assertTrue(!arePrime(4,9));
		assertTrue(arePrime(7, 53));
		assertTrue(arePrime(2, 5));
		assertTrue(arePrime(2, 3));
		assertTrue(!arePrime(7, 4));
		assertTrue(arePrime(47, 53));
		
	}
	@Test
	public void testArePrime2(){
		assertTrue(!arePrime2(4,9));
		assertTrue(arePrime2(7, 53));
		assertTrue(arePrime2(2, 5));
		assertTrue(arePrime2(2, 3));
		assertTrue(!arePrime2(7, 4));
		assertTrue(arePrime2(47, 53));
		
	}
}
