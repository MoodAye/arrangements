package com.moodaye.playground.programmerSchool.May2018;

import java.util.Scanner;
import java.util.Locale;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;

/** Programmer's School #408 */
// Failing on test 29
public class Letter {
	void solve(Scanner in, PrintWriter out) {
		int w = in.nextInt();
		int n = in.nextInt();
		String[] answers = new String[n];
		boolean isPossible = true;
		in.nextLine();
		for (int j = 0; j < n; j++) {
			String line = in.nextLine();
			int firstIndex = -1;
			int lastIndex = -1;
			for (int i = 0; i < line.length(); i++) {
				if (line.charAt(i) == ' ') {
					continue;
				}
				firstIndex = i;
				break;
			}

			for (int i = line.length() - 1; i >= 0; i--) {
				if (line.charAt(i) == ' ') {
					continue;
				}
				lastIndex = i;
				break;
			}

			int middle = lastIndex - firstIndex + 1;
			if (middle > w) {
				isPossible = false;
				break;
			}
			int padding = w - middle;

			// create formatted line;
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < padding / 2; i++) {
				sb.append(' ');
			}
			sb.append(line.substring(firstIndex, lastIndex + 1));
			for (int i = 0; i < (padding + 1) / 2; i++) {
				sb.append(' ');
			}
			answers[j] = sb.toString();
		}
		if (isPossible) {
			for (String s : answers) {
				out.println(s);
			}
		} else {
			out.println("Impossible.");
		}

	}

	public static void main(String args[]) {
		new Letter().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		// try (Scanner in = new Scanner(System.in); PrintWriter out = new
		// PrintWriter(System.out)) {
		// solve(in, out);
		// }
		try (Scanner in = new Scanner(new InputStreamReader(System.in, "cp866"));
				PrintWriter out = new PrintWriter(new OutputStreamWriter(System.out, "cp866"))) {
			solve(in, out);
		} catch (UnsupportedEncodingException e) {
		}
	}
}
