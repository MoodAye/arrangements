package com.moodaye.playground.programmerSchool.May2018;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #166 */
// Start 8:40. End Time = 9:34pm
// Wrong answer test 5 - now passing
// Wrong answer test 6
// Wrong answer on test 5 again. 
// Finally passing all test cases ... total time ~ > 2hrs
public class RobotsCommunity {
	void solve(Scanner in, PrintWriter out) {
		int k = in.nextInt();
		int n = in.nextInt();

		if (k == 1 || k == 2) {
			if (n <= 3) {
				out.println(k);
			} else {
				out.println(0);
			}
			return;
		}

		int[] cntRobots = new int[4];
		cntRobots[1] = k;

		for (int i = 1; i <= n; i++) {
			k = cntRobots[0] + cntRobots[1] + cntRobots[2];
			if (k == 4) {
				cntRobots[2] = 5;
				continue;
			}
			if (k == 7) {
				cntRobots[2] = 10;
				continue;
			}

			int robots5 = k / 5;
			int robots3 = (k % 5) / 3;
			while (robots5 * 5 + robots3 * 3 != k) {
				robots5--;
				robots3 = (k - robots5 * 5) / 3;
			}
			cntRobots[(i + 1) % 3] = robots5 * 9 + robots3 * 5;
		}
		out.println(k);
	}

	public static void main(String args[]) {
		new RobotsCommunity().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}