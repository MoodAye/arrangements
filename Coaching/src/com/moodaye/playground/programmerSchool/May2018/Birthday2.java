package com.moodaye.playground.programmerSchool.May2018;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #376 */
// Time Taken = 35min
public class Birthday2 {
	void solve(Scanner in, PrintWriter out) {
		int bday = in.nextInt();
		int bmon = in.nextInt();
		int cday = in.nextInt();
		int cmon = in.nextInt();
		int cyr = in.nextInt();
	
		int cnt = 0;
		while(bday != cday || bmon != cmon){
			cnt++;
			cday++;
			if(cday == 32){
				cday = 1;
				cmon++;
				if(cmon == 13){
					cmon = 1;
					cyr++;
				}
			}
			else if(cday == 31){
				if(cmon == 4 || cmon == 6 || cmon == 9 || cmon == 11){
					cday = 1;
					cmon++;
				}
			}
			else if(cday == 30 && cmon == 2){
				cday = 1;
				cmon++;
			}
			else if(cday == 29 && cmon == 2 && !isLeap(cyr)){
				cday = 1;
				cmon++;
			}
		}
		out.println(cnt);
	}
	
	public static boolean isLeap(int yr){
		return yr % 400 == 0 || (yr % 4 == 0 && yr % 100 != 0);
	}

	public static void main(String args[]) {
		new Birthday2().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
