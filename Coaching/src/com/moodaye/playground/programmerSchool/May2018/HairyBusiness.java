package com.moodaye.playground.programmerSchool.May2018;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #39 */
// Start Time = 11:57am End Time = 12:26pm Time Take = 29min
// Reworked solution for O(n) complexity ... additional time = ~20 min
public class HairyBusiness {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int[] a = new int[n];
		for(int i = 0; i < n; i++){
			a[i] = in.nextInt();
		}
		int earnings = 0;
		int lastMax = 0;
		for(int i = n - 1; i >= 0; i--){
			if(a[i] > lastMax){
				lastMax = a[i];
			}
			earnings += lastMax;
		}
		out.println(earnings);
	}

	public static void main(String args[]) {
		new HairyBusiness().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
