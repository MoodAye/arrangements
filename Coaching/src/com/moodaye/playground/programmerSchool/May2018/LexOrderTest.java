package com.moodaye.playground.programmerSchool.May2018;

import static org.junit.Assert.*;

import org.junit.Test;
import static com.moodaye.playground.programmerSchool.May2018.LexOrder.*;
public class LexOrderTest {

	@Test
	public void testLess() {
//		assertTrue(less(1, 10));
		assertTrue(less(1, 2));
		assertTrue(less(10, 100));
		assertTrue(less(100, 1000));
		assertTrue(less(2, 20));
		assertTrue(less(10, 9));
		assertTrue(less(2, 9));
		assertTrue(less(2, 20));
		assertTrue(less(8999,9));
		assertTrue(less(1,2));
		assertTrue(less(10,2));
		assertTrue(less(100,2));
		assertTrue(less(1000,2));
	}
	
	@Test
	public void testHeapSort() {
		int[] a0 = {10};
		int[] b0 = {10};
		heapSort(a0);
		assertArrayEquals(a0, b0);
		
		int[] a1 = {10,1};
		int[] b1 = {1,10};
		heapSort(a1);
		assertArrayEquals(a1, b1);
		
		int[] a2 = {1,2,100};
		int[] b2 = {1,100,2};
		heapSort(a2);
		assertArrayEquals(a2, b2);
		
		int[] a3 = {1,10, 100};
		int[] b3 = {1,10,100};
		heapSort(a3);
		assertArrayEquals(a3, b3);
		
		int[] a4 = {2,10};
		int[] b4 = {10,2};
		heapSort(a4);
		assertArrayEquals(a4, b4);
		
		int[] a5 = {1,2,3};
		int[] b5 = {1,2,3};
		heapSort(a5);
		assertArrayEquals(a5, b5);
		
		
		int[] a6 = {1,2,3,4};
		int[] b6 = {1,2,3,4};
		heapSort(a6);
		assertArrayEquals(a6, b6);
		
		
		int[] a7 = {1000,2,10,100};
		int[] b7 = {10,100,1000,2};
		heapSort(a7);
		assertArrayEquals(a7, b7);
		
		int[] a = {2,3,4,10,100};
		int[] b = {10,100,2,3,4};
		heapSort(a);
		assertArrayEquals(a, b);
		
	}

}
