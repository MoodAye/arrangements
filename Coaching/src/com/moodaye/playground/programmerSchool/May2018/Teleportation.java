package com.moodaye.playground.programmerSchool.May2018;

import java.util.Scanner;
import java.io.PrintWriter;
import java.util.Locale;

/** Programmer's School #330 */
// Time Taken ~30min
public class Teleportation {
	void solve(Scanner in, PrintWriter out) {
		int x1 = in.nextInt();
		int y1 = in.nextInt();
		int x2 = in.nextInt();
		int y2 = in.nextInt();

		int dx = x1 - x2;
		int dy = y1 - y2;

		if ((dx - dy) % 2 == 0) {
			if (dx * dx == dy * dy) {
				out.println(1);
			} else {
				out.println(2);
			}
		} else {
			out.println(0);
		}
	}

	public static void main(String[] args) {
		new Teleportation().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}

}
