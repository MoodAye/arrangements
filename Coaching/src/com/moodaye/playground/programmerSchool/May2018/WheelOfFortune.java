package com.moodaye.playground.programmerSchool.May2018;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #17 */
// Time Taken = 2hr+ (includes time for fixing issues with code)
public class WheelOfFortune {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int[] a = new int[n];
		for (int i = 0; i < n; i++) {
			a[i] = in.nextInt();
		}
		int len = 0;
		for (int i = 0; i < n - 1; i++) {
			boolean patternExists = true;
			if((n - 1) % (i + 1) != 0) {
				continue;
			}
			len = i + 1;
			for(int j = len; j < a.length - 1; j++) {
				if(a[j] != a[j - len]){
					patternExists = false;
				}
			}
			if (patternExists) {
				break;
			}
		}
		out.println(len);
	}

	public static void main(String args[]) {
		new WheelOfFortune().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
