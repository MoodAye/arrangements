package com.moodaye.playground.programmerSchool.May2018;

import java.util.Scanner;
import java.util.function.ToIntBiFunction;
import java.util.Arrays;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #682 */

/*
TRYING TO FIND PATTERN
4       5
49      05
49 4    550
49 49   5500
49 499  55000
49 4999 550000
*/
public class SumNDigitNumbers {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		if (n > 1) {
			int[] start = new int[n];
			start[0] = 1;
			int[] end = new int[n];
			Arrays.fill(end, 9);
			String sum = add(toString(start), toString(end));
			int[] f = new int[n];
			f[0] = 4;
			f[1] = 5;
			out.println(multiply(toIntArray(sum), f));
		}
		else{
			out.println((1 + 9) / 2 * 9);
		}
	}

	static int[] toIntArray(String sn) {
		int[] n = new int[sn.length()];
		for (int i = 0; i < sn.length(); i++) {
			n[i] = sn.charAt(i) - '0';
		}
		return n;
	}

	static String toString(int[] n) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < n.length; i++) {
			sb.append(n[i]);
		}
		return sb.toString();
	}

	public static String add(String sn1, String sn2) {
		int[] n1 = new int[Math.max(sn1.length(), sn2.length())];
		int[] n2 = new int[Math.max(sn1.length(), sn2.length())];

		for (int i = 0; i < sn1.length(); i++) {
			n1[n1.length - sn1.length() + i] = sn1.charAt(i) - '0';
		}

		for (int i = 0; i < sn2.length(); i++) {
			n2[n2.length - sn2.length() + i] = sn2.charAt(i) - '0';
		}

		int[] sum = new int[n1.length + 1];
		int carry = 0;
		for (int i = sum.length - 1; i > 0; i--) {
			sum[i] = (n1[i - 1] + n2[i - 1] + carry) % 10;
			carry = (n1[i - 1] + n2[i - 1] + carry) / 10;
		}
		sum[0] = carry;

		StringBuilder sb = new StringBuilder();
		for (int i = sum[0] == 0 ? 1 : 0; i < sum.length; i++) {
			sb.append(sum[i]);
		}

		return sb.toString();
	}
	
	public static String multiply(String sn1, String sn2){
		return multiply(toIntArray(sn1), toIntArray(sn2));
	}
	
	public static String multiply2(String sn1, String sn2){
		return multiply2(toIntArray(sn1), toIntArray(sn2));
	}

	// should we change parameters to byte to save space?
	public static String multiply(int[] n1, int[] n2) {
		int[] ans = new int[n1.length + n2.length];
		for (int outer = n1.length - 1; outer >= 0; outer--) {
			int carry = 0;
			int index = 0;
			for (int inner = n2.length - 1; inner >= 0; inner--) {
				index = ans.length - 1 - (n1.length - inner - 1 + n2.length - outer - 1);
				int digit = n1[outer] * n2[inner] + carry + ans[index];
				carry = digit / 10;
				digit %= 10;
				ans[index] = digit;
			}
			ans[index - 1] += carry;
		}
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < ans.length; i++) {
			if (sb.length() == 0 && ans[i] == 0) {
				continue;
			}
			sb.append(ans[i]);
		}
		if (sb.length() == 0) {
			sb.append("0");
		}
		return sb.toString();
	}
	
	// here least significant digit is n[0]
	public static String multiply2(int[] n1, int[] n2) {
		int[] ans = new int[n1.length + n2.length];
		for (int outer = 0; outer < n1.length; outer++) {
			int carry = 0;
			int index = 0;
			for (int inner = 0; inner < n2.length; inner++) {
				index = inner + outer;
				int digit = n1[outer] * n2[inner] + carry + ans[index];
				carry = digit / 10;
				digit %= 10;
				ans[index] = digit;
			}
			ans[index + 1] += carry;
		}
		StringBuilder sb = new StringBuilder();
		for (int i = ans.length - 1; i >= 0; i--) {
			if (sb.length() == 0 && ans[i] == 0) {
				continue;
			}
			sb.append(ans[i]);
		}
		if (sb.length() == 0) {
			sb.append("0");
		}
		return sb.toString();
	}
	
	public static void main(String args[]) {
		new SumNDigitNumbers().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}

}
