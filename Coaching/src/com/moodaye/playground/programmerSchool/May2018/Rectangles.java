package com.moodaye.playground.programmerSchool.May2018;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #282 */
// Start Time = 10:20pm  End Time = 10:47  Time Taken = 27minutes
public class Rectangles {
	/*  
	 * Reduces to figuring out how many points in grid
	 * have x and y co-ordinates greater than each point.
	 * If W = H = 2; then we have the following points:
	 * 0,0    0,1    0,2 
	 * 1,0    1,1    1,2 
	 * 2,0    2,1    2,2 
	 *	
	 * There are 4 points having x and y greater than 0,0   (or (2 - 0) * (2 - 0))
	 * There are 2 points having x and y greater than 1,0   (or (2 - 1) * (2 - 0))
	 * There are 0 points having x and y greater than 2,0   (or (2 - 2) * (2 - 0))
	 * There are 2 points having x and y greater than 0,1   (or (2 - 0) * (2 - 1))
	 * There are 1 points having x and y greater than 1,1   (or (2 - 1) * (2 - 0))
	 * There are 0 points having x and y greater than 2,1   (or (2 - 1) * (2 - 1))
	 * There are 0 points having x and y greater than 0,2   (or (2 - 0) * (2 - 2))
	 * There are 0 points having x and y greater than 1,2   (or (2 - 1) * (2 - 2))
	 * There are 0 points having x and y greater than 2,2   (or (2 - 2) * (2 - 2))
	 * Total = 9.
	 * 
	 * So we can apply the formula Sum of (w - x) * (h - y)
	 * for 0 <= x < w  and 0 <= y < h
	 *
	 * Complexity is O(n^2).  With max h and w = 1_000 
	 * the number of times the loop will run = (1_000 * 1_000)
	 * 
	 * Number of operations is about 1e6.  So we have enough time.
	 */
	void solve(Scanner in, PrintWriter out) {
		int w = in.nextInt();
		int h = in.nextInt();
		long sum = 0;
		for(int x = 0; x < w; x++){
			for(int y = 0; y < h; y++){
				sum += (w - x) * (h - y);
			}
		}
		out.println(sum);
	}

	public static void main(String args[]) {
		new Rectangles().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}

}
