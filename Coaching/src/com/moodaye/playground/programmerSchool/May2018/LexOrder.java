package com.moodaye.playground.programmerSchool.May2018;

import java.util.Scanner;
import java.util.stream.IntStream;
import java.util.Locale;
import java.io.PrintWriter;

//Problem 314
public class LexOrder {

	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int k = in.nextInt();
		int[] a = lexOrder(n);
		int idx = 1;
		for (int i : a) {
			if (i == k) {
				out.println(idx);
			}
			idx++;
		}
	}

	void solve2(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int k = in.nextInt();
		int[] a = IntStream.range(1, n).toArray();
		heapSort(a);
		out.println(a[k]);
	}

	public int[] lexOrder(int n) {
		int[] a = new int[10_000];
		int idx = 0;
		for (int i = 1; i < 10; i++) {
			if (i > n) {
				break;
			}
			a[idx++] = i;
			for (int j = 0; j < 10; j++) {
				int num = i * 10 + j;
				if (num > n) {
					break;
				}
				a[idx++] = num;
				for (int m = 0; m < 10; m++) {
					num = i * 100 + j * 10 + m;
					if (num > n) {
						break;
					}
					a[idx++] = num;
					for (int p = 0; p < 10; p++) {
						num = i * 1000 + j * 100 + m * 10 + p;
						if (num > n) {
							break;
						}
						a[idx++] = num;
						if(num == 1_000 && n == 10_000){
							a[idx++] = 10_000;
						}
					}
				}
			}
		}
		return a;
	}

	public static void heapSort(int[] a) {
		int n = a.length;
		for (int k = n / 2; k >= 1; k--) {
			sink(a, k, n);
			while (n > 1) {
				exch(a, 1, n--);
				sink(a, 1, n);
			}
		}
	}

	public static void sink(int[] a, int k, int n) {
		while (2 * k <= n) {
			int j = 2 * k;
			if (j < n && less(a, j, j + 1)) {
				j++;
			}
			if (!less(a, k, j)) {
				break;
			}
			exch(a, k, j);
			k = j;
		}
	}

	public static boolean less(int i, int j) {

		String si = String.valueOf(i);
		String sj = String.valueOf(j);
		return si.compareTo(sj) < 0;

	}

	public static boolean less(int[] a, int i, int j) {
		String si = String.valueOf(a[i - 1]);
		String sj = String.valueOf(a[j - 1]);
		return si.compareTo(sj) < 0;
	}

	public static void exch(int[] a, int i, int j) {
		int temp = a[i - 1];
		a[i - 1] = a[j - 1];
		a[j - 1] = temp;
	}

	public static void main(String args[]) {
		new LexOrder().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
