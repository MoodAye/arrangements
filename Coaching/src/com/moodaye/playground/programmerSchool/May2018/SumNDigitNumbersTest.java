package com.moodaye.playground.programmerSchool.May2018;

import static org.junit.Assert.*;

import javax.print.attribute.IntegerSyntax;

import org.junit.AfterClass;
import org.junit.Test;
import static com.moodaye.playground.programmerSchool.May2018.SumNDigitNumbers.*;
public class SumNDigitNumbersTest {

	@Test
	public void testMultiply() {
		int[] f1 = {2,3};
		int[] f2 = {2};
		assertEquals(23*2, (int) Integer.valueOf(multiply(f1,f2)));
		f2[0] = 0;
		assertEquals(0*2, (int) Integer.valueOf(multiply(f1,f2)));
		
		f1 = new int[3];
		f1[0] = 1;
		f1[1] = 0;
		f1[2] = 9;
		f2 = new int[2];
		f2[0] = 4;
		f2[1] = 5;
		
		assertEquals("4905", multiply(f1,f2));
		assertEquals("10", multiply("10","1"));
		assertEquals("10", multiply("2","5"));
		assertEquals("209", multiply("11","19"));
		
		for(int i = 1; i < 10; i++){
			for(int j = 0; j < 10000; j++){
				String si = String.valueOf(i);
				String sj = String.valueOf(j);
				System.out.printf("%d %d%n", i, j);
				assertEquals(String.valueOf(i * j), multiply(si, sj));
			}
		}
	}
	
	
	@Test
	public void testMultiply2() {
		int[] f1 = {3,2};
		int[] f2 = {2};
		assertEquals(23*2, (int) Integer.valueOf(multiply2(f1,f2)));
		f2[0] = 0;
		assertEquals(0*2, (int) Integer.valueOf(multiply2(f1,f2)));
		
		f1 = new int[3];
		f1[0] = 9;
		f1[1] = 0;
		f1[2] = 1;
		f2 = new int[2];
		f2[0] = 5;
		f2[1] = 4;
		
		assertEquals("4905", multiply2(f1,f2));
		assertEquals("10", multiply2("01","1"));
		assertEquals("10", multiply2("2","5"));
		assertEquals("209", multiply2("11","91"));
		
		for(int i = 1; i < 10; i++){
			for(int j = 0; j < 10000; j++){
				String si = reverse(String.valueOf(i));
				String sj = reverse(String.valueOf(j));
				System.out.printf("%d %d%n", i, j);
				assertEquals(String.valueOf(i * j), multiply2(si, sj));
			}
		}
	}

	@Test
	public void testReverse(){
		assertEquals("r", reverse("r"));
		assertEquals("ra", reverse("ar"));
		assertEquals("Rajiv", reverse("vijaR"));
	}
	
	public static String reverse(String s){
		return String.valueOf(reverse(s.toCharArray(), 0));
	}
	
	static char[] reverse(char[] s, int i){
		if(i >= s.length / 2){
			return s;
		}
		char temp = s[i];
		s[i] = s[s.length - 1 - i];
		s[s.length - 1 - i] = temp;
		return reverse(s, i + 1);
	}
	
	@Test
	public void testAdd(){
		int f1 = 1;
		int f2 = 2;
		assertEquals(String.valueOf(f1 + f2), add(String.valueOf(f1), String.valueOf(f2)));
		
		for(long i = 0; i < 2; i++){
			for(long j = 0; j < 19; j++){
				assertEquals(String.valueOf(i + j), add(String.valueOf(i), String.valueOf(j)));
			}
		}
		
		assertEquals("10", add("8", "2"));
		assertEquals("100", add("82", "18"));
		assertEquals("12", add("8", "4"));
		assertEquals("103", add("99", "4"));
		assertEquals("1223", add("823", "400"));
		assertEquals("900", add("858", "42"));
		
	}

}
