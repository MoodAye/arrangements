package com.moodaye.playground.programmerSchool.May2018;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #203 */
// Start Time = 9:27pm  End Time = 10:20  Time Taken = 53min
public class TextShift {
	void solve(Scanner in, PrintWriter out) {
		String s1 = in.next();
		String s2 = in.next();
		int len = s1.length();
		char first = s1.charAt(0);
		for(int i = 0; i < s1.length(); i++){
			if(s2.substring(i).equals(s1.substring(0, len - i)) && 
					s2.substring(0,i).equals(s1.substring(len - i))){
				out.println(i);
				return;
			}
			
		/*	
		I expect that substring method of a string of length n
		has to go through max n operations to copy from one array to another.  
		Therefore I don't understand why the code above does not cause TLE 
		while the code below does.
		
		  if(s2.charAt(i) == first){
				int diff = i;
				if(shifted(s1, s2, diff)){
					out.println(diff);
					return;
				}
			}
		*/
			
		}
		out.println(-1);
	}

	public boolean shifted(String s1, String s2, int diff){
		for(int i = 0; i < s1.length(); i++){
			int shift = i + diff >= s1.length() ? (i + diff) - s1.length() : i + diff;
			if(s1.charAt(i) != s2.charAt(shift)){
				return false;
			}
		}
		return true;
	}

	public static void main(String args[]) {
		new TextShift().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
