package com.moodaye.playground.programmerSchool.May2018;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #316 */
// Start Time = 9:32pm End Time = 9:45pm  Time Taken = 13min
public class WireTransfer {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		
		int max = (n / 107) * 100;
		int cost = (n / 107) * 7;
		n = n - (n / 107) * 107;
		
		if (n > 7){
			n -= 7;
			cost += 7;
			max += n;
		}
		out.printf("%d %d", max, cost);
	}

	public static void main(String args[]) {
		new WireTransfer().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
