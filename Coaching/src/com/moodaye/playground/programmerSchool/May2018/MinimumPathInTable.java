package com.moodaye.playground.programmerSchool.May2018;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #120 */
// Time Taken = 2hr+
public class MinimumPathInTable {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int m = in.nextInt();
		int[][] table = new int[n][m];
		int[][] path = new int[n][m];

		for (int i = 0; i < n; i++) {
			for (int j = 0; j < m; j++) {
				table[i][j] = in.nextInt();
			}
		}

		for (int i = 0; i < n; i++) {
			for (int j = 0; j < m; j++) {
				if (i == 0 && j == 0) {
					path[0][0] = table[0][0];
				} else if (i == 0) {
					path[i][j] = table[i][j] + path[i][j - 1];
				} else if (j == 0) {
					path[i][j] = table[i][j] + path[i - 1][j];
				} else {
					path[i][j] = Math.min(path[i - 1][j], path[i][j - 1]) + table[i][j];
				}
			}
		}
		out.println(path[n - 1][m - 1]);
	}

	public static void main(String args[]) {
		new MinimumPathInTable().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
