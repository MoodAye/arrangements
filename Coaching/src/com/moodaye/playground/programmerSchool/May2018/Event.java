package com.moodaye.playground.programmerSchool.May2018;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #339 */
// Start Time = 10:59pm  End Time = 11:42 Time Taken = 43min
public class Event {
	void solve(Scanner in, PrintWriter out) {
		in.useDelimiter("\\.|\\s+");
		int startD = in.nextInt();
		int startM = in.nextInt();
		int startY = in.nextInt();
		int endD = in.nextInt();
		int endM = in.nextInt();
		int endY = in.nextInt();
	
		int cnt = 1;
		while(startD != endD || startM != endM || startY != endY){
			cnt++;
			startD++;
			if(startD == 32){
				if(startM == 1 || startM == 3 || startM == 5 || 
						startM == 7 || startM == 8 || startM == 10){
					startM++;
				}
				else{
					startM = 1;
					startY++;
				}
				startD = 1;
			}
			else if(startD == 31){
				if(startM == 4 || startM == 6 || startM == 9 || startM == 11){
					startM++;
					startD = 1;
				}
			}
			else if(startD == 30 && isLeapYear(startY) && startM == 2){
				startM++;
				startD = 1;
			}
			else if(startD == 29 && !isLeapYear(startY) && startM == 2){
				startM++;
				startD = 1;
			}
//			out.printf("%d.%d.%d%n", startD, startM, startY);
		}
		out.println(cnt);
	}
	
	private boolean isLeapYear(int year){
		if(year % 400 == 0 || (year % 4 == 0 && year % 100 != 0)){
			return true;
		}
		return false;
	}

	public static void main(String args[]) {
		new Event().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
