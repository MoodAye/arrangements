package com.moodaye.playground.programmerSchool.May2018;

import java.util.Scanner;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Locale;

/** Programmer's School #673, 672 */
// TIme Take ~2hrs.
public class NDigitNumbers {
	public static boolean incrementFromLeft(int[] a, int startIdx) {
		// base case
		boolean nines = true;
		for (int i = startIdx; i < a.length; i++) {
			if (a[i] != 9) {
				nines = false;
			}
		}
		if (nines) {
			return false;
		}

		if (startIdx == a.length - 1) {
			a[startIdx]++;
		} else {
			if (!incrementFromLeft(a, startIdx + 1)) {
				a[startIdx]++;
				for(int i = startIdx + 1; i < a.length; i++){
					a[i] = a[startIdx];
				}
			}
		}
		return true;
	}

	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();

		if (n == 1) {
			out.println("10 0");
			return;
		}
		// recursion
		int[] a = new int[n];
		Arrays.fill(a, 1);

		long cnt = 0;
		boolean isFirst = true;
		int[] smallest = new int[n];
		int startIdx = Math.max(0, a.length - 5);
		while (incrementFromLeft(a, startIdx)) {
			if (isSumEqualProd(a)) {
				cnt += perms(a);
				if (isFirst) {
					smallest = Arrays.copyOf(a, a.length);
					isFirst = false;
				}
			}
		}

		out.print(cnt + " ");
		StringBuilder sb = new StringBuilder();
		for (int i : smallest) {
			sb.append(i);
		}
		out.println(sb);
	}

	public static long perms(int[] a) {
		long p = 1;

		int[] cnts = new int[10];
		for (int i : a) {
			cnts[i]++;
		}

		// e.g., if n = 16 comprised of fourteen digits 1s, we have 16!/14! = 16
		// * 15
		// This avoids exceeding Long.MAX_VALUE;
		for (int i = a.length; i > cnts[1]; i--) {
			p *= i;
		}

		for (int i = 2; i < 10; i++) {
			p /= factorial(cnts[i]);
		}
		return p;
	}

	public static boolean isSumEqualProd(int[] a) {
		int sum = 0;
		int prod = 1;
		for (int x : a) {
			sum += x;
			prod *= x;
		}
		return sum == prod;
	}

	public static long factorial(int n) {
		long ans = 1;
		if (n == 0) {
			return 1;
		}
		for (int i = n; i > 0; i--) {
			ans *= i;
		}
		return ans;
	}

	public static void main(String[] args) {
		new NDigitNumbers().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}

}
