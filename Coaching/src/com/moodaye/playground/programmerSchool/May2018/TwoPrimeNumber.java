package com.moodaye.playground.programmerSchool.May2018;

import java.util.Scanner;
import java.util.stream.IntStream;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #516 */
// ~40min++
public class TwoPrimeNumber {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int[] n1 = arrayForm(n);
		int[] n2 = Arrays.copyOf(n1, n1.length);
		Arrays.sort(n1);
		n2 = IntStream.of(n2).boxed().sorted(Comparator.reverseOrder()).mapToInt(i -> i).toArray();

		long lng1 = createLong(n1);
		long lng2 = createLong(n2);

		out.println(arePrime(lng1, lng2) ? "Yes" : "No");
	}

	static long createLong(int[] n) {
		long lngn = 0;
		for (int i = 0; i < n.length; i++) {
			lngn = lngn * 10 + n[i];
		}
		return lngn;
	}

	static int[] arrayForm(int n) {
		int size = 1;
		long nTemp = n;
		while (nTemp >= 10) {
			nTemp /= 10;
			size++;
		}
		int[] arrayOfN = new int[size];
		for (int i = size - 1; i >= 0; i--) {
			arrayOfN[i] = n % 10;
			n /= 10;
		}
		return arrayOfN;
	}

	public static long reverse(long n) {
		long ans = 0;
		while (n > 0) {
			ans = ans * 10 + n % 10;
			n /= 10;
		}
		return ans;
	}

	static boolean arePrime2(long n1, long n2) {
		long limit = Math.max(n1, n2);
		boolean[] isPrime = new boolean[(int) Math.sqrt(limit) + 1];
		Arrays.fill(isPrime, true);
		isPrime[0] = false;
		isPrime[1] = false;
		for (int i = 2; i < isPrime.length; i++) {
			if (isPrime[i]) {
				mark(isPrime, i);
			}
		}
		
		for(int i = 0; i < isPrime.length; i++){
			if(isPrime[i]){
				if ((n1 != i && n1 % i == 0) || (n2 != 1 && n2 % i == 0)) {
					return false;
				}
			}
		}
		
		return true;
	}

	static boolean arePrime(long n1, long n2) {
		long limit = Math.max(n1, n2);
		boolean[] isPrime = new boolean[(int) Math.sqrt(limit) + 1];
		Arrays.fill(isPrime, true);
		isPrime[0] = false;
		isPrime[1] = false;
		for (int i = 2; i < isPrime.length; i++) {
			if (isPrime[i]) {
				mark(isPrime, i);
				if ((n1 != i && n1 % i == 0) || (n2 != 1 && n2 % i == 0)) {
					return false;
				}
			}
		}
		return true;
	}

	static void mark(boolean[] isPrime, int p) {
		for (int i = p * p; i < isPrime.length; i += p) {
			isPrime[i] = false;
		}
	}

	public static void main(String args[]) {
		new TwoPrimeNumber().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
