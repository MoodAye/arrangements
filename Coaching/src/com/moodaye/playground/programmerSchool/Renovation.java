package com.moodaye.playground.programmerSchool;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

/** Programmer's school problem 697 */
public class Renovation {
	void solve(Scanner in, PrintWriter out) {
		int l =  in.nextInt();
		int w =  in.nextInt();
		int h =  in.nextInt();
		//rounding up trick 
		// (n + d - 1) / d
		out.println(((l+w)*h*2 + 16 - 1) / 16);
	}

	public static void main(String[] args) {
		new Renovation().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
