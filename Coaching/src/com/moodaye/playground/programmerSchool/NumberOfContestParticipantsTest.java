package com.moodaye.playground.programmerSchool;

import static org.junit.Assert.*;
import static com.moodaye.playground.programmerSchool.NumberOfContestParticipants.lcm;
import static com.moodaye.playground.programmerSchool.NumberOfContestParticipants.computeCountOfParticipants;
import static com.moodaye.playground.programmerSchool.NumberOfContestParticipants.computeCountOfParticipantsWithoutLCM;

import org.junit.Test;

public class NumberOfContestParticipantsTest {

	@Test
	public void testLcm() {
		assertEquals(4, lcm(2,4));
		assertEquals(28, lcm(4,7));
//		assertEquals(28, lcm(4,-7));
	}
	
	@Test
	public void testNumberOfContestParticipants(){
		assertEquals(28, computeCountOfParticipants(2,4,7,3));
		assertEquals(-1, computeCountOfParticipants(2,2,2,3));
		assertEquals(-1, computeCountOfParticipants(2,4,7,4));
		assertEquals(99, computeCountOfParticipants(99,99,99,96));
		assertEquals(84, computeCountOfParticipants(42,21,7,66));
		assertEquals(100, computeCountOfParticipants(25,5,20,71));
		assertEquals(140, computeCountOfParticipants(7,14,14,100));
		assertEquals(-1, computeCountOfParticipants(96,64,64,92));
		assertEquals(-1, computeCountOfParticipants(3,4,4,1));
		assertEquals(-1, computeCountOfParticipants(99,99,99,64));
		
	}
	
	@Test
	public void testNumberOfContestParticipantsWithoutLCM(){
		assertEquals(28, computeCountOfParticipantsWithoutLCM(2,4,7,3));
		assertEquals(-1, computeCountOfParticipantsWithoutLCM(2,2,2,3));
		assertEquals(-1, computeCountOfParticipantsWithoutLCM(2,4,7,4));
		assertEquals(99, computeCountOfParticipantsWithoutLCM(99,99,99,96));
		assertEquals(84, computeCountOfParticipantsWithoutLCM(42,21,7,66));
		assertEquals(100, computeCountOfParticipantsWithoutLCM(25,5,20,71));
		assertEquals(140, computeCountOfParticipantsWithoutLCM(7,14,14,100));
		assertEquals(-1, computeCountOfParticipantsWithoutLCM(96,64,64,92));
		assertEquals(-1, computeCountOfParticipantsWithoutLCM(3,4,4,1));
		assertEquals(-1, computeCountOfParticipantsWithoutLCM(99,99,99,64));
		
	}
	@Test
	public void testLcmUnlimited(){
		int a1[] = {2,4};
		assertEquals(4, lcm(a1));
		
		int a2[] = {4,7};
		assertEquals(28, lcm(a2));
		
		int a3[] = {4,7, 56};
		assertEquals(56, lcm(a3));
		
		int a4[] = {1,2,3,4,5};
		assertEquals(60, lcm(a4));
		
		int a5[] = {-1,2,3,4,5};
//		assertEquals(60, lcm(a5));
	}

}
