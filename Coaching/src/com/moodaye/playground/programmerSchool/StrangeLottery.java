package com.moodaye.playground.programmerSchool;

import java.util.Locale;
import java.util.Scanner;
import java.io.PrintWriter; 

/** Programmer's school #641 */
//complexity is O(n) since we run through the input string twice.
//Can we improve?  Priority queue would give O(logN).  Will think about this next.
public class StrangeLottery {
	void solve(Scanner in, PrintWriter out){
		String input = in.next();
		int cnt = 2;
		while(cnt > 0){
			for(int i = 0; i < input.length(); i++){
				if( (i == input.length() - 1) || 
						(i + cnt > input.length()) || 
						(input.charAt(i) < input.charAt(i + 1))){
					input = pack(input, i);
					cnt--;
					break;
				}
			}
		}
		System.out.println(input);
	}

	/** removes char at index from string */
	private static String pack(String input, int index){
		if (index == input.length() - 1){
			return input.substring(0, index);
		}
		return input.substring(0, index) + input.substring(index + 1);
	}
	
	public static void main(String[] args) {
			new StrangeLottery().run();
	}
	
	void run(){
		Locale.setDefault(Locale.US);
		try(Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)){
			solve(in, out);
		}
	}

}
