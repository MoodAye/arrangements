package com.moodaye.playground.programmerSchool.November2017;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #317 */
public class GiftsSantaClaus {
	void solve(Scanner in, PrintWriter out) {
		int x = in.nextInt();
		int y = in.nextInt();
		int z = in.nextInt();
		int w = in.nextInt();
		int cnt = 0;
		for (int xi = 0; xi <= w / x; xi++) {
			for (int yi = 0; yi <= (w - xi) / y; yi++) {
				int zi = (w - yi * y - xi * x) / z;
				if (zi < 0){
					break;
				}
				if (xi * x + yi * y + zi * z == w) {
					cnt++;
				}
			}
		}
		out.println(cnt);
	}

	public static void main(String args[]) {
		new GiftsSantaClaus().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
