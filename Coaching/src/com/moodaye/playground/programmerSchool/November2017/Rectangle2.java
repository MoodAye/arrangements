package com.moodaye.playground.programmerSchool.November2017;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #182 */
public class Rectangle2 {
	void solve(Scanner in, PrintWriter out) {
		Point p1 = new Point(in.nextDouble(), in.nextDouble());
		Point p2 = new Point(in.nextDouble(), in.nextDouble());
		Point p3 = new Point(in.nextDouble(), in.nextDouble());
		Point p4 = point(p1, p2, p3);
		out.printf("%.0f %.0f", p4.x, p4.y);
	}

	/** Finds 4th corner of rectangle */
	public static Point point(Point a, Point b, Point c) {
		Point corner;
		Point p1;
		Point p2;

		// find longest segment - this is the diagnol.
		// then the point not on the diagnol is the corner that is connected
		// to the other 2 points
		double ab_sq = (a.x - b.x) * (a.x - b.x) + (a.y - b.y) * (a.y - b.y);
		double bc_sq = (b.x - c.x) * (b.x - c.x) + (b.y - c.y) * (b.y - c.y);
		double ca_sq = (c.x - a.x) * (c.x - a.x) + (c.y - a.y) * (c.y - a.y);

		if (ab_sq > bc_sq && ab_sq > ca_sq) {
			corner = c;
			p1 = a;
			p2 = b;
		} else if (bc_sq > ca_sq) {
			corner = a;
			p1 = b;
			p2 = c;
		} else {
			corner = b;
			p1 = a;
			p2 = c;
		}

		Point fourth = new Point(p1.x - (corner.x - p2.x), p1.y - (corner.y - p2.y));
		return fourth;
	}

	public static void main(String args[]) {
		new Rectangle2().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}

}

// need x, y to be doubles to support random tests
class Point {
	double x;
	double y;

	Point(double x, double y) {
		this.x = x;
		this.y = y;
	}
}