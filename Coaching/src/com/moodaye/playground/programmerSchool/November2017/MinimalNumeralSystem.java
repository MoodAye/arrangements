package com.moodaye.playground.programmerSchool.November2017;

import java.util.Scanner;
import java.util.regex.Pattern;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Locale;

/** Programmer's School #315 */
public class MinimalNumeralSystem {
	void solve(Scanner in, PrintWriter out) {
		String s = in.nextLine();
		out.println(minBase(s));
	}


	public static int minBase(String s) {
		if(!Pattern.compile("^[0-9A-Z]+$").matcher(s).matches()){
			return -1;
		}
		
		char[] c = s.toCharArray();
		Arrays.sort(c);
		int max = c[c.length - 1];
		if (max == '0' || max == '1') {
			return 2;
		//normalizing character code to the base
		} else if (max >= '2' && max <= '9') {
			return max - 47; 
		} else {
			return max - 54;
		}
	}

	public static void main(String[] args) {
		new MinimalNumeralSystem().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}

}
