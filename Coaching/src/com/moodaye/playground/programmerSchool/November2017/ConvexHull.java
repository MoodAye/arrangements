package com.moodaye.playground.programmerSchool.November2017;

import java.util.Scanner;
import java.io.PrintWriter;
import java.util.Locale;

/** Programmer's School #84 */
/*
 * Need to assume the following in order to explain the sample outputs: 1. The
 * points of the squares may lie inside the polygon 2. The lines joining the
 * squares can only make 90 degree turns.
 * 
 * This then reduces to adding minimal number of squares so that the outer most
 * squares make a rectangular shape that encompasses all the other squares.
 */
public class ConvexHull {

	void solve(Scanner in, PrintWriter out) {
		int lines = in.nextInt();
		int columns = in.nextInt();
		char[][] paper = new char[lines][columns];
		for (int i = 0; i < lines; i++) {
			String sln = in.next();
			for (int j = 0; j < columns; j++) {
				paper[i][j] = sln.charAt(j);
			}
		}
		
		int top = Integer.MAX_VALUE;
		int bot = -1;
		int lft = Integer.MAX_VALUE;
		int rgt = -1;
		
		for (int i = 0; i < paper.length; i++) {
			for (int j = 0; j < paper[0].length; j++) {
				if (paper[i][j] == '*') {
					top = Math.min(top, i);
					bot = Math.max(bot, i);
					lft = Math.min(lft, j);
					rgt = Math.max(rgt, j);
				}
			}
		}
		
		// print paper 
		for (int i = 0; i < paper.length; i++) {
			for (int j = 0; j < paper[0].length; j++) {
				if(j >= lft && j <= rgt && i >= top && i<= bot){
					out.print('*');
				}
				else{
					out.print('.');
				}
			}
			out.println();
		}
	}

	public static void main(String[] args) {
		new ConvexHull().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
