package com.moodaye.playground.programmerSchool.November2017;

import static org.junit.Assert.*;

import org.junit.Test;
import static com.moodaye.playground.programmerSchool.November2017.Rectangle2.*;

public class Rectangle2Test {
	
	private final double TOLERANCE = 1e-6;

	//any guidance on what this should be 
	//the tests fail intermittently for values
	//less than 1e-6.
	
	@Test
	public void test() {
		for(int i = 0; i < 1_000_000; i++){
			Point a = new Point(Math.random() * 2000 - 1000, Math.random() * 2000 - 1000);
			Point b = new Point(Math.random() * 2000 - 1000, Math.random() * 2000 - 1000);
		
			//slopes
			double m_ab = (a.y - b.y) / (a.x - b.x);
			double m_bc = -1 / m_ab;
			double m_cd = m_ab;
			double m_da = m_bc;
			
			// y intercepts (c)
			double c_ab = a.y - (a.x * m_ab);
			double c_bc = b.y - (b.x * m_bc);
			double c_da = a.y - (a.x * m_da);
			double cx = Math.random() * 1000;
			double cy = m_bc * cx + c_bc;
			
			Point c = new Point(cx, cy);
			double c_cd = c.y - (c.x * m_cd);
			double dx = -1 * (c_cd - c_da) / (m_cd - m_da);
			double dy = m_da * dx + c_da;
			
			Point d = point(a, b, c);
			assertEquals(dx, d.x, TOLERANCE);
			assertEquals(dy, d.y, TOLERANCE);
		}
	}
	
	@Test
	public void test2(){
		Point a = new Point(0, 3);
		Point b = new Point(0, 0);
		Point c = new Point(5, 0);
		Point d = point(a, b, c);
		assertEquals(5, (int) d.x);
		assertEquals(3, (int) d.y);
		
		a = new Point(1, 4);
		b = new Point(8, 3);
		c = new Point(7, 6);
		d = point(a, b, c);
		assertEquals(2, (int) d.x);
		assertEquals(1, (int) d.y);
	}
}
