package com.moodaye.playground.programmerSchool.November2017;
import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #328 */

public class DominoDots {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		out.println(getSum(n));
	}
	
	/* Approach with O(1) complexity.   Think of this 
	 * as a sum of a sequence.  (n)*(0 + n + 1)/2.  
	 * Each number from 0 .. n occurs n+2 times. 
	 * Say n = 4.  Then we have the following dominos:
	 * 
	 * 0-4	0-3	0-2	0-1	0-0
	 * 1-4 	1-3	1-2	1-1	
	 * 2-4	2-3	2-2	
	 * 3-4	3-3	
	 * 4-4	
	 * 
	 *  the number can be re-arranged as 4 + 2 sequences
	 *  
	 * 0	0	0	0	0	0
	 * 1 	1	1	1	1	1
	 * 2	2	2	2	2	2
	 * 3	3	3	3	3	3
	 * 4	4	4	4	4	4
	 * 
	 */
	public static long getSum(int n){
		return n * (n + 1) * (n + 2) / 2;
	}
	
	// This is obvious approach with complexity O(n^2)
	public static long getSum_2(int n){
		long sum = 0;
		for(int i = 0; i <= n; i++){
			for(int j = 2*i ; j <= n + i; j++){
				sum += j;
			}
		}
		return sum;
	}

	public static void main(String args[]) {
		new DominoDots().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
