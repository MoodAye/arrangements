package com.moodaye.playground.programmerSchool.November2017;

import java.util.Scanner;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/** Programmer's School #530 */
//refactored to use 2 dimensional array instead of Map.
public class BlackWhiteGraphics {
	void solve(Scanner in, PrintWriter out) {
		int w = in.nextInt();
		int h = in.nextInt();
		String[] img1 = new String[h];
		String[] img2 = new String[h];
		for(int i = 0; i < h; i++){
			img1[i] = in.next();
		}
		for(int i = 0; i < h; i++){
			img2[i] = in.next();
		}
		int result = in.nextInt();
//		Map<Integer, Integer> resMap = resultMap(result);
		int[][] resMap = resultMapArray(result);
		for(int i = 0; i < h; i++){
			out.println(resImage(resMap, img1[i], img2[i]));
		}
	}
	
	public String resImage(int[][] rm, String img1, String img2){
		char[] cImg1 = img1.toCharArray();
		char[] cImg2 = img2.toCharArray();
		StringBuilder sb = new StringBuilder();
		for(int i = 0; i < cImg1.length; i++){
			int pix1 = Integer.valueOf(String.valueOf(cImg1[i]));
			int pix2 = Integer.valueOf(String.valueOf(cImg2[i]));
			int r = rm[pix1][pix2];
			sb.append(String.valueOf(r));
		}
		return sb.toString();
	}
	
	public String resImage(Map<Integer, Integer> rm, String img1, String img2){
		char[] cImg1 = img1.toCharArray();
		char[] cImg2 = img2.toCharArray();
		StringBuilder sb = new StringBuilder();
		for(int i = 0; i < cImg1.length; i++){
			int h = hash(cImg1[i], cImg2[i]);
			int r = rm.get(h);
			sb.append(String.valueOf(r));
		}
		return sb.toString();
	}
	
	public int[][] resultMapArray(int result){
		int[][] resMap = new int[2][2];
		resMap[1][1] = result%2;
		result /= 10;
		resMap[1][0] = result%2;
		result /= 10;
		resMap[0][1] = result%2;
		result /= 10;
		resMap[0][0] = result%2;
		result /= 10;
		return resMap;
	}
	
	public Map<Integer,Integer> resultMap(int result){
		Map<Integer,Integer> resMap = new HashMap<>();
		resMap.put(hash('1','1'), result % 2);
		result /= 10;
		resMap.put(hash('1','0'), result % 2);
		result /= 10;
		resMap.put(hash('0','1'), result % 2);
		result /= 10;
		resMap.put(hash('0','0'), result % 2);
		result /= 10;
		return resMap;
	}
	
	public static int hash(int i, int j){
		return i + 10*j;
	}

	public static void main(String[] args) {
		new BlackWhiteGraphics().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
