package com.moodaye.playground.programmerSchool.November2017;

import static org.junit.Assert.*;

import org.junit.Test;
import static com.moodaye.playground.programmerSchool.November2017.BestDivisor.*;
public class BestDivisorTest {

	@Test
	public void test() {
		assertEquals(5, bestDivisor(10));
		assertEquals(239, bestDivisor(239));
		assertEquals(1, bestDivisor(1));
		assertEquals(9, bestDivisor(18));
	}

}
