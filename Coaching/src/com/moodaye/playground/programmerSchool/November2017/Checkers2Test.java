package com.moodaye.playground.programmerSchool.November2017;

import static org.junit.Assert.*;

import org.junit.Test;
import static com.moodaye.playground.programmerSchool.November2017.Checkers2.*;
public class Checkers2Test {

	@Test
	public void test() {
		assertTrue(isReachable("a1", "b2"));
		assertTrue(isReachable("a1", "c3"));
		assertTrue(isReachable("a1", "d4"));
		assertTrue(isReachable("a1", "e5"));
		assertTrue(isReachable("a1", "f6"));
		assertTrue(isReachable("a1", "g7"));
		assertTrue(isReachable("a1", "h8"));
		
		assertTrue(isReachable("a1", "a3"));
		assertTrue(isReachable("a1", "a5"));
		assertTrue(isReachable("a1", "a7"));
		
		assertTrue(isReachable("e3", "f8"));
		assertFalse(isReachable("e3", "f2"));
		assertFalse(isReachable("h8", "a1"));
	}
	
	@Test
	public void test_approach2() {
		assertTrue(isReachableFewestOperations("a1", "b2"));
		assertTrue(isReachableFewestOperations("a1", "c3"));
		assertTrue(isReachableFewestOperations("a1", "d4"));
		assertTrue(isReachableFewestOperations("a1", "e5"));
		assertTrue(isReachableFewestOperations("a1", "f6"));
		assertTrue(isReachableFewestOperations("a1", "g7"));
		assertTrue(isReachableFewestOperations("a1", "h8"));
		
		assertTrue(isReachableFewestOperations("a1", "a3"));
		assertTrue(isReachableFewestOperations("a1", "a5"));
		assertTrue(isReachableFewestOperations("a1", "a7"));
		
		assertTrue(isReachableFewestOperations("e3", "f8"));
		assertFalse(isReachableFewestOperations("e3", "f2"));
		assertFalse(isReachableFewestOperations("h8", "a1"));
	}
	
	@Test
	public void testIsEndBlack(){
		assertTrue(isBlack("a3"));
		assertTrue(isBlack("b2"));
		assertTrue(isBlack("c3"));
		assertTrue(isBlack("d4"));
		assertTrue(isBlack("e5"));
		assertTrue(isBlack("f6"));
		assertTrue(isBlack("g7"));
		assertTrue(isBlack("h8"));
		assertTrue(isBlack("a3"));
		assertTrue(isBlack("a5"));
	}

}
