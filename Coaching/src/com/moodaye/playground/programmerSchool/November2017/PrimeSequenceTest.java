package com.moodaye.playground.programmerSchool.November2017;

import static org.junit.Assert.*;

import org.junit.Test;
import static com.moodaye.playground.programmerSchool.November2017.PrimeSequence.*;

public class PrimeSequenceTest {

	@Test
	public void testSeive() {
		boolean[] sv1 = sieve(10);
		boolean[] esv1 = {true, true, false, false, true, false, true, false, true, true, true};
		assertArrayEquals(esv1, sv1);
		assertTrue(esv1[9]);
		
		boolean[] sv2 = sieve(100);
		assertFalse(sv2[7]);
		assertFalse(sv2[11]);
		assertFalse(sv2[13]);
		assertFalse(sv2[29]);
		assertFalse(sv2[53]);
		assertTrue(sv2[27]);
		assertTrue(sv2[99]);
		
		boolean[] sv3 = sieve(10_000);
		assertTrue(sv3[9998]);
		assertTrue(sv3[9993]);
		
		assertFalse(sv3[9511]);
		assertFalse(sv3[9521]);
		assertFalse(sv3[9497]);
		
		boolean[] sv4 = sieve(20_000);
		sv4 = sieve(46_349);
		assertTrue(sv4[9998]);
	}
	
	
	@Test
	public void testPrimeDigits(){
		int[] a = primeDigits();
		assertEquals(2, a[1]);
		assertEquals(3, a[2]);
		assertEquals(5, a[3]);
		assertEquals(7, a[4]);
		assertEquals(1, a[5]);
		assertEquals(1, a[6]);
		assertEquals(1, a[7]);
		assertEquals(3, a[8]);
		assertEquals(1, a[9]);
		assertEquals(7, a[10]);
		assertEquals(1, a[11]);
		assertEquals(9, a[12]);
		assertEquals(2, a[13]);
		assertEquals(3, a[14]);
		assertEquals(2, a[15]);
		assertEquals(9, a[16]);
		
		assertEquals(8, a[4000]);
		assertEquals(3, a[4001]);
		assertEquals(8, a[4002]);
		assertEquals(7, a[4003]);
		assertEquals(8, a[4004]);
		assertEquals(3, a[4005]);
		assertEquals(8, a[4006]);
		assertEquals(9, a[4007]);
		assertEquals(8, a[4008]);
		assertEquals(4, a[4009]);
	}
	

}
