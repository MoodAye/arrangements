package com.moodaye.playground.programmerSchool.November2017;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #47 */
public class BestDivisor {

	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		out.println(bestDivisor(n));
	}

	public static int bestDivisor(int n) {
		int div = 1;
		int val = 1;
		// we can stop this loop at i = n/2 since the only
		// other factor after n/2 is n.  (We would need
		// to add the code to get sum of digits of n. 
		// Not worth the extra code since for n max = 10^5
		// you need 5 loops to get sum of digits (divide by 10 each loop).
		// So worst case would be 5*10^5 (5e5). The website allows 
		// 1e8 ops per second.
		for (int i = 2; i <= n ; i++) {
			if (n % i == 0) {
				int newVal = getSumOfDigits(i);
				if (newVal > val) {
					div = i;
					val = newVal;
				}
			}
		}
		return div;
	}

	public static int getSumOfDigits(int n) {
		int sumOfDigits = 0;
		while (n > 0) {
			sumOfDigits += n % 10;
			n /= 10;
		}
		return sumOfDigits;
	}

	public static void main(String args[]) {
		new BestDivisor().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}

}
