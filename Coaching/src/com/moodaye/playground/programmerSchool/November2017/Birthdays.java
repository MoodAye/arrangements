package com.moodaye.playground.programmerSchool.November2017;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #490 */
public class Birthdays {
	void solve(Scanner in, PrintWriter out) {
		in.useDelimiter("\\.|\\s+");
		String d1 = in.next();
		String m1 = in.next();
		String y1 = in.next();
		String d2 = in.next();
		String m2 = in.next();
		String y2 = in.next();
		out.println(daysSince010193(y2, m2, d2) - daysSince010193(y1, m1, d1));
}

	public static void main(String args[]) {
		new Birthdays().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
	
	public static int daysSince010193(String y, String m, String d){
		return daysSince010193(Integer.valueOf(y), Integer.valueOf(m), Integer.valueOf(d));
	}
	
	public static int daysSince010193_Approach2(int y, int m, int d){
		int[] md = {0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334, 365};
		int days = d;
		for(int i = m - 1; i > 0; i--){
			days += md[i];
		}
		return days;
	}
	
	public static int daysSince010193(int y, int m, int d){
		int days = d;
		switch (m) {
		case 12:
			days += 30;
		case 11:
			days += 31;
		case 10:
			days += 30;
		case 9:
			days += 31;
		case 8:
			days += 31;
		case 7:
			days += 30;
		case 6:
			days += 31;
		case 5:
			days += 30;
		case 4:
			days += 31;
		case 3:
			days += 28;
		case 2:
			days += 31;
		}
		if (y == 94) {
			days += 365;
		}
		return days;
	}
}	
