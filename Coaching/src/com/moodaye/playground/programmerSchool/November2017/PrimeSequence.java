package com.moodaye.playground.programmerSchool.November2017;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #64 */
// Since each test requires the same input of prime numbers
// we pre-create an array of prime digits.  Then each test
// runs in constant O(1) time.

// Density of logs for N = 10_000 is (10_000 / 9.2 ~ 1_000).  
// Density of logs for N = 100_000 is (100_000 / 11.5 ~ 10_000). 
// (Using prime number function --- N / log N. 
// Therefore we need to generate prime numbers less than N = 100_000
// and more than  N = 10_000 so that we have enough prime number digits.
// to populate the 10_000 digit array.

// Generating the prime number sieve has a complexity of O(n) --- 
// because number of loops is less than (n/p1 + n/p2 + n/p3 +.... + n/pk).
// where k = largest prime less than sqrt(n).
//
// Populating the prime digits array requires converting each prime 
// number to a char arary and then back to a int array of its digits.
// Since the length of each prime number is less than 6 with avg 
// close to 5 ... the time complexity here should be constant.

public class PrimeSequence {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int[] p = primeDigits();
		for(int i = 0; i < n; i++){
			out.print(p[in.nextInt()]);
		}
	}

	/** creates an array with first 10,000 prime digits */
	public static int[] primeDigits(){
		int[] p = new int[10_000 + 1];
		int k = 1; //current index
		//todo - return only primes 
		boolean[] sv = sieve(100_000);
		
		for(int i = 0; i < sv.length; i++){
			if(!sv[i]){ //is i prime?
				k = addPrimeDigits(p,i,k);
			}
		}
		return p;
	}

	/** adds digits of prime number to array, starting at idx position.  
	    returns the next idx position */
	//TODO - simplify ... do we really need to conver to char[] first?
	//TODO - try diving by 10^k to extract the digits -- is this more efficient
	//TODO - try using lambda to make code cleaner
	private static int addPrimeDigits(int[] p, int prime, int idx){
		char[] c = String.valueOf(prime).toCharArray();
		for(int i = 0; i < c.length && idx < p.length; i++, idx++){
			p[idx] = Integer.valueOf(String.valueOf(c[i]));
		}
		return idx;
	}

	/** generates sieve of prime numbers true indicates non-prime */
	// optimize by 1. reducing size of sieve (only store odd values)
	public static boolean[] sieve(int n){
		boolean[] sv = new boolean[n + 1];
		sv[0] = true;
		sv[1] = true;
		for(int i = 4; i <= n; i +=2){
			sv[i] = true;
		}
		
		for(int i = 3; i <= Math.sqrt(n); i++){
			if(!sv[i]){
				mark(sv, i);
			}
		}
		return sv;
	}
	
	private static void mark(boolean[] sv, int k){
		for(int i = k * k; i < sv.length; i += k){
			sv[i] = true;
		}
	}
	
	public static void main(String args[]) {
		new PrimeSequence().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}

}
