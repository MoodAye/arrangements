package com.moodaye.playground.programmerSchool.November2017;

import static org.junit.Assert.*;
import static com.moodaye.playground.programmerSchool.November2017.WholeSalePurchase.*;

import org.junit.Test;

public class WholeSalePurchaseTest {

	@Test
	public void test() {
		int[] p0 = {0,1,0};
		assertArrayEquals(p0, buy(11));
		int[] p1 = {3,5,8};
		assertArrayEquals(p1, buy(500));
		int[] p2 = {0,0,1};
		assertArrayEquals(p2, buy(1));
		int[] p3 = {0,1,0};
		assertArrayEquals(p3, buy(12));
		int[] p4 = {0,2,0};
		assertArrayEquals(p4, buy(24));
		int[] p5 = {6_944_444,5,4};
		assertArrayEquals(p5, buy(1_000_000_000));
		int[] p6 = {0,11,0};
		assertArrayEquals(p6, buy(132));
		int[] p7 = {0,11,1};
		assertArrayEquals(p7, buy(133));
		int[] p8 = {1,0,9};
		assertArrayEquals(p8, buy(153));
		int[] p9 = {1,0,8};
		assertArrayEquals(p9, buy(152));
		int[] p10 = {1,1,0};
		assertArrayEquals(p10, buy(154));
	}
}