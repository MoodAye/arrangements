package com.moodaye.playground.programmerSchool.November2017;

import java.util.Scanner;
import java.util.Arrays;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #461 */
public class Elections {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int[] gps = new int[n];
		for (int j = 0; j < gps.length; j++) {
			gps[j] = in.nextInt();
		}
		
		Arrays.sort(gps);
		int voters = 0;
		for(int i = 0; i < n / 2 + 1; i++){
			voters += gps[i] / 2 + 1;
		}
		
		out.println(voters);
	}

	public static void main(String args[]) {
		new Elections().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
