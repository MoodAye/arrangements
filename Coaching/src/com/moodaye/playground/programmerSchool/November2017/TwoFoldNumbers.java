package com.moodaye.playground.programmerSchool.November2017;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #250 */
public class TwoFoldNumbers {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		
		int diff = 0;
		while(true){
			if(isTwoFold(n - diff)){
				n -= diff;
				break;
			}
			else if(isTwoFold(n + diff)){
				n += diff;
				break;
			}
			diff++;
		}
		out.print(n);
	}
	
	public static boolean isTwoFold(int n){
		boolean[] seen = new boolean[10];
		int cnt = 0;
		while(n > 0 && cnt <= 2){
			int rem = n % 10;
			n /= 10;
			if(! seen[rem]){
				cnt++;
				seen[rem] = true;
			}
		}
		return cnt <= 2;
	}

	public static void main(String args[]) {
		new TwoFoldNumbers().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
