package com.moodaye.playground.programmerSchool.November2017;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School # 684 */
/**
 * Approach1 - Uses minimal operations
 * Apprach2 - Check all possible next steps (recursion used)
 */
public class Checkers2 {
	void solve(Scanner in, PrintWriter out) {
		String start = in.next();
		String end = in.next();

		out.println(isReachable(start, end) ? "YES" : "NO");
	}

	// Approach 1 
	public static boolean isReachableFewestOperations(String start, String end) {
		int h = Math.abs(end.charAt(0) - start.charAt(0));
		int v = end.charAt(1) - start.charAt(1);
		
		if(!isBlack(end) || v <= 0 || h > v){
			return false;
		}
		return true;
	}

	// Given that start is black.
	public static boolean isBlack(String square){
		return ((int) square.charAt(0) + (int) square.charAt(1)) % 2 == (int) 0;
	}
	

	/**
	 * Simple Approach - no proof required: Use recursion to travel from
	 * start to all possible squares. (Moving up and left or up and right). When
	 * you find target - return true. If at the end - not found return false.
	 */
	// assumes start != end when method is first invoked.
	public static boolean isReachable(String start, String end) {
		if (start.equals(end)) {
			return true;
		}

		char row = (char) (start.charAt(1) + 1);
		// reached top?
		if (row == '9') {
			return false;
		}

		// move one step up and left
		char col = (char) (start.charAt(0) - 1);
		if (!(col == 'a' - 1)) {
			if (isReachable(String.valueOf(col) + String.valueOf(row), end)) {
				return true;
			}
		}

		// move one step up and right
		col = (char) (start.charAt(0) + 1);
		if (!(col == 'h' + 1)) {
			if (isReachable(String.valueOf(col) + String.valueOf(row), end)) {
				return true;
			}
		}
		return false;
	}

	public static void main(String args[]) {
		new Checkers2().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
