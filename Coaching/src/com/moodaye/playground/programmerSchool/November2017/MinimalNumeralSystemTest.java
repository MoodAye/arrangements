package com.moodaye.playground.programmerSchool.November2017;

import static org.junit.Assert.*;

import static com.moodaye.playground.programmerSchool.November2017.MinimalNumeralSystem.*;

import org.junit.Test;

public class MinimalNumeralSystemTest {

	@Test
	public void test() {
		assertEquals(2, minBase("0"));
		assertEquals(2, minBase("1"));
		for(int i = 2; i < 10; i++){
			assertEquals(i + 1, minBase(String.valueOf(i)));
		}
	
		//check for ' ' (confirm that no trimming is happening);
		assertEquals(-1, minBase(" 3"));
		assertEquals(-1, minBase("1 3"));
		
		assertEquals(10, minBase("90"));
		assertEquals(11, minBase("1A3"));
		assertEquals(16, minBase("1F3"));
		
		assertEquals(4, minBase("123"));
		assertEquals(16, minBase("ABCDEF"));
		assertEquals(16, minBase("abcdef"));
		assertEquals(-1, minBase("AD%AF"));
		
		assertEquals(-1, minBase(" "));
		assertEquals(-1, minBase("!"));
		
		assertEquals(-1, minBase("00"));
		assertEquals(-1, minBase("0.0"));
		assertEquals(-1, minBase("0.1"));
		assertEquals(-1, minBase("2.1"));
		assertEquals(-1, minBase("009"));
		assertEquals(4, minBase(Character.toString('3')));
		assertEquals(11, minBase(Character.toString((char) 65)));
		assertEquals(-1, minBase(Character.toString((char) 127)));
		assertEquals(-1, minBase(Character.toString((char) 32) + 
				Character.toString((char) 127) + Character.toString('3')));
		
	}

}
