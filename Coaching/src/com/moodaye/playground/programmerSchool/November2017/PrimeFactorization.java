package com.moodaye.playground.programmerSchool.November2017;

import java.util.Scanner;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #354 */

public class PrimeFactorization {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		List<Integer> factors = getFactors(n);

		for (int i = 0; i < factors.size() - 1; i++) {
			out.print(factors.get(i) + "*");
		}
		out.println(factors.get(factors.size() - 1));
	}

	public static List<Integer> getFactors(int n) {
		List<Integer> factors = new ArrayList<>();
		
		for (int i = 2; i * i <= n; i++){
			while(n % i == 0){
				factors.add(i);
				n /= i;
			}
		}

		if(n != 1){
			factors.add(n);
		}

		return factors;
	}

	public static void main(String args[]) {
		new PrimeFactorization().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
