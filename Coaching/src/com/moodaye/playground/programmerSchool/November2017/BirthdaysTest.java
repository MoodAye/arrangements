package com.moodaye.playground.programmerSchool.November2017;

import static com.moodaye.playground.programmerSchool.November2017.Birthdays.*;
import static org.junit.Assert.*;

import org.junit.Test;

public class BirthdaysTest {

	@Test
	public void test() {
		int bd1 = 0;
		int bd2 = 0;
		for (int y1 = 93; y1 <= 92; y1++) {
			for (int m1 = 1; m1 <= 12; m1++) {
				for (int d1 = 1; d1 <= 31; d1++) {
					if (checkmd(m1, d1)) {
						break;
					}
					bd1++;
					for (int y2 = y1; y2 <= 92; y2++) {
						for (int m2 = m1; m2 <= 12; m2++) {
							for (int d2 = d1; d2 <= 31; d2++) {
								if (checkmd(m2, d2)) {
									break;
								}
								bd2++;
								assertEquals(bd2 - bd1, daysSince010193(y2,m2,d2) - daysSince010193(y1,m1,d1));
							}
						}
					}
				}
			}
		}
	}

	
	@Test
	public void test2() {
		int bd1 = 0;
		int bd2 = 0;
		for (int y1 = 93; y1 <= 92; y1++) {
			for (int m1 = 1; m1 <= 12; m1++) {
				for (int d1 = 1; d1 <= 31; d1++) {
					if (checkmd(m1, d1)) {
						break;
					}
					bd1++;
					for (int y2 = y1; y2 <= 92; y2++) {
						for (int m2 = m1; m2 <= 12; m2++) {
							for (int d2 = d1; d2 <= 31; d2++) {
								if (checkmd(m2, d2)) {
									break;
								}
								bd2++;
								assertEquals(bd2 - bd1, daysSince010193_Approach2(y2,m2,d2) - 
										daysSince010193_Approach2(y1,m1,d1));
							}
						}
					}
				}
			}
		}
	}
	private boolean checkmd(int m, int d) {
		if (m == 2) {
			if (d > 28) {
				return false;
			}
		}
		if (m == 4 || m == 6 || m == 9 || m == 11) {
			if (d > 30) {
				return false;
			}
		}
		return true;
	}
}
