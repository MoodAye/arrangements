package com.moodaye.playground.programmerSchool.November2017;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #88 */
// Create an array of size n x n
// For each, row, column and small square - increment the array using the
// elements for the index.
// Each time = confirm that for n = 1,n*n - each array element value is = i.
// Increment i each time.
// number of operations = (n * n * n * 3) * n = 10 * 10 * 10 * 10 * 3 = 30_000
// memory required = n x n x n x n x 32 bytes = 10_000 * 32 = 320_000 bytes =
// 320K
public class Sudoku {
	private static final int MAX = 10;
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int[][] sdku = new int[n * n][n * n];
		for (int i = 0; i < n * n; i++) {
			for (int j = 0; j < n * n; j++) {
				sdku[i][j] = in.nextInt();
			}
			if (!checkRow(sdku, i, n)) {
				out.println("Incorrect");
				return;
			}
		}

		for (int c = 0; c < n * n; c++) {
			if (!checkCol(sdku, c, n)) {
				out.println("Incorrect");
				return;
			}
		}

		if (!checkSq(sdku, n)) {
			out.println("Incorrect");
			return;
		}
		out.println("Correct");
	}

	boolean checkRow(int[][] sdku, int row, int n) {
		int[] check = new int[MAX * MAX + 1];
		for (int i = 0; i < n * n; i++) {
			check[sdku[row][i]]++;
		}
		for (int i = 1; i <= n * n; i++) {
			if (check[i] != 1) {
				return false;
			}
		}
		return true;
	}

	boolean checkCol(int[][] sdku, int col, int n) {
		int[] check = new int[MAX * MAX + 1];
		for (int i = 0; i < n * n; i++) {
			check[sdku[i][col]]++;
		}
		for (int i = 1; i <= n * n; i++) {
			if (check[i] != 1) {
				return false;
			}
		}
		return true;
	}

	boolean checkSq(int[][] sdku, int n) {
		int[] check = new int[MAX * MAX + 1];
		for (int r = 0; r < n; r++) {
			for (int c = 0; c < n; c++) {
				for (int i = 0; i < n; i++) {
					for (int j = 0; j < n; j++) {
						check[sdku[r * n + i][c * n + j]]++;
					}
				}
				for (int k = 1; k <= n * n; k++) {
					if (check[k] != 1) {
						return false;
					}
				}
				check = new int[MAX * MAX + 1];
			}
		}

		return true;
	}

	public static void main(String args[]) {
		new Sudoku().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
