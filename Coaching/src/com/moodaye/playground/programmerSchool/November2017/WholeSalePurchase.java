package com.moodaye.playground.programmerSchool.November2017;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #520 */
public class WholeSalePurchase {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int[] p = buy(n);
		out.printf("%d %d %d", p[0], p[1], p[2]);
	}
	
	public static int[] buy(int n){
		int[] p = new int[3];
		int boxes = n / 144;  //$1140
		int bundles = (n - boxes*144) / 12; //102
		int pairs = (n - boxes*144 - bundles*12); //$10.50
		
		//exchange pairs for bundles when cost of pairs exceeds that of one bundle
		//change bundle for box when cost of bundles exceeds that of one box
		
		//number of socks to be had for $102 = 9 (9 * 10.5 = 94.5)
		//number of bundles to be had for 1140 = 11 (11 * 102 = 1122)

		if(pairs > 9){
			pairs = 0;
			bundles++;
		}
		if(bundles > 11){
			bundles = 0;
			boxes++;
			pairs = Math.max(0, n - boxes*144);
		}
		if(bundles == 11 && pairs > 1){
			bundles = 0;
			pairs = 0;
			boxes++;
		}
		
		p[0] = boxes;
		p[1] = bundles;
		p[2] = pairs;
		return p;
	}
	
	public static void main(String args[]) {
		new WholeSalePurchase().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}