package com.moodaye.playground.programmerSchool;

import static org.junit.Assert.*;

import org.junit.Test;

public class ChessTest {

	@Test
	public void test() {
		assertEquals("NO", Chess.checkKnightsMove("E2-E4"));
		assertEquals("ERROR", Chess.checkKnightsMove("BSN"));
		assertEquals("ERROR", Chess.checkKnightsMove("C7-X4"));
		assertEquals("ERROR", Chess.checkKnightsMove("X4-C7"));
		assertEquals("YES", Chess.checkKnightsMove("C7-D5"));
	}
}
