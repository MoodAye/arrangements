package com.moodaye.playground.programmerSchool;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

/** Programmer's School - #95 
 * initially wrongly thought that input of 10^1000 reflected size of input string.  Some 
 * off the code below that is commented out is actually trying to solve for that very long string.
 */
public class Numerologist {
	public int transforms = 0;

	void solve(Scanner in, PrintWriter out) {
		String input = in.next();
		out.printf("%d %d", reduce(input), transforms);
		
		// for now use this --- refactor later using BufferedReader or something
		// so that
		// only a single char is read at a time.
	/*	
		int memi = 0;
		long[] mem = new long[100];
		for (int i = 1; i <= input.length(); i++) {
			mem[memi] += Integer.valueOf(input.substring(i-1, i));
			if (i % 1_000_000 == 0) {
				memi++;
			}
		}
		out.printf("%d %d", reduceMem(mem), transforms);
	  */
	}
	
	public int reduce(String input){
		if(input.length() == 1){
			return Integer.valueOf(input);
		}
		transforms++;
		int sum = 0;
		for(int i = 0; i < input.length(); i++){
			sum += Integer.valueOf(input.substring(i, i + 1));
		}
		return reduce(String.valueOf(sum));
	}

	public long reduceMem(long[] mem) {
		// scan mem to see if reduction necessary
		boolean reduce = false;
		while (true) {
			for (int i = 0; i < mem.length; i++) {
				if (mem[i] >= 10) {
					reduce = true;
					break;
				}
			}
			if (reduce) {
				transforms++;
				for (int i = 0; i < mem.length; i++) {
					mem[i] = reduce(mem[i]);
					reduce = false;
				}
			} else {
				break;
			}
		}

		int sum = 0;
		for (int i = 0; i < mem.length; i++) {
			sum += mem[i];
		}
		return reduce(sum);
	}

	public long reduce(long k) {
		long sum = 0;
		if (k < 10) {
			return k;
		} else {
			String s = String.valueOf(k);
			for (int i = 0; i < s.length(); i++) {
				sum += Long.valueOf(s.substring(i, i + 1));
			}
		}
		return sum;
	}

	/**
	 * Cannot use int or long because the max total of the digits is 10^1000 * 9
	 * ~ 10^1001. The max int is ~10^9. The max long is ~10^20.
	 * 
	 * We cannot simply use a string (~char[]) and sum the digits since the max
	 * sum can exceed the max of the int or long.
	 * 
	 * Try divide and conquer.
	 * 
	 * Break the input into certain number of digits so that the max of the sum
	 * is well below the max long number supported by java (say 10^12). The max
	 * number of time the initial input will need to be divided is 1001 / 12 =
	 * 53. So create an array of longs to hold these sums - of size 100. Then
	 * use recursion for each of these to sum them up.
	 * 
	 * E.g, --- say number is 123_456_789. Answer = 9 ( sum = 45 - after
	 * transform = 9) . Say we are break this into groups of three. initialize
	 * and array of size 5. Read the first char = 1; sum+=1 = 1; char = 2; sum
	 * += 2 = 3; read 3 --> sum+=3 == 6 Number of operations = 3 reads and 3
	 * additions. = 6. Number of transforms = 1. Next we read 456 --> sum = 15
	 * --> mem[1] = 15. Read the last 3 digits --> sum = 24 -->mem[2] = 24.
	 * 
	 * Run first transform.... mem[0] =6; mem[1] = 6; mem[2] = 6. 6 + 6 + 6 =
	 * 18.
	 * 
	 * Read 18 --> 1 + 8 = 9. mem[0] = 9. The sum of all elements of array is
	 * less than 10 - so return (this is the base case).
	 * 
	 * The memory requirements for input 10^1000 bytes ( 2 byte per char) So
	 * that would be 10^1000 * 16.
	 * 
	 * Memory limit is 16 MB --> 16 * 10^9.
	 * 
	 * So we need to solve for memory as well.
	 * 
	 * If we read the chars streaming in and store them in a long. We need max
	 * 53 longs so the mem usage will be max of 8 bytes per long * 53 ---> which
	 * is ~500 bytes.
	 * 
	 * So strategy will have to be to read in less than 10^9 characters... say
	 * 10^6 characters. Then max mem usage will be 10^6 * 2 bytes per char +
	 * 1001/6 longs * 8 bytes per long --> max = 2*10^6 + 137 * 8 < 2^10^8 which
	 * is well below the max.
	 * 
	 */

	public static void main(String[] args) {
		new Numerologist().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
