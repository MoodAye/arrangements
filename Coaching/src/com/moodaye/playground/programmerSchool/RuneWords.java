package com.moodaye.playground.programmerSchool;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

/** Programmer's School #283 */
public class RuneWords {
	private static final String NO = "No";
	private static final String YES = "Yes";
	
	void solve(Scanner in, PrintWriter out){
		String input = in.next();
		
		if(!isUpper(input.charAt(0)) || 
				isUpper(input.charAt(input.length() - 1))){
			out.print(NO);
			return;
		}
		
		boolean isRune = true;
		int lowerCount = 0;
		for(int i = 1; i < input.length(); i++){
			char c = input.charAt(i);
			
			if(!isLetter(c)){
				isRune = false;
				break;
			}
			
			if(isUpper(c)){
				if(lowerCount == 0){
					isRune = false;
					break;
				}
				lowerCount = 0;
				continue;
			}
			
			lowerCount++;
			if(lowerCount > 3){
				isRune = false;
				break;
			}
		}
		
		if(isRune){
			out.print(YES);
		}
		else{
			out.print(NO);
		}
	}
	
	boolean isLetter(char c){
		return ((c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z'));
	}
	
	boolean isUpper(char c){
		return (c >= 'A' && c <= 'Z');
	}
	

	public static void main(String[] args) {
		new RuneWords().run();
	}
	
	void run(){
		Locale.setDefault(Locale.US);
		try(Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)){
			solve(in, out);
		}
	}
}
