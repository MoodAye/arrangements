package com.moodaye.playground.programmerSchool.May2019;

import java.util.Scanner;
import java.util.Locale;
import java.util.Map;
import java.util.List;
import java.util.ArrayList;
import java.util.HashMap;
import java.io.PrintWriter;

/** Programmer's School #87 */
// 7:50am 8:03am (13min)
public class Strings2 {
	void solve(Scanner in, PrintWriter out) {
		Map<String, Integer> list = new HashMap<>();
		
		while(true) {
			String s = in.next();
			if(s.equals("ENDOFINPUT")) {
				break;
			}
			list.merge(s, 1, Integer::sum);
		}
		int cnt = 0;
		List<String> keys = new ArrayList<>(list.keySet());
		for(String s1 : keys) {
			for(String s2 : keys) {
				if(s1.length() + s2.length() > 100) {
					continue;
				}
				String combined = s1 + s2;
				if(list.containsKey(combined)) {
					cnt += list.get(combined);
					list.remove(combined);
				}
			}
		}
		out.println(cnt);
	}

	public static void main(String args[]) {
		new Strings2().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
