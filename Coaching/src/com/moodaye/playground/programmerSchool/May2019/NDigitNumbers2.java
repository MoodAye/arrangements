package com.moodaye.playground.programmerSchool.May2019;

import java.util.Scanner;
import java.util.stream.LongStream;
import java.util.Arrays;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #673 */
public class NDigitNumbers2 {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		if (n == 1) {
			out.println("10 0");
			return;
		}
		int cnt = 0;
		String min = "";
		int idx = n - 1;
		int[] a = new int[n];
		Arrays.fill(a, 1);
		while (true) {
			if (a[n - 1] == 10) { // or a[idx]??
				idx = next(a);
				if(idx == -1){
					break;
				}
				a[idx]++;
				for (int i = idx + 1; i < n; i++) {
					a[i] = a[idx];
				}
				idx = n - 1;
			}
			if(sumEqualsProd(a)){
				if(min.equals("")){
					min = p(a);
				}
				cnt += perms(a);
			}
			a[idx]++;
		}
		out.println(cnt + " " + min);
	}
	
	int next(int[] a) {
		int n = a.length;
		for (int i = n - 1; i >= 0 && i >= n - 5; i--) {
			if (a[i] < 9) {
				return i;
			}
		}
		return -1;
	}

	String p(int[] a) {
		StringBuilder sb = new StringBuilder();
		for (int i : a) {
			sb.append(i);
		}
		return sb.toString();
	}
	
	boolean sumEqualsProd(int[] a){
		long sum = 0;
		long prod = 1;
		for(int i : a){
			sum += i;
			prod *= i;
		}
		return sum == prod;
	}

	long factorial(int n) {
		return LongStream.rangeClosed(2, n).reduce(1L, (a, b) -> a * b);
	}
	
	long perms(int[] a){
		int[] cnt = new int[10];
		for(int i : a){
			cnt[i]++;
		}
		long p = 1L;
		for (int i = a.length; i > cnt[1]; i--) {
			p *= i;
		}
		for (int i = 2; i <= 9; i++) {
			p /= factorial(cnt[i]);
		}
		return p;
	}

	public static void main(String args[]) {
		new NDigitNumbers2().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
