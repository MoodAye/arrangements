package com.moodaye.playground.programmerSchool.May2019;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #447 */
// start = 9:29pm - 9:35pm
public class LastDigitOfNFactorial {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int cnt2 = 0;
		int cnt5 = 0;
		int lastDigit = 1;
		for(int i = 2; i <= n; i++){
			int temp = i;
			while(temp % 2 == 0){
				cnt2++;
				temp /= 2;
			}
			while(temp % 5 == 0){
				cnt5++;
				temp /= 5;
			}
			lastDigit = (temp * lastDigit) % 10;
		}
		cnt2 -= cnt5;
		while(cnt2-- > 0){
			lastDigit = (2 * lastDigit) % 10;
		}
		out.println(lastDigit);
	}

	public static void main(String args[]) {
		new LastDigitOfNFactorial().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
