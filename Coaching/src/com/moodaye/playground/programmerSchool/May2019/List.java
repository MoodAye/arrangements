package com.moodaye.playground.programmerSchool.May2019;

import java.util.Scanner;
import java.util.Arrays;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #444 */
// 1219am - 12:34pm -- time taken = 15min
public class List {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int[] numbers = new int[n];
		for(int i = 0; i < n; i++){
			numbers[i] = in.nextInt();
		}
		Arrays.sort(numbers);
		int start = numbers[0];
		int prev = start;
		int next = start;
		for(int i = 1; i < n; i++){
			prev = next;
			next = numbers[i];  // not needed --- just use  numbers[i]
			if(next - prev > 1){
				out.print(getShortestList(start, prev));
				start = next;
			}
		}
		String last = getShortestList(start, next);
		out.print(last.substring(0, last.length() - 2));
	}
	
	private String getShortestList(int start, int end){
		StringBuilder sbDots = new StringBuilder();
		sbDots.append(start).append(", ..., ").append(end).append(", ");
		
		StringBuilder sbFull = new StringBuilder();
		for(int i = start; i <= end; i++){
			sbFull.append(i).append(", ");
			if(sbFull.length() > sbDots.length()){
				return sbDots.toString();
			}
		}
		
		return sbFull.toString();
	}

	public static void main(String args[]) {
		new List().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}