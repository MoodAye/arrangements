package com.moodaye.playground.programmerSchool.May2019;

import java.util.Locale;

import java.io.InputStream;
import java.io.PrintWriter;

/** Programmer's School #20 */
// 445pm - 453pm  Time = 8min
public class SawLikeSequence {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		if(n == 0){
			out.println(0);	
			return;
		}
		int prev = in.nextInt();
		int prevPrev = prev;
		int curr = prev;
		int len = 0;
		int maxLen = 1;
		for(int i = 1; i < n; i++){
			prevPrev = prev;
			prev = curr;
			curr = in.nextInt();
			if((prevPrev < prev && prev > curr) ||
			   (prevPrev > prev && prev < curr)){
				len++;
			}
			else if(prev == curr){
				len = 1;
			}
			else{
				len = 2;
			}
			maxLen = Math.max(len, maxLen);
		}
		out.println(maxLen);
	}

	public static void main(String args[]) {
		new SawLikeSequence().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}

	static class Scanner implements AutoCloseable {
		InputStream is;
		int pos = 0;
		int size = 0;
		byte[] buffer = new byte[1024];

		Scanner(InputStream is) {
			this.is = is;
		}

		int nextChar() {
			if (pos >= size) {
				try {
					size = is.read(buffer);
				} catch (java.io.IOException e) {
					throw new java.io.IOError(e);
				}
				pos = 0;
				if (size == -1) {
					return -1;
				}
			}
			Assert.check(pos < size);
			int c = buffer[pos] & 0xFF;
			pos++;
			return c;
		}

		int nextInt() {
			int c = nextChar();
			while (c == ' ' || c == '\r' || c == '\n' || c == '\t') {
				c = nextChar();
			}
			if (c == '-') {
				c = nextChar();
				Assert.check('0' <= c && c <= '9');
				int n = -(c - '0');
				c = nextChar();
				while ('0' <= c && c <= '9') {
					int d = c - '0';
					c = nextChar();
					Assert.check(n > Integer.MIN_VALUE / 10
							|| n == Integer.MIN_VALUE / 10 && d <= -(Integer.MIN_VALUE % 10));
					n = n * 10 - d;
				}
				return n;
			} else {
				Assert.check('0' <= c && c <= '9');
				int n = c - '0';
				c = nextChar();
				while ('0' <= c && c <= '9') {
					int d = c - '0';
					c = nextChar();
					Assert.check(
							n < Integer.MAX_VALUE / 10 || n == Integer.MAX_VALUE / 10 && d <= Integer.MAX_VALUE % 10);
					n = n * 10 + d;
				}
				return n;
			}
		}

		@Override
		public void close() {
		}
	}

	static class Assert {
		static void check(boolean e) {
			if (!e) {
				throw new AssertionError();
			}
		}
	}
}
