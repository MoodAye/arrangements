package com.moodaye.playground.programmerSchool.May2019;

import java.util.Scanner;
import java.util.Arrays;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #245 */
// ~12min
public class UnitedTeam {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		if(n == 0){
			out.println(0);
			return;
		}
		if(n == 1){
			out.println(in.nextInt());
			return;
		}
		
		int[] pi = new int[n];
		for (int i = 0; i < n; i++) {
			pi[i] = in.nextInt();
		}
		Arrays.sort(pi);

		int hi = n - 1;
		int maxPi = pi[hi] + pi[n - 2];
		int currPi = maxPi;
		for (int i = n - 3; i >= 0; i--){
			currPi += pi[i];
			while (pi[i] + pi[i + 1] < pi[hi]) {
				currPi -= pi[hi];
				hi--;
			}
			maxPi = Math.max(currPi, maxPi);
		}
		out.println(maxPi);
	}

	public static void main(String args[]) {
		new UnitedTeam().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}