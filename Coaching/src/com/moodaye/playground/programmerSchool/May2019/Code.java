package com.moodaye.playground.programmerSchool.May2019;

import java.util.Scanner;
import java.util.Arrays;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #301 */
// Start = 9:41pm - 10:04 --- 23min
public class Code {
	void solve(Scanner in, PrintWriter out) {
		int s = in.nextInt();
		int n = in.nextInt();
		int[] min = new int[n];
		int[] max = new int[n];

		int sum = s;
		int i = 0;
		while (sum > 0) {
			max[i] = Math.min(sum, 9);
			sum -= max[i++];
		}
		Arrays.stream(max).forEach(out::print);
		out.print(" ");
		
		min[0] = Math.max(s - 9 * (n - 1), 1);
		sum = s - min[0];
		i = 1;
		while(sum > 0){
			min[i] = Math.max(sum - 9 * (n - i - 1), 0);
			sum -= min[i++];
		}
		Arrays.stream(min).forEach(out::print);
	}

	public static void main(String args[]) {
		new Code().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
