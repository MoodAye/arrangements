package com.moodaye.playground.programmerSchool.May2019;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #196 */
// 20min
public class Spiral {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int[][] matrix = new int[n][n];
		int dx = 0;
		int dy = 1;
		int x = 0;
		int y = -1;
		int cntr = 1;

		for (int r = 0; r < n; r++) {
			for (int c = 0; c < n; c++) {
				x += dx;
				y += dy;
				if (turn(matrix, x, y)) {
					x -= dx;
					y -= dy;
					int temp = dx;
					dx = dy * 1;
					dy = temp * -1;
					x += dx;
					y += dy;
				}
				matrix[x][y] = cntr++;
			}
		}
		
		int w = String.valueOf(n * n).length() + 1;
		for (int r = 0; r < n; r++) {
			for (int c = 0; c < n; c++) {
				out.printf("%" + w + "d", matrix[r][c]);
			}
			out.println();
		}
	}

	private boolean turn(int[][] matrix, int row, int col){
		int n = matrix[0].length;
		return (row == n || col == n || row == -1 || col == -1 || matrix[row][col] != 0);
	}

	public static void main(String args[]) {
		new Spiral().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
