package com.moodaye.playground.programmerSchool.May2019;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #666 */
// ~5min
public class Abracadabra {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		//length
		int len = 1;
		for(int i = 2; i <= 26; i++){
			len = (len * 2) + 1;  // 1, 3, 7, 15
		}
		char letter = 'z';
		while(n != 1){
			len = (len - 1) / 2;
			n--;
			if(n > len){
				n = n - len;
			}
			letter--;
		}
		out.println(letter);
	}

	public static void main(String args[]) {
		new Abracadabra().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}

}
