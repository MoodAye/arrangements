package com.moodaye.playground.programmerSchool.May2019;

import java.util.Scanner;
import java.util.Arrays;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #449 */
// 605pm - 616pm (11min)
public class FloatingNumbers {
	void solve(Scanner in, PrintWriter out) {
		int ell = in.nextInt();
		int n = in.nextInt();
		
		int[] a = new int[32768 * 2];
		for (int i = 0; i < n; i++) {
			a[in.nextInt() + 32768]++;
		}
		
		int minCnt = 0;
		for(int i = 0; i < a.length; i++) {
			if(a[i] > 0) {
				minCnt++;
				i += 2 * ell + 1;
			}
		}
		out.println(minCnt);
	}

	public static void main(String args[]) {
		new FloatingNumbers().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
