package com.moodaye.playground.programmerSchool.May2019;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #71 */
// start = 11:52am End = 12:05pm (~13min)
public class TwoPilesOfStones {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int[] weights = new int[n];
		for(int i = 0; i < n; i++){
			weights[i] = in.nextInt();
		}
		out.println(minDiff(weights, 1, weights[0], 0));
	}
	
	int minDiff(int[] weights, int nextStone, int pile1, int pile2){
		if(nextStone == weights.length){
			return Math.abs(pile1 - pile2);
		}
		
		int w1 = minDiff(weights, nextStone + 1, pile1 + weights[nextStone], pile2);
		int w2 = minDiff(weights, nextStone + 1, pile1, pile2 + weights[nextStone]);
		return Math.min(w1, w2);
	}

	public static void main(String args[]) {
		new TwoPilesOfStones().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
