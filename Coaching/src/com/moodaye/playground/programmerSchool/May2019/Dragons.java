package com.moodaye.playground.programmerSchool.May2019;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #42 */
// 1120 - 1135  (15min)
public class Dragons {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		if(n < 3){
			out.println(n);
			return;
		}
		long strength = 1L;
		if(n % 3 == 1){
			n -= 4;
			strength *= 4L;
		}
		else if(n % 3 == 2){
			strength *= 2;
		}
		int cnt3 = n / 3;
		while(cnt3-- > 0){
			strength *= 3L;
		}
		out.println(strength);
	}

	public static void main(String args[]) {
		new Dragons().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
