package com.moodaye.playground.programmerSchool.May2019;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #318 */
public class NextNumber {
	void solve(Scanner in, PrintWriter out) {
		long n = in.nextLong();
		// n = 1101 0110 (say)
		// tail1 (first "1" from right) : 1101 0110 --> 0000 0010
		long tail1 = n & ~(n - 1);
		// resultHead (rh) = 1101 0110 + 0000 0010 = 1101 1000
		long resultHead = n + tail1;
		// oldOnes = n & ~rh = 1101 0110 & 0010 0111 = 0000 0110
		long oldOnes = n & ~resultHead;
		// 0000 0110 / 0000 0010 / 2 = 6/2/2 = 1
		long newOnes = oldOnes / tail1 / 2;
		// 1101 1000 + 0000 0001 = 1101 1001
		out.println(resultHead + newOnes);
		//int i = Integer.MAX_VALUE;
	}

	public static void main(String args[]) {
		new NextNumber().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
