package com.moodaye.playground.programmerSchool.May2019;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #673 */
// 2:58pm  - 3:14pm  (16 min)
public class NDigitNumbers2_BT {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		if(n == 1){
			out.println("10 0");
			return;
		}
		
		int[] digits = new int[n];
		Answer ans = new Answer();
		count(0, digits, 0, 1, ans);
		out.print(ans.cnt + " ");
		for (int d : ans.min) {
			out.print(d);
		}
	}

	void count(int index, int[] digits, int sum, int product, Answer ans) {
		if (index == digits.length) {
			if (sum == product) {
				if (ans.cnt == 0) {
					ans.min = digits.clone();
				}
				ans.cnt++;
			}
			return;
		}

		for (int digit = 1; digit <= 9 && product * digit <= sum + digit + (digits.length - index - 1) * 9; digit++) {
			digits[index] = digit;
			count(index + 1, digits, sum + digit, product * digit, ans);
		}
	}

	public static void main(String args[]) {
		new NDigitNumbers2_BT().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}

class Answer {
	int cnt = 0;
	int[] min = null;
}