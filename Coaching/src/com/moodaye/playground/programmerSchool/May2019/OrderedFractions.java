package com.moodaye.playground.programmerSchool.May2019;

import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #422 */
// ~20min
public class OrderedFractions {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		Set<Fraction> list = new TreeSet<>();
		for(int den = 2; den <= n; den++){
			for(int num = 1; num < den; num++){
				list.add(new Fraction(num, den));
			}
		}
		
		for(Fraction f : list){
			out.println(f.toString());
		}
	}

	public static void main(String args[]) {
		new OrderedFractions().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}

class Fraction implements Comparable<Fraction>{
	int num;
	int den;
	Fraction(int num, int den){
		this.num = num;
		this.den = den;
		reduce();
	}
	
	@Override
	public int compareTo(Fraction arg0) {
		return num * arg0.den - den * arg0.num;
	}
	
	@Override
	public String toString(){
		return num + "/" + den;
	}
	
	@Override
	public boolean equals(Object o){
		Fraction fo = (Fraction) o;
		return fo.num == num && fo.den == den;
	}
	
	private void reduce(){
		while(true){
			int g = gcd(num, den);
			if(g == 1){
				break;
			}
			num /= g;
			den /= g;
		}
	}
	
	int gcd(int a, int b){
		while(a != 0){
			int temp = a;
			a = b % a;
			b = temp;
		}
		return b;
	}
}