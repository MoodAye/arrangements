package com.moodaye.playground.programmerSchool.May2019;

import java.util.Scanner;
import java.util.Arrays;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #472 */
// 752am - 8:10  = 18 min
public class Gifts {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int add = in.nextInt();
		int[] gifts = new int[n];
		
		for (int i = 0; i < n; i++) {
			gifts[i] = in.nextInt();
		}
		
		Arrays.sort(gifts);
		int i;
		for (i = 0; i < n - 1; i++) {
			if(gifts[i] == gifts[i + 1]) {
				continue;
			}
			if((((gifts[i + 1] -gifts[i])) * (i + 1)) > add){
				break;
			}
			add -= (gifts[i + 1] -gifts[i]) * (i + 1);
		}
		if(add == 0) {
			out.println(gifts[i]);
		}
		else {
			out.println(gifts[i] + add / (i + 1));
		}
	}

	public static void main(String args[]) {
		new Gifts().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
