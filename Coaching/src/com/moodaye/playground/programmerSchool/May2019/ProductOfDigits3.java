package com.moodaye.playground.programmerSchool.May2019;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School 669 */
// 10:48 - 10:56 (8min)
public class ProductOfDigits3 {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		
		StringBuilder smallest = new StringBuilder();
		int copyN = n;
		for(int i = 9; i >= 2; i--){
			while(copyN % i == 0){
				smallest.append(i);
				copyN /= i;
			}
		}
		if(copyN != 1){
			out.println(-1 + " " + -1);
			return;
		}
		else{
			out.println(smallest.reverse().toString() + " ");
		}
		
		StringBuilder largest = new StringBuilder();
		for(int i = 2; i <= 9; i++){
			while(n % i == 0){
				largest.append(i);
				n /= i;
			}
		}
		out.println(largest.reverse().toString());
	}

	public static void main(String args[]) {
		new ProductOfDigits3().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
	
}
