package com.moodaye.playground.programmerSchool.May2019;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #267 */
//  6:48pm - 655pm (7min)
public class Copies {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int x = in.nextInt();
		int y = in.nextInt();
		
		n--; //first copy
		
		// n = 29.  x = 4, y = 7.  t = 29*4*7/(11) = 56s
		long t = 1L * n * x* y/ (x+ y);
		
		// additional = n - (56/4 + 56/7) = 29 - 14 - 8 = 7
		int additionalCopiesNeeded = (int) (n - (t / x + t / y));
	
		// (56/4 + 7) * 4 = (14 + 7) * 4 = 21 * 4 = 84
		long timex = (t / x + additionalCopiesNeeded) * x;
		// (56/7 + 7) * 7 = (8 + 7) * 7 =- 15*7 = 105
		long timey = (t / y + additionalCopiesNeeded) * y;
		
		t = Math.min(timex, timey);
		out.println(t + Math.min(x, y));
		
		
	}

	public static void main(String args[]) {
		new Copies().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
