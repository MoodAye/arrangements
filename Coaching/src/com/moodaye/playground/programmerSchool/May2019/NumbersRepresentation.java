package com.moodaye.playground.programmerSchool.May2019;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #255 */
// Start = 1:06pm - end = 1:11pm (time = 5min)
public class NumbersRepresentation {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		
		for(int i = 2; i * i <= n; i++){
			if(n % i == 0){
				out.println(n / i + " " + (n - n / i));
				return;
			}
		}
		out.println(1 + " " + (n - 1));
	}

	public static void main(String args[]) {
		new NumbersRepresentation().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}

}