package com.moodaye.playground.programmerSchool.May2019;

import java.util.Arrays;
import java.util.Locale;
import java.io.InputStream;
import java.io.PrintWriter;

/** Programmer's School #630 */
// 9:58pm - 10:43 (45min)
// 12:23am - 12:36 (13min)
public class Guards {
	void solve(Scanner in, PrintWriter out) {
		int tests = in.nextInt();
		int[] times = new int[10_002];
		while (tests-- > 0) {
			Arrays.fill(times, 0);
			int n = in.nextInt();
			int[] startTimes = new int[n];
			int[] endTimes = new int[n];
			for (int i = 0; i < n; i++) {
				startTimes[i] = in.nextInt();
				endTimes[i] = in.nextInt();
				times[startTimes[i] + 1]++;
				times[endTimes[i] + 1]--;
			}

			// count guards
			boolean noGuard = false;
			for (int i = 1; i < times.length - 1; i++) {
				times[i] += times[i - 1];
				if (times[i] == 0) {
					noGuard = true;
					break;
				}
			}
			if (noGuard) {
				out.println("Wrong Answer");
				continue; // next test
			}
			
			boolean extraGuard = true;
			boolean wrongAnswer = false;
			for(int i = 0; i < n; i++) {
				for(int j = startTimes[i] + 1; j <= endTimes[i]; j++) {
					if(times[j] == 1) {
						extraGuard = false;
						break;
					}
				}
				if(extraGuard) {
					wrongAnswer = true;
					break;
				}
				extraGuard = true;
			}
			out.println(wrongAnswer ? "Wrong Answer" : "Accepted");
		}

	}

	public static void main(String[] args) {
		new Guards().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}

	static class Scanner implements AutoCloseable {

		InputStream is;
		byte buffer[] = new byte[1 << 16];
		int size = 0;
		int pos = 0;

		Scanner(InputStream is) {
			this.is = is;
		}

		int nextChar() {
			if (pos >= size) {
				try {
					size = is.read(buffer);
				} catch (java.io.IOException e) {
					throw new java.io.IOError(e);
				}
				pos = 0;
				if (size == -1) {
					return -1;
				}
			}
			Assert.check(pos < size);
			int c = buffer[pos] & 0xFF;
			pos++;
			return c;
		}

		int nextInt() {
			int c = nextChar();
			while (c == ' ' || c == '\r' || c == '\n' || c == '\t') {
				c = nextChar();
			}
			if (c == '-') {
				c = nextChar();
				Assert.check('0' <= c && c <= '9');
				int n = -(c - '0');
				c = nextChar();
				while ('0' <= c && c <= '9') {
					int d = c - '0';
					c = nextChar();
					Assert.check(n > Integer.MIN_VALUE / 10
							|| n == Integer.MIN_VALUE / 10 && d <= -(Integer.MIN_VALUE % 10));
					n = n * 10 - d;
				}
				return n;
			} else {
				Assert.check('0' <= c && c <= '9');
				int n = c - '0';
				c = nextChar();
				while ('0' <= c && c <= '9') {
					int d = c - '0';
					c = nextChar();
					Assert.check(
							n < Integer.MAX_VALUE / 10 || n == Integer.MAX_VALUE / 10 && d <= Integer.MAX_VALUE % 10);
					n = n * 10 + d;
				}
				return n;
			}
		}

		@Override
		public void close() {
		}
	}

	static class Assert {
		static void check(boolean e) {
			if (!e) {
				throw new AssertionError();
			}
		}
	}
}