package com.moodaye.playground.programmerSchool.May2019;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #170 */
// 5min
public class NumberDecomposition {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		// t.a + s = n... a = (n - s) / t
		int t = 1;
		int s = 1;
		int max = 1;
		while(s < n){
			t++;
			s += t;
			if((n - s) % t == 0){
				max = t;
			}
		}
		out.println(max);
	}

	public static void main(String args[]) {
		new NumberDecomposition().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}