package com.moodaye.playground.programmerSchool.May2019;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #671 */
// 11:11pm - 11:26 (Time = 15min)
public class LuckyNumbers {
	void solve(Scanner in, PrintWriter out) {
		char[] n = in.next().toCharArray();
		long cnt = (1L << n.length) - 2; 
		
		for(int i = 0; i < n.length; i++) {
			if(n[i] < '4') {
				break;
			}
			else if(n[i] == '4') {
				if(i == n.length - 1) {
					cnt += 1;
					break;
				}
				continue;
			}
			else if(n[i] < '7') {
				cnt += (1L << (n.length - i - 1));
				break;
			}
			else if(n[i] == '7') {
				if(i == n.length - 1) {
					cnt += 2;
					break;
				}
				cnt += (1L << (n.length - i - 1));
				continue;
			}
			else {
				cnt += (1L << n.length - i);
				break;
			}
		}
		out.println(cnt);
	}

	public static void main(String args[]) {
		new LuckyNumbers().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}

	
