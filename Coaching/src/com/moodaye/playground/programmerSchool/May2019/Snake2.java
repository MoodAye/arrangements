package com.moodaye.playground.programmerSchool.May2019;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #359 */
// 747am - 7:53
public class Snake2 {
	void solve(Scanner in, PrintWriter out) {
		int col = in.nextInt();
		// squares filled prior columns
		// 1 + 2 + ... + n - 1
		long squares = 1L * (1 + col - 1) * (col - 1) / 2;  // need long
		// squares in current column is n
		 squares += col / 2 + (col % 2);
		 
		 // count ... but remove and multiples of 10
		 squares += (squares - 1) / 9;
		 out.println(squares);
	}

	public static void main(String args[]) {
		new Snake2().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}