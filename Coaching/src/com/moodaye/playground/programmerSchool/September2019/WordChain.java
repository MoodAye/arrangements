package com.moodaye.playground.programmerSchool.September2019;

import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Locale;
import java.util.Scanner;

// Problem 872
public class WordChain {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		String[] words = new String[n];
		for(int i = 0; i < n; i++) {
			words[i] = in.next();
		}
		
		Arrays.sort(words);
		int[] cnts = new int[n];
		Arrays.fill(cnts, 1);
		
		for(int i = 1; i < n; i++) {
			for(int j = i - 1; j >= 0; j--) {
				if(words[i].startsWith(words[j]) && !words[i].equals(words[j]) ) {
					cnts[i] = Math.max(cnts[i], cnts[j] + 1);
				}
			}
		}
		out.println(Arrays.stream(cnts).max().getAsInt());
	}
	
	public static void main(String[] args) {
		new WordChain().run();
	}
	void run() {
		Locale.setDefault(Locale.US);
		try(Scanner in = new Scanner(System.in);
				PrintWriter out = new PrintWriter(System.out)){
			solve(in, out);
		}
	}
}
