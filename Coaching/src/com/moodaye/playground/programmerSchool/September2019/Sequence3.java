package com.moodaye.playground.programmerSchool.September2019;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;

// Problem 789
// Time > 1hr
public class Sequence3 {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		if(n <= 3) {
			out.println(n);
			return;
		}
		TreeSet<Long> seq = new TreeSet<>();
		long next = 1L;
		seq.add(next);
		for(int i = 1; i <= n; i++) {
			next = seq.first();
			seq.add(next * 2);
			seq.add(next * 3);
			seq.add(next * 5);
			seq.remove(next);
		}
		 out.println(next);
	}
	
	public static void main(String[] args) {
		new Sequence3().run();
	}
	void run() {
		Locale.setDefault(Locale.US);
		try(Scanner in = new Scanner(System.in);
				PrintWriter out = new PrintWriter(System.out)){
			solve(in, out);
		}
	}
}
