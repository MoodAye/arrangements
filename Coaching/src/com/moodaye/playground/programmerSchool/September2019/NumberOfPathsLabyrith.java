package com.moodaye.playground.programmerSchool.September2019;

import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Locale;
import java.util.Scanner;

public class NumberOfPathsLabyrith {
	void solve4(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int k = in.nextInt();

		boolean[][] allowed = new boolean[n + 1][n + 1];
		for (int i = 0; i <= n; i++) {
			Arrays.fill(allowed[i], true);
		}

		for (int r = 1; r <= n; r++) {
			String next = in.next();
			for (int c = 1; c <= n; c++) {
				char nextCell = next.charAt(c - 1);
				if (nextCell == '1') {
					allowed[r][c] = false;
				}
			}
		}
		
		int[][] grid = new int[n + 2][n + 2];
		grid[1][1] = 1;
		for (int stepCount = 1; stepCount <= k; stepCount++) {
			for (int r = 1; r <= n; r++) {
				for (int c = 1; c <= n; c++) {
					if (((r + c + stepCount) % 2) == 0) {
						if (allowed[r][c]) {
							grid[r][c] = grid[r - 1][c] + grid[r + 1][c] + grid[r][c - 1] + grid[r][c + 1];
						}

// wierd compile error for below statement with = instead of +
//						grid[r][c] = grid[r - 1][c] + grid[r + 1][c] + grid[r][c - 1] = grid[r][c + 1];
					}
				}
			}
		}
		out.println(grid[n][n]);
	}

	void solve3(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int k = in.nextInt();

		boolean[][] allowed = new boolean[n + 1][n + 1];
		for (int i = 0; i <= n; i++) {
			Arrays.fill(allowed[i], true);
		}
		for (int r = 1; r <= n; r++) {
			String next = in.next();
			for (int c = 1; c <= n; c++) {
				char nextCell = next.charAt(c - 1);
				if (nextCell == '1') {
					allowed[r][c] = false;
				}
			}
		}

		int[][] gridCurr = new int[n + 2][n + 2];
		int[][] gridPrev = new int[n + 2][n + 2];

		gridCurr[1][1] = 1;

		int stepCount = 1;
		while (k >= stepCount++) {
			int[][] temp = gridPrev;
			gridPrev = gridCurr;
			gridCurr = temp;
			for (int r = 1; r <= n; r++) {
				for (int c = 1; c <= n; c++) {
					if (allowed[r][c]) {
						gridCurr[r][c] = gridPrev[r - 1][c] + gridPrev[r + 1][c] + gridPrev[r][c - 1]
								+ gridPrev[r][c + 1];
					}
				}
			}
		}
		out.println(gridCurr[n][n]);
	}

	void solve2(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int k = in.nextInt();

		// n+2 precludes need to check for out of bounds
		int[][][] grid = new int[n + 2][n + 2][k + 1];

		boolean[][] allowed = new boolean[n + 1][n + 1];
		for (int i = 0; i <= n; i++) {
			Arrays.fill(allowed[i], true);
		}
		for (int r = 1; r <= n; r++) {
			// todo : user regex
			String next = in.next();
			for (int c = 1; c <= n; c++) {
				char nextCell = next.charAt(c - 1);
				if (nextCell == '1') {
					allowed[r][c] = false;
				}
			}
		}

		grid[1][1][0] = 1;
		for (int steps = 1; steps <= k; steps++) {
			for (int r = 1; r <= n; r++) {
				for (int c = 1; c <= n; c++) {
					if (allowed[r][c]) {
						grid[r][c][steps] = grid[r - 1][c][steps - 1] + grid[r + 1][c][steps - 1]
								+ grid[r][c - 1][steps - 1] + grid[r][c + 1][steps - 1];
					}
				}
			}
		}
		out.println(grid[n][n][k]);
	}

	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int k = in.nextInt();

		int[][][] grid = new int[n + 1][n + 1][k + 1];
		for (int i = 0; i < (n + 1); i++) {
			for (int j = 0; j < (n + 1); j++) {
				Arrays.fill(grid[i][j], -1);
			}
		}

		// todo - do w/o
		boolean[][] allowed = new boolean[n + 1][n + 1];
		for (int i = 0; i <= n; i++) {
			Arrays.fill(allowed[i], true);
		}

		for (int r = 1; r <= n; r++) {
			// todo : user regex
			String next = in.next();
			for (int c = 1; c <= n; c++) {
				char nextCell = next.charAt(c - 1);
				if (nextCell == '1') {
					allowed[r][c] = false;
				}
			}
		}
		out.println(pathCount(n, n, k, allowed, grid));
	}

	int pathCount(int r, int c, int steps, boolean[][] allowed, int[][][] grid) {
		int n = allowed[0].length - 1;
		if (r == 1 && c == 1 && steps == 0) {
			return 1;
		} else if (r > n || c > n || r < 1 || c < 1 || !allowed[r][c] || steps == 0) {
			return 0;
		} else if (grid[r][c][steps] != -1) {
			return grid[r][c][steps];
		}

		grid[r][c][steps] = pathCount(r - 1, c, steps - 1, allowed, grid)
				+ pathCount(r, c - 1, steps - 1, allowed, grid) + pathCount(r + 1, c, steps - 1, allowed, grid)
				+ pathCount(r, c + 1, steps - 1, allowed, grid);

		return grid[r][c][steps];
	}

	public static void main(String[] args) {
		new NumberOfPathsLabyrith().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			// solve(in, out);
			// solve2(in, out);
			// solve3(in, out);
			solve4(in, out);
		}
	}
}
