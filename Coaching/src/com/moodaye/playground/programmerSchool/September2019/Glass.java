package com.moodaye.playground.programmerSchool.September2019;

import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Locale;
import java.util.Scanner;

// Problem 954; 
// Time taken = 830am - 930am ~ 1hr
public class Glass {
	private static final int MOD = 1_000_000;
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		
		if(n < 10) {
			out.println(0);
			return;
		}
		
		int[] h = new int[n + 3];
	
		h[10] = 2;  
		h[11] = 2;  
		h[12] = 2;  
		
		for(int i = 13; i <= n; i++) {
			h[i] = (h[i - 10] + h[i - 10 - 1] + h[i - 10 - 2]) % MOD;
		}
		
		out.println(h[n]);
	}
	
	public static void main(String[] args) {
		new Glass().run();
	}
	
	void run() {
		Locale.setDefault(Locale.US);
		try(Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)){
			solve(in, out);
		}
	}
	
}
