package com.moodaye.playground.programmerSchool.September2019;

import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Locale;
import java.util.Scanner;

// Problem 332
public class MinimumCostOfTravel {
	
	// Alternate solution with space complexity = O(n) 
	// Original solution had s.c. O(n^2)
	void solve2(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		
		int[] minCostToStation = new int[n + 1];
		Arrays.fill(minCostToStation, Integer.MAX_VALUE);
		minCostToStation[0] = 0;
		
		for(int i = 0; i <= n; i++) {
			for(int j = i + 1; j <= n; j++) {
				int fare = in.nextInt();
				minCostToStation[j] = Math.min(minCostToStation[j], minCostToStation[i] + fare);
			}
		}
		
		out.println(minCostToStation[n]);
	}
	
	public static void main(String[] args) {
		new MinimumCostOfTravel().run();
	}
	
	void run() {
		Locale.setDefault(Locale.US);
		try(Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)){
			solve2(in, out);
		}
	}
}
