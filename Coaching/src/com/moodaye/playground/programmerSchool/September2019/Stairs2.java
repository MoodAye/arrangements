package com.moodaye.playground.programmerSchool.September2019;

import java.io.PrintWriter;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Locale;
import java.util.Scanner;

/** Problem 329 */
public class Stairs2 {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int[] a = new int[n + 1];
		for (int i = 1; i <= n; i++) {
			a[i] = in.nextInt();
		}

		int sum_2 = 0;
		int sum_1 = a[1];
		int sum = a[1];
		a[1] = 0;
		if (sum_1 > sum_2) {
			sum = sum_1 + a[2];
			a[2] = 1;
		} else {
			sum = sum_2 + a[2];
			a[2] = 0;
			a[1] = -1;
		}

		sum_2 = sum_1;
		sum_1 = sum;

		for (int i = 3; i <= n; i++) {
			if (sum_2 + a[i] > sum_1 + a[i]) {
				sum = sum_2 + a[i];
				a[i] = i - 2;
			} else {
				sum = sum_1 + a[i];
				a[i] = i - 1;
			}
			sum_2 = sum_1;
			sum_1 = sum;
		}
		out.println(sum);

		Deque<Integer> stack = new ArrayDeque<>();
		for (int i = n; i > 0; i = a[i]) {
				stack.push(i);
		}
		
		while(stack.size() > 0) {
			out.printf("%d ", stack.pop());
		}
	}

	public static void main(String[] args) {
		new Stairs2().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}

	}
}
