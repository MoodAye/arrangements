package com.moodaye.playground.programmerSchool.September2019;

import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Locale;
import java.util.Scanner;

// # Problem 368
public class Route {
	void solve2(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int[][] grid = new int[n][n];

		for (int r = 0; r < n; r++) {
			char[] line = in.next().toCharArray();
			for (int c = 0; c < n; c++) {
				grid[r][c] = line[c] - '0';
			}
		}
		
		for (int r = 0; r < n; r++) {
			for (int c = 0; c < n; c++) {
				if(r == 0 && c == 0) {
					continue;
				}
				if(r == 0) {
					grid[r][c] += grid[r][c - 1];
				}
				else if(c == 0) {
					grid[r][c] += grid[r - 1][c];
				}
				else {
					grid[r][c] += Math.min(grid[r -1 ][c], grid[r][c - 1]);
				}
			}
		}
		
		int row = n - 1;
		int col = n - 1;
		while (row != 0 || col != 0) {
			grid[row][col] = -1;
			if(row == 0) {
				col--;
			}
			else if (col == 0){
				row--;
			}
			else if ((grid[row][col - 1] < grid[row - 1][col])) {
				col--;
			}
			else {
				row--;
			}
		}
		grid[0][0] = -1;

		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				out.printf("%s", grid[i][j] == -1 ? "#" : ".");
			}
			out.println();
		}
	}
					
	public static void main(String[] args) {
		new Route().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve2(in, out);
		}
	}
}
