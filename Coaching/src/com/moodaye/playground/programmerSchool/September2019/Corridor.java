package com.moodaye.playground.programmerSchool.September2019;

import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Locale;
import java.util.Scanner;

// Problem 320
public class Corridor {
	void solve(Scanner in, PrintWriter out) {
		int m = in.nextInt();
		int n = in.nextInt();
		long[] memo = new long[n + 1];
		for (int i = 1; i <= n; i++) {
			Arrays.fill(memo, -1);
		}
		out.println(cntOptions(n, m, memo));
	}

	long cntOptions(int n, int m, long[] memo) {
		if(n < m) {
			return 1;
		}
		if(m == 1) {
			return 1;
		}
		if (n == m) {
			return 2;
		}
		if (memo[n - m] == -1) {
			memo[n - m] = cntOptions(n - m, m, memo);
		}
		if (memo[n - 1] == -1) {
			memo[n - 1] = cntOptions(n - 1, m, memo);
		}
		return memo[n - m] + memo[n - 1];
	}

	public static void main(String[] args) {
		new Corridor().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}

}
