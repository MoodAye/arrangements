package com.moodaye.playground.programmerSchool.September2019;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

// Programmer's School #29
// 9:38am - 9:57am (19min)
public class ComputerGame {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int[] h = new int[n];
		for(int i = 0; i < n; i++) {
			h[i] = in.nextInt();
		}
		
		if(n == 1) {
			out.println(0);
			return;
		}
		
		int sumPrev = Math.abs(h[0] - h[1]);
		int sumPrev_2 = 0;
		for(int i = 2; i < n; i++) {
			int opt1 = Math.abs(h[i - 1] - h[i]) + sumPrev;
			int opt2 = 3 * Math.abs(h[i - 2] - h[i]) + sumPrev_2;
			sumPrev_2 = sumPrev;
			sumPrev = Math.min(opt1, opt2);
		}
		
		out.println(sumPrev);
	}
	
	public static void main(String[] args) {
		new ComputerGame().run();
	}
	
	void run() {
		Locale.setDefault(Locale.US);
		try(Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)){
			solve(in, out);
		}
	}
}
