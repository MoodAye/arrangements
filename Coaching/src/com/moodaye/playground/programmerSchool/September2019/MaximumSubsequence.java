package com.moodaye.playground.programmerSchool.September2019;

import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Locale;
import java.util.Scanner;

// Problem 122
// Start 11:44pm  - 11:57pm
public class MaximumSubsequence {
	void run(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int[] seq = new int[n];
		int[] cnts = new int[n];
		Arrays.fill(cnts, 1);
		
		for(int i = 0; i < n; i++) {
			seq[i] = in.nextInt();
		}
		
		for(int i = 1; i < n; i++) {
			for(int j = i -1; j >= 0; j--) {
				if(seq[j] < seq[i]) {
					cnts[i] = Math.max(cnts[i], cnts[j] + 1);
				}
			}
		}
		out.println(Arrays.stream(cnts).max().getAsInt());
	}
	
	public static void main(String[] args) {
		new MaximumSubsequence().solve();
	}
	
	void solve() {
		Locale.setDefault(Locale.US);
		try(Scanner in = new Scanner(System.in); 
				PrintWriter out = new PrintWriter(System.out)){
			run(in, out);
		}
	}
	
}
