package com.moodaye.playground.programmerSchool.September2019;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

// Problem 268
// solve complexity is O(n^3)
// solve2 complexity is O(n^20
// solve2 and solve3 pass all tests.  solve - tle
public class AlmostPalindrome {

	// not working for
	// 3 3
	// aaa
	void solve3(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int k = in.nextInt();
		char[] word = in.next().toCharArray();

		int cnt = 0;

		for (int i = 0; i < n; i++) {
			int ksingle = k;
			int kdouble = k;
			cnt++;
			for (int j = i + 1; j < n; j++) {
				if ((i - (j - i)) >= 0) {
					if (word[j] != word[i - (j - i)]) {
						ksingle--;
					}
					if (ksingle >= 0) {
						cnt++;
					}
				}
				if (i - (j - i - 1) >= 0) {
					if (word[j] != word[i - (j - i - 1)]) {
						kdouble--;
					}
					if (kdouble >= 0) {
						cnt++;
					}
				}
			}
		}
		out.println(cnt);
	}

	void solve2(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int k = in.nextInt();
		char[] word = in.next().toCharArray();

		int cnt = 0;

		// single character centers
		for (int i = 0; i < n; i++) {
			int k2 = k;
			cnt++;
			// i - (J - 1) >= 0;   i - (i + 1 - 1) = 0; 1; 2; 3... when i = 0
			for (int j = i + 1; j < n && (i - (j - i)) >= 0; j++) {
				if (word[j] != word[i - (j - i)]) {
					k2--;
				}
				if (k2 < 0) {
					break;
				}
				cnt++;
			}
		}

		// for double character centers
		for (int i = 0; i < n - 1; i++) {
			int k2 = k;
			for (int j = i + 1; j < n && (i - (j - i - 1)) >= 0; j++) {
				if (word[j] != word[i - (j - i - 1)]) {
					k2--;
				}
				if (k2 < 0) {
					break;
				}
				cnt++;
			}
		}
		out.println(cnt);
	}

	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int k = in.nextInt();
		char[] word = in.next().toCharArray();

		int cnt = 0;
		for (int len = 1; len <= n; len++) {
			for (int c = 0; c < n - len + 1; c++) {
				int start = c;
				int end = c + len - 1;
				if (almostP(word, start, end, k)) {
					cnt++;
				}
			}
		}

		out.println(cnt);
	}

	boolean almostP(char[] word, int start, int end, int k) {
		// e.g., abcde ---> we need to change 2 letters to make subpalindrome. 2 = 5/2
		if (k >= (end - start + 1) / 2) {
			return true;
		}

		int cnt = 0;
		for (int i = start; i <= (start + end) / 2; i++) {
			if (word[i] != word[end - (i - start)]) {
				cnt++;
			}
			if (cnt > k) {
				return false;
			}
		}
		return true;
	}

	public static void main(String[] args) {
		new AlmostPalindrome().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
//			solve(in, out);
			solve2(in, out);
//			solve3(in, out);
		}
	}
}
