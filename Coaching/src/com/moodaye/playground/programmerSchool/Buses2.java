package com.moodaye.playground.programmerSchool;
import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

/** Programmer's School - Problem 667 */
public class Buses2 {
	void solve(Scanner in, PrintWriter out){
		int c = in.nextInt();
		int a = in.nextInt();
		int b = in.nextInt();
		
		System.out.println(buses(c, a, b));
	}
	
	public static int buses(int c, int a, int b){
		if ((b <= 2) || ((c + b - 3)/(b - 2) * 2 > a)){
			return 0;
		}
		return (c + a + b - 1) / b;
	}
	
	public static void main(String[] args) {
		new Buses2().run();
	}
	
	void run(){
		Locale.setDefault(Locale.US);
		try( Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)){
			solve(in, out);
		}
	}
}
