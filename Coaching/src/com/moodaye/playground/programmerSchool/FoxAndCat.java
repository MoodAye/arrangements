package com.moodaye.playground.programmerSchool;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

/** Programmer School Problem 296 */
public class FoxAndCat {
	void solve(Scanner in, PrintWriter out) {
		out.println(minCoins2(in.nextInt()));
	}

	public static String minCoins3(int price){
		int min5coins = price / 5;
		while((price - min5coins * 5) % 3 != 0){
			min5coins--;
		}
		return min5coins + " " + (price - min5coins * 5) / 3;
	}
	
	public static String minCoins2(int price){
		int min5coins = price / 5;
		int	min3coins = (price - min5coins * 5) / 3;
		
		while(min5coins * 5 + min3coins*3 != price){
			min5coins--;
			min3coins = (price - min5coins * 5) / 3;
		}
		
		return Integer.toString(min5coins) + " " + Integer.toString(min3coins);
	}

	public static void main(String[] args) {
		new FoxAndCat().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
