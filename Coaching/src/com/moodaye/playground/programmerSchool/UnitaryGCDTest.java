package com.moodaye.playground.programmerSchool;

import static org.junit.Assert.*;
import static com.moodaye.playground.programmerSchool.UnitaryGCD.*;

import org.junit.Test;

public class UnitaryGCDTest {

	@Test
	public void testBigIntGCD() {
		assertEquals("17", UnitaryGCD.bigIntGCD("51", "34"));
		assertEquals("2", UnitaryGCD.bigIntGCD("52", "34"));
		assertEquals("1", UnitaryGCD.bigIntGCD("1", "1"));
		assertEquals("9", UnitaryGCD.bigIntGCD("36", "27"));
		assertEquals("18", UnitaryGCD.bigIntGCD("18", "36"));
	}
	
	@Test
	public void testGcdOnes(){
		assertEquals("1", getStringOfOnes(gcd(1, 1)));
		assertEquals("1", getStringOfOnes(gcd(1, 2)));
		assertEquals("11111", getStringOfOnes(gcd(10, 5)));
		assertEquals("111111111", getStringOfOnes(gcd(36, 27)));
		assertEquals("11111111", getStringOfOnes(gcd(1840, 8)));
		assertEquals("11", getStringOfOnes(gcd(2, 302)));
		assertEquals("11111", getStringOfOnes(gcd(1845, 10)));
		assertEquals("11111111", getStringOfOnes(gcd(1504, 8)));
		assertEquals("1111111", getStringOfOnes(gcd(875, 581)));
		assertEquals("111111", getStringOfOnes(gcd(228,1494)));
		assertEquals("11111111111111111111111", getStringOfOnes(gcd(575,736)));
	}
}
