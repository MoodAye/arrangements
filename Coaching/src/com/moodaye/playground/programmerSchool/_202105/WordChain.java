package com.moodaye.playground.programmerSchool._202105;

import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Locale;
import java.util.Scanner;

// Problem 
public class WordChain {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int[] words = new int[n];
		for (int i = 0; i < n; i++) {
			words[i] = in.nextInt();
		}
		
//		Arrays.sort(words);
		
		int[] cnts = new int[n];
		Arrays.fill(cnts, 1);
	
		int max = 1;
		for(int i = 1; i < n; i++) {
			for(int j = i - 1; j >= 0; j--) {
				if(words[i] > words[j]) {
					cnts[i] = Math.max(cnts[j] + 1, cnts[i]);
				}
			}
			max = Math.max(cnts[i], max);
		}
		
		out.println(max);
//		out.println(Arrays.stream(cnts).max().getAsInt());
	}

	public static void main(String[] args) {
		new WordChain().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
