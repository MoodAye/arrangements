package com.moodaye.playground.programmerSchool._202105;

import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Locale;
import java.util.Scanner;

// Problem 368
public class Route {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int[][] nums = new int[n + 1][n + 1];
		for (int i = 1; i <= n; i++) {
			for (int j = 1; j <= n; j++) {
				nums[i][j] = in.nextInt();
			}
		}
		
		Arrays.fill(nums[0], Integer.MAX_VALUE);
		for(int i = 1; i <= n; i++) {
			nums[i][0] = Integer.MAX_VALUE;
		}
		
		for (int i = 1; i <= n; i++) {
			for (int j = 1; j <= n; j++) {
				nums[i][j] += Math.min(nums[i - 1][j], nums[i][j - 1]);
			}
		}
		
		int nextX = n; 
		int nextY = n;
		while(nextX != 1 && nextY != 1) {
			nums[nextX][nextY] = -1;
			if(nums[nextX - 1][nextY] < nums[nextX][nextY - 1]) {
				nextX -= 1;
			}
			else {
				nextY -= 1;
			}
		}
		
		nums[1][1] = -1;
		
		for (int i = 1; i <= n; i++) {
			for (int j = 1; j <= n; j++) {
				out.print(nums[i][j] == -1 ? "#" : ".");
			}
			out.println();
		}
	}

	public static void main(String[] args) {
		new Route().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
