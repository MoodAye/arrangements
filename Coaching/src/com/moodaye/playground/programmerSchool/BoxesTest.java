package com.moodaye.playground.programmerSchool;
import static org.junit.Assert.*;

import org.junit.Test;

public class BoxesTest {

	@Test
	public void test() {
		assertEquals("Boxes are equal", Boxes.checkBoxSizes(1, 2, 3, 3, 2, 1));
		assertEquals("The first box is larger than the second one", Boxes.checkBoxSizes(2, 2, 3, 3, 2, 1));
		assertEquals("The first box is smaller than the second one", Boxes.checkBoxSizes(2, 2, 3, 3, 2, 3));
		assertEquals("Boxes are incomparable", Boxes.checkBoxSizes(3, 4, 5, 2, 4, 6));
		assertEquals("Boxes are equal", Boxes.checkBoxSizes(2, 2, 2, 2, 2, 2));

	}

}
