package com.moodaye.playground.programmerSchool.February2020.review;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

// Problem 255
// ~5 min to solve
public class NumbersRepresentation {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		for(int i = 2; i * i <= n; i++) {
			if(n % i == 0) {
				out.printf("%d %d%n", n / i, n - n / i);
				return;
			}
		}
		out.printf("%d %d%n", 1, n - 1);
	}

	public static void main(String[] args) {
		new NumbersRepresentation().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
