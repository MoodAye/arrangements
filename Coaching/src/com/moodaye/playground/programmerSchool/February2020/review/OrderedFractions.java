package com.moodaye.playground.programmerSchool.February2020.review;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;

// Problem 422
public class OrderedFractions {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		
		Set<Fraction> list = new TreeSet<>();
		for(int denominator = 2; denominator <= n; denominator++) {
			for(int numerator = 1; numerator < denominator; numerator++) {
				list.add(new Fraction(numerator, denominator));
			}
		}
		
		for(Fraction f : list) {
			out.printf("%d/%d%n", f.numerator, f.denominator);
		}
	}

	public static void main(String[] args) {
		new OrderedFractions().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}

	static class Fraction implements Comparable<Fraction> {
		int numerator;
		int denominator;

		public Fraction(int numerator, int denominator) {
			this.numerator = numerator;
			this.denominator = denominator;
		}

		@Override
		public int compareTo(Fraction o) {
			return numerator * o.denominator - denominator * o.numerator;
		}

		public boolean equals(Object o) {
			return compareTo((Fraction) o) == 0;
		}
	}
}
