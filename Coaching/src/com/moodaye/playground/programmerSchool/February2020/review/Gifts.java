package com.moodaye.playground.programmerSchool.February2020.review;

import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Locale;
import java.util.Scanner;

// Problem 472  
/* tests
 *		ok1. candiesToAdd will get over before reaching largest gift  (2 3 1 2) (2 5 1 2)
 *		ok2. candiesToAdd will not get over before reaching largest gift (2 4 1 2)
 *		3. candiesToAdd is make all gifts equal
 *		4. candiesToAdd will make the first few gifts equal
 *		5. candiesToAdd = 0
 *		6. candiesToAdd = 1;
 *		7. add initial gifts are equal
 *		8. number of gifts = 1; candiesToAdd > 1
 *		ok9. number of gifts = 1; candiesToAdd = 0
 *		ok10. number of gifts = 1; candiesToAdd = 1
 *		11. number of gifts > 1; candiesToAdd > 1
 *		12. number of gifts > 1; candiesToAdd = 0
 *		13. number of gifts > 1; candiesToAdd = 1
 */
public class Gifts {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int candiesToAdd = in.nextInt();
		int[] gifts = new int[n];
		
		for(int i = 0; i < n; i++) {
			gifts[i] = in.nextInt();
		}
		
		Arrays.sort(gifts);
		int smallestGift = gifts[0];
		for(int i = 1; i < n; i++) {
			int diff = gifts[i] - gifts[i - 1];
			int candiesNeeded = diff * i;
			if(candiesNeeded == candiesToAdd) {
				smallestGift += diff;
				candiesToAdd = 0;
				break;
			}
			if(candiesNeeded > candiesToAdd) {
				smallestGift += candiesToAdd / i;
				candiesToAdd = 0;
				break;
			}
			smallestGift += diff;
			candiesToAdd -= candiesNeeded;
		}
		
		if(candiesToAdd != 0) {
				smallestGift += candiesToAdd / n;
		}
		out.println(smallestGift);
	}

	public static void main(String[] args) {
		new Gifts().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
