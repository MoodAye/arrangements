package com.moodaye.playground.programmerSchool.February2020.review;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

// Problem 669
// 1. Factorize - prime numbers
// 2. If prime is gt that 7 - no solution (can't use 2 digit prime)
// 2. Primes should give smallest number? E.G, if we have 3,3,7 - then answer
//    should be 97.  largest would be 733. 
//    OK - so largest is simply writing out primes
//    smallest is combining the digits using the following rules
//    if prime is 5,7 -> skip (no possible way to make a single digit multiple.
//    that leaves 2 and 3.  Given any number of 2s and any number of 3s the following rules 
//         1.  Make 8 from 2x2x2.  
//         2.  Make 9 from 3,3
//         3.  Make 6 from 2,3
//         4.  Make 4 from 2,2
//        

public class ProductOfDigits3 {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();

		int cnt2 = 0;
		int cnt3 = 0;
		int cnt5 = 0;
		int cnt7 = 0;
		
		StringBuilder largest = new StringBuilder();
		
		while (n % 7 == 0) {
			cnt7++;
			n /= 7;
			largest.append(7);
		}
		while (n % 5 == 0) {
			cnt7++;
			n /= 5;
			largest.append(5);
		}
		while (n % 3 == 0) {
			cnt3++;
			n /= 3;
			largest.append(3);
		}
		while (n % 2 == 0) {
			cnt2++;
			n /= 2;
			largest.append(2);
		}

		if (n != 1) {
			out.println("-1 - 1");
			return;
		}
		
		StringBuilder smallest = new StringBuilder();
		int cnt8 = 0;
		while(cnt2 >= 3) {
			cnt8++;
			cnt2 -= 3;
		}
		int cnt9 = 0;
		while(cnt3 >= 2) {
			cnt9++;
			cnt3 -= 2;
		}
		int cnt6 = 0;
		while(cnt3 >= 1 && cnt2 >= 1) {
			cnt6++;
			cnt3--; cnt2--;
		}
		int cnt4 = 0;
		while(cnt2 >= 2) {
			cnt4++;
			cnt2 -= 2;
		}
		
		if(cnt2 == 1) smallest.append(2);
		if(cnt3 == 1) smallest.append(3);
		if(cnt4 == 1) smallest.append(4);
		while(cnt5-- > 0) smallest.append(5);
		while(cnt6-- > 0) smallest.append(6);
		while(cnt7-- > 0) smallest.append(7);
		while(cnt8-- > 0) smallest.append(8);
		while(cnt9-- > 0) smallest.append(9);
	
		out.println(smallest + " " + largest);
	}

	public static void main(String[] args) {
		new ProductOfDigits3().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
