package com.moodaye.playground.programmerSchool.February2020.review;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

// Problem 447
// start 9:53am
public class LastDigitOfN {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		
		int lastTwoDigits = 1;
		for(int i = 1; i <= n; i++) {
			int m = i;
			while(m % 10 == 0) {
				m /= 10;
			}
			lastTwoDigits *= (m % 100);
			while(lastTwoDigits % 10 == 0) {
				lastTwoDigits /= 10;
			}
			lastTwoDigits %= 100;
		}
		out.println(lastTwoDigits % 10);
	}

	public static void main(String[] args) {
		new LastDigitOfN().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
