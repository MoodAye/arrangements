package com.moodaye.playground.programmerSchool.February2020.review;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

// Problem 42
public class Dragons {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		long[] memo = new long[n + 1];
		memo[1] = 1;
//		memo[2]	 = 2;
//		memo[3]	 = 3;
		
		for(int i = 2; i <= n; i++) {
			long max = i;
			for(int j = i - 1; j > 0; j--) {
				max = Math.max(max, memo[j] * memo[i - j]);
			}
			memo[i] = max;
		}
		
		out.println(memo[n]);
	}

	public static void main(String[] args) {
		new Dragons().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
