package com.moodaye.playground.programmerSchool.December2018;

import java.util.Scanner;
import java.util.Arrays;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #377 */
// Total time ~32 min
public class PaintingTheNumberLine {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		Segment[] segs = new Segment[n];
		for(int i = 0; i < n; i++){
			segs[i] = new Segment();
			segs[i].left = in.nextInt();
			segs[i].right = in.nextInt();
		}
		
		Arrays.sort(segs);
		
		Segment prev = new Segment();
		prev.left = Integer.MIN_VALUE;
		prev.right = Integer.MIN_VALUE;
		
		long length = 0L;
		for(Segment seg : segs){
			if(seg.left <= prev.right){
				length += Math.max(0, (seg.right - prev.right));
			}
			else{
				length += seg.right - seg.left;
			}
			prev.left = seg.left;
			if(prev.right < seg.right){
				prev.right = seg.right;
			}
		}
		
		out.println(length);
	}

	public static void main(String args[]) {
		new PaintingTheNumberLine().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}

}

class Segment implements Comparable<Segment>{
	int left;
	int right;
	@Override
	
	public int compareTo(Segment seg) {
		return left - seg.left;
	}
}
