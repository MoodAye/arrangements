package com.moodaye.playground.programmerSchool.December2018;

import static org.junit.Assert.*;
import static com.moodaye.playground.programmerSchool.December2018.SumOfTwoNumbers2.*;

import org.junit.AfterClass;
import org.junit.Test;

public class SumOfTwoNumbersTest {

	@Test
	public void test() {
		int[] soln = soln(12, 31, 25);
		assertEquals(12, soln[0]);
		assertEquals(13, soln[1]);
		
		soln = soln(12, 31, 26);
		assertEquals(Integer.MAX_VALUE, soln[0]);
		assertEquals(0, soln[1]);
		
		soln = soln(11, 2, 13);
		assertEquals(11, soln[0]);
		assertEquals(2, soln[1]);
		
		soln = soln(2, 11, 13);
		assertEquals(2, soln[0]);
		assertEquals(11, soln[1]);
		
		soln = soln(2222, 121, 2433);
		assertEquals(2222, soln[0]);
		assertEquals(211, soln[1]);
		
		soln = soln(121, 2222, 2433);
		assertEquals(211, soln[0]);
		assertEquals(2222, soln[1]);
		
		soln = soln(101, 2, 13);
		assertEquals(11, soln[0]);
		assertEquals(2, soln[1]);
		
		soln = soln(230000, 2, 25);
		assertEquals(23, soln[0]);
		assertEquals(2, soln[1]);
		
		soln = soln(2, 23000, 25);
		assertEquals(2, soln[0]);
		assertEquals(23, soln[1]);
	}

}
