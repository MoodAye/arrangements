package com.moodaye.playground.programmerSchool.December2018;

import java.util.Scanner;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #397 */
public class StringQuality {
	void solve(Scanner in, PrintWriter out) {
		String s = in.next();
		out.println(getStringQuality(s));
	}
	
	public static String getStringQuality(String s){
		char[] cs = s.toCharArray();
		char[] csTemp = s.toCharArray();
		Arrays.sort(csTemp);
		char minc = csTemp[0];
		char maxc = csTemp[cs.length - 1];
		
		// when "zzzzz"
		if (minc == maxc) {
			return String.valueOf(cs[0]);
		}
	
		int lastMinC = -1;
		int lastMaxC = -1;
		int ansMinCIdx = -100_000_000;
		int ansMaxCIdx = -1;
		
		for(int i = 0; i < s.length(); i++){
			if(s.charAt(i) == minc){
				lastMinC = i;
			}
			else if(s.charAt(i) == maxc){
				lastMaxC = i;
			}
			if(lastMinC == -1 || lastMaxC == -1){
				continue;
			}
			if(Math.abs(lastMaxC - lastMinC) < Math.abs(ansMinCIdx - ansMaxCIdx)){
				ansMinCIdx = lastMinC;
				ansMaxCIdx = lastMaxC;
			}
		}
		
		if(ansMinCIdx < ansMaxCIdx){
			return s.substring(ansMinCIdx, ansMaxCIdx + 1);
		}
			return s.substring(ansMaxCIdx, ansMinCIdx + 1);
	}

	public static void main(String args[]) {
		new StringQuality().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
