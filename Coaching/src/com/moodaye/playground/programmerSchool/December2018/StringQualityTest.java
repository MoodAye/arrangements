package com.moodaye.playground.programmerSchool.December2018;

import static org.junit.Assert.*;
import static com.moodaye.playground.programmerSchool.December2018.StringQuality.*;

import org.junit.AfterClass;
import org.junit.Test;

public class StringQualityTest {

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Test
	public void test() {
		assertEquals("a", getStringQuality("a"));
		assertEquals("a", getStringQuality("aa"));
		assertEquals("a", getStringQuality("aaa"));
		assertEquals("a", getStringQuality("aaaa"));
		assertEquals("abc", getStringQuality("abc"));
		assertEquals("za", getStringQuality("za"));
		assertEquals("az", getStringQuality("azb"));
		assertEquals("za", getStringQuality("abzaz"));
		assertEquals("az", getStringQuality("azazazaz"));
		assertEquals("cba", getStringQuality("cbabbc"));
		assertEquals("za", getStringQuality("zza"));
		assertEquals("zxxxxxxxxxa", getStringQuality("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaannnnnnnnnnnnnzzzzzzzzzzzzzzzzzzzzzzzzxxxxxxxxxaaaaaaaa"));
		assertEquals("zbbbbbba", getStringQuality("bczbbbbbba"));
		assertEquals("ab", getStringQuality("aaabbb"));
		assertEquals("ab", getStringQuality("aaabbb"));
		assertEquals("adbecf", getStringQuality("adbecf"));
		assertEquals("klm", getStringQuality("klmlllllllm"));
		assertEquals("kllllllllm", getStringQuality("kllllllllm"));
		assertEquals("klllm", getStringQuality("klllllklllm"));
		assertEquals("mlllk", getStringQuality("mlllllmlllk"));
		assertEquals("cba", getStringQuality("cbabbc"));
		assertEquals("cba", getStringQuality("cbabbc"));
		assertEquals("cba", getStringQuality("cbabbcc")); // rank is coming back as 2 for a and c at 0.. Should be 1
		assertEquals("cba", getStringQuality("cbabbcbbabbc"));
		assertEquals("ca", getStringQuality("bcbcabbc"));
		assertEquals("ca", getStringQuality("cbcbcabbc"));
		assertEquals("ac", getStringQuality("cbaca"));
		assertEquals("ca", getStringQuality("cbabca"));
		assertEquals("ca", getStringQuality("cbabcabbc"));
		assertEquals("ca", getStringQuality("cbabbcbcabbc"));

	}

}
