package com.moodaye.playground.programmerSchool.December2018;

import static org.junit.Assert.*;
import static com.moodaye.playground.programmerSchool.December2018.LuckyPage.*;

import org.junit.AfterClass;
import org.junit.Test;

public class LuckyPageTest {

	@Test
	public void test() {
		assertEquals(true, isLucky(100, 42));
		assertEquals(false, isLucky(100, 128));
		assertEquals(true, isLucky(1, 1));
		assertEquals(false, isLucky(1, 2));
		assertEquals(true, isLucky(2, 2));
		assertEquals(true, isLucky(3, 2));
		assertEquals(true, isLucky(8, 8));
		assertEquals(true, isLucky(8, 7));
		assertEquals(false, isLucky(2, 8));
		assertEquals(false, isLucky(2, 6*8));
		assertEquals(true, isLucky(68, 6*8));
		assertEquals(true, isLucky(289, 2*9*8));
		assertEquals(true, isLucky(689, 6*9*8));
		assertEquals(true, isLucky(3689, 3*6*9*8));
		assertEquals(true, isLucky(2899, 2*9*9*8));
		assertEquals(false, isLucky(6, 7));
		assertEquals(false, isLucky(56, 35));
		assertEquals(true, isLucky(57, 35));
		assertEquals(true, isLucky(289, 2*8*9));
		assertEquals(false, isLucky(11, 11));
		assertEquals(false, isLucky(4000, 5*7*9*8));
		assertEquals(true, isLucky(5789, 5*7*8*9));
		assertEquals(true, isLucky(10_000, 5*7*9*8));
		assertEquals(false, isLucky(100_000, 5*7*9*11));
		assertEquals(false, isLucky(100_000, 2*23));
		assertEquals(true, isLucky(29, 2*2*3));  
		assertEquals(true, isLucky(29, 2*2*3));  
		assertEquals(true, isLucky(269, 2*2*3*3*3));  
		assertEquals(true, isLucky(689, 2*2*2*2*3*3*3));  
		assertEquals(true, isLucky(499, 2*2*3*3*3*3));  
		assertEquals(false, isLucky(1, 5859375));
	}

}
