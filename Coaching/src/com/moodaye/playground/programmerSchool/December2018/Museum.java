package com.moodaye.playground.programmerSchool.December2018;

import java.util.Arrays;
import java.util.Locale;
import java.io.InputStream;
import java.io.PrintWriter;

/** Programmer's School #76 */
// ~40min
public class Museum {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int[] inTimes = new int[n];
		int[] outTimes = new int[n];
		for(int i = 0; i < n; i++){
			int inHour = in.nextInt();
			int inMin = in.nextInt();
			int outHour = in.nextInt();
			int outMin = in.nextInt();
			inTimes[i] = inHour * 60 + inMin;
			outTimes[i] = outHour * 60 + outMin;
		}
		
		Arrays.sort(inTimes);
		Arrays.sort(outTimes);
		
		int maxVisitors = 0;
		int visitors = 0;
		int inIdx = 0;
		int outIdx = 0;
		
		for(int i = 0; i < 24 * 60; i++){
			while(inIdx < n && inTimes[inIdx] == i){
				visitors++;
				inIdx++;
			}
			maxVisitors = Math.max(maxVisitors, visitors);
			while(outIdx < n && outTimes[outIdx] == i){
				visitors--;
				outIdx++;
			}
		}
		out.println(maxVisitors);
	}

	public static void main(String args[]) {
		new Museum().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
	static class Scanner implements AutoCloseable {
		InputStream is;
		int pos = 0;
		int size = 0;
		byte[] buffer = new byte[1024];

		Scanner(InputStream is) {
			this.is = is;
		}

		int nextChar() {
			if (pos >= size) {
				try {
					size = is.read(buffer);
				} catch (java.io.IOException e) {
					throw new java.io.IOError(e);
				}
				pos = 0;
				if (size == -1) {
					return -1;
				}
			}
			Assert.check(pos < size);
			int c = buffer[pos] & 0xFF;
			pos++;
			return c;
		}

		int nextInt() {
			int c = nextChar();
			while (c == ' ' || c == '\r' || c == '\n' || c == '\t' || c == ':') {
				c = nextChar();
			}
			if (c == '-') {
				c = nextChar();
				Assert.check('0' <= c && c <= '9');
				int n = -(c - '0');
				c = nextChar();
				while ('0' <= c && c <= '9') {
					int d = c - '0';
					c = nextChar();
					Assert.check(n > Integer.MIN_VALUE / 10
							|| n == Integer.MIN_VALUE / 10 && d <= -(Integer.MIN_VALUE % 10));
					n = n * 10 - d;
				}
				return n;
			} else {
				Assert.check('0' <= c && c <= '9');
				int n = c - '0';
				c = nextChar();
				while ('0' <= c && c <= '9') {
					int d = c - '0';
					c = nextChar();
					Assert.check(
							n < Integer.MAX_VALUE / 10 || n == Integer.MAX_VALUE / 10 && d <= Integer.MAX_VALUE % 10);
					n = n * 10 + d;
				}
				return n;
			}
		}

		@Override
		public void close() {
		}
	}

	static class Assert {
		static void check(boolean e) {
			if (!e) {
				throw new AssertionError();
			}
		}
	}
}
