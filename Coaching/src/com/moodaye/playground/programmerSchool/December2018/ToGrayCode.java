package com.moodaye.playground.programmerSchool.December2018;


import java.util.Locale;
import java.util.Set;
import java.util.TreeSet;
import java.io.InputStream;
import java.io.PrintWriter;

/** Programmer's School #389 */
// Memory estimate 4 bytes / int x 64K ints ~ 264K memory
// Operations: 65536 x 1e5 x 16 x 2 = 2e11 ... won't work in 1 sec.
// One optimization is to avoid scanning the entire array after each swap.
public class ToGrayCode {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		n = 1 << n;
		int[] a = new int[n];
		for(int i = 0; i < n; i++){
			a[i] = in.nextInt();
		}
		
		Set<Integer> notGray = notGray(a);
		
		int nSwaps = in.nextInt();
		for(int i = 0; i < nSwaps; i++){
			int next1 = in.nextInt();
			int next2 = in.nextInt();
			
			swap(a, next1, next2);
			
			if(isGray(a, next1)) notGray.remove(next1); else notGray.add(next1);
			
			int nLeft = next1 - 1;
			int nRight = next1 + 1;
			nLeft = (nLeft == -1 ? a.length - 1 : nLeft);
			nRight = (nRight == a.length ? 0 : nRight);
		
			if(isGray(a, nLeft)) notGray.remove(nLeft); else notGray.add(nLeft);
			if(isGray(a, nRight)) notGray.remove(nRight); else notGray.add(nRight);
			
			if(isGray(a, next2)) notGray.remove(next2); else notGray.add(next2);
			
			nLeft = next2 - 1;
			nRight = next2 + 1;
			nLeft = (nLeft == -1 ? a.length - 1 : nLeft);
			nRight = (nRight == a.length ? 0 : nRight);
		
			if(isGray(a, nLeft)) notGray.remove(nLeft); else notGray.add(nLeft);
			if(isGray(a, nRight)) notGray.remove(nRight); else notGray.add(nRight);
			
			out.println(notGray.size() == 0 ? "Yes" : "No");
		}
	}
	
	private static Set<Integer> notGray(int[] a){
		Set<Integer> notGray = new TreeSet<>();
		for(int i = 0; i < a.length; i++){
			if(!isGray(a, i)){
				notGray.add(i);
			}
		}
		return notGray;
	}
	
	private static void swap(int[] a, int i, int j){
		int temp = a[i];
		a[i] = a[j];
		a[j] = temp;
	}

	public static boolean isGray(int[] a, int i) {
		int xorLeft = 0;
		int xorRight = 0;

		if (i == 0) {
			xorLeft = a[a.length - 1] ^ a[i];
		} else {
			xorLeft = a[i - 1] ^ a[i];
		}
		
		if (i == a.length - 1) {
			xorRight = a[i] ^ a[0];
		} else {
			xorRight = a[i] ^ a[i + 1];
		}

		return pow2(xorLeft) && pow2(xorRight);
	}

	private static boolean pow2(int n) {
		return n > 0 && ((n & (n - 1)) == 0);
	}

	public static void main(String args[]) {
		new ToGrayCode().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}

class Scanner implements AutoCloseable {
	InputStream is;
	int pos = 0;
	int size = 0;
	byte[] buffer = new byte[1024];

	Scanner(InputStream is) {
		this.is = is;
	}

	int nextChar() {
		if (pos >= size) {
			try {
				size = is.read(buffer);
			} catch (java.io.IOException e) {
				throw new java.io.IOError(e);
			}
			pos = 0;
			if (size == -1) {
				return -1;
			}
		}
		Assert.check(pos < size);
		int c = buffer[pos] & 0xFF;
		pos++;
		return c;
	}

	int nextInt() {
		int c = nextChar();
		while (c == ' ' || c == '\r' || c == '\n' || c == '\t') {
			c = nextChar();
		}
		if (c == '-') {
			c = nextChar();
			Assert.check('0' <= c && c <= '9');
			int n = -(c - '0');
			c = nextChar();
			while ('0' <= c && c <= '9') {
				int d = c - '0';
				c = nextChar();
				Assert.check(n > Integer.MIN_VALUE / 10
						|| n == Integer.MIN_VALUE / 10 && d <= -(Integer.MIN_VALUE % 10));
				n = n * 10 - d;
			}
			return n;
		} else {
			Assert.check('0' <= c && c <= '9');
			int n = c - '0';
			c = nextChar();
			while ('0' <= c && c <= '9') {
				int d = c - '0';
				c = nextChar();
				Assert.check(
						n < Integer.MAX_VALUE / 10 || n == Integer.MAX_VALUE / 10 && d <= Integer.MAX_VALUE % 10);
				n = n * 10 + d;
			}
			return n;
		}
	}

	@Override
	public void close() {
	}
}

class Assert {
	static void check(boolean e) {
		if (!e) {
			throw new AssertionError();
		}
	}
}