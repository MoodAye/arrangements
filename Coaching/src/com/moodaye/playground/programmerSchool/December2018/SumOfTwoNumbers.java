package com.moodaye.playground.programmerSchool.December2018;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #346 */
public class SumOfTwoNumbers {
	void solve(Scanner in, PrintWriter out) {
		int a = in.nextInt();
		int b = in.nextInt();
		int c = in.nextInt();
	}

	public static int[] soln(int a, int b, int c) {
		int[] arrayA = toArray(a);
		int[] arrayB = toArray(b);
		int[] arrayC = toArray(c);

		return soln(arrayA, arrayB, arrayC, new boolean[arrayA.length], new boolean[arrayB.length],
				new int[arrayA.length], new int[arrayB.length], arrayC.length - 1, 0,
				Integer.MAX_VALUE, Integer.MAX_VALUE);
	}

	private static int[] toArray(int a) {
		int len = 0;
		int temp = a;
		while (temp > 0) {
			temp /= 10;
			len++;
		}
		int[] arr = new int[len];
		int i = len - 1;
		while (a > 0) {
			arr[i--] = a % 10;
			a /= 10;
		}
		return arr;
	}

	private static int[] soln(int[] a, int[] b, int[] c, boolean[] ba, boolean[] bb, int[] sa, int[] sb, int idx,
			int carry, int minSolnA, int minSolnB) {
		if (idx == -1) {
			int solnA = toInt(sa);
			if (solnA < minSolnA) {
				minSolnA = solnA;
				minSolnB = toInt(sb);
			}
			int[] soln = new int[2];
			soln[0] = minSolnA;
			soln[1] = minSolnB;
			return soln;
		}

		for (int i = 0; i < a.length; i++) {
			if (ba[i] == true) {
				continue;
			}
			for (int j = 0; j < b.length; j++) {
				if (bb[j] == true) {
					continue;
				}
				if ((a[i] + b[j] + carry) % 10 == c[idx]) {
					ba[i] = true;
					bb[j] = true;
					sa[idx] = a[i];
					sb[idx] = b[j];
					carry = (a[i] + b[j] + carry) / 10;
					return soln(a, b, c, ba, bb, sa, sb, idx - 1, carry, minSolnA, minSolnB);
				}
			}
		}
		return null;
	}

	private static int toInt(int[] a) {
		int na = 0;
		int place = 10;
		for (int i = 0; i < a.length; i++) {
			na = na * place + a[i];
		}
		return na;
	}

	public static void main(String args[]) {
		new SumOfTwoNumbers().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
