package com.moodaye.playground.programmerSchool.December2018;

import java.util.Scanner;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #180 */
public class LuckyPage {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int p = in.nextInt();
		if (isLucky(n, p)) {
			out.println("YES");
		} else {
			out.println("NO");
		}
	}

	public static boolean isLucky(int n, int p) {
		int[] factors = factorize(p);
		if (factors == null) {
			return false;
		}
		long minFromP = combineFactors(factors);

		if (minFromP <= n) {
			return true;
		} else {
			return false;
		}
	}

	private static long combineFactors(int[] factors) {
		long n = 1L * factors[0];
		int place = 10;
		for (int i = 1; i <= 9; i++) {
			while (factors[i] > 0) {
				n = n * place + i;
				factors[i]--;
			}
		}
		return n;
	}

	private static int[] factorize(int n) {
		int[] factors = new int[10];
		if (n == 1) {
			factors[1]++;
			return factors;
		}

		for (int i = 9; i > 1; i--) {
			while (n % i == 0) {
				factors[i]++;
				n /= i;
			}
		}
		if (n != 1) {
			return null;
		}
		return factors;
	}

	public static void main(String args[]) {
		new LuckyPage().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
