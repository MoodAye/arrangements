package com.moodaye.playground.programmerSchool.December2018;

import java.util.Scanner;
import java.util.ArrayList;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #372 */
public class Brackets2 {
	private List<String> brs = new ArrayList<>();
	
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		brackets(n, "", 0, 0);
		for(String s : brs)
			out.println(s);
	}
	
	public void brackets(int n, String s, int cntCr, int cntSq){
		if(s.length() == n){
			if(cntCr == 0 && cntSq == 0){
				if(good(s)) brs.add(s);
			}
			return;
		}
		else if(cntCr < 0 || cntSq < 0 || cntCr + cntSq > n - s.length()){
			return;
		}
		
		brackets(n, s + "(", cntCr + 1, cntSq);
		brackets(n, s + ")", cntCr - 1, cntSq);
		brackets(n, s + "[", cntCr, cntSq + 1);
		brackets(n, s + "]", cntCr, cntSq - 1);
	}
	
	private static boolean good(String s){
		char[] cs = s.toCharArray();
		Deque<Character> stack = new LinkedList<>();
		for(char c : cs){
			if(c == '(') stack.push(c);
			else if(c == '[') stack.push(c);
			else{
				char cpop = stack.pop();
				if((c == ')' && cpop != '(') || (c == ']' && cpop != '[')){
					return false;
				}
			}
		}
		if(stack.size() != 0){
			return false;
		}
		return true;
	}

	public static void main(String args[]) {
		new Brackets2().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}