package com.moodaye.playground.programmerSchool.December2018;

import static org.junit.Assert.*;

import java.util.Collections;
import java.util.Set;

import org.junit.AfterClass;
import org.junit.Test;
import static com.moodaye.playground.programmerSchool.December2018.SumOfTwoNumbers3.*;
public class SumOfTwoNumbers3Test {

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}
	
	@Test
	public void testMyEquals2(){
		int a1 = 123;
		int[] b1 = {1,2,3};
		assertTrue(myEquals(a1, b1));
		
		int a2 = 123;
		int[] b2 = {0,0,1,2,3};
		assertTrue(myEquals(a2, b2));
		
		int a3 = 1023;
		int[] b3 = {1,0,2,3};
		assertTrue(myEquals(a3, b3));
		
		int a4 = 3;
		int[] b4 = {3};
		assertTrue(myEquals(a4, b4));
		
		int a5 = 12300;
		int[] b5 = {0,0,1,2,3};
		assertTrue(myEquals(a5, b5));
	}

	@Test
	public void testMyEquals() {
		int[] a1 = {1,2,3};
		int[] b1 = {1,2,3};
		assertTrue(myEquals(a1, b1));
		
		int[] a2 = {1,2,3};
		int[] b2 = {0,0,1,2,3};
		assertTrue(myEquals(a2, b2));
		
		int[] a3 = {1,0,2,3};
		int[] b3 = {1,0,2,3};
		assertTrue(myEquals(a3, b3));
		
		int[] a4 = {3};
		int[] b4 = {3};
		assertTrue(myEquals(a4, b4));
	}
	
	@Test 
	public void testArr(){
		int[] a = {1,2};
		assertArrayEquals(a, toArray(12));
		
		int[] b = {2};
		assertArrayEquals(b, toArray(2));
		
		int[] c = {1,0,0,0,0,0,0,0,0};
		assertArrayEquals(c, toArray(100_000_000));
		
		int[] d = {2,3,4,5,6,7,8,9};
		assertArrayEquals(d, toArray(23456789));
		
	}
	
	@Test
	public void testSubArr(){
		int[] a = {1,2};
		int[] ea = {2};
		assertArrayEquals(ea, subArray(a,0));
		ea[0] = 1;
		assertArrayEquals(ea, subArray(a,1));
		
		int[] b = {1,2,3,4,5,6};
		int[] eb = {1,2,4,5,6};
		assertArrayEquals(eb, subArray(b,2));
		eb[2] = 3; eb[3] = 4; eb[4] = 5;
		assertArrayEquals(eb, subArray(b,5));
		
		int[] c = {1,2,3,4,5,6};
		int[] ec = {2,3,4,5,6};
		assertArrayEquals(ec, subArray(c,0));
	}
	
	@Test
	public void testPowAdd(){
		assertEquals(9102, powAdd(9,102,3));
		assertEquals(91, powAdd(9,1,1));
	}
	
	@Test
	public void testPerms(){
		Set<Integer> set = perms(toArray(12));
		assertTrue(set.contains(12));
		assertTrue(set.contains(21));
		
		set = perms(toArray(123));
		assertTrue(set.contains(123));
		assertTrue(set.contains(132));
		assertTrue(set.contains(231));
		assertTrue(set.contains(213));
		assertTrue(set.contains(312));
		assertTrue(set.contains(321));
	}

}
