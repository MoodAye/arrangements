package com.moodaye.playground.programmerSchool.December2018;

import java.util.Scanner;
import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Deque;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #346 */
public class SumOfTwoNumbers2 {
	void solve(Scanner in, PrintWriter out) {
		int a = in.nextInt();
		int b = in.nextInt();
		int c = in.nextInt();
		int[] ans = soln(a, b, c);
		if(ans[0] == Integer.MAX_VALUE){
			out.printf("NO");
		}
		else{
			out.printf("YES%n%d %d %n", ans[0], ans[1]);
		}
	}

	/* return solution - 2 ints -- in the returned array */
	public static int[] soln(int a, int b, int c) {
		int[] aArr;
		int[] bArr;
		boolean switched = false;
		
		if(a > b){  // if a is longer ... make it the first one in the loop - makes logic a little easier
			aArr = toArrayReverse(a);
			bArr = toArrayReverse(b);
		}
		else{
			aArr = toArrayReverse(b);
			bArr = toArrayReverse(a);
			switched = true;
		}
		
		int[] cArr = toArrayReverse(c);
		int[] ans = new int[2];
		ans[0] = Integer.MAX_VALUE;
		
		StateHolder seed = new StateHolder();
		seed.aIdx = -1;
		seed.bIdx = -1;
		seed.sIdx = -1;
		seed.aUsed = new boolean[aArr.length];
		seed.bUsed = new boolean[bArr.length];
		
		Deque<StateHolder> queue = new ArrayDeque<>();
		queue.add(seed);

		while (!queue.isEmpty()) {
			StateHolder next = queue.remove();
			if(allUsed(next.aUsed, aArr)){
				updateAns(ans, next, aArr, bArr, switched);
				continue;
			}
			
			for (int i = 0; i < aArr.length; i++) {
				if(next.aUsed[i]) continue;
				boolean array2Done = true;
				for(int j = 0; next.bUsed != null && j < bArr.length; j++){
					if(next.bUsed[j]) continue;
					array2Done = false;
					if((aArr[i] + bArr[j] + next.carry) % 10 == cArr[next.sIdx + 1]){
						StateHolder pot = new StateHolder();
						pot.aUsed = Arrays.copyOf(next.aUsed, aArr.length);
						pot.bUsed = Arrays.copyOf(next.bUsed, bArr.length);
						pot.carry = (aArr[i] + bArr[j] + next.carry) / 10;
						pot.aIdx = i;
						pot.bIdx = j;
						pot.sIdx = next.sIdx + 1;
						pot.aUsed[i] = true;
						pot.bUsed[j] = true;
						pot.prev = next;
						queue.add(pot);
						break;
					}
				}
				if(array2Done){
					if((aArr[i] + next.carry) % 10 == cArr[next.sIdx + 1]){
						StateHolder pot = new StateHolder();
						pot.aUsed = Arrays.copyOf(next.aUsed, aArr.length);
						pot.carry = (aArr[i] + next.carry) / 10;
						pot.aIdx = i;
						pot.bIdx = -1;
						pot.sIdx = next.sIdx + 1;
						pot.aUsed[i] = true;
						pot.prev = next;
						queue.add(pot);
						break;
					}
				}
			} // for
		} // while
		return ans;
	}
	private static boolean allUsed(boolean[] used, int[] a){
		boolean usedUp = true;
		for(int i = 0; i < used.length; i++){
			if(!used[i] && a[i] != 0){
				usedUp = false;
				break;
			}
		}
		return usedUp;
	}
	
	private static void updateAns(int[] ans, StateHolder last, int[] aArr, int[] bArr, boolean switched){
		int a = aArr[last.aIdx];
		int b = last.bIdx == -1 ? 0 : bArr[last.bIdx];
		int pow = 10;
		while(last.prev.prev != null){
			last = last.prev;
			a = a * pow + aArr[last.aIdx];
			if(last.bIdx != -1){
				b = b * pow + bArr[last.bIdx];
			}
		}
		if(switched){
			int temp = a;
			a = b;
			b = temp;
		}
		if(ans[0] > a){
			ans[0] = a;
			ans[1] = b;
		}
	}

	// TODO - will this work if a == 0? (problem allows a=0; b=0
	private static int[] toArrayReverse(int a) {
		int len = 0;
		int temp = a;
		while (temp > 0) {
			temp /= 10;
			len++;
		}

		int[] arr = new int[len];
		int i = 0;
		while (a > 0) {
			arr[i++] = a % 10;
			a /= 10;
		}
		return arr;
	}

	public static void main(String args[]) {
		new SumOfTwoNumbers2().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}

class StateHolder {
	int aIdx;
	int bIdx;
	int carry;
	int sIdx;
	boolean[] aUsed;
	boolean[] bUsed;
	StateHolder prev;
}
