package com.moodaye.playground.programmerSchool.December2018;

import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #346 */
// Mem Limit Exceeded - Test 8
public class SumOfTwoNumbers3 {
	void solve(Scanner in, PrintWriter out) {
		int a = in.nextInt();
		int b = in.nextInt();
		int c = in.nextInt();

		Set<Integer> setA = perms(toArray(a));
		int[] bArr = toArray(b); 
		int ansA = Integer.MAX_VALUE;
		int ansB = 0;
		for (int as : setA) {
			int diff = c - as;
			if (diff > 0) {
				if (myEquals(diff, bArr) && as < ansA) {
					ansA = as;
					ansB = diff;
				}
			}
		}
		if (ansA != Integer.MAX_VALUE) {
			out.printf("YES%n%d %d", ansA, ansB);
		} else {
			out.println("NO");
		}
	}

	public static boolean myEquals(int a, int[] bArr) {
		boolean found = false;
		while (a > 0) {
			int rem = a % 10;
			a /= 10;
			found = false;
			for (int i = bArr.length - 1; i >= 0; i--) {
				if (bArr[i] == rem) {
					found = true;
					bArr[i] *= -1;
					if (bArr[i] == 0) {
						bArr[i] = Integer.MIN_VALUE;
					}
					break;
				}
			}
			if(!found){
				return false;
			}
		}
		for (int i = 0; i < bArr.length - 1; i++) {
			if (bArr[i] < 0) {
				bArr[i] = bArr[i] == Integer.MIN_VALUE ? 0 : bArr[i] * -1;
			}
		}
		return found;
	}

	/* assumes array (a) won't have leading zeroes */
	public static boolean myEquals(int[] a, int[] b) {
		int ib = 0;
		while (b[ib] == 0) {
			ib++;
		}

		boolean eq = true;
		int ia = 0;
		for (; ia < a.length && ib < b.length; ia++, ib++) {
			if (a[ia] != b[ib]) {
				eq = false;
				break;
			}
		}

		if (ia == a.length && ib == b.length && eq) {
			return true;
		}

		return false;
	}

	// permutations
	public static Set<Integer> perms(int[] a) {
		if (a.length == 1) {
			Set<Integer> s = new TreeSet<>();
			s.add(toInt(a));
			return s;
		}

		Set<Integer> ret = new TreeSet<>();
		for (int i = 0; i < a.length; i++) {
			Set<Integer> x = perms(subArray(a, i));
			for (int xi : x) {
				ret.add(powAdd(a[i], xi, a.length - 1));
			}
		}
		return ret;
	}

	/* prepends a to b */
	public static int powAdd(int a, int b, int len) {
		for (int i = 1; i <= len; i++) {
			a *= 10;
		}
		return a + b;
	}

	/* returns an array after removing the element at rem */
	public static int[] subArray(int[] a, int rem) {
		int[] ret = new int[a.length - 1];
		for (int i = 0; i < a.length; i++) {
			if (i == rem) {
				continue;
			}
			if (i < rem) {
				ret[i] = a[i];
			} else {
				ret[i - 1] = a[i];
			}
		}
		return ret;
	}

	/* convert int to array */
	public static int[] toArray(int a) {
		int[] temp = new int[10];
		int len = 0;
		while (a > 0) {
			temp[10 - (len++ + 1)] = a % 10;
			a /= 10;
		}
		return Arrays.copyOfRange(temp, 10 - len, 10);
	}

	public static int toInt(int[] a) {
		int ret = a[0];
		for (int i = 1; i < a.length; i++) {
			ret = ret * 10 + a[i];
		}
		return ret;
	}

	public static void main(String args[]) {
		new SumOfTwoNumbers3().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
