package com.moodaye.playground.programmerSchool.December2018;

import java.util.Scanner;
import java.util.Set;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Deque;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #171 */
public class NumberOfDivisors {
	void solve(Scanner in, PrintWriter out) {
		long n = in.nextLong();
		List<Long> pFactors = primeFactorsCounts(n);
		int cnt = 1;
		for(long f : pFactors){
			cnt *= (f + 1);
		}
		out.println(cnt);
	}
	
	public static List<Long> primeFactorsCounts(long n) {
		List<Long> factors = new ArrayList<>();
		boolean[] prime = new boolean[1_000];
		Arrays.fill(prime, true);
		prime[0] = false;
		prime[1] = false;
		long div = n;
		for (int i = 2; i < 1_000; i++) {
			if (!prime[i]) {
				continue;
			} else {
				for (int j = i * i; j < prime.length; j += i) {
					prime[j] = false;
				}
			}
			int cnt = 0;
			while(div % i == 0){
				cnt++;
				div /= i;
			}
			if(cnt != 0){
				factors.add(1L * cnt);
			}
		}
		if(div != 1){
			factors.add(div);
		}
		return factors;
	}

	public static void main(String args[]) {
		new NumberOfDivisors().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
