package com.moodaye.playground.programmerSchool.December2018;

import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #350 */
public class Permutations {
	void solve(Scanner in, PrintWriter out) {
		String s = in.next();
		Set<String> set = perms(s);
		for(String sx : set){
			out.println(sx);
		}
	}
	
	
	private static Set<String> perms(String s){
		if(s.length() == 1){
			Set<String> set1 = new TreeSet<>();
			set1.add(s);
			return set1;
		}
		
		Set<String> set = new TreeSet<>();
		for(int i = 0; i < s.length(); i++){
			Set<String> setx = perms(s.substring(0, i) + s.substring(i + 1, s.length()));
			for(String sx : setx){
				set.add(s.substring(i, i + 1) + sx);
			}
		}
		return set;
	}

	public static void main(String args[]) {
		new Permutations().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
