package com.moodaye.playground.programmerSchool.December2017;

import static org.junit.Assert.*;
import static com.moodaye.playground.programmerSchool.December2017.TwoeN.*;

import java.math.BigInteger;

import org.junit.Test;

public class TwoeNTest {
	
	@Test
	public void test4() {
		for(int n = 1; n < 1000; n++){
			assertEquals((new BigInteger(String.valueOf("2")))
					.pow(n).toString(), powerOfTwo_4(n));
		}
	}

	@Test
	public void test() {
		for(int n = 1; n < 1000; n++){
			assertEquals((new BigInteger(String.valueOf("2")))
					.pow(n).toString(), powerOfTwo_3(n));
		}
	}
	
	@Test
	public void testDoubleArray(){
		char[] c1 = {'1'};
		char[] exp = {'2'};
		doubleArray(c1, 0);
		assertArrayEquals(exp, c1);
		
		char[] c2 = {'1', '2'};
		char[] exp2 = {'2', '4'};
		doubleArray(c2, 0);
		assertArrayEquals(exp2, c2);
		
		char[] c3 = {' ', '9'};
		char[] exp3 = {'1', '8'};
		doubleArray(c3, 1);
		assertArrayEquals(exp3, c3);
		
		char[] c4 = {'4', '9'};
		char[] exp4 = {'9', '8'};
		doubleArray(c4, 0);
		assertArrayEquals(exp4, c4);
		
		char[] c5 = String.valueOf(1L << 62).toCharArray();
		char[] exp5 = new BigInteger(String.valueOf(1L << 62)).
				multiply(new BigInteger("2")).toString().toCharArray();
		doubleArray(c5, 0);
		assertArrayEquals(exp5, c5);
	}

}
