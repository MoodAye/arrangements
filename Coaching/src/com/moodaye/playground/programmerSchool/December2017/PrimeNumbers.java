package com.moodaye.playground.programmerSchool.December2017;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #348 */
public class PrimeNumbers {
	void solve(Scanner in, PrintWriter out) {
		int first = in.nextInt();
		int last = in.nextInt();
		boolean[] isPrime = getIsPrime(last);
		boolean absent = true;
		for (int i = first; i <= last; i++) {
			if (isPrime[i] == true) {
				out.println(i);
				absent = false;
			}
		}
		if(absent){
			out.println("Absent");
		}
	}

	/** uses sieve of Eratosthenes **/
	public static boolean[] getIsPrime(int last) {
		boolean[] isPrime = new boolean[last + 1];
		for (int i = 0; i < isPrime.length; i++){
			isPrime[i] = true;
		}
		isPrime[0] = false;
		isPrime[1] = false;
		for (int i = 2; i * i  <= last; i++) {
			if (isPrime[i] == true) {
				for (int j = i * i; j <= last; j += i) {
					isPrime[j] = false;
				}
			}
		}
		return isPrime;
	}

	public static void main(String args[]) {
		new PrimeNumbers().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
