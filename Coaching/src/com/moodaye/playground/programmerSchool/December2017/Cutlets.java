package com.moodaye.playground.programmerSchool.December2017;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #664 */
// max output when p = 1; s = n = 3 x 10^4
public class Cutlets {
	void solve(Scanner in, PrintWriter out) {
		int p = in.nextInt();
		int s = in.nextInt();
		int n = in.nextInt();
		
		int batches = 0;
		boolean newBatch = true;
		int i = 1;
		for(; i <= 2 * n; i++){
			if(newBatch){
				batches++;
				newBatch = false;
			}
			if(i == n + 1 && batches == 1){
				batches++;
				continue;
			}
			if(i % p == 0){
				newBatch = true;
			}
		}
		out.println(batches * s);
	}

	public static void main(String args[]) {
		new Cutlets().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}

}
