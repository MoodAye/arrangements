package com.moodaye.playground.programmerSchool.December2017;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #482 */
public class ShortSequence {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		StringBuilder number = new StringBuilder();
		for(int i = 1; i <= n && number.length() <= n; i++){
			for(int j = 1; j <= i; j++){
				number.append(j);
			}
		}
		out.println(number.charAt(n - 1));
	}

	public static void main(String args[]) {
		new ShortSequence().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
