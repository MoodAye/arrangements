package com.moodaye.playground.programmerSchool.December2017;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #277 */
/* ax + by + c */
public class SchoolAlgebra {
	void solve(Scanner in, PrintWriter out) {
		int a = in.nextInt();
		int b = in.nextInt();
		int c = in.nextInt();
		out.println(trinomial(a, b, c));
	}
	
	private static String addElement(String polynomial, int k, String var){
		String sk;
		if(k == 1){
			sk = plusSign(polynomial) + var;
		}
		else if(k == -1){
			sk = "-" + var;
		}
		else if (k > 1){
			sk = plusSign(polynomial) + k + var;
		}
		else if (k < -1){
			sk = k + var;
		}
		else{
			sk = "";
		}
		
		return polynomial + sk;
	}
	
	private static String plusSign(String p){
		return p.isEmpty() ? "" : "+";
	}

	public static String trinomial(int a, int b, int c){
		String trinomial = a == 0 ? "" : "" + a;
		
		trinomial = addElement(trinomial, b, "x");
		trinomial = addElement(trinomial, c, "y");
		if(trinomial.isEmpty()){
			trinomial = "0";
		}
		return trinomial;
	}
	
	public static void main(String args[]) {
		new SchoolAlgebra().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}

}
