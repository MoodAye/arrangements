package com.moodaye.playground.programmerSchool.December2017;

import java.io.InputStream;
import java.io.PrintWriter;
import java.util.Locale;

/** Programmer's School #388 */
public class SaddlePoint {
	void solve(Scanner in, PrintWriter out){
		int rows = in.nextInt();
		int cols = in.nextInt();
		int[][] matrix = new int[rows][cols];
		for (int r = 0; r < rows; r++) {
			for (int c = 0; c < cols; c++) {
				matrix[r][c] = in.nextInt();
			}
		}
		
		int[] rowMins = new int[rows];
		for(int r = 0; r < rows; r++){
			rowMins[r] = Integer.MAX_VALUE;
			for(int c = 0; c < cols; c++){
				rowMins[r] = Math.min(matrix[r][c], rowMins[r]);
			}
		}
		
		int[] colMaxs = new int[cols];
		for(int c = 0; c < cols; c++){
			colMaxs[c] = Integer.MIN_VALUE;
			for(int r = 0; r < rows; r++){
				colMaxs[c] = Math.max(matrix[r][c], colMaxs[c]);
			}
		}
		
		int saddlePoints = 0;
		for (int r = 0; r < rows; r++) {
			for (int c = 0; c < cols; c++) {
				if(matrix[r][c] == rowMins[r] && matrix[r][c] == colMaxs[c]){
					saddlePoints++;
				}
			}
		}
		
		out.println(saddlePoints);
	}
	
	public static void main(String[] args) {
		new SaddlePoint().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}

	static class Scanner implements AutoCloseable {

		InputStream is;
		byte buffer[] = new byte[1 << 16];
		int size = 0;
		int pos = 0;

		Scanner(InputStream is) {
			this.is = is;
		}

		int nextChar() {
			if (pos >= size) {
				try {
					size = is.read(buffer);
				} catch (java.io.IOException e) {
					throw new java.io.IOError(e);
				}
				pos = 0;
				if (size == -1) {
					return -1;
				}
			}
			Assert.check(pos < size);
			int c = buffer[pos] & 0xFF;
			pos++;
			return c;
		}

		int nextInt() {
			int c = nextChar();
			while (c == ' ' || c == '\r' || c == '\n' || c == '\t') {
				c = nextChar();
			}
			if (c == '-') {
				c = nextChar();
				Assert.check('0' <= c && c <= '9');
				int n = -(c - '0');
				c = nextChar();
				while ('0' <= c && c <= '9') {
					int d = c - '0';
					c = nextChar();
					Assert.check(n > Integer.MIN_VALUE / 10
							|| n == Integer.MIN_VALUE / 10 && d <= -(Integer.MIN_VALUE % 10));
					n = n * 10 - d;
				}
				return n;
			} else {
				Assert.check('0' <= c && c <= '9');
				int n = c - '0';
				c = nextChar();
				while ('0' <= c && c <= '9') {
					int d = c - '0';
					c = nextChar();
					Assert.check(
							n < Integer.MAX_VALUE / 10 || n == Integer.MAX_VALUE / 10 && d <= Integer.MAX_VALUE % 10);
					n = n * 10 + d;
				}
				return n;
			}
		}

		@Override
		public void close() {
		}
	}

	static class Assert {
		static void check(boolean e) {
			if (!e) {
				throw new AssertionError();
			}
		}
	}
}