package com.moodaye.playground.programmerSchool.December2017;


import java.util.Locale;
import java.io.InputStream;
import java.io.PrintWriter;

/** Programmer's School #313 */
// We'll use an indexed to the bus number where we will track the last time for that bus
// We'll store the max time of all buses in a separate variable.
// This should require one iteration through the array - O(n)
// Getting "Time limit exceeded" on test 10 - which is not expected since
// we have max 10^6 times ... giving us about 10^6 * 3 operations.= total.
// 
// Resolved time limit exceeded by using FastScanner class provided by FM.
public class EveryMinuteBuses {
	void solve(FastScanner in, PrintWriter out) {
		int n = in.nextInt();
		int[] lastMinute = new int[101];
		for(int i = 0; i < 101; i++){
			lastMinute[i] = Integer.MAX_VALUE;
		}
		int maxWaitTime = 0;
		for(int i = 1; i <= n; i++){
			int bus = in.nextInt();
			maxWaitTime = Math.max(i - lastMinute[bus], maxWaitTime);
			lastMinute[bus] = i;
		}
		out.println(maxWaitTime);
	}

	public static void main(String args[]) {
		new EveryMinuteBuses().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (FastScanner in = new FastScanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}

static class FastScanner implements AutoCloseable {

    InputStream is;
    byte buffer[] = new byte[1 << 16];
    int size = 0;
    int pos = 0;

    FastScanner(InputStream is) {
        this.is = is;
    }

    int nextChar() {
        if (pos >= size) {
            try {
                size = is.read(buffer);
            } catch (java.io.IOException e) {
                throw new java.io.IOError(e);
            }
            pos = 0;
            if (size == -1) {
                return -1;
            }
        }
        Assert.check(pos < size);
        int c = buffer[pos] & 0xFF;
        pos++;
        return c;
    }

    int nextInt() {
        int c = nextChar();
        while (c == ' ' || c == '\r' || c == '\n' || c == '\t') {
            c = nextChar();
        }
        if (c == '-') {
            c = nextChar();
            Assert.check('0' <= c && c <= '9');
            int n = -(c - '0');
            c = nextChar();
            while ('0' <= c && c <= '9') {
                int d = c - '0';
                c = nextChar();
                Assert.check(n > Integer.MIN_VALUE / 10 || n == Integer.MIN_VALUE / 10 && d <= -(Integer.MIN_VALUE % 10));
                n = n * 10 - d;
            }
            return n;
        } else {
            Assert.check('0' <= c && c <= '9');
            int n = c - '0';
            c = nextChar();
            while ('0' <= c && c <= '9') {
                int d = c - '0';
                c = nextChar();
                Assert.check(n < Integer.MAX_VALUE / 10 || n == Integer.MAX_VALUE / 10 && d <= Integer.MAX_VALUE % 10);
                n = n * 10 + d;
            }
            return n;
        }
    }

    @Override
    public void close() {
    }
}

static class Assert {
    static void check(boolean e) {
        if (!e) {
            throw new AssertionError();
        }
    }
}
}