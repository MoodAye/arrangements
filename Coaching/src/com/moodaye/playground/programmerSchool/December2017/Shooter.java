package com.moodaye.playground.programmerSchool.December2017;

import java.util.Scanner;
import java.io.PrintWriter;
import java.util.Locale;

/** Programmer's School #298 */
// To check if targets lie in straight line relative to origin
// we need to check whether y1/x1 = y2/x2 or ... to avoid division
// y1.x2 = y2.x1.
// To then confirm that the points lie in the same direction from the shooter (origin) - 
// we confirm that the points lie in the same quadrant.
// Since -10 <= x,y <= 10 .. we can use int data type.
// Since number of targets is 20 - the most evident approach with O(n^2) should work.
public class Shooter {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int[] x = new int[n];
		int[] y = new int[n];
		for(int i = 0; i < n; i++){
			x[i] = in.nextInt();
			y[i] = in.nextInt();
		}
		
		int shots = 1; 
		for(int i = 1; i < n; i++){
			boolean shotFired = true;
			for(int j = 0; j < i; j++){
				if( y[i] * x[j] == y[j] * x[i]){
					//Code below is hard to read - is there a more elegant way 
					//to check for points lying in the same line and direction from origin?
					if(((y[i] <= 0 && y[j] <= 0) || (y[i] > 0 && y[j] > 0))
					 &&((x[i] <= 0 && x[j] <= 0) || (x[i] > 0 && x[j] > 0))){
						shotFired = false;
						break;
					}
				}
			}
			shots += shotFired ? 1 : 0;
		}
		out.println(shots);
	}

	public static void main(String[] args) {
		new Shooter().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}

}
