package com.moodaye.playground.programmerSchool.December2017;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;
import java.math.BigDecimal;

/** Programmer's School #411 */
public class QuadraticEquation {
	void solve(Scanner in, PrintWriter out) {
		double a = in.nextDouble();
		double b = in.nextDouble();
		double c = in.nextDouble();
		if (a == 0 && b == 0 && c == 0) {
			out.println(-1);
		} else if (a == 0 && b == 0) {
			out.println(0);
		} else if (a == 0) {
			out.println(1);
			out.println(-c / b);
		} else if (b * b == 4 * a * c) {
			out.println(1);
			out.println(-b / 2 / a);
		} else if (b * b < 4 * a * c) {
			out.println(0);
		} else {
			out.println(2);
			double term2 = Math.sqrt(b * b - 4 * a * c);
			out.println((-b - term2) / 2 / a);
			out.println((-b + term2) / 2 / a);
		}
	}

	public static void main(String args[]) {
		new QuadraticEquation().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
