package com.moodaye.playground.programmerSchool.December2017;

import static org.junit.Assert.*;

import org.junit.Test;

import static com.moodaye.playground.programmerSchool.December2017.SchoolAlgebra.*;

public class SchoolAlgebraTest {

	@Test
	public void test() {
		assertEquals("1+2x+3y", trinomial(1,2,3));
		assertEquals("-1-2x-3y", trinomial(-1,-2,-3));
		assertEquals("-2x-3y", trinomial(0,-2,-3));
		assertEquals("1-3y", trinomial(1,0,-3));
		assertEquals("2x-y", trinomial(0,2,-1));
		assertEquals("-x+2y", trinomial(0,-1,2));
		assertEquals("1+2x", trinomial(1,2,0));
		assertEquals("1+x", trinomial(1,1,0));
		assertEquals("1+x+y", trinomial(1,1,1));
		assertEquals("3y", trinomial(0,0,3));
		assertEquals("3x+3y", trinomial(0,3,3));
		assertEquals("3y", trinomial(0,0,3));
		assertEquals("11+3y", trinomial(11,0,3));
		assertEquals("-30000+3y", trinomial(-30000,0,3));
		assertEquals("0", trinomial(0,0,0));
	}
}
