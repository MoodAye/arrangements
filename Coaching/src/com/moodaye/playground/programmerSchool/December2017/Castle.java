package com.moodaye.playground.programmerSchool.December2017;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #528 */
// Number of rooms is simply sum of rooms of all polygons minus overlapping rooms.
// Sum of rooms of all polygons = (n + levels*n)*levels / 2
// Overlapping rooms = (3 + 3+2*(levels - 2))* (levels - 1) / 2 
//
// max n = 10^6; max levels = 10^6 -- so order of magnitude of 
// rooms is (10^6)*(10^6)*(10^6) = 10^18.  Therefore - we use "long" type.
// 

public class Castle {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int k = in.nextInt();
		out.println(numberOfRooms(n, k));

	}

	//ensure that division by 2 happens at end to avoid 
	//truncating any decimal (0.5)
	//Used int parameters - which gave incorrect answer (integer overflow).
	//IOW - the return type did not ensure that overflow would not occur.
	public static long numberOfRooms(long n, long k){
		if( k == 1){
			return n;
		}
		return  ((n + n * (k)) * k) / 2 - 
				(((3 + 3 + 2 * (k - 2)) * (k - 1)) / 2);
	}
	

	public static void main(String args[]) {
		new Castle().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}

}
