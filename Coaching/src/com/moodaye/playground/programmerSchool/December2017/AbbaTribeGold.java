package com.moodaye.playground.programmerSchool.December2017;

import java.util.Scanner;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.util.Locale;

/** Programmer's School #7 */
/*
 * We need to find the largest of 3 integer numbers. Since the max is 10^100, we
 * cannot use integer primitives. Will use the java.math library. Alternately -
 * the longest string is max. If strings are of equal length - we compare digit
 * by digit starting with the most significant digit. Complexity for this
 * process is O(n). Max length of string is 100+1. Therefore this should be
 * doable in less than 1 sec (max 10^8 operations). We could make this even more
 * efficient using divide and conquer
 */
public class AbbaTribeGold {
	void solve(Scanner in, PrintWriter out) {
		String n1 = in.next();
		String n2 = in.next();
		String n3 = in.next();
		out.println(findMax_1(n1, n2, n3));
	}

	public static String findMax_2(String n1, String n2, String n3) {
		MyBigInteger big1 = new MyBigInteger(n1);
		MyBigInteger big2 = new MyBigInteger(n2);
		MyBigInteger big3 = new MyBigInteger(n3);
		
		MyBigInteger max = big1.max(big2);
		max = big3.max(max);
		return max.toString();
	}

	public static String findMax_1(String n1, String n2, String n3) {
		BigInteger big1 = new BigInteger(n1);
		BigInteger big2 = new BigInteger(n2);
		BigInteger big3 = new BigInteger(n3);
		
		BigInteger max = big1.max(big2);
		max = big3.max(max);
		return max.toString();
	}

	public static void main(String[] args) {
		new AbbaTribeGold().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}

}

class MyBigInteger implements Comparable<MyBigInteger> {
	String s;

	MyBigInteger(String s) {
		this.s = s;
	}

	@Override
	public int compareTo(MyBigInteger o) {
		if (s.equals(o.s)) {
			return 0;
		}
		if (s.length() > o.s.length()) {
			return +1;
		}
		if (s.length() < o.s.length()) {
			return -1;
		}

		// lengths are equals ... so compare strings
		return (s.compareTo(o.s));
	}
	
	public MyBigInteger max(MyBigInteger val){
		if(this.compareTo(val) < 0){
			return val;
		}
		return this;
	}
	
	public String toString(){
		return s;
	}
}
