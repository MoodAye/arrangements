package com.moodaye.playground.programmerSchool.December2017;

import static org.junit.Assert.*;

import org.junit.Test;
import static com.moodaye.playground.programmerSchool.December2017.AbbaTribeGold.*;
public class AbbaTribeGoldTest {

	@Test
	public void test1() {
		assertEquals("7", findMax_1("5", "7", "3"));
		assertEquals("7", findMax_1("5", "3", "7"));
		assertEquals("7", findMax_1("7", "5", "3"));
		
		assertEquals("7", findMax_1("7", "3", "5"));
		assertEquals("7", findMax_1("3", "5", "7"));
		assertEquals("7", findMax_1("3", "7", "5"));
		assertEquals("7", findMax_1("7", "7", "7"));
		assertEquals("7", findMax_1("7", "7", "3"));
		assertEquals("7", findMax_1("7", "3", "7"));
		assertEquals("7", findMax_1("3", "7", "7"));
		
		assertEquals("7", findMax_1("3", "3", "7"));
		assertEquals("7", findMax_1("3", "7", "3"));
		assertEquals("7", findMax_1("7", "3", "3"));
		
		assertEquals("987531", findMax_1("987531", "234", "86364"));
		assertEquals("4958439238923098349024", findMax_1("189285", "283", "4958439238923098349024"));
		
	}
	
	@Test
	public void testMyBigInteger(){
		MyBigInteger m1 = new MyBigInteger("3");
		MyBigInteger m2 = new MyBigInteger("7");
		
		assertTrue(m1.compareTo(m2) < 0);
		assertTrue(m2.compareTo(m1) > 0);
		assertTrue(m2.compareTo(m2) == 0);
		
		m1 = new MyBigInteger("4958439238923098349024");
		m2 = new MyBigInteger("4958439238923098349024");
		
		assertTrue(m1.compareTo(m2) == 0);
		assertTrue(m2.compareTo(m1) == 0);
		
		m2 = new MyBigInteger("958439238923098349024");
		assertTrue(m1.compareTo(m2) > 0);
		assertTrue(m2.compareTo(m1) < 0);
		
		m1 = new MyBigInteger("2");
		m2 = new MyBigInteger("20");
		assertTrue(m1.compareTo(m2) < 0);
		assertTrue(m2.compareTo(m1) > 0);
		
		
		
	}

	@Test
	public void test2() {
		assertEquals("7", findMax_2("5", "7", "3"));
		assertEquals("7", findMax_2("5", "3", "7"));
		assertEquals("7", findMax_2("7", "5", "3"));
		
		assertEquals("7", findMax_2("7", "3", "5"));
		assertEquals("7", findMax_2("3", "5", "7"));
		assertEquals("7", findMax_2("3", "7", "5"));
		assertEquals("7", findMax_2("7", "7", "7"));
		assertEquals("7", findMax_2("7", "7", "3"));
		assertEquals("7", findMax_2("7", "3", "7"));
		assertEquals("7", findMax_2("3", "7", "7"));
		
		assertEquals("987531", findMax_2("987531", "234", "86364"));
		assertEquals("4958439238923098349024", findMax_2("189285", "283", "4958439238923098349024"));
		
	}
}
