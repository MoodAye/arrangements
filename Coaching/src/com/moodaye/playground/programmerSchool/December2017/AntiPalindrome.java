package com.moodaye.playground.programmerSchool.December2017;

import java.util.Scanner;
import java.util.Locale;
import java.util.Optional;
import java.io.PrintWriter;

/** Programmer's School #491 */
/*
 * Approach 1 = brute force. Complexity is O(n^2). Time limit is 1sec. Max input
 * string length = 10^5. (so this likely won't work).
 * 
 * After running - the solution (approach 1) is indeed failing on test 15
 * (running time = 1.156s)
 * 
 * Approach 2 is failing on test 17 (running time = 1.189).
 * 
 * Approach 3 has O(n^2) - and this is failing as expected (test 17, 1.189)
 * 
 * Approach 4 --- Arrived at a solution - that needs to be proved. O(n)
 * complexity. Proof from FM comments in BitBucket: If s is a palindrome and s
 * without last character is not empty and is a palindrome, then all chars are
 * equal. You can prove it. Let's first character of s is x. Then as s is a
 * palindrome, last char of s is x. As s without last char is a palindrome,
 * second from the end char of s is x. Then if the second from the end char of s
 * is x then as s is a palindrome, second from the start character is x. And so
 * on. If these two strings are palindromes, then all chars are the same, and
 * then you cannot get a string that is not a palindrome. 
 * 
 * Approach 5 --- Simplified approach 4
 * 
 */
public class AntiPalindrome {

	void solve(Scanner in, PrintWriter out) {
		String s = in.next();
		// Optional<String> subs =
		// Optional.ofNullable(longestAntiPalindrome_4(s));
		// out.println(subs.orElse("NO SOLUTION"));

		Optional<String> subs = Optional.ofNullable(approach5(s));
		out.println(subs.orElse("NO SOLUTION"));
	}

	public static String approach5(String s) {
		if (!isPalindrome(s)) {
			return s;
		} else if (s.length() == 1 || isPalindrome(s.substring(0, s.length() - 1))) {
			return null;
		} else {
			return s.substring(0, s.length() - 1);
		}
	}

	/*
	 * I have some intuition that if the original string is a palindrome and the
	 * characters are not all the same, then the longest anti-palidrome is the
	 * first character to the second to last.
	 * 
	 * Complexity here is O(n)
	 */
	public static String longestAntiPalindrome_4(String s) {
		char[] cs = s.toCharArray();
		int n = s.length();
		boolean isPal = true;
		boolean allCharsSame = true;

		for (int i = 0; i < n / 2 + (n % 2); i++) {
			if (cs[i] != cs[0]) {
				allCharsSame = false;
			}

			if (cs[i] != cs[n - i - 1]) {
				isPal = false;
				break;
			}
		}
		return isPal ? s.length() == 1 || allCharsSame ? null : s.substring(0, n - 1) : s;
	}

	/*
	 * Assumes that the longest palindrome has to begin at the first character.
	 * (need to prove this ... but will try this first). However this will have
	 * complexity of O(n^2) - so this won't work either
	 */
	public static String longestAntiPalindrome_3(String s) {
		int n = s.length();
		String sub = null;
		boolean fnd = false;
		for (int len = n; len > 1 && !fnd; len--) {
			if (!isPalindrome(s.substring(0, 0 + len))) {
				sub = s.substring(0, 0 + len);
				fnd = true;
				break;
			}
		}
		return sub;
	}

	/*
	 * given string say abcdefg - test all substrings of size 7 ("abcdefg").
	 * Then size 6 ("abcdef", "bcdef") then size 5 ("abcd", "bcde", "cdef"....).
	 * This will be more efficient than the approach 1 ... but still complexity
	 * is O(n^3)
	 */
	public static String longestAntiPalindrome_2(String s) {
		int n = s.length();
		String sub = null;
		boolean fnd = false;
		for (int len = n; len > 1 && !fnd; len--) {
			for (int i = 0; i + len <= n; i++) {
				if (!isPalindrome(s.substring(i, i + len))) {
					sub = s.substring(i, i + len);
					fnd = true;
					break;
				}
			}
		}
		return sub;
	}

	public static String longestAntiPalindrome_1(String s) {
		int length = 0;
		int n = s.length();
		String subs = null;

		// abcde ... i = b(1); n = 5; n - i
		for (int i = 0; i < n - 1; i++) {
			for (int j = n - i; j > i; j--) {
				if (!isPalindrome(s.substring(i, j))) {
					if (length < j - i + 1) {
						length = j - 1 + 1;
						subs = s.substring(i, j);
						break;
					}
				}
			}
		}
		return subs;
	}

	public static boolean isPalindrome(String s) {
		char[] cs = s.toCharArray();
		int n = s.length();
		boolean isPal = true;
		for (int i = 0; i < n / 2 + (n % 2); i++) {
			if (cs[i] != cs[n - i - 1]) {
				isPal = false;
				break;
			}
		}
		return isPal;
	}

	public static void main(String args[]) {
		new AntiPalindrome().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}

}
