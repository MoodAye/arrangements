package com.moodaye.playground.programmerSchool.December2017;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #517 */
// 2 ways to solve this that i can think of.
// First way is to input all the balls; add all the scores and
// then go back through and calculate any extra points for strikes.
public class Bowling {

	// Solve this by first reading in pinsHit for all 10 frames
	// then going through the pins hit to score.
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int[] pinsHit = new int[n];
		for (int i = 0; i < n; i++) {
			pinsHit[i] = in.nextInt();
		}
		int score = 0;
		int ball = 0;
		for (int frame = 1; frame <= 10; frame++) {
			if (pinsHit[ball] == 10) {
				score += pinsHit[ball] + pinsHit[ball + 1] + pinsHit[ball + 2];
				ball++;
			} else {
				score += pinsHit[ball] + pinsHit[ball + 1];
				if (pinsHit[ball] + pinsHit[ball + 1] == 10) {
					score += pinsHit[ball + 2];
				}
				ball += 2;
			}
		}
		out.println(score);
	}

	public static void main(String args[]) {
		new Bowling().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
