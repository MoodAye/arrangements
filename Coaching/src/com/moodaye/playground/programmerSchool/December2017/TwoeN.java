package com.moodaye.playground.programmerSchool.December2017;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;
import java.math.BigInteger;

/** Programmer's School #40 */
// Max value of long is 2^63 - 1.  Max power of 2 it can hold is 2^62.
// To get to 2^999 - we can start with this value and manually implement
// the *2 operation (multiply by 2) using int array representation
// of the number. 
//
// Length of 2^999 ~ 2^1000 is ... log2^1000 =  1000 * log 2 ~ 1000 * 0.3 ~ 300.
// Therefore 2~999 has ~300 digits.
// To double 999 - 62 ~ 1000 times will have complexity of O(n^2).  


public class TwoeN {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		out.println(powerOfTwo_4(n));
	}
	
	public static void doubleArray(int[] d){
		int carry = 0;
		for(int i = 0; i < d.length; i++){
			d[i] = d[i] * 2 + carry;
			carry = d[i] / 10;
			d[i] %= 10;
		}
	}
	
	public static String powerOfTwo_4(int n){
		int[] d = new int[350];
		d[0] = 1; //seed
		for(int i = 0; i < n; i++){
			doubleArray(d);
		}
		
		StringBuilder sb = new StringBuilder();
		int firstDigit = d.length - 1;
		while(d[firstDigit] == 0){
			firstDigit--;
		}
		for(int k = firstDigit; k >= 0; k--){
			sb.append(d[k]);
		}
		return String.valueOf(sb);
	}
	
	// What appears below is the initial implementation using char array
	// Can ignore
	public int multiply(int i, int factor){
		return i * factor; 
	}
	
	public static String powerOfTwo_3(int n){
		if(n <= 62){
			return String.valueOf(1L << n);
		}
		
		char[] d = new char[350];
		for(int i = 0; i < 350; i++){
			d[i] = '0';
		}
		d[349] = '1';
		int start = 349;
		for(int i = 1; i <= n; i++){
			start = doubleArray(d, start);
		}
		int i = 0;
		while(d[i] == '0'){
			i++;
		}
		char[] finalC = new char[350 - i];
		for(int j = 0; j < 350 - i; j++){
			finalC[j] = d[j + i];
		}
		return String.valueOf(finalC);
	}
	

	// k = start index for number
	// note - array in needs to be large enough to hold the double value 
	// might need an additional cell for this.
	public static int doubleArray(char[] a, int k){
		int crry = 0;
		int i = a.length - 1;
		for(; i >= k; i--){
			char c = a[i];
			//TODO - combine the 2 statements below - does not seem to work
			int x = ((c - '0') * 2) % 10 + '0' + crry;
			a[i] = (char) x;
			crry =  (int) ((c - '0') * 2) / 10;
		}
		if(crry != 0){
			a[i] = (char) ((int) '0' + crry);
			k--;
		}
		return k;
	}
	
	private static int initDigitsWith2pow62(char[] d){
		char[] pow62 = String.valueOf(1L << 62).toCharArray();
		for(int i = pow62.length - 1, j = d.length - 1; i >= 0; i--, j--){
			d[j] = pow62[i];
		}
		return pow62.length;
	}

	/** Power of two using BigInteger */
	public static String powerOfTwo(int n){
		return BigInteger.valueOf(2).pow(n).toString();
	}

	public static void main(String args[]) {
		new TwoeN().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}

}
