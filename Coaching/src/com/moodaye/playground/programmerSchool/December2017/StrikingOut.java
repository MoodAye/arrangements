package com.moodaye.playground.programmerSchool.December2017;

import java.util.Scanner;
import java.util.Set;
import java.util.HashSet;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #273 */
// The length of the input is 101 (10^100)
// If we try all 3 char combos -- we get complexity of O(n^3) ~ 1_000_000
// which is less than the max allowed in 1 sec ( ~ 10^8 operations).
public class StrikingOut {
	void solve(Scanner in, PrintWriter out) {
		out.println(cnt3DigitNumbers(in.next()));
	}

	//Refactored code to make more efficient.
	//Improved by factor of 60 for a 1000 char input string.
	public static int cnt3DigitNumbers_Efficient(String s) {
		char[] c = s.toCharArray();
		int n = s.length();
		boolean[] counted = new boolean[1000];
		int cnt = 0;

		for (int i = 0; i < n - 2; i++) {
			if (c[i] == '0') {
				continue;
			}
			for (int j = i + 1; j < n - 1; j++) {
				for (int k = j + 1; k < n; k++) {
					int number = (c[i] - '0') * 100 
							+ (c[j] - '0') * 10 
							+ (c[k] - '0');
					if (!counted[number]) {
						cnt++;
						counted[number] = true;
					}
				}
			}
		}
		return cnt;
	}

	public static int cnt3DigitNumbers(String s) {
		char[] c = s.toCharArray();
		int n = s.length();
		Set<String> combos = new HashSet<>();

		for (int i = 0; i < n - 2; i++) {
			if (c[i] == '0') {
				continue;
			}
			for (int j = i + 1; j < n - 1; j++) {
				for (int k = j + 1; k < n; k++) {
					StringBuilder sb = new StringBuilder().append(c[i]).append(c[j]).append(c[k]);
					combos.add(sb.toString());
				}
			}
		}
		return combos.size();
	}

	public static void main(String args[]) {
		new StrikingOut().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}

}
