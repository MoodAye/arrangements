package com.moodaye.playground.programmerSchool.December2017;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #680 */
public class GardnerArtist {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		// there are 3 ways to paint the first tree, 2 ways to paint the subsequent trees.
		// max combos = 3 * 2 ^ (n - 1)
		out.println( 3L << (n - 1));
		
		// option 2
//		out.println((long) (3 * Math.pow(2, n - 1)));
		
		
		
	}

	public static void main(String args[]) {
		new GardnerArtist().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}

}
