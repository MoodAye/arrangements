package com.moodaye.playground.programmerSchool.December2017;

import java.util.Scanner;
import java.io.PrintWriter;
import java.util.Locale;

/** Programmer's School #676 */
/** Arrived at solution with difficulty -- and by trying it on the website. 
 * Started by considering numbers less than 1000.  
 * Arrived at n % 3 == 0 ---> (2) wins.  Otherwise (1) wins.
 *
 * Then looked at numbers greater than 1000 ... and arrived at 
 * (n - 1002) % 3 == 0 ---> (2) wins.  
 * 
 * Then looked at n > 1,999 and while doing this thought of trying the final solution - which worked.
 * 
 * I don't know how to prove it. Is there a structured way for solving similar problems. 
 */
public class GamesWithMatches {

	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		out.println((n % 1002) % 3 == 0 ? 2 : 1);
	}

	public static void main(String[] args) {
		new GamesWithMatches().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}

}
