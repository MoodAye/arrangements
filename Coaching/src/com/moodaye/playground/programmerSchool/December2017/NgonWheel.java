package com.moodaye.playground.programmerSchool.December2017;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;
import java.math.BigDecimal;

/** Programmer's School #69 */
public class NgonWheel {
	void solve(Scanner in, PrintWriter out) {
		double n = in.nextDouble();
		double a = in.nextDouble();
		
		double angle = Math.PI / n; 
		double diff =  a/2 * (1/Math.sin(angle) - 1/Math.tan(angle));
		out.println(diff >= 1 ? "NO" : "YES");
	}

	public static void main(String args[]) {
		new NgonWheel().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
