package com.moodaye.playground.programmerSchool.December2017;

import static org.junit.Assert.*;
import org.junit.Test;
import static com.moodaye.playground.programmerSchool.December2017.AntiPalindrome.*;

public class AntiPalindromeTest {
	
	@Test
	public void testLongestPal(){
		assertNull(longestAntiPalindrome_1("a"));
		assertEquals("ab", longestAntiPalindrome_1("ab"));
		assertEquals("abcdcbaxe", longestAntiPalindrome_1("abcdcbaxe"));
		assertEquals("abcdcb", longestAntiPalindrome_1("abcdcba"));
		assertNull(longestAntiPalindrome_1("aaaaa"));
		assertEquals("aabba", longestAntiPalindrome_1("aabbaa"));
	}
	
	@Test
	public void testApproach5(){
		assertNull(approach5("a"));
		assertEquals("ab", approach5("ab"));
		assertEquals("abcdcbaxe", approach5("abcdcbaxe"));
		assertEquals("abcdcb", approach5("abcdcba"));
		assertNull(approach5("aaaaa"));
		assertEquals("aabba", approach5("aabbaa"));
	}

	@Test
	public void testIsPal() {
		assertTrue(isPalindrome("a"));
		assertTrue(isPalindrome("aba"));
		assertTrue(isPalindrome("abba"));
		assertTrue(isPalindrome("abcdefgfedcba"));
		assertTrue(isPalindrome("aaaa"));
		assertTrue(isPalindrome("aaaaa"));
		
		assertFalse(isPalindrome("ab"));
		assertFalse(isPalindrome("abcdefg"));
		assertFalse(isPalindrome("aaab"));
	}
	
	@Test
	public void testLongestPal2(){
		assertNull(longestAntiPalindrome_2("a"));
		assertEquals("ab", longestAntiPalindrome_2("ab"));
		assertEquals("abcdcbaxe", longestAntiPalindrome_2("abcdcbaxe"));
		assertEquals("abcdcb", longestAntiPalindrome_2("abcdcba"));
		assertNull(longestAntiPalindrome_2("aaaaa"));
		assertEquals("aabba", longestAntiPalindrome_2("aabbaa"));
	}

	
	@Test
	public void testLongestPal3(){
		assertNull(longestAntiPalindrome_3("a"));
		assertEquals("ab", longestAntiPalindrome_3("ab"));
		assertEquals("abcdcbaxe", longestAntiPalindrome_3("abcdcbaxe"));
		assertEquals("abcdcb", longestAntiPalindrome_3("abcdcba"));
		assertNull(longestAntiPalindrome_3("aaaaa"));
		assertEquals("aabba", longestAntiPalindrome_3("aabbaa"));
	}
	
	@Test
	public void testLongestPal4(){
		assertNull(longestAntiPalindrome_4("a"));
		assertEquals("ab", longestAntiPalindrome_4("ab"));
		assertEquals("abcdcbaxe", longestAntiPalindrome_4("abcdcbaxe"));
		assertEquals("abcdcb", longestAntiPalindrome_4("abcdcba"));
		assertNull(longestAntiPalindrome_4("aaaaa"));
		assertEquals("aabba", longestAntiPalindrome_4("aabbaa"));
	}
}
