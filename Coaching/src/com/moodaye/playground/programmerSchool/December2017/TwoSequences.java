package com.moodaye.playground.programmerSchool.December2017;

import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;
import java.util.HashSet;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #91 */
 /* Performance complexity is O(n).  Since max n is 10_000 - we have enough time to solve this problem.
  * We mark the b array to indicate "a" values. Therefore we need array of size large enough- which 
  * via trial and error is 100_000. Therefore space complexity is 100_000 x 2 x 32 = 6_400_000 bytes. We have enough space.
  * 
  * Implemented second solution using TreeSet to track arrayValues.  This took longer - due to lookups in tree structure
  * taking longer than the array lookup of the alternate solution.  
  * The space complexity of this solution is 10_000 x 2 x 32 = 640_000 bytes (1/10th less)
  * The performance complexity with be poorer since the lookup on the treeset has O(logN) vs. O(1) for the alternate solution.
 */
public class TwoSequences {
	void solve2(Scanner in, PrintWriter out){
		int n = in.nextInt();
		int[] a = new int[10_000 + 1];
		int[] b = new int[10_000 + 1];
		a[1] = 2;
		a[2] = 3;
		a[3] = 4;
		b[1] = 1;
		b[2] = 5;
		b[3] = 6;
		Set<Integer> aValues = new TreeSet<Integer>();
		
		for(int i = 4; i <= n; i++){
			a[i] = b[i - 1] + b[i -3];
			aValues.add(a[i]);
			b[i] = b[i - 1] + 1;
			while(aValues.contains(b[i])){
				b[i]++;
			}
		}
		out.println(a[n]);
		out.println(b[n]);
	}
	
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int[] a = new int[100_000 + 1];
		int[] b = new int[100_000 + 1];
		a[1] = 2;
		a[2] = 3;
		a[3] = 4;
		b[1] = 1;
		b[2] = 5;
		b[3] = 6;
		
		for(int i = 4; i <= n; i++){
			a[i] = b[i - 1] + b[i -3];
			b[a[i]] = -1;
			b[i] = b[i - 1] + 1;
			while(b[b[i]] == -1){
				b[i]++;
			}
		}
		out.println(a[n]);
		out.println(b[n]);
	}

	public static void main(String args[]) {
		new TwoSequences().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
