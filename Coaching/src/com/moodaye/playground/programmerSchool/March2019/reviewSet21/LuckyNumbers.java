package com.moodaye.playground.programmerSchool.March2019.reviewSet21;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #671 */
// > 3hrs.
public class LuckyNumbers {
	void solve(Scanner in, PrintWriter out) {
		String n = in.next();
		out.println(luckyCnt(n));
	}

	public static long luckyCnt(String n) {
		long sum = (1L << n.length()) - 2L;
		return sum + luckyCnt(n, 0);
	}

	public static long luckyCnt(String n, int idx) {
		int len = n.length();

		char c = n.charAt(idx);
		if (c < '4') {
			return 0;
		} else if (c == '4') {
			if (idx == len - 1) {
				return 1;
			}
			return luckyCnt(n, idx + 1);
		} else if ('4' < c && c < '7') {
			if (idx == len - 1) {
				return 1;
			}
			return 1 << (len - idx - 1);
		} else if (c < '7') {
			return 0;
		} else if (c == '7') {
			if (idx == len - 1) {
				return 2;
			}
			long sum4 = 1 << (len - idx - 1);
			return sum4 + luckyCnt(n, idx + 1);
		}
		return (1L << n.length() - idx);
	}

	public static void main(String args[]) {
		new LuckyNumbers().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
