package com.moodaye.playground.programmerSchool.March2019.reviewSet21;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #671 */
// Start = 8:28pm  End 8:43pm   (15min)
public class LuckyNumbers_2 {
	void solve(Scanner in, PrintWriter out) {
		String n = in.next();
//		out.println(cntLucky(n));
		out.println(cntLuckyNoRecursion(n));
	}
	
	public static long cntLucky(String n){
		long sum = (1L << n.length()) - 2;  // geo series for numbers of length smaller than n
		return sum + cntLucky(n, 0);
	}
	
	private static long cntLuckyNoRecursion(String n){
		long sum = (1L << n.length()) - 2;
		int idx = 0;
		int len = n.length();
		char c = '0';
		while(idx < len){
			c = n.charAt(idx);
			if(c < '4'){
				break;
			}
			else if(c == '4'){
				if(idx == len - 1){
					sum += 1;
				}
			}
			else if('4' < c && c < '7'){
				sum += 1L << (len - idx - 1);
				break;
			}
			else if(c == '7'){
				if(idx == len - 1){
					sum += 2;
				}
				else{
					// we know cnt for numbers with 4 as the first digit
					sum += 1L << (len - idx - 1); 
				}
			}
			else if(c > '7'){
				sum += 1L << (len - idx);
				break;
			}
			idx++;
		}
		return sum;
	}
	
	private static long cntLucky(String n, int idx ){
		int len = n.length();
		
		char c = n.charAt(idx);
		long cnt = 0L;
		
		if(c < '4'){
			return 0L;
		}
		if(c == '4'){
			if(idx == len - 1){
				return 1L;
			}
			return cntLucky(n, idx + 1);
		}
		if('4' < c && c < '7'){
			return 1L << (len - idx - 1);
		}
		if(c == '7'){
			if(idx == len - 1){
				return 2L;
			}
			cnt = 1L << (len - idx - 1);
			return cnt + cntLucky(n, idx + 1);
		}
		if(c > '7'){
			return 1L << (len - idx);
		}
		return cnt;
	}

	public static void main(String args[]) {
		new LuckyNumbers_2().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}