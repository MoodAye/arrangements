package com.moodaye.playground.programmerSchool.March2019.reviewSet21;

import java.util.Scanner;
import java.util.Arrays;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #71 */
// for recursive solution - stack depth will be 2^18 or about 500,000. 
// how would I estimate the memory requirement? 
// (2 integers per call + some overhead --- so about 10bytes per method call?)
// or 5Mb?
// Time taken > 2hrs
public class TwoPilesOfStones {
	private static int totWt;
	private int[] stones;
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		stones = new int[n];
		for (int i = 0; i < stones.length; i++) {
			stones[i] = in.nextInt();
			totWt += stones[i];
		}
		out.println(minWeight(0, 0));
	}
	
	// each stone can go into w1 or not.  w2 = totWt - w1
	private int minWeight(int w1, int idx){
		//base case
		if(idx == stones.length){
			return Math.abs((totWt - w1) - w1);
		}
		int d1 = minWeight(w1 + stones[idx], idx + 1);
		int d2 = minWeight(w1, idx + 1);
		return Math.min(d1, d2);
	}

	public static void main(String args[]) {
		new TwoPilesOfStones().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
