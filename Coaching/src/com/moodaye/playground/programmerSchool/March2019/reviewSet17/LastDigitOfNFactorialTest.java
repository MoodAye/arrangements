package com.moodaye.playground.programmerSchool.March2019.reviewSet17;

import static org.junit.Assert.*;

import java.math.BigInteger;

import org.junit.AfterClass;
import org.junit.Test;
import static com.moodaye.playground.programmerSchool.March2019.reviewSet17.LastDigitOfNFactorial.*;

public class LastDigitOfNFactorialTest {

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Test
	public void test() {
		for(int i = 1; i < 10_000; i++){
			if(lastDigit(i) != biLastDigit(i)){
				System.out.printf("%d %d %d%n", i, lastDigit(i), biLastDigit(i));
			}
		}
	}

	// wierd that here it identifies incorrect i = 25 ...but then not i = 26 ... etc. until i = 125.
	// After than all subsequent i are incorrect. If i = 25 is incorrect then all i after should also be.
	private int biLastDigit(int n){
	BigInteger biN = BigInteger.valueOf(1);
		
		for(int i = 1; i <= n; i++){
			biN = biN.multiply(BigInteger.valueOf(i));
		}
		
		
		BigInteger ten = BigInteger.valueOf(10);
		BigInteger zero = BigInteger.valueOf(0);
		while(biN.mod(ten).equals(zero)){
			biN = biN.divide(ten);
		}
		return biN.mod(ten).intValue();
	}
}
