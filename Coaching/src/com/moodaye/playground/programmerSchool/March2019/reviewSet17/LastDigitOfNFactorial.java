package com.moodaye.playground.programmerSchool.March2019.reviewSet17;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #447 */
// Time Taken > 1hr.
public class LastDigitOfNFactorial {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		out.println(lastDigit(n));
	}
	
	static int lastDigit(int n){
		int lastDigit = 1;
		int cnt5x2 = 0;
		for (int i = 2; i <= n; i++) {
			int temp = i;
			while(temp % 2 == 0){
				temp /= 2;
				cnt5x2++;
			}
			while(temp % 5 == 0){
				temp /= 5;
				cnt5x2--;
			}
			int next = getLastDigit(temp);
			lastDigit = getLastDigit(next * lastDigit);
		}
		while(cnt5x2 > 0){
			lastDigit = getLastDigit(2 * lastDigit);
			cnt5x2--;
		}
		return lastDigit;
	}

	static int getLastDigit(int n) {
		while (n % 10 == 0) {
			n /= 10;
		}
		return n % 10;
	}

	public static void main(String args[]) {
		new LastDigitOfNFactorial().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
