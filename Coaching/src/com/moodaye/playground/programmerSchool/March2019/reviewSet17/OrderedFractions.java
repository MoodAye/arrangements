package com.moodaye.playground.programmerSchool.March2019.reviewSet17;

import java.util.Scanner;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #422 */
// Time Taken ~ 20 min
public class OrderedFractions {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		List<F> list = new ArrayList<>();
		for(int i = 2; i <= n; i++){
			list.add(new F(1, i));
			for(int j = 2; j < i; j++){
				if(gcd(i, j) == 1){
					list.add(new F(j, i));
				}
			}
		}
		Collections.sort(list);
		for(F f : list){
			out.printf("%d/%d%n", f.n, f.d);
		}
	}
	
	int gcd(int a, int b){
		while(b != 0){
			int temp = a % b;
			a = b; 
			b = temp;
		}
		return a;
	}

	public static void main(String args[]) {
		new OrderedFractions().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}

class F implements Comparable<F>{
	int n;
	int d;
	
	F(int n, int d){
		this.n = n;
		this.d = d;
	}
	
	@Override
	public int compareTo(F f) {
		return (n * f.d) - (f.n * d);
	}
	
}
