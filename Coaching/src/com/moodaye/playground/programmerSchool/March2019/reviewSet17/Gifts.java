package com.moodaye.playground.programmerSchool.March2019.reviewSet17;

import java.util.Scanner;
import java.util.Arrays;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #472 */
// Time Taken ~ 30min
public class Gifts {
	void solve(Scanner in, PrintWriter out) {
		int chl = in.nextInt();
		int cnd = in.nextInt();

		int[] gifts = new int[chl];
		for (int i = 0; i < chl; i++) {
			gifts[i] = in.nextInt();
		}

		Arrays.sort(gifts);

		for (int i = 0; i < chl - 1; i++) {
			if (gifts[i] < gifts[i + 1]) {
				if ((gifts[i + 1] - gifts[i]) * (i + 1) >= cnd) {
					int smallest = gifts[i] + cnd / (i + 1);
					out.println(smallest);
					return;
				}
				cnd -= (gifts[i + 1] - gifts[i]) * (i + 1);
			}
		}
		int smallest = gifts[chl - 1] + cnd / (chl);
		out.println(smallest);
	}

	public static void main(String args[]) {
		new Gifts().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}

}
