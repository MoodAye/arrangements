package com.moodaye.playground.programmerSchool.March2019;

import java.util.Scanner;
import java.util.Arrays;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #673 */
// Took a long time ; > 3 hrs
public class NDigitNumbers2 {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int[] a = new int[n];
		if (n == 1) {
			out.println(10 + " " + 0);
			return;
		}
		Arrays.fill(a, 1);
		long cnt = 0;
		String smallest = "";
		int rightMost = a.length - 1;

		while (true) {
			long sum = addDigits(a);
			long prod = multiplyDigits(a);
			if (sum == prod) {
				cnt += getPerms(a);
				if (smallest.equals("")) {
					smallest = getInt(a);
				}
			}
			rightMost = next(a, rightMost, sum, prod);
			if (rightMost < 0) {
				break;
			}
		}
		out.println(cnt + " " + smallest);
	}

	long getPerms(int[] a) {
		long perms = fact(a.length);
		int[] cnt = new int[10];
		for (int i : a) {
			cnt[i]++;
		}

		for (int i : cnt) {
			perms /= fact(i);
		}

		return perms;
	}

	long fact(int n) {
		if (n == 0) {
			return 1;
		}
		long nFact = 1L;
		for (long i = 1; i <= n; i++) {
			nFact *= i;
		}
		return nFact;
	}

	int next(int[] a, int rightMost, long sum, long prod) {
		if (sum <= prod) {
			for(int i = a.length - 2; 0 < i && i >= rightMost; i--){
				if(a[i] < a[i - 1]){
					a[i]++;
					for(int j = i + 1; j < a.length; j++){
						a[j] = a[i];
					}
					return rightMost;
				}
			}
		}
			
		if (a[rightMost] == 9) {
			rightMost--;
			for (int i = rightMost; 0 <= i && i < a.length; i++) {
				a[i] = 2;
			}
			return rightMost;
		}

		int i = a.length - 1;
		while (i >= 0 && a[i] == 9) {
			i--;
		}
		a[i]++;
		int setValue = a[i];
		while (++i < a.length) {
			a[i] = setValue;
		}
		return rightMost;
	}

	String getInt(int[] a) {
		StringBuilder sb = new StringBuilder();
		for(int i : a){
			sb.append(i);
		}
		return sb.toString();
	}

	long addDigits(int[] a) {
		long sum = 0L;
		for (int i = 0; i < a.length; i++) {
			sum += 0L + a[i];
		}
		return sum;
	}

	long multiplyDigits(int[] a) {
		long prod = 1L;
		for (int i = 0; i < a.length; i++) {
			prod *= 1L * a[i];
		}
		return prod;
	}

	public static void main(String args[]) {
		new NDigitNumbers2().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
