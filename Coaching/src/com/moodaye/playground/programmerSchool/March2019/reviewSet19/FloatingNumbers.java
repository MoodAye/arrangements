package com.moodaye.playground.programmerSchool.March2019.reviewSet19;

import java.util.Scanner;
import java.util.Arrays;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #449 */
// Space Requirements: 32_768 x 2 x 4 ~250KB; 
// complexity is O(n). (And - use count sort)
// approach is greedy - absorb all integers within 2*f (f = float)
public class FloatingNumbers {
	void solve(Scanner in, PrintWriter out) {
		int f = in.nextInt();
		int n = in.nextInt();
		
		final int SHIFT = 32_768;
		int[] a = new int[SHIFT * 2];
		for (int i = 0; i < n; i++) {
			a[in.nextInt() + SHIFT]++;
		}

		int cnt = 0;
		for (int i = 0; i < a.length; i++) {
			if (a[i] == 0) {
				continue;
			}
			cnt++;
			// absorb all numbers within 2*f
			for(int j = i + 1; j < a.length && j <= i + 2 * f; j++){
				a[j] = 0;
			}
		}

		out.println(cnt);
	}

	public static void main(String args[]) {
		new FloatingNumbers().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
