package com.moodaye.playground.programmerSchool.March2019.reviewSet19;

import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #87 */
public class Strings2 {
	void solve(Scanner in, PrintWriter out) {
		List<String> list = new ArrayList<>();
		while (true) {
			String next = in.next();
			if (next.equals("ENDOFINPUT")) {
				break;
			}
			list.add(next);
		}
		
		Set<String> combinable = new TreeSet<>();
		for (int i = 0; i < list.size(); i++) {
			for (int j = 0; j < list.size(); j++) {
				String combo = list.get(i) + list.get(j);
				if (combo.length() <= 100) {
					combinable.add(combo);
				}
			}
		}

		int cnt = 0;
		for (String s : list) {
			if (combinable.contains(s)) {
				cnt++;
			}
		}
		out.println(cnt);
	}

	public static void main(String args[]) {
		new Strings2().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
