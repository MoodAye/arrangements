package com.moodaye.playground.programmerSchool.March2019.reviewSet19;

import java.util.Scanner;
import java.util.Arrays;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #216 */
// you need to pair smallest and largest albums together.
public class LabelsCollection {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int[] c = new int[n];
		for (int i = 0; i < n; i++) {
			c[i] = in.nextInt();
		}

		Arrays.sort(c);

		int cnt = 0;
		int left = 0;
		while (true) {
/*			This code is causing a time out - if used instead of the sort.  Why?
  			int right = n - 1;
			while (right > 0 && c[right] < c[right - 1]) {
				right--;
			}
			exch(c, right, n - 1);
			
*/
			Arrays.sort(c);
			while (left < n && c[left] == 0) {
				left++;
			}
			if (left >= n - 1) {
				break;
			}
			c[left]--;
			c[n - 1]--;
			cnt++;
		}
		out.println(cnt);
	}

	void resort(int[] c) {
	}

	void exch(int[] a, int i, int j) {
		int temp = a[i];
		a[i] = a[j];
		a[j] = temp;
	}

	public static void main(String args[]) {
		new LabelsCollection().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
