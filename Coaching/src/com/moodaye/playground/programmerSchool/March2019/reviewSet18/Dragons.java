package com.moodaye.playground.programmerSchool.March2019.reviewSet18;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #42 */
// Time Taken ~20min
public class Dragons {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		if(n == 1){
			out.println(1);
			return;
		}
		
		long cnt3 = 1;
		long cnt2 = 1;
		while(n >= 3){
			n -= 3;
			cnt3 *= 3;
		}
		while(n >= 2){
			n -= 2;
			cnt2 *= 2;
		}
		if(n == 1){
			cnt3 /= 3;
			cnt2 *= 2 * 2;
		}
		out.println(cnt2 * cnt3);
	}

	public static void main(String args[]) {
		new Dragons().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
