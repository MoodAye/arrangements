package com.moodaye.playground.programmerSchool.March2019.reviewSet18;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #255 */
// Tried to do this using hand... seems like
// the answer is derived using all the factors except the smallest.
public class NumbersRepresentation {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int smallestFactor = 0;
		for (int i = 2; i * i <= n; i++) {
			if (n % i == 0) {
				smallestFactor = i;
				break;
			}
		}
		if (smallestFactor == 0) {
			out.println(1 + " " + (n - 1));
		} else {
			out.println(n / smallestFactor + " " + (n - n / smallestFactor));
		}
	}

	int gcd(int a, int b) {
		while (b != 0) {
			int temp = a % b;
			a = b;
			b = temp;
		}
		return a;
	}

	public static void main(String args[]) {
		new NumbersRepresentation().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
