package com.moodaye.playground.programmerSchool.March2019.reviewSet18;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #669 */
public class ProductOfDigits3 {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();

		int cnt2 = 0;
		int cnt3 = 0;
		int cnt5 = 0;
		int cnt7 = 0;

		while (n % 2 == 0) {
			n /= 2;
			cnt2++;
		}
		while (n % 3 == 0) {
			n /= 3;
			cnt3++;
		}
		while (n % 5 == 0) {
			n /= 5;
			cnt5++;
		}
		while (n % 7 == 0) {
			n /= 7;
			cnt7++;
		}

		if (n != 1) {
			out.println(-1 + " " + -1);
			return;
		}

		StringBuilder largest = new StringBuilder();
		for (int i = 1; i <= cnt7; i++) {
			largest.append(7);
		}
		for (int i = 1; i <= cnt5; i++) {
			largest.append(5);
		}
		for (int i = 1; i <= cnt3; i++) {
			largest.append(3);
		}
		for (int i = 1; i <= cnt2; i++) {
			largest.append(2);
		}

		int cnt8 = 0;
		while (cnt2 >= 3) {
			cnt2 -= 3;
			cnt8++;
		}

		// at this point - we have no more than 2 2s.

		int cnt6 = 0;
		int cnt4 = 0;
		int cnt9 = 0;

		if (cnt2 == 1) {
			if ((cnt3 % 2) == 1) {
				cnt6++;
				cnt2--;
				cnt3--;
			}
		}

		if (cnt2 == 2) {
			if ((cnt3 % 2) == 0) {
				cnt4++;
				cnt2 -= 2;
			}
			if(cnt3 == 1){
				cnt3--;
				cnt2--;
				cnt6++;
			}
		}

		while (cnt3 > 1) {
			cnt3 -= 2;
			cnt9++;
		}
		
		StringBuilder smallest = new StringBuilder();
		append(smallest, 2, cnt2);
		append(smallest, 3, cnt3);
		append(smallest, 4, cnt4);
		append(smallest, 5, cnt5);
		append(smallest, 6, cnt6);
		append(smallest, 7, cnt7);
		append(smallest, 8, cnt8);
		append(smallest, 9, cnt9);

		out.println(smallest + " " + largest);

	}

	void append(StringBuilder sb, int f, int cnt) {
		while (cnt > 0) {
			sb.append(f);
			cnt--;
		}
	}

	public static void main(String args[]) {
		new ProductOfDigits3().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
