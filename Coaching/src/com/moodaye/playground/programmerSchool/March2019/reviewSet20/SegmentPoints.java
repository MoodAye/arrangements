package com.moodaye.playground.programmerSchool.March2019.reviewSet20;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #319 */
// TLE 
// Test Case: 10 1 -1 20   (Having trouble with rounding -- e.g., stepX method)
public class SegmentPoints {

	void solve(Scanner in, PrintWriter out) {
		long x1 = in.nextLong();
		long y1 = in.nextLong();
		long x2 = in.nextLong();
		long y2 = in.nextLong();

		if (x1 == x2) {
			out.println(y2 - y1 + 1);
			return;
		}

		if (y1 == y2) {
			out.println(x2 - x1 + 1);
			return;
		}

		long a = y2 - y1;
		long b = x1 - x2;
		long c = x2 * y1 - x1 * y2;

		// ensure move are towards the opposite end of the segment
		long dx = x1 > x2 ? -1 : 1;

		// start with move along x axis - then y - then alternate
		long x = x1 + dx;
		long y = y1;
		long side = isAbove(a, b, c, x, y);

		boolean xNext = false;

		// start (given)
		int cnt = 1;

		// while we have not reached the opposite end
		while ((x != x2 || y != y2) && ((x1 - x2) * (x - x2) >= 0 && (y1 - y2) * (y - y2) >= 0)) {
			if (side == 0) {
				cnt++;
				break;
			} else if (side < 0) {
				while (side < 0) {
					if (xNext) {
						// x += dx;
						x = stepX(a, b, c, x, y);
					} else {
						// y += dy;
						y = stepY(a, b, c, x, y);
					}
					side = isAbove(a, b, c, x, y);
				}
			} else {
				while (side > 0) {
					if (xNext) {
						x = stepX(a, b, c, x, y);
					} else {
						y = stepY(a, b, c, x, y);
					}
					side = isAbove(a, b, c, x, y);
				}
			}
			xNext = (xNext == false);
		}

		if (x == x2 && y == y2) {
			cnt += 1;
		} else {
			cnt += ((x2 - x1) / (x - x1)) - 1;
		}
		out.println(cnt);
	}

	long stepX(long a, long b, long c, long x, long y) {
		long dx;
		if ((b * y + c) % a == 0) {
			dx = -(b * y + c) / a;
		} else {
			dx = -(b * y + c) / a;
			if (dx == x) {
				dx = dx < 0 ? dx - 1 : dx + 1;
			}
		}
		return dx;
	}

	long stepY(long a, long b, long c, long x, long y) {
		long dy;
		if ((a * x + c) % b == 0) {
			dy = -(a * x + c) / b;
		} else {
			dy = -(a * x + c) / b;
			if (dy == y) {
				dy = dy < 0 ? dy - 1 : dy + 1;
			}
		}
		return dy;
	}

	long isAbove(long a, long b, long c, long x, long y) {
		return a * x + b * y + c;
	}

	public static void main(String args[]) {
		new SegmentPoints().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}

}
