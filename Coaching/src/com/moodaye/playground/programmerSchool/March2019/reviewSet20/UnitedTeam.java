package com.moodaye.playground.programmerSchool.March2019.reviewSet20;

import java.util.Arrays;
import java.util.Locale;
import java.util.Scanner;
import java.io.PrintWriter;

/** Programmer's School #245 */
// Complexity is O(n + nlogn)
// number of operations - worst case = n + nlogn ~ 100_000 < 1e8 / 2 (1/2 sec) 
public class UnitedTeam {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		if (n == 0) {
			out.println(0);
			return;
		}
		
		int[] p = new int[n];
		for (int i = 0; i < n; i++) {
			p[i] = in.nextInt();
		}
		Arrays.sort(p);
		
		int hi = n - 1;
		int maxPi = p[hi];
		int currPi = maxPi;
		for (int i = n - 2; i >= 0; i--) {
			currPi += p[i];
			while (p[hi] > p[i] + p[i + 1]) {
				currPi -= p[hi];
				hi--;
			}
			maxPi = Math.max(maxPi, currPi);
		}
		out.println(maxPi);
	}

	public static void main(String args[]) {
		new UnitedTeam().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}