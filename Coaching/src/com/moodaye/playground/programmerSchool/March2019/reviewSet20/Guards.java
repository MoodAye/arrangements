package com.moodaye.playground.programmerSchool.March2019.reviewSet20;

import java.util.Locale;
import java.io.InputStream;
import java.io.PrintWriter;

/** Programmer's School #630 */
// Time take ~ 2hr
public class Guards {
	void solve(Scanner in, PrintWriter out) {
		int t = in.nextInt();
		for (int i = 0; i < t; i++) {
			int n = in.nextInt();
			int[] start = new int[n];
			int[] end = new int[n];
			for (int j = 0; j < n; j++) {
				start[j] = in.nextInt();
				end[j] = in.nextInt();
			}
			if (ans2(start, end)){
				out.println("Accepted");
			}
			else{
				out.println("Wrong Answer");
			}
		}
	}
	
	boolean ans2(int[] start, int[] end){
		int g = start.length;
		int[] schedule = new int[10_000 + 2];
		
		for(int i = 0; i < g; i++){
			schedule[start[i] + 1]++;
			schedule[end[i] + 1]--;
		}
		
		for(int i = 1; i < schedule.length; i++){
			schedule[i] += schedule[i - 1];
		}
		
		for(int i = 1; i < schedule.length - 1; i++){
			if(schedule[i] == 0){
				return false;
			}
		}
		
		for(int i = 0; i < g; i++){
			boolean alone = false;
			for(int j = start[i] + 1; j <= end[i]; j++){
				if(schedule[j] == 1){
					alone = true;
					break;
				}
			}
			if(!alone){
				return false;
			}
		}
		
		return true;
	}

	// This approach gives TLE Test 13 as complexity is O(n^2) and operations worst case > 1e8
	boolean ans(int[] start, int[] end) {
		int g = start.length;
		int[] schedule = new int[10_000 + 1];
		for (int i = 0; i < g; i++) {
			for (int j = start[i]; j < end[i]; j++) {
				schedule[j + 1]++;
			}
		}

		for (int i = 1; i < schedule.length; i++) {
			if (schedule[i] == 0) {
				return false;
			}
		}

		for (int i = 0; i < g; i++) {
			boolean optimal = false;
			for (int j = start[i]; j < end[i]; j++) {
				if (schedule[j + 1] == 1) {
					optimal = true;
				}
			}
			if (!optimal) {
				return false;
			}
		}
		return true;
	}

	public static void main(String args[]) {
		new Guards().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}

class Scanner implements AutoCloseable {
	InputStream is;
	int pos = 0;
	int size = 0;
	byte[] buffer = new byte[1024];

	Scanner(InputStream is) {
		this.is = is;
	}

	int nextChar() {
		if (pos >= size) {
			try {
				size = is.read(buffer);
			} catch (java.io.IOException e) {
				throw new java.io.IOError(e);
			}
			pos = 0;
			if (size == -1) {
				return -1;
			}
		}
		Assert.check(pos < size);
		int c = buffer[pos] & 0xFF;
		pos++;
		return c;
	}

	int nextInt() {
		int c = nextChar();
		while (c == ' ' || c == '\r' || c == '\n' || c == '\t') {
			c = nextChar();
		}
		if (c == '-') {
			c = nextChar();
			Assert.check('0' <= c && c <= '9');
			int n = -(c - '0');
			c = nextChar();
			while ('0' <= c && c <= '9') {
				int d = c - '0';
				c = nextChar();
				Assert.check(
						n > Integer.MIN_VALUE / 10 || n == Integer.MIN_VALUE / 10 && d <= -(Integer.MIN_VALUE % 10));
				n = n * 10 - d;
			}
			return n;
		} else {
			Assert.check('0' <= c && c <= '9');
			int n = c - '0';
			c = nextChar();
			while ('0' <= c && c <= '9') {
				int d = c - '0';
				c = nextChar();
				Assert.check(n < Integer.MAX_VALUE / 10 || n == Integer.MAX_VALUE / 10 && d <= Integer.MAX_VALUE % 10);
				n = n * 10 + d;
			}
			return n;
		}
	}

	@Override
	public void close() {
	}
}

class Assert {
	static void check(boolean e) {
		if (!e) {
			throw new AssertionError();
		}
	}
}
