package com.moodaye.playground.programmerSchool;

import static org.junit.Assert.*;

import org.junit.Test;

public class FastestTrainTest {

	@Test
	public void test1() {
		Train t1 = new Train("\"ER-200\" 06:43 10:40");
		Train t2 = new Train("\"Red Arrow\" 23:55 07:55");
		Train t3 = new Train("\"Express\" 23:59 08:00");
		assertTrue(t1.duration < t2.duration);
		assertEquals(t1.name, t1.compareTo(t2).name);
		assertEquals(t1, t3.compareTo(t1));
		assertEquals(165, t1.speed());
		
		t1 = new Train("\"ER-200\"", "06:43", "10:40");
		t2 = new Train("\"Red Arrow\"", "23:55", "07:55");
		t3 = new Train("\"Express\"", "23:59", "08:00");
		assertTrue(t1.duration < t2.duration);
		assertEquals(t1.name, t1.compareTo(t2).name);
		assertEquals(t1, t3.compareTo(t1));
		assertEquals(165, t1.speed());
		
		t1 = new Train("\"ER-200\"", 6, 43, 10, 40);
		t2 = new Train("\"Red Arrow\"", 23, 55, 07, 55);
		t3 = new Train("\"Express\"", 23, 59, 8, 00);
		assertTrue(t1.duration < t2.duration);
		assertEquals(t1.name, t1.compareTo(t2).name);
		assertEquals(t1, t3.compareTo(t1));
		assertEquals(165, t1.speed());
	}

	@Test
	public void test2() {
		Train t1 = new Train("\"Train1\" 00:00 00:00");
		Train t2 = new Train("\"Train2\" 00:00 00:01");
		Train t3 = new Train("\"Train3\" 01:01 01:01");
		assertEquals(t2, t1.compareTo(t2));
		assertEquals(t2, t2.compareTo(t3));
		assertEquals(39000, t2.speed());
		assertEquals(24*60, t1.duration);
		assertEquals(1, t2.duration);
		assertEquals(24*60, t3.duration);
		
		t1 = new Train("\"Train1\"", "00:00", "00:00");
		t2 = new Train("\"Train2\"", "00:00", "00:01");
		t3 = new Train("\"Train3\"", "01:01", "01:01");
		assertEquals(t2, t1.compareTo(t2));
		assertEquals(t2, t2.compareTo(t3));
		assertEquals(39000, t2.speed());
		assertEquals(24*60, t1.duration);
		assertEquals(1, t2.duration);
		assertEquals(24*60, t3.duration);
		
		t1 = new Train("\"Train1\"", 0, 0, 0, 0);
		t2 = new Train("\"Train2\"", 0, 0, 0, 1);
		t3 = new Train("\"Train3\"", 1, 1, 1, 1);
		assertEquals(t2, t1.compareTo(t2));
		assertEquals(t2, t2.compareTo(t3));
		assertEquals(39000, t2.speed());
		assertEquals(24*60, t1.duration);
		assertEquals(1, t2.duration);
		assertEquals(24*60, t3.duration);
	}
	
	@Test
	public void test3() {
		Train t1 = new Train("\"Slow Train 1\" 10:00 09:59");
		Train t2 = new Train("\"Slow Train 2\" 10:00 10:00");
		assertEquals(t1, t1.compareTo(t2));
		assertEquals(27, t1.speed());
		
		t1 = new Train("\"Slow Train 1\"", "10:00", "09:59");
		t2 = new Train("\"Slow Train 2\"", "10:00", "10:00");
		assertEquals(t1, t1.compareTo(t2));
		assertEquals(27, t1.speed());
		
		t1 = new Train("\"Slow Train 1\"", 10, 0, 9, 59);
		t2 = new Train("\"Slow Train 2\"", 10, 0, 10, 0);
		assertEquals(t1, t1.compareTo(t2));
		assertEquals(27, t1.speed());
	}
}
