package com.moodaye.playground.programmerSchool;
import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

/** Programmer's School - # 48*/
// The choice is between 1 and any other divisor that 
// has a sum equal to 1 - since no other divisor can 
// have its digits' sum less than 1. This means the 
// choice is between 1 and a divisor that is a multiple of 10.
// The winner is the greatest multiple of 10. So the problem reduces
// to determining the number of trailing zeros.
// What is the best way to count trailing zeros for say 
// 230_000_000_000_000_000_000_000
public class WorstDivisor {
	void solve(Scanner in, PrintWriter out) {
		String n = in.next();
		char[] nc = n.toCharArray();
		int i = n.length() - 1;
		out.println("1");
		while(nc[i] == '0' && i >= 0){
			i--;
			out.println("0");
		}
	}
	
	public static void main(String[] args) {
		new WorstDivisor().run();
	}

	void run(){
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
