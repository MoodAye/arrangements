package com.moodaye.playground.programmerSchool;

import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Locale;
import java.util.Scanner;

/** Programmer's School - #693*/
public class Anagram {

	void solve(Scanner in, PrintWriter out) {
		char[] word1 = in.next().toLowerCase().toCharArray();
		char[] word2 = in.next().toLowerCase().toCharArray();
		
		Arrays.sort(word1);
		Arrays.sort(word2);
		
		out.println(Arrays.equals(word1, word2) ? "Yes" : "No");
	}

	public static void main(String[] args) {
		new Anagram().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
