package com.moodaye.playground.programmerSchool;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

/** Programmer School - problem 26 */
public class TwoCircles {
	void solve(Scanner in, PrintWriter out) {
		int a1 = in.nextInt();
		int b1 = in.nextInt();
		int r1 = in.nextInt();
		int a2 = in.nextInt();
		int b2 = in.nextInt();
		int r2 = in.nextInt();

		out.println(commonPoint2(a1, b1, r1, a2, b2, r2) ? "YES" : "NO");
	}
	

	/** 
	 * Checks whether the distance between the centers is greater that the sum 
	 * of the radii and less than the difference.  If the distance is greater - it 
	 * means  the circles are apart - they cannot touch - and if the distance is 
	 * smaller - then one circle encloses the other. 
	 * 
	 */
	public static boolean commonPoint2(int a1, int b1, int r1, int a2, int b2, int r2){
		//we can use squares here - to avoid having to use floats
		int c_sq = (a1 - a2) * (a1 - a2) + (b1 - b2) * (b1 - b2);
		int rDiff_sq = (r1 - r2) * (r1 - r2);
		int rSum_sq = (r1 + r2) * (r1 + r2);
	
		return c_sq >= rDiff_sq && c_sq <= rSum_sq;
	}
	

	/** Original approach - flawed - won't account for circles - e.g., in the shadow of a 
	 * bigger circle - think of a point (circle of zero radius) at the point where the base
	 * tangent of the circle meets the side tangent (vertical) of the circle*/
	public static String commonPoint(int a1, int b1, int r1, int a2, int b2, int r2) {

		int x1left = a1 - r1;
		int x1right = a1 + r1;
		int y1down = b1 - r1;
		int y1up = b1 + r1;

		int x2left = a2 - r2;
		int x2right = a2 + r2;
		int y2down = b2 - r2;
		int y2up = b2 + r2;

		if (     ((isBetween(x1left, x2left, x2right) || isBetween(x1right, x2left, x2right)) 
			     && (isBetween(y1down, y2down, y2up) || isBetween(y1up, y2down, y2up))) 
				
		     && ((isBetween(x2left, x1left, x1right) || isBetween(x2right, x1left, x1right)) 
		         && (isBetween(y2down, y1down, y1up) || isBetween(y2up, y1down, y1up)))     ){ 
			return "YES";
		} else {
			return "NO";
		}
	}

	/** left needs to be less than equal to right */
	public static boolean isBetween(int x, int left, int right) {
		return ((x >= left) && (x <= right)) ? true : false;
	}

	public static void main(String args[]) {
		new TwoCircles().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
