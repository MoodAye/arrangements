package com.moodaye.playground.programmerSchool.November2019;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

// Problem 373 
// 
// Complexity O(N^2 x K).  
// Space: O(N^2)
public class Route2 {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int k = in.nextInt();
		
		int[][] matrix = new int[n + 2][n + 2];
		
		for(int r = 1; r <= n; r++) {
			for(int c = 1; c <= n; c++) {
				matrix[r][c] = in.nextInt();
			}
		}
		
		int[][] pathValuesPrev = new int[n + 2][n + 2];
		int[][] pathValues = new int[n + 2][n + 2];
		
		for(int r = 1; r <= n; r++) {
			for(int c = 1; c <= n; c++) {
				pathValuesPrev[r][c] = matrix[r][c];
			}
		}
		
		for(int p = 1; p <= k; p++) {
			for(int r = 1; r <= n; r++) {
				for(int c = 1; c <= n; c++) {
					pathValues[r][c] = Math.max(Math.max(Math.max(
							pathValuesPrev[r - 1][c] + matrix[r][c],
							pathValuesPrev[r + 1][c] + matrix[r][c]),
							pathValuesPrev[r][c - 1] + matrix[r][c]),
							pathValuesPrev[r][c + 1] + matrix[r][c]);
				}
			}
			
			int[][] temp = pathValuesPrev;
			pathValuesPrev = pathValues;
			pathValues = temp;
		}
		
		out.println(pathValues[1][1]);
	}

	public static void main(String[] args) {
		new Route2().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
