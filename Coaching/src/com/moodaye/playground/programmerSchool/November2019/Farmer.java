package com.moodaye.playground.programmerSchool.November2019;

import java.io.InputStream;
import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

// Problem 113
public class Farmer {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int[][] plot = new int[n + 2][n + 2];

		for (int i = 0; i < n; i++) {
			String line = in.next();
			for (int j = 0; j < n; j++) {
				plot[i + 1][j + 1] = (line.charAt(j) - '0');
			}
		}

		int max = 0;
		for (int r = 1; r <= n; r++) {
			for (int c = 1; c <= n; c++) {
				int temp = Math.min(Math.min(plot[r - 1][c - 1], plot[r][c - 1]), plot[r - 1][c]);
				if(temp != 0 && plot[r][c] != 0) {
					plot[r][c] = temp + 1;
				}
				max = Math.max(plot[r][c], max);
			}
		}
		out.println(max * max);
	}

	public static void main(String[] args) {
		new Farmer().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
