package com.moodaye.playground.programmerSchool.November2019;

import java.io.PrintWriter;
import java.util.HashSet;
import java.util.Locale;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;

// Problem #378 
public class Sums {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int[] numbers = new int[n];
		for(int i = 0; i < n; i++) {
			numbers[i] = in.nextInt();
		}
		
		Set<Integer> sums = new TreeSet<Integer>();
		sums.add(0);
		
		int loopCnt = 0;
		
		for(int i = 0; i < n; i++) {
			Set<Integer> sumsTemp = new HashSet<>();
			for(int sum : sums) {
				sumsTemp.add(sum + numbers[i]);
				loopCnt++;
			}
			sums.addAll(sumsTemp);
		}
		
		out.println(sums.size());
//		out.println(loopCnt);
		
	}
	void solve2(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int[] numbers = new int[n];
		for(int i = 0; i < n; i++) {
			numbers[i] = in.nextInt();
		}
		
		final int MAX = 500 * 100;
		
		boolean[] sums = new boolean[MAX + 1];
		sums[0] = true;
		
		int loopCnt = 0;
		
		for(int i = 0; i < n; i++) {
			for(int sum = MAX; sum >= 0; sum--) {
				loopCnt++;
				if((sum - numbers[i] >= 0) && sums[sum - numbers[i]]) {
					sums[sum] = true;
				}
			}
		}
		
		int cnt = 0;
		for(boolean s : sums) {
			loopCnt++;
			if(s) {
				cnt++;
			}
		}
		
		out.println(cnt);
		out.println(loopCnt);
		
	}
	
	public static void main(String[] args) {
		new Sums().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
//			solve2(in, out);
		}
	}

}
