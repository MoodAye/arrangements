package com.moodaye.playground.programmerSchool.November2019;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

// Problem 62 : Leetcode
// start 928am - end 950am ~ 22min
public class UniquePaths {
	void solve(Scanner in, PrintWriter out) {
		int m = in.nextInt();
		int n = in.nextInt();
		out.println(uniquePaths(m, n));
	}
	
	public int uniquePaths(int m, int n) {
		int[][] grid = new int[m + 2][n + 2];
		grid[1][1] = 1;
		
		for(int col = 1; col <= n; col++) {
			for(int row = 1; row <= m; row++) {
				if(row == 1 && col == 1) {
					continue;
				}
				grid[row][col] += grid[row - 1][col] + grid[row][col - 1];
			}
		}
		
		return grid[m][n];
	}

	public static void main(String[] args) {
		new UniquePaths().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
