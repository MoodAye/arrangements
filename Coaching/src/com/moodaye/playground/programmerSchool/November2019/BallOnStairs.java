package com.moodaye.playground.programmerSchool.November2019;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

// Problem 544 
// #544
// Complexity = O(n).  
// Space = O(n) 
// Time taken ~15min
public class BallOnStairs {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		if(n == 1) {
			out.println(1);
			return;
		}
		else if(n == 2	) {
			out.println(2);
			return;
		}
		else if(n == 3) {
			out.println(4);
			return;
		}
		
		long[] cnt = new long[n + 1]; 
		cnt[n] = 1;
		cnt[n - 1] = 1;
		cnt[n - 2] = 2;
		cnt[n - 3] = 4;
		
		for(int step = n - 4; step >= 0; step--) {
			cnt[step] += cnt[step + 1] + cnt[step + 2] + cnt[step + 3];
		}
		
		out.println(cnt[0]);
		out.println(cnt[0]);
	}

	public static void main(String[] args) {
		new BallOnStairs().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
