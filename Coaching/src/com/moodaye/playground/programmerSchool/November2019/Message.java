package com.moodaye.playground.programmerSchool.November2019;

import java.io.PrintWriter;
import java.math.BigInteger;
import java.util.Locale;
import java.util.Scanner;

// Problem 423
// 
public class Message {
	void solve(Scanner in, PrintWriter out) {
		String line = in.next();
		int n = line.length();
		int max = 33;

		int[] numbers = new int[n + 1];
		for (int i = 1; i <= n; i++) {
			numbers[i] = line.charAt(i - 1) - '0';
		}

		BigInteger[] cnt = new BigInteger[n + 1];
		cnt[0] = BigInteger.valueOf(1);

		for (int prefixSize = 1; prefixSize <= n; prefixSize++) {
			cnt[prefixSize] = cnt[prefixSize - 1];
			int temp = numbers[prefixSize];
			if (numbers[prefixSize - 1] == 0) {
				continue;
			}

			temp += numbers[prefixSize - 1] * 10;
			if (temp > max) {
				continue;
			}
			cnt[prefixSize] = cnt[prefixSize].add(cnt[prefixSize - 2]);
		}

		out.println(cnt[n].toString());
	}

	public static void main(String[] args) {
		new Message().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
