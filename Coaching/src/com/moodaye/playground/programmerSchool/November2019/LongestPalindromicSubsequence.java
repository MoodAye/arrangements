package com.moodaye.playground.programmerSchool.November2019;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

// Problem 516 - Leetcode
// Complexity = O(n); space = O(n)
public class LongestPalindromicSubsequence {

	void solve(Scanner in, PrintWriter out) {
		String str = in.next();
		out.println(longestPalindrome(str));
	}

	public String longestPalindrome(String str) {
		int len = str.length();
		if (len == 0) {
			return str;
		}

		boolean[][] isPal = new boolean[len][len];
		for (int i = 0; i < len; i++) {
			isPal[i][i] = true;
		}
		
		for (int i = 0; i < len - 1; i++) {
			isPal[i][i + 1] = str.charAt(i) == str.charAt(i + 1);
		}

		for (int plen = 3; plen <= len; plen++) {
			for (int i = 0; i + plen <= len; i++) {
				int last = i + plen - 1;
				isPal[i][last] = isPal[i + 1][last - 1] && str.charAt(i) == str.charAt(last);
			}
		}
		
		int maxLen = 1;
		int startIdx = 0;
		for(int plen = len; plen > 1; plen--) {
			for(int i = 0; i + plen  - 1 < len; i++) {
				int last = i + plen - 1;
				if(isPal[i][last]) {
					maxLen = plen;
					startIdx = i;
					break;
				}
			}
			if(maxLen > 1) {
				break;
			}
		}

		return str.substring(startIdx, startIdx + maxLen);
	}

	boolean prevCharSame(char[] cstr, int start, int adjLen) {
		for (int i = start - 1; i >= start - adjLen; i--) {
			if (cstr[i] != cstr[start]) {
				return false;
			}
		}
		return true;
	}

	public static void main(String[] args) {
		new LongestPalindromicSubsequence().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
