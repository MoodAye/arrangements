package com.moodaye.playground.programmerSchool.November2019;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

// Problem 536
public class Numbers2 {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int max = in.nextInt();
		int limit = in.nextInt();

		String line = in.next();
		int[] numbers = new int[n + 1];
		for (int i = 1; i <= n; i++) {
			numbers[i] = line.charAt(i - 1) - '0';
		}

		long mod = (long) Math.pow(10, limit);
		long[] cnt = new long[n + 1];
		int endIdx = 1;
		cnt[0] = 1;
		
		for (int prefixSize = 1; prefixSize <= n && numbers[prefixSize] <= max; prefixSize++) {
			int pow = 1;
			cnt[prefixSize] = cnt[prefixSize - 1];
			int temp = numbers[prefixSize];
			for(int i = prefixSize - 1; i >= endIdx && pow <= max; i--) {
				pow *= 10;
				
				if(numbers[i] == 0) {
					continue;
				}
				
				temp += numbers[i] *pow;
				if(temp > max) {
					endIdx = i;
					break;
				}
				cnt[prefixSize] = (cnt[prefixSize] + cnt[i - 1]) % mod;
			}
		}
		out.println(cnt[n]);
	}

	public static void main(String[] args) {
		new Numbers2().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
