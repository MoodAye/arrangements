package com.moodaye.playground.programmerSchool.November2019;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

// Problem #38 
// we calculate the outcomes of increasing sub-arrays 
// e.g., for n = 5 we do
// [0,1] [1,2] [2,3] [3,4] 
// [0,1,2] [1,2,3] [2,3,4]
// [0,1,2,3] [1,2,3,4]
// [0,1,2,3,4]

// for [0,1,2,3,4] - p1 chooses element [0] or [4]
// the problem reduces to [0],[1,2,3,4] or [4],[0,1,2,3]

// We start with the arrays of length = 2, and keep track of differences 
// in the p1 and p2 values as that is all that matters.

public class Game2 {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int[] values = new int[n];
		
		for (int i = 0; i < n; i++) {
			values[i] = in.nextInt();
		}
		
		int[] subValues = values.clone();
		
		// len of subarray
		for(int len = 2; len <= n; len++) {
			// i = starting index
			for(int i = 0; i + len - 1 < n; i++) {
				subValues[i] = Math.max(values[i] - subValues[i + 1], values[i + len - 1] - subValues[i]);
			}
		}
		
		if(subValues[0] > 0) {
			out.println(1);
		}
		else if (subValues[0] < 0){
			out.println(2);
		}
		else {
			out.println(0);
		}
	}

	public static void main(String[] args) {
		new Game2().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
