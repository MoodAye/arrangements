package com.moodaye.playground.programmerSchool.November2019;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

// Problem 476
public class QueenToTheCorner {
	void solve(Scanner in, PrintWriter out) {
		int qr = in.nextInt();
		int qc = in.nextInt();
		
		// dimensions of square
		int dim = Math.max(qr, qc);
		
		// if(winner) then player to lands on this square wins
		boolean[][] w = new boolean[dim + 1][dim + 1];

		// move from edge to edge coming to center
		for (int r = 1; r <= dim; r++) {
			for(int c = 1; c <= dim; c++) {
				if(w[r][c]) {
					continue;
				}
				markWinner(w, r, c);
			}
		}
		
		out.println(w[qr][qc] ? 1 : 2);
	}
	
	private void markWinner(boolean[][] w, int row, int col) {
		int dim = w[0].length;
		for(int r = row + 1; r < dim; r++) {
			w[r][col] = true;
		}
		for(int c = col + 1; c < dim; c++) {
			w[row][c] = true;
		}
		for(int diag = 1; row + diag < dim && col + diag < dim; diag++) {
			w[row + diag][col + diag] = true;
		}
	}

	public static void main(String[] args) {
		new QueenToTheCorner().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
