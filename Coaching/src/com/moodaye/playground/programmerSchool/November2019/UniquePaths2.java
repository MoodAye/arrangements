package com.moodaye.playground.programmerSchool.November2019;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

// Problem 63 Leetcode
// Start 9am
public class UniquePaths2 {
	void solve(Scanner in, PrintWriter out) {
		int rows = in.nextInt();
		int cols = in.nextInt();
		int[][] grid = new int[rows][cols];

		for (int r = 0; r < rows; r++) {
			for (int c = 0; c < cols; c++) {
				grid[r][c] = in.nextInt();
			}
		}
		out.println(uniquePathsWithObstacles2(grid));
		out.println(uniquePathsWithObstacles(grid));
	}
	
	// time complexity = O(rc)
	// space complexity = O(c)
	// We could make this O(min(r,c)) ... but would need additional logic to determine whether to parse by col or row
	public int uniquePathsWithObstacles2(int[][] obstacleGrid) {
		int cols = obstacleGrid[0].length;
		int rows = obstacleGrid.length;

		int[] cnt = new int[cols];
		cnt[0] = obstacleGrid[0][0] == 1 ? 0 : 1;

		for (int r = 0; r < rows; r++) {
			for (int c = 0; c < cols; c++) {
				if(obstacleGrid[r][c] == 1) {
					cnt[c] = 0;
					continue;
				}
				if(c != 0) {
					cnt[c] += cnt[c - 1];
				}
			}
		}
		return cnt[cols - 1];
	}

	// time complexity = O(rc);
	// space complexity = O(1); (reuse the grid that is passed as an argument)
	public int uniquePathsWithObstacles(int[][] obstacleGrid) {
		int cols = obstacleGrid[0].length;
		int rows = obstacleGrid.length;

		for (int c = 0; c < cols; c++) {
			for (int r = 0; r < rows; r++) {
				if (obstacleGrid[r][c] == 1) {
					obstacleGrid[r][c] = 0;
					continue;
				}
				if (r == 0 && c == 0) {
					obstacleGrid[r][c] = 1;
					continue;
				}
				if (r != 0) {
					obstacleGrid[r][c] += obstacleGrid[r - 1][c];
				}
				if (c != 0) {
					obstacleGrid[r][c] += obstacleGrid[r][c - 1];
				}
			}
		}
		return obstacleGrid[rows - 1][cols - 1];
	}

	public static void main(String[] args) {
		new UniquePaths2().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
