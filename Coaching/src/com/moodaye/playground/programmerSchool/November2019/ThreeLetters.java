package com.moodaye.playground.programmerSchool.November2019;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

// Problem 960
public class ThreeLetters {
	void solve(Scanner in, PrintWriter out) {
		String str = null;

		if (in.hasNext()) {
			str = in.next();
		}

		if (str == null) {
			out.println(0);
			return;
		}
		char[] s = str.toCharArray();
		if (s == null) {
			out.println(0);
			return;
		}

		long a = 0L;
		long ab = 0L;
		long abc = 0L;

		for (int i = 0; i < s.length; i++) {
			if (s[i] == 'c') {
				abc += ab;
			} else if (s[i] == 'b') {
				ab += a;
			} else if (s[i] == 'a') {
				a++;
			}
		}
		out.println(abc);
	}

	public static void main(String[] args) {
		new ThreeLetters().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
