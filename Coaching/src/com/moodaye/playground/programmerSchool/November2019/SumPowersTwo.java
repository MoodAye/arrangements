package com.moodaye.playground.programmerSchool.November2019;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

// Problem 525 
public class SumPowersTwo {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int[] sum = new int[n + 1];

		sum[1] = 1;
		sum[2] = 2;

		for (int i = 3; i <= n; i++) {
			if (i % 2 == 1) {
				sum[i] = sum[i - 1];
			} else {
				sum[i] = sum[i - 1] + sum[i / 2];
			}
		}
		out.println(sum[n]);
	}

	public static void main(String[] args) {
		new SumPowersTwo().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
