package com.moodaye.playground.programmerSchool.November2019;

import java.io.PrintWriter;
import java.math.BigInteger;
import java.util.Locale;
import java.util.Scanner;

// Problem 179
// Pattern = (previousSum - 2) * 3 + 4
public class Sequence {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		
		if(n == 0) {
			out.println(2);
			return;
		}
		if(n == 1) {
			out.println(4);
			return;
		}
		
		
		BigInteger four = BigInteger.valueOf(4);
		BigInteger sum = four;
		BigInteger three = BigInteger.valueOf(3);
		for(int i = 2; i <= n; i++) {
			sum = sum.subtract(BigInteger.TWO).multiply(three).add(four);
		}
		out.println(sum);
	}

	public static void main(String[] args) {
		new Sequence().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
