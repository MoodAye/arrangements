package com.moodaye.playground.programmerSchool;
import static com.moodaye.playground.programmerSchool.FibonacciAgain.*;

import static org.junit.Assert.*;

import org.junit.Test;

public class FibonacciAgainTest {

	@Test
	public void test() {
		assertEquals(1, lastDigitCyclePattern()[0 % 60]);
		assertEquals(1, lastDigitCyclePattern()[1 % 60]);
		assertEquals(2, lastDigitCyclePattern()[2 % 60]);
		assertEquals(3, lastDigitCyclePattern()[3 % 60]);
		assertEquals(5, lastDigitCyclePattern()[4 % 60]);
		assertEquals(8, lastDigitCyclePattern()[5 % 60]);
		assertEquals(3, lastDigitCyclePattern()[6 % 60]);
		assertEquals(1, lastDigitCyclePattern()[7 % 60]);
		assertEquals(1, lastDigitCyclePattern()[21 % 60]);
		assertEquals(7, lastDigitCyclePattern()[22 % 60]);
		assertEquals(3, lastDigitCyclePattern()[25 % 60]);
		assertEquals(7, lastDigitCyclePattern()[33 % 60]);
		assertEquals(1, lastDigitCyclePattern()[300 % 60]);
		assertEquals(9, lastDigitCyclePattern()[91 % 60]);
	}

	@Test
	public void testSimple() {
		assertEquals(1, simpleLastDigit(0));
		assertEquals(1, simpleLastDigit(1));
		assertEquals(2, simpleLastDigit(2));
		assertEquals(3, simpleLastDigit(3));
		assertEquals(5, simpleLastDigit(4));
		assertEquals(8, simpleLastDigit(5));
		assertEquals(3, simpleLastDigit(6));
		assertEquals(1, simpleLastDigit(7));
		assertEquals(1, simpleLastDigit(21));
		assertEquals(7, simpleLastDigit(22));
		assertEquals(3, simpleLastDigit(25));
		assertEquals(7, simpleLastDigit(33));
		assertEquals(1, simpleLastDigit(300));
		assertEquals(9, simpleLastDigit(91));
	}
}
