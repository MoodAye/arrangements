package com.moodaye.playground.programmerSchool;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

/** Programmer's School Problem 554 */
public class Pizza {
	
	void solve(Scanner in, PrintWriter out){
		out.println(fib(in.nextInt()));
	}

	// not sure of the how to prove this formula 
	// but it works
	public static int fib(int n){
		if (n == 1) return 2;
		else return n + fib(n-1);
	}
	
	
	public static void main(String[] args) {
		new Pizza().run();
	}
	
	void run(){
		Locale.setDefault(Locale.US);
		try(Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)){
			solve(in, out);
		}
	}
}
