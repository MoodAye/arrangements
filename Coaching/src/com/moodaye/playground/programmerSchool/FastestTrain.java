package com.moodaye.playground.programmerSchool;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;
import java.util.regex.Pattern;
import java.util.regex.Matcher;


/** Programmer's School - #89 */
public class FastestTrain {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		in.nextLine();
		Train fastest = null;
		for(int i = 0; i < n; i++){
			String line = in.nextLine();
			Train train = new Train(line);
			if(fastest == null){
				fastest = train;
			}
			else{
				fastest = fastest.compareTo(train);
			}
		}
		out.println("The fastest train is " + fastest.name + ".");
		out.println("Its speed is " + fastest.speed() + " km/h, approximately.");
	}
	
	public static void main(String[] args) {
		new FastestTrain().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}

}

class Train{
	String name;
	int startH;
	int startM;
	int endH;
	int endM;
	
	/** duration in minutes */
	int duration;
	
	Train(String input){
		Pattern p = Pattern.compile("(\".*\")\\s.*([0-9][0-9]:[0-9][0-9])\\s.*([0-9][0-9]:[0-9][0-9])");
		Matcher m = p.matcher(input);
		m.find();
		String name = m.group(1);
		String start = m.group(2);
		String end = m.group(3);
		this.name = name;
		parseTimes(start, end);
		calculateDuration();
	}
	
	Train(String name, String start, String end){
		this.name = name;
		parseTimes(start,end);
		calculateDuration();
	}
	
	Train(String name, int startH, int startM, int endH, int endM){
		this.name = name;
		this.startH = startH;
		this.startM = startM;
		this.endH = endH;
		this.endM = endM;
		calculateDuration();
	}
	
	private void parseTimes(String start, String end){
		startH = Integer.parseInt(start.substring(0,2));
		startM = Integer.parseInt(start.substring(3));
		endH = Integer.parseInt(end.substring(0,2));
		endM = Integer.parseInt(end.substring(3));
	}
	
	private void calculateDuration(){
		if(endH == startH && endM <= startM){
			duration = 24*60 + (endM - startM); 
		}
		else if (endH < startH){
			duration = ((24 + endH - startH) * 60) + (endM - startM);
		}
		else{
			duration = ((endH - startH) * 60) + (endM - startM);
		}
	}
	
	public int speed(){
		return (((650 * 60) * 10  / duration) + 5)/10;
	}
	
	public Train compareTo(Train t){
		return duration < t.duration ? this : t;
	}
	
	public String toString(){
		return name;
	}
}