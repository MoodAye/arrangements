package com.moodaye.playground.programmerSchool;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

/** Programmer School - Problem #94 */
public class PrinceAndDragon {
	void solve(Scanner in, PrintWriter out) {
		int s = in.nextInt();
		int d = in.nextInt();
		int r = in.nextInt();
		out.println(numberOfBlows(s, d, r));
	}
	
	public static String numberOfBlows(int swordHeads, int dragonHeads, int regeneratedHeads){
		String blows = "NO";
		int afterOneBlow = dragonHeads - swordHeads;
		int eachBlow = swordHeads - regeneratedHeads;
		if (afterOneBlow <= 0){ 
			return "1";
		}
		else if ( eachBlow <= 0){
			return "NO";
		}
		else{
			return Integer.toString(((afterOneBlow + eachBlow -1)/ eachBlow) + 1);
		}
	}

	public static void main(String[] args) {
		new PrinceAndDragon().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
