package com.moodaye.playground.programmerSchool.January2018;

import static org.junit.Assert.*;

import org.junit.Test;
import static com.moodaye.playground.programmerSchool.January2018.StringExponentiation.*;
public class StringExponentiationTest {

	@Test
	public void test() {
		assertEquals("abcabcabc", root("abc", 3));
		assertEquals("abcd", root("abcdabcd", -2));
		assertEquals("NO SOLUTION", root("abcd", -4));
		
		assertEquals("aaaaaaaaaa", root("a", 10));
		assertEquals("a", root("aaaaaaaaaa", -10));
		assertEquals("a", root("a", -1));
		assertEquals("a", root("a", 1));
		
		assertEquals("NO SOLUTION", root("a", -2));
		assertEquals("NO SOLUTION", root("aaa", -2));
		assertEquals("aaa", root("aaa", -1));
	}

}
