package com.moodaye.playground.programmerSchool.January2018;

import static org.junit.Assert.*;

import org.junit.Test;
import static com.moodaye.playground.programmerSchool.January2018.TransformationOfMonocellular.*;
public class TransformationOfMonocellularTest {

	@Test
	public void test() {
		assertEquals(3, steps(2,36));
		assertEquals(16, steps(32768,3));
		assertEquals(0, steps(1434,1434));
		assertEquals(1, steps(14,28));
	}
	
	@Test
	public void testHcd(){
		assertEquals(4, hcd(8,28));
		assertEquals(1, hcd(8,13));
		assertEquals(1, hcd(18,13));
		assertEquals(4, hcd(28,8));
	}
	
	@Test
	public void testPrimeFactors(){
		assertEquals(2, primeFactors(4).size());
		assertEquals(4, primeFactors(36).size());
		assertEquals(1, primeFactors(17).size());
	}
}
