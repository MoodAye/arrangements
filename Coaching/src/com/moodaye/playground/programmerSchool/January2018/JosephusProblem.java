package com.moodaye.playground.programmerSchool.January2018;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #118 */
// Complexity is O(n^2) when K = N - which is the worst case.
// Since N <= 500; we are ok here.  
// Eg., if N = 10_000; then N^2 = 100_000_000
// and time taken is more than 1 second.

public class JosephusProblem {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int k = in.nextInt();
		boolean[] isDead = new boolean[n];
		int i = -1;
		int cnt = 0;
		while (!allDead(isDead)) {
			while (cnt != k) {
				i++;
				i = i == n ? 0 : i;
				cnt += isDead[i] ? 0 : 1;
			}
			cnt = 0;
			isDead[i] = true;
		}
		out.println(i + 1);
	}

	public static boolean allDead(boolean[] isDead) {
		boolean anyAlive = true;
		for (int i = 0; i < isDead.length; i++) {
			if (!isDead[i]) {
				anyAlive = false;
				break;
			}
		}
		return anyAlive;
	}

	public static void main(String args[]) {
		new JosephusProblem().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
