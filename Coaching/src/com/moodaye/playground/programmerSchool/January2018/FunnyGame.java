package com.moodaye.playground.programmerSchool.January2018;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #298 */
public class FunnyGame {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		out.println(rolln(n));
	}

	public static int rolln(int n) {
		if (n == 0) {
			return 0;
		}
		int leftMostDigit = 1 << 30;
		while ((leftMostDigit & n) == 0) {
			leftMostDigit >>= 1;
		}
		int nextInt = n;
		int max = 0;
		while (true) {
			int lastDigit = nextInt & 1;
			nextInt >>= 1;
			if (lastDigit == 1) {
				nextInt |= leftMostDigit;
			}
			max = Math.max(nextInt, max);
			if (nextInt == n) {
				break;
			}
		}
		return max;
	}

	public static void main(String args[]) {
		new FunnyGame().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
