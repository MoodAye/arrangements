package com.moodaye.playground.programmerSchool.January2018;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #551 */
// Failing on test 16
public class HareAndTree {
	void solve(Scanner in, PrintWriter out) {
		int R = in.nextInt();
		int r = in.nextInt();
		int h = in.nextInt();
		int b = in.nextInt();
		
		int distanceToFence = R + b - r;
		// second term checks width of tree
		if(distanceToFence > h && (R * R > (r * r) + (r - b) * (r - b))){
			out.println("YES");
		}
		else{
			out.println("NO");
		}
	}

	public static void main(String args[]) {
		new HareAndTree().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
