package com.moodaye.playground.programmerSchool.January2018.set4;

import java.util.Scanner;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.util.Locale;

/** Programmer's School #85 */
public class UnitaryGCD {
	void solve(Scanner in, PrintWriter out) {
		int aIn = in.nextInt();
		int bIn = in.nextInt();
		StringBuilder aSb = new StringBuilder();
		StringBuilder bSb = new StringBuilder();
		
		for(int i = 0; i < aIn; i++){
			aSb.append("1");
		}
		for(int i = 0; i < bIn; i++){
			bSb.append("1");
		}
		
		BigInteger a = new BigInteger(aSb.toString());
		BigInteger b = new BigInteger(bSb.toString());
		BigInteger zero = new BigInteger("0");
		while(!b.equals(zero)){
			BigInteger rem = a.mod(b);
			a = b;
			b = rem;
		}
		out.println(a);
	}

	public static void main(String[] args) {
		new UnitaryGCD().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}

}
