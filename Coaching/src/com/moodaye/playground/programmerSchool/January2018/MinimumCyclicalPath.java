package com.moodaye.playground.programmerSchool.January2018;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #126 */
/* We'll use a 2 dimensional matrix 
 * Space = 100 x 100 = 10_000 ints. Ok here. 
 * Complexity = O(n^3) ~ (100^3 = 1_000_000 < 10^8)
 * Though not needed to solve within the constraints of the problem
 *  we limit the loops to i < n - 2; j < n - 1 and j = i + 1 and k = j + 1.
 *  We can do this because this is an undirected graph.
 *  
 *  Since we use the int[][] matrix finding a path between 2 squares is O(1), I don't think
 *  any of the basic graph algorithms would make this solution any more efficient.
 */
public class MinimumCyclicalPath {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int[][] adj = new int[n][n];

		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				adj[i][j] = in.nextInt();
			}
		}
		int minPath = Integer.MAX_VALUE;
		for (int i = 0; i < n - 2; i++) {
			for (int j = i + 1; j < n - 1; j++) {
				if (j == i) {
					continue;
				}
				for (int k = j + 1; k < n; k++) {
					if (k == i || k == j) {
						continue;
					}
					int path = 0;
					path += adj[i][j];
					path += adj[j][k];
					path += adj[k][i];
					minPath = Math.min(path, minPath);
				}
			}
		}
		out.println(minPath);
	}

	public static void main(String args[]) {
		new MinimumCyclicalPath().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
