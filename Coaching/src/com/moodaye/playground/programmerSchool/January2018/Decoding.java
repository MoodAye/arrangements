package com.moodaye.playground.programmerSchool.January2018;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #73 */
// Failing on test 5.  Was returning using decoded.toString which 
// won't work because decoded is an array. Using String.valueOf() instead.
public class Decoding {
	void solve(Scanner in, PrintWriter out) {
		String code = in.next();
		out.println(decode(code));
	}
	
	public static String decode(String sCode){
		char[] code = sCode.toCharArray();
		char[] decoded = new char[code.length];
		for (int i = 0; i < code.length; i++) {
			int p = norm27(code[i]);
			int rmod = reverseMod(i + 1, p, 27);
			char c = rmod == 0 ? ' ' : (char) ('a' + (rmod - 1));
			decoded[i] = c;
		}
		return String.valueOf(decoded);
	}
	
	public static int norm27(char c) {
		int p;
		if (c <= '9') {
			p = c - '0';
		} else {
			p = c - 'A' + 10;
		}
		return p;
	}

	/* For equation (n + x) % m = p; given n,m,p - find x */
	public static int reverseMod(int n, int p, int m) {
		int factor = p + m * (n / m);
		factor = factor < n ? factor + m : factor;
		return factor - n;
	}

	public static void main(String args[]) {
		new Decoding().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
