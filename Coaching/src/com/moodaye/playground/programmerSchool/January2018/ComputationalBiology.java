package com.moodaye.playground.programmerSchool.January2018;

import java.util.Scanner;
import java.io.FileInputStream;
import java.io.PrintWriter;
import java.util.Locale;

/** Programmer's School #278 */
// Max size of input is 256,000 characters (1 byte per character).
// Brute force solution has complexity of O(n).
//
// Failing on test 13 with "Time Limit Exceeded".
// -Fixed this by plugging in the appropriate break so that solution is O(n).
//
// Now failing on test 35. Simulated failure using test case "aaa" "a". Fixed.
// Now passing all test cases. There might be an opportunity to clean up the
// code-
// it seems like there are a lot of "if" statements.
//
public class ComputationalBiology {

	void solve(Scanner in, PrintWriter out) {

		char[] s = in.next().toCharArray();
		char[] t = in.next().toCharArray();
		out.println(singleLoop(s, t));
	}

	public static String doubleLoop(char[] s, char[] t) {

		int nextIdx = 0;
		boolean evolved = true;
		for (int i = 0; i < s.length; i++) {
			if (nextIdx == t.length) {
				evolved = false;
				break;
			}
			for (int j = nextIdx; j < t.length; j++) {
				if (s[i] == t[j]) {
					nextIdx = j + 1;
					break;
				} else if (j == t.length - 1) {
					evolved = false;
					break;
				}
			}
			if (!evolved) {
				break;
			}
		}

		return evolved ? "YES" : "NO";
	}

	public String singleLoop(char[] s, char[] t) {
		int sPos = 0;
		for(char c : t){
			if (s[sPos] != c) {
				continue;
			}
			sPos++;
			if (sPos == s.length) {
				return "YES";
			}
		}
		return "NO";
	}

	public static void main(String[] args) {
		new ComputationalBiology().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}

}
