package com.moodaye.playground.programmerSchool.January2018;

import static org.junit.Assert.*;

import org.junit.Test;
import static com.moodaye.playground.programmerSchool.January2018.Decoding.*;

public class DecodingTest {

	@Test
	public void testReverseMod() {
		assertEquals(10, reverseMod(24, 7, 27));
		assertEquals(1, reverseMod(1, 2, 27));
	}
	
	@Test
	public void testDecode(){
		assertEquals("a b", decode("225"));
	}
}
