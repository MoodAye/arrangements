package com.moodaye.playground.programmerSchool.January2018;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #70 */
/*
 * Failing on test 8. Fixed by checking for s.length % k == 0 Now failing on
 * test 11. Noticed that i need to code for answer length > 1023. Now getting
 * memory limit exceeded on test 12. Max String length ~ 16Mb * 10^6 / 2 (2
 * bytes per char). 8_000_000 Input max = 1_000 * 100_000 = 100_000_000
 */
public class StringExponentiation {
	void solve(Scanner in, PrintWriter out) {
		String s = in.next();
		int k = in.nextInt();
		out.println(root(s, k));
	}

	public static String root(String s, int k) {
		if (k > 0) {
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < k; i++) {
				sb.append(s);
				if (sb.length() > 1023) {
					return sb.substring(0, 1023);
				}
			}
			return sb.toString();
		} else {
			k *= -1;
			if (s.length() % k != 0) {
				return "NO SOLUTION";
			}
			int div = s.length() / k;
			String rep = s.substring(0, div);
			for (int i = 1; i < k; i++) {
				int end = Math.min((i + 1) * div, s.length());
				String sub = s.substring(i * div, end);
				if (!rep.equals(sub)) {
					return "NO SOLUTION";
				}
			}
			if (rep.length() > 1023) {
				rep = rep.substring(0, 1023);
			}
			return rep;
		}
	}

	public static void main(String args[]) {
		new StringExponentiation().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
