package com.moodaye.playground.programmerSchool.January2018;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #98 */
/*  Input has max 10_000 integers - java.util.Scanner can handle 80_000 per second.
 *  So we have enough time for input.
 *  
 *  The solution - brute force - has O(n) complexity. 
 *  Therefore, 1second should be sufficient.
 *  
 *  Max of sum = 10_000 / 2 * 1_000 = 5_000_000.  Ints are fine
 * 
 */
public class NumbersGame {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int[] a = new int[n];
		for(int i = 0; i < n; i++){
			a[i] = in.nextInt();
		}
		int firstSum = 0;
		int secondSum = 0;
		int left= 0;
		int right= n - 1;
		boolean firstUp = true;
		int amt = 0;
		while(left <= right){
			if(a[left] >= a[right]){
				amt = a[left++];
			}
			else{
				amt = a[right--];
			}
			if(firstUp){
				firstSum += amt;
			}
			else{
				secondSum += amt;
			}
			firstUp = !firstUp;
		}
		out.println(firstSum + ":" + secondSum);
	}

	public static void main(String args[]) {
		new NumbersGame().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
