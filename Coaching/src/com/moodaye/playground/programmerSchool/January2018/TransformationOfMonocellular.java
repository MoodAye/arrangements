package com.moodaye.playground.programmerSchool.January2018;
import java.util.Scanner;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/** Programmer's School #651 */
/*Solution will find the HCD of the 2 numbers.  
 * Then find the prime factors required to multiply the HCD to reach the
 * 2 numbers. The number of factors (steps) is the answer.
 * E.g,. N = 8, M = 28, HCD = 4.  8/4 = 2.    28 / 4 = 7
 *  8 / 2 * 7 = 28.  Number of steps = 2
 *  
 *  Getting MemoryLimitExceeded on test 12
 *  Refactored - not collecting primes in List<Integer>.  Simply counting.
 *  Does not help.
 *  
 *  Space requirements: boolean[] isPrime has max size of 10^9.  Bytes = 10^9 / 8 ~10^8 bytes ~ 100Mb.
 *  Optimized size of isPrime - reduced it to sqrt(m).  
 */
public class TransformationOfMonocellular {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int m = in.nextInt();
		out.println(steps(m, n));
	}
	
	public static int steps(int m, int n){
		int hcd = hcd(m, n);
//		int cnt = primeFactors(m / hcd).size();
//		cnt += primeFactors(n / hcd).size();
		return getCountOfPrimeFactors(m / hcd) + getCountOfPrimeFactors(n / hcd);
	}
	
	public static int getCountOfPrimeFactors(int m){
		int cnt = 0;
		boolean[] isPrime = new boolean[(int) Math.sqrt(m) + 1];
		for(int i = 2; i < isPrime.length; i++){
			isPrime[i] = true;
		}
		for(int i = 2; i < isPrime.length; i++){
			if(!isPrime[i]){
				continue;
			}
			markNonPrimes(isPrime, i);
			while ( m % i == 0){
				cnt++;
				m /= i;
			}
		}
		if(m != 1){
			cnt++;
		}
		return cnt;
	}
	
	public static List<Integer> primeFactors(int m){
		List<Integer> factors = new ArrayList<>();
		boolean[] isPrime = new boolean[(int) Math.sqrt(m) + 1];
		for(int i = 2; i < isPrime.length; i++){
			isPrime[i] = true;
		}
		for(int i = 2; i < isPrime.length; i++){
			if(!isPrime[i]){
				continue;
			}
			markNonPrimes(isPrime, i);
			while ( m % i == 0){
				factors.add(i);
				m /= i;
			}
		}
		if(m != 1){
			factors.add(m);
		}
		return factors;
	}
	
	public static void markNonPrimes(boolean[] isPrime, int p){
		for(int i = 2; i * p < isPrime.length; i++){
			isPrime[i * p] = false;
		}
	}
	
	public static int hcd(int m, int n){
		int rem = -1;
		while(rem != 0){
			rem = m % n;
			m = n;
			n = rem;
		}
		return m;
	}

	public static void main(String[] args) {
		new TransformationOfMonocellular().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}


}
