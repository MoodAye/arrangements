package com.moodaye.playground.programmerSchool.January2018;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #295 */
// This is failing on test 5
// Realized that 
// 1. i have not accounted for the rolling where  where Z rolls to A and such.
// 2. Not sure if the requirement is to chose the min shift or the absolute min shift. Seems reasonable that
// we need to chose the absolute min shift and not min shift.  
public class EncryptedMessage {
	void solve(Scanner in, PrintWriter out) {
		char[] encrypted = in.next().toCharArray();
		char[] decrypted = in.next().toCharArray();

		int diff = 0;
		int minDiff = Integer.MAX_VALUE;
		boolean deciphered = false;
		for (int i = 0; i < encrypted.length; i++) {
			diff = decrypted[0] - encrypted[i];
			deciphered = true;
			for (int j = 1; j < decrypted.length; j++) {
				if (decrypted.length + i > encrypted.length || diff != decrypted[j] - encrypted[i + j]) {
					deciphered = false;
					break;
				}
			}
			if (deciphered == true) {
				minDiff = Math.min(minDiff, diff);
				break;
			}
		}
	
		if(deciphered){
			for(int i = 0; i < encrypted.length; i++){
				out.print((char) (encrypted[i] + minDiff));
			}
		}
		else{
			out.println("IMPOSSIBLE");
		}
	}

	public static void main(String args[]) {
		new EncryptedMessage().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}

}
