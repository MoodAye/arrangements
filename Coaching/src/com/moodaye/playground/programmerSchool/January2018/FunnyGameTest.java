package com.moodaye.playground.programmerSchool.January2018;

import static org.junit.Assert.*;

import org.junit.Test;
import static com.moodaye.playground.programmerSchool.January2018.FunnyGame.*;

public class FunnyGameTest {

	@Test
	public void test() {
		long start = System.currentTimeMillis();
		for (int k = 0; k < 10_000; k++) {
			for (int i = 1; i < 32_767; i++) {
				rolln(i);
			}
		}
		long end = System.currentTimeMillis();
		System.out.println((end - start) / 1000);
	}

}
