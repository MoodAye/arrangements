package com.moodaye.playground.programmerSchool;

import static org.junit.Assert.*;

import org.junit.Test;

public class FoxAndCatTest {

	@Test
	public void test2() {
		assertEquals("1 1", FoxAndCat.minCoins2(8));
		assertEquals("1 2", FoxAndCat.minCoins2(11));
		assertEquals("3 0", FoxAndCat.minCoins2(15));
		assertEquals("21 1", FoxAndCat.minCoins2(108));
		assertEquals("202 4", FoxAndCat.minCoins2(1022));
		assertEquals("198 3", FoxAndCat.minCoins2(999));
	}

	@Test
	public void test3() {
		assertEquals("1 1", FoxAndCat.minCoins3(8));
		assertEquals("1 2", FoxAndCat.minCoins3(11));
		assertEquals("3 0", FoxAndCat.minCoins3(15));
		assertEquals("21 1", FoxAndCat.minCoins3(108));
		assertEquals("202 4", FoxAndCat.minCoins3(1022));
		assertEquals("198 3", FoxAndCat.minCoins3(999));
	}
}
