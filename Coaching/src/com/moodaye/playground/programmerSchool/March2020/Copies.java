package com.moodaye.playground.programmerSchool.March2020;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

// Problem 
public class Copies {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int x = in.nextInt();
		int y = in.nextInt();
		
		int time = Math.min(x, y); // first copy
		time += ((n - 1) / (x + y)) * x * y;
		int rem = (n - 1) % (x + y);
		
		int addedTime = 0;
		while(rem > 0) {
			addedTime++;
			if(addedTime % x == 0) {
				rem--;
			}
			if(addedTime % y == 0) {
				rem--;
			}
		}
		out.println(time + addedTime);
	}

	public static void main(String[] args) {
		new Copies().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
