package com.moodaye.playground.programmerSchool.March2020;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/* Edge Cases 
 * 1. Line is vertical (making x1 = x2 - div by zero).
 * 2. Line is horizontal (ok) 
 * 3. Lines are parallel (ma = mb) (ok)
 * 4. Lines do not intersect (ok)
 * 5. Intersection is at one of the ends of the line segment 
 * */
public class LSIntersection_CCI {
	void solve(Scanner in, PrintWriter out) {
		int ax1 = in.nextInt();
		int ay1 = in.nextInt();
		int ax2 = in.nextInt();
		int ay2 = in.nextInt();
		int bx1 = in.nextInt();
		int by1 = in.nextInt();
		int bx2 = in.nextInt();
		int by2 = in.nextInt();

		double ma = 0;
		double mb = 0;
		double ca = 0;
		double cb = 0;

		if (ax1 != ax2) {
			ma = (ay1 - ay2) / (ax1 - ax2);
			ca = ay1 - (ax1 * ma);
		}
		if (bx1 != bx2) {
			mb = (by1 - by2) / (bx1 - bx2);
			cb = by1 - (bx1 * mb);
		}

		if (((ax1 == ax2) && (bx1 == bx2)) || ma == mb) {
			out.println("no intersect - segments are parallel");
			return;
		}
		double px = 0;
		double py = 0;

		if (!((ax1 == ax2) || (bx1 == bx2))) {

			px = -(cb - ca) / (mb - ma);
			py = ma * px + ca;

			if ((inBetween(px, ax1, ax2)) && (inBetween(px, ay1, ay2)) && (inBetween(px, bx1, bx2))
					&& (inBetween(px, by1, by2))) {
				out.printf("%.5f, %.5f%n", px, py);
			} else {
				out.println("no intersect");
			}
		} else {
			if (ax1 == ax2) {
				px = ax1;
				py = mb * px + cb;
			} else {
				px = bx1;
				py = ma * px + ca;
			}
			if ((inBetween(px, ax1, ax2)) && (inBetween(px, ay1, ay2)) && (inBetween(px, bx1, bx2))
					&& (inBetween(px, by1, by2))) {
				out.printf("%.5f, %.5f%n", px, py);
			} else {
				out.println("no intersect");
				out.printf("%.5f, %.5f%n", px, py);
			}
		}
	}

	static boolean inBetween(double p, double a1, double a2) {
		double min = a1;
		double max = a2;

		if (a1 > a2) {
			min = a2;
			max = a1;
		}

		return (min <= p && p <= max);
	}

	public static void main(String args[]) {
		new LSIntersection_CCI().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}

}
