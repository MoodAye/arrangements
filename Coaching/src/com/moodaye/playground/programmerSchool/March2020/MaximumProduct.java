package com.moodaye.playground.programmerSchool.March2020;

import java.io.InputStream;
import java.io.PrintWriter;
import java.util.Locale;


// Problem 224
// TLE - test 48
// Still TLE on test 48
public class MaximumProduct {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int[] values = new int[30_000 * 2 + 1];
		
		// max input ops = 3_000_000
		for(int i = 0; i < n; i++) {
			int value = in.nextInt();
			values[value + 30_000]++;
		}
		long option1 = 1L;
		long option2 = 1L;
		int cnt = 0;
		
		// max ops = 60_001
		for(int i = 0; i < 30_000 * 2 + 1; i++) {
			int counter = values[i];
			while(counter-- > 0 && cnt < 2) {
				option1 *= (i - 30_000);
				cnt++;
			}
		}
		cnt = 0;
		// max ops = 60_001
		for(int i = 30_000 * 2; i >= 0; i--) {
			int counter = values[i];
			while(counter-- > 0 && cnt < 3) {
				option2 *= (i - 30_000);
				if(cnt == 0) {
					option1 *= (i - 30_000);
				}
				cnt++;
			}
		}
		
		out.println(Math.max(option1, option2));
	}

	public static void main(String[] args) {
		new MaximumProduct().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
	static class Scanner implements AutoCloseable {

	    InputStream is;
	    byte buffer[] = new byte[1 << 16];
	    int size = 0;
	    int pos = 0;

	    Scanner(InputStream is) {
	        this.is = is;
	    }

	    int nextChar() {
	        if (pos >= size) {
	            try {
	                size = is.read(buffer);
	            } catch (java.io.IOException e) {
	                throw new java.io.IOError(e);
	            }
	            pos = 0;
	            if (size == -1) {
	                return -1;
	            }
	        }
	        Assert.check(pos < size);
	        int c = buffer[pos] & 0xFF;
	        pos++;
	        return c;
	    }

	    int nextInt() {
	        int c = nextChar();
	        while (c == ' ' || c == '\r' || c == '\n' || c == '\t') {
	            c = nextChar();
	        }
	        if (c == '-') {
	            c = nextChar();
	            Assert.check('0' <= c && c <= '9');
	            int n = -(c - '0');
	            c = nextChar();
	            while ('0' <= c && c <= '9') {
	                int d = c - '0';
	                c = nextChar();
	                Assert.check(n > Integer.MIN_VALUE / 10 || n == Integer.MIN_VALUE / 10 && d <= -(Integer.MIN_VALUE % 10));
	                n = n * 10 - d;
	            }
	            return n;
	        } else {
	            Assert.check('0' <= c && c <= '9');
	            int n = c - '0';
	            c = nextChar();
	            while ('0' <= c && c <= '9') {
	                int d = c - '0';
	                c = nextChar();
	                Assert.check(n < Integer.MAX_VALUE / 10 || n == Integer.MAX_VALUE / 10 && d <= Integer.MAX_VALUE % 10);
	                n = n * 10 + d;
	            }
	            return n;
	        }
	    }

	    @Override
	    public void close() {
	    }
	}

	static class Assert {
	    static void check(boolean e) {
	        if (!e) {
	            throw new AssertionError();
	        }
	    }
	}
}
