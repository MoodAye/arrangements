package com.moodaye.playground.programmerSchool.March2020;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School 170 */

public class NumberDecomposition {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		// p*a + s = n; check for (n - s) % p == 0
		int p = 2;
		int s = 1;
		int max = 1;
		while(s < n){
			if((n - s) % p == 0){
				max = p;
			}
			s += p;
			p++;
		}
		out.println(max);
	}

	public static void main(String args[]) {
		new NumberDecomposition().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}

}
