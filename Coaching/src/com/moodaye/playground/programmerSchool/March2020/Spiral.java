package com.moodaye.playground.programmerSchool.March2020;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

// Problem #196
public class Spiral {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int[][] square = new int[n + 2][n + 2];
		for(int row = 1; row <= n; row++) {
			for(int col = 1; col <= n; col++) {
				square[row][col] = Integer.MAX_VALUE;
			}
		}
		int dx = 0;
		int dy = 1;
		// Rule: dy = dx; dx = (1-dy)%2
		int nextx = 1;
		int nexty = 0;
		int next = 1;
		while(true) {
			nextx += dx;
			nexty += dy;
			if(square[nextx][nexty] != Integer.MAX_VALUE) {
				// step back and turn
				nextx -= dx;
				nexty -= dy;
				dy = dx;
				dx = (1 - dy) % 2;
				nextx += dx;
				nexty += dy;
				if(square[nextx][nexty] != Integer.MAX_VALUE) {
					break;
				}
			}
			square[nextx][nexty] = next++;
		}
		
		for(int row = 1; row <= n; row++) {
			for(int col = 1; col <= n; col++) {
				out.printf("%d ", square[row][col]);
			}
			out.println();
		}
	}

	public static void main(String[] args) {
		new Spiral().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
