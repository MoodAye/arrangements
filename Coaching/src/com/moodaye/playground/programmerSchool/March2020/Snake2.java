package com.moodaye.playground.programmerSchool.March2020;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

// Problem 359 
public class Snake2 {
	void solve(Scanner in, PrintWriter out) {
		long n = in.nextLong();
		long squares = ((1 + n) * n) / 2;
		if(squares % 9 == 0) {
		    squares = squares / 9 * 10 - 1;
		}
		else { 
		    squares = squares / 9 * 10 + (squares % 9);
		}
		
		long skip = n / 2;
	
		long ans = 0L;
		
		if(skip % 9 == 0) {
			ans = squares - ((skip / 9 * 10) - 1);
		}
		else {
			ans = squares - ((skip / 9 * 10) + (skip % 9));
		}
		
		while(skip-- > 0) {
			squares--;
			if(squares % 10 == 0) {
				squares--;
			}
		}
		
		
		
//		printSnake(out, n);
		out.println(squares);
		out.println(ans);
	}
	private void printSnake(PrintWriter out, long n) {
		int next = 1;
		for(int i = 1; i <= n; i++) {
			int len = i;
			while(len-- > 0) {
				if(next % 10 == 0) {
					next++;
				}
				out.print(next++ + " ");
			}
			out.println();
		}
	}

	public static void main(String[] args) {
		new Snake2().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
