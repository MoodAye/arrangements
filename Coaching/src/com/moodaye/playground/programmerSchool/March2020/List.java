package com.moodaye.playground.programmerSchool.March2020;

import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Locale;
import java.util.Scanner;

// Problem #444
public class List {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int[] values = new int[n];
		for(int i = 0; i < n; i++) {
			values[i] = in.nextInt();
		}
		Arrays.sort(values);
		
		StringBuilder output = new StringBuilder();
		int startIdx = 0;
		for(int i = 1; i < n; i++) {
			if(values[i] == values[i - 1]) continue;
			if(values[i] - values[i - 1] != 1) {
				processSequence(values, startIdx, i - 1, output);
				startIdx = i;
			}
		}
		processSequence(values, startIdx, n - 1, output);
		out.println(output.toString());
	}
	
	private static void processSequence(int[] values, int startIdx, int endIdx, 
			StringBuilder output) {
		
		String everyDigit = String.valueOf(values[startIdx]);
		String dots = String.valueOf(values[startIdx]);
	
		for(int i = startIdx + 1; i <= endIdx; i++) {
			if(values[i] == values[i - 1]) continue;
			everyDigit += ", " + String.valueOf(values[i]);
		}
		
		if(endIdx != startIdx) {
			dots += ", ..., " + String.valueOf(values[endIdx]);
		}
		
		if(everyDigit.length() <= dots.length()) {
			output.append(everyDigit);
		}
		else {
			output.append(dots);
		}
	}
	

	public static void main(String[] args) {
		new List().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
