package com.moodaye.playground.programmerSchool._202010;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

// Problem 203 1141pm - 1149
// Tests
// 1. abcde deabc 2
// 2. a a = 0
public class TextShift {
	void solve(Scanner in, PrintWriter out) {
		String s1 = in.next();
		String s2 = in.next();
		out.print((s2 + s2).indexOf(s1));
	}

	public static void main(String[] args) {
		new TextShift().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
