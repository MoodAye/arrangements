package com.moodaye.playground.programmerSchool._202010;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

// Problem 
public class StairsAgain {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int[] nums = new int[n + 1];
		for (int i = 1; i <= n; i++) {
			nums[i] = in.nextInt();
		}
		
		int[] steps = new int[n + 1];
		steps[1] = 0;
		
		for(int i = 2; i <= n; i++) {
			if(nums[i - 1] > nums[i - 2]) {
				steps[i] = i - 1;
				nums[i] += nums[i - 1];
			}
			else {
				steps[i] = i - 2;
				nums[i] += nums[i - 2];
			}
		}
		out.println(nums[n]);
		
		int nextStep = n;
		StringBuilder sb = new StringBuilder();
		while(nextStep != 0) {
			sb.append(" ").append(new StringBuilder().append(nextStep).reverse().toString());
			nextStep = steps[nextStep];
		}
//		sb.append(" ").append(0);
		out.println(sb.reverse());
	}

	public static void main(String[] args) {
		new StairsAgain().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
