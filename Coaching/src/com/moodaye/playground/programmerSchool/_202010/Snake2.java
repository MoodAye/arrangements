package com.moodaye.playground.programmerSchool._202010;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

// Problem 359
public class Snake2 {
	void solve(Scanner in, PrintWriter out) {
		long n = in.nextLong();
		out.println(computeN(n));
	}

	public static long computeN2(long n) {
		long col = 0L;
		long start = 1L;
		long end = 0L;
		while (col != n) {
			col++;
			if (col == n) {
				end = start + (col - 1) / 2;
			} else {
				end = start + col - 1;
			}
			for (long i = start; i <= end; i++) {
				if (i % 10 == 0) {
					end++;
					continue;
				}
//				System.out.printf("%d ", i);
			}
//			System.out.println();
			start = end + 1;
		}

		return end;
	}

	public static long computeN(long n) {
		// compute largest number in the prior column (sum of series)
		long largest = ((1 + n - 1) * (n - 1)) / 2;
		
		// start of nth col
		largest++;
		
		// midpoint of nth col
		largest += (n + 1) / 2 - 1;
		
		// skip zeros
		largest = largest * 10 / 9;
	
		return largest;
	}

	public static void main(String[] args) {
		new Snake2().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
