package com.moodaye.playground.programmerSchool._202010;

import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Locale;
import java.util.Scanner;

// Problem 

/*
 * Given a source char array - print all the combinations 
 * keeping order
 * 
 * Create an iterator with hasNext and next methods
 */
public class ComboPrinter2 {
	/*
	 * For iterator - we need ...
	 * the current state of the combo;
	 * the integer version of the combo;
	 * the source and len attributes of the input
	 */
	private char[] source;
	private int len;
	private int[] prior;
	private char[] cprior;
	private boolean isFirst;
	
	public ComboPrinter2(char[] source, int len) {
		this.source = source;
		this.len = len;
		prior = new int[len];
		cprior = new char[len];
		isFirst = true;
	}
	
	public boolean hasNext() {
		if(isFirst) {
			return isFirst;
		}
		
		for(int i = 0; i < len; i++) {
			if(!(prior[i] == getMax(i))) {
				return true;
			}
		}
		return false;
	}
	
	public char[] next() {
		if(!hasNext()) {
			return null;
		}
		if(isFirst) {
			setFirst();
			isFirst = false;
		}
		else {
			setNext();
		}
		return cprior;
	}
	
	private void setNext() {
		// start from end of prior - increment by one
		// if more than max allowable 
		// go to prior element and do the same
		for(int i = len - 1; i >= 0; i--) {
			if(prior[i] == getMax(i)){
				continue;
			}
			prior[i]++;
			cprior[i] = source[prior[i]];
			for(int j = i + 1; j < len; j++) {
				prior[j] = prior[j - 1] + 1;
				cprior[j] = source[prior[j]];
			}
			break;
		}
	}
	
	/* for given element in prior - what is 
	 * the max possible value;
	 * if source = a,b,c,d,e
	 * if len = 3
	 * the max prior= [2,3,4] or [c,d,e]
	 * or max(i) = source.length - len + i  (2+0; 2+1; 2+2)
	 */
	private int getMax(int i) {
		return source.length - len + i;
	}
	
	private void setFirst() {
		for(int i = 0; i < len; i++) {
			prior[i] = i;
			cprior[i] = source[i];
		}
	}
	
	
	
	
	void solve(Scanner in, PrintWriter out) {
		char[] src = in.next().toCharArray();
		int len = in.nextInt();
		printCombos(src, len, 0, new char[len]);
	}
	
	
	/*
	 * abcde 3
	 * [a] bcde 2
	 * [b] cde 1
	 * [c] ==> abc
	 * [d] ==>..
	 */
	static void printCombos(char[] src, int len, int startIdx, char[] comboHolder){
		if(len == 0) {
//			comboHolder[len - 1] = src[startIdx];
			printCharArray(comboHolder);
			return;
		}
		
		for(int i = startIdx; i < src.length - len + 1; i++) {
			comboHolder[comboHolder.length - 1 - (len - 1)] = src[i];
			printCombos(src, len - 1, i + 1, comboHolder);
		}
	}
	
	static void printCharArray(char[] c) {
		for(int i = 0; i < c.length; i++) System.out.print(c[i]);
		System.out.println();
	}
	

	public static void main(String[] args) {
//		new ComboPrinter2().run();
		Scanner in = new Scanner(System.in);
		char[] source = in.next().toCharArray();
		int len = in.nextInt();
		ComboPrinter2 cb = new ComboPrinter2(source, len);
		while(cb.hasNext()) {
			char[] next = cb.next();
			System.out.println(String.valueOf(next));
		}
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
