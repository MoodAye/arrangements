package com.moodaye.playground.programmerSchool._202010;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.util.Locale;
import java.util.Scanner;

// Problem 423
public class Message {
	void solve(Scanner in, PrintWriter out) {
		char[] c = in.next().toCharArray();
		BigInteger cnt = BigInteger.valueOf(1);
		BigInteger prevCntSingle1_2_3 = BigInteger.valueOf(1);
		for(int i = 1; i < c.length; i++) {
			BigInteger pcnt = cnt.add(BigInteger.ZERO);
			if(c[i - 1] == '1' || c[i - 1] == '2' || (c[i - 1] == '3' && c[i] <= '3')) {
				cnt = cnt.add(prevCntSingle1_2_3);
			}
			prevCntSingle1_2_3 = pcnt.add(BigInteger.ZERO);
		}
		out.println(cnt);
	}

	public static void main(String[] args) {
		new Message().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}

}

