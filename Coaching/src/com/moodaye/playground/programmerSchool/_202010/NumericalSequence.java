package com.moodaye.playground.programmerSchool._202010;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

// Problem 341
// WA test 19; time < 20min
public class NumericalSequence {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();

		int[] ans = { 0, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 21, 30, 41, 50, 61, 70, 81, 90, 111 };
		if (n <= 20) {
			out.println(ans[n]);
			return;
		}

		int firstDigit = 1;
		int len = 3;
		for (int i = 21; i <= n; i++) {
			if (firstDigit == 9) {
				firstDigit = 2;
				len++;
				continue;
			}
			firstDigit++;
		}

		StringBuilder strAns = new StringBuilder().append(firstDigit);
		for (int i = 1; i < len; i++) {
			if (firstDigit == 2 || firstDigit == 4 || firstDigit == 6 || firstDigit == 8) {
				strAns.append("0");
			} else {
				strAns.append("1");
			}
		}
		out.println(strAns.toString());
	}

	public static void main(String[] args) {
		new NumericalSequence().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
