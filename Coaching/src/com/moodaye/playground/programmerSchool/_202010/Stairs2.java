package com.moodaye.playground.programmerSchool._202010;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

// Problem 329 Time 1031pm
public class Stairs2 {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int[] values = new int[n + 1];
		for (int i = 1; i <= n; i++) {
			values[i] = in.nextInt();
		}
		int max = 0;

		// start at stair 2 
		max = values[1];
		int max_1 = max;
		int max_2 = 0;
		values[1] = 0;

		for (int i = 2; i <= n; i++) {
			if (max_1 > max_2) {
				max = values[i] + max_1;
				values[i] = i - 1;
			} else {
				max = values[i] + max_2;
				values[i] = i - 2;
			}
			max_2 = max_1;
			max_1 = max;
		}

		out.println(max);
		while (n != 0) {
			int temp = n;
			n = values[n];
			values[temp] = 0;
			if(n != temp - 1) {
				values[temp - 1] = -1;
			}
		}

		for (int i = 1; i < values.length; i++) {
			if (values[i] == 0) {
				out.printf("%d ", i);
			}
		}
	}

	public static void main(String[] args) {
		new Stairs2().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
