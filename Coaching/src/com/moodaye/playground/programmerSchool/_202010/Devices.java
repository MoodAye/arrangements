package com.moodaye.playground.programmerSchool._202010;

import java.io.PrintWriter;
import java.util.Deque;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Locale;
import java.util.Map;
import java.util.Queue;
import java.util.Scanner;
import java.util.Stack;
import java.util.TreeMap;

// Problem 674 Time = 8am - 830am -- take a break.
public class Devices {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		out.println(numberOfWays(n));
	}

	public static int numberOfWays(int n) {
		if (n <= 2) {
			return 0;
		}

		Map<Integer, Integer> map = new HashMap<Integer, Integer>();
		map.put(2, 0);
		map.put(3, 1);

		Deque<Integer> stack = new LinkedList<>();
		stack.push(n);
		while (!stack.isEmpty()) {
			int sn = stack.pop();
			if (map.containsKey(sn)) continue;
			if (sn % 2 == 0) {
				if(map.containsKey(sn / 2)) {
					map.put(sn, map.get(sn / 2) * 2);
				}
				else {
					stack.push(sn);
					stack.push(sn / 2);
				}
			}
			else {
				if(map.containsKey(sn / 2) && map.containsKey(sn / 2 + 1)) {
					map.put(sn, map.get(sn / 2) + map.get(sn / 2 + 1));
				}
				else {
					stack.push(sn);
					if(!map.containsKey(sn / 2 + 1)) {
						stack.push(sn / 2 + 1);
					}
					if(!map.containsKey(sn / 2)) {
						stack.push(sn / 2);
					}
				}
			}
		}
		return map.get(n);
	}

	public static void main(String[] args) {
		new Devices().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
