package com.moodaye.playground.programmerSchool._202010;

import java.util.Arrays;
import java.util.Scanner;

public class ComboPrinter {
	private int[] prior;
	private char[] cprior;
	private char[] source;
	private int len;
	private boolean first = true;
	
	public ComboPrinter(char[] source, int len) {
		this.source = source;
		this.len = len;
		this.prior = new int[len];
		this.cprior = new char[len];
	}
	
	public boolean hasNext() {
		if(first) {
			return true;
		}
		
		for(int i = len - 1; i >= 0; i--) {
			if(! (prior[i] == (source.length - 1) - (len - 1 - i))) {
				return true;
			}
		}
		
		return false;
	}
	
	public char[] next() {
		if(!hasNext()) {
			return null;
		}
		if(first) {
			setFirst();
			first = false;
		}
		else {
			setNext();
		}
		return cprior;
	}
	
	private void setNext() {
		for(int i = len - 1; i >= 0; i--) {
			if(prior[i] == source.length - (len - i)) {
				continue;
			}
			else {
				prior[i]++;
				for(int k = i + 1; k < len; k++) {
					prior[k] = prior[k - 1] + 1;
				}
				for(int j = 0; j < len; j++) {
					cprior[j] = source[prior[j]];
				}
				return;
			}
		}
	}
	
	private void setFirst() {
		for(int i = 0; i < len; i++) {
			prior[i] = i;
			cprior[i] = source[i];
		}
	}

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		String src = in.next();
		int len = in.nextInt();
		
		char[] c = src.toCharArray();
		ComboPrinter cp = new ComboPrinter(c, len);
		
		while(cp.hasNext()) {
			System.out.println(Arrays.toString(cp.next()));
		}
		
	}
}
