package com.moodaye.playground.programmerSchool._202010;

import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Locale;
import java.util.Scanner;

/* lc 62 10:56pm - 11:06pm */
public class UniquePaths {
	void solve(Scanner in, PrintWriter out) {
		int r = in.nextInt();
		int c = in.nextInt();
	}

	public static int uniquePaths(int r, int c){
		// paths(x,y) = paths(x-1,y) + paths(x, y-1)
		int[][] grid = new int[r + 1][c + 1];
		grid[0][1] = 1;
		for(int row = 1; row <= r; row++) {
			for(int col = 1; col <= c; col++) {
				grid[row][col] = grid[row - 1][col] + grid[row][col - 1];
			}
		}
		return grid[r][c];
	}

	public static void main(String[] args) {
		new UniquePaths().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
