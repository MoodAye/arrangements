package com.moodaye.playground.programmerSchool._202010;

import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Locale;
import java.util.Scanner;

public class Nails {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		
		int[] coord = new int[n];
		for (int i = 0; i < coord.length; i++) {
			coord[i] = in.nextInt();
		}
		out.println(getShortestLength(coord));
	}
	
	public static int getShortestLength(int[] coord) {
		int n = coord.length;
		if(n <= 1) return 0;
		
		Arrays.sort(coord);
		
		int len = coord[1] - coord[0];
		int prev = len;
		int prev_1 = len;
		
		for(int i = 2; i < n; i++) {
			len = coord[i] - coord[i - 1] + Math.min(prev, prev_1);
			prev_1 = prev;
			prev = len;
		}
		return len;
	}
	
	public static void main(String[] args) {
		new Nails().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
