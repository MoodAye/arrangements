package com.moodaye.playground.programmerSchool._202010;

import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Scanner;

// Problem cci 16.11
public class DivingBoard {
void solve(Scanner in, PrintWriter out) {
	int k = in.nextInt();
	int lng = in.nextInt();
	int srt = in.nextInt();
	
	Map<Integer,Integer> map = new HashMap<Integer, Integer>();
	for(int i = 1; i < srt; i++) {
		map.put(i, 0);
	}
	map.put(srt, 1);
	map.put(0, 1);
	
	out.println(count(k, lng, srt, map));
}

public int count(int k, int lng, int srt, Map<Integer, Integer> map) {
	if(map.containsKey(k)) return map.get(k);
	int cnt = count(k - srt, lng, srt, map) + count(k - lng, lng, srt, map);
	map.put(k, cnt);
	return cnt;
}

public static void main(String[] args) {
	new DivingBoard().run();
}

void run() {
	Locale.setDefault(Locale.US);
	try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
		solve(in, out);
	}
}
}
