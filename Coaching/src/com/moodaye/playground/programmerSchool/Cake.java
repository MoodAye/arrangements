package com.moodaye.playground.programmerSchool;

import java.util.Scanner;

/** Problem # 539 https://acmp.ru/?main=task&id_task=539&locale=en */ 
public class Cake {
	public static void main(String args[]){
		Scanner in = new Scanner(System.in);
		Integer guestCount = in.nextInt();
		if (guestCount == 1) System.out.println(0);
		else if(guestCount % 2 == 0) System.out.println(guestCount/2);
		else System.out.println(guestCount);
	}
}
