package com.moodaye.playground.programmerSchool.October2018;

import static org.junit.Assert.*;

import org.junit.AfterClass;
import org.junit.Test;
import static com.moodaye.playground.programmerSchool.October2018.TwoPilesOfStones.*;
public class TwoPilesOfStonesTest {

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Test
	public void testRecursive() {
		int s0[] = {1,2,3};
		assertEquals(0, minPile(s0));
		
		int s01[] = {1,2,3,4}; //0
		assertEquals(0, minPile(s01));
		
		// 27, 5    
		int s1[] = {5,8,13,27,14};
		assertEquals(3, minPile(s1));
		
		int s2[] = {52, 47, 59, 33, 32};
		// ans = {52,59}  {32,33,47}
		assertEquals(1, minPile(s2));
		
		int s3[] = {52, 47, 59, 33, 32, 1};
		// ans = {52,59,1}  {32,33,47}
		assertEquals(0, minPile(s3));
		
		int s4[] = {1, 10_000};
		// ans = {52,59,1}  {32,33,47}
		assertEquals(9_999, minPile(s4));
		
		int s5[] = {1,1,5,7,2};
		assertEquals(0, minPile(s5));
		
		int s6[] = {1,1,5,7};
		assertEquals(0, minPile(s6));
		
		int s7[] = {4,10,2,10,7};
		assertEquals(1, minPile(s7));
		
		int s8[] = {4,10,2,10};
		assertEquals(2, minPile(s8));
		
		int s9[] = {4};
		assertEquals(4, minPile(s9));
		
		int s10[] = {4,3,5,3,3};
		assertEquals(0, minPile(s10));
		
		int s11[] = {1,4,5,6};
		assertEquals(2, minPile(s11));
	}
	
	@Test
	public void testRecursive2() {
		int s0[] = {1,2,3};
		assertEquals(0, minPile2(s0));
		
		int s01[] = {1,2,3,4}; //0
		assertEquals(0, minPile2(s01));
		
		// 27, 5    
		int s1[] = {5,8,13,27,14};
		assertEquals(3, minPile2(s1));
		
		int s2[] = {52, 47, 59, 33, 32};
		// ans = {52,59}  {32,33,47}
		assertEquals(1, minPile2(s2));
		
		int s3[] = {52, 47, 59, 33, 32, 1};
		// ans = {52,59,1}  {32,33,47}
		assertEquals(0, minPile2(s3));
		
		int s4[] = {1, 10_000};
		// ans = {52,59,1}  {32,33,47}
		assertEquals(9_999, minPile2(s4));
		
		int s5[] = {1,1,5,7,2};
		assertEquals(0, minPile2(s5));
		
		int s6[] = {1,1,5,7};
		assertEquals(0, minPile2(s6));
		
		int s7[] = {4,10,2,10,7};
		assertEquals(1, minPile2(s7));
		
		int s8[] = {4,10,2,10};
		assertEquals(2, minPile2(s8));
		
		int s9[] = {4};
		assertEquals(4, minPile2(s9));
		
		int s10[] = {4,3,5,3,3};
		assertEquals(0, minPile2(s10));
		
		int s11[] = {1,4,5,6};
		assertEquals(2, minPile2(s11));
	}
}
