package com.moodaye.playground.programmerSchool.October2018;

import java.util.Scanner;
import java.util.Arrays;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #245 */
public class UnitedTeam {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		if(n == 0){
			out.println(0);
			return;
		}
		int[] nos = new int[n];
		for(int i = 0; i < n; i++){
			nos[i] = in.nextInt();
		}
		out.println(solution2(nos));
	}

	public static int solution2(int[] nos){
		if(nos.length == 1){
			return nos[0];
		}
		Arrays.sort(nos);
		int max = 0;
		int sum = nos[0] + nos[1];
		int k = 2;
		for(int i = 0; i < nos.length - 1; i++){
			while(k < nos.length && nos[i + 1] + nos[i] >= nos[k]){
				sum += nos[k];
				k++;
			}
			max = Math.max(max, sum);
			sum -= nos[i];
		}
		return max;
	}

	/* initial solution - does not work for some tests */
	public static int solution1(int[] nos){
		Arrays.sort(nos);
		int max = nos[nos.length - 1];
		int sum = Arrays.stream(nos).sum();
		int sum2 = sum;
		for(int i = 0; i < nos.length - 1; i++){
			if(nos[i] + nos[i + 1] >= max){
				break;
			}
			sum -= nos[i];
		}
		return sum;
	}

	public static void main(String args[]) {
		new UnitedTeam().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
