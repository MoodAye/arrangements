package com.moodaye.playground.programmerSchool.October2018;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #671 */
public class LuckyNumbers {
	void solve(Scanner in, PrintWriter out) {
		String n = in.next();
		out.println(luckyCnt(n));
	}

	public static long luckyCnt(String n) {
		char[] cn = n.toCharArray();
		int len = n.length();

		long cnt = 1L;

		// all numbers with length < len
		cnt = (1L << len) - 2;

		return cnt += getFinalPerms(n.toCharArray(), 0);

	}

	/* return power of 2 */
	public static long p2(int n) {
		return (1L << n);
	}

	private static long getFinalPerms(char[] cn, int idx) {
		int len = cn.length;
		long cnt = 0;

		if (len == 1) {
			if (cn[0] >= '7') {
				return 2;
			} else if (cn[0] >= '4') {
				return 1;
			} else {
				return 0;
			}
		}

		if (cn[idx] >= '8') {
			cnt = 1L << (len - idx);
		} else if (cn[idx] < '4') {
			cnt = 0;
		} else if (cn[idx] == '5' || cn[idx] == '6') {
			cnt += 1L << (len - idx - 1);
		} else if (cn[idx] == '7') {
			if (idx == len - 1) {
				cnt += 1;
			} else {
				cnt += getFinalPerms(cn, idx + 1);
			}
			cnt += (1L << len - idx - 1);
		} else if (cn[idx] == '4') {
			if (idx == len - 1) {
				cnt += 1;
			} else {
				cnt += getFinalPerms(cn, idx + 1);
			}
		}

		return cnt;
	}

	public static void main(String args[]) {
		new LuckyNumbers().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}

}
