package com.moodaye.playground.programmerSchool.October2018;

import static org.junit.Assert.*;
import static com.moodaye.playground.programmerSchool.October2018.LuckyNumbers.*;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class LuckyNumbersTest {
	private static int[] p2 = new int[10];

	@BeforeClass
	public static void init() {
		for (int i = 0; i < 10; i++) {
			p2[i] = (int) Math.pow(2, i);
		}
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Test
	public void test() {
		assertEquals(0, luckyCnt("1"));  //TODO - why we don't need 0L?
		assertEquals(0, luckyCnt("2"));
		assertEquals(0, luckyCnt("3"));
		assertEquals(1, luckyCnt("4"));
		assertEquals(1, luckyCnt("5"));
		assertEquals(1, luckyCnt("6"));
		assertEquals(2, luckyCnt("7"));
		assertEquals(2, luckyCnt("8"));
		assertEquals(2, luckyCnt("9"));
		assertEquals(2, luckyCnt("10"));
		assertEquals(2, luckyCnt("14"));
	
		// Numbers starting with 8 or 9
		assertEquals(p2(2) + (p2(2) - 2), luckyCnt("80"));  //4,7,44,47,74,77 = 6 = 2^2 + 2^2 - 2
		assertEquals(p2(5) + p2(5) - 2, luckyCnt("80000")); 
		assertEquals(p2(5) + p2(5) - 2, luckyCnt("90000")); 
		assertEquals(2L * p2(26) - 2L, luckyCnt("90000000000000000000000000")); 
		assertEquals(2L * p2(31) - 2L, luckyCnt("9000000000000000000000000000000")); 
		assertEquals(2L * p2(31) - 2L, luckyCnt("8000000000000000000000000000000")); 
		
		// Numbers starting with 3, 2, or 1
		assertEquals(p2(5) - 2, luckyCnt("10000")); 
		assertEquals(p2(31) - 2, luckyCnt("2000000000000000000000000000000")); 
		
		// Numbers starting with 5 or 6
		assertEquals(p2(4) + (p2(5) - 2L), luckyCnt("60000")); 
		assertEquals(p2(30) + (p2(31) - 2L), luckyCnt("5000000000000000000000000000000")); 
		
		// Numbers starting with 7
		assertEquals(6 + p2(3) - 2, luckyCnt("766"));
		assertEquals(p2(2) + p2(3) + p2(4) - 2, luckyCnt("7666"));
		assertEquals(p2(2) + p2(3) - 2, luckyCnt("720"));
		assertEquals(8 + p2(3) - 2, luckyCnt("777")); 
		assertEquals(7 + p2(3) - 2, luckyCnt("776")); 
		assertEquals(7 + p2(3) - 2, luckyCnt("774")); 
		assertEquals(2 + 4 + p2(3) - 2, luckyCnt("750")); 
		assertEquals(7 + p2(3) - 2, luckyCnt("774"));  //774 747 744 477 474 447 444
		assertEquals(4 + p2(3) - 2, luckyCnt("740"));  
		
		for(int i = 799; i >= 700; i--){
			if( i >= 780){
				assertEquals(p2(3) + p2(3) - 2, luckyCnt(String.valueOf(i)));
			}
			else if( 777 <= i && i < 780){ 
				assertEquals(p2(3) + p2(3) - 2, luckyCnt(String.valueOf(i)));
			}
			else if( 774 <= i && i < 777){ 
				assertEquals(7 + p2(3) - 2, luckyCnt(String.valueOf(i)));
			}
			else if( 748 <= i && i <= 769){
				assertEquals(p2(1) + p2(2) + p2(3) - 2, luckyCnt(String.valueOf(i)));
			}
			else if( i < 740){  
				assertEquals(p2(2) + p2(3) - 2, luckyCnt(String.valueOf(i)));
			}
		}
		
		assertEquals(p2(30) +  p2(31) - 2L, luckyCnt("5000000000000000000000000000000")); 
		
		assertEquals(3, luckyCnt("44"));
		assertEquals(3, luckyCnt("45"));
		assertEquals(4, luckyCnt("47"));
		assertEquals(4, luckyCnt("54"));
		assertEquals(4, luckyCnt("56"));
		assertEquals(6, luckyCnt("77"));

		int cnt = 0;
		int n = 1;
		for (int i = 1; i < 10; i++) {
			cnt += p2[i];
			n *= 10;
			assertEquals(cnt, luckyCnt(String.valueOf(n)));
		}

	}
}
