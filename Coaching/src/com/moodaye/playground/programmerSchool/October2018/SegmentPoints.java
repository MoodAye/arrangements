package com.moodaye.playground.programmerSchool.October2018;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #319 */

public class SegmentPoints {
	void solve(Scanner in, PrintWriter out) {
		int x1 = in.nextInt();
		int y1 = in.nextInt();
		int x2 = in.nextInt();
		int y2 = in.nextInt();
//		out.println(nPoints(x1, y1, x2, y2));
		out.println(nPointsGcd(x1, y1, x2, y2));
	}
	
	public static long nPointsGcd(int x1, int y1, int x2, int y2){
		int dx = Math.abs(x1 - x2);
		int dy = Math.abs(y1 - y2);
		int gcd = gcd(dx, dy);
		return gcd + 1;
	}
	
	public static int gcd(int a, int b){
		while(b != 0){
			int rem = a % b;
			a = b;
			b = rem;
		}
		return a;
	}
	

	public static long nPoints(int x1, int y1, int x2, int y2) {
		if (x1 > x2) {
			int temp = x1;
			x1 = x2;
			x2 = temp;
			temp = y1;
			y1 = y2;
			y2 = temp;
		}

		long m1 = y2 - y1;
		long m2 = x2 - x1;
		long c1 = x2 * y1 - x1 * y2;

		if (m1 == 0 || m2 == 0) {
			return (x2 - x1) + (y2 - y1) + 1;
		}

		long cnt = 2L;
		long start = Long.MAX_VALUE;
		
		for (long i = x1 + 1; i < x2; i++) {
			if ((m1 * i + c1) % m2 == 0L) {
				start = i;
				break;
			}
		}
		if(start != Long.MAX_VALUE) cnt += ((x2 - start) + (m2 -1)) / m2;
		return cnt;
	}

	public static void main(String args[]) {
		new SegmentPoints().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
