package com.moodaye.playground.programmerSchool.October2018;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #161 */
// After trying to write out some permutations and corresponding inversion tables:
// arrived at following algorithm
// E.g., n = 5;  3,2,5,1,4 ----> Inversion Table = 0,1,0,3,1

// so for the set 1,2,3,4,5
// the value at index = 5 is the 5 - w(i) = 5 - 1 = 4 (4th) element left = 4 ---> (,,,,4)

// Elements left = 1,2,3,5
// the value at index = 4 is the 4 - w(i) = 4 - 3 = 1 (first) element left = 1 ---> (,,,1,4)

// Elements left = 2,3,5
// the value at index = 3 is the 3 - w(i) = 3 - 0 = 3 (3rd) element left = 5 ---> (,,5,1,4)

// Elements left = 2,3
// the value at index = 2 is the 2 - w(i) = 2 - 1 = 1 (first) element left = 2 ---> (,2,5,1,4)

// Elements left = 3
// Permutation is 3,2,5,1,4

public class PermutationReconstruction {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int[] invT = new int[n + 1];
		for(int i = 1; i <= n; i++){
			invT[i] = in.nextInt();
		}
		
		int[] perm = new int[n + 1];
		boolean[] used = new boolean[n + 1];
		
		for(int i = n; i >= 1; i--){
			int elementId = i - invT[i];
			for(int j = 1; j <= n; j++){
				if(used[j]){
					continue;
				}
				elementId--;
				if(elementId == 0){
					used[j] = true;
					perm[i] = j;
					continue;
				}
			}
		}
		
		for(int i = 1; i <= n; i++){
			out.print(perm[i] + " ");
		}
	}

	public static void main(String args[]) {
		new PermutationReconstruction().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
