package com.moodaye.playground.programmerSchool.October2018;

import static org.junit.Assert.*;

import org.junit.AfterClass;
import org.junit.Test;
import static com.moodaye.playground.programmerSchool.October2018.UnitedTeam.*;
public class UnitedTeamTest {

	@Test
	public void test() {
		int[] a1 = {1,5,3,3};
		assertEquals(11, solution1(a1));
		
		int[] a2 = {20,20,20,20,20,100};
		assertEquals(120, solution1(a2));
		
		int[] a3 = {20};
		assertEquals(20, solution1(a3));
		
		int[] a4 = {20, 120};
		assertEquals(140, solution1(a4));
		
		int[] a5 = {20, 20};
		assertEquals(40, solution1(a5));
		
		int[] a6 = {2,10,100,1000};
		assertEquals(1100, solution1(a6));
		
		int[] a7 = {20};
		assertEquals(20, solution1(a7));
		
		int[] a8 = {20};
		assertEquals(20, solution1(a8));
		
		int[] a9 = new int[30_000];
		for(int i = 0; i < 30_000; i++){
			a9[i] = 60_000;
		}
		assertEquals(30_000 * 60_000, solution1(a9));
		
		int[] a10 = {0,0,0,0,0,0,0};
		assertEquals(0, solution1(a10));
		
		int[] a11 = {0,1,0,0,0,0,0};
		assertEquals(1, solution1(a11));
		
		int[] a12 = {10,1000,1010};
		assertEquals(2020, solution1(a12));
		
		int[] a13 = {1,1,10,10,10,1000,1000};
		assertEquals(2010, solution1(a13));
		
		int[] a14 = {20,20,20,20,20,20,20, 100};
//		assertEquals(140, solution1(a14));
	}

	@Test
	public void test2() {
		int[] a1 = {1,5,3,3};
		assertEquals(11, solution2(a1));
		
		int[] a2 = {20,20,20,20,20,100};
		assertEquals(120, solution2(a2));
		
		int[] a3 = {20};
		assertEquals(20, solution2(a3));
		
		int[] a4 = {20, 120};
		assertEquals(140, solution2(a4));
		
		int[] a5 = {20, 20};
		assertEquals(40, solution2(a5));
		
		int[] a6 = {2,10,100,1000};
		assertEquals(1100, solution2(a6));
		
		int[] a7 = {20};
		assertEquals(20, solution2(a7));
		
		int[] a8 = {20};
		assertEquals(20, solution2(a8));
		
		int[] a9 = new int[30_000];
		for(int i = 0; i < 30_000; i++){
			a9[i] = 60_000;
		}
		assertEquals(30_000 * 60_000, solution2(a9));
		
		int[] a10 = {0,0,0,0,0,0,0};
		assertEquals(0, solution2(a10));
		
		int[] a11 = {0,1,0,0,0,0,0};
		assertEquals(1, solution2(a11));
		
		int[] a12 = {10,1000,1010};
		assertEquals(2020, solution2(a12));
		
		int[] a13 = {1,1,10,10,10,1000,1000};
		assertEquals(2010, solution2(a13));
		
		int[] a14 = {20,20,20,20,20,20,20, 100};
		assertEquals(140, solution2(a14));
	}
}
