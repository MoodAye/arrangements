package com.moodaye.playground.programmerSchool.October2018;

import java.util.Scanner;
import java.util.Arrays;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #### */
public class Signature2 {
	void solve(Scanner in, PrintWriter out) {
		String name1 = in.next();
		String name2 = in.next();
		out.println(signature(name1, name2));
	}
	
	public static String signature(String name1, String name2){
		char[] cn1 = name1.toCharArray();
		char[] cn2 = name2.toCharArray();
		
		String sign = "";

		char[] c1 = new char[cn1.length * 2 + cn2.length];
		char[] c2 = new char[cn1.length * 2 + cn2.length];

		for (int i = cn1.length; i < cn1.length + cn2.length; i++) {
			c2[i] = cn2[i - cn1.length];
		}

		for (int k = 0; k <= name1.length() + name2.length(); k++) {
			Arrays.fill(c1, '\u0000');
			for (int i = k; i < k + name1.length(); i++) {
				c1[i] = name1.charAt(i - k);
			}
			String merged = merge(c1, c2);
			if(merged == null){
				continue;
			}
			sign = minSign(sign, merged);
		}
		return sign;
	}

	private static boolean canMerge(char c1, char c2) {
		if (c1 < 'a' && c1 >= 'A') {
			c1 = (char) (c1 - 'A' + 'a');
		}

		if (c2 < 'a' && c2 >= 'A') {
			c2 = (char) (c2 - 'A' + 'a');
		}
		return c1 == c2 || c1 == '\u0000' || c2 == '\u0000';
	}

	private static String merge(char[] c1, char[] c2) {
		StringBuilder sb = new StringBuilder();
		boolean canMerge = true;
		for (int i = 0; i < c1.length; i++) {
			if(!canMerge(c1[i], c2[i])){
				canMerge = false;
				break;
			}
			if(c1[i] == '\u0000' && c2[i] == '\u0000'){
				continue;
			}
			else if (c1[i] == '\u0000') {
				sb.append(c2[i]);
			}
			else if (c2[i] == '\u0000') {
				sb.append(c1[i]);
			}
			else{
				sb.append(c1[i] < c2[i] ? c1[i] : c2[i]);
			}
		}
		
		return canMerge ? sb.toString() : null;
	}

	private static String minSign(String s1, String s2) {
		if ("".equals(s1) || "".equals(s2) || s1.equals(s2)) {
			return s1 + s2;
		}
		else if (s1.length() < s2.length()){
			return s1;
		}
		else if (s2.length() < s1.length()){
			return s2;
		}
		return s1.compareTo(s2) < 0 ? s1 : s2;
	}

	public static void main(String args[]) {
		new Signature2().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}

}