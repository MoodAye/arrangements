package com.moodaye.playground.programmerSchool.October2018;

import java.util.Locale;
import java.io.InputStream;
import java.io.PrintWriter;

/** Programmer's School #630 */
public class Guards {
	void solve(Scanner in, PrintWriter out) {
		int t = in.nextInt();
		for (int i = 0; i < t; i++) {
			int n = in.nextInt();
			int[] counter = new int[10_000 + 2];
			int[][] times = new int[n][2];

			for (int j = 0; j < n; j++) {
				times[j][0] = in.nextInt();
				times[j][1] = in.nextInt();
				counter[times[j][0] + 1]++;
				counter[times[j][1] + 1]--;
			}

			boolean unguarded = false;
			for (int j = 1; j <= 10_000; j++) {
				counter[j] += counter[j - 1];
				if (counter[j] == 0) {
					unguarded = true;
					out.println("Wrong Answer");
					break;
				}
			}

			if (unguarded) {
				continue;
			}

			// for each guard - check if redundant
			boolean redundant = true;
			for (int j = 0; j < n; j++) {
				redundant = true;
				for (int k = times[j][0] + 1; k <= times[j][1]; k++) {
					if (counter[k] == 1) {
						redundant = false;
						break;
					}
				}
				if (redundant) {
					out.println("Wrong Answer");
					break;
				}
			}
			if (!redundant) {
				out.println("Accepted");
			}
		}
	}

	public static void main(String args[]) {
		new Guards().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
	
	static class Scanner implements AutoCloseable {
		InputStream is;
		int pos = 0;
		int size = 0;
		byte[] buffer = new byte[1024];

		Scanner(InputStream is) {
			this.is = is;
		}

		int nextChar() {
			if (pos >= size) {
				try {
					size = is.read(buffer);
				} catch (java.io.IOException e) {
					throw new java.io.IOError(e);
				}
				pos = 0;
				if (size == -1) {
					return -1;
				}
			}
			Assert.check(pos < size);
			int c = buffer[pos] & 0xFF;
			pos++;
			return c;
		}

		int nextInt() {
			int c = nextChar();
			while (c == ' ' || c == '\r' || c == '\n' || c == '\t') {
				c = nextChar();
			}
			if (c == '-') {
				c = nextChar();
				Assert.check('0' <= c && c <= '9');
				int n = -(c - '0');
				c = nextChar();
				while ('0' <= c && c <= '9') {
					int d = c - '0';
					c = nextChar();
					Assert.check(n > Integer.MIN_VALUE / 10
							|| n == Integer.MIN_VALUE / 10 && d <= -(Integer.MIN_VALUE % 10));
					n = n * 10 - d;
				}
				return n;
			} else {
				Assert.check('0' <= c && c <= '9');
				int n = c - '0';
				c = nextChar();
				while ('0' <= c && c <= '9') {
					int d = c - '0';
					c = nextChar();
					Assert.check(
							n < Integer.MAX_VALUE / 10 || n == Integer.MAX_VALUE / 10 && d <= Integer.MAX_VALUE % 10);
					n = n * 10 + d;
				}
				return n;
			}
		}

		@Override
		public void close() {
		}
	}

	static class Assert {
		static void check(boolean e) {
			if (!e) {
				throw new AssertionError();
			}
		}
	}
}
