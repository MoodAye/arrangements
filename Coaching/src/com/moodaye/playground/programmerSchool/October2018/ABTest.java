package com.moodaye.playground.programmerSchool.October2018;

import static org.junit.Assert.*;

import org.junit.AfterClass;
import org.junit.Test;
import static com.moodaye.playground.programmerSchool.October2018.AB.*;
public class ABTest {

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Test
	public void test() {
		assertEquals("4", divide(100, 25));
		assertEquals("4.5", divide(9, 2));
		assertEquals("10.1", divide(101,10));
		assertEquals("0.(3)", divide(1, 3));
		assertEquals("1.(6)", divide(10, 6));
		assertEquals("0.(110)", divide(110, 999));
		assertEquals("0.1(6)", divide(1, 6));
		assertEquals("0.01(6)", divide(1, 60));
		assertEquals("0.001(6)", divide(1, 600));
		assertEquals("1.(428571)", divide(10, 7));
		assertEquals("0.(010309278350515463917525773195876288659793814432989690721649484536082474226804123711340206185567)", divide(1, 97)); 
		

	}

}
