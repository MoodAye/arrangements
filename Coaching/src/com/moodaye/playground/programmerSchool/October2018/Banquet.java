package com.moodaye.playground.programmerSchool.October2018;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #151 */
/* Approach:
 * Seems like a graph algorithm with the pairs being edges
 * We only care about the VIPs who cannot sit at the same table
 * We place connected VIPs using either Breadth First or Depth First
 * 
 * We have max 100 VIPs - therefore the matrix will have 100 x 100 elements
 * Max size of matrix (boolean) will be 10_000 bytes.
 * 
 * We also need an array to store the tables for each VIP
 * 
 */
// Taken : < 1 hr. 
public class Banquet {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int p = in.nextInt();
		boolean[][] mustSeparate = new boolean[n + 1][n + 1];
		for(int i = 0; i < p; i++){
			int vip1 = in.nextInt();
			int vip2 = in.nextInt();
			mustSeparate[vip1][vip2] = true;
			mustSeparate[vip2][vip1] = true;
		}
	
		int[] tables = new int[n + 1];
		for(int vip = 1; vip <= n; vip++){
			if(tables[vip] == 0){
				tables[vip] = 1;
				if(!assign(vip, mustSeparate, tables)){
					out.println("NO");
					return;
				}
			}
		}
		out.println("YES");
	}

	/* Depth First Search */
	private static boolean assign(int vip, boolean[][] mustSeparate, int[] tables){
		int table = tables[vip] == 1 ? 2 : 1;
		
		for(int v = 1; v < tables.length; v++){
			if(mustSeparate[vip][v]){ // TODO - better name for 'v'
				if(tables[v] == 0){
					tables[v] = table;
				}
				else if(tables[v] == table){
					continue;
				}
				else{
					return false;
				}
				if(!assign(v, mustSeparate, tables)){
					return false;	
				}
			}
		}
		
		return true;
	}

	public static void main(String args[]) {
		new Banquet().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
