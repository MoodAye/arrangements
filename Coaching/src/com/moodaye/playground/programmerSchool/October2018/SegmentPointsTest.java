package com.moodaye.playground.programmerSchool.October2018;

import static org.junit.Assert.*;

import org.junit.AfterClass;
import org.junit.Test;
import static com.moodaye.playground.programmerSchool.October2018.SegmentPoints.*;

public class SegmentPointsTest {

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Test
	public void testGcd() {
		assertEquals(2, nPointsGcd(1, 1, 2, 2));
		assertEquals(3, nPointsGcd(0, 0, -2, -2));
		assertEquals(10, nPointsGcd(1, 1, 1, 10));
		assertEquals(10, nPointsGcd(1, 1, 10, 1));
		assertEquals(2, nPointsGcd(8361, 8234, 4968, 2595));
		assertEquals(bruteForce(1,1,2,2), nPointsGcd(1, 1, 2, 2));
		assertEquals(bruteForce(0, 0, -2, -2), nPointsGcd(0, 0, -2, -2));
		assertEquals(bruteForce(1,1,1,10), nPointsGcd(1, 1, 1, 10));
		assertEquals(bruteForce(1,1,10,1), nPointsGcd(1, 1, 10, 1));
		assertEquals(bruteForce(66, 62, 33, 32), nPointsGcd(66, 62, 33, 32));
		assertEquals(bruteForce(8361, 8234, 4968, 2595), nPointsGcd(8361, 8234, 4968, 2595));
		assertEquals(4, nPointsGcd(881604882, 427243853, 286755663, 813875711));
		assertEquals(bruteForce(622, 423, 589, 411), nPointsGcd(622, 423, 589, 411));
		assertEquals(bruteForce(15, 73, 15, 19), nPointsGcd(622, 423, 589, 411));

		
		for(int i = 0; i < 100_000; i++){
			int max = 100;
			int x1 = (int) (max * Math.random());
			int y1 = (int) (max * Math.random());
			int x2 = (int) (max * Math.random());
			int y2 = (int) (max * Math.random());
			System.out.printf("%d, %d, %d, %d\n", x1, y1, x2, y2);
			assertEquals(bruteForce(x1, y1, x2, y2), nPointsGcd(x1, y1, x2, y2));
		}
		
		
	}
	@Test
	public void test() {
		assertEquals(2, nPoints(1, 1, 2, 2));
		assertEquals(3, nPoints(0, 0, -2, -2));
		assertEquals(10, nPoints(1, 1, 1, 10));
		assertEquals(10, nPoints(1, 1, 10, 1));
		assertEquals(2, nPoints(8361, 8234, 4968, 2595));
		assertEquals(bruteForce(1,1,2,2), nPoints(1, 1, 2, 2));
		assertEquals(bruteForce(0, 0, -2, -2), nPoints(0, 0, -2, -2));
		assertEquals(bruteForce(1,1,1,10), nPoints(1, 1, 1, 10));
		assertEquals(bruteForce(1,1,10,1), nPoints(1, 1, 10, 1));
		assertEquals(bruteForce(66, 62, 33, 32), nPoints(66, 62, 33, 32));
		assertEquals(bruteForce(8361, 8234, 4968, 2595), nPoints(8361, 8234, 4968, 2595));
		assertEquals(4, nPoints(881604882, 427243853, 286755663, 813875711));
		assertEquals(bruteForce(622, 423, 589, 411), nPoints(622, 423, 589, 411));

		
		for(int i = 0; i < 100_000; i++){
			int max = 100;
			int x1 = (int) (max * Math.random());
			int y1 = (int) (max * Math.random());
			int x2 = (int) (max * Math.random());
			int y2 = (int) (max * Math.random());
//			System.out.printf("%d, %d, %d, %d\n", x1, y1, x2, y2);
			assertEquals(bruteForce(x1, y1, x2, y2), nPoints(x1, y1, x2, y2));
		}
		
		
	}
	
	
	private int bruteForce(int x1, int y1, int x2, int y2){
		if (x1 > x2) {
			int temp = x1;
			x1 = x2;
			x2 = temp;
			temp = y1;
			y1 = y2;
			y2 = temp;
		}

		long m1 = y2 - y1;
		long m2 = x2 - x1;
		long c1 = x2 * y1 - x1 * y2;

		if (m1 == 0 || m2 == 0) {
			return (x2 - x1) + (y2 - y1) + 1;
		}

		int cnt = 2;
		for (long i = x1 + 1; i < x2; i++) {
			if ((m1 * i + c1) % m2 == 0L) {
				cnt++;
//				System.out.println(" --- x1 = " + i);
			}
		}
		return cnt;
	}
}