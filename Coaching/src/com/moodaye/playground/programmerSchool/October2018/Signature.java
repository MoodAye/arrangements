package com.moodaye.playground.programmerSchool.October2018;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

public class Signature {
	void solve(Scanner in, PrintWriter out) {
		String name1 = in.next();
		String name2 = in.next();
		out.println(signature(name1, name2));
	}

	/* finds longest prefix length of child in parent */
	private static int longestPrefix(String parent, String child) {
		parent = parent.toLowerCase();
		child = child.toLowerCase();

		int idx = parent.indexOf(child);
		if (idx != -1) {
			return idx;
		}

		int prefixIdx = -1;
		for (int i = child.length(); i > 0; i--) {
			int index = parent.lastIndexOf(child.substring(0, i));
			if (index == -1) {
				continue;
			} else {
				if (i == parent.length() - index) {
					prefixIdx = i;
					break;
				}
			}
		}
		return prefixIdx;
	}
	
	private static String subsetSign(String name1, String name2){
		String lowerName1 = name1.toLowerCase();
		String lowerName2 = name2.toLowerCase();
		if(lowerName1.indexOf(lowerName2) == -1){
			return "";
		}
			int idx = lowerName1.indexOf(lowerName2);
			if(idx == 0) {
				if(lowerName1.length() > 1 && lowerName1.substring(1).indexOf(lowerName2) != -1){
					idx = lowerName1.substring(1).indexOf(lowerName2) + 1;
				}
				else{
					return name1;
				}
			}
			return name1.substring(0, idx) + name2 + name1.substring(idx + name2.length());
	}

	public static String signature(String name1, String name2) {
		if (name1.equals(name2)) {
			return name1;
		}
	
		// Abcd, Bc
		String ss = subsetSign(name1, name2);
		if(ss.length() > 0){
			return ss;
		}
		
		ss = subsetSign(name2, name1);
		if(ss.length() > 0){
			return ss;
		}
		
		int name1InName2Idx = longestPrefix(name1, name2);
		int name2InName1Idx = longestPrefix(name2, name1);

		if (name1InName2Idx == -1 && name2InName1Idx == -1) {
			if (name1.compareTo(name2) < 0) {
				return name1 + name2;
			} else {
				return name2 + name1;
			}
		}

		if (name1InName2Idx == -1) {
				return name2.substring(0, name2.length() - name2InName1Idx) + name1;
		}

		if (name2InName1Idx == -1) {
				return name1.substring(0, name1.length() - name1InName2Idx) + name2;
		}

		if (name2.compareTo(name1) < 0) {
			return name2.substring(0, name2.length() - name2InName1Idx) + name1;
		}
		return name1.substring(0, name1.length() - name1InName2Idx) + name2;
	}

	public static void main(String args[]) {
		new Signature().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
