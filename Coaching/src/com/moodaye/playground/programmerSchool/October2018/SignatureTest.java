package com.moodaye.playground.programmerSchool.October2018;

import static org.junit.Assert.*;

import org.junit.AfterClass;
import org.junit.Test;
import static com.moodaye.playground.programmerSchool.October2018.Signature.*;
public class SignatureTest {

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Test
	public void test() {
		assertEquals("MashaMisha", signature("Masha", "Misha"));
		assertEquals("MashaMisha", signature("Misha", "Masha"));
		assertEquals("JuLyalya", signature("Julya", "Lyalya")); //76
		assertEquals("JuLyalya", signature("Lyalya", "Julya")); //68
		assertEquals("Masha", signature("Masha", "Masha"));
		assertEquals("AnblaMashan", signature("Anblama", "Mashan")); 
		assertEquals("AnblaMashan", signature("Mashan", "Anblama")); 
		assertEquals("ABcd", signature("Abcd", "Bc")); //74
		assertEquals("ABcd", signature("Bc", "Abcd")); //66
		assertEquals("AbCd", signature("Abcd", "Cd")); 
		assertEquals("AaBbaa", signature("Aabb", "Bbaa")); 
		assertEquals("AaBbaa", signature("Bbaa", "Aabb")); 
		assertEquals("A", signature("A", "A")); 
		assertEquals("AB", signature("A", "B")); 
		assertEquals("AB", signature("B", "A")); 
		assertEquals("ABa", signature("Ab", "Ba")); 
		assertEquals("ABa", signature("Ba", "Ab")); 
		assertEquals("BlUeeeeeue", signature("Blueeeeeue", "Ue")); 
		assertEquals("RAjiv", signature("Rajiv", "Aj")); 
		assertEquals("ABcd", signature("Abcd", "Bc")); 
		assertEquals("ZBcd", signature("Zbcd", "Bc")); 
		assertEquals("Rajiv", signature("Rajiv", "Raji")); 
		assertEquals("AaaaB", signature("Aaaa", "B")); 
		assertEquals("AA", signature("Aa", "A")); 
		assertEquals("AA", signature("A", "Aa")); 
		assertEquals("AbAb", signature("Abab", "Ab")); 
		assertEquals("AbAb", signature("Ab", "Abab")); 
		assertEquals("AbAbabab", signature("Ab", "Abababab")); 
		assertEquals("AbAbabab", signature("Abababab", "Ab")); 
		assertEquals("AbAbabab", signature("Abababab", "Abab")); 
		assertEquals("AbAbabab", signature("Abab", "Abababab")); 
		assertEquals("AZa", signature("Za", "Aza")); 
		assertEquals("AZa", signature("Aza", "Za")); 
		assertEquals("AZ", signature("A", "Z")); 
		assertEquals("AZ", signature("Z", "A")); 
		assertEquals("AZ", signature("Z", "Az")); 
		assertEquals("AZ", signature("Az", "Z")); 
		assertEquals("AAb", signature("Ab", "Aa")); 
	}
}
