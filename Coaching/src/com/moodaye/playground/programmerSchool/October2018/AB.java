package com.moodaye.playground.programmerSchool.October2018;

import java.util.Scanner;
import java.util.regex.MatchResult;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #109 */
public class AB {
	void solve(Scanner in, PrintWriter out) {
		// in.useDelimiter("/"); // todo - use this
		in.findInLine("(\\d+)/(\\d+)");
		MatchResult result = in.match();
		int n = Integer.valueOf(result.group(1));
		int d = Integer.valueOf(result.group(2));
		out.println(divide(n, d));
	}

	public static String divide(int n, int d) {
		String mantissa = String.valueOf(n / d);
		// String decimal = getDecimalPart(n % d, d, "");
		String decimal = getDecimalPart(n, d, 0);
		return mantissa + (decimal.equals("") ? "" : "." + decimal);
	}

	/**
	 * Prebuild the decimal part to say 10_000 characters Look for a repeat
	 * pattern - use KPM substring search. Use the size of the pattern and the
	 * starting digit to return the appropriately formatted decimal.
	 */
	private static String getDecimalPart(int n, int d, int startIdx) {
		// TODO - I thought all recursive methods needed a base case?
		// Don't have one here

		// Step 1: Prebuild decimal to len = 2 * d
		int rem = n % d;
		StringBuilder decimal = new StringBuilder();
		// TODO -- do we need rem != 0
		// TODO -- understand why repeat pattern size cannot be greater than d.
		// TODO - we do this for each recursive call ... not needed for each -
		// do once and pass
		while (rem != 0 && decimal.length() <= 2 * d) {
			decimal.append(rem * 10 / d);
			rem = ((rem * 10) % d);
		}

		// TODO - let logic below address this condition
		if (rem == 0) {
			return decimal.toString();
		}

		// Step 2: find repeating pattern
		// TODO - understand how far out the from the decimal point can the
		// pattern begin
		// e.g., 1 / 6 = 0.1(3); 1 / 60 = 0.01(6)
		char[] cdec = decimal.toString().toCharArray();
		int patternSize = 1;
		int patternStartIdx = 0;
		boolean patternFound = false;
		int checkOneMore = 0;
		for (; patternSize <= cdec.length / 2; patternSize++) {
			patternFound = true;
			for (int idx = startIdx + patternSize; idx <= startIdx + 2 * patternSize - 1; idx++) {
/*				if (cdec[idx - patternSize] == cdec[idx] && checkOneMore != 2) {
					checkOneMore++;
					continue;
				} else */
				if (cdec[idx - patternSize] != cdec[idx] || 
					(patternSize <= 3 && cdec.length > (idx + 1) && cdec[idx - patternSize] != cdec[idx + 1])){
					patternFound = false;
					break;
				}
				if (patternFound == true) {
					patternStartIdx = idx; // todo - merge with idx?
					break;
				}
			}
			if (patternFound == true) {
				break;
			}
		}
		if (!patternFound) {
			return getDecimalPart(n, d, startIdx + 1);
		} else {
			if (startIdx == 0) {
				return "(" + decimal.substring(patternStartIdx, patternStartIdx + patternSize) + ")";
			} else {
				return decimal.substring(0, startIdx) + "("
						+ decimal.substring(startIdx + patternStartIdx, startIdx + patternStartIdx + patternSize) + ")";
			}
		}
	}

	// todo - do away with recursion
	private static String getDecimalPart(int rem, int d, String decimal) {
		if (rem == 0) {
			return "";
		}

		if (checkRepeat(decimal)) {
			decimal = "(" + decimal.substring(0, decimal.length() / 2) + ")";
			return decimal;
		}

		decimal += String.valueOf(rem * 10 / d); // todo - can we keep this int?
		String newDecimal = getDecimalPart((rem * 10 % d), d, decimal);
		if (newDecimal.length() > 1 && newDecimal.charAt(0) == '(') { // todo -
																		// do we
																		// need
																		// the
																		// first
																		// clause
			return newDecimal;
		}

		return decimal + newDecimal;
	}

	public static boolean checkRepeat(String decimal) {
		if (decimal.equals("")) {
			return false;
		}
		if (decimal.substring(0, decimal.length() / 2).equals(decimal.substring(decimal.length() / 2))) {
			return true;
		}
		return false;
	}

	public static void main(String args[]) {
		new AB().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
