package com.moodaye.playground.programmerSchool.October2018;

import java.util.Scanner;
import java.util.Arrays;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #71 */
// Start = 10pm
// Seems like a dynamic programming problem
public class TwoPilesOfStones {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int stones[] = new int[n];
		for (int i = 0; i < n; i++) {
			stones[i] = in.nextInt();
		}
		out.println(minPile(stones));
	}

	public static int minPile2(int[] stones){
		int sum = 0;
		for(int i : stones){
			sum += i;
		}
		return minPile2(stones, sum, 0, 0);
		
	}
	public static int minPile2(int[] stones, int sum, int pile1, int idx) {
		if(idx == stones.length){
			return sum - 2 * pile1;
		}
		int diff1 = Math.abs(minPile2(stones, sum, pile1 + stones[idx], idx + 1));
		int diff2 = Math.abs(minPile2(stones, sum, pile1, idx + 1));
		return Math.min(diff1, diff2);
	}


	public static int minPile(int[] stones) {
		if (stones.length == 1) {
			return stones[0];
		}
		int min = Integer.MAX_VALUE;
		int sum = 0;
		for (int i : stones) {
			sum += i;
		}
		for (int i = 1; i <= stones.length / 2; i++) {
			min = Math.min(min, minPile(0, 0, 0, i, stones, sum));
		}
		return min;
	}

	public static int minPile(int idx, int pile1, int stonesInPile, int howManyStonesNeeded, int[] stones, 
			int sumOfStones) {
		if (howManyStonesNeeded == stonesInPile) {
			return Math.abs(sumOfStones - 2 * pile1);
		}
		
		int min = Integer.MAX_VALUE;
		for (int i = idx; i <= stones.length - howManyStonesNeeded + stonesInPile; i++) {
			int pile1WithAnotherStone = pile1 + stones[i];
			min = Math.min(min, minPile(i + 1, pile1WithAnotherStone, stonesInPile + 1, howManyStonesNeeded, stones,
					sumOfStones));
		}
		return min;
	}

	public static void main(String args[]) {
		new TwoPilesOfStones().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
