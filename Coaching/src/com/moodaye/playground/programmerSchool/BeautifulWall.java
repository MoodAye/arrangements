package com.moodaye.playground.programmerSchool;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

/** Programmer's School - #645 
 */
public class BeautifulWall {
	
	void solve(Scanner in, PrintWriter out){
		int n = in.nextInt();
		
		int min = Integer.MAX_VALUE;
		int h = 0;
		int w = 0;
		for(int i = 1; i * i <= n ; i++){
			// note optimization:  n / i is max value of W for given i (H)
			// originally this was a loop from j = i to i*j <= n
			int x = n / i * (1 - i) - i + n;
			if(min > x){
				min = x;
				h = i;
				w = n / i;
			}
		}
		System.out.printf("%d %d", h, w);
	}
	
	public static void main(String[] args) {
		new BeautifulWall().run();
	}
	
	void run(){
		Locale.setDefault(Locale.US);
		try(Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)){
			solve(in, out);
		}
	}
}
