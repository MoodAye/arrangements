package com.moodaye.playground.programmerSchool.February2019.reviewSet14;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #666 */
// Time take = 1hr 5min
public class Abracadabra {
	void solve(Scanner in, PrintWriter out) {
		int pos = in.nextInt();
		int letter = 'z';

		// a ... baa (1<<1 - 1) ... cbaabaa (7 = 1<<2 - 1) 14 + 1 = 15; 30 + 1 =
		int len = (1 << 26) - 1;

		while (pos > 1) {
			len = len / 2;
			pos = (pos - 1) % len;
			letter--;
		}
		if (pos == 0) {
			out.print('a');
		} else
			out.print((char) (letter));
	}

	public static void main(String args[]) {
		new Abracadabra().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
