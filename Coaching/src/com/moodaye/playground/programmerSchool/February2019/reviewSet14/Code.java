package com.moodaye.playground.programmerSchool.February2019.reviewSet14;

import java.util.Scanner;
import java.util.Arrays;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #301 */
// Time Taken = ~1/2 hr
public class Code {
	void solve(Scanner in, PrintWriter out) {
		int psum = in.nextInt();
		int n = in.nextInt();
		
		int[] min = new int[n];
		int sum = psum;
		min[0] = Math.max(1, sum - (n - 1) * 9);
		sum -= min[0];
		for (int i = n - 1; i >= 1; i--) {
			if (sum <= 9) {
				min[i] = sum;
			} else {
				min[i] = Math.min(9, sum);
			}
			sum -= min[i];
		}
		
		int[] max = new int[n];
		for(int i = 0; i < n; i++){
			if(psum < 9){
			max[i] = psum;
			}
			else{
				max[i] = 9;
			}
			psum -= max[i];
		}
		
		Arrays.stream(max).forEach(out::print);
		out.println();
		Arrays.stream(min).forEach(out::print);
	}

	public static void main(String args[]) {
		new Code().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
