package com.moodaye.playground.programmerSchool.February2019.reviewSet14;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #318 */
// Start Time = 8:06pm  End Time = 9:05am
// Time taken = 1 hr
public class TheNextNumber {
	void solve(Scanner in, PrintWriter out) {
		long n = in.nextLong();
		long next = nextBin(n);
		out.println(next);
	}
	
	long nextBin(long n){
		long mask = 1L;
	
		// find first 1
		while((n & mask) == 0){
			mask <<= 1;
		}
		
		// find next 0
		while((n & mask) != 0){
			mask <<= 1;
		}
	
		// move 1 to the left
		n ^= mask;
		mask >>= 1;
		n ^= mask;
		
		// move 1s to the right to the right
		mask >>= 1;
		long rmask = 1L;
		
		while(true){
			while((rmask & n) != 0){
				rmask <<= 1;
			}
			
			while(mask != 0 && (mask & n) == 0){
				mask >>= 1;
			}
			
			if(rmask >= mask){
				break;
			}
			
			n ^= mask;
			n ^= 0L;
			n ^= rmask;
		}
		return n;
	}
	
	public static void main(String args[]) {
		new TheNextNumber().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
