package com.moodaye.playground.programmerSchool.February2019.reviewSet16;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #267 */
// Time ~45min
public class Copies {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int fast = in.nextInt();
		int slow = in.nextInt();
		if(fast > slow){
			int temp = fast;
			fast = slow;
			slow = temp;
		}
		
		// mast first copy
		int reqTime = fast;
		n--;
		
		// lcm is time when machines finish together
		int lcm = (fast * slow) / gcd(fast, slow);
		int nInLcm = (lcm / fast) + (lcm / slow);
		
		reqTime += n / nInLcm * lcm;
		int rem = n % nInLcm;
	
		// start slow and fast machines to make the extra copies
		int xFast = fast;
		int xSlow = slow;
		
		while(rem > 0){
			xFast--;
			xSlow--;
			reqTime++;
			if(xFast == 0){
				rem--;
				xFast = fast;
			}
			if(xSlow == 0){
				rem--;
				xSlow = slow;
			}
		}
		out.println(reqTime);
	}

	int gcd(int a, int b){
		while(b != 0){
			int rem = a % b;
			a = b;
			b = rem;
		}
		return a;
	}

	public static void main(String args[]) {
		new Copies().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
