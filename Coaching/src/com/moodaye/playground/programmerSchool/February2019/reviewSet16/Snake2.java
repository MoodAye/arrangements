package com.moodaye.playground.programmerSchool.February2019.reviewSet16;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #359 */
// ~40min
public class Snake2 {
	void solve(Scanner in, PrintWriter out) {
		long n = in.nextLong();

		long nth = 0;
		for (long i = 1; i <= n; i++) {
			if (i % 2 == 1) {
				nth += i;
			} else {
				nth += i - 1;
			}
		}

		// remove 0s
		long prevZeros = 0;
		long zeros;
		do {
			zeros = nth / 10;
			nth += zeros - prevZeros;
			prevZeros = zeros;
			zeros = nth / 10;
		} while (prevZeros != zeros);

		out.println(nth);
	}

	public static void main(String args[]) {
		new Snake2().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
