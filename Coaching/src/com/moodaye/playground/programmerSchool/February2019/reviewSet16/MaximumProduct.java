package com.moodaye.playground.programmerSchool.February2019.reviewSet16;



import java.util.Arrays;
import java.util.Locale;
import java.io.InputStream;
import java.io.PrintWriter;

/** Programmer's School #224 */
// Could have used counting sort - for O(n + n) ~ O(n) performance - but the logic would become a little bit more complicated.
// Current implementation has performance of O(nlog(n))
public class MaximumProduct {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int[] numbers = new int[n];
		for (int i = 0; i < n; i++) {
			numbers[i] = in.nextInt();
		}

		Arrays.sort(numbers);

		if (1L * numbers[0] * numbers[1] * numbers[n - 1] > 1L * numbers[n - 1] * numbers[n - 2] * numbers[n - 3]) {
			out.println(1L * numbers[0] * numbers[1] * numbers[n - 1]);
		} else {
			out.println(1L * numbers[n - 1] * numbers[n - 2] * numbers[n - 3]);
		}
	}

	public static void main(String args[]) {
		new MaximumProduct().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}

}
class Scanner implements AutoCloseable {

    InputStream is;
    byte buffer[] = new byte[1 << 16];
    int size = 0;
    int pos = 0;

    Scanner(InputStream is) {
        this.is = is;
    }

    int nextChar() {
        if (pos >= size) {
            try {
                size = is.read(buffer);
            } catch (java.io.IOException e) {
                throw new java.io.IOError(e);
            }
            pos = 0;
            if (size == -1) {
                return -1;
            }
        }
        Assert.check(pos < size);
        int c = buffer[pos] & 0xFF;
        pos++;
        return c;
    }

    int nextInt() {
        int c = nextChar();
        while (c == ' ' || c == '\r' || c == '\n' || c == '\t') {
            c = nextChar();
        }
        if (c == '-') {
            c = nextChar();
            Assert.check('0' <= c && c <= '9');
            int n = -(c - '0');
            c = nextChar();
            while ('0' <= c && c <= '9') {
                int d = c - '0';
                c = nextChar();
                Assert.check(n > Integer.MIN_VALUE / 10 || n == Integer.MIN_VALUE / 10 && d <= -(Integer.MIN_VALUE % 10));
                n = n * 10 - d;
            }
            return n;
        } else {
            Assert.check('0' <= c && c <= '9');
            int n = c - '0';
            c = nextChar();
            while ('0' <= c && c <= '9') {
                int d = c - '0';
                c = nextChar();
                Assert.check(n < Integer.MAX_VALUE / 10 || n == Integer.MAX_VALUE / 10 && d <= Integer.MAX_VALUE % 10);
                n = n * 10 + d;
            }
            return n;
        }
    }

    @Override
    public void close() {
    }
}

class Assert {
    static void check(boolean e) {
        if (!e) {
            throw new AssertionError();
        }
    }
}