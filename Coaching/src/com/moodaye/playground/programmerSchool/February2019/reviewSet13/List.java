package com.moodaye.playground.programmerSchool.February2019.reviewSet13;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #444 */
public class List {
	final String DOTS = "...";
	final String SEP = ", ";
	final int lenDOTS = DOTS.length();
	final int lenSEP = SEP.length();
	
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int[] cnts = new int[2 * 10_000 + 1];
		for (int i = 0; i < n; i++) {
			cnts[10_000 + in.nextInt()]++;
		}

		int curr = 0;
		int prev;
		boolean start = true;
		int first = 0;
		int cnt = 0;
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < 2 * 10_000 + 1; i++) {
			if (cnts[i] == 0) {
				continue;
			}
			if (start) {
				curr = i - 10_000;
				first = i - 10_000;
				start = false;
				cnt = 1;
				continue;
			}
			prev = curr;
			curr = i - 10_000;
			if (curr - prev == 1) {
				cnt++;
				continue;
			} else {
				sb.append(first + ", ");
				if (cnt > 1) {
					printSeries(sb, first, first + cnt - 1);
				}
				first = curr;
				cnt = 1;
			}
		}
		sb.append(first + ", ");
		if (cnt > 1) {
			printSeries(sb, first, first + cnt - 1);
		}
		if (first + cnt - 1 != curr) { 
			sb.append(curr + ", ");
		}
		String answer = sb.toString().trim();
		out.println(answer.substring(0, answer.length() - 1));
	}

	public void printSeries(StringBuilder sb, int first, int last){
		int lenF = String.valueOf(first).length();
		int lenL = String.valueOf(last).length();
		
		int lenAbbr = lenF + lenSEP + lenDOTS + lenSEP + lenL;
		
		int lenFull = lenF + lenSEP;
		for(int i = first + 1; i < last && lenFull < lenAbbr; i++){
			lenFull += lenSEP + String.valueOf(i).length();
		}
		lenFull += lenL;
		
		if(lenFull < lenAbbr){
			for(int i = first + 1; i <= last; i++){
				sb.append(i + SEP);
			}
		}
		else{
			sb.append(DOTS + SEP + last + SEP);
		}
	}

	public static void main(String args[]) {
		new List().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
