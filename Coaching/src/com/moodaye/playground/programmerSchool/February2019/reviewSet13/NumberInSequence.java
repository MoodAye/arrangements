package com.moodaye.playground.programmerSchool.February2019.reviewSet13;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #464 */
/*
 Calculate the number of transforms required to reach the digit.
 e.g., if n = 50;
 the 50th digit is a result of transforming the 18th digit 
 18th digit is from the 2nd digit
 2nd digit is from the first digit

                    18th                         50th
**                   *                             *
0112 1220 1220 2001 1220 2001 2001 0112 1220 2001 2001 0112 2001 0112 0112 1220 
*/
public class NumberInSequence {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int temp = n;
	
		int digit = 0;
		while(temp > 1){
			temp -= lowerPowOf2(temp - 1);
			digit = (digit + 1) % 3;
		}
		out.println(digit);
	}

	/* finds the power of 2 closest but lower than n */
	int lowerPowOf2(int n){
		// left most bit
		int mask = 1 << 30;
		while((mask & n) == 0){
			mask >>= 1;
		}
		return mask;
	}
	
	
	public static void main(String args[]) {
		new NumberInSequence().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
