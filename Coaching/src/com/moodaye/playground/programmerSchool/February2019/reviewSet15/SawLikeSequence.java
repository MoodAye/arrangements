package com.moodaye.playground.programmerSchool.February2019.reviewSet15;


import java.util.Locale;
import java.io.InputStream;
import java.io.PrintWriter;

/** Programmer's School #20 */
public class SawLikeSequence {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		if (n <= 1) {
			out.println(n);
			return;
		}

		int prevN = in.nextInt();
		int currN = in.nextInt();
		int currDiff = currN - prevN;
		int prevDiff = 0;

		int cnt = currDiff == 0 ? 1 : 2;
		int maxCnt = cnt;

		for (int i = 3; i <= n; i++) {
			prevN = currN;
			prevDiff = currDiff;
			currN = in.nextInt();
			currDiff = currN - prevN;
			if ((long) currDiff * prevDiff < 0) {
				cnt++;
			} else {
				maxCnt = Math.max(cnt, maxCnt);
				cnt = currDiff == 0 ? 1 : 2;
			}
		}
		maxCnt = Math.max(cnt, maxCnt);
		out.println(maxCnt);
	}

	public static void main(String args[]) {
		new SawLikeSequence().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}

	class Scanner implements AutoCloseable {
		InputStream is;
		int pos = 0;
		int size = 0;
		byte[] buffer = new byte[1024];

		Scanner(InputStream is) {
			this.is = is;
		}

		int nextChar() {
			if (pos >= size) {
				try {
					size = is.read(buffer);
				} catch (java.io.IOException e) {
					throw new java.io.IOError(e);
				}
				pos = 0;
				if (size == -1) {
					return -1;
				}
			}
			Assert.check(pos < size);
			int c = buffer[pos] & 0xFF;
			pos++;
			return c;
		}

		int nextInt() {
			int c = nextChar();
			while (c == ' ' || c == '\r' || c == '\n' || c == '\t') {
				c = nextChar();
			}
			if (c == '-') {
				c = nextChar();
				Assert.check('0' <= c && c <= '9');
				int n = -(c - '0');
				c = nextChar();
				while ('0' <= c && c <= '9') {
					int d = c - '0';
					c = nextChar();
					Assert.check(n > Integer.MIN_VALUE / 10
							|| n == Integer.MIN_VALUE / 10 && d <= -(Integer.MIN_VALUE % 10));
					n = n * 10 - d;
				}
				return n;
			} else {
				Assert.check('0' <= c && c <= '9');
				int n = c - '0';
				c = nextChar();
				while ('0' <= c && c <= '9') {
					int d = c - '0';
					c = nextChar();
					Assert.check(
							n < Integer.MAX_VALUE / 10 || n == Integer.MAX_VALUE / 10 && d <= Integer.MAX_VALUE % 10);
					n = n * 10 + d;
				}
				return n;
			}
		}

		@Override
		public void close() {
		}
	}

	static class Assert {
		static void check(boolean e) {
			if (!e) {
				throw new AssertionError();
			}
		}
	}
}