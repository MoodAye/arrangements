package com.moodaye.playground.programmerSchool.February2019.reviewSet15;

import java.util.Scanner;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.io.PrintWriter;

/** Programmer's School #202 */
// Test case:  xaaa aaa;  xaaaa aaa
public class SubStringSearch {
	void solve(Scanner in, PrintWriter out) {
		String t = in.next();
		String p = in.next();

		Map<Character, Integer> table = new HashMap<>();
		int idx = 0;
		for (char c : p.toCharArray()) {
			table.put(c, Math.max(1, p.length() - idx - 1));
			idx++;
		}
		int numSkips;
		for (int i = 0; i <= t.length() - p.length(); i += Math.max(1, numSkips)) {
			numSkips = 0;
			for (int j = p.length() - 1; j >= 0; j--) {
				if (p.charAt(j) != t.charAt(j + i)) {
					if (table.get(t.charAt(j + i)) == null) {
						numSkips = 1;
//						numSkips = Math.max(1, p.length() - 1);
//						numSkips = p.length();
					} else {
						numSkips = table.get(t.charAt(j + i));
					}
					break;
				}
			}
			if (numSkips == 0) {
					out.print(i + " ");
			}
		}
	}

	public static void main(String args[]) {
		new SubStringSearch().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
