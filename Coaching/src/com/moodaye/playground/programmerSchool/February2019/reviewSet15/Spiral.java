package com.moodaye.playground.programmerSchool.February2019.reviewSet15;

import java.util.Scanner;
import java.util.Arrays;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #196 */
// Max size of array = 100 x 100 = 10_000 x 4bytes = 40Kbytes
// ~1 hr
public class Spiral {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		
		if(n == 1){
			out.println(1);
			return;
		}
		
		
		int[][] mat = new int[n][n];

		// Right; Down; Left; Up
		int[] dirX = { 0, +1, 0, -1 };
		int[] dirY = { +1, 0, -1, 0 };
		int dir = 0; // Starting dir = Right

		int x = 0;
		int y = -1;
		int idx = 1;
		
		while (true) {
			int newX = x + dirX[dir];
			int newY = y + dirY[dir];
		
			// do we need to turn? ... is it at the edge?  
			if (newX == n || newY == n || newX == -1 || newY == -1 || mat[x + dirX[dir]][y + dirY[dir]] != 0) {
				dir = (dir + 1) % 4;
			}
			
			x += dirX[dir];
			y += dirY[dir];
			if (mat[x][y] != 0) {
				break;
			}
			
			mat[x][y] = idx++;
		}
		
		int len = String.valueOf(n * n).length();

		for (int ix = 0; ix < n; ix++) {
			for (int iy = 0; iy < n; iy++) {
				if(iy == 0){
					out.printf("%" + len + "d", mat[ix][iy]);
				}
				else{
					out.printf("%" + (len + 1) + "d", mat[ix][iy]);
				}
			}
			out.println();
		}
	}

	public static void main(String args[]) {
		new Spiral().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
