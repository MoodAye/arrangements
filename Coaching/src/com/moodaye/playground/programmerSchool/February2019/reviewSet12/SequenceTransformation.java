package com.moodaye.playground.programmerSchool.February2019.reviewSet12;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #178 */
// ~20min
public class SequenceTransformation {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int[] cnts = new int[2_000_001];
		int[] numbers = new int[n];
		for (int i = 0; i < n; i++) {
			numbers[i] = in.nextInt();
			cnts[numbers[i] + 1_000_000]++;
		}
		int maxIdx = -1;
		int max = 0;
		for (int i = 0; i < 2_000_001; i++) {
			if (max < cnts[i]) {
				max = cnts[i];
				maxIdx = i;
			}
		}
		for(int i : numbers){
			if(i == maxIdx - 1_000_000){
				continue;
			}
			out.print(i + " ");
		}
		while(cnts[maxIdx]-- > 0){
				out.print(maxIdx - 1_000_000 + " ");
		}
	}

	public static void main(String args[]) {
		new SequenceTransformation().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
