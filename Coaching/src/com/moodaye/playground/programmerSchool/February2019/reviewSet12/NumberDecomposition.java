package com.moodaye.playground.programmerSchool.February2019.reviewSet12;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #170 */
// Some test cases that will take time > 1s when using inefficient solution
// 1. 999999986 (cnt = 4)
// 2. 999999939 (cnt = 6)

// Time taken > 2 hours ... 
// used other methods 
// (quadratic equation formula ; dividing number by 2 then 3 then 4...) 
// before arriving that this solution
public class NumberDecomposition {
	
	// if series has t terms:  a, a + 1, a + 2, a + n - 1
	// then n = ta + (0 + 1 + 2 + 3 ... + t - 1) = ta + s
	// therefore   ta = (n - s)  or a = (n - s) / t
	// so we test that (n - s) % t == 0 is true and if so then we have a valid t.
	// do this for increasing values of t until n - s <= 0
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int t = 1; 
		int s = 0;
		int maxCnt = 1;
		while(true){
			if(s + t >= n){
				break;
			}
			s += t;
			t++;
			if((n - s) % t == 0){
				maxCnt = t;
			}
		}
		out.println(maxCnt);
	}

	public static void main(String args[]) {
		new NumberDecomposition().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
