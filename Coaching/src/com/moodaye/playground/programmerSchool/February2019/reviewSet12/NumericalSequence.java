package com.moodaye.playground.programmerSchool.February2019.reviewSet12;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #341 */
// ~45 min
public class NumericalSequence {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		String a = "0";
		for(int i = 2;  i <= n; i++){
			a = getNext(a);
		}
		out.println(a);
	}

	String getNext(String a) {
		char[] ca = a.toCharArray();
		int[] cnts = new int[10];
		for (int i = 0; i < ca.length; i++) {
			cnts[ca[i] - '0']++;
		}

		StringBuffer sb = new StringBuffer();
		if (ca[0] == '9') {
			int next = 1;
			while (cnts[next] != 0) {
				next++;
			}
			sb.append((char) ('0' + next));
			if (cnts[0] == 0) {
				next = 0;
			}
			for (int i = 0; i < ca.length; i++) {
				sb.append((char) ('0' + next));
			}
		}
		else{
			int next = ca[0] + 1 - '0';
			while(cnts[next] != 0){
				next++;
			}
			sb.append((char) ('0' + next));
			next = 0;
			while(cnts[next] != 0){
				next++;
			}
			for (int i = 1; i < ca.length; i++) {
				sb.append((char) ('0' + next));
			}
		}
		return sb.toString();
	}

	public static void main(String args[]) {
		new NumericalSequence().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}

}
