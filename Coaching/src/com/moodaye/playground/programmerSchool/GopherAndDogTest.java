package com.moodaye.playground.programmerSchool;

import static org.junit.Assert.*;

import org.junit.Test;

public class GopherAndDogTest {

	@Test
	public void test() {
		assertEquals("NO", GopherAndDog.burrowForSurviving(10, 10, 20, 20, new int[][] {{15,15}}));
		assertEquals("2", GopherAndDog.burrowForSurviving(20, 20, 10, 10, new int[][] {{15,15}, {25,25}}));
		assertEquals("NO", GopherAndDog.burrowForSurviving(10, 10, 20, 20, new int[][] {{-10000,-10000}}));
		assertEquals("NO", GopherAndDog.burrowForSurviving(20, 20, 20, 20, new int[][] {{15,15}, {25,25}}));
		assertEquals("1", GopherAndDog.burrowForSurviving(20, 20, 15, 15, new int[][] {{20,20}, {25,25}}));
		assertEquals("2", GopherAndDog.burrowForSurviving(20, 20, 15, 15, new int[][] {{15,15}, {21,21}, {22,22}}));
		assertEquals("NO", GopherAndDog.burrowForSurviving(-10000, -10000, 15, 15, new int[][] {{10000, 10000}}));
	}
}