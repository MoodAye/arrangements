package com.moodaye.playground.programmerSchool;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

/** Programmer School - problem 688 */
public class GopherAndDog {

	void solve(Scanner in, PrintWriter out) {
		int xGopher = in.nextInt();
		int yGopher = in.nextInt();
		int xDog = in.nextInt();
		int yDog = in.nextInt();
		int nBurrows = in.nextInt();
		int[][] burrowCoords = new int[nBurrows][2];
		for(int i = 0; i < nBurrows; i++){
			burrowCoords[i][0] = in.nextInt();
			burrowCoords[i][1] = in.nextInt();
		}
		
		System.out.println(burrowForSurviving(xGopher, yGopher, xDog, yDog, burrowCoords));

	}

	// The distance for gopher needs to be no more than 1/2 the distance for the dog */
	public static String burrowForSurviving(int xGopher, int yGopher, int xDog,int yDog, int[][] burrowCoords ){
		
		for (int i = 0; i < burrowCoords.length; i++){
			long distGopher_sq = (xGopher - burrowCoords[i][0])*(xGopher - burrowCoords[i][0]) + (yGopher - burrowCoords[i][1])*(yGopher - burrowCoords[i][1]);
			long distDog_sq = (xDog - burrowCoords[i][0])*(xDog - burrowCoords[i][0]) + (yDog - burrowCoords[i][1])*(yDog - burrowCoords[i][1]);
			if(distGopher_sq * 4 <= distDog_sq){
				return Integer.toString(i+1);
			}
		}
		return "NO";
	}

	public static void main(String[] args) {
		new GopherAndDog().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out);) {
			solve(in, out);
		}
	}

}
