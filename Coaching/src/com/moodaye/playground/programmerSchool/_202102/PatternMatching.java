package com.moodaye.playground.programmerSchool._202102;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

// Problem 16.18 (Cracking The Coding Interview)
// todo - use char array instead of string
// failing test: r abb (true - b = ''; also r ab is returning true)
public class PatternMatching {
	void solve(Scanner in, PrintWriter out) {
		String value = in.next();
		String pattern = in.next();
		
		out.println(isStringFollowingPattern(value, pattern));
	}
	
	public static boolean isStringFollowingPattern(String s, String pattern) {
		if(pattern.charAt(0) == 'b') {
			pattern = swapAB(pattern);
		}
		
		int aCnt = countCharOccurences('a', pattern);
		int bCnt = countCharOccurences('b', pattern);
		
		if(aCnt < 2 && bCnt < 2) {
			return true;
		}
		
		if(s.length() == 1) {
			if(aCnt == 1 || bCnt == 1) {
				return true;
			}
		}
		
		// to do - don't use strings
		for(int i = 1; i < s.length(); i++) {
			String aPattern = s.substring(0, i);
			int cntAPattern = countOfSubstring(s, aPattern);
			if(cntAPattern < aCnt) {
				return false;
			}
			
			//todo - test aPattern = bPattern
			String bPattern = getNextPattern(s, aPattern);
			
			if(patternFollowed(s, aPattern, bPattern, pattern)){
				return true;
			}
		}
		
		return false;
	}
	
	public static boolean patternFollowed(String s, String a, String b, String pattern) {
		StringBuilder sb = new StringBuilder();
		for(char c : pattern.toCharArray()) {
			if(c == 'a') {
				sb.append(a);
			}
			else {
				sb.append(b);
			}
		}
		return s.equals(sb.toString());
	}

	/* returns the string that lies between the first 2 occurrencese of
	 * pattern.  E.g., if s = "thisisthisisnot" and pattern = "this"
	 * then the string "is" is returned.
	 */
	public static String getNextPattern(String s, String pattern) {
		// find the first two incidence of s
		int idx1 = s.indexOf(pattern);
		
		int idx2 = s.indexOf(pattern, idx1 + pattern.length());
		
		return idx1 == -1 || idx2 == -1 ? null : s.substring(idx1 + pattern.length(), idx2);
	}
	
	public static int countOfSubstring(String s, String ss) {
		int count = 0;
		int nextIdx = s.indexOf(ss, 0);
		while(nextIdx != -1) {
			count++;
			// todo - test end of string ...make sure no overflow
			nextIdx = s.indexOf(ss, nextIdx + 1);
		}
		return count;
	}
	
	public static String swapAB(String pattern) {
		char[] cp = pattern.toCharArray();
		for(int i = 0; i < cp.length; i++) {
			if(cp[i] == 'a') {
				cp[i] = 'b';
			}
			else {
				cp[i] = 'a';
			}
		}
		
		//todo - valueOf is efficient?
		return String.valueOf(cp);
	}
	
	public static int countCharOccurences(char ch, String pattern) {
		int count = 0;
		
		for(int i = 0; i < pattern.length(); i++) {
			if(pattern.charAt(i) == ch) {
				count++;
			}
		}
		return count;
	}

	public static void main(String[] args) {
		new PatternMatching().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
/**
 * Strategy
 * 1. Evaluate pattern code by counting occurrences of 'a' and 'b'
 * 	*a. If count of both is lte 1 (one) then return true since either may be ''
 *  *b. If the first character of pattern is 'b' - switch 'a' and 'b'
 * 2. Evaluate value
 * 	a. for each substring starting at idx = 0 and increasing length;
 * 	    - if count of occurences of substring is lt count of 'a' - then return false
 * 		- if count of 'a' pattern is gt count of 'a' in pattern then continue
 *      - else - find 'b' pattern - and check if the pattern code is satisfied
 *  b. edge cases
 *  	- 
 */

