package com.moodaye.playground.programmerSchool._202102;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

// Problem 954
public class Glass {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		
		if(n < 10) {
			out.println(0);
			return;
		}
		
		int[] cnt = new int[n + 1];
		cnt[10] = 2;
		cnt[11] = 2;
		if(n >= 12) {
			cnt[12] = 2;
		}
		
		for(int i = 13; i <= n; i++) {
			cnt[i] = (cnt[i - 10] + cnt[i - 11] + cnt[i - 12]) % 1_000_000;
		}
		
		out.println(cnt[n]);
	}

	public static void main(String[] args) {
		new Glass().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
