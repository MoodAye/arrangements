package com.moodaye.playground.programmerSchool._202102;

public class VarArgs {
	
	public static void main(String[] args) {
		varargsmethod(1,2,3,4);
		varargsmethod();
	}
	
	private static void varargsmethod(int... a) {
		System.out.printf("num of args = %d%n", a.length);
		if(a.length > 0) {
			System.out.printf("first args is %d%n", a[0]);
		}
		System.out.println(a.getClass());
		String[] ar = new String[2];
		System.out.println(ar.getClass());
	}
}
