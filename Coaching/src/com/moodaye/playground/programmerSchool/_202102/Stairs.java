package com.moodaye.playground.programmerSchool._202102;

import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

// Problem 329
public class Stairs {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int[] steps = new int[n + 1];
		for (int i = 1; i <= n; i++) {
			steps[i] = in.nextInt();
		}
		
		int[] previousStep = new int[n + 1];

		
		for(int i = 2; i <= n; i++) {
			if(steps[i - 1] > steps[i - 2]) {
				previousStep[i] = (i - 1);
				steps[i] += steps[i - 1];
			}
			else {
				previousStep[i] = (i - 2);
				steps[i] += steps[i - 2];
			}
		}
		
		out.println(steps[n]);
		
		List<Integer> orderOfSteps = new LinkedList<>();
		
		int index = n;
		
		while(index > 0) {
			orderOfSteps.add(index);
			index = previousStep[index];
		}
		
		
		
	}

	public static void main(String[] args) {
		new Stairs().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
	
}
