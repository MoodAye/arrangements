package com.moodaye.playground.programmerSchool._202102;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Scanner;

// Problem 536
// Test2 failing

/*
 *  Additional tests
 *  5 230  10
 *  12345
 *  10 (Ans)
 *  
 *  
 *  Passing first 35 test cases
 */
public class Numbers2 {
	void solve(Scanner in, PrintWriter out) {
		long n = in.nextLong();
		long m = in.nextLong();
		
		long maxCntDigits = in.nextLong();
		long maxCnt = 10;
		for(long i = 1; i < maxCntDigits; i++) {
			maxCnt *= 10;
		}
		
		char[] cs = in.next().toCharArray();
		long cnt = 0; 
		Map<Long, Long> list = new HashMap<>();
		
		long next = cs[0] - '0';
		if(next > m) {
			out.println(0);
			return;
		}
		list.put(next, 1L);
		
		for(int i = 1; i < cs.length; i++) {
			next = cs[i] - '0';
			if((next) > m) {
				out.println(0);
				return;
			}
			Map<Long, Long> newList = new HashMap<>();
			
			long nextCnt = 0;
			for(long k : list.keySet()) {
				nextCnt = (nextCnt + list.get(k)) % maxCnt;
//				nextCnt = (nextCnt + list.get(k));
				long nextNumber = k * 10 + next;
				if(nextNumber <= m) {
					newList.put(nextNumber, list.get(k));
				}
			}
			newList.put(next, nextCnt);
			list = newList;
		}
		
		for(long k : list.keySet()) {
				cnt = (cnt + list.get(k)) % maxCnt;
//				cnt = (cnt + list.get(k));
		}
		
		out.println(cnt);
	}

	public static void main(String[] args) {
		new Numbers2().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
