package com.moodaye.playground.programmerSchool._202102;

import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Locale;
import java.util.Scanner;

// Problem 
public class WordChain {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		String[] words = new String[n];
		for (int i = 0; i < n; i++) {
			words[i] = in.next();
		}
		Arrays.sort(words);
		int cnts[] = new int[n];
		Arrays.fill(cnts, 1);
		int max = 1;
		for (int i = 1; i < n; i++) {
			max = 1;
			for (int j = i - 1; j >= 0; j--) {
				if (words[j].equals(words[i]) || !words[j].startsWith(words[i])) {
					max = Math.max(max, 1);
				}
			}
			cnts[i] = max;
		}
		out.println(Arrays.stream(cnts).max().getAsInt());
	}

	public static void main(String[] args) {
		new WordChain().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
