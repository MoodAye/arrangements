package com.moodaye.playground.programmerSchool._202102;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

// Problem 510
public class ChocolateBar {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		
		if(n % 2 == 1) {
			out.println(0);
			return;
		}
		if(n == 2) {
			out.println(3);
			return;
		}
		
		
		int[] cnt = new int[n + 1];
		
		cnt[2] = 3;
		cnt[4] = 11;
		for(int i = 6; i <= n; i += 2) {
			cnt[i] = 3 * cnt[i - 2] + 2 * cnt[i - 4];
		}
		out.println(cnt[n]);
	}

	public static void main(String[] args) {
		new ChocolateBar().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
