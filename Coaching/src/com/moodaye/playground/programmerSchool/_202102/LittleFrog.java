package com.moodaye.playground.programmerSchool._202102;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

// Problem 
public class LittleFrog {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int[][] nums = new int[n + 1][n + 1];
		for (int i = 1; i <= n; i++) {
			for (int j = 1; j <= n; j++) {
				nums[i][j] = in.nextInt();
			}
		}
		out.println(maxWeight(nums));
	}

	public static int maxWeight(int[][] wts) {
		int n = wts.length - 1;
		int[][] mw = new int[n + 1][n + 1];
		for (int row = 1; row <= n; row++) {
			for (int col = 1; col <= n; col++) {
				mw[row][col] = Math.max(wts[row][col], mw[row][col - 1]);
				for (int prow = row - 1; prow >= 1; prow--) {
					mw[row][col] = Math.max(mw[row][col], mw[prow][col - 1] + wts[row - prow][col]);
				}
			}
		}
		return mw[n][n];
	}

	public static void main(String[] args) {
		new LittleFrog().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
