package com.moodaye.playground.programmerSchool._202102;

import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Locale;
import java.util.Scanner;

public class Nails {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int[] nails = new int[n];
		for (int i = 0; i < nails.length; i++) {
			nails[i] = in.nextInt();
		}
		Arrays.sort(nails);
		
		if(n == 2) {
			out.println(nails[1] - nails[0]);
			return;
		}
		
		if(n == 3) {
			out.println(nails[2] - nails[0]);
			return;
		}
		
		// start with i = 3;
		int sum_i_1 = nails[2] - nails[0];
		int sum_i_2 = nails[1] - nails[0];
		int sum_i = 0;
		
		for (int i = 3; i < n; i++) {
			sum_i = nails[i] - nails[i - 1] + Math.min(sum_i_2, sum_i_1);
			sum_i_2 = sum_i_1;
			sum_i_1 = sum_i;
		}
		out.println(sum_i);
	}

	public static void main(String[] args) {
		new Nails().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
