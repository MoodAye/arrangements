package com.moodaye.playground.programmerSchool._202102;

import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Scanner;

// Problem 
public class DeviceSelection {
void solve(Scanner in, PrintWriter out) {
	int n = in.nextInt();
	
	Map<Integer, Integer> memo = new HashMap<Integer, Integer>();
	memo.put(1,0);
	memo.put(2, 0);
	memo.put(3, 1);
	
	
	out.println(count(n, memo));
	out.println(memo.size());
}

public int count(int n, Map<Integer, Integer> memo) {
	if(memo.containsKey(n)) {
		return memo.get(n);
	}
	
	memo.put(n, count(n/2, memo) + count(n - n/2, memo));
	return memo.get(n);
}

public static void main(String[] args) {
	new DeviceSelection().run();
}

void run() {
	Locale.setDefault(Locale.US);
	try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
		solve(in, out);
	}
}
}
