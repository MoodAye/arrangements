package com.moodaye.playground.programmerSchool._202102;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

// Problem 518
public class PathsInLabyrinth {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int stepsLeft = in.nextInt();
		boolean[][] allowed = new boolean[n + 2][n + 2];
		for (int i = 1; i <= n; i++) {
			char[] line = in.next().toCharArray();
			for(int j = 1; j <= n; j++) {
				allowed[i][j] = line[j - 1] == '0' ? true : false;
			}
		}
		
		int[][] cnt = new int[n + 2][n + 2];
		cnt[n][n] = 1;
		
		while(stepsLeft > 0) {
			int[][] newCnt = new int[n + 2][n + 2];
			for(int i = 1; i <= n; i++) {
				for(int j = 1; j <= n; j++) {
					if(allowed[i][j]) {
						newCnt[i - 1][j] += cnt[i][j];
						newCnt[i + 1][j] += cnt[i][j];
						newCnt[i][j - 1] += cnt[i][j];
						newCnt[i][j + 1] += cnt[i][j];
					}
				}
			}
			cnt = newCnt;
			stepsLeft--;
		}
		
		out.println(cnt[1][1]);
	}

	public static void main(String[] args) {
		new PathsInLabyrinth().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
