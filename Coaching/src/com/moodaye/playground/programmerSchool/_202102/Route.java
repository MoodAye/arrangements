package com.moodaye.playground.programmerSchool._202102;

import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Locale;
import java.util.Scanner;

// Problem 368
public class Route {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int[][] nums = new int[n][n];
		for (int i = 0; i < nums.length; i++) {
			char[] line = in.next().toCharArray();
			for (int j = 0; j < n; j++) {
				nums[i][j] = line[j] - '0';
			}
		}

		char[][] route = new char[n][n];

		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				if (i == 0 && j == 0) {
					continue;
				}
				if (i == 0) {
					nums[i][j] += nums[i][j - 1];
					route[i][j] = 'L';
				}
				else if (j == 0) {
					nums[i][j] += nums[i - 1][j];
					route[i][j] = 'T';
				}
				else if (nums[i - 1][j] > nums[i][j - 1]) {
					nums[i][j] += nums[i - 1][j];
					route[i][j] = 'L';
				} else {
					nums[i][j] += nums[i][j - 1];
					route[i][j] = 'T';
				}
			}
		}
		
		int i = n - 1;
		int j = n - 1;
		while(!(i == 0 && j == 0)) {
			if(route[i][j] == 'L') {
			route[i][j] = '#';
				j--;
			}
			else if(route[i][j] == 'T') {
			route[i][j] = '#';
				i--;
			}
		}
		route[0][0] = '#';
		
		for (i = 0; i < n; i++) {
			for (j = 0; j < n; j++) {
				if(route[i][j] == '#') {
					out.print(route[i][j]);
				}
				else {
					out.print('.');
				}
			}
			out.println();
		}
	}

	public static void main(String[] args) {
		new Route().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
