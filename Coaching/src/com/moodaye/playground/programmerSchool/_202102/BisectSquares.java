package com.moodaye.playground.programmerSchool._202102;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

// Problem 
// cci 16.13
// test1: 1 1 4 1 1 4 4 4     9 2 13 2 13 6 9 6   (expect points to be around (1,1.9, 13, 4.1)
public class BisectSquares {
	void solve(Scanner in, PrintWriter out) {
		Point p1 = new Point(in.nextInt(), in.nextInt());
		Point p2 = new Point(in.nextInt(), in.nextInt());
		Point p3 = new Point(in.nextInt(), in.nextInt());
		Point p4 = new Point(in.nextInt(), in.nextInt());
		Square sq1 = new Square(p1, p2, p3, p4);
		p1 = new Point(in.nextInt(), in.nextInt());
		p2 = new Point(in.nextInt(), in.nextInt());
		p3 = new Point(in.nextInt(), in.nextInt());
		p4 = new Point(in.nextInt(), in.nextInt());
		Square sq2 = new Square(p1, p2, p3, p4);

		Point[] ps = Square.bisectEndPoints(sq1, sq2);

		out.printf("Point1=(%.2f, %.2f  Point2=(%.2f, %f%n", ps[0].x, ps[0].y, ps[1].x, ps[1].y);

	}

	public static void main(String[] args) {
		new BisectSquares().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}

class Point {
	double x;
	double y;

	public Point(double x, double y) {
		this.x = x;
		this.y = y;
	}

	public static boolean equals(Point p1, Point p2) {
		return (Double.compare(p1.x, p2.x) == 0 && Double.compare(p1.y, p2.y) == 0);
	}
}

class Line {
	double slope;
	double intercept;

	/* when line is parallel to y axis */
	double xConst;

	public Line(double slope, double intercept) {
		this.slope = slope;
		this.intercept = intercept;
	}

	public Line(Point p1, Point p2) {
		if (Square.equal(p1.x, p2.x)) {
			this.slope = Double.POSITIVE_INFINITY;
			this.intercept = Double.POSITIVE_INFINITY;
			xConst = p1.x;
		} else {
			this.slope = (p1.y - p2.y) / (p1.x - p2.x);
			this.intercept = p1.y - slope * p1.x;
		}
	}

	/* returns null if lines are parallel */
	public static Point intersect(Line n1, Line n2) {
		Point intersect = null;
		if (!(Double.compare(n1.slope, n2.slope) == 0)) {
			if (n1.slope == Double.POSITIVE_INFINITY) {
				intersect = new Point(n1.xConst, n2.slope * n1.xConst + n2.intercept);
			} else if(n2.slope == Double.POSITIVE_INFINITY) {
				intersect = new Point(n2.xConst, n1.slope * n2.xConst + n1.intercept);
			}
			else {
				double x = (n1.intercept - n2.intercept) / (n2.slope - n1.slope);
				double y = n1.slope * x + n1.intercept;
				intersect = new Point(x, y);
			}
		}
		return intersect;
	}
}

/* Assumes sides are parallel to axes */
class Square {
	Point diag1_p1;
	Point diag1_p2;
	Point diag2_p1;
	Point diag2_p2;

	public Square(Point p1, Point p2, Point p3, Point p4) {
		if (areDiags(p1, p2)) {
			diag1_p1 = p1;
			diag1_p2 = p2;
			diag2_p1 = p3;
			diag2_p2 = p4;
		} else if (areDiags(p1, p3)) {
			diag1_p1 = p1;
			diag1_p2 = p3;
			diag2_p1 = p2;
			diag2_p2 = p4;
		} else {
			diag1_p1 = p1;
			diag1_p2 = p4;
			diag2_p1 = p2;
			diag2_p2 = p3;
		}
	}

	public static boolean equal(double x, double y) {
		return Double.compare(x, y) == 0;
	}

	// TODO - how to do this if we relieve assumption that sides are not parallel to
	// axes
	private boolean areDiags(Point p1, Point p2) {
		return (!(equal(p1.x, p2.x) || equal(p1.y, p2.y)));
	}

	public Point center() {
		return new Point((diag1_p1.x + diag1_p2.x) / 2, (diag2_p1.y + diag2_p2.y) / 2);
	}

	public static Line bisectSquares(Square sq1, Square sq2) {
		Point c1 = sq1.center();
		Point c2 = sq2.center();

		if (Point.equals(c1, c2)) {
			// return any line through center
			return new Line(c1, new Point(0, 0));
		}
		return new Line(sq1.center(), sq2.center());
	}

	/*
	 * extend line to the square side 1. Determine which sides the line bisects by
	 * the slope of the line. - since the sides are parallel to the axes - if the
	 * line has a small angle (less than 45 deg) - then it cuts the parallel sides
	 * and vv. - 45 deg slope => m = 1; -45 deg m = -1 therefore abs(m) < 1 - edge
	 * cases: m = 1 or -1 - cuts through the edges. (see below) - squares are
	 * identical - one square encloses the other
	 * 
	 * 2. Now the line will cut one side of each square to reach the other center.
	 */
	/* Returns the 2 endpoints of the line that bisects 2 sqaures */
	public static Point[] bisectEndPoints(Square sq1, Square sq2) {
		Point[] p = new Point[2];
		Line line = bisectSquares(sq1, sq2);

		Line[][] sq1Lines = getSides(sq1);
		Line[][] sq2Lines = getSides(sq2);

		if (Math.abs(line.slope) < 1) {
			// line cuts vertical sides
			p = extendLine(line, sq1Lines[0][0], sq1Lines[0][1], sq2Lines[0][0], sq2Lines[0][1]);
		} else {
			// line cuts horizontal sides
			p = extendLine(line, sq1Lines[1][0], sq1Lines[1][1], sq2Lines[1][0], sq2Lines[1][1]);
		}
		return p;
	}

	/*
	 * returns 2 Points that lie outside the given line n0. We intersect the lines
	 * and return the 2 outer points
	 */
	public static Point[] extendLine(Line n0, Line n1, Line n2, Line n3, Line n4) {
		Point p1 = Line.intersect(n0, n1);
		Point p2 = Line.intersect(n0, n2);
		Point p3 = Line.intersect(n0, n3);
		Point p4 = Line.intersect(n0, n4);

		// find min x co-oord
		Point minP = p1;
		if (minP.x > p2.x)
			minP = p2;
		if (minP.x > p2.x)
			minP = p3;
		if (minP.x > p4.x)
			minP = p4;

		// find max x co-oord
		Point maxP = p1;
		if (maxP.x < p2.x)
			maxP = p2;
		if (maxP.x < p2.x)
			maxP = p3;
		if (maxP.x < p4.x)
			maxP = p4;

		Point p[] = new Point[2];
		p[0] = minP;
		p[1] = maxP;

		return p;
	}

	/*
	 * Line[0] is 2 horizontal sides Line[1] is 2 vertical sides
	 */
	public static Line[][] getSides(Square sq) {
		Line[][] lines = new Line[2][2];
		// line cuts vertical sides - x coords are same
		if (sq.diag1_p1.x == sq.diag2_p1.x) {
			lines[0][0] = new Line(sq.diag1_p1, sq.diag2_p1);
			lines[0][1] = new Line(sq.diag1_p2, sq.diag2_p2);
			lines[1][0] = new Line(sq.diag1_p1, sq.diag2_p2);
			lines[1][1] = new Line(sq.diag1_p2, sq.diag2_p1);
		} else {
			lines[0][0] = new Line(sq.diag1_p1, sq.diag2_p2);
			lines[0][1] = new Line(sq.diag1_p2, sq.diag2_p1);
			lines[1][0] = new Line(sq.diag1_p1, sq.diag2_p1);
			lines[1][1] = new Line(sq.diag1_p2, sq.diag2_p2);
		}
		return lines;
	}

}
