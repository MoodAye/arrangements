package com.moodaye.playground.programmerSchool;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

/** Programmer's School - Problem #14 */
public class LCM{
	void solve(Scanner in, PrintWriter out){
		int a = in.nextInt();
		int b = in.nextInt();
		int gcd = gcd(a,b);
	
		// even though we divide first - we need to cast to long
		// since the gcd may be low in and a*b are large numbers.
		out.println((long) (a / gcd) * b);
	}
	
	private int gcd(int a, int b){
		while(b!= 0){
			int rem = a % b;
			a = b;
			b = rem;
		}
		return a;
	}
	
	public static void main(String[] args) {
		new LCM().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}