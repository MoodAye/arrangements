package com.moodaye.playground.programmerSchool.September2018;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #42 */
public class Dragons {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		out.println(maxHeads2(n));
	}

	/* Per hint from FM (to derive algo that has O(n) complexity
	 * Uses idea that largest product is obtained by 
	 * using the numbers 3 and 2 as summands to h. 
	 * Any integer other than 1 can be expressed as a sum of 2s and 3s.
	 */
	public static long maxHeads2(int h) {
		if(h == 1){
			return 1;
		}
		
		int twos = h / 2;
		int threes = 0;
		if (h % 2 == 1) {
			threes++;
			twos--;
		}
		
		// 3 + 3 = 2 + 2 + 2  and 3 * 3 > 2 * 2 * 2
		// therefore we replace 2,2,2 for 3,3
		threes += (twos / 3) * 2;
		twos -= (twos / 3) * 3;
		
		long heads = 1;
		for(int i = twos; i > 0; i--){
			heads *= 2;
		}
		for(int i = threes; i > 0; i--){
			heads *= 3;
		}
		return heads;
	}

	// Dynamic Programming approach - my initial approach - but overkill for this problem
	// Start Time = 8:20pm End Time = 8:45pm Time Taken = 25min
	public static long maxHeads(int h) {
		long[] mh = new long[99];
		return maxHeads(h, mh);
	}

	public static long maxHeads(int h, long[] mh) {
		if (h <= 4) {
			return h;
		}

		if (mh[h] != 0) {
			return mh[h];
		}

		long max = h;
		for (int i = h - 1; i >= 1; i--) {
			max = Math.max(max, i * maxHeads(h - i, mh));
		}

		mh[h] = max;
		return max;
	}

	public static void main(String args[]) {
		new Dragons().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
