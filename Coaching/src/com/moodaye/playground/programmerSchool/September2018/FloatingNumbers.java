package com.moodaye.playground.programmerSchool.September2018;

import java.util.Scanner;
import java.util.Arrays;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #449 */
// Start Time = 10:27am  Pause = 10:41am
public class FloatingNumbers {
	void solve(Scanner in, PrintWriter out) {
		int len = in.nextInt();
		int n = in.nextInt();
		int a[] = new int[n];
		for(int i = 0; i < n; i++){
			a[i] = in.nextInt();
		}
		Arrays.sort(a);;
		out.println(soln2(a, len));
	}

	/** Greedy approach */
	public static int soln2(int[] a, int len){
		int min = 1;
		int i = 0;
		int base = a[i];
		while(true){
			i++;
			while(i < a.length && (base + 2 * len) >= a[i]){
				i++;
			}
			if(i == a.length){
				break;
			}
			min++;
//			i++;
			if(i == a.length){
				break;
			}
			base = a[i];
		}
		return min;
	}
	
	public static void main(String args[]) {
		new FloatingNumbers().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
