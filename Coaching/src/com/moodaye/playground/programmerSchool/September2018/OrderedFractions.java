package com.moodaye.playground.programmerSchool.September2018;

import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #422 */
// Start 10:33pm  End 10:44  (~11min)
public class OrderedFractions {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		Set<Fraction> fset = new TreeSet<>();
		for(int den = 2; den <= n; den++){
			for(int num = 1; num <= den - 1; num++){
				if(reducible(num, den)){
					continue;
				}
				fset.add(new Fraction(num, den));
			}
		}
		for(Fraction f : fset){
			out.println(f.toString());
		}
		
	}

	public static void main(String args[]) {
		new OrderedFractions().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
	
	boolean reducible(int a, int b){
		while(b > 0){
			int temp = a % b;
			a = b;
			b = temp;
		}
		return a > 1;
	}
}

class Fraction implements Comparable<Fraction>{
	int num;
	int den;
	Fraction(int num, int den){
		this.num = num;
		this.den = den;
	}
	@Override
	public int compareTo(Fraction o) {
		return num * o.den - den * o.num;
	}
	
	public String toString(){
		return num + "/" + den;
	}
}