package com.moodaye.playground.programmerSchool.September2018;

import java.util.Scanner;
import java.util.Arrays;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #216 */
// Complexity = O(n) as we use the counting sort algorithm.
public class LabelsCollection {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int[] cap = new int[100_001];
		
		for(int i = 0; i < n ; i++){
			cap[in.nextInt()]++;
		}
	
		int left = 1;
		int right = 100_000;
		int labels = 0;
		while(true){
			while(left < 100_000 && cap[left] == 0){
				left++;
			}
			while(right > 1 && cap[right] == 0){
				right--;
			}
			if((left == right && cap[left] == 1) || (cap[left] + cap[right]) <= 1){
				break;
			}
			labels++;
			cap[left]--;
			cap[right]--;
			if(left != 1){
				left--;
				cap[left]++;
			}
			if(right != 1){
				cap[right - 1]++;
			}
		}
		out.println(labels);
	}

	public static void main(String args[]) {
		new LabelsCollection().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
