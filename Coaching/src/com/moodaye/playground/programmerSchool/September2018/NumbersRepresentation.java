package com.moodaye.playground.programmerSchool.September2018;

import java.util.Scanner;
import java.util.Arrays;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #255 */
public class NumbersRepresentation {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int a = getA3(n);
		out.println(a + " " + (n - a));
	}

	/* returns largest divisor less than n */
	public static int getA3(int n){
		int a = 1;
		for(int i = 2; i * i <= n; i++){
			if(n % i == 0){
				a = n / i;
				break;
			}
		}
		
		return a;
	}

	// don't have proof for this
	public static int getA2(int n){
		if(n % 2 == 0){
			return n / 2;
		}
		int mp = maxPrimeFactor(n); 
		if(mp == n){
			return 1;
		}
		int a = mp;
		for(int i = 2 * mp; i <= n / 2; i += mp){
			if((n - i) % i == 0){
				a = i;
			}
		}
		return a;
	}

	// brute force
	public static int getA(int n) {
		int max = 0;
		if (n % 2 == 0) {
			return n / 2;
		}
		int a = 0;
		for (int i = 1; i <= n / 2; i += 2) {
			int currGcd = gcd(i, n - i);
			if (max < currGcd) {
				max = currGcd;
				a = i;
			}
		}
		return a;
	}

	public static int maxPrimeFactor(int n) {
		int maxPrime = 1;
		int temp = n;
		for (int i = 2; i * i <= n; i++) {
			while(temp % i == 0){
				maxPrime = i;
				temp /= i;
			}
			if(temp == 1){
				break;
			}
		}
		return temp != 1 ? temp : maxPrime;
	}

	public static int gcd(int a, int b) {
		while (b != 0) {
			int rem = a % b;
			a = b;
			b = rem;
		}
		return a;
	}

	public static void main(String args[]) {
		new NumbersRepresentation().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
