package com.moodaye.playground.programmerSchool.September2018;

import static org.junit.Assert.*;

import org.junit.AfterClass;
import org.junit.Test;
import static com.moodaye.playground.programmerSchool.September2018.NumbersRepresentation.*;
public class NumbersRepresentationTest {

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}
	
	@Test
	public void test(){
		assertEquals(25, getA2(75));
		
		for(int i = 1_000_000_000; i < 1_000_000_100; i++){
	//		System.out.println(i);
//			assertEquals(getA(i), getA2(i));
			getA2(i);
		}
	}

	@Test
	public void testMaxPrimeFactor() {
		
		assertEquals(7, maxPrimeFactor(14));
		assertEquals(3, maxPrimeFactor(36));
	}

}
