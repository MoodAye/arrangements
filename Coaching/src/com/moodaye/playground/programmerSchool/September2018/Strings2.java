package com.moodaye.playground.programmerSchool.September2018;

import java.util.Set;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;

/** Programmer's School #87 */
public class Strings2 {
	void solve(BufferedReader in, PrintWriter out) throws Exception{
		List<String> list = new ArrayList<>();
		while (true) {
			String s = in.readLine();
			if (s.equals("ENDOFINPUT")) {
				break;
			}
			list.add(s);
		}
		out.println(solution2(list));
	}

	public static int solution2(List<String> list) {
		Set<String> combos = new HashSet<>();
		int cnt = 0;
		for (int i = 0; i < list.size(); i++) {
			for (int j = 0; j < list.size(); j++) {
				if(list.get(i).length() + list.get(j).length() <= 100){
					combos.add(list.get(i) + list.get(j));
				}
			}
		}

		for (int i = 0; i < list.size(); i++) {
				if(combos.contains(list.get(i))){
					cnt++;
				}
		}
		return cnt;
	}

	public static void main(String args[]) {
		new Strings2().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (BufferedReader in = new BufferedReader(new InputStreamReader(System.in)); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
		catch(Exception e){}
	}

}
