package com.moodaye.playground.programmerSchool;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

/** Programmer's School - #665 */
public class PalindromicTime {
	void solve(Scanner in, PrintWriter out) {
		in.useDelimiter("\\s+|\\:");
		int hh = in.nextInt();
		int mm = in.nextInt();
		out.println(finishTimeSimple(hh, mm));
	}
	
	/** Refactored (overloaded) */
	public static String finishTimeSimple_old(int hh, int mm){
		mm++;
		int finhh = 0;
		int finmm = 0;
		boolean found = false;
		for(int i = 0; i < 24 && !found; i++){
			for(int j = mm; j < 60; j++){
				if(checkPalindrome(((hh + i) % 24), j)){
					finhh = (hh + i) % 24;
					finmm = j;
					found = true;
					break;
				}
			}
			mm = 0;
		}
		return String.format("%02d:%02d", finhh, finmm);
	}

	/** Simpler implementation (#1) relative to finishTimeSimple_old */
	public static String finishTimeSimple1(int hh, int mm){
		int hm = hh * 60 + mm;
        while (true) {
            hm = (hm + 1) % (24 * 60);
            int h = hm / 60;
            int m = hm % 60;
            int h1 = h / 10;
            int h2 = h % 10;
            int m1 = m / 10;
            int m2 = m % 10;
            if (h1 == m2 && h2 == m1) {
                return ("" + h1 + h2 + ":" + m1 + m2);
            }
        }	
	}
	
	/** Simpler implementation (#2) relative to finishTimeSimple_old */
	public static String finishTimeSimple(int hh, int mm){
		while (true) {
	        mm++;
	        if (mm == 60) {
	            mm = 0;
	            hh++;
	            if (hh == 24) {
	                hh = 0;
	            }
	        }
	        if (hh % 10 == mm / 10 && mm % 10 == hh / 10) {
	          return String.format("%02d:%02d", hh, mm);
	        }
	    }
	}
	
	/** Checks every possible time until palindrome found */
	public static String finishTimeSimple(String hhmmIn){
		int hh = Integer.valueOf(hhmmIn.substring(0,2));
		int mm = Integer.valueOf(hhmmIn.substring(3));
		return finishTimeSimple(hh, mm);
	}
	
	private static boolean checkPalindrome(int hh, int mm){
		return pad0(hh).equals(flip(pad0(mm))) ? true : false;
	}
	
	private static String pad0(int k){
		if (k < 10){
			return "0" + String.valueOf(k);
		}
		return String.valueOf(k);
	}

	/** Faster - but easy to make error here */
	public static String finishTime(String hhmmIn){
		String str_hh = hhmmIn.substring(0,2);
		int hh = Integer.valueOf(str_hh);
		int mm = Integer.valueOf(hhmmIn.substring(3));
		
		String ft = ""; 
		
		switch(str_hh){
			case("00"):
				ft = "01:10";
				break;
			case("05"):
				if(mm < 50){
					ft = "05:50";
					break;
				}
			case("06"):
			case("07"):
			case("08"):
			case("09"):
				ft = "10:01";
				break;
			case("15"):
				if(mm < 51){
					ft = "15:51";
					break;
				}
			case("16"):
			case("17"):
			case("18"):
			case("19"):
				ft = "20:02";
				break;
			case("23"):
				if(mm < 32){
					ft = "23:32";
				}
				else{
					ft = "00:00";
				}
				break;
			default:
				if(mm < Integer.valueOf(flip(str_hh))){
					ft = str_hh + ":" + flip(str_hh);
				}
				else{
					String str_hhplus1 = String.valueOf(hh + 1);
					if(hh + 1 < 10){
						str_hhplus1 = "0" + str_hhplus1;
					}
					ft = str_hhplus1 + ":" + flip(str_hhplus1);
				}
		}
		return ft;
	}

	// s = "xy"  return "yx"
	private static String flip(String s){
		return s.substring(1) + s.substring(0,1);
	}

	public static void main(String[] args) {
		new PalindromicTime().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
