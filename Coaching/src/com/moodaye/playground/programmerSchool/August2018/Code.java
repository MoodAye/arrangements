package com.moodaye.playground.programmerSchool.August2018;


import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #301 */
// Time Taken = 21 min
public class Code {
	void solve(Scanner in, PrintWriter out) {
		int s = in.nextInt();
		int n = in.nextInt();
		out.println(makeString(generateMax(s, n)) + " " + makeString(generateMin(s, n)));
	}

	public static int[] generateMin(int s, int n){
		int[] an = new int[n];
		s--; // a[0] has to be 1 or more
		int idx = n - 1;
		while(s != 0){
			an[idx] = Math.min(9, s);
			s -= an[idx--];
		}
		an[0]++; //a[0] has to be 1 
		return an;
	}
	
	public static int[] generateMax(int s, int n){
		int[] an = new int[n];
		int idx = 0;
		while(s != 0){
			an[idx] = Math.min(9, s);
			s -= an[idx++];
		}
		return an;
	}

	public static String makeString(int[] a){
		StringBuilder sb = new StringBuilder();
		for(int i : a){
			sb.append(i);
		}
		return sb.toString();
	}
	
	
	public static void main(String args[]) {
		new Code().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
