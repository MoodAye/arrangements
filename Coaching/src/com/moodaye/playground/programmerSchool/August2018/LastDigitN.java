package com.moodaye.playground.programmerSchool.August2018;

import java.util.Scanner;

import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #447 */
// Time Taken = Needed hint to solve
public class LastDigitN {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int lastDigit = 1;
		int cnt2 = 0;
		int cnt5 = 0;
		for (int i = 2; i <= n; i++) {
			int x = i;
			while (x % 2 == 0) {
				cnt2++;
				x /= 2;
			}
			while (x % 5 == 0) {
				cnt5++;
				x /= 5;
			}
			lastDigit = (lastDigit * x) % 10;
		}
		cnt2 -= cnt5;
		for (int i = 0; i < cnt2; i++) {
			lastDigit = (lastDigit * 2) % 10;
		}
		out.println(lastDigit);
	}

	public static void main(String args[]) {
		new LastDigitN().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}