package com.moodaye.playground.programmerSchool.August2018;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #196 */
// Time Taken ~ 1 hr.
public class Spiral {
	final static int NOT_SET = 0;
	final static int UP = 1;
	final static int DOWN = 2;
	final static int RIGHT = 3;
	final static int LEFT = 4;

	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int[][] m = new int[n][n];
		int[][] v = new int[n][n];
		int nextX = 0;
		int nextY = 0;
		int currX = 0;
		int currY = 0;

		m[currX][currY] = RIGHT;
		v[currX][currY] = 1;
		int idx = 2;

		while (true) {
			currX = nextX;
			currY = nextY;
			if (m[currX][currY] == RIGHT) {
				if (currY + 1 == n || m[currX][currY + 1] != NOT_SET) {
					nextX = currX + 1;
					nextY = currY;
					if (m[nextX][nextY] != NOT_SET) {
						break;
					}
					m[nextX][nextY] = DOWN;
				} else {
					nextX = currX;
					nextY = currY + 1;
					if (m[nextX][nextY] != NOT_SET) {
						break;
					}
					m[nextX][nextY] = RIGHT;
				}
			}

			if (m[currX][currY] == LEFT) {
				if (currY - 1 == -1 || m[currX][currY - 1] != NOT_SET) {
					nextX = currX - 1;
					nextY = currY;
					if (m[nextX][nextY] != NOT_SET) {
						break;
					}
					m[nextX][nextY] = UP;
				} else {
					nextX = currX;
					nextY = currY - 1;
					if (m[nextX][nextY] != NOT_SET) {
						break;
					}
					m[nextX][nextY] = LEFT;
				}
			}

			if (m[currX][currY] == DOWN) {
				if (currX + 1 == n || m[currX + 1][currY] != NOT_SET) {
					nextX = currX;
					nextY = currY - 1;
					if (m[nextX][nextY] != NOT_SET) {
						break;
					}
					m[nextX][nextY] = LEFT;
				} else {
					nextX = currX + 1;
					nextY = currY;
					if (m[nextX][nextY] != NOT_SET) {
						break;
					}
					m[nextX][nextY] = DOWN;
				}
			}

			if (m[currX][currY] == UP) {
				if (currX - 1 == -1 || m[currX - 1][currY] != NOT_SET) {
					nextX = currX;
					nextY = currY + 1;
					if (m[nextX][nextY] != NOT_SET) {
						break;
					}
					m[nextX][nextY] = RIGHT;
				} else {
					nextX = currX - 1;
					nextY = currY;
					if (m[nextX][nextY] != NOT_SET) {
						break;
					}
					m[nextX][nextY] = UP;
				}
			}
			v[nextX][nextY] = idx++;
		}
		
		int size = String.valueOf(n * n).length();
		
		for(int i = 0; i < n; i++){
			for(int j = 0; j < n; j++){
				out.printf("%" + size + "d ", v[i][j]);
			}
			out.println();
		}
	}

	public static void main(String args[]) {
		new Spiral().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}

}
