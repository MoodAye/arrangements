package com.moodaye.playground.programmerSchool.August2018;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #267 */
// Took about 2 hrs to solve this.
// First did this via brute force - did not anticipate getting TLE.
// Then refined to use LCM.
public class Copies {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int x = in.nextInt();
		int y = in.nextInt();
	
		//make first copy
		int minTime = Math.min(x, y);
		n--;
	
		int lcm = x * y / hcd(x, y);
		int cLcm = n / (lcm / x + lcm / y); // number of cycles of lcm
		int nLcm = cLcm * (lcm / x + lcm / y); // number of copies 
		n -= nLcm;
		minTime += cLcm * lcm;
		
		int extraTime = 0;
		while(n > 0){
			extraTime++;
			if(extraTime % x == 0){
				n--;
			}
			if(extraTime % y == 0){
				n--;
			}
		}
		out.println(minTime + extraTime);
	}
	
	public static int hcd(int x, int y){
		while(y != 0){
			int rem = x % y;
			x = y;
			y = rem;
		}
		return x;
	}

	public static void main(String args[]) {
		new Copies().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
