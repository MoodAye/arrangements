package com.moodaye.playground.programmerSchool.August2018;

import java.util.Scanner;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #422 */
public class OrderedFractions {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		List<Fraction> list = new ArrayList<>();
		for (int i = 2; i <= n; i++) {
			Fraction e = new Fraction(1, i);
			list.add(e);
			for (int j = 2; j < i; j++) {
				if (hcd(i, j) == 1) {
					e = new Fraction(j, i);
					list.add(e);
				}
			}
		}

		Collections.sort(list);

		for (Fraction f : list) {
			out.println(f);
		}
	}

	public static void main(String args[]) {
		new OrderedFractions().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}

	public static int hcd(int a, int b) {
		while (b != 0) {
			int rem = a % b;
			a = b;
			b = rem;
		}
		return a;
	}
}

class Fraction implements Comparable<Fraction> {
	int numerator;
	int denominator;

	Fraction(int numerator, int denominator) {
		this.numerator = numerator;
		this.denominator = denominator;
	}

	@Override
	public int compareTo(Fraction f) {
		return this.numerator * f.denominator - this.denominator * f.numerator;
	}

	public String toString() {
		return numerator + "/" + denominator;
	}
}
