package com.moodaye.playground.programmerSchool.August2018;

import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Scanner;

/* Programmers School #668 */
// Time Taken > 2 hours
public class ProductDigit3 {
	public void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		out.println(getMin(n) + " " + getMax(n));
	}

	public static int getMax(int n) {
		int[] x = { 2, 3, 5, 7 };
		int max = 0;
		int pow10 = 1;
		
		for (int d : x) {
			while (n % d == 0) {
				max += d * pow10;
				pow10 *= 10;
				n /= d;
			}
		}
		
		if(n != 1){
			return -1;
		}

		return max;
	}
	
	public static int getMin(int n){
		int min = 0;
		int pow10 = 1;
		for(int i = 9; i > 1; i--){
			while(n % i == 0){
				min += i * pow10;
				pow10 *= 10;
				n /= i;
			}
		}
		if(n != 1){
			return -1;
		}
		return min;
	}

	public static void main(String[] args) {
		new ProductDigit3().run();
	}

	public void run() {
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
