package com.moodaye.playground.programmerSchool.August2018;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #359 */
//  Time take ~ 1hr
public class Snake2 {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		long ans = 0; // not sure if we need long - ok we do ... ans for n = 1_000_000 is 555_555_555_555

		for (int i = 2; i <= n; i++) {
			ans += ((i - 1) / 2) * 2 + 1; // go down; go up; go one across
		}
		ans = addSkipTens(1L, ans);
		out.println(ans);
	}	

	/*
	 * the count of numbers ending in zero is dependent on hops and start if
	 * start = 15 and hops = 22 ----- 15 + 2 = 17; + 10 + 10 + 2
	 * start = 15 and hops = 29 ----- 15 + 9 = 26 + 1; + 10 + 10 + 2
	 * start = 15 and hops = 59 ----- 15 + 9 = 26 + 1; + 10 + 10 + 10 + 10 + 10 + 5 
	 *        or 26 + 1 + 50 + 5 or 57 + 5 (recurse)
	 */
	public static long addSkipTens(long start, long hops) {
		long addAmt = hops % 10;
		while (addAmt > 0) {
			start++;
			if (start % 10 == 0) {
				start++;
			}
			addAmt--;
		}
		if (hops < 10) {
			return start;
		}
		
		start += (hops / 10 * 10);
		return addSkipTens(start, hops / 10);
	}

	public static void main(String args[]) {
		new Snake2().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
