package com.moodaye.playground.programmerSchool.August2018;

import static org.junit.Assert.*;

import org.junit.AfterClass;
import org.junit.Test;
import static com.moodaye.playground.programmerSchool.August2018.Copies.*;
public class CopiesTest {

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Test
	public void test() {
		
	}
	
	@Test
	public void testHcd(){
		assertEquals(hcd(2,4), 2);
		assertEquals(hcd(10,4), 2);
		assertEquals(hcd(1,1), 1);
		assertEquals(hcd(10,1), 1);
		assertEquals(hcd(1,10), 1);
		assertEquals(hcd(52,13), 13);
	}

}
