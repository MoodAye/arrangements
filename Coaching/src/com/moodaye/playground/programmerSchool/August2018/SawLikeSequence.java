package com.moodaye.playground.programmerSchool.August2018;

import java.util.Locale;
import java.io.InputStream;
import java.io.PrintWriter;

/** Programmer's School #70 */
// Time > 3hrs
public class SawLikeSequence {
	static int[][] tt = truthTable();
	final static int UP = 1;
	final static int DOWN = 2;
	final static int FLAT = 0;

	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int[] seq = new int[n];
		for (int i = 0; i < n; i++) {
			seq[i] = in.nextInt();
		}
		out.println(maxLengthSeq(seq));
	}

	public static int maxLengthSeq(int[] seq) {
		int cnt = 0;
		int max = 0;
		for (int i = 0; i < seq.length; i++) {
			cnt = adjustCnt(seq, i, cnt);
			max = Math.max(max, cnt);
		}
		return max;
	}
	
	public static int getDir(int[] seq, int i, int j){
		if(seq[i] - seq[j] > 0){
			return DOWN;
		}
		else if(seq[i] - seq[j] < 0){
			return UP;
		}
		return FLAT;
	}
	
	public static int adjustCnt(int[] seq, int idx, int cnt){
		int idx1;
		int idx2;
		
		if(idx == 0){
			return 1;
		}
		
		idx2 = idx <= 1 ? FLAT : getDir(seq, idx - 2, idx - 1);
		idx1 = getDir(seq, idx - 1, idx);
		
		if (tt[idx2][idx1] == 11){
			return cnt + 1;
		}
		return tt[idx2][idx1];
	}

	/** 11 denotes that the count should be incremented */
	public static int[][] truthTable() {
		int[][] ttss = new int[3][3];
		ttss[FLAT][FLAT] = 1; 
		ttss[FLAT][UP] = 2; 
		ttss[FLAT][DOWN] = 2; 
		ttss[UP][FLAT] = 1; 
		ttss[UP][UP] = 2;
		ttss[UP][DOWN] = 11;
		ttss[DOWN][FLAT] = 1;
		ttss[DOWN][UP] = 11;
		ttss[DOWN][DOWN] = 2;
		return ttss;
	}

	public static void main(String args[]) {
		new SawLikeSequence().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}

	static class Scanner implements AutoCloseable {

		InputStream is;
		byte buffer[] = new byte[1 << 16];
		int size = 0;
		int pos = 0;

		Scanner(InputStream is) {
			this.is = is;
		}

		int nextChar() {
			if (pos >= size) {
				try {
					size = is.read(buffer);
				} catch (java.io.IOException e) {
					throw new java.io.IOError(e);
				}
				pos = 0;
				if (size == -1) {
					return -1;
				}
			}
			Assert.check(pos < size);
			int c = buffer[pos] & 0xFF;
			pos++;
			return c;
		}

		int nextInt() {
			int c = nextChar();
			while (c == ' ' || c == '\r' || c == '\n' || c == '\t') {
				c = nextChar();
			}
			if (c == '-') {
				c = nextChar();
				Assert.check('0' <= c && c <= '9');
				int n = -(c - '0');
				c = nextChar();
				while ('0' <= c && c <= '9') {
					int d = c - '0';
					c = nextChar();
					Assert.check(n > Integer.MIN_VALUE / 10
							|| n == Integer.MIN_VALUE / 10 && d <= -(Integer.MIN_VALUE % 10));
					n = n * 10 - d;
				}
				return n;
			} else {
				Assert.check('0' <= c && c <= '9');
				int n = c - '0';
				c = nextChar();
				while ('0' <= c && c <= '9') {
					int d = c - '0';
					c = nextChar();
					Assert.check(
							n < Integer.MAX_VALUE / 10 || n == Integer.MAX_VALUE / 10 && d <= Integer.MAX_VALUE % 10);
					n = n * 10 + d;
				}
				return n;
			}
		}

		@Override
		public void close() {
		}
	}

	static class Assert {
		static void check(boolean e) {
			if (!e) {
				throw new AssertionError();
			}
		}
	}
}
