package com.moodaye.playground.programmerSchool.August2018;

import static org.junit.Assert.*;
import org.junit.AfterClass;
import org.junit.Test;
import static com.moodaye.playground.programmerSchool.August2018.SawLikeSequence.*;

public class SawLikeSequenceTest {

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Test
	public void test() {
		int[] seq1 = { 1, 2, 1 };
		assertEquals(3, maxLengthSeq(seq1));

		int[] seq2 = { 1 };
		assertEquals(1, maxLengthSeq(seq2));

		int[] seq3 = { 1, 1 };
		assertEquals(1, maxLengthSeq(seq3));

		int[] seq4 = { 1, 2 };
		assertEquals(2, maxLengthSeq(seq4));

		int[] seq5 = { 2, 1, 2 };
		assertEquals(3, maxLengthSeq(seq5));

		int[] seq6 = { 2, 1, 2, 1, 2, 2, 2, 2 };
		assertEquals(5, maxLengthSeq(seq6));

		int[] seq7 = { 2, 2, 2, 2, 1, 2, 1, 2, 2, 2, 2 };
		assertEquals(5, maxLengthSeq(seq7));

		int[] seq8 = { 2, 1, 0 };
		assertEquals(2, maxLengthSeq(seq8));

		int[] seq9 = { 1, 2, 3 };
		assertEquals(2, maxLengthSeq(seq9));
	}
}
