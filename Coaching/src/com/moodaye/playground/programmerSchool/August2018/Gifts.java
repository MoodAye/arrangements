package com.moodaye.playground.programmerSchool.August2018;

import java.util.Locale;
import java.util.Scanner;
import java.io.PrintWriter;

/** Programmer's School #472 */
// Time taken = 35 min
public class Gifts {
	void solve(Scanner in, PrintWriter out) {
		int kids = in.nextInt();
		int moreCandies = in.nextInt();
		int[] candies = new int[30_001]; 
		for(int i = 0; i < kids; i++){
			candies[in.nextInt()]++;
		}
		
		int idx = 1;
		while(moreCandies > 0){
			if(candies[idx] != 0){
				candies[idx]--;
				candies[idx + 1]++;
				moreCandies--;
			}
			else{
				idx++;
				if(idx == 30_000){
					out.println(idx + moreCandies / candies[idx]);
					return;
				}
			}
		}
		out.println(candies[idx] == 0 ? idx + 1 : idx);
	}

	public static void main(String args[]) {
		new Gifts().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
