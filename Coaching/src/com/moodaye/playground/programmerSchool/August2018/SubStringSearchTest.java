package com.moodaye.playground.programmerSchool.August2018;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.AfterClass;
import org.junit.Test;
import static com.moodaye.playground.programmerSchool.August2018.SubStringSearch.*;

public class SubStringSearchTest {

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Test
	public void testPmt() {
		int[] pmt0 = { 0 };
		assertArrayEquals(pmt0, new KMP("A").pmt);

		int[] pmt1 = { 0, 0, 0, 0, 1, 2, 0 };
		assertArrayEquals(pmt1, new KMP("ABCDABD").pmt);

		int[] pmt2 = { 0, 0, 1, 2 };
		assertArrayEquals(pmt2, new KMP("ABAB").pmt);

		int[] pmt3 = { 0, 0, 1, 2, 3, 0, 1, 2, 3, 4, 5, 0, 1, 2, 3, 4, 5, 4, 5, 6 };
		assertArrayEquals(pmt3, new KMP("ABAB" + "ACAB" + "ABAD" + "ABAB" + "ABAC").pmt);

		int[] pmt4 = { 0, 0, 0 };
		assertArrayEquals(pmt4, new KMP("BAA").pmt);
	}

	@Test
	public void test() {
		assertEquals(0, new KMP("A").ss("ABCD", 0));
		assertEquals(-1, new KMP("X").ss("ABCD", 0));
		assertEquals(1, new KMP("B").ss("ABCD", 0));
		assertEquals(2, new KMP("C").ss("ABCD", 0));
		assertEquals(3, new KMP("D").ss("ABCD", 0));
		assertEquals(0, new KMP("AB").ss("ABCD", 0));
		assertEquals(1, new KMP("BC").ss("ABCD", 0));
		assertEquals(2, new KMP("CD").ss("ABCD", 0));
		assertEquals(-1, new KMP("DA").ss("ABCD", 0));
		assertEquals(0, new KMP("aba").ss("ababbababa", 0));
		assertEquals(4, new KMP("baba").ss("ababbababa", 0));
		// the updated code does not support starting non - zero index unless
		// it is part of the get all indexes code
//		assertEquals(6, new KMP("baba").ss("ababbababa", 5));
		assertEquals(-1, new KMP("baa").ss("baba", 0));
		assertEquals(1, new KMP("bba").ss("bbba", 0));
		assertEquals(0, new KMP("bbbbb").ss("bbbbbbbbbbbbbbbb", 0));
	}

	@Test
	public void test2() {
		List<Integer> list = new KMP("A").ssAll("ABCD");
		List<Integer> listExp = new ArrayList<>();
		listExp.add(0);
		assertEquals(list, listExp);

		list = new KMP("X").ssAll("ABCD");
		listExp = new ArrayList<>();
		assertEquals(list, listExp);

		list = new KMP("B").ssAll("ABCD");
		listExp = new ArrayList<>();
		listExp.add(1);
		assertEquals(list, listExp);

		list = new KMP("C").ssAll("ABCD");
		listExp = new ArrayList<>();
		listExp.add(2);
		assertEquals(list, listExp);

		list = new KMP("D").ssAll("ABCD");
		listExp = new ArrayList<>();
		listExp.add(3);
		assertEquals(list, listExp);

		list = new KMP("AB").ssAll("ABCD");
		listExp = new ArrayList<>();
		listExp.add(0);
		assertEquals(list, listExp);

		list = new KMP("BC").ssAll("ABCD");
		listExp = new ArrayList<>();
		listExp.add(1);
		assertEquals(list, listExp);

		list = new KMP("CD").ssAll("ABCD");
		listExp = new ArrayList<>();
		listExp.add(2);
		assertEquals(list, listExp);

		list = new KMP("DA").ssAll("ABCD");
		listExp = new ArrayList<>();
		assertEquals(list, listExp);

		list = new KMP("aba").ssAll("ababbababa");
		listExp = new ArrayList<>();
		listExp.add(0);
		listExp.add(5);
		listExp.add(7);
		assertEquals(list, listExp);

		list = new KMP("baba").ssAll("ababbababa");
		listExp = new ArrayList<>();
		listExp.add(4);
		listExp.add(6);
		assertEquals(list, listExp);

		list = new KMP("baba").ssAll("ababbababa");
		listExp = new ArrayList<>();
		listExp.add(4);
		listExp.add(6);
		assertEquals(list, listExp);

		list = new KMP("baa").ssAll("baba");
		listExp = new ArrayList<>();
		assertEquals(list, listExp);

		list = new KMP("bba").ssAll("bbba");
		listExp = new ArrayList<>();
		listExp.add(1);
		assertEquals(list, listExp);

		list = new KMP("bbbbb").ssAll("bbbbbbbbbbbbbbbb");
		listExp = new ArrayList<>();
		for (int i = 0; i < 12; i++) {
			listExp.add(i);
		}
		assertEquals(list, listExp);

	}

	@Test
	public void testAll() {
		List<Integer> list1 = new KMP("A").ssAll("AA");
		List<Integer> list1Exp = new ArrayList<>();
		list1Exp.add(0);
		list1Exp.add(1);
		assertEquals(list1, list1Exp);

	}

}
