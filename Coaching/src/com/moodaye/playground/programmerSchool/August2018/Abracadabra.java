package com.moodaye.playground.programmerSchool.August2018;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;
import java.math.BigInteger;

/** Programmer's School #666 */
// Needed hint to solve this. Time taken > 3hr.
public class Abracadabra {
	void solve(Scanner in, PrintWriter out) {
		// size after 26th step
		int len = (1 << 26) - 1;
		char step = 'z';
		
		int n = in.nextInt();
		while(true){
			if(n == 1){
				out.print(step);
				return;
			}
			n = n > (len - 1) / 2 + 1 ? n - (len - 1) / 2 - 1 : n - 1;
			len = (len - 1) / 2;
			step--;
		}
	}

	public static void main(String args[]) {
		new Abracadabra().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
