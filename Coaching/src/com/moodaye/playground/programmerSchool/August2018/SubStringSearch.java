package com.moodaye.playground.programmerSchool.August2018;

import java.util.Scanner;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #202 */
public class SubStringSearch {
	void solve(Scanner in, PrintWriter out) {
		String t = in.next();
		String p = in.next();
		KMP kmp = new KMP(p);
		List<Integer> idx = kmp.ssAll(t);
		for(int i : idx){
			out.print(i + " ");
		}
	}
	
	public static void main(String args[]) {
		new SubStringSearch().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}

class KMP {
	String pat;
	int[] pmt;
	char[] pc;
	char[] tc;

	KMP(String pat) {
		this.pat = pat;
		pc = pat.toCharArray();
		computePmt();
	}

	private void computePmt() {
		char[] patternString = pat.toCharArray();
		int patternLength = patternString.length;
		pmt = new int[patternLength];
		// value for partial_match at index 0 will always be 0 as no proper
		// suffix or prefix exist
		pmt[0] = 0;
		int length = 0;
		int currentIndex = 1;
		while (currentIndex < patternLength) {
			if (patternString[currentIndex] == patternString[length]) {
				// match is found
				length = length + 1;
				pmt[currentIndex] = length;
				currentIndex = currentIndex + 1;
			} else {
				// for mismatch case
				if (length != 0) {
					length = pmt[length - 1];
				} else {
					pmt[currentIndex] = 0;
					currentIndex = currentIndex + 1;
				}
			}
		}
	}
	
	public List<Integer> ssAll(String text){
		List<Integer> allMatches = new ArrayList<>();
		int startIndex = 0;
		
		while(startIndex < text.length() - pat.length() + 1){ 
			int index = ss(text, startIndex);
			if(index == -1){
				break;
			}
			allMatches.add(index);
			startIndex = index + 1;
		}
		return allMatches;
	}

	public int ss(String text, int startIdx) {
		int pidx = startIdx == 0 ? 0 : pmt[pat.length() - 1];
		int i = startIdx == 0 ? 0 : startIdx + pat.length() - 1;
		while (i < text.length()) {
			if (pc[pidx] == text.charAt(i)){
				pidx++;
				if (pidx == pat.length()) {
					break;
				}
				i++;
			}
			else if(pidx == 0){
				i++;
			}
			else{
				pidx = pmt[pidx - 1];
			}
		}
		return pidx == pat.length() ? i - pidx + 1: -1;
	}
}
