package com.moodaye.playground.programmerSchool.August2018;

import static org.junit.Assert.*;

import org.junit.AfterClass;
import org.junit.Test;

import static com.moodaye.playground.programmerSchool.August2018.MaximumProduct.*;

public class MaximumProductTest {

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Test
	public void test() {
		int[] a1 = {3, 5, 1, 7, 9, 0, 9, -3, 10};
		assertEquals(810, maxProd(a1));
		
		int[] a2 = {-5, -30000, -12};
		assertEquals(-1_800_000, maxProd(a2));
		
		int[] a3 = {-100, -10, 1, 2};
		assertEquals(-100 * -10 * 2, maxProd(a3));
		
		int[] a4 = {-100, -10, -1, -2};
		assertEquals(-10 * -1 * -2, maxProd(a4));
		
		int[] a5 = {-100, -10, -1, 2};
		assertEquals(-100 * -10 * 2, maxProd(a5));
		
		int[] a6 = {-100, -10, -1, -2, 0};
		assertEquals(-10 * -1 * 0, maxProd(a6));
		
		int[] a7 = {-100, -10, -1, 2, 0};
		assertEquals(-100 * -10 * 2, maxProd(a7));
	
		int[] a8 = {-100, 0, 3, 2};
		assertEquals(0, maxProd(a8));
	
		int[] a9 = {-100, -100, 0, 3, 2, 2, 2, 2, 2};
		assertEquals(-100 * -100 * 3, maxProd(a9));
	
		int[] a10 = {-10, -100, 0, 30, 200, 2, 2, 2, 2};
		assertEquals(200 * -100 * -10, maxProd(a10));
		
		int[] a11 = {-1,-2, 0, 0, 1};
		assertEquals(-1 * -2 * 1, maxProd(a11));
		
		int[] a12 = {-1, -2, 0, 2, 3};
		assertEquals(-1 * -2 * 3, maxProd(a12));
		
		int[] a13 = {-30_000,-30_000,-30_000};
		assertEquals(-30_000L * -30_000 * -30_000, maxProd(a13));
		
		int[] a14 = {-30,30,30};
		assertEquals(-30L * 30 * 30, maxProd(a14));
		
		int[] a14_1 = {-30_000,30_000,30_000};
		assertEquals(-30_000L * 30_000 * 30_000, maxProd(a14_1));
		
		int[] a15 = {30_000,30_000,30_000};
		assertEquals(30_000L * 30_000 * 30_000, maxProd(a15));
		
		for(int i = 0; i < 1_000_000; i++){
			int[] a = new int[10];
			for(int j = 0; j < 10; j++){
				a[j] = (int) (Math.random() * 200);
				if(a[j] < -1){
					a[j] *= -1;
				}
				else{
					a[j] -= 100;
				}
			}
			assertEquals(maxProdBf(a), maxProd(a));
			if(maxProdBf(a) != maxProd(a)){
				printArray(a);
			}
		}
		
	}
	
	public static long maxProdBf(int[] a){
		long max = Integer.MIN_VALUE;
		for(int i = 0; i < a.length - 2; i++){
			for(int j = i + 1; j < a.length - 1; j++){
				for(int k = j + 1; k < a.length; k++){
					max = Math.max(max, 1L * a[i] * a[j] * a[k]);
				}
			}
		}
		return max;
	}
	
	public static void printArray(int[] a){
		for(int i = 0; i < a.length; i++){
			System.out.print(a[i] + " ");
		}
	}
}
