package com.moodaye.playground.programmerSchool;

import static org.junit.Assert.*;
import static com.moodaye.playground.programmerSchool.PalindromicTime.*;

import org.junit.Test;

public class PalindromicTimeTest {
	
	@Test
	public void test3(){
		for (int h = 0; h < 24; h++){
			for (int m = 0; m < 60; m++){
				String hhmm = (h < 10 ? "0" + String.valueOf(h) : String.valueOf(h)) + 
    						  (m < 10 ? "0" + String.valueOf(m) : String.valueOf(m));
				assertEquals(finishTime(hhmm), finishTimeSimple(hhmm));
			}
		}
		
	}

	@Test
	public void test() {
		assertEquals("01:10", PalindromicTime.finishTime("00:00"));
		assertEquals("01:10", PalindromicTime.finishTime("00:01"));
		
		assertEquals("01:10", PalindromicTime.finishTime("01:00"));
		assertEquals("01:10", PalindromicTime.finishTime("01:01"));
		assertEquals("01:10", PalindromicTime.finishTime("01:09"));
		assertEquals("02:20", PalindromicTime.finishTime("01:10"));
		assertEquals("02:20", PalindromicTime.finishTime("01:59"));
		
		assertEquals("02:20", PalindromicTime.finishTime("02:00"));
		assertEquals("02:20", PalindromicTime.finishTime("02:02"));
		assertEquals("02:20", PalindromicTime.finishTime("02:19"));
		assertEquals("03:30", PalindromicTime.finishTime("02:20"));
		assertEquals("03:30", PalindromicTime.finishTime("02:59"));
		
		assertEquals("03:30", PalindromicTime.finishTime("03:00"));
		assertEquals("03:30", PalindromicTime.finishTime("03:29"));
		assertEquals("04:40", PalindromicTime.finishTime("03:30"));
		assertEquals("04:40", PalindromicTime.finishTime("03:31"));
		assertEquals("04:40", PalindromicTime.finishTime("03:59"));
		
		assertEquals("04:40", PalindromicTime.finishTime("04:00"));
		assertEquals("04:40", PalindromicTime.finishTime("04:39"));
		assertEquals("05:50", PalindromicTime.finishTime("04:40"));
		assertEquals("05:50", PalindromicTime.finishTime("04:41"));
		assertEquals("05:50", PalindromicTime.finishTime("04:59"));
		
		assertEquals("05:50", PalindromicTime.finishTime("05:00"));
		assertEquals("05:50", PalindromicTime.finishTime("05:04"));
		assertEquals("05:50", PalindromicTime.finishTime("05:49"));
		assertEquals("10:01", PalindromicTime.finishTime("05:50"));
		assertEquals("10:01", PalindromicTime.finishTime("05:51"));
		assertEquals("10:01", PalindromicTime.finishTime("05:59"));
		
		assertEquals("10:01", PalindromicTime.finishTime("06:00"));
		assertEquals("10:01", PalindromicTime.finishTime("06:45"));
		assertEquals("10:01", PalindromicTime.finishTime("06:59"));
		
		assertEquals("10:01", PalindromicTime.finishTime("07:00"));
		assertEquals("10:01", PalindromicTime.finishTime("07:45"));
		assertEquals("10:01", PalindromicTime.finishTime("07:59"));
		
		assertEquals("10:01", PalindromicTime.finishTime("08:00"));
		assertEquals("10:01", PalindromicTime.finishTime("08:45"));
		assertEquals("10:01", PalindromicTime.finishTime("08:59"));
		
		assertEquals("10:01", PalindromicTime.finishTime("09:00"));
		assertEquals("10:01", PalindromicTime.finishTime("09:45"));
		assertEquals("10:01", PalindromicTime.finishTime("09:59"));
		
		assertEquals("10:01", PalindromicTime.finishTime("10:00"));
		assertEquals("11:11", PalindromicTime.finishTime("10:01"));
		assertEquals("11:11", PalindromicTime.finishTime("10:30"));
		assertEquals("11:11", PalindromicTime.finishTime("10:59"));
		
		assertEquals("11:11", PalindromicTime.finishTime("11:00"));
		assertEquals("11:11", PalindromicTime.finishTime("11:10"));
		assertEquals("12:21", PalindromicTime.finishTime("11:11"));
		assertEquals("12:21", PalindromicTime.finishTime("11:12"));
		assertEquals("12:21", PalindromicTime.finishTime("11:59"));
		
		assertEquals("12:21", PalindromicTime.finishTime("12:00"));
		assertEquals("12:21", PalindromicTime.finishTime("12:20"));
		assertEquals("13:31", PalindromicTime.finishTime("12:21"));
		assertEquals("13:31", PalindromicTime.finishTime("12:22"));
		assertEquals("13:31", PalindromicTime.finishTime("12:59"));
		
		assertEquals("13:31", PalindromicTime.finishTime("13:00"));
		assertEquals("13:31", PalindromicTime.finishTime("13:30"));
		assertEquals("14:41", PalindromicTime.finishTime("13:31"));
		assertEquals("14:41", PalindromicTime.finishTime("13:32"));
		assertEquals("14:41", PalindromicTime.finishTime("13:59"));
		
		assertEquals("14:41", PalindromicTime.finishTime("14:00"));
		assertEquals("14:41", PalindromicTime.finishTime("14:40"));
		assertEquals("15:51", PalindromicTime.finishTime("14:41"));
		assertEquals("15:51", PalindromicTime.finishTime("14:42"));
		assertEquals("15:51", PalindromicTime.finishTime("14:59"));
		
		assertEquals("15:51", PalindromicTime.finishTime("15:00"));
		assertEquals("15:51", PalindromicTime.finishTime("15:50"));
		assertEquals("20:02", PalindromicTime.finishTime("15:51"));
		assertEquals("20:02", PalindromicTime.finishTime("15:52"));
		assertEquals("20:02", PalindromicTime.finishTime("15:59"));
		
		assertEquals("20:02", PalindromicTime.finishTime("16:00"));
		assertEquals("20:02", PalindromicTime.finishTime("16:50"));
		assertEquals("20:02", PalindromicTime.finishTime("16:59"));
		
		assertEquals("20:02", PalindromicTime.finishTime("17:00"));
		assertEquals("20:02", PalindromicTime.finishTime("17:50"));
		assertEquals("20:02", PalindromicTime.finishTime("17:59"));
		
		assertEquals("20:02", PalindromicTime.finishTime("18:00"));
		assertEquals("20:02", PalindromicTime.finishTime("18:50"));
		assertEquals("20:02", PalindromicTime.finishTime("18:59"));
		
		assertEquals("20:02", PalindromicTime.finishTime("19:00"));
		assertEquals("20:02", PalindromicTime.finishTime("19:01"));
		assertEquals("20:02", PalindromicTime.finishTime("19:59"));
		
		assertEquals("20:02", PalindromicTime.finishTime("20:00"));
		assertEquals("20:02", PalindromicTime.finishTime("20:01"));
		assertEquals("21:12", PalindromicTime.finishTime("20:02"));
		assertEquals("21:12", PalindromicTime.finishTime("20:59"));
		assertEquals("21:12", PalindromicTime.finishTime("20:30"));
		
		assertEquals("21:12", PalindromicTime.finishTime("21:00"));
		assertEquals("21:12", PalindromicTime.finishTime("21:11"));
		assertEquals("22:22", PalindromicTime.finishTime("21:12"));
		assertEquals("22:22", PalindromicTime.finishTime("21:13"));
		assertEquals("22:22", PalindromicTime.finishTime("21:59"));
		
		assertEquals("22:22", PalindromicTime.finishTime("22:00"));
		assertEquals("22:22", PalindromicTime.finishTime("22:21"));
		assertEquals("23:32", PalindromicTime.finishTime("22:22"));
		assertEquals("23:32", PalindromicTime.finishTime("22:23"));
		assertEquals("23:32", PalindromicTime.finishTime("22:59"));
		
		assertEquals("23:32", PalindromicTime.finishTime("23:00"));
		assertEquals("23:32", PalindromicTime.finishTime("23:31"));
		assertEquals("00:00", PalindromicTime.finishTime("23:32"));
		assertEquals("00:00", PalindromicTime.finishTime("23:33"));
		assertEquals("00:00", PalindromicTime.finishTime("23:59"));
	}
	
	@Test
	public void test2() {
		assertEquals("01:10", PalindromicTime.finishTimeSimple("00:00"));
		assertEquals("01:10", PalindromicTime.finishTimeSimple("00:01"));
		
		assertEquals("01:10", PalindromicTime.finishTimeSimple("01:00"));
		assertEquals("01:10", PalindromicTime.finishTimeSimple("01:01"));
		assertEquals("02:20", PalindromicTime.finishTimeSimple("01:10"));
		assertEquals("02:20", PalindromicTime.finishTimeSimple("01:59"));
		
		assertEquals("02:20", PalindromicTime.finishTimeSimple("02:00"));
		assertEquals("02:20", PalindromicTime.finishTimeSimple("02:02"));
		assertEquals("03:30", PalindromicTime.finishTimeSimple("02:20"));
		assertEquals("03:30", PalindromicTime.finishTimeSimple("02:59"));
		
		assertEquals("05:50", PalindromicTime.finishTimeSimple("05:00"));
		assertEquals("05:50", PalindromicTime.finishTimeSimple("05:04"));
		assertEquals("05:50", PalindromicTime.finishTimeSimple("05:49"));
		assertEquals("10:01", PalindromicTime.finishTimeSimple("05:50"));
		assertEquals("10:01", PalindromicTime.finishTimeSimple("05:52"));
		
		assertEquals("10:01", PalindromicTime.finishTimeSimple("06:45"));
		assertEquals("10:01", PalindromicTime.finishTimeSimple("06:49"));
		
		assertEquals("13:31", PalindromicTime.finishTimeSimple("12:34"));
		
		assertEquals("20:02", PalindromicTime.finishTimeSimple("15:55"));
		
		assertEquals("20:02", PalindromicTime.finishTimeSimple("17:33"));
		assertEquals("20:02", PalindromicTime.finishTimeSimple("20:00"));
		assertEquals("20:02", PalindromicTime.finishTimeSimple("20:01"));
		assertEquals("21:12", PalindromicTime.finishTimeSimple("20:02"));
		assertEquals("22:22", PalindromicTime.finishTimeSimple("22:21"));
		assertEquals("23:32", PalindromicTime.finishTimeSimple("22:22"));
		
		assertEquals("00:00", PalindromicTime.finishTimeSimple("23:49"));
		assertEquals("00:00", PalindromicTime.finishTimeSimple("23:59"));
	}
}
