package com.moodaye.playground.programmerSchool;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

public class Biathlon {

	void solve(Scanner in, PrintWriter out){
		boolean[] targetHit = new boolean[5];
		for(int i = 0; i < 5; i++){
			int x = in.nextInt();
			int y = in.nextInt();
			int y2 = (y - 0) * (y - 0);
			
			if(!targetHit[0] && ((x - 0)*(x - 0) + y2) <= 100){
				targetHit[0] = true;
			}
			else if(!targetHit[1] && ((x - 25)*(x - 25) + y2) <= 100){
				targetHit[1] = true;
			}
			else if(!targetHit[2] && ((x - 50)*(x - 50) + y2) <= 100){
				targetHit[2] = true;
			}
			else if(!targetHit[3] && ((x - 75)*(x - 75) + y2) <= 100){
				targetHit[3] = true;
			}
			else if(!targetHit[4] && ((x - 100)*(x - 100) + y2) <= 100){
				targetHit[4] = true;
			}
		}
			
		int count = 0;
		for(int i = 0; i < 5; i++){
			if(targetHit[i] == true){
				count++;
			}
		}
		out.println(count);
	}

	public static void main(String[] args) {
		new Biathlon().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
