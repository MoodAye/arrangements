package com.moodaye.playground.programmerSchool;

import static org.junit.Assert.*;

import static com.moodaye.playground.programmerSchool.EquationFifthGrade.*;

import org.junit.AfterClass;
import org.junit.Test;

public class EquationFifthGradeTest {

	@Test
	public void test() {
		assertEquals(4, getAnswer("1+x=5"));
		assertEquals(4, getAnswer("x+1=5"));
		assertEquals(0, getAnswer("0+x=0"));
		assertEquals(0, getAnswer("0-x=0"));
		assertEquals(3, getAnswer("3-x=0"));
		assertEquals(18, getAnswer("x-9=9"));
		assertEquals(18, getAnswer("9+9=x"));
		assertEquals(0, getAnswer("9-9=x"));
		assertEquals(-8, getAnswer("1-9=x"));
	}

}
