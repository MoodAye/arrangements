package com.moodaye.playground.programmerSchool;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

/** Programmer's School Problem #159 */
public class InversePermutation {
	void solve(Scanner in, PrintWriter out){

		int n = in.nextInt();
		int[] ip = new int[n + 1];
		for (int i = 1; i <= n; i++) {
			ip[in.nextInt()] = i;
		}
		
		for(int i = 1; i < ip.length; i++){
			out.printf("%d ", ip[i]);
		}
	}
	
	public static void main(String[] args) {
		new InversePermutation().run();
	}
	
	void run(){
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)){
			solve(in, out);
		}
	}
}