package com.moodaye.playground.programmerSchool;

import static org.junit.Assert.*;

import java.util.Set;

import org.junit.Test;

public class EquationTest {

	@Test
	public void test() {
		
		Set<Integer> roots1 = Equation.getRootsCubicEquation(-1, 3, 0, 0);
		Equation.getRootsCubicEquationSimpleVersion(-1, 3, 0, 0);
		assertEquals(2,roots1.size());
		assertTrue(roots1.contains(new Integer(0)));
		assertTrue(roots1.contains(new Integer(3)));
		
		Set<Integer> roots2 = Equation.getRootsCubicEquation(3, -15, 18, 0);
		Equation.getRootsCubicEquationSimpleVersion(3, -15, 18, 0);
		assertEquals(3,roots2.size());
		assertTrue(roots2.contains(new Integer(0)));
		assertTrue(roots2.contains(new Integer(2)));
		assertTrue(roots2.contains(new Integer(3)));
		
		Set<Integer> roots3 = Equation.getRootsCubicEquation(1, -7, -33, 135);
		Equation.getRootsCubicEquationSimpleVersion(1, -7, -33, 135);
		assertEquals(3,roots3.size());
		assertTrue(roots3.contains(new Integer(-5)));
		assertTrue(roots3.contains(new Integer(3)));
		assertTrue(roots3.contains(new Integer(9)));
		
		Set<Integer> roots4 = Equation.getRootsCubicEquation(1, 1, 1, 1);
		Equation.getRootsCubicEquationSimpleVersion(1, 1, 1, 1);
		assertEquals(1,roots4.size());
		assertTrue(roots4.contains(new Integer(-1)));
		
	}

}
