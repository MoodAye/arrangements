package com.moodaye.playground.programmerSchool.October2017;
import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #253 */
public class StrikingClock {
	void solve(Scanner in, PrintWriter out) {
		int stH = in.nextInt();
		int stM = in.nextInt();
		int endH = in.nextInt();
		int endM = in.nextInt();
		out.println(countStrikes(stH, stM, endH, endM));
	}
	
	public static int countStrikes(int stH, int stM, int endH, int endM){
		int cnt = 0;
		endH = (endH * 60 + endM < stH * 60 + stM) ? endH + 24 : endH;
		
		for(int h = stH + 1; h <= endH; h++){
			cnt += (h % 12 == 0 ? 12 : h % 12) + 1;
		}
	
		if(stM < 30){
			cnt++;
		}
		if(endM < 30){
			cnt--;
		}
		return cnt;
	}
	
	public static void main(String args[]) {
		new StrikingClock().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
	
	public static int countStrikesByMinute(int stH, int stM, int endH, int endM){
		int cnt = 0;
		int h = stH;
		int m = stM;
		
		while(h != endH || m != endM){
			if(m == 30 && (m != stM || h != stH)){
				cnt++;
			}
			else if(m == 0 && (m != stM || h != stH)){
				cnt += h % 12 == 0 ? 12 : h % 12;
			}
			m++;
			h += m / 60;
			h %= 24;
			m %= 60;
		}
		return cnt;
	}
	

}
