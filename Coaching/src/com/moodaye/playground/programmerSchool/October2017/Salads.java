package com.moodaye.playground.programmerSchool.October2017;
import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;
import java.math.BigInteger;

/** Programmer's School #513 */
public class Salads {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		long sum = 0;
		for(int r = 2; r <= n; r++){
			sum += nCr(n,r);
		}
		out.println(sum);
	}
	
	public static long nCr(int n, int r){
		int end = r > n - r ? r : n - r;
		BigInteger num = BigInteger.valueOf(1);
		BigInteger den =  BigInteger.valueOf(1);
		for(int i = n; i > end; i--){
			num = num.multiply(BigInteger.valueOf(i));
			den = den.multiply(BigInteger.valueOf(i - end));
		}
		return num.divide(den).longValue();
	}

	public static void main(String args[]) {
		new Salads().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
