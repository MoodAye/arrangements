package com.moodaye.playground.programmerSchool.October2017;

import static org.junit.Assert.*;
import static com.moodaye.playground.programmerSchool.October2017.SimplificationOfNumbers.*;

import org.junit.Test;

public class SimplificationOfNumbersTest {

	@Test
	public void test() {
		for (int b = 2; b <= 36; b++) {
			assertEquals(2, comp(1, b));
		}

		assertEquals(2, comp(2, 3));
		assertEquals("FFFF".length() + 1, comp(65_535, 16));
		assertEquals("E30UWM".length() + 6, comp(851605366, 36));
		assertEquals("111011100110101100100111111111".length() + 2, comp(999_999_999, 2));
		assertEquals("2120200200021010000".length() + 3, comp(999_999_999, 3));
	}

}
