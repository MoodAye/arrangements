package com.moodaye.playground.programmerSchool.October2017;

import static org.junit.Assert.*;
import static com.moodaye.playground.programmerSchool.October2017.NumbersWithoutEqualDigits.*;

import org.junit.Test;

public class NumbersWithoutEqualDigitsTest {

	@Test
	public void test() {
		for(int i = 1; i < 100; i++){
			if(i % 11 == 0){
				assertTrue(anyDigitEqual(i));
			}
			else{
				assertTrue(!anyDigitEqual(i));
			}
		}
		assertTrue(anyDigitEqual(100));
		assertTrue(anyDigitEqual(101));
		assertTrue(anyDigitEqual(111));
		assertTrue(anyDigitEqual(2032));
		assertTrue(anyDigitEqual(10111));
		assertTrue(anyDigitEqual(8977));
		
		assertTrue(!anyDigitEqual(1234));
		assertTrue(!anyDigitEqual(234));
	}
}
