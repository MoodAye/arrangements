package com.moodaye.playground.programmerSchool.October2017;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #501 */
// this is tougher that i expected.  There should be a much simpler
// set of equations - i could not figure it out.  
// At this point - need the test case on which this is failing.
public class Construction {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		Plot[] plots = new Plot[n];
		for (int i = 0; i < n; i++) {
			plots[i] = new Plot(in.nextInt(), in.nextInt(), in.nextInt(), in.nextInt());
		}
		Plot site = new Plot(in.nextInt(), in.nextInt(), in.nextInt(), in.nextInt());
		int sum = 0;
		for(Plot plot : plots){
			sum += intersection(plot, site);
		}
		out.println(sum);
	}

	/**
	 * @param p1
	 *            plot
	 * @param p2
	 *            construction site
	 * @return area
	 */
	public static int intersection(Plot p1, Plot p2) {
		// case 1 = rectangles don't intersect
		if ((p1.x1 <= p2.x1 && p1.x2 <= p2.x1) || (p1.x1 >= p2.x2 && p1.x2 >= p2.x2)
				|| (p1.y1 <= p2.y1 && p1.y2 <= p2.y1) || (p1.y1 >= p2.y2 && p1.y2 >= p2.y2)) {
			return 0;
		}

		// case 2 - plot/site completely enclosing the other
		if (p1.x1 >= p2.x1 && p1.x2 <= p2.x2 && p1.y1 >= p2.y1 && p1.y2 <= p2.y2) {
			return p1.area();
		}
		if (p1.x1 <= p2.x1 && p1.x2 >= p2.x2 && p1.y1 <= p2.y1 && p1.y2 >= p2.y2) {
			return p2.area();
		}

		// case 3 - partial overlap
		// cross
		if(p1.x2 >= p2.x2 && p1.x1 <= p2.x1 && p1.y2 <= p2.y2 && p1.y1 >= p2.y1){
			return (p1.y2 - p1.y1) * (p2.x2 - p2.x1);
		}
		if(p1.x2 <= p2.x2 && p1.x1 >= p2.x1 && p1.y2 >= p2.y2 && p1.y1 <= p2.y1){
			return (p1.x2 - p1.x1) * (p2.y2 - p2.y1);
		}
		
		// bottom left corner of p2
		if(p2.x1 < p1.x2 && p2.y1 < p1.y2 && p2.x2 > p1.x2 && p2.y2 > p1.y2){
			return (p1.x2 - p2.x1) * (p1.y2 - p2.y1);
		}
		// bottom right corner of p2
		if(p1.x1 > p2.x1 && p1.x2 > p2.x2 && p1.y1 < p2.y1 && p1.y2 < p2.y2){
			return (p2.x2 - p1.x1) * (p1.y2 - p2.y1);
		}
		// top right corner of p2
		if(p1.x1 > p2.x1 && p1.y1 > p2.y1 && p1.x2 > p2.x2 && p1.y2 > p2.y2){
			return (p2.x2 - p1.x1) * (p2.y2 - p1.y1);
		}
		// top left corner of p2
		if(p1.x1 < p2.x2 && p1.y1 > p2.y1 && p1.x2 < p2.x2 && p1.y2 > p2.y2){
			return (p1.x2 - p2.x1) * (p2.y2 - p1.y1);
		}
		
		// if complete left side of site is overlapped
		if(p1.x1 < p2.x1 && p1.y1 < p2.y1 && p1.x2 > p2.x1 && p1.y2 > p2.y2){
			return (p1.x2 - p2.x1) * (p2.y2 - p2.y1);
		}
		
		// if complete right side of site is overlapped
		if(p1.y1 < p2.y1 && p1.x1 < p2.x2 && p1.x2 > p2.x2 && p1.y2 > p2.y2){
			return (p2.x2 - p1.x1) * (p2.y2 - p2.y1);
		}
		
		// if complete bottom side of site is overlapped
		if(p1.y1 < p2.y1 && p1.x1 < p2.x2 && p1.x2 > p2.x2 && p1.y2 < p2.y2){
			return (p2.x2 - p2.x1) * (p1.y2 - p2.y1);
		}
		
		// if complete top side of site is overlapped
		if(p1.y1 > p2.y1 && p1.x1 < p2.x1 && p1.x2 > p2.x2 && p1.y2 > p2.y2){
			return (p2.x2 - p2.x1) * (p2.y2 - p1.y1);
		}
		
		return 0;
	}

	public static void main(String args[]) {
		new Construction().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}

class Plot {
	int x1;
	int x2;
	int y1;
	int y2;

	// assume bottom left and top right co-ordinates are provided.
	Plot(int x1, int y1, int x2, int y2) {
		this.x1 = x1;
		this.y1 = y1;
		this.x2 = x2;
		this.y2 = y2;

		ensureLeftToRight();
		ensureBottomToTop();
	}

	// use opposite corners from given corners if
	// needed to ensure all plots are depicted the same way
	private void ensureLeftToRight() {
		if ((y1 - y2) * (x1 - x2) < 0) {
			int temp = x1;
			x1 = x2;
			x2 = temp;
		}
	}

	private void ensureBottomToTop() {
		if (x1 > x2) {
			int temp = x1;
			this.x1 = x2;
			this.x2 = temp;
			temp = y1;
			this.y1 = y2;
			this.y2 = temp;
		}
	}

	public int area() {
		return (x2 - x1) * (y2 - y1);
	}
}
