package com.moodaye.playground.programmerSchool.October2017;

import java.util.Scanner;
import java.util.Arrays;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #670 */
public class NumbersWithoutEqualDigits {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int on = 0;
		while(n != 0){
			on++;
			while(anyDigitEqual(on)){
				on++;
			}
			n--;
		}
		out.println(on);
	}
	
	public static boolean anyDigitEqual(int n){
		char[] nc = String.valueOf(n).toCharArray();
		Arrays.sort(nc);
		int f = nc[0];
		for(int i = 1; i < nc.length; i++){
			if(f == nc[i]){
				return true;
			}
			f = nc[i];
		}
		return false;
	}

	public static void main(String args[]) {
		new NumbersWithoutEqualDigits().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
