package com.moodaye.playground.programmerSchool.October2017;

import java.util.Scanner;
import java.util.Arrays;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #689 */
public class SimplificationOfNumbers {

	void solve(Scanner in, PrintWriter out) {
		int tests = in.nextInt();
		for (int t = 0; t < tests; t++) {
			int n = in.nextInt();
			int min = Integer.MAX_VALUE;
			int radix = 0;
			String nb = "";
			for (int b = 2; b <= 36; b++) {
				int c = comp(n, b);
				if (min > c) {
					min = c;
					radix = b;
					nb = Integer.toString(n, b);
				}
			}
			out.println(radix + " " + nb.toUpperCase());
		}
	}

	/** returns complexity (as defined in the problem statement)
	 * 	complexity = length of string + number of unique chars 
	 * @param n number
	 * @param b radix
	 * @return complexity
	 */
	public static int comp(int n, int b) {
		String ns = Integer.toString(n, b);
		int sum = ns.length();
		char[] c = ns.toCharArray();
		Arrays.sort(c);
		sum++; // for the first char 
		for (int i = 1; i < c.length; i++) {
			if (c[i - 1] == c[i]) {
				continue;
			}
			sum++;
		}
		return sum;
	}

	public static void main(String args[]) {
		new SimplificationOfNumbers().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
