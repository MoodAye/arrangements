package com.moodaye.playground.programmerSchool.October2017;
import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #409 */
public class RailRoad {

	// use the fact that the area under the lines plotted by pts[i] remains constant
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		double[] pts = new double[n];
		double area = 0;
	
		// area = area of trapezoid under each segment.
		// length = 1. Each point except first and last
		// get summed twice * 1/2.
		for (int i = 0; i < pts.length; i++) {
			pts[i] = in.nextDouble();
			area += pts[i] * 1;
		}
		area = area - 0.5 * (pts[0] + pts[n-1]);
		
		// precision of double is 15 decimal digits?
		// not sure i understand this ... but it seems from the 
		// javadoc / wikipedia that this is true.  Is there a reference
		// you can provide to help me understand precision in java?
		double x = area / (n - 1);
		out.println(x);
	}

	public static void main(String args[]) {
		new RailRoad().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}

}
