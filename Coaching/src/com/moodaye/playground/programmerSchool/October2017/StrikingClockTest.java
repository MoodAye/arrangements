package com.moodaye.playground.programmerSchool.October2017;

import static org.junit.Assert.*;
import static com.moodaye.playground.programmerSchool.October2017.StrikingClock.*;

import org.junit.Test;

public class StrikingClockTest {
	/** strike 1 for every 1/2 hour */
	private final static int HFHR = 1;

	@Test
	public void test() {
		// less than 1 hour
		assertEquals(HFHR, countStrikes(5, 10, 5, 31));
		assertEquals(0, countStrikes(5, 10, 5, 29));
		assertEquals(0, countStrikes(0, 0, 0, 0));
		assertEquals(HFHR, countStrikes(0, 0, 0, 30));
		assertEquals(HFHR, countStrikes(0, 0, 0, 59));
		
		// more than 1 hour - simple
		assertEquals(HFHR + 1 + HFHR, countStrikes(0, 0, 1, 59));
		assertEquals(HFHR + 5 + HFHR + 6 + HFHR + 7 + HFHR + 8, countStrikes(4, 0, 8, 23));
		
		// ahead of noon - simple
		assertEquals(HFHR + 2 + HFHR + 3 + HFHR + 4 + HFHR + 5 + HFHR + 6 
	      		   + HFHR + 7 + HFHR + 8 + HFHR + 9 + HFHR + 10 + HFHR + 11, countStrikes(13, 10, 23, 22)); 
		assertEquals(HFHR + 1 + HFHR + 2, countStrikes(12, 10, 14, 22));  
		
		// cross noon - simple
		assertEquals(HFHR + 11 + HFHR + 12, countStrikes(10, 10, 12, 22));  
		
		// cross midnight - simple
		assertEquals(HFHR + 9 + HFHR + 10 + HFHR + 11 + HFHR + 12, countStrikes(20, 10, 0, 22)); 
		
		// start time less than end time; end minutes less than start minutes
		assertEquals(HFHR + 9 + HFHR + 10 + HFHR + 11 + HFHR + 12
				   + HFHR + 1 + HFHR + 2 + HFHR + 3 + HFHR + 4 + HFHR + 5, countStrikes(20, 10, 5, 5)); 
		
	}
	
}
