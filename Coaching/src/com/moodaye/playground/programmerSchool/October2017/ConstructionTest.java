package com.moodaye.playground.programmerSchool.October2017;

import static org.junit.Assert.*;
import static com.moodaye.playground.programmerSchool.October2017.Construction.*;

import org.junit.Test;
//todo - flip all
public class ConstructionTest {

	@Test
	public void testNoOverlap() {
		Plot site = new Plot(25,30,55,65);
		Plot leftBelow = new Plot(10,10,20,20);
		Plot below = new Plot(32,10,50,20);
		Plot belowRight = new Plot(60,10,70,20);
		Plot right = new Plot(60,40,70,50);
		Plot rightAbove = new Plot(60,70,70,80);
		Plot above = new Plot(32,70,50,80);
		Plot aboveLeft = new Plot(10,60,20,70);
		
		assertEquals(0, intersection(leftBelow, site));
		assertEquals(0, intersection(below, site));
		assertEquals(0, intersection(belowRight, site));
		assertEquals(0, intersection(right, site));
		assertEquals(0, intersection(rightAbove, site));
		assertEquals(0, intersection(above, site));
		assertEquals(0, intersection(aboveLeft, site));
		
		//use the other opposite corners (top left; bottom right) for a few
		leftBelow = new Plot(20,20,10,10);
		below = new Plot(50,20,32,10);
		assertEquals(0, intersection(leftBelow, site));
		assertEquals(0, intersection(below, site));
	}
	
	@Test
	public void testCompleteOverlap() {
		Plot site = new Plot(25,30,55,65);
		Plot insidePlot = new Plot(32,40,50,60);
		Plot encompassSite = new Plot(24,29,56,66);
		Plot insideCornerLeft = new Plot(25,30,30,40);
		assertEquals(insidePlot.area(), intersection(insidePlot, site));
		assertEquals(site.area(), intersection(encompassSite, site));
		assertEquals(site.area(), intersection(site, site));
		assertEquals(insideCornerLeft.area(), intersection(insideCornerLeft, site));
		
		Plot fromPS = new Plot(2,2,1,1);
		site = new Plot(1,1,2,2);
		assertEquals(1, intersection(fromPS, site));
	}
	
	@Test
	public void testPartialOverlap(){
		Plot site = new Plot(25,30,55,65);
		Plot crossSiteHorizontal = new Plot(20,40,60,50);
		Plot crossSiteVertical = new Plot(30,10,32,80);
		assertEquals((32-30)*(65-30), intersection(crossSiteVertical, site));
		assertEquals((55-25)*(50-40), intersection(crossSiteHorizontal, site));
		
		Plot bottomLeftCornerOverlap = new Plot(20,25,30,40);
		Plot bottomRightCornerOverlap = new Plot(50,20,60,40);
		Plot topRightCornerOverLap = new Plot(50,60,60,70);
		Plot topLeftCornerOverlap = new Plot(20,60,27,68);
		assertEquals((30-25)*(40-30), intersection(bottomLeftCornerOverlap, site));
		assertEquals((55-50)*(40-30), intersection(bottomRightCornerOverlap, site));
		assertEquals((55-50)*(65-60), intersection(topRightCornerOverLap, site));
		assertEquals((27-25)*(65-60), intersection(topLeftCornerOverlap, site));
	
		//complete left side
		Plot c = new Plot(20,25,27,68);
		assertEquals((site.y2 - site.y1)*(c.x2 - site.x1), intersection(c, site));
		
		//complete right side
		Plot d = new Plot(50,20,60,70);
		assertEquals((site.y2 - site.y1)*(site.x2 - d.x1), intersection(d, site));
		
		//complete bottom side
		Plot e = new Plot(20,25,60,40);
		assertEquals((site.x2 - site.x1)*(e.y2 - site.y1), intersection(e, site));
		
		//complete top side
		Plot f = new Plot(20,60,60,70);
		assertEquals((site.x2 - site.x1)*(site.y2 - f.y1), intersection(f, site));
	}
	
	@Test
	public void testMisc(){
		Plot site = new Plot(25,30,55,65);
		// perfect overlap with left side of site
		Plot a = new Plot(25,30,30,65);
		assertEquals((site.y2 - site.y1)*(a.x2 - a.x1), intersection(a, site));
		
		// partial overlap with left side of site; identical lower left corner
		Plot b = new Plot(25,30,30,40);
		assertEquals((b.y2 - b.y1)*(b.x2 - b.x1), intersection(b, site));
		
		// perfect overlap with right side of site
		Plot c = new Plot(45,30,55,65);
		assertEquals((c.x2 - c.x1)*(site.y2 - site.y1), intersection(c, site));
		
		// partial overlap with left side of site
		Plot d = new Plot(45,30,55,40);
		assertEquals((d.y2 - d.y1)*(d.x2 - d.x1), intersection(d, site));
		
		// no overlap - plot below site
		Plot e = new Plot(10,10,70,20);
		assertEquals(0, intersection(e, site));
		
		// no overlap - plot below site
		Plot f = new Plot(10,10,40,30);
		assertEquals(0, intersection(f, site));
		
		// no overlap - plot below site
		Plot g = new Plot(25,10,55,30);
		assertEquals(0, intersection(g, site));
		
		// no overlap - plot right of site
		Plot h = new Plot(60,10,70,80);
		assertEquals(0, intersection(h, site));
		
		// no overlap - plot right of site
		Plot i = new Plot(55,30,65,65);
		assertEquals(0, intersection(i, site));
		
		// no overlap - plot above the site
		Plot j = new Plot(25,65,55,70);
		assertEquals(0, intersection(j, site));
		
		// no overlap - plot left of site
		Plot k = new Plot(20,30,25,65);
		assertEquals(0, intersection(k, site));
		
		// large overlap - except small portion of left vertical.
		Plot m = new Plot(30,25,60,70);
		assertEquals((site.y2 - site.y1)*(site.x2 - m.x1), intersection(m, site));
		
		// large overlap - except small portion of bottom horizontal
		Plot n = new Plot(20,35,70,80);
		assertEquals((site.y2 - n.y1)*(site.x2 - site.x1), intersection(n, site));
		
		// large overlap - except small portion of bottom horizontal
		Plot p = new Plot(20,35,70,80);
		assertEquals((site.y2 - p.y1)*(site.x2 - site.x1), intersection(p, site));
		
	}
	

}
