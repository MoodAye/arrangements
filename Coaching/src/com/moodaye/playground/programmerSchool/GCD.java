package com.moodaye.playground.programmerSchool;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

/** Programmer School - Problem # 148 */
public class GCD {
	void solve(Scanner in, PrintWriter out) {
		int A = in.nextInt();
		int B = in.nextInt();
		out.println(gcd2(A, B));
	}

	/** Slightly more efficient version from Fyodor M. 
	 *  relative to gcd2 below. It has one less comparison per 
	 *  loop - though it leads to one more modulo in some 
	 *  cases (when t = 1 - i.e., whenever the answer is 1). 
	 *  Also works when one argument is zero (important for some 
	 *  geometry applications). 
	 */
	public static int gcd(int a, int b) {
		while (b != 0) {
			int t = a % b;
			a = b;
			b = t;
		}
		return a;
	}
	
	public static int gcd2(int A, int B) {
		int R = Integer.MAX_VALUE;

		// wanted to test A > B
		// but realized formula below addresses this

		// Euclid's formula
		while (R != 1) {
			R = A % B;
			if (R == 0) {
				return B;
			}
			A = B;
			B = R;
		}
		return 1;
	}

	public static void main(String args[]) {
		new GCD().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}

}
