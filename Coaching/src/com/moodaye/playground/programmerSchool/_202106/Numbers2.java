package com.moodaye.playground.programmerSchool._202106;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

// Problem 536
// getting different ans from nov solution for
// 10 100 5
// 1231231231

// 6 100 6
// 123123
public class Numbers2 {
	
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int max = in.nextInt();
		int limit = in.nextInt();

		String line = in.next();
		char[] numbers = line.toCharArray();
		long[] cnt = new long[n];

		long mod = 1L;
		for (int i = 0; i < limit; i++) {
			mod *= 10;
		}

		if (numbers[0] - '0' > max) {
			out.println(0);
			return;
		}
		cnt[0] = 1;  
		
		int lenMax = String.valueOf(max).length();
		int bLen = lenMax - 2;

		for (int i = 1; i < n && numbers[i] - '0' <= max; i++) {

			if(lenMax > 1) {
				for(int j = i; j >= 1 && j >= i - bLen; j--) {
					cnt[i] = (cnt[i] + cnt[j - 1]) % mod;
				}
			}
			if(i - bLen <= 0) {
				cnt[i] = (cnt[i] + 1) % mod;
			}
			
			if (i - lenMax + 1 >= 0) {
				if (Long.valueOf(line.substring(i - lenMax + 1, i + 1)) <= max) {
					if(i - lenMax >= 0) {
						cnt[i] = (cnt[i] + cnt[i - lenMax]) % mod;
					}
					else {
						cnt[i] = (cnt[i] + 1) % mod;
					}
				}
			}
			cnt[i] %= mod;
		}
		out.println(cnt[n - 1]);
	}

	void solve2(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		long m = in.nextLong();
		long l = in.nextLong();

		char[] numbers = in.next().toCharArray();
		long[] cnt = new long[n];

		if (numbers[0] - '0' > m) {
			out.println(0);
			return;
		}
		cnt[0] = 1;

		for (int i = 1; i < n; i++) {

			if (numbers[i] - '0' > m) {
				out.println(0);
				return;
			}

			cnt[i] = cnt[i - 1];

			long merged = 0L + numbers[i] - '0';
			for (int j = i - 1; j >= 0; j--) {

				if (numbers[j] == '0') {
					continue;
				}

				merged = merged + (numbers[j] - '0') * 10;
				if (merged <= m) {
					if (j > 0) {
						cnt[i] += cnt[j - 1];
					} else {
						cnt[i]++;
					}
				}
			}
		}
		out.println(cnt[n - 1]);
	}

	public static void main(String[] args) {
		new Numbers2().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
