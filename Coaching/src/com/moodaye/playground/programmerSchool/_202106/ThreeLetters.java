package com.moodaye.playground.programmerSchool._202106;

import java.io.PrintWriter;
import java.math.BigInteger;
import java.util.Locale;
import java.util.Scanner;

// Problem 960
public class ThreeLetters {
void solve(Scanner in, PrintWriter out) {
	String str = in.next();
	if(str == null || str.isBlank() || str.isEmpty()) {
		out.println(0);
		return;
	}
	
	BigInteger na = BigInteger.ZERO;
	BigInteger nab = BigInteger.ZERO;
	BigInteger nabc = BigInteger.ZERO;
	
	for(int i = 0; i < str.length(); i++) {
		char next = str.charAt(i);
		switch(next) {
			case('a'): 
				na = na.add(BigInteger.ONE);
				break;
			case('b'):
				nab = nab.add(na);
				break;
			case('c'):
				nabc = nabc.add(nab);
				break;
		}
	}
	out.println(nabc.toString());
}

public static void main(String[] args) {
	new ThreeLetters().run();
}

void run() {
	Locale.setDefault(Locale.US);
	try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
		solve(in, out);
	}
}
}
