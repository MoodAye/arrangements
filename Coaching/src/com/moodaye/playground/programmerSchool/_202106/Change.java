package com.moodaye.playground.programmerSchool._202106;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

// Problem 944
public class Change {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int[] coins = new int[n];
		for (int i = 0; i < coins.length; i++) {
			coins[i] = in.nextInt();
		}
		int k = in.nextInt();
		int[] amounts = new int[k];
		int maxAmount = 0;
		for (int i = 0; i < amounts.length; i++) {
			amounts[i] = in.nextInt();
			maxAmount = Math.max(amounts[i], maxAmount);
		}

		int[][] memo = new int[2][maxAmount + 1];
		memo[0][0] = 1;
		int nextRow = 1;

		for (int nextCoin = 0; nextCoin < coins.length; nextCoin++) {
			int prevRow = (nextRow + 1) % 2;
			for (int i = 0; i <= maxAmount; i++) {
				if (memo[prevRow][i] == 1) {
					memo[nextRow][i] = 1;
				}

				if (i - coins[nextCoin] >= 0 && memo[nextRow][i - coins[nextCoin]] == 1) {
					memo[nextRow][i] = 1;
				}
			}
			nextRow = prevRow;
		}
		
		nextRow = (nextRow + 1) % 2;

		for (int i = 0; i < amounts.length; i++) {
			out.println(memo[nextRow][amounts[i]]);
		}
	}

	public static void main(String[] args) {
		new Change().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
