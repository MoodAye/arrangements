package com.moodaye.playground.programmerSchool._202106;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

// Problem 113
public class Farmer {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();

		int[][] grid = new int[n][n];

		for (int i = 0; i < n; i++) {
			char[] line = in.next().toCharArray();
			for (int j = 0; j < n; j++) {
				grid[i][j] = line[j] - '0';
			}
		}

		int[][] cntHor = new int[n][n];
		int[][] cntVer = new int[n][n];

		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				if (grid[i][j] == 1) {
					if (j == 0) {
						cntHor[i][j] = 1;
					} else {
						cntHor[i][j] = cntHor[i][j - 1] + 1;
					}
				}
				if (grid[j][i] == 1) {
					if (j == 0) {
						cntVer[j][i] = 1;
					} else {
						cntVer[j][i] = cntVer[j - 1][i] + 1;
					}
				}
			}
		}
		
		int max = 0;
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				if(cntHor[i][j] == 0) {
					continue;
				}
				if(cntHor[i][j] == cntVer[i][j - cntHor[i][j] + 1]) {
					max = Math.max(max, cntHor[i][j]);
				}
			}
		}
		
		out.println(max * max);
	}

	public static void main(String[] args) {
		new Farmer().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
