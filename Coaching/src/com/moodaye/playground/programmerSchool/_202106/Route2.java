package com.moodaye.playground.programmerSchool._202106;

import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Locale;
import java.util.Scanner;

// Problem 
public class Route2 {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int k = in.nextInt();
		int[][] nums = new int[n + 2][n + 2];
		for (int i = 1; i <= n; i++) {
			for (int j = 1; j <= n; j++) {
				nums[i][j] = in.nextInt();
			}
		}
		
		int loopCount = 0;

		int[][] sums = new int[n + 2][n + 2];
		int[][] temp = new int[n + 2][n + 2];

		sums[1][1] = nums[1][1];
		
		boolean[][] nextStep = new boolean[n + 2][n + 2];
		nextStep[1][1] = true;
		
		k--;
		while (k-- > 0) {
			nextStep = updateNextStep(nextStep);
			int[][] temp2 = temp;
			temp = sums;
			sums = temp2;
			clear(sums);
			for (int i = 1; i <= n; i++) {
				for (int j = 1; j <= n; j++) {
					loopCount++;
					// temp[i][j] == 0 can be used to denote a cell that is reachable in this step
					if(nextStep[i][j]) {
						sums[i][j] = nums[i][j] + Math.max(temp[i][j + 1],
							Math.max(temp[i][j - 1], Math.max(temp[i - 1][j], temp[i + 1][j])));
					}
					// else - sum[i][j] = 0 - which indicates this cell is reachable next iteration of i
				}
			}
		}
		out.println(max(sums));
		out.printf("loopCount = %d%n", loopCount);
	}
	
	private boolean[][] updateNextStep(boolean[][] nextStep) {
		boolean[][] temp = new boolean[nextStep.length][nextStep.length];
		for(int i = 1; i < nextStep.length - 1; i++) {
			for(int j = 1; j < nextStep.length - 1; j++) {
				if(nextStep[i][j]) {
					temp[i - 1][j] = true;
					temp[i + 1][j] = true;
					temp[i][j - 1] = true;
					temp[i][j + 1] = true;
				}
			}
		}
		return temp;
	}

	private int max(int sums[][]) {
		int maxV = 0;
		for (int i = 1; i < sums.length - 1; i++) {
			for (int j = 1; j < sums.length - 1; j++) {
				maxV = Math.max(maxV, sums[i][j]);
			}
		}
		return maxV;
	}

	private void clear(int[][] array) {
		for (int i = 0; i < array.length; i++) {
			Arrays.fill(array[0], 0);
		}
	}

	public static void main(String[] args) {
		new Route2().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}

}
