package com.moodaye.playground.programmerSchool._202106;

import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Locale;
import java.util.Scanner;

// Problem 38
public class Game2 {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int[] nums = new int[n];
		for (int i = 0; i < nums.length; i++) {
			nums[i] = in.nextInt();
		}
		
		int winner = findDiff(nums, 0, n - 1);
		if(winner > 0){
			out.println(1);
		}
		else if (winner < 0){
			out.println(2);
		}
		else {
			out.println(0);
		}
	}
	
	public int findDiff(int[] a, int start, int end) {
		int[][] memo = new int[a.length][a.length];	
		for(int i = 0; i < a.length; i++)
			Arrays.fill(memo[i], Integer.MIN_VALUE);
		return findDiff(a, start, end, memo);
	}
	
	public int findDiff(int[] a, int start, int end, int[][] memo) {
		if(start == end) {
			return a[start];
		}
		if(start + 1 == end) {
			return Math.abs(a[start] - a[end]);
		}
		if(memo[start][end] != Integer.MIN_VALUE) {
			System.out.println("FOUND");
			return memo[start][end];
		}
		
		// player can choose either start or end.
		// Then the next player plays.
		int left = a[start] - findDiff(a, start + 1, end, memo);
		int right = a[end] - findDiff(a, start, end - 1, memo);
		
		memo[start][end] = Math.max(left, right);
		
		return Math.max(left, right);
	}

	public static void main(String[] args) {
		new Game2().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
