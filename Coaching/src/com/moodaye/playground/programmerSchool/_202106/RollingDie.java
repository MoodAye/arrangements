package com.moodaye.playground.programmerSchool._202106;

import java.io.PrintWriter;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

// Problem 
public class RollingDie {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int q = in.nextInt();

		int max = n * 6; // 500 * 3000 * 8 = 12Mb < 16Mb

		double[] p = new double[max + 1];

		for (int turn = 0; turn < n; turn++) {
			double[] pTmp = new double[max + 1];
			for (int dice = 1; dice <= 6; dice++) {
				if (turn == 0) {
					pTmp[dice] = 1.0 / 6;
					continue;
				}
				for (int j = 1; j < p.length; j++) {
					if (p[j] == 0) {
						continue;
					}
					pTmp[dice + j] += p[j] / 6;
				}
			}
			p = pTmp;
		}
		
		out.printf("%.6f", q < p.length ? p[q] : 0);
	}

	public static void main(String[] args) {
		new RollingDie().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
