package com.moodaye.playground.programmerSchool._202106;

import java.io.PrintWriter;
import java.math.BigInteger;
import java.util.Locale;
import java.util.Scanner;

// Problem 423
// WA - #7
public class Message {
	void solve(Scanner in, PrintWriter out) {
		char[] digits = in.next().toCharArray();
		
		BigInteger cnt_2 = BigInteger.ONE;
		BigInteger cnt_1 = BigInteger.ONE;
		BigInteger prev_3 = BigInteger.ZERO;
		BigInteger prev_2Or1 = BigInteger.ZERO;
		int prev = -1;
		
//		long cnt_2 = 1L;
//		long cnt_1 = 1L;
//		long prev_3 = 0L;
//		long prev_2Or1 = 0L;
//		int prev = -1;
		
		BigInteger cnt = BigInteger.ONE;
		
		for(int i = 1; i < digits.length; i++) {
			cnt_2 = cnt_1;
			cnt_1 = cnt;
			prev = digits[i - 1] - '0';
			prev_2Or1 = prev == 1 || prev == 2 ? cnt_2 : BigInteger.ZERO;
			prev_3 = prev == 3 ? cnt_2 : BigInteger.ZERO;
			cnt = cnt_1.add(prev_2Or1).add(prev_3);
		}
			
		// added check for final combos ending in space...but did not help get back #7
		out.println(prev == 0 ? cnt_1.toString() : cnt.toString());
	}

	public static void main(String[] args) {
		new Message().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
