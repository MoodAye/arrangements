package com.moodaye.playground.programmerSchool._202106;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

// Problem 544
public class BallOnStairs {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		
		long cx = 0;
		long cx1 = 1;
		long cx2 = 2;
		long cx3 = 4;
		
		if(n <= 3) {
			if(n == 1) out.println(1);
			if(n == 2) out.println(2);
			if(n == 3) out.println(4);
			return;
		}
		
		for(int x = n - 3; x > 0; x--) {
			cx = cx1 + cx2 + cx3;
			cx1 = cx2;
			cx2 = cx3;
			cx3 = cx;
		}
		
		out.println(cx);
	}

	public static void main(String[] args) {
		new BallOnStairs().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
