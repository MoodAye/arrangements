package com.moodaye.playground.programmerSchool;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;
import java.util.regex.Pattern;

/** Programmer's School - Problem 6 */
public class Chess {
	void solve(Scanner in, PrintWriter out){
		String knightsMove = in.next();
		out.println(checkKnightsMove(knightsMove));
	}
	
	public static String checkKnightsMove(String move){
		Pattern pattern = Pattern.compile("[A-H][1-8]-[A-H][1-8]");
		if(!pattern.matcher(move).find()){
			return "ERROR";
		}
		
		int horizontalMove = move.charAt(0) - move.charAt(3);
		int verticalMove = move.charAt(1) - move.charAt(4);
		if(Math.abs(horizontalMove * verticalMove) == 2){
			return "YES";
		}
		return "NO";
	}
	
	public static void main(String args[]){
		new Chess().run();
	}
	
	void run(){
		Locale.setDefault(Locale.US);
		try( Scanner in  = new Scanner(System.in);
				PrintWriter out = new PrintWriter(System.out)){
			solve(in, out);
		}
	}
}
