package com.moodaye.playground.programmerSchool;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

/** Programmer's School - Problem 336 */
public class Elevator {

	void solve(Scanner in, PrintWriter out) {
		String buttons = in.next();
		out.println(floors(buttons));
	}

	// determine number of floors by evaluating the range
	// of the floors visited.
	public static int floors(String buttons) {
		
		int sum = 0;
		int max = 0;
		int min = 0;
		
		for (char c : buttons.toCharArray()) {
			if (c == '1') {
				sum++;
			} else {
				sum--;
			}

			max = Math.max(max, sum);
			min = Math.min(min, sum);
		}

		return max - min + 1;

	}

	public static void main(String[] args) {
		new Elevator().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}

}
