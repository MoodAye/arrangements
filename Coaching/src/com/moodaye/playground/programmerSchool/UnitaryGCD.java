package com.moodaye.playground.programmerSchool;

import java.io.PrintWriter;
import java.math.BigInteger;
import java.util.Locale;
import java.util.Scanner;

/** Programmer's School - Problem #85 */
public class UnitaryGCD {
	void solve(Scanner in, PrintWriter out){
		int n1 = in.nextInt();
		int n2 = in.nextInt();
		out.println(getStringOfOnes(gcd(n1,n2)));
	}

	public static int gcd(int a, int b){
		while(b != 0){
			int t = a % b;
			a = b;
			b = t;
		}
		return a;
	}

	public static String getStringOfOnes(int n){
		StringBuilder sn = new StringBuilder();
		String one = "1";
		for(int i = 0; i < n; i++){
			sn.append(one);
		}
		return sn.toString();
	}
	
	public static String bigIntGCD(String n1, String n2){
		String sGcd = null;
		BigInteger a = new BigInteger(n1);
		BigInteger b = new BigInteger(n2);
		BigInteger zero = new BigInteger("0");
		
		while (!b.equals(zero)){
			BigInteger t = a.mod(b);
			a = b;
			b = t;
		}
		return a.toString();	
	}
	
	
	public static void main(String[] args) {
		new UnitaryGCD().run();
	}
	
	void run(){
		Locale.setDefault(Locale.US);
		try(Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)){
			solve(in,out);
		}
	}
}
