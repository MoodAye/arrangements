package com.moodaye.playground.programmerSchool;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Locale;
import java.util.Scanner;

/** Problem 46 */
public class NumberE {

	public static void main(String args[]) {
		Locale.setDefault(Locale.US);
		Scanner in = new Scanner(System.in);
		int decPlaces = in.nextInt();
		String sn = "2.7182818284590452353602875";
		int lastDigit = 0;
		if (decPlaces == 0) {
			System.out.println(3);
		} else if (decPlaces == 25) {
			System.out.println(sn);
		} else {
			lastDigit = Integer.valueOf(sn.substring(1 + decPlaces, 1 + decPlaces + 1));
			int nextToLastDigit = Integer.valueOf(sn.substring(1 + decPlaces + 1, 1 + decPlaces + 1 + 1));
			if (nextToLastDigit >= 5) {
				lastDigit++;
			}
			System.out.printf("%s%d", sn.substring(0, 1 + decPlaces), lastDigit);
		}
	}
}
