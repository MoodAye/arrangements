package com.moodaye.playground.programmerSchool.reviewJanFeb2018;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #79 Start = 5:59am End = 6:22am*/
/* If A end in 0 --> A^B ends in 0
 * If A end in 1 --> A^B ends in 1
 * If A end in 2 --> A^B ends in 2,4,6,8
 * If A end in 3 --> A^B ends in 1,3,7,9
 * If A end in 4 --> A^B ends in 4,6
 * If A end in 5 --> A^B ends in 5
 * If A end in 6 --> A^B ends in 6
 * If A end in 7 --> A^B ends in 1,3,7,9
 * If A end in 8 --> A^B ends in 2,4,6,8
 * If A end in 9 --> A^B ends in 1,9
 */
public class LastDigitAToPowerB {
	void solve(Scanner in, PrintWriter out) {
		int a = in.nextInt();
		int b = in.nextInt();
		int lastDigit = a % 10;
		if(lastDigit == 0 || lastDigit == 1 || lastDigit == 5 || lastDigit == 6){
			out.println(lastDigit);
		}
		else if(lastDigit == 4){
			if(b % 2 == 0){
				out.println(6);
			}
			else{
				out.println(4);
			}
		}
		else if(lastDigit == 9){
			if(b % 2 == 0){
				out.println(1);
			}
			else{
				out.println(9);
			}
		}
		else if(lastDigit == 2){
			if(b % 4 == 0){
				out.println(6);
			}
			else if(b % 4 == 1){
				out.println(2);
			}
			else if(b % 4 == 2){
				out.println(4);
			}
			else if(b % 4 == 3){
				out.println(8);
			}
		}
		else if(lastDigit == 3){
			if(b % 4 == 0){
				out.println(1);
			}
			else if(b % 4 == 1){
				out.println(3);
			}
			else if(b % 4 == 2){
				out.println(9);
			}
			else if(b % 4 == 3){
				out.println(7);
			}
		}
		else if(lastDigit == 7){
			if(b % 4 == 0){
				out.println(1);
			}
			else if(b % 4 == 1){
				out.println(7);
			}
			else if(b % 4 == 2){
				out.println(9);
			}
			else if(b % 4 == 3){
				out.println(3);
			}
		}
		else if(lastDigit == 8){
			if(b % 4 == 0){
				out.println(6);
			}
			else if(b % 4 == 1){
				out.println(8);
			}
			else if(b % 4 == 2){
				out.println(4);
			}
			else if(b % 4 == 3){
				out.println(2);
			}
		}
	}

	public static void main(String args[]) {
		new LastDigitAToPowerB().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
