package com.moodaye.playground.programmerSchool.reviewJanFeb2018;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #688 Start = 8:56pm End 10:12pm */
public class GopherAndDog {
	void solve(Scanner in, PrintWriter out) {
		long xg = in.nextLong();
		long yg = in.nextLong();
		long xd = in.nextLong();
		long yd = in.nextLong();
		int n = in.nextInt();
		for (int i = 0; i < n; i++) {
			long x = in.nextLong();
			long y = in.nextLong();
			if (canHide(xg, yg, xd, yd, x, y)) {
				out.println(i + 1);
				return;
			}
		}
		out.println("NO");
	}

	/*
	 * Gopher needs to be less than 1/2 the distance of the dog from the burrow
	 */
	public static boolean canHide(long xg, long yg, long xd, long yd, long x, long y) {
		long gopherDistSq = (xg - x) * (xg - x) + (yg - y) * (yg - y);
		long dogDistSq = (xd - x) * (xd - x) + (yd - y) * (yd - y);
		return gopherDistSq * 4 <= dogDistSq;
	}

	public static void main(String args[]) {
		new GopherAndDog().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
