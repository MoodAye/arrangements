package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set43;

import java.util.Locale;
import java.util.Scanner;
import java.util.StringTokenizer;
import java.io.PrintWriter;

/** Programmer's School #272 */
//Start Time 7:36am  Pause = 7:57am
// Resume 10:45am  End = 11:07am
// Time Taken = 43min
// Took time trying to use the hasNext() feature of Scanner class vs. StringTokenizer.
public class SumMaxMin {
	void solve(Scanner in, PrintWriter out) {
		int max = Integer.MIN_VALUE;
		int min = Integer.MAX_VALUE;
	
		boolean isEven = false;
		StringTokenizer input = new StringTokenizer(in.nextLine());
		
		while(input.hasMoreTokens()){
			int n = Integer.valueOf(input.nextToken());
			if(isEven){
				max = Math.max(max, n);
			}
			else{
				min = Math.min(min, n);
			}
			isEven = !isEven;
		}
		
		out.println(max + min);
	}

	public static void main(String args[]) {
		new SumMaxMin().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
