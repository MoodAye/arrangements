package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set43;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #73 */
// Time Taken ~ 120minutes
public class Decoding {
	void solve(Scanner in, PrintWriter out) {
		String encrypted = in.next();
		for(int i = 0; i < encrypted.length(); i++){
			out.print(decrypt(encrypted.charAt(i), i + 1));
		}
	}
	
	public static char decrypt(char c, int idx){
		int code = 0;
		if('0' <= c && c <= '9'){
			code = c - '0';
		}
		else{
			code = 10 + c - 'A';
		}
		
		int cvalue = (code + (27 - (idx % 27))) % 27 - 1;
		if(cvalue == -1){
			return ' ';
		}
		else{
			return (char) ('a' + cvalue);
		}
	}

	public static void main(String args[]) {
		new Decoding().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
