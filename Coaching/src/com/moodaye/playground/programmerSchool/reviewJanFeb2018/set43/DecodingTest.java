package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set43;

import static org.junit.Assert.*;

import org.junit.AfterClass;
import org.junit.Test;
import static com.moodaye.playground.programmerSchool.reviewJanFeb2018.set43.Decoding.*;
public class DecodingTest {

	@Test
	public void test() {
		String word = "a";
		String encoded = encoder(word);
		for(int i = 0; i < encoded.length(); i++){
			assertEquals(word.charAt(i), decrypt(encoded.charAt(i), i));
		}
		
		word = " ";
		encoded = encoder(word);
		for(int i = 0; i < encoded.length(); i++){
			assertEquals(word.charAt(i), decrypt(encoded.charAt(i), i));
		}
		
		word = "t t t t";
		encoded = encoder(word);
		for(int i = 0; i < encoded.length(); i++){
			assertEquals(word.charAt(i), decrypt(encoded.charAt(i), i));
		}
		
		word = "tttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt";
		encoded = encoder(word);
		for(int i = 0; i < encoded.length(); i++){
			assertEquals(word.charAt(i), decrypt(encoded.charAt(i), i));
		}
		
		word = "this is a test";
		encoded = encoder(word);
		for(int i = 0; i < encoded.length(); i++){
			assertEquals(word.charAt(i), decrypt(encoded.charAt(i), i));
		}
		
		word = "zzz aaaa qqqq tttt";
		encoded = encoder(word);
		for(int i = 0; i < encoded.length(); i++){
			assertEquals(word.charAt(i), decrypt(encoded.charAt(i), i));
		}
	}
	
	static String encoder(String sentence){
		StringBuilder sb = new StringBuilder();
		for(int i = 0; i < sentence.length(); i++){
			char c = sentence.charAt(i);
			int cvalue = 0;
			if(sentence.charAt(i) == ' '){
				cvalue = (27 + i) % 27;
			}
			else{
				cvalue = (sentence.charAt(i) - 'a' + 1 + i ) % 27;
			}
			sb.append((char) (cvalue >= 10 ? 'A' + cvalue - 10 : '0' + cvalue));
		}
		return sb.toString();
	}

}
