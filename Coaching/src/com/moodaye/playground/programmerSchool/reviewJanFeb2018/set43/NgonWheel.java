package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set43;

import java.util.Scanner;
import java.io.PrintWriter;
import java.util.Locale;

/** Programmer's School #69 */
// Start Time = 12:40pm End Time = 1:18pm  Time Taken 38 min
// Most of time was taken as I was using Cos(angle) vs. Sin(angle)
public class NgonWheel {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int a = in.nextInt();
		double angle = Math.PI / n;
		double diff = 1.0 * a / 2 / Math.sin(angle) - 1.0 * a / 2 / Math.tan(angle);
		
		out.println(diff < 1 ? "YES" : "NO");
	}

	public static void main(String[] args) {
		new NgonWheel().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}

}
