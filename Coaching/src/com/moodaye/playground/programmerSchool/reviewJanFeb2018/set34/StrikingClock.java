package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set34;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #253 */
// Start Time = 9:54pm End Time = 10:14pm Time Taken = 20min
public class StrikingClock {
	void solve(Scanner in, PrintWriter out) {
		int h1 = in.nextInt();
		int m1 = in.nextInt();
		int h2 = in.nextInt();
		int m2 = in.nextInt();

		int cnt = 0;
		while (h1 != h2 || m1 != m2) {
			m1++;
			if (m1 == 30) {
				cnt++;
			} else if (m1 == 60) {
				m1 = 0;
				h1++;
				if (h1 == 24) {
					h1 = 0;
				} 
				cnt += h1 == 0 || h1 == 12 ? 12 : h1 % 12;
			}
		}
		out.println(cnt);
	}

	public static void main(String args[]) {
		new StrikingClock().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
