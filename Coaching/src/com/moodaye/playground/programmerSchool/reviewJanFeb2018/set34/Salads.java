package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set34;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #513 */
// Start Time = 10:16pm End Time = 10:22pm Time take = 6min
public class Salads {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		//count = 2^n - n - 1.  (-n for 1 ingredient; -1 for zero ingredients).
		out.println((1 << n) - n - 1);
	}

	public static void main(String args[]) {
		new Salads().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
