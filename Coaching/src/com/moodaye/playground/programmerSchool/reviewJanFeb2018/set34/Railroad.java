package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set34;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #409 */
// Start Time = 9:34 pm End Time = 9:51pm.  Time Taken = 15 min.
public class Railroad {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		// area - max = 10_000 x 30_000 = 300_000_000 or 3e8.  int is fine.
		int area = in.nextInt();
		for(int i = 2; i <= n - 1; i++){
			area += in.nextInt() * 2;
		}
		area += in.nextInt();
		out.printf("%.5f", (double) area / (n - 1) / 2); 
	}
	
	public static void main(String args[]) {
		new Railroad().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
