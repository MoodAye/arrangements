package com.moodaye.playground.programmerSchool.reviewJanFeb2018;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #336 Start = 9:28pm End = 9:39*/
public class Elevator {
	void solve(Scanner in, PrintWriter out) {
		char[] btn = in.next().toCharArray();
		int min = 0;
		int max = 0;
		int floor = 0;
		for(char b : btn){
			if(b == '1'){
				floor++;
			}
			else if(b == '2'){
				floor--;
			}
			else {
				throw new AssertionError("Unknown button: " + b);
			}
			min = Math.min(floor, min);
			max = Math.max(floor, max);
		}
		out.println(max - min + 1);
	}

	public static void main(String args[]) {
		new Elevator().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
