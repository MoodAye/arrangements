package com.moodaye.playground.programmerSchool.reviewJanFeb2018;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #539   Start = 9:23am; End = 9:33am */
/* Complexity = O(1)
 * Storage = O(1) 
 */
public class Cake {
void solve(Scanner in, PrintWriter out) {
	int n = in.nextInt();
	if(n == 1){
		out.println(0);
	}
	else if(n % 2 == 0){
		out.println(n / 2);
	}
	else{
		out.println(n);
	}
}

public static void main(String args[]) {
	new Cake().run();

}

void run() {
	Locale.setDefault(Locale.US);
	try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
		solve(in, out);
	}
}
}
