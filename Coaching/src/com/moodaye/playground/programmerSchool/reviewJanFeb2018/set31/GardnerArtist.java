package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set31;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #680 */
// Start Time = 10:13pm End = 10:17pm Time Take = 4min!
// Answer = 3 * 2^(n-1); long is 2^63 - 1
public class GardnerArtist {
	void solve(Scanner in, PrintWriter out) {
		out.println(3L << (in.nextInt() - 1));
	}

	public static void main(String args[]) {
		new GardnerArtist().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
