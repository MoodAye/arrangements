package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set31;

import java.util.Scanner;
import java.util.Arrays;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #349 */
// Start Time = 9:53pm End Time = 10:11pm Time Taken = 18min
public class PrimeNumbers {
	void solve(Scanner in, PrintWriter out) {
		int a = in.nextInt();
		int b = in.nextInt();
		boolean[] primes = seive(b);
		boolean noPrime = true;
		for(int i = a; i <= b; i++){ 
			if(primes[i]){
				noPrime = false;
				out.println(i);
			}
		}
		if(noPrime){
			out.println("Absent");
		}
	}
	
	public static boolean[] seive(int n){
		boolean[] primes = new boolean[n + 1];
		Arrays.fill(primes, true);
		primes[0] = false;
		primes[1] = false;
		
		for(int i = 2; i <= n; i++) {
			if(primes[i]){
				for(int j = 2 * i; j < primes.length; j += i){
					primes[j] = false;
				}
			}
		}
		return primes;
	}
	

	public static void main(String args[]) {
		new PrimeNumbers().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
