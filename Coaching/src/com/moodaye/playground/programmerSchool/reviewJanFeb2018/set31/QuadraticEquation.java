package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set31;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #411 */
// Start = 9:04pm End = 9:37pm Time Take = 33 minutes
public class QuadraticEquation {
	void solve(Scanner in, PrintWriter out) {
		double a = in.nextDouble();
		double b = in.nextDouble();
		double c = in.nextDouble();

		if (a == 0 && b == 0 && c == 0) {
			out.println(-1);
			return;
		}
		
		if (a == 0 && b == 0) {
			out.println(0);
			return;
		}
		
		if(a == 0){
			out.println(1);
			out.printf("%.6f", -c / b);
			return;
		}

		if (b * b - 4 * a * c == 0) {
			out.println(1);
			out.printf("%.6f", -b / (2 * a));
			return;
		}
		
		if (b * b - 4 * a * c < 0) {
			out.println(0);
			return;
		}

		double root1 = (-b + Math.sqrt(b * b - 4 * a * c)) / (2 * a);
		double root2 = (-b - Math.sqrt(b * b - 4 * a * c)) / (2 * a);
		out.println(2);
		out.printf("%.6f %.6f", root1, root2);
	}

	public static void main(String args[]) {
		new QuadraticEquation().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
