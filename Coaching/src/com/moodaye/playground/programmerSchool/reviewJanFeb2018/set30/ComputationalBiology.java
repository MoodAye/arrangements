package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set30;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #278 */
public class ComputationalBiology {
	void solve(Scanner in, PrintWriter out) {
		char[] s = in.next().toCharArray();
		char[] t = in.next().toCharArray();
		int sPos = 0;
		boolean found = false;
		for (int ti = 0; ti < t.length; ti++) {
			if(t[ti] == s[sPos]){
				sPos++;
				if(sPos == s.length){
					found = true;
					break;
				}
			}
		}
		out.println(found == true ? "YES" : "NO");
	}

	public static void main(String args[]) {
		new ComputationalBiology().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
