package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set30;
import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #684 */
// start = 10:07pm end = 10:33pm  total time = 26min
public class Checkers2 {
	void solve(Scanner in, PrintWriter out) {
		String start = in.next();
		String end = in.next();
		if(!isBlack(start) || !isBlack(end)){
			out.println("NO");
		}
		else if((end.charAt(1) - start.charAt(1)) <= 0){
			out.println("NO");
		}
		else if((end.charAt(0) - start.charAt(0)) * ((end.charAt(0) - start.charAt(0))) < 
				(end.charAt(1) - start.charAt(1)) * ((end.charAt(1) - start.charAt(1)))){
			out.println("NO");
		}
		else{
			out.println("YES");
		}
	}
	
	boolean isBlack(String sq){
		if(sq.charAt(0) > 'h' || sq.charAt(1) > '8' || 
				(sq.charAt(0) - 'a' + sq.charAt(1) - '1') % 2 != 0){
			return false;
		}
		return true;
	}

	public static void main(String args[]) {
		new Checkers2().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
