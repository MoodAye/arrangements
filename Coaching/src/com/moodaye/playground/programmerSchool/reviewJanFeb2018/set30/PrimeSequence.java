package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set30;

import java.util.Scanner;
import java.util.Arrays;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #64 */
// Start = 945pm End = 10:04pm  total time = 19min
public class PrimeSequence {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		String pdigits = primeDigits(10000);
		for(int i = 0; i < n; i++){
			out.print(pdigits.charAt(in.nextInt() - 1));
		}
	}

	/**
	 * @param len length of prime digits string to return
	 */
	public static String primeDigits(int len){
		boolean[] isPrime = new boolean[len * 10];
		Arrays.fill(isPrime, true);
		isPrime[0] = false;
		isPrime[1] = false;
		
		StringBuilder sb = new StringBuilder();
		for(int p = 2; p < isPrime.length; p++){
			if(isPrime[p]){
				sb.append(p);
				if(sb.length() >= len){
					break;
				}
				mask(isPrime, p);
			}
		}
		return sb.substring(0, len);
	}
	
	public static void mask(boolean[] isPrime, int p){
		for(int i = p * p; i < isPrime.length; i += p){
			isPrime[i] = false;
		}
	}

	public static void main(String args[]) {
		new PrimeSequence().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
