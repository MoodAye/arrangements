package com.moodaye.playground.programmerSchool.reviewJanFeb2018;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #94 Start = 8:43pm End = 8:53pm */
public class PrinceAndDragon {
	void solve(Scanner in, PrintWriter out) {
		int b = in.nextInt();
		int h = in.nextInt();
		int r = in.nextInt();
		
		if(r >= b && h > b){
			out.println("NO");
			return;
		}
		
		h -= b;
		int cnt = 1;
		while(h > 0){
			h += r - b;
			cnt++;
		}
		out.println(cnt);
	}

	public static void main(String args[]) {
		new PrinceAndDragon().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
