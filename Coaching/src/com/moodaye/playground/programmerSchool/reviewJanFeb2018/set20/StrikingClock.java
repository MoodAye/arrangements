package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set20;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #253 */
public class StrikingClock {
	void solve(Scanner in, PrintWriter out) {
		int stHr = in.nextInt();
		int stMin = in.nextInt();
		int endHr = in.nextInt();
		int endMin = in.nextInt();
		
		int time = stHr * 60 + stMin;
		int endTime = endHr * 60 + endMin;
		
		int cnt = 0;
		while(time != endTime){
			time++;
			if(time % 30 == 0){
				if(time % 60 == 0){
					int hr = (time / 60) % 12;
					cnt += hr == 0 ? 12 : hr;
					time = time == 24 * 60 ? 0 : time;
				}
				else{
					cnt++;
				}
			}
		}
		out.println(cnt);
	}

	public static void main(String args[]) {
		new StrikingClock().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
