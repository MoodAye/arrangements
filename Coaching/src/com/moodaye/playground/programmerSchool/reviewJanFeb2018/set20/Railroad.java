package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set20;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #409 */
public class Railroad {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		double sum = in.nextDouble() / 2;
		for(int i = 1; i < n - 1; i++){
			sum += in.nextDouble();
		}
		sum += in.nextDouble() / 2;
		out.printf("%.10f", (sum / (n - 1)));
		
	}

	public static void main(String args[]) {
		new Railroad().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
