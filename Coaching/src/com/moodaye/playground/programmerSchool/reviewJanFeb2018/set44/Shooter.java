package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set44;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #298 */
// Start Time = 9:39pm End Time = 9:54pm Time Taken = 15min
public class Shooter {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int[][] coords = new int[n][2];
		for (int i = 0; i < n; i++) {
			coords[i][0] = in.nextInt();
			coords[i][1] = in.nextInt();
		}
		int min = n;
		for (int i = 1; i < n; i++) {
			for (int j = i - 1; j >= 0; j--) {
				if ((coords[i][0] * coords[j][1] == coords[i][1] * coords[j][0])
						&& (coords[i][0] * coords[j][0] >= 0 && coords[i][1] * coords[j][1] >= 0)) {
					min--;
					break;
				}
			}
		}
		out.println(min);
	}

	public static void main(String args[]) {
		new Shooter().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
