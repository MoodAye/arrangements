package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set44;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #517 */
// Start Time = 9:04pm  End Time = 9:36pm Time Taken = 32min
public class Bowling {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int[] pins = new int[n];
		for (int i = 0; i < n; i++) {
			pins[i] = in.nextInt();
		}
		int score = 0;
		int frame = 1;
		int ball = 1;
		for (int i = 0; i < n; i++) {
			score += pins[i];
			if (frame != 10) {
				if (ball == 1 && pins[i] == 10) {
					score = score + pins[i + 1] + pins[i + 2];
					frame++;
				} else if (ball == 2 && pins[i] + pins[i - 1] == 10) {
					score = score + pins[i + 1];
					ball = 1;
					frame++;
				}
				else{
					if(ball == 2){
						frame++;
						ball = 1;
					}
					else{
						ball = 2;
					}
				}
			}
			else{
				score += pins[i + 1];
				if(pins[i] == 10 || pins[i] + pins[i + 1] == 10){
					score += pins[i + 2];
				}
				break;
			}
		}
		out.println(score);
	}

	public static void main(String args[]) {
		new Bowling().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
