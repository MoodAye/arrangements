package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set44;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #303 */
// Start Time = 8:53pm End Time = 9:02pm  Time Taken = 9 min
public class Digits {
	void solve(Scanner in, PrintWriter out) {
		char[] n = in.next().toCharArray();
		int max = Integer.MIN_VALUE;
		for(int i = 0; i < n.length; i++){
			max = Math.max(computeSum(n, i), max);
		}
		out.println(max);
	}
	
	static int computeSum(char[] n, int skip){
		int sum = 0;
		boolean add = true;
		for(int i = 0; i < n.length; i++){
			if(i == skip){
				continue;
			}
			if(add){
				sum += n[i] - '0';
			}
			else{
				sum -= n[i] - '0';
			}
			add = !add;
		}
		return sum;
	}

	public static void main(String args[]) {
		new Digits().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
