package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set42;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #520 */
// Stopped tracking time - took > 2 hours to do this.
// Using the cost numbers - assumes cost of box per unit (sock) is always less than 
// cost of bundle per unit (sock) which in turn is always less than the cost of a sock.
public class WholesalePurchase {
	void solve(Scanner in, PrintWriter out) {
		int costSock = 1050;
		int costBundle = 10250;
		int costBox = 114000;

		int n = in.nextInt();

		int nBoxes = (n + 143) / 144;
		long priceOnlyBoxes = 1L * nBoxes * costBox;

		int nBundles = ((n - (nBoxes - 1) * 144) + 11) / 12;
		long priceOnlyBoxesAndBundles = 1L * (nBoxes - 1) * costBox + 1L * nBundles * costBundle;

		int nSocks = n - (nBoxes - 1) * 144 - (nBundles - 1) * 12;
		long priceBoxesBudlesAndSocks = 1L * (nBoxes - 1) * costBox + 1L * (nBundles - 1) * costBundle
				+ 1L * (nSocks) * costSock;

		if (priceOnlyBoxes < priceOnlyBoxesAndBundles) {
			if (priceOnlyBoxes < priceBoxesBudlesAndSocks) {
				out.printf("%d %d %d", nBoxes, 0, 0);
			} else {
				out.printf("%d %d %d", nBoxes - 1, nBundles - 1, nSocks);
			}
		} else if (priceOnlyBoxesAndBundles < priceBoxesBudlesAndSocks) {
			out.printf("%d %d %d", nBoxes - 1, nBundles, 0);
		} else {
			out.printf("%d %d %d", nBoxes - 1, nBundles - 1, nSocks);
		}
	}

	public static void main(String args[]) {
		new WholesalePurchase().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
