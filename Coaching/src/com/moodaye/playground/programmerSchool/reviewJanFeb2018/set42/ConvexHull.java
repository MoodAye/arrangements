package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set42;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #84 */
// Start Time = 10:15pm 10:29pm Time Taken = 14 min
public class ConvexHull {
	void solve(Scanner in, PrintWriter out) {
		int minX = Integer.MAX_VALUE;
		int maxX = Integer.MIN_VALUE;
		int minY = Integer.MAX_VALUE;
		int maxY = Integer.MIN_VALUE;
		
		int lines = in.nextInt();
		int cols = in.nextInt();
		
		for(int i = 0; i < lines; i++){
			String line = in.next();
			for(int j = 0; j < cols; j++){
				if(line.charAt(j) == '*'){
					minX = Math.min(minX, i);
					maxX = Math.max(maxX, i);
					minY = Math.min(minY, j);
					maxY = Math.max(maxY, j);
				}
			}
		}
		
		for(int i = 0; i < lines; i++){
			for(int j = 0; j < cols; j++){
				if(minX <= i && i <= maxX && 
						minY <= j && j <= maxY){
					out.print("*");
				}
				else{
					out.print(".");
				}
			}
			out.println();
		}	
	}

	public static void main(String args[]) {
		new ConvexHull().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
