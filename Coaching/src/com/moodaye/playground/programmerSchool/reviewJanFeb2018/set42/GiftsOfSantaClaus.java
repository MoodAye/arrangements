package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set42;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #317 */
// Start Time = 11:27pm  End Time = 11:35pm Time Taken = 8 min
public class GiftsOfSantaClaus {
	void solve(Scanner in, PrintWriter out) {
		int x = in.nextInt();
		int y = in.nextInt();
		int z = in.nextInt();
		int w = in.nextInt();
	
		int cnt = 0;
		for(int cntX = 0; cntX * x <= w; cntX++){
			for(int cntY = 0; cntX * x + cntY * y <= w; cntY++){
				if((w - (cntX * x + cntY * y)) % z == 0){
					cnt++;
				}
			}
		}
		out.println(cnt);
	}

	public static void main(String args[]) {
		new GiftsOfSantaClaus().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
