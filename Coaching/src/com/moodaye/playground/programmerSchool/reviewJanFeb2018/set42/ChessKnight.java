package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set42;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #416 */
// Start Time = 9:56pm  End Time = 10:10pm Time Taken = 14min
public class ChessKnight {
	void solve(Scanner in, PrintWriter out) {
		String sq = in.next();
		
		for(int x = 'a'; x <= 'h'; x++){
			for(int y = '1'; y <= '8'; y++){
				int dx = x - sq.charAt(0);
				int dy = y - sq.charAt(1);
				if(Math.abs(dx * dy) == 2){
					out.printf("%c%c%n", x,y);
				}
			}
		}
	}
	
	public static void main(String args[]) {
		new ChessKnight().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
