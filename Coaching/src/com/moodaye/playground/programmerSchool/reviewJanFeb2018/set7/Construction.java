package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set7;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #501 */
// took about ~2 hours
// appropriate data type for opposite corners
public class Construction {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int[][] plots = new int[n][4];
		for (int[] plot : plots) {
			for (int i = 0; i < 4; i++) {
				plot[i] = in.nextInt();
			}
		}
		int[] facility = new int[4];
		for (int i = 0; i < 4; i++) {
			facility[i] = in.nextInt();
		}

		int intersectingAreas = 0;
		for (int[] plot : plots) {
			int xOverlap = getOverlap(plot[0], plot[2], facility[0], facility[2]);
			int yOverlap = getOverlap(plot[1], plot[3], facility[1], facility[3]);
			intersectingAreas += xOverlap * yOverlap;
		}

		out.println(intersectingAreas);
	}
	
	

	public static int getOverlap(int c1, int c2, int d1, int d2) {
		if (c1 > c2) {
			int temp = c1;
			c1 = c2;
			c2 = temp;
		}

		if (d1 > d2) {
			int temp = d1;
			d1 = d2;
			d2 = temp;
		}
		
		return Math.max(0, Math.min(c2,d2) - Math.max(c1, d1));
	}

	public static void main(String args[]) {
		new Construction().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
