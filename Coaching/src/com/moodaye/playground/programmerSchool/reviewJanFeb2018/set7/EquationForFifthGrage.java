package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set7;
import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School # */
// Wrote in ~10
public class EquationForFifthGrage {
	void solve(Scanner in, PrintWriter out) {
		char[] eqn = in.next().toCharArray();
		if(eqn[4] == 'x'){
			if(eqn[1] == '+'){
				out.println((eqn[0] - '0') + (eqn[2] - '0'));
			}
			else{
				out.println((eqn[0] - '0') - (eqn[2] - '0'));
			}
		}
		else if(eqn[0] == 'x'){
			if(eqn[1] == '+'){
				out.println((eqn[4] - '0') - (eqn[2] - '0'));
			}
			else{
				out.println((eqn[4] - '0') + (eqn[2] - '0'));
			}
		}
		else{ // eqn[2] == 'x'
			if(eqn[1] == '+'){
				out.println((eqn[4] - '0') - (eqn[0] - '0'));
			}
			else{
				out.println((eqn[0] - '0') - (eqn[4] - '0'));
			}
		}
	}

	public static void main(String args[]) {
		new EquationForFifthGrage().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
