package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set7;

import java.util.Scanner;
import java.util.Arrays;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #670 */
/*
 * Brute force way - check each number's digits one by one if for duplicates. We
 * can sort the digits first and then make one sweep through the digits O(nlogn
 * + n). Therefore - with largest N = 10e4 - we have complexity of nlogn + n
 * .... from 1 to 10e4 .. for worst case complexity of O(n^2)
 */
public class NumbersWithoutEqualDigits {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int cnt = 0;
		int i = 1;
		while(cnt != n){
			if(containsDups(i++)){
				continue;
			}
			cnt++;
		}
		out.println(i - 1);
	}
	
	public static boolean containsDups(int n){
		char[] cn = String.valueOf(n).toCharArray();
		Arrays.sort(cn);
		for(int i = 0; i < cn.length - 1; i++){
			if(cn[i] == cn[i+1]){
				return true;
			}
		}
		return false;
	}

	public static void main(String args[]) {
		new NumbersWithoutEqualDigits().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
