package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set7;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #641 */
// took about 1 hour
public class StrangeLottery {
	void solve(Scanner in, PrintWriter out) {
		String number = in.next();
		number = maxWin(number);
		number = maxWin(number);
		out.println(number);
	}
	
	String maxWin(String lottery){
		char[] c = lottery.toCharArray();
		int len = c.length;
		int j = 0;
		while(j < len - 1 && c[j] >= c[j + 1]){
			j++;
		}
		
		int skip = j < len - 1 ? j + 1 : len - 1;
		
		char[] m = new char[len - 1];
		for(int i = 0, k = 0; i < len; i++){
			if(i != skip){
				m[k++] = c[i];
			}
		}
		return String.valueOf(m);
	}

	public static void main(String args[]) {
		new StrangeLottery().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
