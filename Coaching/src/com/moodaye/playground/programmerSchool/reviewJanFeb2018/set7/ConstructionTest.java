package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set7;

import static org.junit.Assert.*;

import org.junit.Test;
import static com.moodaye.playground.programmerSchool.reviewJanFeb2018.set7.Construction.*;
public class ConstructionTest {

	@Test
	public void test() {
		int fx1 = 10, fy1 = 10, fx2 = 20, fy2 = 20;
		
		// plot = facility
		int px1 = 10, py1 = 10, px2 = 20, py2 = 20;
		assertEquals( 10 * 10, getOverlap(fx1, fx2, px1, px2) * getOverlap(fy1, fy2, py1, py2));
		
		// no intersection any coordinate
		px1 = 100; py1 = 100; px2 = 200; py2 = 200;
		assertEquals( 0 , getOverlap(fx1, fx2, px1, px2) * getOverlap(fy1, fy2, py1, py2));
		
		// no intersection  - but x overlap
		px1 = 10; py1 = 100; px2 = 20; py2 = 200;
		assertEquals( 0 , getOverlap(fx1, fx2, px1, px2) * getOverlap(fy1, fy2, py1, py2));
		
		// no intersection  - but x overlap and y touch
		px1 = 10; py1 = 20; px2 = 20; py2 = 200;
		assertEquals( 0 , getOverlap(fx1, fx2, px1, px2) * getOverlap(fy1, fy2, py1, py2));
		
		// no intersection  - but y overlap
		px1 = 100; py1 = 15; px2 = 200; py2 = 25;
		assertEquals( 0 , getOverlap(fx1, fx2, px1, px2) * getOverlap(fy1, fy2, py1, py2));
	
		// plot below and left of facility
		px1 = 5; py1 = 5; px2 = 15; py2 = 15;
		assertEquals( 5 * 5 , getOverlap(fx1, fx2, px1, px2) * getOverlap(fy1, fy2, py1, py2));
		
		// plot above and left of facility
		px1 = 15; py1 = 15; px2 = 25; py2 = 25;
		assertEquals( 5 * 5 , getOverlap(fx1, fx2, px1, px2) * getOverlap(fy1, fy2, py1, py2));
		
		// plot below and right of facility
		px1 = 15; py1 = 5; px2 = 25; py2 = 15;
		assertEquals( 5 * 5 , getOverlap(fx1, fx2, px1, px2) * getOverlap(fy1, fy2, py1, py2));
		
		// plot above and right of facility
		px1 = 15; py1 = 15; px2 = 25; py2 = 25;
		assertEquals( 5 * 5 , getOverlap(fx1, fx2, px1, px2) * getOverlap(fy1, fy2, py1, py2));
		
		// plot within facility
		px1 = 15; py1 = 15; px2 = 16; py2 = 16;
		assertEquals( 1 * 1 , getOverlap(fx1, fx2, px1, px2) * getOverlap(fy1, fy2, py1, py2));
		
		//switch corners-----------------------------------------------------------
		fx1 = 10; fy1 = 20; fx2 = 20; fy2 = 10;
		
		// plot = facility
		px1 = 10; py1 = 10; px2 = 20; py2 = 20;
		assertEquals( 10 * 10, getOverlap(fx1, fx2, px1, px2) * getOverlap(fy1, fy2, py1, py2));
		
		// no intersection any coordinate
		px1 = 100; py1 = 200; px2 = 200; py2 = 100;
		assertEquals( 0, getOverlap(fx1, fx2, px1, px2) * getOverlap(fy1, fy2, py1, py2));
		
		// no intersection  - but x overlap
		px1 = 10; py1 = 300; px2 = 20; py2 = 200;
		assertEquals( 0 , getOverlap(fx1, fx2, px1, px2) * getOverlap(fy1, fy2, py1, py2));
		
		// no intersection  - but x overlap and y touch
		px1 = 10; py1 = 200; px2 = 20; py2 = 20;
		px1 = 10; py1 = 20; px2 = 20; py2 = 200;
		assertEquals( 0 , getOverlap(fx1, fx2, px1, px2) * getOverlap(fy1, fy2, py1, py2));
		
		// no intersection  - but y overlap
		px1 = 100; py1 = 25; px2 = 200; py2 = 15;
		assertEquals( 0 , getOverlap(fx1, fx2, px1, px2) * getOverlap(fy1, fy2, py1, py2));
	
		// plot below and left of facility
		px1 = 5; py1 = 15; px2 = 15; py2 = 5;
		assertEquals( 5 * 5 , getOverlap(fx1, fx2, px1, px2) * getOverlap(fy1, fy2, py1, py2));
		
		// plot above and left of facility
		px1 = 15; py1 = 25; px2 = 25; py2 = 15;
		assertEquals( 5 * 5 , getOverlap(fx1, fx2, px1, px2) * getOverlap(fy1, fy2, py1, py2));
		
		// plot below and right of facility
		px1 = 15; py1 = 15; px2 = 25; py2 = 5;
		assertEquals( 5 * 5 , getOverlap(fx1, fx2, px1, px2) * getOverlap(fy1, fy2, py1, py2));
		
		// plot above and right of facility
		px1 = 15; py1 = 25; px2 = 25; py2 = 15;
		assertEquals( 5 * 5 , getOverlap(fx1, fx2, px1, px2) * getOverlap(fy1, fy2, py1, py2));
		
	}

}
