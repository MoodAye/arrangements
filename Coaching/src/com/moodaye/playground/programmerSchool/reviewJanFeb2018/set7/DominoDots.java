package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set7;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #328 */
// wrote program in couple of minutes - but failing on test 17
// realized that sum needs to be long type.
// my estiamte for number of dots is ~10e12 
// answer max = 500_150_010_000 .. does not match my estimate above ... will try to figure this out
public class DominoDots {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		long sum = 0;
		for(int i = 0; i <= n; i++){
			// could not think of a way to replace the first loop.  
			// but the formula below replaces the second loop
			sum += (i + i + n + i) * (n - i + 1) / 2;
		}
		out.println(sum);
	}

	public static void main(String args[]) {
		new DominoDots().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
