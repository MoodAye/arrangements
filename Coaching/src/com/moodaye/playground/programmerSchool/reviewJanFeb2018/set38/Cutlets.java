package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set38;

import java.io.PrintWriter;
import java.util.Scanner;

/* Programmer's School - Problem #664 */
// Start = 7:47pm End = 8:09pm Time Taken = 22min
public class Cutlets {
	void solve(Scanner in, PrintWriter out) {
		int p = in.nextInt();
		int s = in.nextInt();
		int n = in.nextInt();
		// first fry first side of n cutlets.

		int batches = (n + p - 1) / p;
		int rem = batches * p - n;
		int n2 = n - rem;
		if (n > p) {
			batches += (n2 + p - 1) / p;
		} else {
			batches *= 2;
		}
		out.println(batches * s);
	}

	public static void main(String[] args) {
		new Cutlets().run();
	}

	void run() {
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
