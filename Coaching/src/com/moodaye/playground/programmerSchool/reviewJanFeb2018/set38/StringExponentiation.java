package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set38;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #70 */
// Start = 10:13pm End = 10:37pm Time Taken = 24min
// first submission failed test 11. Second submission passed all tests
public class StringExponentiation {
	void solve(Scanner in, PrintWriter out) {
		String s = in.next();
		int n = in.nextInt();

		if (n > 0) {
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < n && sb.length() <= 1023; i++) {
				sb.append(s);
			}
			if (sb.length() > 1023) {
				out.println(sb.substring(0, 1023));
			} else {
				out.println(sb);
			}
			return;
		}

		n *= -1;

		if (s.length() % n != 0) {
			out.println("NO SOLUTION");
			return;
		}

		int factor = s.length() / n;

		String root = s.substring(0, factor);
		for (int i = factor; i < s.length(); i += factor) {
			if (!s.substring(i, i + factor).equals(root)) {
				out.println("NO SOLUTION");
				return;
			}
		}
		out.println(root);
	}

	public static void main(String args[]) {
		new StringExponentiation().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
