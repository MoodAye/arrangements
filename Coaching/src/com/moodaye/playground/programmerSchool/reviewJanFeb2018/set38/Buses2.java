package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set38;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #667 */
public class Buses2 {
	void solve(Scanner in, PrintWriter out) {
		int children = in.nextInt();
		int adults = in.nextInt();
		int busCap = in.nextInt();
		
		if(busCap <= 2){
			out.println(0);
			return;
		}
		
		int cLeft = children;
		
		int cnt = 0;
		while(cLeft > 0){
			cLeft -= (busCap - 2);
			cnt++;
			adults -= 2;
		}
		
		if(adults < 0){
			out.println(0);
			return;
		}
		
		adults -= (cnt * (busCap - 2)) - children;
		
		if(adults <= 0){
			out.println(cnt);
			return;
		}
		cnt += (adults + busCap - 1) / busCap ;
		out.println(cnt);
	}

	public static void main(String args[]) {
		new Buses2().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
