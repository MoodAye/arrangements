package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set28;

import java.io.PrintWriter;
import java.util.Scanner;

/* Programmer's School Problem 315 */
// Start 8:09pm End 8:37pm
// Failed on test 6 - figured out it must be a space in the input.
public class MinimalNumeralSystem {
	
	public void solve(Scanner in, PrintWriter out) {
		char[] cn = in.nextLine().toCharArray();
		int max = 0;
		for(char c : cn) {
			if(!(('0' <= c && c <= '9') || 
					('A' <= c && c <= 'Z'))){
				out.println(-1);
				return;
			}
			max = Math.max(c, max);
		}
		max = max == '0' ? '1' : max;
		if(max <= '9') {
			out.println(max - '0' + 1);
		}
		else {
			out.println(10 + max - 'A' + 1);
		}
	}
	
	
	public static void main(String[] args) {
		new MinimalNumeralSystem().run();
	}
	
	public void run() {
		try(Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)){
			solve(in, out);
		}
	}
}
