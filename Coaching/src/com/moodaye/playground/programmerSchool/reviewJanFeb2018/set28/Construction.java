package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set28;

import java.io.PrintWriter;
import java.util.Scanner;

/* Programmer's School Problem 501 */
// Wrong answer on test 9;  Added test cases - but all my test cases are passing.
// Total time = 75min

// tried normalizing the corners --- did not help. Still failing test 9.
// passing test 9.  But now failing on test 10
public class Construction {
	
	public void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		Rectangle[] rects = new Rectangle[n];
		for(int i = 0; i < n; i++){
			rects[i] = new Rectangle();
			rects[i].x1 = in.nextInt();
			rects[i].y1 = in.nextInt();
			rects[i].x2 = in.nextInt();
			rects[i].y2 = in.nextInt();
		}
		Rectangle site = new Rectangle();
			site.x1 = in.nextInt();
			site.y1 = in.nextInt();
			site.x2 = in.nextInt();
			site.y2 = in.nextInt();
			
		int intersection = 0;
		for(int i = 0; i < n; i++){
			intersection += intersection(site, rects[i]);
		}
		
		out.println(intersection);
	}

	/* ensure corner x1,y1 lies to the left of and below corner x2,y2 */
	public static void normalizeCorners(Rectangle r){
		if(r.x1 > r.x2 && r.y1 > r.y2){
			int temp = r.y2;
			r.y2 = r.y1;
			r.y1 = temp;
			temp = r.x2;
			r.x2 = r.x1;
			r.x1 = temp;
		}
		else if(r.x1 > r.x2){
			int temp = r.x2;
			r.x2 = r.x1;
			r.x1 = temp;
		}
		else if(r.y1 > r.y2){
			int temp = r.y2;
			r.y2 = r.y1;
			r.y1 = temp;
		}
	}
	
	public static int intersection(Rectangle r1, Rectangle r2){
		normalizeCorners(r1);
		normalizeCorners(r2);
		
		if(r1.x1 < r2.x1 && r1.x2 <= r2.x1){
			return 0;
		}
		
		if(r1.x1 >= r2.x2 && r1.x2 > r2.x2){
			return 0;
		}
		
		if(r1.y1 < r2.y1 && r1.y2 <= r2.y1){
			return 0;
		}
		
		if(r1.y1 >= r2.y2 && r1.y2 > r2.y2){
			return 0;
		}
		
		//x overlap
		int dx = Math.min(r2.x2 - r2.x1, Math.min(r1.x2 - r1.x1, Math.min((r1.x2 - r2.x1), (r2.x2 - r1.x1))));
		int dy = Math.min(r2.y2 - r2.y1, Math.min(r1.y2 - r1.y1, Math.min((r1.y2 - r2.y1), (r2.y2 - r1.y1))));
		
		return dy * dx;
	}
	
	
	public static void main(String[] args) {
		new Construction().run();
	}
	
	public void run() {
		try(Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)){
			solve(in, out);
		}
	}
}

class Rectangle{
	int x1;
	int y1;
	int x2;
	int y2;
}
