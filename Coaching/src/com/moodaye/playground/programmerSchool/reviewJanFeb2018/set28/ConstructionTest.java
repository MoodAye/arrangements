package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set28;

import static org.junit.Assert.*;
import static com.moodaye.playground.programmerSchool.reviewJanFeb2018.set28.Construction.*;
import org.junit.AfterClass;
import org.junit.Test;

public class ConstructionTest {

	@Test
	public void testIntersection() {
		Rectangle r1 = new Rectangle(); 
		r1.x1 = 10;
		r1.y1 = 10;
		r1.x2 = 100;
		r1.y2 = 100;
	
		//equal
		Rectangle r2 = new Rectangle(); 
		r2.x1 = 10;
		r2.y1 = 10;
		r2.x2 = 100;
		r2.y2 = 100;
		assertEquals(90 * 90, intersection(r1, r2));
		
		//left of
		r2.x1 = 5;
		r2.y1 = 10;
		r2.x2 = 9;
		r2.y2 = 100;
		assertEquals(0, intersection(r1, r2));
		
		//right of
		r2.x1 = 100;
		r2.y1 = 10;
		r2.x2 = 110;
		r2.y2 = 100;
		assertEquals(0, intersection(r1, r2));
		
		//over
		r2.x1 = 11;
		r2.y1 = 110;
		r2.x2 = 99;
		r2.y2 = 200;
		assertEquals(0, intersection(r1, r2));
		
		//under
		r2.x1 = 11;
		r2.y1 = 5;
		r2.x2 = 99;
		r2.y2 = 9;
		assertEquals(0, intersection(r1, r2));
	
		//inside
		r2.x1 = 20;
		r2.y1 = 20;
		r2.x2 = 90;
		r2.y2 = 90;
		assertEquals(70 * 70, intersection(r1, r2));

		//around
		r2.x1 = 5;
		r2.y1 = 5;
		r2.x2 = 200;
		r2.y2 = 200;
		assertEquals(90 * 90, intersection(r1, r2));
		
		//left, lower overlap
		r2.x1 = 5;
		r2.y1 = 5;
		r2.x2 = 15;
		r2.y2 = 15;
		assertEquals((15 - 10) * (15 - 10), intersection(r1, r2));
		
		//right, lower overlap
		r2.x1 = 15;
		r2.y1 = 5;
		r2.x2 = 115;
		r2.y2 = 15;
		assertEquals((100 - 15) * (15 - 10), intersection(r1, r2));
		
		//left, upper overlap
		r2.x1 = 5;
		r2.y1 = 90;
		r2.x2 = 15;
		r2.y2 = 110;
		assertEquals((15 - 10) * (100 - 90), intersection(r1, r2));
		
		//right, upper overlap
		r2.x1 = 95;
		r2.y1 = 95;
		r2.x2 = 110;
		r2.y2 = 110;
		assertEquals((100 - 95) * (100 - 95), intersection(r1, r2));
		
		//upper overlap
		r2.x1 = 20;
		r2.y1 = 80;
		r2.x2 = 80;
		r2.y2 = 110;
		assertEquals((80 - 20) * (100 - 80), intersection(r1, r2));
		
		//upper overlap
		r2.x1 = 5;
		r2.y1 = 80;
		r2.x2 = 110;
		r2.y2 = 110;
		assertEquals((100 - 10) * (100 - 80), intersection(r1, r2));
		
		//lower overlap
		r2.x1 = 20;
		r2.y1 = 5;
		r2.x2 = 80;
		r2.y2 = 20;
		assertEquals((80 - 20) * (20 - 10), intersection(r1, r2));
		
		//lower overlap
		r2.x1 = 5;
		r2.y1 = 5;
		r2.x2 = 110;
		r2.y2 = 20;
		assertEquals((100 - 10) * (20 - 10), intersection(r1, r2));
		
		//left overlap
		r2.x1 = 5;
		r2.y1 = 15;
		r2.x2 = 20;
		r2.y2 = 80;
		assertEquals((20 - 10) * (80 - 15), intersection(r1, r2));
		
		//left overlap
		r2.x1 = 5;
		r2.y1 = 5;
		r2.x2 = 20;
		r2.y2 = 180;
		assertEquals((20 - 10) * (100 - 10), intersection(r1, r2));
		
		//right overlap
		r2.x1 = 20;
		r2.y1 = 20;
		r2.x2 = 110;
		r2.y2 = 80;
		assertEquals((100 - 20) * (80 - 20), intersection(r1, r2));
		
		//right overlap
		r2.x1 = 20;
		r2.y1 = 5;
		r2.x2 = 110;
		r2.y2 = 110;
		assertEquals((100 - 20) * (100 - 10), intersection(r1, r2));
	
		// testing normalization of square corners
		// top right and bottom left
		r1.x1 = 1;
		r1.y1 = 1;
		r1.x2 = 2;
		r1.y2 = 2;
		r2.x1 = 2;
		r2.y1 = 2;
		r2.x2 = 1;
		r2.y2 = 1;
		assertEquals((2 -1) * (2 - 1), intersection(r1, r2));
	
		// top left and bottom right
		r1.x1 = 1;
		r1.y1 = 2;
		r1.x2 = 2;
		r1.y2 = 1;
		r2.x1 = 2;
		r2.y1 = 2;
		r2.x2 = 1;
		r2.y2 = 1;
		assertEquals((2 -1) * (2 - 1), intersection(r1, r2));
		
		// top left and bottom right
		r1.x1 = 1;
		r1.y1 = 2;
		r1.x2 = 2;
		r1.y2 = 1;
		r2.x1 = 2;
		r2.y1 = 1;
		r2.x2 = 1;
		r2.y2 = 2;
		assertEquals((2 -1) * (2 - 1), intersection(r1, r2));
	}

}
