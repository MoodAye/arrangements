package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set28;

import java.io.PrintWriter;
import java.util.Scanner;

/* Programmer's School Problem #641 */
// 632pm - 717pm - Wrong Answer on test 4
public class StrangeLotter {	
	// Test Cases
	// 2571 --> 71
	// 5714 --> 74
	// 992571 --> 9971
	// 9971 --> 9971
	// 102 --> 2
	// 192 --> 9 
	// 912 --> 9
	// 9102 --> 92
	// 1102 --> 12
	// 2102 --> 22
	// 4302 --> 43
	// 4444 --> 44
	
	public void solve(Scanner in, PrintWriter out) {
		//find first number from left that is lower than the next number.  Delete it.
		//start again and find the next number that is lower than the next number.  Delete it.
		String n = in.next();
		int index = findIndexToDelete(n);
		n = n.substring(0,index) + 
				(index == n.length() - 1 ? "" : n.substring(index + 1));
		index = findIndexToDelete(n);
		n = n.substring(0,index) + n.substring(index + 1);
		out.println(n);
	}
	
	int findIndexToDelete(String n) {
		char[] cn = n.toCharArray();
		int index1 = cn.length - 1;
		
		for(int i = 0; i < cn.length - 1; i++) {
			if(cn[i] < cn[i + 1]) {
				index1 = i;
				break;
			}
		}
		return index1;
	}
	
	public static void main(String[] args) {
		new StrangeLotter().run();
	}
	
	public void run() {
		try(Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)){
			solve(in, out);
		}
	}
}
