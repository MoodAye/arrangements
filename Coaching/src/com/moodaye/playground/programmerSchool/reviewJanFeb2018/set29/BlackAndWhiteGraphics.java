package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set29;
import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #530 */
//Start = 10:43am End = 11:01am
public class BlackAndWhiteGraphics {
	void solve(Scanner in, PrintWriter out) {
		int w = in.nextInt();
		int h = in.nextInt();
		int[][] image1 = new int[h][w];
		for(int row = 0; row < h; row++){
			char[] cin = in.next().toCharArray();
			for(int col = 0; col < w; col++){
				image1[row][col] =	cin[col] - '0';
			}
		}
		
		int[][] image2 = new int[h][w];
		for(int row = 0; row < h; row++){
			char[] cin = in.next().toCharArray();
			for(int col = 0; col < w; col++){
				image2[row][col] =	cin[col] - '0';
			}
		}
		int[][] table = new int[2][2];
		char[] cin = in.next().toCharArray();
		table[0][0] = cin[0] - '0';
		table[0][1] = cin[1] - '0';
		table[1][0] = cin[2] - '0';
		table[1][1] = cin[3] - '0';
		
		for(int row = 0; row < h; row++){
			for(int col = 0; col < w; col++){
				out.print(table[image1[row][col]][image2[row][col]]);
			}
			out.println();
		}
	}

	public static void main(String args[]) {
		new BlackAndWhiteGraphics().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
