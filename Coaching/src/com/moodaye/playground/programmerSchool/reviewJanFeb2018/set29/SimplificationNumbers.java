package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set29;

import java.util.Scanner;
import java.util.Arrays;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #689 */
// Start = 9:51am; End = 10:35am
public class SimplificationNumbers {
	void solve(Scanner in, PrintWriter out) {
		int tests = in.nextInt();
		for(int i = 0; i < tests; i++){
			int n = in.nextInt();
			int minComp = Integer.MAX_VALUE;
			String ans = "";
			int minBase = 0;
			for(int base = 2; base <= 36; base++){
				String nBase = convertToBase(n, base);
				int comp = complexity(nBase);
				if(comp < minComp){
					minComp = comp;
					ans = nBase;
					minBase = base;
				}
			}
			out.printf("%d %s%n", minBase, ans);
		}
	}

	public static String convertToBase(int n, int base){
		StringBuilder sbn = new StringBuilder();
		while (n != 0){
			int rem = n % base;
			sbn = sbn.append(rem >= 10 ? (char) ('A' + rem - 10) : (char) ('0' + rem));
			n = n / base;
		}
		sbn.reverse();
		return sbn.toString();
	}

	public static int complexity(String n){
		char[] cn = n.toCharArray();
		Arrays.sort(cn);
		int comp = n.length() + 1;
		for(int i = 1; i < cn.length; i++){
			if(cn[i] == cn[i - 1]){
				continue;
			}
			comp++;
		}
		return comp;
	}

	public static void main(String args[]) {
		new SimplificationNumbers().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
