package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set29;

import static org.junit.Assert.*;

import org.junit.AfterClass;
import org.junit.Test;
import static com.moodaye.playground.programmerSchool.reviewJanFeb2018.set29.SimplificationNumbers.*;
public class SimplificationNumbersTest {

	@Test
	public void test() {
		assertEquals("10",convertToBase(2,2));
		assertEquals("Z",convertToBase(35,36));
		assertEquals("1020",convertToBase(1020,10));
		assertEquals("FFFF",convertToBase(65535,16));
	}

}
