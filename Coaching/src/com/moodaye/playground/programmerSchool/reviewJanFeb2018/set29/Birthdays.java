package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set29;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #490 */
// Start = 11:04am End = 11:22am
// Version with useDelimiter is giving me runtime error on test case 14
public class Birthdays {
	void solve(Scanner in, PrintWriter out) {
		in.useDelimiter("\\.|\\s+");
		
		int d1 = in.nextInt();
		int m1 = in.nextInt();
		int y1 = in.nextInt();
		
		int d2 = in.nextInt();
		int m2 = in.nextInt();
		int y2 = in.nextInt();
		
		int d = d1, m = m1, y = y1;
		int days = 0;
		
		while(d != d2 || m != m2 || y != y2){
			days++;
			d++;
			if(d == 29 && m == 2){
				d = 1;
				m++;
			}
			else if( d == 31 && (m == 4 || m == 6 || m == 9 || m == 11)){
				d = 1;
				m++;
			}
			else if( d == 32 && (m == 1 || m == 3 || m == 5 || m == 7 || m == 8 || m == 10 || m == 12)){
				d = 1;
				if(m == 12){
					m = 1; 
					y++;
				}
				else{
					m++;
				}
			}
		}
		out.println(days);
	}

	public static void main(String args[]) {
		new Birthdays().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
