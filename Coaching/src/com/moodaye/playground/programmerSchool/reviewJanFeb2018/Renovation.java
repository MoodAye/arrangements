package com.moodaye.playground.programmerSchool.reviewJanFeb2018;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #697 Start = 9:43am End = 9:49 */
/* Complexity = O(1)
 * Space = O(1)
 */
public class Renovation {
	void solve(Scanner in, PrintWriter out) {
		int lngth = in.nextInt();
		int wdth = in.nextInt();
		int hght = in.nextInt();
		int area = 2 * lngth * hght + 2 * wdth * hght;
		out.println((area + 16 - 1) / 16);     
	}

	public static void main(String args[]) {
		new Renovation().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
