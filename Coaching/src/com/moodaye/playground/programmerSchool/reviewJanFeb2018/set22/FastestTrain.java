package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set22;

import java.util.Scanner;
import java.util.regex.MatchResult;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #89 */
public class FastestTrain {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		in.nextLine();
		String fastest = "";
		int time = Integer.MAX_VALUE;
		for (int i = 0; i < n; i++) {
			in.findInLine("(\\\"[a-zA-Z0-9 _-]+\\\") ([0-9][0-9]:[0-9][0-9]) ([0-9][0-9]:[0-9][0-9])");
			MatchResult result = in.match();
			String name = result.group(1);
			String start = result.group(2);
			String end = result.group(3);
			int duration = duration(start, end);
			if(time > duration){
				time = duration;
				fastest = name;
			}
			in.nextLine();
		}
		out.printf("The fastest train is %s.%n", fastest);
		out.printf("Its speed is %d km/h, approximately.%n", (int) (650f * 60 * 2 + time)/ (2 * time));
	}
	
	public static int duration(String start, String end){
		int startMin = Integer.valueOf(start.substring(0,2)) * 60 + 
				Integer.valueOf(start.substring(3));
		int endMin = Integer.valueOf(end.substring(0,2)) * 60 + 
				Integer.valueOf(end.substring(3));
		if(endMin < startMin){
			endMin += 24 * 60;
		}
		return endMin == startMin ? 24 * 60 : endMin - startMin;
	}

	public static void main(String args[]) {
		new FastestTrain().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
