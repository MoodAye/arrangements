package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set22;

import java.util.Scanner;
import java.util.Arrays;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #651 */
public class TransformationMonocellular {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int m = in.nextInt();
		int gcd = gcd(n, m);
		out.println(primeCount( n / gcd) + primeCount(m / gcd));
	}
	
	public static int primeCount(int a){
		int cnt = 0;
		for(int i = 2; i * i  < (a + 9) && a != 1; i++){
			while(a % i == 0){
				cnt++;
				a /= i;
			}
		}
		return a == 1 ? cnt : cnt + 1;
	}

	public static int gcd(int a, int b){
		while(b != 0){
			int rem = a % b;
			a = b;
			b = rem;
		}
		return a;
	}

	public static void main(String args[]) {
		new TransformationMonocellular().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
