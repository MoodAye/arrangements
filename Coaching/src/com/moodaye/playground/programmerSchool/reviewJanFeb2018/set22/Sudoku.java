package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set22;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #88 */
public class Sudoku {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int[][] sudo = new int[n * n][n * n];
		for (int i = 0; i < n * n; i++) {
			for (int j = 0; j < n * n; j++) {
				sudo[i][j] = in.nextInt();
			}
		}

		// check rows
		for (int i = 0; i < n; i++) {
			boolean[] was = new boolean[n * n];
			for(int k : sudo[i]){
				if(k > n * n){
					out.println("Incorrect");
					return;
				}
				was[k - 1] = true;
			}
			if(!found(was)){
				out.println("Incorrect");
				return;
			}
		}

		// check columns
		for (int colid = 0; colid < n * n; colid++) {
			boolean[] was = new boolean[n * n];
			for (int rowid = 0; rowid < n * n; rowid++) {
				was[sudo[rowid][colid] - 1] = true;
			}
			if(!found(was)){
				out.println("Incorrect");
				return;
			}
		}

		// check blocks
		for (int col = 0; col < n * n; col += n) {
			for (int row = 0; row < n * n; row += n) {
				boolean[] was = new boolean[n * n];
				for (int i = 0; i < n; i++) {
					for (int j = 0; j < n; j++) {
						was[sudo[row + i][col + j] - 1] = true;
					}
				}
				if (!found(was)) {
					out.println("Incorrect");
					return;
				}
			}
		}
		out.println("Correct");
	}

	public static boolean found(boolean[] was) {
		for (boolean b : was) {
			if (!b) {
				return false;
			}
		}
		return true;
	}

	public static void main(String args[]) {
		new Sudoku().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
