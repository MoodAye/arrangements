package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set26;
import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #623 */
public class FibonacciAgain {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int fib2 = 1;
		int fib1 = 1;
		int fib = 1;
		for(int i = 2; i <= n; i++){
			fib = (fib2 + fib1) % 10;
			fib2 = fib1;
			fib1 = fib;
		}
		out.print(fib);
	}

	public static void main(String args[]) {
		new FibonacciAgain().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
