package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set26;

import java.util.Scanner;
import java.util.Arrays;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #119 */
/* Start Time = 10:43pm End Time = 10:48pm*/
public class TimeSorting {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int[] times = new int[n];
		for(int i = 0; i < n; i++){
			int h = in.nextInt();
			int m = in.nextInt();
			int s = in.nextInt();
			times[i] =  h * 3600 + m * 60 + s;
		}
		Arrays.sort(times);
		for(int time : times){
			out.printf("%d %d %d%n", time / 60 / 60, time / 60 % 60, time % 60);
		}
	}

	public static void main(String args[]) {
		new TimeSorting().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
