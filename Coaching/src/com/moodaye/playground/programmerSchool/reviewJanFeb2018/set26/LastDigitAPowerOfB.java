package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set26;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #79 */
public class LastDigitAPowerOfB {
	void solve(Scanner in, PrintWriter out) {
		int a = in.nextInt();
		int b = in.nextInt();
		int ans = 1;
		for(int i = 1; i <=b; i++){
			ans *= a;
			ans %= 10;
		}
		out.println(ans);
	}
	
	public static int lastDigit(int a, int b){
		int lastDigit = 1;
		
		int lda = a % 10;
		
		switch(lda){
		case 0:
		case 1:
		case 5:
		case 6:
			lastDigit = lda;
			break;
		case 4:
		case 9:
			int mod2 = b % 2;
			lastDigit = mod2 == 0 ? lastDigit * lda : lda;
			break;
		case 2:
		case 3:
		case 7:
		case 8:
			int mod4 = b % 4;
			mod4 = mod4 == 0 ? 4 : mod4;
			for(int i = 0; i < mod4; i++){
				lastDigit = (lastDigit * lda) % 10;
			}
			break;
		}
		return lastDigit;
	}

	public static void main(String args[]) {
		new LastDigitAPowerOfB().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
