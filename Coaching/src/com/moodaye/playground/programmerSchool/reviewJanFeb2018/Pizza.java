package com.moodaye.playground.programmerSchool.reviewJanFeb2018;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #554 Start = 10:48pm End = 11:04pm*/
/*
 * Each time you cut across a line, you create another slice.
 * 2 + 2 + 3 + 4 + ... + n..
 * cnt = ((1 + n) * n) / 2 + 1;
 * 
 * Solved this first using a loop ... which does not require a proof so might be a better approach?
 */
public class Pizza {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		out.println(((1 + n) * n) / 2 + 1);
	}

	public static void main(String args[]) {
		new Pizza().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
