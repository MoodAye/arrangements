package com.moodaye.playground.programmerSchool.reviewJanFeb2018;

import java.util.Scanner;
import java.util.Arrays;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #340 */
/*
 * Start Time = 8:49pm End Time = 8:54pm
 */
public class Boxes {
	void solve(Scanner in, PrintWriter out) {
		int[] box1 = new int[3];
		int[] box2 = new int[3];
		for(int i = 0; i < 3; i++){
			box1[i] = in.nextInt();
		}
		for(int i = 0; i < 3; i++){
			box2[i] = in.nextInt();
		}
		Arrays.sort(box1);
		Arrays.sort(box2);
		if(box1[0] == box2[0] && box1[1] == box2[1] && box1[2] == box2[2]){
			out.println("Boxes are equal");
		}
		else if(box1[0] <= box2[0] && box1[1] <= box2[1] && box1[2] <= box2[2]){
			out.println("The first box is smaller than the second one");
		}
		else if(box1[0] >= box2[0] && box1[1] >= box2[1] && box1[2] >= box2[2]){
			out.println("The first box is larger than the second one");
		}
		else{
			out.println("Boxes are incomparable");
		}
	}

	public static void main(String args[]) {
		new Boxes().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
