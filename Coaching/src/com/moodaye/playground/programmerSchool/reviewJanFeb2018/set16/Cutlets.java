package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set16;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #664 */
public class Cutlets {
	void solve(Scanner in, PrintWriter out) {
		int pan = in.nextInt();
		int min = in.nextInt();
		int n = in.nextInt();
		
		//first - how many batches required to fry first side of cutlets
		int batches = (n + pan - 1)/ pan;
		//subsequent batches needed
		if(batches == 1){
			batches++;
		}
		else{
			batches += ((2 * n - pan * batches) + (pan - 1)) / pan;
		}
		
		out.println(batches * min);
	}

	public static void main(String args[]) {
		new Cutlets().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
