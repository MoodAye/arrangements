package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set6;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #159 Start = 11:37 End = 11:42*/
public class InversePermutation {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int[] perm = new int[n + 1];
		int[] inv = new int[n + 1];
		for(int i = 1; i <= n; i++){
			perm[i] = in.nextInt();
		}
		for(int i = 1; i <= n; i++){
			inv[perm[i]] = i;
		}
		for(int i = 1; i <= n; i++){
			out.print(inv[i] + " ");
		}
	}

	public static void main(String args[]) {
		new InversePermutation().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
