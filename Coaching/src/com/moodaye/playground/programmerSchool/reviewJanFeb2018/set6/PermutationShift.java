package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set6;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #392 */
/*
 * Could not figure this out yesterday -- thought about it on the way to work
 * and realized that the sequence would need to begin with one.
 */
public class PermutationShift {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int[] a = new int[n];
		int[] b = new int[n];
		int shift = -1;
		for (int i = 0; i < n; i++) {
			a[i] = in.nextInt();
			if (a[i] == 1) {
				shift = n - i;
			}
		}

		for (int i = 0; i < n; i++) {
			b[(i + shift) % n] = a[i];
		}

		for (int p : b) {
			out.print(p + " ");
		}
	}

	public static void main(String args[]) {
		new PermutationShift().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
