package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set6;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #283 Start = 12:10am End = 12:24am*/
public class RuneWords {
	void solve(Scanner in, PrintWriter out) {
		char[] word = in.next().toCharArray();
		int len = 0;
		for(char c : word){
			if(c >= 'A' && c <= 'Z'){
				if(len == 1){
					out.println("NO");
					return;
				}
				len = 1;
				continue;
			}
			else{
				if(len < 1 || len >= 4){
					out.println("NO");
					return;
				}
				len++;
			}
		}
		out.println("YES");
	}

	public static void main(String args[]) {
		new RuneWords().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
