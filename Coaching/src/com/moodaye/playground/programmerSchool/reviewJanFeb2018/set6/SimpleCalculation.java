package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set6;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #59 Start = 6:48pm End = 7:06pm*/
/*
 * Did not immediately think of a way to do this yesterday. 
 * 
 */
public class SimpleCalculation {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int pow = in.nextInt();
		int sum = 0;
		int prod = 1;
		while (n != 0) {
			int rem = n % pow;
			n /= pow;
			sum += rem;
			prod *= rem;
		}
		out.println(prod - sum);
	}

	public static void main(String args[]) {
		new SimpleCalculation().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
