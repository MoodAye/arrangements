package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set6;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #231 Start = 11:45pm End 12:08am */
public class StringUnpacking {
	void solve(Scanner in, PrintWriter out) {
		char[] packed = in.next().toCharArray();
		int multiplier = 0;
		int line = 0;
		for (char c : packed) {
			if (c >= '0' && c <= '9') {
				multiplier = multiplier * 10 + (c - '0');
			} 
			else {
				if (multiplier == 0) {
					multiplier = 1;
				}
				for (int j = 1; j <= Integer.valueOf(multiplier); j++) {
					if (line % 40 == 0) {
						out.println();
					}
					out.print(c);
					line++;
				}
				multiplier = 0;
			}
		}
	}

	public static void main(String args[]) {
		new StringUnpacking().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
