package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set8;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #47 Start = 10:10pm End = 10:25pm*/

// problem seems to be one of determining divisors.
// Brute force complexity = O(n)		
public class BestDivisor {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int best = 1;
		int sumOfBest = 1;
		for (int i = 2; i <= n; i++) {
			if (n % i == 0) {
				int sum = getSum(i);
				if(sumOfBest < sum){
					best = i;
					sumOfBest = sum;
				}
			}
		}
		out.println(best);
	}

	public static int getSum(int n) {
		int sum = 0;
		while (n != 0) {
			sum += n % 10;
			n /= 10;
		}
		return sum;
	}

	public static void main(String args[]) {
		new BestDivisor().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
