package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set8;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #315 Start = 10:32pm Pause = 10:48pm*/
// Solved next day .. ~time = 25min.
public class MinimalNumeralSystem {
	void solve(Scanner in, PrintWriter out) {
		char[] cs = in.nextLine().toCharArray();
		int max = 0;
		for(char c : cs){
			if(c < '0' || (c > '9' && c < 'A') || c > 'Z'){
				out.println(-1);
				return;
			}
			if(c <= '9'){
				max = Math.max(max, c - '0' + 1);
			}
			else{
				max = Math.max(max, c - 'A' + 11);
			}
		}
		out.println(max < 2 ? 2 : max);
	}

	public static void main(String args[]) {
		new MinimalNumeralSystem().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
