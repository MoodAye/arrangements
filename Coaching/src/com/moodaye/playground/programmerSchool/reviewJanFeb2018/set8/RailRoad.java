package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set8;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #409 Start = 9:38pm Pause = 10:05pm */
// Should be the average of the heights.  Also, seems like we ignore first measurement since 
// land is flat between measurements.
// failing on test 2 - seems to be correct when run locally
public class RailRoad {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
//		int[] hts = new int[n];
		double sum = 0;
		in.nextInt();
		for(int i = 1; i < n; i++){
//			hts[i] = in.nextInt();
			sum += in.nextInt();
		}
		out.printf("%.10f", sum / (n - 1));
	}

	public static void main(String args[]) {
		new RailRoad().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
