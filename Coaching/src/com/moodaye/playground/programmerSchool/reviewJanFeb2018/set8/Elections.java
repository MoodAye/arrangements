package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set8;

import java.util.Scanner;
import java.util.Arrays;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #461 Start = 9:26pm  End = 9:35pm*/
// strategy will be to sort the K groups from smallest to largest and consider the 
// first K/2 + 1 groups.
public class Elections {
	void solve(Scanner in, PrintWriter out) {
		int k = in.nextInt();
		int[] group = new int[k];
		for(int i = 0; i < k; i++){
			group[i] = in.nextInt();
		}
		Arrays.sort(group);
		int min = 0;
		for(int i = 0; i < k/2 + 1; i++){
			min += group[i]/2 + 1;
		}
		out.println(min);
	}

	public static void main(String args[]) {
		new Elections().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
