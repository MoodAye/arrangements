package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set8;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #513 Start = 10:50pm Pause = 10:53*/
/*  Failing test 7
 */
public class Salads {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		long cnt = 0;
		for(int i = 2; i <= n; i++){
			// nCi
			cnt += nCi(n, i);
		}
		out.println(cnt);
	}

	// n (n - 1) (n - 2) ... (n - i - 1) / i (i - 1) ..(1)
	public static long nCi(long n, long i){
		long num = 1;
		long den = 1;
		for(int k = 0; k < i; k++){
			num *= (n - k);
			den *= (i - k);
		}
		return num / den;
	}

	public static void main(String args[]) {
		new Salads().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
