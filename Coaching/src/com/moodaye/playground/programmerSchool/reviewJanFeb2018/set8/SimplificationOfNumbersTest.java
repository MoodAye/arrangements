package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set8;

import static org.junit.Assert.*;

import org.junit.Test;
import static com.moodaye.playground.programmerSchool.reviewJanFeb2018.set8.SimplificationOfNumbers.*;
public class SimplificationOfNumbersTest {

	@Test
	public void test() {
		assertEquals("FFFF", baseValue(16, 65535));
		assertEquals("1000", baseValue(2, 8));
	}
}
