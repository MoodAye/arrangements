package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set8;

import java.util.Scanner;
import java.util.Arrays;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #689 Start = 10:54pm Pause = 10:55pm */
// Took about 1 hr to solve.  Had to review my notes on changing the base of a number.
public class SimplificationOfNumbers {
	void solve(Scanner in, PrintWriter out) {
		int t = in.nextInt();
		for (int test = 0; test < t; test++) {
			int n = in.nextInt();
			int base = 0;
			String value = "";
			int minComplexity = Integer.MAX_VALUE;
			for (int i = 2; i <= 36; i++) {
				String v = baseValue(i, n);
				int complexity = v.length() + uniqueChars(v);
				if (minComplexity > complexity) {
					minComplexity = complexity;
					base = i;
					value = v;
				}
			}
			out.println(base + " " + value);
		}
	}

	public static int uniqueChars(String s) {
		char[] cs = s.toCharArray();
		Arrays.sort(cs);
		int cnt = 0;
		for (int i = 0; i < cs.length - 1; i++) {
			if (cs[i] == cs[i + 1]) {
				continue;
			}
			cnt++;
		}
		return cnt;
	}

	public static String baseValue(int b, int n) {
		char[] ascii = new char[36];
		for(int i = 0; i < 10; i++){
			ascii[i] = (char) ('0' + i);
		}
		for(int i = 10; i < 36; i++){
			ascii[i] = (char) ('A' + i - 10);
		}
		StringBuilder sb = new StringBuilder();
		while (n != 0) {
			sb.append(ascii[n % b]);
			n /= b;
		}
		return sb.reverse().toString();
	}

	public static void main(String args[]) {
		new SimplificationOfNumbers().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
