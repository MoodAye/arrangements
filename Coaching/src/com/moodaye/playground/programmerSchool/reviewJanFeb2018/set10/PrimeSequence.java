package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set10;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #64 */
public class PrimeSequence {
	void solve(Scanner in, PrintWriter out) {
		int t = in.nextInt();
		String ps = primeSequence(30_000);
		for (int i = 0; i < t; i++) {
			out.print(ps.charAt(in.nextInt() - 1));
		}
	}

	public static String primeSequence(int size) {
		StringBuilder sb = new StringBuilder();
		boolean[] isPrime = new boolean[size];
		for (int i = 0; i < size; i++) {
			isPrime[i] = true;
		}
		isPrime[0] = false;
		isPrime[1] = false;

		for (int i = 2; i < size; i++) {
			if (isPrime[i] == true) {
				for (int j = i * i; j < size; j += i) {
					isPrime[j] = false;
				}
				sb.append(i);
			}
		}
		return sb.toString();
	}

	public static void main(String args[]) {
		new PrimeSequence().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
