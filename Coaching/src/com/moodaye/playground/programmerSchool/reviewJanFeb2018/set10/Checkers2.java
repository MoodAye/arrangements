package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set10;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #684 */
public class Checkers2 {
	void solve(Scanner in, PrintWriter out) {
		String start = in.next();
		String target = in.next();
	
		//start has to be black
		if(!isBlack(start)){
			throw new AssertionError("Starting square is not black");
		}
		//target has to be black
		if(!isBlack(target)){
			out.println("NO");
			return;
		}
		//squares have to be valid
		if(!isValid(start) || !isValid(target)){
			throw new AssertionError("Square is invalid");
		}
		
		//target has to be higher than start
		if(start.charAt(1) > target.charAt(1)){
			out.println("NO");
			return;
		}
		if(Math.abs(target.charAt(1) - start.charAt(1)) < Math.abs(target.charAt(0) - start.charAt(0))){
			out.println("NO");
			return;
		}
		out.println("YES");
	}
	
	public static boolean isValid(String pos){
		return(pos.charAt(0) >= 'a' && pos.charAt(0) <= 'h'
				&& pos.charAt(1) >= '1' && pos.charAt(1) <= '8');
	}
	
	public static boolean isBlack(String pos){
		return (pos.charAt(0) + pos.charAt(1)) % 2 == 0;
	}

	public static void main(String args[]) {
		new Checkers2().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
