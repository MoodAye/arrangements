package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set10;

import java.util.Scanner;
import java.util.Arrays;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #88 */
// Failing on test 6
public class Sudoku {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		in.nextLine();
		String[] rows = new String[n * n];
		for (int i = 0; i < n * n; i++) {
			rows[i] = in.nextLine().replaceAll(" ", "");
		}
		char[] all = new char[n * n];
		for (int i = 0; i < n * n; i++) {
			all[i] = (char) ('1' + i);
		}

		// check rows
		for (int i = 0; i < n * n; i++) {
			char[] cRow = rows[i].toCharArray();
			Arrays.sort(cRow);
			if (!Arrays.equals(all, cRow)) {
				out.println("Incorrect");
				return;
			}
		}

		// check columns
		for (int i = 0; i < n * n; i++) {
			char[] col = new char[n * n];
			for (int j = 0; j < n * n; j++) {
				col[j] = rows[j].toCharArray()[i];
			}
			Arrays.sort(col);
			if (!Arrays.equals(all, col)) {
				out.println("Incorrect");
				return;
			}
		}

		// check blocks
		for (int i = 0; i < n; i = i + n) {
			for (int j = 0; j < n; j = j + n) {
				char[] block = new char[n * n];
				for (int k = 0; k < n; k++) {
					for (int l = 0; l < n; l++) {
						block[k * n + l] = rows[i + k].toCharArray()[j + l];
					}
				}
				Arrays.sort(block);
				if (!Arrays.equals(all, block)) {
					out.println("Incorrect");
					return;
				}
			}
		}
		out.println("Correct");
	}

	public static void main(String args[]) {
		new Sudoku().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
