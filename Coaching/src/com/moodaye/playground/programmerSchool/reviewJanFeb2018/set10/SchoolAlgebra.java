package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set10;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #277 */
public class SchoolAlgebra {
	void solve(Scanner in, PrintWriter out) {
		int a = in.nextInt();
		int b = in.nextInt();
		int c = in.nextInt();
		
		String trin = getString(a, "") + getString(b, "x") + getString(c, "y");
		if(trin.equals("")){
			out.println(0);
			return;
		}
		if(trin.charAt(0) == '+'){
			out.println(trin.substring(1));
			return;
		}
		out.println(trin);
	}
	
	public static String getString(int a, String x){
		if(a == 0){
			return "";
		}
		else if(a == 1 || a == -1){
			if(x.equals("")){
				return String.valueOf(a);
			}
			else{
				return a == 1 ? "+" + x : "-" + x;
			}
		}
		else if(a > 0){
			return "+" + String.valueOf(a) + x;
		}
		else{
			return String.valueOf(a) + x;
		}
	}

	public static void main(String args[]) {
		new SchoolAlgebra().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}

}
