package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set10;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #354 */
// memory limit exceeded on test 14
public class PrimeFactorization {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		printFactors2(n);
	}

	public static void printFactors2(int n) {
		int size = n + 1;
		int primes = 0; 
		primes = primes | 1; 

		String star = "";
		for (int i = 2; i < size; i++) {
			if ((primes & (1 << i)) == 0) { 
				for (int j = i * i; j < size; j += i) {
					int mask = (1 << j);
					primes = primes | mask;
				}
				while (n % i == 0) {
					System.out.print(star + i);
					star = "*";
					n /= i;
				}
			}
		}
	}

	public static void printFactors(int n) {
		int size = n + 1;
		boolean[] isPrime = new boolean[size];
		for (int i = 0; i < size; i++) {
			isPrime[i] = true;
		}
		isPrime[0] = false;
		isPrime[1] = false;

		String star = "";
		for (int i = 2; i < size; i++) {
			if (isPrime[i] == true) {
				for (int j = i * i; j < size; j += i) {
					isPrime[j] = false;
				}
				while (n % i == 0) {
					System.out.print(star + i);
					star = "*";
					n /= i;
				}
			}
		}
	}

	public static void main(String args[]) {
		new PrimeFactorization().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
