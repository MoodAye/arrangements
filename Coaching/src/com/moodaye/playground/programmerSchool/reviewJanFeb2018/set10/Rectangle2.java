package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set10;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #182 */
public class Rectangle2 {
	void solve(Scanner in, PrintWriter out) {
		int x1 = in.nextInt();
		int y1 = in.nextInt();
		int x2 = in.nextInt();
		int y2 = in.nextInt();
		int x3 = in.nextInt();
		int y3 = in.nextInt();
		int x4 = 0;
		int y4 = 0;
		
		//find diagnol
		int s12 = (x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2);
		int s23 = (x2 - x3) * (x2 - x3) + (y2 - y3) * (y2 - y3);
		int s31 = (x3 - x1) * (x3 - x1) + (y3 - y1) * (y3 - y1);
		
		if(s12 > s23 && s12 > s31){
			x4 = x2 - (x3 - x1);
			y4 = y2 - (y3 - y1);
		}
		else if(s23 > s12 && s23 > s31){
			x4 = x3 - (x1 - x2);
			y4 = y3 - (y1 - y2);
		}
		else if(s31 > s12 && s31 > s23){
			x4 = x1 - (x2 - x3);
			y4 = y1 - (y2 - y3);
		}
		else{
			throw new AssertionError("invalid inputs");
		}
		out.println(x4 + " " + y4);
	}

	public static void main(String args[]) {
		new Rectangle2().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
