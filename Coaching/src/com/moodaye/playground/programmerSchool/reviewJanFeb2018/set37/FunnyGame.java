package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set37;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #208 */
// Start Time = 8:12pm End Time = 8:51pm ; Time Taken = 39min
public class FunnyGame {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		
		if(n == 0){
			out.println(0);
			return;
		}
			
		int mask = 1 << 15;
		while((n & mask) == 0){
			mask >>= 1;
		}
			
		int rotN = rotateBits(n, mask);
		int max = n;
		while(rotN != n){
			max = Math.max(rotN, max);
			rotN = rotateBits(rotN, mask);
		}
		out.println(max);
	}
	
	
	static int rotateBits(int n, int mask){
		if((n & 1) == 1){
			return (mask | (n >> 1));
		}
		return n >> 1;
	}
	
	public static void main(String args[]) {
		new FunnyGame().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
