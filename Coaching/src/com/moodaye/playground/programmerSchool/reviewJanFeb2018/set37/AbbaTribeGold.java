package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set37;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #7 */
// Start Time = 11:55pm End Time 12:02 Time Taken = 7min
public class AbbaTribeGold {
	void solve(Scanner in, PrintWriter out) {
		String n1 = in.next();
		String n2 = in.next();
		String n3 = in.next();
		out.println(maxString(n3,maxString(n1, n2)));
	}
	
	static String maxString(String n1, String n2){
		if(n1.length() == n2.length()){
			if(n1.compareTo(n2) > 0){
				return n1;
			}
			else{
				return n2;
			}
		}
		return n1.length() > n2.length() ? n1 : n2;
	}

	public static void main(String args[]) {
		new AbbaTribeGold().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
