package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set37;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #651 */
// Start Time = 10:10pm End Time = 11:02 ; Time Taken = 52min
// Spent 5 more minutes simplifying the solution
public class TransformationMonocellular {
	void solve(Scanner in, PrintWriter out) {
		long n =  in.nextInt();
		long m = in.nextInt();
		long hcd = hcd(n, m);
		out.println(cntFactors(n / hcd) + cntFactors(m / hcd));
		int x = Integer.MAX_VALUE;
	}
	
	static int cntFactors(long n){
		int cnt = 0;
		for(long p = 2; p * p <= n && n != 1; p++){
			while(n % p == 0){
				n /= p;
				cnt++;
			}
		}
		if (n != 1){
			cnt++;
		}
		return cnt;
	}

	static long hcd(long n, long m){
		while(m != 0){
			long temp = n % m;
			n = m;
			m = temp;
		}
		return n;
	}

	public static void main(String args[]) {
		new TransformationMonocellular().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
