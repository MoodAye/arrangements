package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set39;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #283 */
// Start Time = 6:35am Pause = 7:18am (Failing on test 5)
// Resume 11:53am End: 12:04pm Total Time = 54min
// Spent additional 15min to refactor code to simplify
// Start 7:40 End 8:02 (Time Taken = 22min to figure out regex to solve complete input string without need to break into individual words)
public class RuneWords {
	void solve(Scanner in, PrintWriter out) {
		String words = in.next();
		out.println(soln4(words));
	}
	
	public static String soln4(String words){
		if (words.matches("([A-Z][a-z]{1,3})+")) {
			return "Yes";
		}
		return "No";
	}
	
	public static String soln2(String words) {
		StringBuilder word = new StringBuilder().append(words.charAt(0));
		for (int i = 1; i < words.length(); i++) {
			if ('A' <= words.charAt(i) && words.charAt(i) <= 'Z') {
				if (!isRune(word.toString())) {
					return "No";
				}
				word = new StringBuilder().append(words.charAt(i));
			} else {
				word.append(words.charAt(i));
			}
		}
		return isRune(word.toString()) ? "Yes" : "No";
	}

	static boolean isRune(String word) {
		if (2 > word.length() || word.length() > 4) {
			return false;
		}

		if (word.matches("^[A-Z][a-z].*")) {
			return true;
		}
		return false;
	}

	public static void main(String args[]) {
		new RuneWords().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
