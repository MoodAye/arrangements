package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set39;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #645 */
// total time taken ~1hr
public class BeautifulWall {
	void solve(Scanner in, PrintWriter out) {
		int k = in.nextInt();
		int bestH = 0;
		int minScore = Integer.MAX_VALUE;
		for(int h = 1; h * h <= k; h++){
			if(minScore > score(k, h, k / h)){
				minScore = score(k, h, k / h);
				bestH = h;
			}
		}
		out.printf("%d %d", bestH, k / bestH);
	}
	
	static int score(int k, int h, int w){
		return (k - h * w) + Math.abs(h - w);
	}

	public static void main(String args[]) {
		new BeautifulWall().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
