package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set39;

import static org.junit.Assert.*;

import org.junit.AfterClass;
import org.junit.Test;
import static com.moodaye.playground.programmerSchool.reviewJanFeb2018.set39.TwoN.*;
public class TwoNTest {

	@Test
	public void test() {
		assertEquals("4", multiply(2,"2"));
		assertEquals("20", multiply(10,"2"));
		assertEquals("462", multiply(2,"231"));
		assertEquals("182", multiply(2,"91"));
		assertEquals("402", multiply(2,"201"));
		assertEquals("99900", multiply(100,"999"));
	}

}
