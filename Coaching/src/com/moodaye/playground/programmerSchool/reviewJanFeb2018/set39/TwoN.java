package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set39;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School # */
// Start = 7:19am End = 7:33am Time Taken - 14min
public class TwoN {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		String s = "1";
		for(int i = 0; i < n; i++){
			s = multiply(2, s);
		}
		out.println(s);
	}
	
	
	public static String multiply(int f, String s){
		char[] sc = s.toCharArray();
		StringBuilder sb = new StringBuilder();
		int carry = 0;
		for(int i = sc.length - 1; i >= 0; i--){
			int m = (sc[i] - '0') * f + carry;
			int digit = m % 10;
			carry = m / 10;
			sb.append(digit);
		}
		if(carry > 0){
			sb.append(carry);
		}
		return sb.reverse().toString();
	}

	public static void main(String args[]) {
		new TwoN().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
