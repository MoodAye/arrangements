package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set36;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #551 */
// Start Time = 8:01pm End Time = 8:30pm.  Time Taken = 29minutes
public class HareAndTree {
	void solve(Scanner in, PrintWriter out) {
		int rFence = in.nextInt();
		int rTree = in.nextInt();
		int h = in.nextInt();
		int b = in.nextInt();
		
		if((h + rTree - b) * (h + rTree - b) > rFence * rFence - rTree * rTree){
			out.println("NO");
		}
		else{
			out.println("YES");
		}
	}

	public static void main(String args[]) {
		new HareAndTree().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
