package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set36;

import java.util.Scanner;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Locale;

/** Programmer's School #118 */
// Start Time = 12:15pm End Time = 12:50pm Time Taken = 35min
public class JosephusProblem {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int k = in.nextInt();
		boolean[] alive = new boolean[n];
		Arrays.fill(alive, true);

		int idx = n - 1;
		for (int i = 0; i < alive.length; i++){
			int cnt = 0;
			while (cnt != k) {
				idx = (idx + 1) % alive.length;
				if (alive[idx]) {
					cnt++;
				}
			}
			alive[idx] = false;
		}
		out.println(idx + 1);
	}


	int kill(boolean[] alive, int idx, int k){
		//Base case
		boolean allDead = true;
		for(int i = 0; i < alive.length; i++){
			if(alive[i]){
				allDead = false;
				break;
			}
		}
		if(allDead){
			return -1;
		}
		
		int cnt = 0;
		while(cnt != k){
			idx = (idx + 1) % alive.length;
			if(alive[idx]){
				cnt++;
			}
		}
		alive[idx] = false;
		int id = kill(alive, idx, k);
		return id == -1 ? idx : id;
	}

	public static void main(String[] args) {
		new JosephusProblem().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}

}
