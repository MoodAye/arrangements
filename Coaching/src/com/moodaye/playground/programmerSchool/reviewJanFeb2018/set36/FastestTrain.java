package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set36;

import java.util.Scanner;
import java.util.regex.MatchResult;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #89 */
// Start Time = 8:32pm End Time = 9:17pm Time Taken = 45min
// Wrong Answer - test case 9 - fixed by comparing using seconds instead of
// speed in km/h
public class FastestTrain {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		in.nextLine();
		Train fastest = null;
		for (int i = 0; i < n; i++) {
			in.findInLine("(\\\".*\\\")\\s+([0-9][0-9]):([0-9][0-9])\\s+([0-9][0-9]):([0-9][0-9])");
			MatchResult result = in.match();
			Train train = new Train(result.group(1), Integer.valueOf(result.group(2)), Integer.valueOf(result.group(3)),
					Integer.valueOf(result.group(4)), Integer.valueOf(result.group(5)));
			if (fastest == null) {
				fastest = train;
			} else {
				if (fastest.timeMinutes() > train.timeMinutes()) {
					fastest = train;
				}
			}
			in.nextLine();
		}

		out.println("The fastest train is " + fastest.name + ".");
		out.println("Its speed is " + fastest.speed() + " km/h, approximately.");
	}

	public static void main(String args[]) {
		new FastestTrain().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}

	static class Train {
		String name;
		int depH;
		int depM;
		int arrH;
		int arrM;

		Train(String name, int depH, int depM, int arrH, int arrM) {
			this.name = name;
			this.depH = depH;
			this.depM = depM;
			this.arrH = arrH;
			this.arrM = arrM;
		}

		Train(String name, String dep, String arr) {
			this(name, Integer.valueOf(dep.substring(0, 2)), Integer.valueOf(dep.substring(3)),
					Integer.valueOf(arr.substring(0, 2)), Integer.valueOf(arr.substring(3)));
		}
		
		int timeMinutes(){
			int departure = depH * 60 + depM;
			int arrival = arrH * 60 + arrM;
			if(arrival <= departure){
				arrival += 24 * 60;
			}
			return arrival - departure;
		}

		int timeSeconds() {
			int departure = depH * 3600 + depM * 60;
			int arrival = arrH * 3600 + arrM * 60;
			if (arrival <= departure) {
				arrival += 24 * 3600;
			}
			return arrival - departure;
		}

		long speed() {
			return Math.round(1.0 * 650 * 60 / timeMinutes());
		}

		public String toString() {
			return name + " " + depH + ":" + depM + " " + arrH + ":" + arrM;
		}
	}
}
