package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set14;

import java.util.Scanner;
import java.util.stream.IntStream;
import java.util.Arrays;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #40 */
public class TwoEn {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		out.println(arrayToString(twoExpN(n)));
	}

	public static void main(String args[]) {
		new TwoEn().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
	
	public static String arrayToString(int[] n){
		return Arrays.toString(n).replaceAll("\\[|\\]|,| ", "");
	}
	
	public static int[] twoExpN(int n){
		int[] result = new int[1];
		result[0] = 1;
		while(n > 0){
			result = multiply(result, 2);
			n--;
		}
		result = reverse(result);
		return result;
	}
	
	public static int[] reverse(int[] n){
		int[] temp = new int[n.length];
		for(int i = 0; i < n.length; i++){
			temp[i] = n[n.length - i - 1];
		}
		return temp;
	}

	public static int[] multiply(int n, int x){
		int[] an = new int[n / 10];
		int idx = 0;
		while(n > 0){
			an[idx++] = n % 10;
			n /= 10;
		}
		return multiply(an, x);
	}
		
	public static int[] multiply(int[] an, int x){
		an = Arrays.copyOf(an, an.length + 1);
		int carry = 0;
		for(int i = 0; i < an.length; i++){
			int prod = x * an[i] + carry;
			an[i] = prod % 10;
			carry = prod / 10;
		}
		if(an[an.length - 1] == 0){
			an = Arrays.copyOf(an, an.length - 1);
		}
		return an;
	}
}
