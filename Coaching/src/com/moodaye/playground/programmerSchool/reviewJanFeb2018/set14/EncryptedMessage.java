package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set14;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #295 */
public class EncryptedMessage {
	void solve(Scanner in, PrintWriter out) {
		String intercept = in.next();
		String word = in.next();
		out.println(getMessage(intercept, word));
	}
	
	public static String getMessage(String intercept, String word){
		int shift = 0;
		int minShift = Integer.MAX_VALUE;
		boolean found = false;
		for (int i = 0; i < intercept.length() - word.length() + 1; i++) {
			found = true;
			shift = intercept.charAt(i) - word.charAt(0);
			if(shift < 0){
				shift += 26;
			}
			for (int j = 1; j < word.length(); j++) {
				int ch = intercept.charAt(i + j) - shift;
				if(ch < 'A'){
					ch += 26;
				}
				if (ch != word.charAt(j)) {
					found = false;
					break;
				}
			}
			if (found) {
				if (minShift > shift) {
					minShift = shift;
				}
			}
		}
		if (minShift != Integer.MAX_VALUE){
			StringBuilder sb = new StringBuilder();
			for (char c : intercept.toCharArray()) {
				if (c - minShift < 'A') {
					sb.append((char) ('Z' - ('A' - (c - minShift)) + 1));
				} else if (c - minShift > 'Z') {
					sb.append((char) (c - minShift - 'Z' - 1 + 'A'));
				} else {
					sb.append((char) (c - minShift));
				}
			}
			return sb.toString();
		} else {
			return "IMPOSSIBLE";
		}

	}

	public static void main(String args[]) {
		new EncryptedMessage().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
