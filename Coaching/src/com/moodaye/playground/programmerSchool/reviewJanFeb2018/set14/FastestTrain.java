package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set14;

import java.util.Scanner;
import java.util.regex.MatchResult;
import java.util.Arrays;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #89 */
// Failing with runtime error on test 7
public class FastestTrain {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		
		Train[] trains = new Train[n];
		for(int i = 0; i < n; i++){
			trains[i] = new Train();
		}
		
		in.nextLine();
		for(Train t : trains){
			String line = in.nextLine();
			int idx = line.indexOf("\" ");
			t.name = line.substring(0, idx + 1);
			t.aTime = line.substring(idx + 2, idx + 7);
			t.dTime = line.substring(idx + 8);
			t.time();
		}
		
		Arrays.sort(trains, (t1, t2) -> t1.travelMin - t2.travelMin);
		
		out.printf("The fastest train is %s.%n", trains[0].name);
		out.printf("Its speed is %.0f km/h, approximately.", trains[0].speed());
	}

	public static void main(String args[]) {
		new FastestTrain().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}

class Train{
	String name;
	String aTime;
	String dTime;
	int travelMin;
	void time(){
		int aHr = Integer.valueOf(aTime.substring(0, 2));
		int aMin = Integer.valueOf(aTime.substring(3));
		int aMins = aHr * 60 + aMin;
		
		int dHr = Integer.valueOf(dTime.substring(0, 2));
		int dMin = Integer.valueOf(dTime.substring(3));
		int dMins = dHr * 60 + dMin;
		
		if(dMins <= aMins){
			dMins += 24 * 60;
		}
		
		travelMin = dMins - aMins;
	}
	
	double speed(){
		return Math.round(650f * 60 / travelMin);
	}
}