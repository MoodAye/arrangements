package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set14;

import java.util.Scanner;
import java.util.Arrays;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #651 */
 /* max size of isPrime is 10e9.  Each boolean is 1 byte.
  * 1_000_000_000 bytes or 1Gb.
  * 
 */
public class TransformationOfMonocellular {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int m = in.nextInt();

		int gcd = getGcd(n, m);
		int factor1 = m / gcd;
		int factor2 = n / gcd;
		out.println(primeFactorCount(factor1) + primeFactorCount(factor2));
	}

	public static int primeFactorCount(int n) {
		if (n == 1) {
			return 0;
		}
		boolean[] isPrime = new boolean[n + 1];
		Arrays.fill(isPrime, true);
		int cnt = 0;
		int p = 2;
		while (n != 1) {
			while (n % p == 0) {
				n /= p;
				cnt++;
			}
			
			for (int i = 2 * p; i <= n; i += p) {
				isPrime[i] = false;
			}
			p = getNextPrime(isPrime, p);
		}
		return cnt;
	}

	public static int getNextPrime(boolean[] isPrime, int k) {
		k++;
		while (k < isPrime.length && !isPrime[k]) {
			k++;
			continue;
		}
		return k;
	}

	public static int getGcd(int a, int b) {
		while (b != 0) {
			int rem = a % b;
			a = b;
			b = rem;
		}

		return a;
	}

	public static void main(String args[]) {
		new TransformationOfMonocellular().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
