package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set14;

import static org.junit.Assert.*;

import org.junit.AfterClass;
import org.junit.Test;
import static com.moodaye.playground.programmerSchool.reviewJanFeb2018.set14.EncryptedMessage.*;
public class EncryptedMessageTest {

	@Test
	public void test() {
		assertEquals("HELLOAMERICA", getMessage("HELLOAMERICA", "AMERICA"));
		assertEquals("HELLOAMERICA", getMessage("KHOORDPHULFD", "HELLOAMERICA"));
		assertEquals("IMPOSSIBLE", getMessage("KHOORDPHULFD", "KHOORDPHULFC"));
		assertEquals("ZY", getMessage("AZ", "Y"));
		assertEquals("SJ", getMessage("AR", "S"));
		assertEquals("CD", getMessage("AB", "C"));
		assertEquals("AB", getMessage("YZ", "A"));
		assertEquals("A", getMessage("A", "A"));
		assertEquals("Z", getMessage("Z", "Z"));
		assertEquals("Z", getMessage("A", "Z"));
		assertEquals("ZZ", getMessage("AA", "ZZ"));
		assertEquals("IMPOSSIBLE", getMessage("AA", "AZ"));
		assertEquals("BAT", getMessage("XWP", "BAT"));
		assertEquals("JIB", getMessage("XWP", "B"));
	}
}
