package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set14;

import static org.junit.Assert.*;

import org.junit.Test;
import static com.moodaye.playground.programmerSchool.reviewJanFeb2018.set14.TwoEn.*;
public class TwoEnTest {

	@Test
	public void test() {
		long twoP = 1;
		for(int i = 1; twoP < Long.MAX_VALUE / 2; i++){
			assertEquals(twoP, (long) Long.valueOf(arrayToString(twoExpN(i - 1))));
			twoP *= 2;
		}	
		assertEquals("4722366482869645213696", arrayToString(twoExpN(72)));
	}
	
	//@Test 
	public void testMultiply(){
		assertEquals( 12 * 9, (int) Integer.valueOf(arrayToString((multiply(12, 9)))));
		for(int i = 1; i < Integer.MAX_VALUE / 10; i++){
			for(int x = 1; x <= 9; x++){
				assertEquals( i * x, (int) Integer.valueOf(arrayToString((multiply(i, x)))));
			}
		}
	}
}
