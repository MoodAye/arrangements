package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set14;

import java.util.Locale;
import java.io.InputStream;
import java.io.PrintWriter;

/** Programmer's School #41 */

/* Failing on test 11 with tle.
 * with up to 10e6 inputs ... we need to use fast scanner
 */
public class CountingSort {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int[] counter = new int[201];
		for(int i = 0; i < n; i++){
			counter[in.nextInt() + 100]++;
		}
		for(int i = 0; i < 201; i++){
			while(counter[i] > 0){
				out.printf("%d ", i - 100);
				counter[i]--;
			}
		}
	}

	public static void main(String args[]) {
		new CountingSort().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
	static class Scanner implements AutoCloseable {

	    InputStream is;
	    byte buffer[] = new byte[1 << 16];
	    int size = 0;
	    int pos = 0;

	    Scanner(InputStream is) {
	        this.is = is;
	    }

	    int nextChar() {
	        if (pos >= size) {
	            try {
	                size = is.read(buffer);
	            } catch (java.io.IOException e) {
	                throw new java.io.IOError(e);
	            }
	            pos = 0;
	            if (size == -1) {
	                return -1;
	            }
	        }
	        Assert.check(pos < size);
	        int c = buffer[pos] & 0xFF;
	        pos++;
	        return c;
	    }

	    int nextInt() {
	        int c = nextChar();
	        while (c == ' ' || c == '\r' || c == '\n' || c == '\t') {
	            c = nextChar();
	        }
	        if (c == '-') {
	            c = nextChar();
	            Assert.check('0' <= c && c <= '9');
	            int n = -(c - '0');
	            c = nextChar();
	            while ('0' <= c && c <= '9') {
	                int d = c - '0';
	                c = nextChar();
	                Assert.check(n > Integer.MIN_VALUE / 10 || n == Integer.MIN_VALUE / 10 && d <= -(Integer.MIN_VALUE % 10));
	                n = n * 10 - d;
	            }
	            return n;
	        } else {
	            Assert.check('0' <= c && c <= '9');
	            int n = c - '0';
	            c = nextChar();
	            while ('0' <= c && c <= '9') {
	                int d = c - '0';
	                c = nextChar();
	                Assert.check(n < Integer.MAX_VALUE / 10 || n == Integer.MAX_VALUE / 10 && d <= Integer.MAX_VALUE % 10);
	                n = n * 10 + d;
	            }
	            return n;
	        }
	    }

	    @Override
	    public void close() {
	    }
	}

	static class Assert {
	    static void check(boolean e) {
	        if (!e) {
	            throw new AssertionError();
	        }
	    }
	}
}
