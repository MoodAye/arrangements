package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set27;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #6 */
/* 1:00pm - 1:15pm */
public class Chess {
	void solve(Scanner in, PrintWriter out) {
		String input = in.next();
		char[] chin = input.toCharArray();
		
		if(input.length() != 5 ||
				'H' < chin[0] || chin[0] < 'A' || 
				'H' < chin[3]  || chin[3] < 'A' ||
				'8' < chin[1] || chin[1] < '1' ||
				'8' < chin[4] || chin[4] < '1' ||
				'-' != chin[2]){
			out.println("ERROR");
			return;
		}
		int dx = chin[0] - chin[3];
		int dy = chin[1] - chin[4];
		if(dx * dx + dy * dy == 5){
			out.println("YES");
		}
		else{
			out.println("NO");
		}
	}

	public static void main(String args[]) {
		new Chess().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
