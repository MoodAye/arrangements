package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set27;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #665 */
public class PalindromicTime {
	void solve(Scanner in, PrintWriter out) {
		String startTime = in.next();
		int hr = Integer.valueOf(startTime.substring(0, 2));
		int min = Integer.valueOf(startTime.substring(3));
		while (true) {
			min++;
			if (min == 60) {
				min = 0;
				hr++;
				if (hr == 24) {
					hr = 0;
				}
			}
			if(palindrome(hr, min)){
				out.printf("%02d:%02d", hr, min);
				return;
			}
		}
	}
	
	boolean palindrome(int hr, int min){
		return (hr / 10 == min % 10 && hr % 10 == min / 10);
	}
	

	public static void main(String args[]) {
		new PalindromicTime().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
