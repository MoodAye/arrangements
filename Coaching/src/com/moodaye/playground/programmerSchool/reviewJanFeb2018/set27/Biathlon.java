package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set27;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #440 */
/* Start Time = 1:17pm End Time = 1:28pm Time Taken = 11 minutes */
public class Biathlon {
	void solve(Scanner in, PrintWriter out) {
		int[] x = new int[5];
		int[] y = new int[5];
		for (int i = 0; i < 5; i++) {
			x[i] = in.nextInt();
			y[i] = in.nextInt();
		}
		int cnt = 0;
		for (int target = 0; target < 5; target++) {
			for (int shot = 0; shot < 5; shot++) {
				if ((x[shot] - target * 25) * (x[shot] - target * 25) + y[shot] * y[shot] <= 10 * 10) {
					cnt++;
					break;
				}
			}
		}
		out.println(cnt);
	}

	public static void main(String args[]) {
		new Biathlon().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
