package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set24;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #697 */
/* Start 10:39pm End 10:44pm*/
public class Renovation {
	void solve(Scanner in, PrintWriter out) {
		int l = in.nextInt();
		int w = in.nextInt();
		int h = in.nextInt();
		out.println((2 * h * (l + w) + 16 - 1) / 16);
	}

	public static void main(String args[]) {
		new Renovation().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
