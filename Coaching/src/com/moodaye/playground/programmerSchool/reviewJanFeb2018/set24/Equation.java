package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set24;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #10 */
/* Start 10:46pm  End 10:56pm*/
public class Equation {
	void solve(Scanner in, PrintWriter out) {
		long a = in.nextInt();
		long b = in.nextInt();
		long c = in.nextInt();
		long d = in.nextInt();
		for(long x = -100; x <= 100; x++){
			if(a * x * x * x + b * x * x + c * x + d == 0){
				out.print(x + " ");
			}
		}
	}

	public static void main(String args[]) {
		new Equation().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
