package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set24;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #66 */
/* Start 10:32pm End 10:37pm*/
public class Keyboard {
	void solve(Scanner in, PrintWriter out) {
		String kb = "qwertyuiopasdfghjklzxcvbnmq";
		String c = in.next();
		out.println(kb.charAt(kb.indexOf(c) + 1));
	}

	public static void main(String args[]) {
		new Keyboard().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
