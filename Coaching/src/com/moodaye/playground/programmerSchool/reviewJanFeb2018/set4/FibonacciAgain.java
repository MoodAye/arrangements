package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set4;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #623 Start = 8:57am End = 9:13*/
public class FibonacciAgain {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();

		int fibi = 1;
		int fibim1 = 1;
		int fibim2;
		if (n < 2) {
			out.println(1);
		} else {
			for (int i = 2; i <= n; i++) {
				fibim2 = fibim1;
				fibim1 = fibi % 10;
				fibi = (fibim1 + fibim2) % 10;
			}
			out.println(fibi % 10);
		}
	}

	public static void main(String args[]) {
		new FibonacciAgain().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
