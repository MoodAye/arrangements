package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set4;

import java.util.Scanner;
import java.util.Arrays;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #119 */
public class TimeSorting {
	void solve(Scanner in, PrintWriter out){
		int n = in.nextInt();
		int[] times = new int[n];
		for(int i = 0; i < n; i++){
			times[i] = in.nextInt() * 60 * 60 + in.nextInt() * 60 + in.nextInt();
		}
		Arrays.sort(times);
		for(int time : times){
			int hr = time / (60 * 60);
			int min = (time % (60 * 60)) / 60;
			int sec = (time % (60 * 60)) % 60;
			out.println( hr + " " + min + " " + sec);
		}
	}

	public static void main(String args[]) {
		new TimeSorting().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}