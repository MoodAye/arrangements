package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set4;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #98 Start = 11:21pm End = */
public class NumbersGame {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int[] numbers = new int[n];
		for(int i = 0; i < n; i++){
			numbers[i] = in.nextInt();
		}
		int left = 0;
		int right = n - 1;
		int p1Score = 0;
		int p2Score = 0;
		boolean p1Next = true;
	
		while(left <= right){
			int next = 0;
			if(numbers[right] > numbers[left]){
				next += numbers[right--];
			}
			else{
				next += numbers[left++];
			}
			if(p1Next){
				p1Score += next;
			}
			else{
				p2Score += next;
			}
			p1Next = !p1Next;
		}
		out.println(p1Score + ":" + p2Score);
	}

	public static void main(String args[]) {
		new NumbersGame().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
