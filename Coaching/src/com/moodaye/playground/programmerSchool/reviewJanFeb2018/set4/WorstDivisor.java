package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set4;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #48 Start = 11:55am End = 12:03*/
public class WorstDivisor {
	void solve(Scanner in, PrintWriter out) {
		String n = in.next();
		out.print("1");
		for(int i = n.length() - 1; i >= 0; i--){
			if (n.charAt(i) == '0'){
				out.print('0');
			}
			else{
				break;
			}
		}
	}

	public static void main(String args[]) {
		new WorstDivisor().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
