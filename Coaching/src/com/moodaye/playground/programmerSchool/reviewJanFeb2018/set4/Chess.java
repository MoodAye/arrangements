package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set4;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #6 Start = 10:41am End = 10:56am*/
public class Chess {
	void solve(Scanner in, PrintWriter out) {
		String move = in.next();
		String valid = "NO";
		if(move.length() != 5 || 
				move.charAt(0) < 'A' || move.charAt(3) < 'A' || 
				move.charAt(0) > 'H' || move.charAt(3) > 'H' || 
				move.charAt(1) < '1' || move.charAt(4) < '1' || 
				move.charAt(1) > '8' || move.charAt(4) > '8' || 
				move.charAt(2) != '-'){
			valid = "ERROR";
		}
		int dx = move.charAt(0) - move.charAt(3);
		int dy = move.charAt(1) - move.charAt(4);
		if(dx * dx + dy * dy == 5){
			valid = "YES";
		}
		out.println(valid);
	}

	public static void main(String args[]) {
		new Chess().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}

}
