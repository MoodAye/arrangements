package com.moodaye.playground.programmerSchool.reviewJanFeb2018;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #511 Start = 9:40pm End = 9:54pm */
public class Queue {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int minutes = 0;
		int hours = 0;
		minutes = (n - 1) * 5;
		hours = minutes / 60;
		minutes %= 60;
		if(hours > 12 || (hours == 12 && minutes > 0)){
			out.println("NO");
		}
		else{
			out.println(hours + " " + minutes);
		}
	}

	public static void main(String args[]) {
		new Queue().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}

}
