package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set35;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #91 */
// Start Time = 10:46pm  End = 11:38pm Time Taken = 52min.  
// Solution accepted on first attempt.
public class TwoSequences {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int[] a = new int[n < 10 ? 11 : n + 1];
		int[] b = new int[n < 10 ? 11 : n + 1];
		a[1] = 2;
		a[2] = 3;
		a[3] = 4;
		a[4] = 7;
		a[5] = 13;

		b[1] = 1;
		b[2] = 5;
		b[3] = 6;
		b[4] = 8;
		b[5] = 9;
		b[6] = 10;
		b[7] = 11;
		b[8] = 12;

		int ai = 5;
		int bi = 8;
		while (ai < n) {
			ai++;
			a[ai] = b[ai - 1] + b[ai - 3];
			if (bi == n) {
				continue;
			} else {
				bi++;
				b[bi] = a[ai - 1] + 1;
				while (b[bi] != a[ai] - 1 && bi < n) {
					bi++;
					b[bi] = b[bi - 1] + 1;
				}
			}
		}
		out.println(a[n]);
		out.println(b[n]);
	}

	public static void main(String args[]) {
		new TwoSequences().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
