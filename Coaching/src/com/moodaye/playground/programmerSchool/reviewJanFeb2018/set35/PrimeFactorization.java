package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set35;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #354 */
// Start Time = 8:27pm   Pause - 8:46pm Continue 11:42pm
//
// 2_147_483_646 is the max n value. Since we can do ~10^8 operations in 1 sec - we wont have enough time.
// but we are only going until sqrt(n) ... therefore we should be ok here??
// boolean = 1 byte .. therefore size of array is 2_147_483_646 or 2147MB.  Therefore 
// cannot use seive approach.
//
// Wrong answer on test 5. (Forgot <= - ok now)
// Getting TLE failure on test 15.
public class PrimeFactorization {
	void solve(Scanner in, PrintWriter out) {
		long n = in.nextLong();
		StringBuilder sb = new StringBuilder();
		int cnt = 0;
		int icnt = 0;
		for(long i = 2; (i * i) <= n; i++){
			cnt++;
			while (n % i == 0){
				icnt++;
				sb.append(i + "*");
				n /= i;
			}
		}
		if(n != 1){
			sb.append(n + "*");
		}
		out.println(sb.substring(0, sb.length() - 1));
		out.println(cnt);
		out.println(icnt);
	}

	public static void main(String args[]) {
		new PrimeFactorization().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
