package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set19;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #283 */
public class RuneWords {
	void solve(Scanner in, PrintWriter out) {
		String word = in.next();
		boolean firstChar = true;
		int cnt = 1;
		for (char c : word.toCharArray()) {
			if((c - 'A') <= ('Z' - 'A')){
				if((!firstChar && cnt == 1) || cnt > 4){
					break;
				}
				else{
					firstChar = false;
					cnt = 1;
				}
			}
			else if(firstChar){
				out.println("No");
				return;
			}
			else{
				cnt++;
			}
		}
		out.println((1 < cnt && cnt < 5) ? "Yes" : "No");
	}

	public static void main(String args[]) {
		new RuneWords().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
