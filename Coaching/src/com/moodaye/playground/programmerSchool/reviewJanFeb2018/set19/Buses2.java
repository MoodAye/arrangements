package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set19;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #667 */
public class Buses2 {
	void solve(Scanner in, PrintWriter out) {
		int c = in.nextInt();
		int a = in.nextInt();
		int b = in.nextInt();
		
		if(b <= 2){
			out.println(0);
			return;
		}
	
		int cBuses = (c + (b - 2 - 1)) / (b - 2);
		int aNeeded = cBuses * 2;
		if(aNeeded > a){
			out.println(0);
			return;
		}
		
		int spaceLeft = (cBuses * b) - c - aNeeded;
		int aLeft = Math.max(0, a - aNeeded - spaceLeft);
		int aBuses = (aLeft + b - 1) / b;
		out.println(aBuses + cBuses);
	}

	public static void main(String args[]) {
		new Buses2().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
