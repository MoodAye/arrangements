package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set19;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #677 */
public class NumberContestParticipants {
	void solve(Scanner in, PrintWriter out) {
		int k = in.nextInt();
		int n = in.nextInt();
		int m = in.nextInt();
		int d = in.nextInt();
	
		int knm = k * n * m;
		if(knm <= k*n + n*m + m*k){
			out.println(-1);
			return;
		}
		int diff = knm - k*n - n*m - m*k;
		int ans = d * knm / diff;
		if((d * knm) % diff != 0 || 
				ans % k != 0 || 
				ans % n != 0 || 
				ans % m != 0){
			out.println(-1);
		}
		else{
			out.println(ans);
		}
	}

	public static void main(String args[]) {
		new NumberContestParticipants().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
