package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set15;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #398 */
public class Sum2 {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int cnt = 0;
		for (int i = 1; i <= n / 4; i++) {
			for (int j = i; j <= (n - i) / 3; j++) {
				cnt += (n - i - j) / 2  - j + 1;
			}
		}
		out.println(cnt);
	}

	public static void main(String args[]) {
		new Sum2().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}

}
