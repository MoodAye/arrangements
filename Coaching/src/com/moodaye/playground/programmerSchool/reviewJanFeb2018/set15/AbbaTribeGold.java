package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set15;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #7 */
public class AbbaTribeGold {
	void solve(Scanner in, PrintWriter out) {
		String s1 = in.next();
		String s2 = in.next();
		String s3 = in.next();
	
		out.println(more(more(s1, s2), s3));
	}

	public static String more(String s1, String s2) {
		if (s1.length() > s2.length()) {
			return s1;
		} else if (s2.length() > s1.length()) {
			return s2;
		} 
		return s1.compareTo(s2) >= 0 ? s1 : s2;
	}

	public static void main(String args[]) {
		new AbbaTribeGold().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}

}
