package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set15;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #70 */
public class StringExponentiation {
	void solve(Scanner in, PrintWriter out) {
		String s = in.next();
		int k = in.nextInt();
		if(k > 0){
			StringBuffer sb = new StringBuffer();
			for(int i = 0; i < k; i++){
				sb.append(s);
				if(sb.length() > 1023){
					break;
				}
			}
			if(sb.length() > 1023){
				out.println(sb.substring(0,1023));
			}
			else{
				out.println(sb);
			}
			return;
		}
		else{
			k *= -1;
			if(s.length() % k != 0){
				out.println("NO SOLUTION");
				return;
			}
			int len = s.length() / k;
			for(int i = len; i < s.length(); i++){
				if(s.charAt(i) != s.charAt(i - len)){
					out.println("NO SOLUTION");
					return;
				}
			}
			out.println(s.substring(0,len));
		}
	}

	public static void main(String args[]) {
		new StringExponentiation().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
