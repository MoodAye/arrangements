package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set15;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #208 */
public class FunnyGame {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		if(n == 0){
			out.println(0);
			return;
		}
		// to start - we find the most significant bit.
		int mask = 1 << 29;
		int firstBit = 30;
		while ((mask & n) == 0) {
			mask >>= 1;
			firstBit--;
		}
		int max = n;
		// then loop the bits right to left - wrapping the lastBit
		for (int i = firstBit; i > 0; i--) {
			boolean lastBitSet = (n & 1) == 1 ? true : false;
			n >>= 1;
			if(lastBitSet){
				n |= 1 << (firstBit - 1);
			}
			max = Math.max(max, n);
		}
		out.println(max);
	}
	
	/* utility to help with visualing bits (positive integer) */
	//not used for solution - but for thinking about it.
	public static String printBits(int n) {
		int mask = 1 << 28;
		int spacer = 1;
		StringBuilder sb = new StringBuilder();
		while (mask >= 1) {
			if ((n & mask) == 0) {
				sb.append("0");
			} else {
				sb.append("1");
			}
			if (spacer == 4) {
				sb.append(" ");
				spacer = 0;
			}
			spacer++;
			mask >>= 1;
		}
		return sb.toString();
	}

	public static void main(String args[]) {
		new FunnyGame().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
