package com.moodaye.playground.programmerSchool.reviewJanFeb2018;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #26 Start = 6:47am End = 7:03*/
/*  Max value of distance squared (x1-x2)^2 + (y1-y2)^2 = 2 * 10_000 ^ 2 = 2e8.  (integers ok)
 *  Complexity = O(1)
 *  Storage = O(1)
 */
public class TwoCircles {
	void solve(Scanner in, PrintWriter out) {
		int c1x = in.nextInt();
		int c1y = in.nextInt();
		int c1r = in.nextInt();
		int c2x = in.nextInt();
		int c2y = in.nextInt();
		int c2r = in.nextInt();
		
		int sqDist = (c1x - c2x) * (c1x - c2x) + (c1y - c2y) * (c1y - c2y);
		int sqRsum = (c1r + c2r) * (c1r + c2r);
		int sqRminus = (c1r - c2r) * (c1r - c2r);
		
		if(sqRminus <= sqDist && sqDist <= sqRsum){
			out.println("YES");
		}
		else{
			out.println("NO");
		}
	}

	public static void main(String args[]) {
		new TwoCircles().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
