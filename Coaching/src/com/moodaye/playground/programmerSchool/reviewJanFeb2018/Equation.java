package com.moodaye.playground.programmerSchool.reviewJanFeb2018;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/**
 * Programmer's School #10 Took about 30min Thought using 'int' would be fine.
 */
public class Equation {
	void solve(Scanner in, PrintWriter out) {
		long a = in.nextLong();
		long b = in.nextLong();
		long c = in.nextLong();
		long d = in.nextLong();
		for (int x = -100; x <= 100; x++) {
			if (a * x * x * x + b * x * x + c * x + d == 0) {
				out.print(x + " ");
			}
		}
	}

	public static void main(String args[]) {
		new Equation().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
