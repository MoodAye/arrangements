package com.moodaye.playground.programmerSchool.reviewJanFeb2018;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #296 Start = 8:33pm End 8:41*/
public class FoxAndCat {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int cat = n / 5;
		int fox = (n - cat * 5) / 3;
		while( cat * 5 + fox * 3 != n){
			cat--;
			fox = (n - cat * 5) / 3;
		}
		out.println(cat + " " + fox);
	}

	public static void main(String args[]) {
		new FoxAndCat().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
