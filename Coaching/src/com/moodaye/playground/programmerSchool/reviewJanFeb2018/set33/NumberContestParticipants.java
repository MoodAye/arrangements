package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set33;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #677 */
// Start Time = 5:58pm  Pause 6:15pm 
// Continue 10:30pm WA on test 11
// Finish 11:18pm ... ~65min
public class NumberContestParticipants {
	void solve(Scanner in, PrintWriter out) {
		int a = in.nextInt();
		int b = in.nextInt();
		int c = in.nextInt();
		int d = in.nextInt();
		
		int numerator = a * b * c * d; 
		int denominator = a * b * c - a * b - b * c - c * a;
		if(denominator <= 0 || numerator % denominator != 0){
			out.println(-1);
			return;
		}
		
		int participants = numerator / denominator;
		if(participants % a != 0 || participants % b != 0 || participants % c != 0){
			out.println(-1);
		}
		else{
			out.println(participants);
		}
	}

	public static void main(String args[]) {
		new NumberContestParticipants().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
