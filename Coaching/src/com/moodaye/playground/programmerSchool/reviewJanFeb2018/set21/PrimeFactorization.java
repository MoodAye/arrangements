package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set21;

import java.util.Scanner;
import java.util.Arrays;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #354 */
public class PrimeFactorization {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		boolean[] primes = seive((int) Math.sqrt(n));
		StringBuilder sb = new StringBuilder();
		for (int i = 2; i < primes.length; i++) {
			while (n % i == 0) {
				sb.append(i + "*");
				n /= i;
			}
		}
		if (n != 1) {
			sb.append(n + "*");
		}
		out.println(sb.substring(0, sb.length() - 1));
	}

	public static boolean[] seive(int n) {
		boolean[] isPrime = new boolean[n + 1];
		Arrays.fill(isPrime, true);
		isPrime[0] = false;
		isPrime[1] = false;

		for (int i = 2; i * i <= n; i++) {
			if (isPrime[i]) {
				markNonPrimes(isPrime, i);
			}
		}
		return isPrime;
	}

	private static void markNonPrimes(boolean[] isPrime, int p) {
		for (int i = p * p; i < isPrime.length; i += p) {
			isPrime[i] = false;
		}
	}

	public static void main(String args[]) {
		new PrimeFactorization().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
