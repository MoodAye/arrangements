package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set21;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #513 */
public class Salads {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		out.printf("%d%n", (1 << n) - 1 - n);
	}

	public static void main(String args[]) {
		new Salads().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
