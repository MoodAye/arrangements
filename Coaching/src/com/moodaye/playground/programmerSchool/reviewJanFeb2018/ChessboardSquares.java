package com.moodaye.playground.programmerSchool.reviewJanFeb2018;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #62 Start = 10:05am End = 10:12am */
/*
 * Complexity = O(1) Storage = O(1)
 */
public class ChessboardSquares {
	void solve(Scanner in, PrintWriter out) {
		String sq = in.next();
		int sum = sq.charAt(0) + sq.charAt(1);
		out.println(sum % 2 == 0 ? "BLACK" : "WHITE");
	}

	public static void main(String args[]) {
		new ChessboardSquares().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
