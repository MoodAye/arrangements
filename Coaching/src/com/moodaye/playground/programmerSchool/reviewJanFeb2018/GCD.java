package com.moodaye.playground.programmerSchool.reviewJanFeb2018;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #148 Start = 10:13am End = 10:24am*/
/* Complexity = O(1)
 * Storage = O(1) 
 */
public class GCD {
	void solve(Scanner in, PrintWriter out) {
		int a = in.nextInt();
		int b = in.nextInt();
		out.println(gcd(a, b));
	}
	
	public static int gcd(int a, int b){
		while(b != 0){
			int rem = a % b; 
			a = b;
			b = rem;
		}
		return a;
	}

	public static void main(String args[]) {
		new GCD().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
