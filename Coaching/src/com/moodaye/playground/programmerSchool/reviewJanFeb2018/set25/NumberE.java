package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set25;

import java.util.Scanner;
import java.io.PrintWriter;
import java.util.Locale;

/** Programmer's School #46 */
/* Start Time = 11:26 End = 11:42 Time taken = 16min */
public class NumberE {
	void solve(Scanner in, PrintWriter out) {
		String e = "2.7182818284590452353602875";
		char[] ce = e.toCharArray();
		int n = in.nextInt();
		
		if(n == 0){
			out.println("3");
			return;
		}
		else if(n == 25){
			out.println(e);
			return;
		}
		else{
			n += 1; // for decimal point
		}
		
		if(ce[n + 1] >= '5'){
			ce[n]++;
		}
		
		out.println(e.substring(0, n) + ce[n]);
	}

	public static void main(String[] args) {
		new NumberE().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}

}
