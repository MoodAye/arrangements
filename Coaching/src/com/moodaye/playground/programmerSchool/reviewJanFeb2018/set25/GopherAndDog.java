package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set25;

import java.util.Scanner;
import java.io.PrintWriter;
import java.util.Locale;

/** Programmer's School #688 */
/* Start Time = 9:41am  pause 9:53 continue 10:00am End 10:07 Total Time = 19 minutes.
 * Failed test 13. Fixed  by using long data type */
public class GopherAndDog {
	void solve(Scanner in, PrintWriter out) {
		long gx = in.nextLong();
		long gy = in.nextLong();
		long dx = in.nextLong();
		long dy = in.nextLong();
		int n = in.nextInt();
		for(int i = 0; i < n; i++){
			long bx = in.nextLong();
			long by = in.nextLong();
			if( 4 * ((gx - bx) * (gx - bx) + (gy - by) * (gy - by)) 
				   <= (dx - bx) * (dx - bx) + (dy - by) * (dy - by)){
				out.println(i + 1);
				return;
			}
		}
		out.println("NO");
	}

	public static void main(String[] args) {
		new GopherAndDog().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}

}
