package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set25;

import java.util.Scanner;
import java.io.PrintWriter;
import java.util.Locale;

/** Programmer's School #511 */
/* Start time = 10:47am End time = 11:03am Time taken = 16 min*/
public class Queue {
	void solve(Scanner in, PrintWriter out) {
		int position = in.nextInt();
		
		if(position > 12 * 60 / 5 + 1){
			out.println("NO");
		}
		else {
			int waitTime = (position - 1) * 5;
			out.printf("%d %d%n", waitTime / 60, waitTime % 60);
		}
	}
	
	public static void main(String[] args) {
		new Queue().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}

}
