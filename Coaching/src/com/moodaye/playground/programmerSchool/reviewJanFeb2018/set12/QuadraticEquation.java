package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set12;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #411 */
public class QuadraticEquation {
	void solve(Scanner in, PrintWriter out) {
		double a = in.nextDouble();
		double b = in.nextDouble();
		double c = in.nextDouble();
		// this condition required for test 26 (infinite roots)
		if (a == 0 && b == 0 && c == 0) {
			out.print(-1);
			//This condition required for test 27 (never true)
		} else if (a == 0 && b == 0) {
			out.print(0);
			//Infinite roots
		} else if (a == 0) {
			out.println(1);
			out.printf("%.6f", -c / b);
		} else if (b * b - 4 * a * c == 0) {
			out.println(1);
			out.printf("%.6f", -b / (2 * a));
			// required for test 31 (no real roots)
		} else if (b * b - 4 * a * c < 0) {
			out.println(0);
		} else {
			out.println(2);
			double sqrt = Math.sqrt(b * b - 4 * a * c);
			double root1 = (-b + sqrt) / (2 * a);
			double root2 = (-b - sqrt) / (2 * a);
			out.printf("%.6f%n%.6f", root1, root2);
		}
	}

	public static void main(String args[]) {
		new QuadraticEquation().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
