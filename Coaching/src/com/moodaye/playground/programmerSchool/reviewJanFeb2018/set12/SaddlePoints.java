package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set12;

import java.util.Arrays;
import java.util.Locale;
import java.util.Optional;
import java.util.OptionalInt;
import java.io.InputStream;
import java.io.PrintWriter;

/** Programmer's School #388 */
// Max inputs = 750 x 750 > 400,000 inputs handled by Scanner per second.
// Need fast scanner
// Performance Complexity: O(n^2) - therefore ok.
// Space Complexity: O(n^2) specifically - array of array of ints + array of array of boolean 
// or 400,000 * 32 (ints) + 400,000 * 8 (boolean) = 16_000_000 bytes --- might be ok.
public class SaddlePoints {
	void solve(Scanner in, PrintWriter out) {
		int rows = in.nextInt();
		int cols = in.nextInt();
		int[][] mat = new int[rows][cols];
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				mat[i][j] = in.nextInt();
			}
		}
	
		// check if cells are max for their columns
		boolean[][] isMaxColCell = new boolean[rows][cols];
		for(int i = 0; i < cols; i++){
			int max = Integer.MIN_VALUE;
			for(int j = 0; j < rows; j++){
				max = Math.max(max, mat[j][i]);
			}
			for(int j = 0; j < rows; j++){
				if(mat[j][i] == max){
					isMaxColCell[j][i] = true;
				}
			}
		}
		
		int cnt = 0;
		
		// for each row 
		//    determine min.
		//    then scan row cells - if min - then check if max for column.
		for(int r = 0; r < rows; r++){
			int min = Arrays.stream(mat[r]).min().getAsInt();
			for(int i = 0; i < cols; i++){
				if (mat[r][i] == min){
					if(isMaxColCell[r][i]){
						cnt++;
					}
				}
			}
					
		}
		out.println(cnt);
	}
	

	public static void main(String args[]) {
		new SaddlePoints().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}

	static class Scanner implements AutoCloseable {

		InputStream is;
		byte buffer[] = new byte[1 << 16];
		int size = 0;
		int pos = 0;

		Scanner(InputStream is) {
			this.is = is;
		}

		int nextChar() {
			if (pos >= size) {
				try {
					size = is.read(buffer);
				} catch (java.io.IOException e) {
					throw new java.io.IOError(e);
				}
				pos = 0;
				if (size == -1) {
					return -1;
				}
			}
			Assert.check(pos < size);
			int c = buffer[pos] & 0xFF;
			pos++;
			return c;
		}

		int nextInt() {
			int c = nextChar();
			while (c == ' ' || c == '\r' || c == '\n' || c == '\t') {
				c = nextChar();
			}
			if (c == '-') {
				c = nextChar();
				Assert.check('0' <= c && c <= '9');
				int n = -(c - '0');
				c = nextChar();
				while ('0' <= c && c <= '9') {
					int d = c - '0';
					c = nextChar();
					Assert.check(n > Integer.MIN_VALUE / 10
							|| n == Integer.MIN_VALUE / 10 && d <= -(Integer.MIN_VALUE % 10));
					n = n * 10 - d;
				}
				return n;
			} else {
				Assert.check('0' <= c && c <= '9');
				int n = c - '0';
				c = nextChar();
				while ('0' <= c && c <= '9') {
					int d = c - '0';
					c = nextChar();
					Assert.check(
							n < Integer.MAX_VALUE / 10 || n == Integer.MAX_VALUE / 10 && d <= Integer.MAX_VALUE % 10);
					n = n * 10 + d;
				}
				return n;
			}
		}

		@Override
		public void close() {
		}
	}

	static class Assert {
		static void check(boolean e) {
			if (!e) {
				throw new AssertionError();
			}
		}
	}
}
