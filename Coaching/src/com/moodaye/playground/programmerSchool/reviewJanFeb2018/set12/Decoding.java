package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set12;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #73 */
public class Decoding {
	void solve(Scanner in, PrintWriter out) {
		String code = in.next();
		char[] decode = decode(code);
		out.println(String.valueOf(decode));
	}

	public static char[] decode(String code) {
		char[] decode = new char[code.length()];
		for (int i = 0; i < code.length(); i++) {
			int codeNumber;
			if (code.charAt(i) >= 'A') {
				codeNumber = 10 + (code.charAt(i) - 'A');
			} else {
				codeNumber = 0 + (code.charAt(i) - '0');
			}

			int decodeNumber = ((codeNumber + 27 * 10) - (i + 1)) % 27;
			if (decodeNumber == 0) {
				decode[i] = ' ';
			} else {
				decode[i] = (char) ('a' + decodeNumber - 1);
			}
		}
		return decode;
	}

	public static void main(String args[]) {
		new Decoding().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
