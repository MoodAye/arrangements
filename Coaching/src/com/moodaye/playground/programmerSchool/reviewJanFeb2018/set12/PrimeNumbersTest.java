package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set12;

import static org.junit.Assert.*;

import org.junit.Test;
import static com.moodaye.playground.programmerSchool.reviewJanFeb2018.set12.PrimeNumbers.*;
public class PrimeNumbersTest {

	@Test
	public void test() {
		boolean[] isPrime = new boolean[20];
		isPrime[2] = true;
		isPrime[3] = true;
		isPrime[5] = true;
		isPrime[7] = true;
		isPrime[11] = true;
		isPrime[13] = true;
		isPrime[17] = true;
		isPrime[19] = true;
		assertArrayEquals(isPrime, primeNumbers(19));
	}

}
