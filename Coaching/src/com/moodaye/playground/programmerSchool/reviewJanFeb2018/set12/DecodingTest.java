package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set12;

import static org.junit.Assert.*;

import org.junit.Test;
import static com.moodaye.playground.programmerSchool.reviewJanFeb2018.set12.Decoding.*;
public class DecodingTest {

	@Test
	public void test() {
		assertEquals("test", String.valueOf(decode("L7MO")));
		assertEquals("decoding", String.valueOf(decode("576J9FLF")));
		assertEquals(" ecoding", String.valueOf(decode("176J9FLF")));
		
	}

}
