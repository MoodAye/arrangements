package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set12;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #680 */
/* Formula is 3 x 2^(n-1).  Max n = 50.
 * Max combos = 3 x 2^49 .... therefore we need long
 */
public class GardnerArtist {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		double combos = 3 * Math.pow(2, n - 1);
		out.printf("%.0f", combos);
	}

	public static void main(String args[]) {
		new GardnerArtist().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
