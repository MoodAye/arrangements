package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set12;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #482 */
public class ShortSequence {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		StringBuffer sb = new StringBuffer();
		int counter = 1;
		int stop = 2;
		while(sb.length() < n){
			if(counter == stop){
				counter = 1;
				stop++;
				continue;
			}
			sb.append(counter++);
		}
		out.println(sb.charAt(n - 1));
	}

	public static void main(String args[]) {
		new ShortSequence().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
