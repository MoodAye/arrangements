package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set12;

import java.util.Scanner;
import java.util.Arrays;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #349 */
public class PrimeNumbers {
	void solve(Scanner in, PrintWriter out) {
		int first = in.nextInt();
		int last = in.nextInt();
		boolean[] isPrime = primeNumbers(last);
		
		boolean absent = true;
		for(int i = first; i <= last; i++){
			if(isPrime[i]){
				out.println(i);
				absent = false;
			}
		}
		
		if(absent){
			out.println("Absent");
		}
	}
	
	public static boolean[] primeNumbers(int n){
		boolean[] isPrime = new boolean[n + 1];
		Arrays.fill(isPrime, true);
		isPrime[0] = false;
		isPrime[1] = false;
		int p = 1;
		while (p < n) {
			p++;
			if(!isPrime[p]){
				continue;
			}
			for (int i = 2 * p; i <= n;  i += p) {
				isPrime[i] = false;
			}
		}
		return isPrime;
	}

	public static int nextPrime(boolean[] isPrime, int p) {
		p++;
		while (p < isPrime.length && !isPrime[p]) {
			p++;
		}
		return p;
	}

	public static void main(String args[]) {
		new PrimeNumbers().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
