package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set9;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #520 */
public class WholesalePurchase {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		//option 1: all boxes
		int boxesOption1 = (n + 144 - 1) / 144;
		int bundlesOption1 = 0;
		int pairsOption1 = 0;
		double costOption1 = boxesOption1 * 1140;
		
		//option 2: boxes + bundle
		int boxesOption2 = boxesOption1 - 1;
		int socksLeftOver = n - boxesOption2 * 144;
		int bundlesOption2 = (socksLeftOver + 12 - 1) / 12;
		int pairsOption2 = 0;
		double costOption2 = boxesOption2 * 1140 + bundlesOption2 * 102.5;
		
		//option 3: boxes + bundles + pairs
		int boxesOption3 = boxesOption2;
		int bundlesOption3 = bundlesOption2 - 1;
		int pairsOption3 = n - boxesOption3 * 144 - bundlesOption3 * 12;
		double costOption3 = boxesOption3 * 1140 + bundlesOption3 * 102.5 + pairsOption3 * 10.5;
		
		if(costOption1 <= costOption2 && costOption1 <= costOption3){
			out.println(boxesOption1 + " " + bundlesOption1 + " " + pairsOption1);
		}
		else if(costOption2 <= costOption3 && costOption2 <= costOption1){
			out.println(boxesOption2 + " " + bundlesOption2 + " " + pairsOption2);
		}
		else{
			out.println(boxesOption3 + " " + bundlesOption3 + " " + pairsOption3);
		}
	}

	public static void main(String args[]) {
		new WholesalePurchase().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
