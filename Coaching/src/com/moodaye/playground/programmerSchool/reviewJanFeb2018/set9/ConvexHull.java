package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set9;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #84 */
public class ConvexHull {
	void solve(Scanner in, PrintWriter out) {
		int lines = in.nextInt();
		int c = in.nextInt();
		int minL = -1;
		int maxL = -1;
		int minC = Integer.MAX_VALUE;
		int maxC = -1;
		for(int i = 0; i < lines; i++){
			String line = in.next();
			int idx = line.indexOf("*");
			int lastIdx = line.lastIndexOf("*");
			if(idx != -1){
				minL = minL == -1 ? i : minL;
				maxL = i;
				minC = minC > idx ? idx : minC;
				maxC = maxC < lastIdx ? lastIdx : maxC;
			}
		}
		for(int i = 0; i < lines; i++){
			if(minL <= i && maxL >= i){
				for(int j = 0; j < c; j++){
					out.print(j < minC || j > maxC ? "." : "*");
				}
			}
			else{
				for(int j = 0; j < c; j++){
					out.print(".");
				}
			}
			out.println();
		}
	}

	public static void main(String args[]) {
		new ConvexHull().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
