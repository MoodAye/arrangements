package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set9;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #490 */
public class Birthdays {
	void solve(Scanner in, PrintWriter out) {
		String b1 = in.next();
		String b2 = in.next();
		int d1 = Integer.valueOf(b1.substring(0, 2));
		int m1 = Integer.valueOf(b1.substring(3, 5));
		int y1 = Integer.valueOf(b1.substring(6));
		int d2 = Integer.valueOf(b2.substring(0, 2));
		int m2 = Integer.valueOf(b2.substring(3, 5));
		int y2 = Integer.valueOf(b2.substring(6));
		int cnt = 0;

		while (d1 != d2 || m1 != m2 || y1 != y2) {
			d1++;
			cnt++;
			if (d1 == 29 && m1 == 2) {
				m1 = 3;
				d1 = 1;
			} 
			else if ((d1 == 31) && (m1 == 4 || m1 == 6 || m1 == 9 || m1 == 11)) {
				m1++;
				d1 = 1;
			} 
			else if (d1 == 32) {
				m1++;
				d1 = 1;
				if (m1 == 13) {
					m1 = 1;
					y1++;
				}
			}
		}
		out.println(cnt);
	}

	public static void main(String args[]) {
		new Birthdays().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
