package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set9;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #250 */
public class TwoFoldNumbers {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		boolean toggle = true;
		int i = 1;
		while (!isTwoFold(n)) {
			n = toggle ? n - i : n + i;
			i++;
			toggle = !toggle;
		}
		out.println(n);
	}

	public static boolean isTwoFold(int n){
		boolean[] a = new boolean[10];
		int cnt = 0;
		while(n > 0){
			if(!a[n % 10]){
				cnt++;
				a[n % 10] = true;
			}
			n /= 10;
		}
		return cnt <= 2;
	}

	public static void main(String args[]) {
		new TwoFoldNumbers().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
