package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set9;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #530 ~15 minutes */
// Max number of pixels per image = 100 * 100 = 10_000
// Max number of operations per second = 10e8.
// Therefore if we do the operations pixel by pixel we should
// be fine.
public class BlackAndWhiteGraphics {
	void solve(Scanner in, PrintWriter out) {
		int w = in.nextInt();
		int h = in.nextInt();
		String[] image1 = new String[h];
		String[] image2 = new String[h];

		for (int i = 0; i < h; i++) {
			image1[i] = in.next();
		}

		for (int i = 0; i < h; i++) {
			image2[i] = in.next();
		}

		char[] cops = in.next().toCharArray();
		char[][] ops = new char[2][2];
		ops[0][0] = cops[0];
		ops[0][1] = cops[1];
		ops[1][0] = cops[2];
		ops[1][1] = cops[3];

		for (int i = 0; i < h; i++) {
			out.println(doOps(image1[i], image2[i], ops));
		}
	}

	public static String doOps(String img1, String img2, char[][] ops) {
		char[] cImg1 = img1.toCharArray();
		char[] cImg2 = img2.toCharArray();
		char[] result = new char[cImg1.length];
		for (int i = 0; i < cImg1.length; i++) {
			result[i] = ops[cImg1[i] - '0'][cImg2[i] - '0'];
		}
		return String.valueOf(result);
	}

	public static void main(String args[]) {
		new BlackAndWhiteGraphics().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
