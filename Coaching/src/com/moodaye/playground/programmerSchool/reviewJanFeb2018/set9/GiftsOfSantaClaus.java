package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set9;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #317 */
public class GiftsOfSantaClaus {
	void solve(Scanner in, PrintWriter out) {
		int x = in.nextInt();
		int y = in.nextInt();
		int z = in.nextInt();
		int w = in.nextInt();
		int cnt = 0;
		for (int ix = 0; ix <= w / x; ix++) {
			for (int iy = 0; iy  <= (w - ix * x)/ y; iy++) {
				if((w - ix * x + iy * y) % z == 0){
					cnt++;
				}
			}
		}
		out.println(cnt);
	}

	public static void main(String args[]) {
		new GiftsOfSantaClaus().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
