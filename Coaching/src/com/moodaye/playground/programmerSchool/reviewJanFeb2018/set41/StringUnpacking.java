package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set41;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #231 */
// Start Time 8:30pm End Time = 9pm  Time Taken = 30min
public class StringUnpacking {
	void solve(Scanner in, PrintWriter out) {
		char[] s = in.next().toCharArray();
		int len = 1;
		for (int i = 0; i < s.length; i++) {
			if ('0' <= s[i] && s[i] <= '9') {
				int cnt = s[i] - '0';
				while ('0' <= s[i + 1] && s[i + 1] <= '9') {
					i++;
					cnt *= 10;
					cnt += s[i] - '0';
				}
				for (int j = 0; j < cnt; j++) {
					out.print(s[i + 1]);
					if (len++ % 40 == 0) {
						out.println();
					}
				}
				i++;
			} else {
				out.print(s[i]);
				if (len++ % 40 == 0) {
					out.println();
				}
			}
		}
	}

	public static void main(String args[]) {
		new StringUnpacking().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
