package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set41;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #328 */
// Start Time = 9:40pm End Time = 9:48pm
// not sure if long is needed - will try to derive formula
public class DominoDots {
	void solve(Scanner in, PrintWriter out) {
		long n = in.nextLong();
		long cnt = 0;
		for(long i = 0; i <= n; i++){
			for(long j = i; j <= n; j++){
				cnt += i + j;
			}
		}
		out.println(cnt);
	}

	public static void main(String args[]) {
		new DominoDots().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
