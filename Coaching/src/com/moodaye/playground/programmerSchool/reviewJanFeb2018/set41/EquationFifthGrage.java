package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set41;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #163 */
// Start 10:25 End 10:34 Time Taken = 9min
// f1 + f2 = f3
public class EquationFifthGrage {
	void solve(Scanner in, PrintWriter out) {
		char[] eqn = in.next().toCharArray();
			int f1 = eqn[0] - '0';
			int f2 = eqn[2] - '0';
			int f3 = eqn[4] - '0';
		
		if(f1 == 'x' - '0'){
			if(eqn[1] == '-'){
				out.println(f3 + f2);
			}
			else{
				out.println(f3 - f2);
			}
		}
		else if(f2 == 'x' - '0'){
			if(eqn[1] == '-'){
				out.println(f1 - f3);
			}
			else{
				out.println(f3 - f1);
			}
		}
		else{
			if(eqn[1] == '-'){
				out.println(f1 - f2);
			}
			else{
				out.println(f1 + f2);
			}
		}
	}

	public static void main(String args[]) {
		new EquationFifthGrage().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
