package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set41;

import java.util.Scanner;
import java.util.Arrays;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #44 */
public class Arrows {
	void solve(Scanner in, PrintWriter out) {
		char[] c = null;
		int cnt = 0;
		// Needed in case input is empty?
		if (in.hasNext()) {
			c = in.next().toCharArray();
			for (int i = 0; i + 4 < c.length; i++) {
				if (c[i] == '>' || c[i] == '<') {
					if (checkForArrow(c, i)) {
						cnt++;
					}
				}
			}
		}
		out.println(cnt);
	}

	boolean checkForArrow(char[] c, int idx) {
		if ((c[idx] == '>' && c[idx + 1] == '>' && c[idx + 2] == '-' && c[idx + 3] == '-' && c[idx + 4] == '>')
				|| (c[idx] == '<' && c[idx + 1] == '-' && c[idx + 2] == '-' && c[idx + 3] == '<' && c[idx + 4] == '<')) {
			return true;
		}
		return false;
	}

	public static void main(String args[]) {
		new Arrows().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
