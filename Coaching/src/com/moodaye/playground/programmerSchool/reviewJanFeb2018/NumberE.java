package com.moodaye.playground.programmerSchool.reviewJanFeb2018;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #46 Start = 5:41am End = 5:58am */
public class NumberE {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		String e = "2.7182818284590452353602875";
		String eRounded = "";
		if (n == 0) {
			eRounded = "3";
		} else if (n == 25) {
			eRounded = e;
		} else {
			eRounded = e.substring(0, n + 2);
			if (e.charAt(n + 2) >= '5') {
				char[] eRoundedChars = eRounded.toCharArray();
				eRoundedChars[n + 1]++;
				eRounded = String.valueOf(eRoundedChars);
			}
		}
		out.println(eRounded);
	}

	public static void main(String args[]) {
		new NumberE().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}

}
