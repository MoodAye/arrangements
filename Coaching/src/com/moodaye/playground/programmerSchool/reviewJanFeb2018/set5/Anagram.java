package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set5;

import java.util.Scanner;
import java.util.Arrays;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #693 Start = 8:40 End = 8:57*/
public class Anagram {
	void solve(Scanner in, PrintWriter out) {
		char[] name1 = in.next().toUpperCase().toCharArray();
		char[] name2 = in.next().toUpperCase().toCharArray();
		Arrays.sort(name1);
		Arrays.sort(name2);
		out.println(String.valueOf(name1).equals(String.valueOf(name2)) ? "Yes" : "No");
	}

	public static void main(String args[]) {
		new Anagram().run();
		
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
