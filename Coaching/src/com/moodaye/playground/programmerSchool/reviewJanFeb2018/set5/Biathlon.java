package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set5;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #440 Start = 7:07pm End = 7:35pm*/
public class Biathlon {
	void solve(Scanner in, PrintWriter out) {
		int[][] p = new int[5][2];
		for (int i = 0; i < 5; i++) {
			p[i][0] = in.nextInt();
			p[i][1] = in.nextInt();
		}
		boolean[] hit = new boolean[5];
		for (int i = 0; i < 5; i++) {
			hit[0] = hit[0] ? hit[0] : isTargetHit(p[i][0], p[i][1], 0, 0);
			hit[1] = hit[1] ? hit[1] : isTargetHit(p[i][0], p[i][1], 25, 0);
			hit[2] = hit[2] ? hit[2] : isTargetHit(p[i][0], p[i][1], 50, 0);
			hit[3] = hit[3] ? hit[3] : isTargetHit(p[i][0], p[i][1], 75, 0);
			hit[4] = hit[4] ? hit[4] : isTargetHit(p[i][0], p[i][1], 100, 0);
		}
		int cnt = 0;
		for(boolean h : hit){
			if(h){
				cnt++;
			}
		}
		out.println(cnt);
	}

	public boolean isTargetHit(int x, int y, int tx, int ty) {
		return ((y - ty) * (y - ty) + (x - tx) * (x - tx) <= 10 * 10);
	}

	public static void main(String args[]) {
		new Biathlon().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
