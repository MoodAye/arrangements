package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set5;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #14 Start = 8:06pm End = 8:13pm */
public class Lcm {
	void solve(Scanner in, PrintWriter out) {
		int a = in.nextInt();
		int b = in.nextInt();
		int hcd = hcd(a, b);
		out.println(a / hcd * b);
	}
	
	public static int hcd(int a, int b){
		int rem = 0;
		while(b != 0){
			rem = a % b;
			a = b;
			b = rem;
		}
		return a;
	}

	public static void main(String args[]) {
		new Lcm().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
