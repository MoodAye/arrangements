package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set5;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #677 Start = 7:37pm Pause = 8:01pm End = */
/* Wrong answer on test # 11 */
public class NumberContestParticipants {
	void solve(Scanner in, PrintWriter out) {
		int k = in.nextInt();
		int n = in.nextInt();
		int m = in.nextInt();
		int d = in.nextInt();
		int num =  k * n + n * m + m * k;
		int den	=  k * n * m;
		if( num >= den  || (d * den) % (den - num) != 0){
			out.println(-1);
		}
		else{
			out.println((d * den) / (den - num));
		}
	}

	public static void main(String args[]) {
		new NumberContestParticipants().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
