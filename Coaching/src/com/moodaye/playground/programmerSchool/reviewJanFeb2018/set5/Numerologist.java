package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set5;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #95 Start = 8:25pm End = 8:33pm*/
public class Numerologist {
	void solve(Scanner in, PrintWriter out) {
		String life = in.next();
		int cnt = 0;
		while(life.length() != 1){
			cnt++;
			life = addDigits(life);
		}
		out.println(life + " " + cnt);
	}
	
	private String addDigits(String s){
		int sum = 0;
		for(char c : s.toCharArray()){
			sum += c - '0';
		}
		return "" + sum;
	}

	public static void main(String args[]) {
		new Numerologist().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
