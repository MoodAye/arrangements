package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set5;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #665 Start = ~10pm End = ~10:15pm */
public class PalindromicTime {
	void solve(Scanner in, PrintWriter out) {
		String timeIn = in.next();
		int hr = Integer.valueOf(timeIn.substring(0,2));
		int min = Integer.valueOf(timeIn.substring(3));
		while(true){
			min++;
			if(min % 60 == 0){
				min = 0;
				hr++;
				if(hr == 24){
					hr = 0;
				}
			}
			if(isPalindrome(hr, min)){
				break;
			}
		}
			out.println(paddedString(hr) + ":" + paddedString(min));
	}

	private static boolean isPalindrome(int hr, int min){
		String strHr = paddedString(hr);
		String strMin = paddedString(min);
		if(strHr.charAt(0) == strMin.charAt(1) && 
				strHr.charAt(1) == strMin.charAt(0)){
			return true;
		}
		return false;
	}
	
	private static String paddedString(int t){
		String strT = String.valueOf(t);
		return t < 10 ? "0" + strT : strT;
	}
	
	public static void main(String args[]) {
		new PalindromicTime().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
