package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set13;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #91 */
public class TwoSequences {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int size = n;
		if(n < 9){
			size = 9;
		}
		int[] a = new int[size + 1];
		int[] b = new int[size + 1];
	
		a[1] = 2;
		a[2] = 3;
		a[3] = 4;
		a[4] = 7;
		a[5] = 13;
		int ai = 6;
		
		b[1] = 1;
		b[2] = 5;
		b[3] = 6;
		b[4] = 8;
		b[5] = 9;
		b[6] = 10;
		b[7] = 11;
		b[8] = 12;
		int bi = 9;
	
		while(ai <= n){
			a[ai] = b[ai - 1] + b[ai - 3];
			bi = fillB(a, b, ai, bi);
			ai++;
		}
		
		out.println(a[n]);
		out.println(b[n]);
		
	}
	
	public static int fillB(int[] a, int[] b, int ai, int bi){
		if(bi == b.length){
			return bi;
		}
		for(int k = a[ai - 1] + 1; k < a[ai]; k++){
			b[bi] = k;
			bi++;
			if(bi == b.length){
				break;
			}
		}
		return bi;
	}

	public static void main(String args[]) {
		new TwoSequences().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
