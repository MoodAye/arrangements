package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set13;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #517 */
public class Bowling {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int[] pins = new int[n];
		for (int i = 0; i < n; i++) {
			pins[i] = in.nextInt();
		}
		int score = 0;
		int frame = 1;
		boolean nextFrame = false;
		for (int i = 0; i < n; i++) {
			if (nextFrame) {
				nextFrame = false;
				continue;
			}
			if (frame == 10) {
				score += pins[i];
				continue;
			} 
			else if (pins[i] == 10) {
				score += pins[i] + pins[i + 1] + pins[i + 2];
			} 
			else if (pins[i] + pins[i + 1] == 10) {
				score += pins[i] + pins[i + 1] + pins[i + 2];
				nextFrame = true;
			} 
			else {
				score += pins[i] + pins[i + 1];
				nextFrame = true;
			}
			frame++;
		}
		out.println(score);
	}

	public static void main(String args[]) {
		new Bowling().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
