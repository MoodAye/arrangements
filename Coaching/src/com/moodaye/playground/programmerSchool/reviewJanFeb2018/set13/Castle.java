package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set13;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;
import java.math.BigInteger;

/** Programmer's School #528 */
/* max N = 1_000_000; max K = 1_000_000
   max rooms > 10e6 * 10e6
 */
public class Castle {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int k = in.nextInt();
		
		long rooms = n;
		for(int level = 2; level <= k; level++){
		  rooms += ((0L + n - 2) * level) + 1;
		}
		out.println(rooms);
	}
	public static void main(String args[]) {
		new Castle().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
