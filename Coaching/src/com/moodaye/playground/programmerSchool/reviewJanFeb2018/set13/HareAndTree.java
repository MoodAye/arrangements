package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set13;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #551 */
public class HareAndTree {
	void solve(Scanner in, PrintWriter out) {
		int R = in.nextInt();
		int r = in.nextInt();
		int h = in.nextInt();
		int b = in.nextInt();
		
		//if r <= b then h + r - b <= R && h + r - b <= sqrt(R^2 - r^2)
		//if r > b	then h + r - b < R ... the other condition is redundant here.
		if(h + r - b > R || (h + r - b) * (h + r - b) > (R * R - r * r)){
			out.println("NO");
		}
		else{
			out.println("YES");
		}
		
	}

	public static void main(String args[]) {
		new HareAndTree().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
