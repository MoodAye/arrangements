package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set13;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #118 */
public class JosephusProblem {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int k = in.nextInt();

		// keep track of the dead people
		int cnt = 0;
		// keep track of dead people's index
		boolean[] isDead = new boolean[n];

		int i = -1;
		while (cnt < n) {
			int skip = 0;
			//find next kth person to kill
			while (skip != k) {
				i = nextAlive(isDead, i);
				skip++;
			}
			isDead[i] = true;
			cnt++;
		}

		out.println(i + 1);
	}

	public static int nextAlive(boolean[] isDead, int idx) {
		idx++;
		if (idx == isDead.length) {
			idx = 0;
		}
		while (isDead[idx]) {
			idx++;
			if (idx == isDead.length) {
				idx = 0;
			}
		}
		return idx;
	}

	public static void main(String args[]) {
		new JosephusProblem().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
