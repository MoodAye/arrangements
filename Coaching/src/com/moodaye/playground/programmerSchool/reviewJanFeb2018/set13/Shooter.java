package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set13;

import java.util.Scanner;
import java.io.PrintWriter;
import java.util.Locale;

/** Programmer's School #298 */
// targets on same path have the same slope (y/x)
public class Shooter {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int[][] ts = new int[n][2];
		for (int i = 0; i < n; i++) {
			ts[i][0] = in.nextInt();
			ts[i][1] = in.nextInt();
		}
		int shots = n;
		boolean[] inline = new boolean[n];
		for (int i = 0; i < n - 1; i++) {
			if (inline[i] == true) {
				continue;
			}
			for (int j = i + 1; j < n; j++) {
				if (inline[j] == true) {
					continue;
				}
				//same line and direction from shooter
				if (ts[i][0] * ts[j][1] == ts[i][1] * ts[j][0] && 
						ts[i][0] * ts[j][0] >= 0 && ts[i][1] * ts[j][1] >= 0) {
					inline[j] = true;
					shots--;
				}
			}
		}
		out.println(shots);
	}

	public static void main(String[] args) {
		new Shooter().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}

}
