package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set11;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #69 */
public class NGonWheel {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		double a = in.nextInt();

		double outer = (a / 2) / Math.sin(Math.PI / n);
		double inner = (a / 2) / Math.tan(Math.PI / n);
		out.println(outer - inner > 1 ? "NO" : "YES");
	}

	public static void main(String args[]) {
		new NGonWheel().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
