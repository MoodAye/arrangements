package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set11;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #491 */
// Brute force algorithm is O(n^2) therefore it won't work (max input = 10e5;
// 10e5 ^ 2 = 10e10 > 10e8 max operations / sec)
// Seems like if string is a palindrome - then substring(0,s.length() - 1) is
// not a palindrome unless
// all characters are equal.
// The proof is as follows ... is string of length s.length() - 1 is a
// palindrome ... then by adding a character at
// the end of the string - it is no longer a palindrome unless all the
// characters of the string are equal.
public class AntiPalindrome {
	void solve(Scanner in, PrintWriter out) {
		String s = in.next();
		if (allSame(s)) {
			out.println("NO SOLUTION");
			return;
		}
		if (isPalindrome(s)) {
			out.println(s.substring(0, s.length() - 1));
		} else {
			out.println(s);
		}
	}

	public static boolean isPalindrome(String s) {
		for (int i = 0, j = s.length() - 1; i < j; i++, j--) {
			if (s.charAt(i) != s.charAt(j)) {
				return false;
			}
		}
		return true;
	}

	public static boolean allSame(String s) {
		for (char c : s.toCharArray()) {
			if (c != s.charAt(0)) {
				return false;
			}
		}
		return true;
	}

	public static void main(String args[]) {
		new AntiPalindrome().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
