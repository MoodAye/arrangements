package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set11;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #126 */
public class MinimumCyclicalPath {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int[][] dist = new int[n][n];
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				dist[i][j] = in.nextInt();
			}
		}

		int minPath = Integer.MAX_VALUE;
		for (int i = 0; i < n - 2; i++) {
			for (int j = i + 1; j < n - 1; j++) {
				for (int k = j + 1; k < n; k++) {
					int path = dist[i][j];
					path += dist[j][k];
					path += dist[k][i];
					minPath = Math.min(path, minPath);
				}
			}
		}
		out.println(minPath);
	}

	public static void main(String args[]) {
		new MinimumCyclicalPath().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
