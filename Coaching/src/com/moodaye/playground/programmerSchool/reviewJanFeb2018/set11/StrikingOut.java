package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set11;

import java.util.Scanner;
import java.util.Arrays;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #273 */
/*
 * Max length input = 101 digits. To check 901 possible numbers it will take 3
 * substring operations on the input. With 1 second to solve the problem we have
 * enough time to do this.
 */
public class StrikingOut {
	void solve(Scanner in, PrintWriter out) {
		String n = in.next();
		if (n.length() < 3) {
			out.println(0);
			return;
		}
		out.println(getCountOptimal(n));
	}

	public static int getCountOptimal(String s) {
		int cnt = 0;
		for (int i = 100; i < 1000; i++) {
			int index = 0;
			for (char c : String.valueOf(i).toCharArray()) {
				index = s.indexOf(c, index);
				if (index == -1) {
					break;
				}
				index++;
			}
			if (index != -1) {
				cnt++;
			}
		}
		return cnt;
	}

	// O(n^3) approach
	public static int getCount(String s) {
		char[] cs = s.toCharArray();
		boolean[] numbers = new boolean[1000];
		for (int i = 0; i < cs.length - 2; i++) {
			if (cs[i] == '0') {
				continue;
			}
			for (int j = i + 1; j < cs.length - 1; j++) {
				for (int k = j + 1; k < cs.length; k++) {
					numbers[(cs[i] - '0') * 100 + (cs[j] - '0') * 10 + (cs[k] - '0')] = true;
				}
			}
		}
		int cnt = 0;
		for (int i = 100; i < 1000; i++) {
			if (numbers[i]) {
				cnt++;
			}
		}
		return cnt;
	}

	public static void main(String args[]) {
		new StrikingOut().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
