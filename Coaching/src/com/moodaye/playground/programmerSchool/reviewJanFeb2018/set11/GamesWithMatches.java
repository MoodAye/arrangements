package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set11;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #676 */
public class GamesWithMatches {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		//1 or 2 matches = Bachet's Game. so a count 
		//divisible by 3 is losing.
		//1001 matches wins - as you remove 2 matches.  1002 loses as you remove 1 match you lose and
		// if you remove 2 matches you lose.
		
		out.println(n % 3 == 0 ? 2 : 1);
	}

	public static void main(String args[]) {
		new GamesWithMatches().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
