package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set11;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #278 */
public class ComputationalBiology {
	void solve(Scanner in, PrintWriter out) {
		String s = in.next();
		String t = in.next();
	
		int k = 0;
		for(char c : s.toCharArray()){
			k = t.indexOf(c, k);
			if(k == -1){
				break;
			}
			k++;
		}
		out.println(k == -1 ? "NO" : "YES");
	}

	public static void main(String args[]) {
		new ComputationalBiology().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
