package com.moodaye.playground.programmerSchool.reviewJanFeb2018;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #86 Start = 9:50am End = 10:00am*/
/* Complexity = O(1)
 * Space = O(1)
 */
public class ChessQueensPuzzle {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		// the corner square covers minimum number of queen positions
		// this works for n = 1 and n = 2 as well.
		out.println((n * n) - (n + n - 1 + n - 1) );
	}

	public static void main(String args[]) {
		new ChessQueensPuzzle().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
