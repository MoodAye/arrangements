package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set32;

import java.util.Scanner;
import java.io.PrintWriter;
import java.util.Locale;

/** Programmer's School #295 */
public class EncryptedMessage {
	void solve(Scanner in, PrintWriter out) {
		String encrypted = in.next();
		String original = in.next();
		for (int i = 0; i <= 25; i++) {
			String trial = decrypt(encrypted, i);
			if (trial.indexOf(original) != -1) {
				out.println(trial);
				return;
			}
		}
		out.println("IMPOSSIBLE");
	}

	static String decrypt(String s, int shift) {
		char[] cs = s.toCharArray();
		StringBuilder sb = new StringBuilder();
		for (char c : cs) {
			char shifted = (char) (c - shift);
			if (shifted < 'A') {
				shifted = (char) (c - shift + 26);
			}
			sb.append(shifted);
		}
		return sb.toString();
	}

	public static void main(String[] args) {
		new EncryptedMessage().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}

}
