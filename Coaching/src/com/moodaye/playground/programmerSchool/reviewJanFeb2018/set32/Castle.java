package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set32;

import java.util.Scanner;
import java.io.PrintWriter;
import java.util.Locale;

/** Programmer's School #528 */
//Start Time = 10:47am End Time = 11:13am; time taken = 26 min
// Max sides = 10e6 max level = 10e6; 
// max rooms in order of n + 2n +3n +.....+10e6n  .... (n + 10e6n)/2 * 10e6 or order of magnitude = 10e12
// number of loops = 10e6 - so we are ok here from a time perspective.
public class Castle {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int k = in.nextInt();
/*		long rooms = n; //(level 1)
		for(long level = 2; level <= k; level++){
			rooms += (level * n) - (3 + (level - 2) * 2);
		}
		out.println(rooms);
		*/
		out.println(roomsWithFormula(n, k));
	}

	/* n = sides; k = levels */
	// max n = 1e6; max k = 1e6
	long roomsWithFormula(int n, int k){
		long rooms = ((n + 1L * n * k) * k) / 2;
		long overlap = 3 * (k - 1) + (k - 1L) * (k - 2);
		return rooms - overlap;
	}

	public static void main(String[] args) {
		new Castle().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}

}
