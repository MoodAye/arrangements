package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set32;

import java.util.Scanner;

import java.io.InputStream;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Locale;

/** Programmer's School #41 */
// Start Time = 11:24am End Time = 11:37am Time taken = 13min
// need fast scanner here since we are reading 1_000_000 ints and regular
// scanner can only do ~400_000 ints per second
public class CountingSort {
	void solve(Scanner in, PrintWriter out) {
		int[] counter = new int[201];
		int n = in.nextInt();
		for(int i = 0; i < n; i++){
			counter[in.nextInt() + 100]++;
		}
		
		for(int i = 0; i < 201; i++){
			while(counter[i] > 0){
				out.print((i - 100) + " ");
				counter[i]--;
			}
		}
	}

	public static void main(String[] args) {
		new CountingSort().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}

	static class Scanner implements AutoCloseable {

		InputStream is;
		byte buffer[] = new byte[1 << 16];
		int size = 0;
		int pos = 0;

		Scanner(InputStream is) {
			this.is = is;
		}

		int nextChar() {
			if (pos >= size) {
				try {
					size = is.read(buffer);
				} catch (java.io.IOException e) {
					throw new java.io.IOError(e);
				}
				pos = 0;
				if (size == -1) {
					return -1;
				}
			}
			Assert.check(pos < size);
			int c = buffer[pos] & 0xFF;
			pos++;
			return c;
		}

		int nextInt() {
			int c = nextChar();
			while (c == ' ' || c == '\r' || c == '\n' || c == '\t') {
				c = nextChar();
			}
			if (c == '-') {
				c = nextChar();
				Assert.check('0' <= c && c <= '9');
				int n = -(c - '0');
				c = nextChar();
				while ('0' <= c && c <= '9') {
					int d = c - '0';
					c = nextChar();
					Assert.check(n > Integer.MIN_VALUE / 10
							|| n == Integer.MIN_VALUE / 10 && d <= -(Integer.MIN_VALUE % 10));
					n = n * 10 - d;
				}
				return n;
			} else {
				Assert.check('0' <= c && c <= '9');
				int n = c - '0';
				c = nextChar();
				while ('0' <= c && c <= '9') {
					int d = c - '0';
					c = nextChar();
					Assert.check(
							n < Integer.MAX_VALUE / 10 || n == Integer.MAX_VALUE / 10 && d <= Integer.MAX_VALUE % 10);
					n = n * 10 + d;
				}
				return n;
			}
		}

		@Override
		public void close() {
		}
	}

	static class Assert {
		static void check(boolean e) {
			if (!e) {
				throw new AssertionError();
			}
		}
	}
}
