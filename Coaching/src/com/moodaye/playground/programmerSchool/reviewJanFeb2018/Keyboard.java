package com.moodaye.playground.programmerSchool.reviewJanFeb2018;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #66 Start = 9:34am End = 9:43am*/
/* Complexity = O(1)
 * Storage = O(1) 
 */
public class Keyboard {
	void solve(Scanner in, PrintWriter out) {
		String c = in.next();
		String kb = "qwertyuiopasdfghjklzxcvbnmq";
		int next = kb.indexOf(c) + 1;
		out.println(kb.charAt(next));
	}

	public static void main(String args[]) {
		new Keyboard().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
