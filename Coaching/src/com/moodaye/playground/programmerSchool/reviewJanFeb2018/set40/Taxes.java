package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set40;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #293 */
// Start = 1:28pm End Time = 1:47 Time Taken = 19min
public class Taxes {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		
		int[] profits = new int[n];
		for(int i = 0; i < n; i++){
			profits[i] = in.nextInt();
		}
		
		int[] taxes = new int[n];
		for(int i = 0; i < n; i++){
			taxes[i] = in.nextInt();
		}
		
		int best = 0;
		int max = Integer.MIN_VALUE;
		for(int i = 0; i < n; i++){
			int tax = profits[i] * taxes[i];
			if(tax > max){
				max = tax;
				best = i + 1;
			}
		}
		out.println(best);
	}

	public static void main(String args[]) {
		new Taxes().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
