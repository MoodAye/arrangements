package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set40;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #26 */
// Start Time = 2:20pm Paused = 2:55pm 
// Resumed 7:10pm End = 7:26pm ... Total Time = 41 min
public class TwoCircles {
	void solve(Scanner in, PrintWriter out) {
		int x1 = in.nextInt();
		int y1 = in.nextInt();
		int r1 = in.nextInt();
		
		int x2 = in.nextInt();
		int y2 = in.nextInt();
		int r2 = in.nextInt();
		
		int dCentersSq = (x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2);
		int rSumSq = (r1 + r2) * (r1 + r2);
		int rDiffSq = (r1 - r2) * (r1 - r2);
		if(dCentersSq <= rSumSq && dCentersSq >= rDiffSq){
			out.println("YES");
		}
		else{
			out.println("NO");
		}
	}

	public static void main(String args[]) {
		new TwoCircles().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
