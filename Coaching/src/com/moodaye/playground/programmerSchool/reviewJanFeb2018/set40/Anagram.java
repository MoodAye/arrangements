package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set40;

import java.util.Scanner;
import java.util.Arrays;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #693 */
// Start Time = 2:58pm End Time = 3:01pm Time Taken = 3min
public class Anagram {
	void solve(Scanner in, PrintWriter out) {
		char[] word1 = in.next().toLowerCase().toCharArray();
		char[] word2 = in.next().toLowerCase().toCharArray();
		Arrays.sort(word1);
		Arrays.sort(word2);
		if (Arrays.equals(word1, word2)) {
			out.println("Yes");
		} else {
			out.println("No");
		}
	}

	public static void main(String args[]) {
		new Anagram().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
