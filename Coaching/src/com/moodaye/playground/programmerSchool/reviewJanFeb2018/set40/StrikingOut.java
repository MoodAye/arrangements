package com.moodaye.playground.programmerSchool.reviewJanFeb2018.set40;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #273 */
// Start Time = 1:50pm End Time = 2:12 Time Taken = 22min
public class StrikingOut {
	void solve(Scanner in, PrintWriter out) {
		char[] n = in.next().toCharArray();
		boolean[] marker = new boolean[1000];
		for(int i = 0; i < n.length - 2; i++){
			if(n[i] == '0'){
				continue;
			}
			for(int j = i + 1; j < n.length - 1; j++){
				for(int k = j + 1; k < n.length; k++){
					int index = (n[i] - '0') * 100 + (n[j] - '0') * 10 + n[k] - '0';
					marker[index] = true;
				}
			}
		}
		
		int cnt = 0;
		for(int i = 100; i < 1000; i++){
			if(marker[i]){
				cnt++;
			}
		}
		out.println(cnt);
	}

	public static void main(String args[]) {
		new StrikingOut().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}

}
