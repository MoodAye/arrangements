package com.moodaye.playground.programmerSchool.April2019.review;

import java.util.Arrays;
import java.util.Locale;
import java.io.InputStream;
import java.io.PrintWriter;

/** Programmer's School #87 */
// Start 8:10am  End 8:23am  Time Taken 13 min
public class UnitedTeam {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();

		if (n == 0) {
			out.println(0);
			return;
		}

		int[] pi = new int[n];
		for (int i = 0; i < n; i++) {
			pi[i] = in.nextInt();
		}

		if (n == 1) {
			out.println(pi[0]);
			return;
		}

		Arrays.sort(pi);
		int maxPi = pi[n - 1] + pi[n - 2];
		int currPi = maxPi;

		int hi = n - 1;
		for (int i = n - 3; i >= 0; i--) {
			while (pi[i] + pi[i + 1] < pi[hi]) {
				currPi -= pi[hi];
				hi--;
			}
			currPi += pi[i];
			maxPi = Math.max(maxPi, currPi);
		}
		out.println(maxPi);
	}

	public static void main(String args[]) {
		new UnitedTeam().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}

	static class Scanner implements AutoCloseable {
		InputStream is;
		int pos = 0;
		int size = 0;
		byte[] buffer = new byte[1024];

		Scanner(InputStream is) {
			this.is = is;
		}

		int nextChar() {
			if (pos >= size) {
				try {
					size = is.read(buffer);
				} catch (java.io.IOException e) {
					throw new java.io.IOError(e);
				}
				pos = 0;
				if (size == -1) {
					return -1;
				}
			}
			Assert.check(pos < size);
			int c = buffer[pos] & 0xFF;
			pos++;
			return c;
		}

		int nextInt() {
			int c = nextChar();
			while (c == ' ' || c == '\r' || c == '\n' || c == '\t') {
				c = nextChar();
			}
			if (c == '-') {
				c = nextChar();
				Assert.check('0' <= c && c <= '9');
				int n = -(c - '0');
				c = nextChar();
				while ('0' <= c && c <= '9') {
					int d = c - '0';
					c = nextChar();
					Assert.check(n > Integer.MIN_VALUE / 10
							|| n == Integer.MIN_VALUE / 10 && d <= -(Integer.MIN_VALUE % 10));
					n = n * 10 - d;
				}
				return n;
			} else {
				Assert.check('0' <= c && c <= '9');
				int n = c - '0';
				c = nextChar();
				while ('0' <= c && c <= '9') {
					int d = c - '0';
					c = nextChar();
					Assert.check(
							n < Integer.MAX_VALUE / 10 || n == Integer.MAX_VALUE / 10 && d <= Integer.MAX_VALUE % 10);
					n = n * 10 + d;
				}
				return n;
			}
		}

		@Override
		public void close() {
		}
	}

	static class Assert {
		static void check(boolean e) {
			if (!e) {
				throw new AssertionError();
			}
		}
	}
}
