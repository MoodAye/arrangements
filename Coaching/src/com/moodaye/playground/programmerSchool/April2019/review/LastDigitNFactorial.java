package com.moodaye.playground.programmerSchool.April2019.review;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #447 */
// Start 10:06pm End = 10:13pm  Time Taken = 7min
public class LastDigitNFactorial {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int lastDigit = 1;
		int cnt2 = 0;
		int cnt5 = 0;
		for(int i = 2; i <= n; i++){
			int temp = i;
			while(temp % 2 == 0){
				cnt2++;
				temp /= 2;
			}
			while(temp % 5 == 0){
				cnt5++;
				temp /= 5;
			}
			lastDigit = (lastDigit * (temp % 10)) % 10;
		}
		cnt2 -= cnt5;
		while(cnt2-- > 0){
			lastDigit = (lastDigit * 2) % 10;
		}
		out.println(lastDigit);
	}

	public static void main(String args[]) {
		new LastDigitNFactorial().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
