package com.moodaye.playground.programmerSchool.April2019.review;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School # */
//start 6:42am End 6:49am ~7min
public class ProductsOfDigits3 {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		out.println(smallest(n) + " " + largest(n));
	}
	
	String smallest(int n){
		StringBuilder sb = new StringBuilder();
		for(int i = 9; i >= 2; i--){
			while(n % i == 0){
				n /= i;
				sb.append(i);
			}
		}
		if(n != 1){
			return "-1";
		}
		
		return sb.reverse().toString();
	}
	
	String largest(int n){
		StringBuilder sb = new StringBuilder();
		for(int i = 2; i <= 7; i++){
			while(n % i == 0){
				n /= i;
				sb.append(i);
			}
		}
		if(n != 1){
			return "-1";
		}
		return sb.reverse().toString();
	}

	public static void main(String args[]) {
		new ProductsOfDigits3().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}