package com.moodaye.playground.programmerSchool.April2019.review;

import java.util.Locale;
import java.io.InputStream;
import java.io.PrintWriter;

/** Programmer's School #630 */
//Start = 11:09pm; End 11:19pm
public class Guards{
	void solve(Scanner in, PrintWriter out) {
		int t = in.nextInt();
		for(int i = 0; i < t; i++){
			int[] periods = new int[10_002];
			int g = in.nextInt();
			int[] start = new int[g];
			int[] end = new int[g];
			for(int j = 0; j < g; j++){
				start[j] = in.nextInt();
				periods[start[j] + 1]++;
				
				end[j] = in.nextInt();
				periods[end[j] + 1]--;
			}
			
			for(int k = 1; k <= 10_000; k++){
				periods[k] += periods[k - 1];
			}
			
			// check for unguarded period
			boolean unguarded = false;
			for(int k = 1; k <= 10_000; k++){
				if(periods[k] == 0){
					unguarded = true;
				}
			}
			
			// check for redundant
			boolean redundant = true;
			for(int k = 0; k < g; k++){
				redundant = true;
				for(int m = start[k] + 1; m <= end[k]; m++){
					if(periods[m] == 1){
						redundant = false;
						break;
					}
				}
				if(redundant){
					break;
				}
			}
			
			if(unguarded || redundant){
				out.println("Wrong Answer");
			}
			else{
				out.println("Accepted");
			}
		}
	}

	public static void main(String args[]) {
		new Guards().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
class Scanner implements AutoCloseable {
	InputStream is;
	int pos = 0;
	int size = 0;
	byte[] buffer = new byte[1024];

	Scanner(InputStream is) {
		this.is = is;
	}

	int nextChar() {
		if (pos >= size) {
			try {
				size = is.read(buffer);
			} catch (java.io.IOException e) {
				throw new java.io.IOError(e);
			}
			pos = 0;
			if (size == -1) {
				return -1;
			}
		}
		Assert.check(pos < size);
		int c = buffer[pos] & 0xFF;
		pos++;
		return c;
	}

	int nextInt() {
		int c = nextChar();
		while (c == ' ' || c == '\r' || c == '\n' || c == '\t') {
			c = nextChar();
		}
		if (c == '-') {
			c = nextChar();
			Assert.check('0' <= c && c <= '9');
			int n = -(c - '0');
			c = nextChar();
			while ('0' <= c && c <= '9') {
				int d = c - '0';
				c = nextChar();
				Assert.check(
						n > Integer.MIN_VALUE / 10 || n == Integer.MIN_VALUE / 10 && d <= -(Integer.MIN_VALUE % 10));
				n = n * 10 - d;
			}
			return n;
		} else {
			Assert.check('0' <= c && c <= '9');
			int n = c - '0';
			c = nextChar();
			while ('0' <= c && c <= '9') {
				int d = c - '0';
				c = nextChar();
				Assert.check(n < Integer.MAX_VALUE / 10 || n == Integer.MAX_VALUE / 10 && d <= Integer.MAX_VALUE % 10);
				n = n * 10 + d;
			}
			return n;
		}
	}

	@Override
	public void close() {
	}
}

class Assert {
	static void check(boolean e) {
		if (!e) {
			throw new AssertionError();
		}
	}
}
