package com.moodaye.playground.programmerSchool.April2019.review;

import java.util.Scanner;
import java.util.Arrays;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #472 */
public class Gifts {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int m = in.nextInt();
		int[] gifts = new int[n];

		for (int i = 0; i < n; i++) {
			gifts[i] = in.nextInt();
		}

		Arrays.sort(gifts);

		int smallest = gifts[0];
		int equalCnt = 1;
		for (int i = 1; i < n; i++) {
			if (gifts[i] == gifts[i - 1]) {
				equalCnt++;
				continue;
			}
			if(m >= equalCnt * (gifts[i] - gifts[i - 1])){
				m -= equalCnt * (gifts[i] - gifts[i - 1]);
				smallest = gifts[i];
				equalCnt++;
			}
			else{
				smallest += m / equalCnt;
				m = 0;
				break;
			}
		}
		smallest += m / equalCnt; 
		out.println(smallest);
	}

	public static void main(String args[]) {
		new Gifts().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
