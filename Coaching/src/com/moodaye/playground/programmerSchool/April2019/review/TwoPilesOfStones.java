package com.moodaye.playground.programmerSchool.April2019.review;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #71 */
// 9:38pm 9:48pm  ~10min
public class TwoPilesOfStones {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int[] wts = new int[n];
		for (int i = 0; i < n; i++) {
			wts[i] = in.nextInt();
		}
		out.println(getMinDiff(wts, 0, 0, 0));
	}

	int getMinDiff(int[] wts, int idx, int p1, int p2) {
		if(idx == wts.length){
			return Math.abs(p1 - p2);
		}
		int diff1 = getMinDiff(wts, idx + 1, p1 + wts[idx], p2);
		int diff2 = getMinDiff(wts, idx + 1, p1, p2 + wts[idx]);
		return Math.min(diff1, diff2);
	}

	public static void main(String args[]) {
		new TwoPilesOfStones().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
