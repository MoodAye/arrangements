package com.moodaye.playground.programmerSchool.April2019.review;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #216 */
// Start 737am  End 7:44am  (~11min)
public class LabelsCollection {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int[] caps = new int[n];
		int max = 0;
		int sumButMax = 0;
		for(int i = 0; i < n; i++){
			caps[i] = in.nextInt();
			max = Math.max(caps[i], max);
			sumButMax += caps[i];
		}
		sumButMax -= max;
		if(sumButMax <= max){
			out.println(sumButMax);
		}
		else{
			out.println((sumButMax + max) / 2);
		}
	}

	public static void main(String args[]) {
		new LabelsCollection().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}