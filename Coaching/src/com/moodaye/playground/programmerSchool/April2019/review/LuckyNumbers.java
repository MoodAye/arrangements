package com.moodaye.playground.programmerSchool.April2019.review;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #671 */
// 8:11am - 8:27am   16min
public class LuckyNumbers { 
	void solve(Scanner in, PrintWriter out) {
		String n = in.next();
		int len = n.length();
		long cnt = (1L << len) - 2;
		int idx = 0;
		while(true){
			if(n.charAt(idx) < '4'){
				out.println(cnt);
				return;
			}
			else if(n.charAt(idx) == '4'){
				if(idx == len -1){
					cnt += 1;
					out.println(cnt);
					return;
				}
				idx++;
				continue;
			}
			else if(n.charAt(idx) < '7'){
				cnt += (1L << len - idx - 1);
				out.println(cnt);
				return;
			}
			else if(n.charAt(idx) == '7'){
				if(idx == len -1){
					cnt += 2;
					out.println(cnt);
					return;
				}
				cnt += (1L << len - idx - 1);
				idx++;
				continue;
			}
			else{
				cnt += (1L << len - idx);
				out.println(cnt);
				return;
			}
		}
	}

	public static void main(String args[]) {
		new LuckyNumbers().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
