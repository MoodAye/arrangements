package com.moodaye.playground.programmerSchool.April2019.review;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #255 */
// 920pm - 928pm ... 8min
public class NumbersRepresentation {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int smallestFactor = n;
		for(int i = 2; i * i <= n; i++){
			if(n % i == 0){
				smallestFactor = i;
				break;
			}
		}
		out.println(n / smallestFactor + " " + (n - n / smallestFactor));
	}

	public static void main(String args[]) {
		new NumbersRepresentation().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
