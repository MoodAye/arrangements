package com.moodaye.playground.programmerSchool.April2019.review;

import java.util.Scanner;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.io.PrintWriter;

/** Programmer's School #87 */
// Start 636pm 6:49pm
public class String2 {
	void solve(Scanner in, PrintWriter out) {
		Map<String, Integer> s = new HashMap<>();
		while(true){
			String next = in.next();
			if(next.equals("ENDOFINPUT")){
				break;
			}
			s.merge(next, 1, Integer::sum);
		}
		int cnt = 0;
		List<String> keys = new ArrayList<>(s.keySet());
		for(String s1 : keys){
			for(String s2 : keys){
				String concat = s1 + s2;
				if((s1 + s2).length() > 100){
					continue;
				}
				if(s.containsKey(concat)){
					cnt += s.get(concat);
					s.put(concat, 0);
				}
			}
		}
		out.println(cnt);
	}

	public static void main(String args[]) {
		new String2().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}