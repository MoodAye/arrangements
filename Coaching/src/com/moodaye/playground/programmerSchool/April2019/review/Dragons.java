package com.moodaye.playground.programmerSchool.April2019.review;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #42 */
// Time ~20min
public class Dragons {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		if(n == 1){
			out.println(1);
		}
		
		int cnt2 = 0;
		int cnt3 = 0;
		if(n % 3 == 1){
			cnt2 = 2;
			cnt3 = (n / 3) - 1;
		}
		else{
			cnt2 = (n % 3) / 2;
			cnt3 = n / 3;
		}
		
		long ans = 1;
		while(cnt3-- > 0){
			ans *= 3;
		}
		while(cnt2-- > 0){
			ans *= 2;
		}
		out.println(ans);
	}

	public static void main(String args[]) {
		new Dragons().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
