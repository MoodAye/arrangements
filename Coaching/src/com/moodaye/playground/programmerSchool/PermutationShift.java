package com.moodaye.playground.programmerSchool;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

/** Programmer's School - # 392*/
public class PermutationShift {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int[] a = new int[n];
		int minIndex = 0;
		for (int i = 0; i < a.length; i++) {
			a[i] = in.nextInt();
			if (a[minIndex] > a[i]){
				minIndex = i;
			}
		}
		
		for(int i = minIndex; i < a.length; i++){
			out.print(a[i] + " ");
		}
		for(int i = 0; i < minIndex; i++){
			out.print(a[i] + " ");
		}
	}

	public static void main(String[] args) {
		new PermutationShift().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}

}
