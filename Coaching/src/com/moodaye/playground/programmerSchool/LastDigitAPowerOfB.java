package com.moodaye.playground.programmerSchool;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

/** Programmer's School - Problem 79 */
public class LastDigitAPowerOfB {
	void solve(Scanner in, PrintWriter out) {
		int A = in.nextInt();
		int B = in.nextInt();
		out.println(lastDigit(A, B));
	}
	
	public static int lastDigitSimpler(int a, int b){
		int lastDigita = a % 10;
		if (lastDigita == 2 || lastDigita == 3 || lastDigita == 7 || lastDigita == 8){
			b = (b - 1) % 4 + 1; //converts b to number [1,4] with the same result of %4 as before
		}
		int p = 1;
		for(int i = 0; i < b; i++){
			p = (p * a) % 10;
		}
		return p;
	}

	/**
	 * 
	 * 
	 * B	1 2 3 4 5 6 7 8 9 0 
	 * 2    2 4 8 6 2 4 8 6 2 4
	 * 3    3 9 7 1 3 9 7 1 3 9
	 * 4    4 6 4 6 4 6 4 6 4 6
	 * 7    7 9 3 1 7 9 3 1 7 9
	 * 8    8 4 2 6 8 4 2 6 8 4
	 * 9	9 1 9 1 9 1 9 1 9 1
	 * so 2^10 ends in the same number as 2^2.  Cycle length is 4. --> 10 % 4 = 2.
	 * any number ending in 2 raised to a power of B % 4 = 2 (e.g., B = 10) will end in 2.
	 * 
	 */
	public static int lastDigit(int A, int B){
		//step 1 - get last digit of A
		int lastDigitA = A % 10;
		int resultLastDigit = 0;
		
		switch(lastDigitA){
			case 0: { 
					resultLastDigit =  0;
					break;
			}
			case 1: {
					resultLastDigit = 1;
					break;
			}
			case 2: {
				int cycleB = B % 4;
				if(cycleB == 0){
					resultLastDigit = 6;
				}
				else if (cycleB == 1){
					resultLastDigit = 2;
				}
				else if (cycleB == 2){
					resultLastDigit = 4;
				}
				else if (cycleB == 3){
					resultLastDigit = 8;
				}
				break;
			}
			case 3: {
				int cycleB = B % 4;
				if(cycleB == 0){
					resultLastDigit = 1;
				}
				else if (cycleB == 1){
					resultLastDigit = 3;
				}
				else if (cycleB == 2){
					resultLastDigit = 9;
				}
				else if (cycleB == 3){
					resultLastDigit = 7;
				}
				break;
			}
			case 4: {
				int cycleB = B % 2;
				if(cycleB == 0){
					resultLastDigit = 6;
				}
				else if (cycleB == 1){
					resultLastDigit = 4;
				}
				break;
			}
			case 5: {
				resultLastDigit = 5;
				break;
			}
			case 6: {
				resultLastDigit = 6;
				break;
			}
			case 7: {
				int cycleB = B % 4;
				if(cycleB == 0){
					resultLastDigit = 1;
				}
				else if (cycleB == 1){
					resultLastDigit = 7;
				}
				else if (cycleB == 2){
					resultLastDigit = 9;
				}
				else if (cycleB == 3){
					resultLastDigit = 3;
				}
				break;
			}
			case 8: {
				int cycleB = B % 4;
				if(cycleB == 0){
					resultLastDigit = 6;
				}
				else if (cycleB == 1){
					resultLastDigit = 8;
				}
				else if (cycleB == 2){
					resultLastDigit = 4;
				}
				else if (cycleB == 3){
					resultLastDigit = 2;
				}
				break;
			}
			case 9:{
				int cycleB = B % 2;
				if(cycleB == 0){
					resultLastDigit = 1;
				}
				else if (cycleB == 1){
					resultLastDigit = 9;
				}
				break;
			}
		}
			return resultLastDigit;
	}

	public static void main(String[] args) {
		new LastDigitAPowerOfB().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}

}
