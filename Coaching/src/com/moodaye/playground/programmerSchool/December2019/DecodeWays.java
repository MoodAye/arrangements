package com.moodaye.playground.programmerSchool.December2019;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

// Problem 91 LeetCode
// Time > 1hr
public class DecodeWays {
	void solve(Scanner in, PrintWriter out) {
		String s = in.next();
		out.println(numDecodings(s));
	}
	
	public int numDecodings(String s) {
		char[] cs = s.toCharArray();
		if(cs[0] == '0') {
			return 0;
		}
		
		int prevNum_1 = 1;
		int prevNum_2 = 1;
		int currNum = 1;
		
		for (int i = 1; i < cs.length; i++) {
			currNum = 0;
			int joinedDigits = (cs[i - 1] - '0') * 10 + (cs[i] - '0');
			if(10 <= joinedDigits && joinedDigits <= 26) {
				currNum = prevNum_2;
			}
			if('1' <= cs[i] && cs[i] <= '9') {
				currNum += prevNum_1;
			}
			prevNum_2 = prevNum_1;
			prevNum_1 = currNum;
		}
		return currNum;
	}

	public static void main(String[] args) {
		new DecodeWays().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
