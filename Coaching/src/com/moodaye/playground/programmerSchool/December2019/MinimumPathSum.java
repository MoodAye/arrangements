package com.moodaye.playground.programmerSchool.December2019;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

// Problem 64 - Leetcode
public class MinimumPathSum {
	void solve(Scanner in, PrintWriter out) {

		int rows = in.nextInt();
		int cols = in.nextInt();

		int[][] values = new int[rows][cols];
		for (int r = 0; r < rows; r++) {
			for (int c = 0; c < cols; c++) {
				values[r][c] = in.nextInt();
			}
		}
		out.println(minPathSum(values));
	}
	
	// Time Complexity = O(rows x columns).  
	// Space Complexity = O(1) as we are reusing grid
	// Or Space Complexity = O(rows | columns) if leaving grid alone.
	public int minPathSum(int[][] grid) {
		int rows = grid.length;
		int cols = grid[0].length;
		
		for(int r = 0; r < rows; r++) {
			for(int c = 0; c < cols; c++) {
				if(c == 0 && r == 0) {
					continue;
				}
				if(c == 0) {
					grid[r][c] += grid[r - 1][c];
				}
				else if(r == 0) {
					grid[r][c] += grid[r][c - 1];
				}
				else {
					grid[r][c] += Math.min(grid[r - 1][c], grid[r][c - 1]);
				}
			}
		}
		return grid[rows - 1][cols - 1];
	}

	public static void main(String[] args) {
		new MinimumPathSum().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}

}
