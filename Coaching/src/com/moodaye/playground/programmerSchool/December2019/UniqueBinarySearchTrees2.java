package com.moodaye.playground.programmerSchool.December2019;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

// Problem 95 Leetcode
public class UniqueBinarySearchTrees2 {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		if(n == 0) {
			return;
		}
		List<TreeNode> trees = generateTrees(n);
		for (TreeNode tree : trees) {
			printTree(tree, out);
			out.println();
		}
	}

	void printTree(TreeNode tree, PrintWriter out) {
		if (tree == null) {
			out.print("null, ");
			return;
		}
		out.print(tree.val + ", ");
		if (!(tree.left == null && tree.right == null)) {
			printTree(tree.left, out);
			printTree(tree.right, out);
		}
	}

	/**
	 * Definition for a binary tree node. public class TreeNode { int val; TreeNode
	 * left; TreeNode right; TreeNode(int x) { val = x; } }
	 */
	public List<TreeNode> generateTrees(int n) {
		if(n == 0) {
			return new ArrayList<TreeNode>();
		}
		List<TreeNode>[][] memo = new ArrayList[n + 1][n + 1];
		List<TreeNode> trees = generateTrees(1, n, memo);
		return trees;
	}

	public List<TreeNode> generateTrees(int left, int right, List<TreeNode>[][] memo) {
		if(memo[left][right] != null) {
			return memo[left][right];
		}
		
		List<TreeNode> bsts = new ArrayList<>();
		if (right == left) {
			TreeNode leaf = new TreeNode(left);
			bsts.add(leaf);
			memo[left][right] = bsts;
			return bsts;
		} else if (right - left == 1) {
			TreeNode r = new TreeNode(right);
			r.left = new TreeNode(left);
			TreeNode lft = new TreeNode(left);
			lft.right = new TreeNode(right);
			bsts.add(lft);
			bsts.add(r);
			memo[left][right] = bsts;
			return bsts;
		}

		for (int root = left; root <= right; root++) {
			List<TreeNode> leftBsts = null;
			if (root != left) {
				leftBsts = generateTrees(left, root - 1, memo);
			}

			List<TreeNode> rightBsts = null;
			if (root != right) {
				rightBsts = generateTrees(root + 1, right, memo);
			}

			// marry trees and tie to root
			if (leftBsts == null) {
				for (TreeNode node : rightBsts) {
					TreeNode r = new TreeNode(root);
					r.right = node;
					bsts.add(r);
				}
			} else if (rightBsts == null) {
				for (TreeNode node : leftBsts) {
					TreeNode r = new TreeNode(root);
					r.left = node;
					bsts.add(r);
				}
			} else {
				for (TreeNode nodeR : rightBsts) {
					for (TreeNode nodeL : leftBsts) {
						TreeNode r = new TreeNode(root);
						r.left = nodeL;
						r.right = nodeR;
						bsts.add(r);
					}
				}
			}
			memo[left][right] = bsts;
		}
		return bsts;
	}

	static class TreeNode {
		int val;
		TreeNode left;
		TreeNode right;

		TreeNode(int x) {
			val = x;
		}
	}

	public static void main(String[] args) {
		new UniqueBinarySearchTrees2().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
