package com.moodaye.playground.programmerSchool.December2019;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

// Problem 139 Leetcode
public class WordBreak {
	void solve(Scanner in, PrintWriter out) {
		String s = in.next();
		int dictSize = in.nextInt();
		List<String> dict = new ArrayList<>();
		for(int i = 0; i < dictSize; i++) {
			dict.add(in.next());
		}
		out.println(wordBreak(s, dict));
	}
	
	public boolean wordBreak(String s, List<String> wordDict) {
		if(s == null  || s.isEmpty()) {
			return true;
		}
		
		int slen = s.length();
		boolean[] isWord = new boolean[slen];
		
		for(int i = 0; i < slen; i++) {
			String prefix = s.substring(0, i + 1);
			int plen = prefix.length();
			for(String word : wordDict) {
				int wlen = word.length();
				if(prefix.endsWith(word) && ((plen - wlen) == 0 || isWord[plen - wlen - 1])) {
					isWord[i] = true;
					break;
				}
			}
		}
		return isWord[slen - 1];
	}
	
	public boolean wordBreakBf(String s, List<String> wordDict) {
		if(s.isEmpty()) {
			return true;
		}
		
		for(String word : wordDict) {
			if(s.indexOf(word) == 0) {
				String subs = s.substring(word.length());
				if(wordBreak(subs, wordDict)) {
					return true;
				};
			}
		}
		return false;
	}

	public static void main(String[] args) {
		new WordBreak().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
