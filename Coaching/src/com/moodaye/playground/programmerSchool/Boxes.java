package com.moodaye.playground.programmerSchool;
import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

/** Programmer's School - Problem 340 */
public class Boxes {
	
	void solve(Scanner in, PrintWriter out){
		
		out.println(checkBoxSizes(in.nextInt(), in.nextInt(), 
				in.nextInt(), in.nextInt(),  
				in.nextInt(),  in.nextInt()));
	}
	
	public static String checkBoxSizes(int box1A, int box1B, int box1C, int box2A, int box2B, int box2C){
		//arrange box dimensions in ascending order so that we can compare
		if(box1A < box1B){   
			int temp = box1A;
			box1A = box1B;
			box1B = temp; 
		}
		if(box1B < box1C){  
			int temp = box1B;  
			box1B = box1C;
			box1C = temp;    
		}
		if(box1A < box1B){  
			int temp = box1A;
			box1A = box1B;
			box1B = temp;	
		}
		
		if(box2A < box2B){  
			int temp = box2A;
			box2A = box2B;
			box2B = temp;
		}
		if(box2B < box2C){  
			int temp = box2B;  
			box2B = box2C;
			box2C = temp;    
		}
		if(box2A < box2B){  
			int temp = box2A;
			box2A = box2B;
			box2B = temp;	
		}
		
		if(box1A == box2A && box1B == box2B && box1C == box2C){
			return "Boxes are equal";
		}
		
		if(box1A >= box2A && box1B >= box2B && box1C >= box2C){
			return "The first box is larger than the second one";
		}
		
		if(box1A <= box2A && box1B <= box2B && box1C <= box2C){
			return "The first box is smaller than the second one";
		}
		
		return "Boxes are incomparable";
	}
	
	public static void main(String[] args) {
		new Boxes().run();
	}
	
	void run(){
		Locale.setDefault(Locale.US);
		try(Scanner in = new Scanner(System.in);
				PrintWriter out = new PrintWriter(System.out)){
			solve(in,out);
		}
	}
}
