package com.moodaye.playground.programmerSchool._202108;

import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Locale;
import java.util.Scanner;

// Problem 
// wa 11
public class RollingDie {
	void solve(Scanner in, PrintWriter out) {
		int trns = in.nextInt();
		int trgt = in.nextInt();

		double[] p = new double[3000 + 7];
		p[0] = 1.0;

		for (int t = 1; t <= trns; t++) {
			for (int i = 3000; i >= 0; i--) {
				for (int d = 6; d >= 1; d--) {
					p[i + d] += p[i] / 6.0;
				}
				p[i] = 0;
			}
		}
		
		out.printf("%.10f%n", p[trgt]);
	}

	public static void main(String[] args) {
		new RollingDie().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
