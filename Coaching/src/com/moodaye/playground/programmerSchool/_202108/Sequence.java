package com.moodaye.playground.programmerSchool._202108;

import java.io.PrintWriter;
import java.math.BigInteger;
import java.util.Locale;
import java.util.Scanner;

// Problem 630pm - 645pm
public class Sequence {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		
		// kth row sum = sum(k - 1) + 2 * sum(k - 1) - 2
		// sum(1) = 2
		// sum(2) = 4 = 3 * 2 - 2
		// sum(3) = 10 = 3 * 4 - 2
		// sum(4) = 3 * 10 - 2 = 28
		
		BigInteger sum = BigInteger.TWO;
		BigInteger three = BigInteger.TWO.add(BigInteger.ONE);
		
		for(int i = 1; i <= n; i++) {
			sum = sum.multiply(three).subtract(BigInteger.TWO);
		}
		
		out.println(sum.toString());
	}

	public static void main(String[] args) {
		new Sequence().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
