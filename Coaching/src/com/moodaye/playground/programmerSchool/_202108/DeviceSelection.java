package com.moodaye.playground.programmerSchool._202108;

import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Scanner;

// Problem 674  6:08am - 6:15am = 7min
public class DeviceSelection {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		
		Map<Integer, Integer> memo = new HashMap<>();
		memo.put(0, 0);
		memo.put(1, 0);
		memo.put(2, 0);
		memo.put(3, 1);
		
		out.println(ways(n, memo));
	}
	
	private int ways(int n, Map<Integer, Integer> memo) {
		if(memo.containsKey(n)) {
			return memo.get(n);
		}
		
		int w = ways(n / 2, memo) + ways((n + 1) / 2, memo);
		
		memo.put(n, w);
		return w;
	}

	public static void main(String[] args) {
		new DeviceSelection().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
