package com.moodaye.playground.programmerSchool._202108;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;
import java.io.InputStream;

// Problem i
public class LandCommittee {
	void solve(FastScanner in, PrintWriter out) {
		int h = in.nextInt();
		int w = in.nextInt();
		int n = in.nextInt();
		
		int[][] cost = new int[h][w];
		for (int row = 0; row < h; row++) {
			for (int col = 0; col < w; col++) {
				cost[row][col] = in.nextInt();
			}
		}
		
		for (int row = 0; row < h; row++) {
			for (int col = 0; col < w; col++) {
				if(row == 0 && col == 0) {
					continue;
				}
				else if(row == 0) {
					cost[row][col] += cost[row][col - 1];
				}
				else if(col == 0) {
					cost[row][col] += cost[row - 1][col];
				}
				else {
					cost[row][col] += cost[row - 1][col] + cost[row][col - 1] - cost[row - 1][col - 1];
				}
			}
		}
		
		while(n-- > 0) {
				int a = in.nextInt() - 1;
				int b = in.nextInt() - 1;
				int c = in.nextInt() - 1;
				int d = in.nextInt() - 1;
				
				if(a == 0 && b == 0) {
					out.println(cost[c][d]);
				}
				else if(a == 0) {
					out.println(cost[c][d] - cost[c][b - 1]);
				}
				else if(b == 0) {
					out.println(cost[c][d] - cost[a - 1][d]);
				}
				else {
					out.println(cost[c][d] - cost[c][b - 1] - cost[a - 1][d] + cost[a - 1][b - 1]);
				}
		}
	}

	public static void main(String[] args) {
		new LandCommittee().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (FastScanner in = new FastScanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}

	static class FastScanner implements AutoCloseable {

		InputStream is;
		byte buffer[] = new byte[1 << 16];
		int size = 0;
		int pos = 0;

		FastScanner(InputStream is) {
			this.is = is;
		}

		int nextChar() {
			if (pos >= size) {
				try {
					size = is.read(buffer);
				} catch (java.io.IOException e) {
					throw new java.io.IOError(e);
				}
				pos = 0;
				if (size == -1) {
					return -1;
				}
			}
			Assert.check(pos < size);
			int c = buffer[pos] & 0xFF;
			pos++;
			return c;
		}

		int nextInt() {
			int c = nextChar();
			while (c == ' ' || c == '\r' || c == '\n' || c == '\t') {
				c = nextChar();
			}
			if (c == '-') {
				c = nextChar();
				Assert.check('0' <= c && c <= '9');
				int n = -(c - '0');
				c = nextChar();
				while ('0' <= c && c <= '9') {
					int d = c - '0';
					c = nextChar();
					Assert.check(n > Integer.MIN_VALUE / 10
							|| n == Integer.MIN_VALUE / 10 && d <= -(Integer.MIN_VALUE % 10));
					n = n * 10 - d;
				}
				return n;
			} else {
				Assert.check('0' <= c && c <= '9');
				int n = c - '0';
				c = nextChar();
				while ('0' <= c && c <= '9') {
					int d = c - '0';
					c = nextChar();
					Assert.check(
							n < Integer.MAX_VALUE / 10 || n == Integer.MAX_VALUE / 10 && d <= Integer.MAX_VALUE % 10);
					n = n * 10 + d;
				}
				return n;
			}
		}

		@Override
		public void close() {
		}
	}

	static class Assert {
		static void check(boolean e) {
			if (!e) {
				throw new AssertionError();
			}
		}
	}
}