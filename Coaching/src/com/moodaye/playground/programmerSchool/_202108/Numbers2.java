package com.moodaye.playground.programmerSchool._202108;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

// Problem 
public class Numbers2 {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int max = in.nextInt();
		int cntLen = in.nextInt();
		
		long mod = 1L;
		for(int i = 1; i <= cntLen; i++) {
			mod *= 10;
		}
		
		char[] line = in.next().toCharArray();
		long[] cnt = new long[n];
		cnt[0] = 1;
		
		int maxLen = String.valueOf(max).length();
		
		for(int i = 1; i < n; i++) {
			if(cnt[i] - '0' > max) {
				out.println(0);
				return;
			}
			cnt[i] = cnt[i - 1];
			// 0,1,2,3,4,....   1 ... 1 - 1 + 2
			for(int j = i - 1; j >= i - maxLen + 2 && j >= 0; j--) {
				cnt[i] += cnt[j] % mod;
			}
			// check number of length maxLen
			if(maxLen > 1 && i - maxLen >= 0) {
				String strN = "";
				for(int j = i - maxLen + 1; j <= i; j++) {
					strN += line[j];
				}
				int number = Integer.valueOf(strN);
				if(number <= max) {
					cnt[i] += cnt[i - maxLen] % mod;
				}
			}
		}
		out.println(cnt[n - 1]);
	}

	public static void main(String[] args) {
		new Numbers2().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
