package com.moodaye.playground.programmerSchool._202108;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

// Problem 
public class OneDirectionTravellingSalesman {
	void solve(Scanner in, PrintWriter out) {
		int r = in.nextInt();
		int c = in.nextInt();
		int[][] wts = new int[r + 2][c + 2];
		for (int row = 1; row <= r; row++) {
			for (int col = 1; col <= c; col++) {
				wts[row][col] = in.nextInt();
			}
		}

		for (int edge = 0; edge <= c + 1; edge++) {
			wts[0][edge] = Integer.MAX_VALUE;
			wts[r + 1][edge] = Integer.MAX_VALUE;
		}

		for (int edge = 0; edge <= r + 1; edge++) {
			wts[edge][0] = Integer.MAX_VALUE;
			wts[edge][c + 1] = Integer.MAX_VALUE;
		}

		int[][] path = new int[r + 2][c + 1];

		for (int col = 2; col <= c; col++) {
			for (int row = 1; row <= r; row++) {
				if (wts[row - 1][col - 1] <= wts[row][col - 1]) {
					if (wts[row - 1][col - 1] <= wts[row + 1][col - 1]) {
						path[row][col] = -1;
					} else {
						path[row][col] = +1;
					}
				} else if (wts[row][col - 1] <= wts[row + 1][col - 1]) {
					path[row][col] = 0;
				} else {
					path[row][col] = +1;
				}
				int minRow = row + path[row][col];
				wts[row][col] += wts[minRow][col - 1];
			}
		}

		// find min wt. in last col (need value and row#)
		int minPathWt = Integer.MAX_VALUE;
		int minRowLastCol = 0;
		for (int row = 1; row <= r; row++) {
			if (wts[row][c] < minPathWt) {
				minRowLastCol = row;
				minPathWt = wts[row][c];
			}
		}

		int[] minPathRows = new int[c + 1];
		minPathRows[c] = minRowLastCol;
		for (int col = c - 1; col >= 1; col--) {
			int rowAhead = minPathRows[col + 1];
			minPathRows[col] = rowAhead + path[rowAhead][col + 1];
		}

		for (int col = 1; col <= c; col++) {
			out.print(minPathRows[col] + " ");
		}

		out.println();
		out.println(minPathWt);
	}

	public static void main(String[] args) {
		new OneDirectionTravellingSalesman().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
