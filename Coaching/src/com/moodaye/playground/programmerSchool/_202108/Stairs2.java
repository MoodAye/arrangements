package com.moodaye.playground.programmerSchool._202108;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

// Problem  616am - 633am = 17min
public class Stairs2 {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int[] val = new int[n + 1];
		for (int i = 1; i <= n; i++) {
			val[i] = in.nextInt();
		}
		
		// can either get to step x from step x - 1, x - 2 
		int sumX_1 = 0;
		int sumX_2 = 0;
		int sumX = 0;
		
		int[] route = new int[n + 1];
		
		for(int i = 1; i <= n; i++) {
			if(sumX_1 >= sumX_2) {
				sumX = val[i] + sumX_1;
				route[i] = i - 1;
			}
			else {
				sumX = val[i] + sumX_2;
				route[i] = i - 2;
			}
			sumX = val[i] + Math.max(sumX_1, sumX_2);
			sumX_2 = sumX_1;
			sumX_1 = sumX;
		}
		
		out.println(sumX);
		
		int step = n;
		while(true) {
			int prevStep = step;
			step = route[step];
			route[prevStep] = -1;
			if(step == 0) {
				break;
			}
		}
		
		for(int i = 1; i <= n; i++) {
			if(route[i] == -1) {
				out.print(i + " ");
			}
		}
		
	}

	public static void main(String[] args) {
		new Stairs2().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
