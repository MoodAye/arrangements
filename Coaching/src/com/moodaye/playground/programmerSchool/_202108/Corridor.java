package com.moodaye.playground.programmerSchool._202108;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

// Problem  2:57pm 3:07 = 10min
public class Corridor {
	void solve(Scanner in, PrintWriter out) {
		int w = in.nextInt();
		int len = in.nextInt();
		
		// nWays = nWays_1 + nWays_w
		// nWays = 1 when x < w
		// nWays = 2 when x = w
		// when x = w + 1; nWays = 2 + 1
		
		long[] nWays = new long[50 + 1];
		for(int i = 1; i < w; i++) {
			nWays[i] = 1;
		}
		nWays[w] = 2;
		
		for(int i = w + 1; i <= len; i++) {
			nWays[i] = nWays[i - 1] +nWays[i - w];
		}
		
		out.println(nWays[len]);
	}

	public static void main(String[] args) {
		new Corridor().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}

}
