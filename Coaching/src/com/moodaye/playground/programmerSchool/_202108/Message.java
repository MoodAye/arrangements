package com.moodaye.playground.programmerSchool._202108;

import java.io.PrintWriter;
import java.math.BigInteger;
import java.util.Locale;
import java.util.Scanner;

// Problem 
public class Message {
	void solve(Scanner in, PrintWriter out) {
		char[] cn = in.next().toCharArray();
		// 1025
		// cnt(1) = 1
		// cnt(10) = cnt(1) + cnt("10") = 2
		// cnt(102) = cnt(10) + cnt(1) + "02" = 2
		// cnt(1025) - cnt(102) + cnt(10) + "25" = 4

		// 21705
		// cnt(2) = 1
		// cnt(21) = cnt(2) + "1" + "21 = 2
		// cnt(217) = cnt(21),"7" + cnt(2),"17" = 3
		// cnt(2170) = cnt(217), " " + cnt(21),"70" = 3
		// cnt(21705) = cnt(2170),"3" + cnt(217)," 3" = 3

		// 33222
		// cnt(3) = 1
		// cnt(33) = 2
		// cnt(332) = cnt(33),"2" + cnt(3),"32" = 3
		// cnt(3322) = cnt(332),"2" + cnt(33),"22" = 5
		// cnt(33222) = 8

		BigInteger prevCnt = BigInteger.ZERO;
		BigInteger prevPrevCnt = BigInteger.ZERO;
		BigInteger cnt = BigInteger.ZERO;

		// idx = 1
		cnt = BigInteger.ONE;
		if (cn.length == 1) {
			out.println("1");
			return;
		}

		if (cn.length == 2) {
			out.println(cn[0] > 3  || cn[0] == '0' || 
					(cn[0] == '3' && cn[1] > '3') ? 1 : 2);
			return;
		}

		prevPrevCnt = BigInteger.ONE;
		prevCnt = cn[0] > '3'  || cn[0] == '0' || (cn[0] == '3' && cn[1] > '3') ? BigInteger.ONE : BigInteger.TWO;
		for(int i = 2; i < cn.length; i++) {
			if(cn[i - 1] > '3' || cn[i - 1] == '0') {
				cnt = prevCnt;
			}
			else if(cn[i - 1] == '3') {
				if(cn[i] <= '3') {
					cnt = prevCnt.add(prevPrevCnt);
				}
				else {
					cnt = prevCnt;
				}
			}
			else {
				cnt = prevCnt.add(prevPrevCnt);
			}
			prevPrevCnt = prevCnt;
			prevCnt = cnt;
		}
		out.println(cnt.toString());
	}

	public static void main(String[] args) {
		new Message().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
