package com.moodaye.playground.programmerSchool._202108;

import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Scanner;

// Problem 
public class Game2 {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int[] nums = new int[n];
		for (int i = 0; i < nums.length; i++) {
			nums[i] = in.nextInt();
		}
		
		int diff = playerWinsBy(nums, 0, n - 1, new HashMap<String, Integer>());
		
		out.println(diff > 0 ? 1 : diff < 0 ? 2 : 0);
	}
	
	public int playerWinsBy(int[] a, int start, int end, Map<String, Integer> memo) {
		if(start == end) {
			return a[start];
		}
		if(end - start == 1) {
			return Math.abs(a[start] - a[end]);
		}
		
		String key = start + "," + end;
		if(memo.containsKey(key)) {
			return memo.get(key);
		}
		
		int diff1 = a[start] - playerWinsBy(a, start + 1, end, memo);
		int diff2 = a[end] - playerWinsBy(a, start, end - 1, memo);
		memo.put(key, Math.max(diff1, diff2));
		return memo.get(key);
	}

	public static void main(String[] args) {
		new Game2().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
