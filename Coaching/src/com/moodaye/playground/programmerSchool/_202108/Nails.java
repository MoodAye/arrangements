package com.moodaye.playground.programmerSchool._202108;

import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Locale;
import java.util.Scanner;

// Problem 5:54am - 6:08am = 14min
public class Nails {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int[] d = new int[n];
		for (int i = 0; i < d.length; i++) {
			d[i] = in.nextInt();
		}
		
		Arrays.sort(d);
		
		int prevCnt = d[1] - d[0];
		if(n == 2) {
			out.println(prevCnt);
			return;
		}
		
		int prev2Cnt = prevCnt;
		int cnt = prevCnt;
		
		for(int i = 2; i < n; i++) {
			prev2Cnt = prevCnt;
			prevCnt = cnt;
			cnt = d[i] - d[i - 1] + Math.min(prevCnt, prev2Cnt);
		}
		
		out.println(cnt);
	}

	public static void main(String[] args) {
		new Nails().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
