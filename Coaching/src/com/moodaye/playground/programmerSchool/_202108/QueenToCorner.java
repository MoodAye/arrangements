package com.moodaye.playground.programmerSchool._202108;

import java.io.PrintWriter;
import java.util.HashSet;
import java.util.Locale;
import java.util.Scanner;
import java.util.Set;

// Problem 
public class QueenToCorner {
	void solve(Scanner in, PrintWriter out) {
		int qr = in.nextInt();
		int qc = in.nextInt();

		Set<Integer> winningRow = new HashSet<>();
		winningRow.add(1);
		Set<Integer> winningDiag = new HashSet<>();
		winningDiag.add(0);

		// setup concentric squares
		for (int width = 2; width < Math.max(qr, qc); width++) {
			for (int cell = 1; cell < width; cell++) {
				if (winningRow.contains(cell) || winningDiag.contains(Math.abs(cell - width))) {
					continue;
				} else {
					winningRow.add(cell);
					winningRow.add(width);
					winningDiag.add(Math.abs(width - cell));
				}
			}
		}
		if (winningRow.contains(Math.min(qr, qc)) || winningDiag.contains(Math.abs(qr - qc))) {
			// why won't this work failes on test 7???
//		if (winningRow.contains(qr) || winningDiag.contains(Math.abs(qr - qc))) {
			out.println(1);
			return;
		}
		out.println(2);
	}

	public static void main(String[] args) {
		new QueenToCorner().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
