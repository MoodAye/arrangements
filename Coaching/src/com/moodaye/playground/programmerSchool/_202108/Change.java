package com.moodaye.playground.programmerSchool._202108;

import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Locale;
import java.util.Scanner;

// Problem 925am
public class Change {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int[] nums = new int[n];
		for (int i = 0; i < nums.length; i++) {
			nums[i] = in.nextInt();
		}

		int k = in.nextInt();
		int[] amts = new int[k];

		for (int i = 0; i < amts.length; i++) {
			amts[i] = in.nextInt();
		}

		int maxAmt = Arrays.stream(amts).max().getAsInt();

		int[][] memo = new int[n][maxAmt + 1];

		for (int row = 0; row < n; row++) {
			memo[row][0] = 1;
		}

		for (int i = 0; i < n; i++) {
			int coin = nums[i];
			for (int amt = 0; amt <= maxAmt; amt++) {
				if (i > 0) {
					if (memo[i - 1][amt] == 1) {
						memo[i][amt] = 1;
					}
				}
				if (amt - coin >= 0) {
					if (memo[i][amt - coin] == 1) {
						memo[i][amt] = 1;
					}
				}
			}
		}

		for (int i = 0; i < k; i++) {
			out.print(memo[n - 1][amts[i]] + " ");
		}

	}

	public static void main(String[] args) {
		new Change().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
