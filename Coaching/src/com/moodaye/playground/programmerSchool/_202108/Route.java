package com.moodaye.playground.programmerSchool._202108;

import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Locale;
import java.util.Scanner;

// Problem 
public class Route {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int[][] vals = new int[n + 2][n + 2];
		for (int i = 1; i <= n; i++) {
			String line = in.next();
			for (int j = 1; j <= n; j++) {
				vals[i][j] = line.charAt(j - 1) - '0';
			}
		}

		//edges
		Arrays.fill(vals[0], Integer.MAX_VALUE);
		for (int i = 1; i <= n; i++) {
			vals[i][0] = Integer.MAX_VALUE;
		}
		
		char[][] path = new char[n + 2][n + 2];
		
		for (int i = 1; i <= n; i++) {
			for (int j = 1; j <= n; j++) {
				
				if(i == 1 && j == 1) {
					continue;
				}
				
				if(vals[i - 1][j] < vals[i][j - 1]) {
					path[i][j] = 'T';
					vals[i][j] += vals[i - 1][j];
				}
				else {
					path[i][j] = 'L';
					vals[i][j] += vals[i][j - 1];
				}
			}
		}
		
		int nextr = n;
		int nextc = n;
		
		while(!(nextr == 1 && nextc == 1)) {
				if(path[nextr][nextc] == 'L'){
					path[nextr][nextc] = '#';
					nextc--;
				}
				else {
					path[nextr][nextc] = '#';
					nextr--;
				}
		}
		path[1][1] = '#';
		
		for (int i = 1; i <= n; i++) {
			for (int j = 1; j <= n; j++) {
				out.print(path[i][j] == '#' ? '#' : '.');
			}
			out.println();
		}
	}

	public static void main(String[] args) {
		new Route().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
