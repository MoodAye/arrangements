package com.moodaye.playground.programmerSchool._202108;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;

// Problem 634am - 640am = 6min
public class Sequence3 {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		
		TreeSet<Long> set = new TreeSet<>();
		
		set.add(1L);
		long next = 0L;
		
		for(int i = 1; i <= n; i++) {
			next = set.first();
			set.add(next * 2);
			set.add(next * 3);
			set.add(next * 5);
			set.remove(next);
		}
		
		out.println(next);
	}

	public static void main(String[] args) {
		new Sequence3().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
