package com.moodaye.playground.programmerSchool._202108;

import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Locale;
import java.util.Scanner;

// Problem 641am - 652am 11min
public class WordChain {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		String[] word = new String[n];
		for (int i = 0; i < word.length; i++) {
			word[i] = in.next();
		}
		
		Arrays.sort(word);
		
		int[] maxLens = new int[n];
		Arrays.fill(maxLens, 1);
		
		for(int i = 1; i < n; i++) {
			for(int j = i - 1; j >= 0; j--) {
				if(word[i].startsWith(word[j]) && !word[i].equals(word[j])) {
					maxLens[i] = Math.max(maxLens[i], maxLens[j] + 1);
				}
			}
		}
		out.println(Arrays.stream(maxLens).max().getAsInt());
	}

	public static void main(String[] args) {
		new WordChain().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}

}
