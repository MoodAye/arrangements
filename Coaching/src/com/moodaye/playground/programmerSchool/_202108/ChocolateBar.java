package com.moodaye.playground.programmerSchool._202108;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

// Problem  107am - 1:18am
public class ChocolateBar {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		
		if(n % 2 == 1) {
			out.println(0);
			return;
		}
		
		if(n == 2) {
			out.println(3);
			return;
		}
		
		long prevCnt = 11L;
		long cumuCnt = 1L + 3L;
		long cnt = 11L;
		
		for(int i = 6; i <= n; i += 2) {
			cnt = prevCnt * 3 + cumuCnt * 2;
			cumuCnt += prevCnt;
			prevCnt = cnt;
		}
		out.println(cnt);
	}

	public static void main(String[] args) {
		new ChocolateBar().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
