package com.moodaye.playground.programmerSchool._202108;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

// Problem  156pm - 209 = 13min
public class ComputerGame {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int[] nums = new int[n];
		for (int i = 0; i < nums.length; i++) {
			nums[i] = in.nextInt();
		}
		
		if(n == 1) {
			out.println(0);
			return;
		}
		
		if(n == 2) {
			out.println(Math.abs(nums[1] - nums[0]));
			return;
		}
		
		int min_2 = 0;
		int min_1 = 0;
		int min = Math.abs(nums[1] - nums[0]);
		
		for(int i = 2; i < n; i++) {
			min_2 = min_1;   // 4
			min_1 = min; //4
			min = Math.min(min_2 + 3 * Math.abs(nums[i] - nums[i - 2]), min_1 + Math.abs(nums[i] - nums[i - 1]));
		}
		out.println(min);
	}

	public static void main(String[] args) {
		new ComputerGame().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
