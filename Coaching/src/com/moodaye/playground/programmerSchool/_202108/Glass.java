package com.moodaye.playground.programmerSchool._202108;

import java.io.PrintWriter;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.Locale;
import java.util.Scanner;

// Problem  10:58am
public class Glass {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();

		BigInteger[] ways = new BigInteger[n >= 20 ? n + 1 : 20];

		if (n <= 9) {
			out.println(0);
			return;
		}
		if (n <= 12) {
			out.println(2);
			return;
		}
		
		Arrays.fill(ways, 0, 10, BigInteger.ZERO);
		Arrays.fill(ways, 10, 13, BigInteger.TWO);
		Arrays.fill(ways, 13, 20, BigInteger.ZERO);

		for (int i = 20; i <= n; i++) {
			// 12 = UUU; DDD
			// 11 = UU; DD
			// 10 = U; D
			ways[i] = ways[i - 12].add(ways[i - 11]).add(ways[i - 10]).mod(BigInteger.valueOf(1_000_000L));
		}
		
		out.println(ways[n].toString());
	}

	public static void main(String[] args) {
		new Glass().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
