package com.moodaye.playground.programmerSchool._202108;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

// Problem  849am
public class Route2 {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int k = in.nextInt();
		int[][] nums = new int[n + 2][n + 2];
		for (int i = 1; i <= n; i++) {
			for (int j = 1; j <= n; j++) {
				nums[i][j] = in.nextInt();
			}
		}

		int[][] step = new int[n + 2][n + 2];
		int[][] sum = new int[n + 2][n + 2];

		int counter = 1;
		step[1][1] = 1;
		sum[1][1] = nums[1][1];
		while (--k > 0) {

			for (int i = 1; i <= n; i++) {
				for (int j = 1; j <= n; j++) {
					if (step[i][j] == counter) {

						if (i != 1) {
							sum[i - 1][j] = Math.max(sum[i - 1][j], nums[i - 1][j] + sum[i][j]);
							step[i - 1][j] = counter + 1;
						}

						if (i != n) {
							sum[i + 1][j] = Math.max(sum[i + 1][j], nums[i + 1][j] + sum[i][j]);
							step[i + 1][j] = counter + 1;
						}

						if (j != 1) {
							sum[i][j - 1] = Math.max(sum[i][j - 1], nums[i][j - 1] + sum[i][j]);
							step[i][j - 1] = counter + 1;
						}

						if (j != n) {
							sum[i][j + 1] = Math.max(sum[i][j + 1], nums[i][j + 1] + sum[i][j]);
							step[i][j + 1] = counter + 1;
						}
					}
				}
			}
			counter++;
			resetPrevStepSums(counter, step, sum);
		}

		out.println(max(sum));
	}

	private int max(int[][] sum) {
		int max = 0;
		int n = sum.length - 2;
		for (int i = 1; i <= n; i++) {
			for (int j = 1; j <= n; j++) {
				max = Math.max(max, sum[i][j]);
			}
		}
		return max;
	}

	private void resetPrevStepSums(int counter, int[][] step, int[][] sum) {
		int n = sum.length - 2;
		for (int i = 1; i <= n; i++) {
			for (int j = 1; j <= n; j++) {
				if (step[i][j] != counter) {
					sum[i][j] = 0;
				}
			}
		}
	}

	public static void main(String[] args) {
		new Route2().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
