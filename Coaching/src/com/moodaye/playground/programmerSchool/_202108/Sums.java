package com.moodaye.playground.programmerSchool._202108;

import java.io.PrintWriter;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Locale;
import java.util.Scanner;
import java.util.Set;

// Problem 
public class Sums {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		// 500 * 100 = max sum = 50_000 : array of this size = 50_000 * 32 = 5_000_000 / 3 = 2Mb
		int[] nums = new int[n];
		for (int i = 0; i < nums.length; i++) {
			nums[i] = in.nextInt();
		}
		
		// 3 4 1:   3, 4, 1, 0, 7 , 4, 5 , 8
		
		//	     [0,1,2  3,4,5  6,7,8  9,          ]	
		// start [1,0,0, 0,0,0, 0,0,0, 0,0,0, 0,0,0]
		// (0)=3 [1,0,0, 1,0,0, 0,0,0, 0,0,0, 0,0,0]
		// (1)=4 [1,0,0, 1,1,0, 0,1,0, 0,0,0, 0,0,0]
		// (2)=1 [1,1,0, 1,1,1, 0,1,1, 0,0,0, 0,0,0]
		
		int[] sums = new int[500 * 100 + 1];
		sums[0] = 1;
		for (int i = 0; i < nums.length; i++) {
			doSum(nums, i, sums);
		}
		out.println(Arrays.stream(sums).filter((a) -> a == 1).count());
	}
	
	private void doSum(int[] nums, int idx, int[] sums) {
		for(int i = sums.length - 1; i >= 0; i--) {
			if(sums[i] == 1) {
				sums[i + nums[idx]] = 1;
			}
		}
	}
	
	
	
	public static void main(String[] args) {
		new Sums().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
