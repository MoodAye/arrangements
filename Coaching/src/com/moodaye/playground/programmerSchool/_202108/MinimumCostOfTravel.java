package com.moodaye.playground.programmerSchool._202108;

import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Locale;
import java.util.Scanner;

// Problem  ~20min
public class MinimumCostOfTravel {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int[][] costs = new int[n + 1][n + 1];
		for (int i = 0; i <= n; i++) {
			for(int j = i + 1; j <= n; j++) {
				costs[i][j] = in.nextInt();
			}
		}
		
		//min cost to get to station x from station i starting from station zero is
		// min of cost to get to station i from station zero 
		// plus min cost to get from i to x.
		
		// minCost[i] depicts min cost to get from station zero to station i
		int[] minCost = new int[n + 1];
		for(int i = 1; i <= n; i++) {
			minCost[i] = costs[0][i];
		}
		
		
		for(int i = 1; i <= n; i++) {
			for(int j = i - 1; j >= 0; j--) {
				minCost[i] = Math.min(minCost[i], minCost[j] + costs[j][i]);
			}
		}
		
		out.println(minCost[n]);
	}

	public static void main(String[] args) {
		new MinimumCostOfTravel().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
