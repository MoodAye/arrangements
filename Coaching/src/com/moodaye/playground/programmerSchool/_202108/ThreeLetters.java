package com.moodaye.playground.programmerSchool._202108;

import java.io.PrintWriter;
import java.math.BigInteger;
import java.util.Locale;
import java.util.Scanner;

// Problem  Start = 1241pm
public class ThreeLetters {
	void solve(Scanner in, PrintWriter out) {
		if (in.hasNext()) {
			String nxt = in.next();
				if(nxt == null) {
					out.println(0);
					return;
				}
			char[] letters = nxt.toCharArray();

			// given c - number of abc = number of (ab)
			// given b - number of ab = number of a
			// so we count a, ab, and then abc.

			BigInteger cntA = BigInteger.ZERO;
			BigInteger cntAb = BigInteger.ZERO;
			BigInteger cntAbc = BigInteger.ZERO;

			for (int i = 0; i < letters.length; i++) {
				if (letters[i] == 'a') {
					cntA = cntA.add(BigInteger.ONE);
				} else if (letters[i] == 'b') {
					cntAb = cntAb.add(cntA);
				} else if(letters[i] == 'c'){
					cntAbc = cntAbc.add(cntAb);
				}
			}
			out.println(cntAbc);
		} else {
			out.println(0);
		}
	}

	public static void main(String[] args) {
		new ThreeLetters().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
