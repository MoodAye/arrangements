package com.moodaye.playground.programmerSchool._202108;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

// Problem  15min
public class Farmer {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int[][] nums = new int[n + 1][n + 1];
		for (int i = 1; i <= n; i++) {
			char[] line = in.next().toCharArray();
			for (int j = 1; j <= n; j++) {
				nums[i][j] = line[j - 1] - '0';
			}
		}
		
		int[][] maxLen = new int[n + 1][n + 1];
		int maxSide = 0;
		
		for (int i = 1; i <= n; i++) {
			for (int j = 1; j <= n; j++) {
				if(nums[i][j] == 0) {
					continue;
				}
				maxLen[i][j] = Math.min(maxLen[i - 1][j - 1], Math.min(maxLen[i][j -1], maxLen[i - 1][j])) + 1;
				maxSide = Math.max(maxLen[i][j], maxSide);
			}
		}
		
		out.println(maxSide * maxSide);
	}

	public static void main(String[] args) {
		new Farmer().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
