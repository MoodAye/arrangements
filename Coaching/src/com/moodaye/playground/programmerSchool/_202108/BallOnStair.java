package com.moodaye.playground.programmerSchool._202108;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

// Problem 544
public class BallOnStair {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();

		long step_minus_3 = 0;
		long step_minus_2 = 0;
		long step_minus_1 = 0;
		long stepCnt = 0;

		// step 1
		step_minus_3 = 0;
		step_minus_2 = 0;
		step_minus_1 = 1;
		stepCnt = 1;
		
		if(n == 1) {
			out.println(1);
			return;
		}

		// step 2
		step_minus_3 = 0;
		step_minus_2 = 1;
		step_minus_1 = 1;
		stepCnt = 2;
		
		if(n == 2) {
			out.println(2);
			return;
		}

		// step 3
		for (int step = n - 2; step >= 1; step--) {
			step_minus_3 = step_minus_2;
			step_minus_2 = step_minus_1;
			step_minus_1 = stepCnt;
			stepCnt = step_minus_1 + step_minus_2 + step_minus_3;
		}
		
		out.printf("%d%n", stepCnt);

	}

	public static void main(String[] args) {
		new BallOnStair().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
