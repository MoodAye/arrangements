package com.moodaye.playground.programmerSchool._202108;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

// Problem  ~60min
public class AlmostPalindrome {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int k = in.nextInt();
		String word = in.next();
		char[] cword = word.toCharArray();
		
		int cnt = 0;
		for (int i = 0; i <= n; i++) {
			
			int k1 = k;
			for (int startIdx = i, endIdx = i + 1; startIdx >= 0 && endIdx <= n; startIdx--, endIdx++) {
				
				if(cword[startIdx] != cword[endIdx - 1]) {
					if(k1 == 0) {
						break;
					}
					k1--;
				}
				cnt++;
				
//				out.println(word.substring(startIdx, endIdx));
			}
		}
		
//		out.println("000000000000000000000000000000000000");
		for (int i = 0; i <= n; i++) {
			int k2 = k;
			for (int startIdx = i, endIdx = i + 2; startIdx >= 0 && endIdx <= n; startIdx--, endIdx++) {
				
				if(cword[startIdx] != cword[endIdx - 1]) {
					if(k2 == 0) {
						break;
					}
					k2--;
				}
				cnt++;
//				out.println(word.substring(startIdx, endIdx));
			}
		}
		out.println(cnt);
	}

	public static void main(String[] args) {
		new AlmostPalindrome().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
