package com.moodaye.playground.programmerSchool._202108;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

// Problem  229am - 233am (4min)
public class TwoDigits {
void solve(Scanner in, PrintWriter out) {
	int n = in.nextInt();
	// keep doubling - but if you have 2 equal digits - then only add
	int cnt = 2;
	int cntTwoEqualDigits = 0;
	
	for(int i = 2; i <= n; i++) {
		int p = cnt - cntTwoEqualDigits;
		cnt = 2 * cnt - cntTwoEqualDigits;
		cntTwoEqualDigits = p;
	}
	
	out.println(cnt);
	
}

public static void main(String[] args) {
	new TwoDigits().run();
}

void run() {
	Locale.setDefault(Locale.US);
	try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
		solve(in, out);
	}
}
}
