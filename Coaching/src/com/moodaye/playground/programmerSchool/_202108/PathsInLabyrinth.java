package com.moodaye.playground.programmerSchool._202108;

import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Locale;
import java.util.Scanner;

// Problem  ~20min
public class PathsInLabyrinth {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int k = in.nextInt();
		int[][] laby = new int[n + 2][n + 2];
		for (int i = 1; i <= n; i++) {
			String line = in.next();
			for (int j = 1; j <= n; j++) {
				laby[i][j] = line.charAt(j - 1) - '0';
			}
		}

		// edges
		Arrays.fill(laby[0], 1);
		Arrays.fill(laby[n + 1], 1);
		for (int i = 0; i < n + 2; i++) {
			laby[i][0] = 1;
			laby[i][n + 1] = 1;
		}

		int[][] cnts = new int[n + 2][n + 2];
		cnts[1][1] = 1;
		for (int step = 1; step <= k; step++) {
			int[][] temp = new int[n + 2][n + 2];
			for (int i = 1; i <= n; i++) {
				for (int j = 1; j <= n; j++) {
					if (cnts[i][j] != 0) {
						if (laby[i - 1][j] == 0) {
							temp[i - 1][j] += cnts[i][j];
						}
						if (laby[i + 1][j] == 0) {
							temp[i + 1][j] += cnts[i][j];
						}
						if (laby[i][j - 1] == 0) {
							temp[i][j - 1] += cnts[i][j];
						}
						if (laby[i][j + 1] == 0) {
							temp[i][j + 1] += cnts[i][j];
						}
					}
				}
			}
			cnts = temp;
		}
		out.println(cnts[n][n]);
	}

	public static void main(String[] args) {
		new PathsInLabyrinth().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
