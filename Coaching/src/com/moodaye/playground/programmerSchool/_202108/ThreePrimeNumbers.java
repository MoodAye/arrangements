package com.moodaye.playground.programmerSchool._202108;

import java.io.PrintWriter;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Scanner;

/*
 * 1. find 3 digit primes
 * 2. find count of 2 digit endings - skip endings starting with 0
 * 3. add odd number to each 2 digit ending and see if prime. if prime 
 * 	   - add count to 2 digit ending - might need to create a new data structure here
 */
// Problem 1015am
public class ThreePrimeNumbers {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();

		boolean tdp[] = threeDigitPrimes();
		
		if(n == 3) {
			long cnt = 0L;
			for(int i = 101; i < 998; i++) {
				if(tdp[i]) {
					cnt++;
				}
			}
			out.println(cnt);
			return;
		}
		
		
		Map<Integer, Long> cnts = twoDigitCnts(tdp);

		for (int i = 4; i <= n; i++) {
			cnts = twoDigitCnts(cnts, tdp);
		}

		long cnt = 0;
		for (int key : cnts.keySet()) {
			cnt = (cnt + cnts.get(key)) % 1_000_000_009L;
		}

		out.println(cnt);
	}

	public Map<Integer, Long> twoDigitCnts(boolean[] tdp) {
		Map<Integer, Long> cnts = new HashMap<Integer, Long>();

		for (int i = 100; i < 1000; i++) {
			if (tdp[i]) {
				int twoDigits = i % 100;
				if (twoDigits < 10) {
					continue;
				}
				cnts.merge(twoDigits, 1L, (a, b) -> {
					return (a + b) % 1_000_009L;
				});
			}
		}
		return cnts;
	}

	public Map<Integer, Long> twoDigitCnts(Map<Integer, Long> oldCnts, boolean[] tdp) {
		Map<Integer, Long> cnts = new HashMap<Integer, Long>();

		for (int twoDigit : oldCnts.keySet()) {
			for (int lastDigit = 1; lastDigit <= 9; lastDigit += 2) {
				int newThreeDigit = twoDigit * 10 + lastDigit;
					if (tdp[newThreeDigit]) {
						long twoDigitCnt = oldCnts.get(twoDigit);
						int newTwoDigit = newThreeDigit % 100;
						cnts.merge(newTwoDigit, twoDigitCnt, (a, b) -> {
							return (a + b) % 1_000_000_009L;
						});
					}
			}
		}
		return cnts;
	}

	public boolean[] threeDigitPrimes() {
		boolean[] tdp = new boolean[1000];
		Arrays.fill(tdp, true);
		tdp[0] = false;
		tdp[1] = false;
		tdp[2] = true;
		for (int i = 2; i < 1000; i++) {
			if (tdp[i]) {
				for (int j = i * 2; j < 1000 && i * i <= 1000; j += i) {
					tdp[j] = false;
				}
			}
		}
		return tdp;
	}

	public static void main(String[] args) {
		new ThreePrimeNumbers().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
