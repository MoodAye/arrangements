package com.moodaye.playground.programmerSchool.November2018;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #454 */
// Time taken ~1.5 hrs.  took about 1 hr to figure out the pattern
public class RemainingNumber {
	void solve(Scanner in, PrintWriter out) {
		int first = in.nextInt();
		int last = in.nextInt();
		
		out.println(remNum(first, last));
	}

	/*if series starts from 1, then
	 * iteration 1:  prevNumber +  n * 2; ---> 2,4,6,8...  where start = 2
	 * iteration 2:  prevNumber + n * 4:  ---> 2,6,10,.... where start = 2
	 * iteration 3:  prevNumber + n * 8:  ---> 6,14,22,....where start = 6
	 * iteration 4:  prevNumber + n * 16: ----> 6,22,38,...where start = 6
	 * iteration 5:  prevNumber + n * 32: ----> 22,38,...where start = 22
	 * iteration 6:  prevNumber + n * 64: ----> 22,86...where start = 22
	 * iteration 8:  prevNumber + n * 128: ----> 86, 86+128......where start = 86
	 * 
	 * So the rule is: Every 2nd iteration the start number shifts to the second number in the prev. seq.
	 * And the sequence progresses by 2^iterationNumber
	 *  */
	public static int remNum(int first, int last){
		int len = last - first + 1;
		int startNumber = 2;
		int iter = 1;
		int jump = 2;
		while(true){
			iter++;
			if(iter % 2 == 1){
				if(startNumber + jump > len){
					break;
				}
				startNumber += jump;
			}
			jump *= 2;
		}
		return first + startNumber - 1;
	}

	public static void main(String args[]) {
		new RemainingNumber().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}

}
