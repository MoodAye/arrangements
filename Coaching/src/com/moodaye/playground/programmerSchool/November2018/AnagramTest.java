package com.moodaye.playground.programmerSchool.November2018;

import static org.junit.Assert.*;
import static com.moodaye.playground.programmerSchool.November2018.Anagram.*;

import org.junit.AfterClass;
import org.junit.Test;

public class AnagramTest {

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Test
	public void test() {
		assertEquals("acbd", anagram("abdc"));
		assertEquals("wrdo", anagram("word"));
		assertEquals("xw", anagram("wx"));
		assertEquals("priro", anagram("prior"));
		assertEquals("alesrt", anagram("alerts"));
		assertEquals("onyl", anagram("only"));
		assertEquals("impatc", anagram("impact"));
		assertEquals("uelr", anagram("rule"));
	}

}
