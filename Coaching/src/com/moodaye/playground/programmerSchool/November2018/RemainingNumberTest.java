package com.moodaye.playground.programmerSchool.November2018;

import static org.junit.Assert.*;

import org.junit.AfterClass;
import org.junit.Test;
import static com.moodaye.playground.programmerSchool.November2018.RemainingNumber.*;
public class RemainingNumberTest {

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Test
	public void test() {
		assertEquals(2, remNum(1,4));
		assertEquals(7, remNum(2,9));
		for(int i = 2; i < 6; i++){
			assertEquals(2, remNum(1, i));
		}
		for(int i = 6; i < 22; i++){
			assertEquals(6, remNum(1, i));
		}
		for(int i = 22; i < 86; i++){
			assertEquals(22, remNum(1, i));
		}
	}

}
