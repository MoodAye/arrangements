package com.moodaye.playground.programmerSchool.November2018;

import static org.junit.Assert.*;

import org.junit.AfterClass;
import org.junit.Test;
import static com.moodaye.playground.programmerSchool.November2018.Chess2.*;

public class Chess2Test {

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Test
	public void test() {
		assertEquals(1, getCnt(1,1));
		assertEquals(4, getCnt(1,2));
		assertEquals(2, getCnt(2,2));
		assertEquals(40320, getCnt(8,8));
	}

}
