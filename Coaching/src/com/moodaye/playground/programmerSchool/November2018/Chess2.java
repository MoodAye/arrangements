package com.moodaye.playground.programmerSchool.November2018;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #156 */
/* Using Backtracking */
// Time taken ~1hr
public class Chess2 {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int k = in.nextInt();
		out.println(getCnt(k, n));
	}
	
	public static int getCnt(int k, int n){
		return getCnt(new boolean[n][n], -1, 1, k, n);
	}
	
	
	private static int getCnt(boolean[][] b, int col, int cn, int k, int n){
		int cnt = 0;
		for(int c = col + 1; c < n; c++){
			for(int r = 0; r < n; r++){
				if(mark(b, c, r, n)){
					if(cn == k) cnt++;
					else cnt += getCnt(b, c, cn + 1, k, n);
					b[r][c] = false;
				}
				else{
					continue;
				}
			}
		}
		return cnt;
	}
	
	private static boolean mark(boolean[][] b, int c, int r, int n){
		for(int i = 0; i < n; i++){
			if(b[r][i] || b[i][c]){
				return false;
			}
		}
		b[r][c] = true;
		return true;
	}
	
	public static void main(String args[]) {
		new Chess2().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
