package com.moodaye.playground.programmerSchool.November2018;

import java.util.Scanner;
import java.util.Arrays;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #72 */
// ~60min
public class Anagram {
	void solve(Scanner in, PrintWriter out) {
		String word = in.next();
		out.println(anagram(word));
	}
	
	public static String anagram(String word){
		return anagram(word.toCharArray(), word.length() - 2);
	}
	
	private static String anagram(char[] word, int idx){
		char[] sub = Arrays.copyOfRange(word, idx + 1, word.length);
		Arrays.sort(sub);
		for(int i = 0; i < sub.length; i++){
			if(sub[i] > word[idx]){
				char temp = word[idx];
				word[idx] = sub[i];
				sub[i] = temp;
				Arrays.sort(sub);
				for(int k = 0; k < sub.length; k++){
					word[idx + k + 1] = sub[k];
				}
				return String.valueOf(word);
			}
		}
		return anagram(word, idx - 1);
	}

	public static void main(String args[]) {
		new Anagram().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
