package com.moodaye.playground.programmerSchool.June2018;

import java.util.Locale;

import javax.swing.text.StyleContext.SmallAttributeSet;

import java.io.InputStream;
import java.io.PrintWriter;

/** Programmer's School #360 */
// Time Taken ~ 40 min
// 
//   right,up    right,right
//   - - - - -   - - - - -
//   - - - y -   - - - - -
//   - - y y -   - - y y y
//   - - - - -   - - - - -
//   - - - - -   - - - - -
//
//   right,down
//   - - - - -   
//   - - - - -  
//   - - y y -  
//   - - - y -   
//   - - - - -   
//
// 	 down,down	 down,right	   up,right
//   - - - - -   - - - - -     - - - - -   
//   - - - - -   - - - - -     - - y y -   
//   - - y - -   - - y - -     - - y - -   
//   - - y - -   - - y y -     - - - - -   
//   - - y - -   - - - - -     - - - - -   

//  Max 6 options for each element.  max elements = 16e6 
//  max operations ~ 6 x 16e6 ~ 1e8 operations.  We have 3 seconds to solve
//  Max size of array = 4 x 4e6 bytes = 16e6 bytes = 16Mb.  
//  Should we use small int?
// TLE on test 19 - fixed by using FastScanner.
// MLE on test 20 -- anticipated this.   Fixed by using byte data type for the array

public class MaximumTriple {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		byte[][] table = new byte[n][n];
		for(int row = 0; row < n; row++){
			for(int col = 0; col < n; col++){
				table[row][col] = (byte) in.nextInt();
			}
		}
		
		int max = Integer.MIN_VALUE;
		
		for(int row = 0; row < n; row++){
			for(int col = 0; col < n; col++){
				// up,right;  right,up
				if(col + 1 < n && row - 1 >= 0){
					max = Math.max(max, table[row][col] + table[row - 1][col] + table[row - 1][col + 1]);
					max = Math.max(max, table[row][col] + table[row][col + 1] + table[row - 1][col + 1]);
				}
				
				// right, right
				if(col + 2 < n){
					max = Math.max(max, table[row][col] + table[row][col + 1] + table[row][col + 2]);
				}
				
				// right,down; down,right
				if(col + 1 < n && row + 1 < n){
					max = Math.max(max, table[row][col] + table[row][col + 1] + table[row + 1][col + 1]);
					max = Math.max(max, table[row][col] + table[row + 1][col] + table[row + 1][col + 1]);
				}
				
				// down, down
				if(row + 2 < n){
					max = Math.max(max, table[row][col] + table[row + 1][col] + table[row + 2][col]);
				}
			}
		}
		out.println(max);
	}

	public static void main(String args[]) {
		new MaximumTriple().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}

static class Scanner implements AutoCloseable {

    InputStream is;
    byte buffer[] = new byte[1 << 16];
    int size = 0;
    int pos = 0;

    Scanner(InputStream is) {
        this.is = is;
    }

    int nextChar() {
        if (pos >= size) {
            try {
                size = is.read(buffer);
            } catch (java.io.IOException e) {
                throw new java.io.IOError(e);
            }
            pos = 0;
            if (size == -1) {
                return -1;
            }
        }
        Assert.check(pos < size);
        int c = buffer[pos] & 0xFF;
        pos++;
        return c;
    }

    int nextInt() {
        int c = nextChar();
        while (c == ' ' || c == '\r' || c == '\n' || c == '\t') {
            c = nextChar();
        }
        if (c == '-') {
            c = nextChar();
            Assert.check('0' <= c && c <= '9');
            int n = -(c - '0');
            c = nextChar();
            while ('0' <= c && c <= '9') {
                int d = c - '0';
                c = nextChar();
                Assert.check(n > Integer.MIN_VALUE / 10 || n == Integer.MIN_VALUE / 10 && d <= -(Integer.MIN_VALUE % 10));
                n = n * 10 - d;
            }
            return n;
        } else {
            Assert.check('0' <= c && c <= '9');
            int n = c - '0';
            c = nextChar();
            while ('0' <= c && c <= '9') {
                int d = c - '0';
                c = nextChar();
                Assert.check(n < Integer.MAX_VALUE / 10 || n == Integer.MAX_VALUE / 10 && d <= Integer.MAX_VALUE % 10);
                n = n * 10 + d;
            }
            return n;
        }
    }

    @Override
    public void close() {
    }
}

static class Assert {
    static void check(boolean e) {
        if (!e) {
            throw new AssertionError();
        }
    }
}
}