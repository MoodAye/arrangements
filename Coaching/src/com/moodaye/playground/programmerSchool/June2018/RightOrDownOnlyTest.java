package com.moodaye.playground.programmerSchool.June2018;

import static org.junit.Assert.*;
import static com.moodaye.playground.programmerSchool.June2018.RightOrDownOnly.*;

import org.junit.AfterClass;
import org.junit.Test;

public class RightOrDownOnlyTest {

	@Test
	public void test() {
		Cell[][] field = new Cell[70][70];
		for (int i = 0; i < 70; i++){
			for(int j = 0; j < 70; j++){
				Cell cell = new Cell();
				cell.x = i;
				cell.y = j;
				cell.wt = 1;
				field[i][j] = cell;
			}
		}
		
		assertEquals(1, pathCount(field));
	}

}
