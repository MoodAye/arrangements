package com.moodaye.playground.programmerSchool.June2018;

import java.util.Scanner;
import java.util.Stack;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #165 */
// Start Time = 12:30pm First submission 1:00pm
public class RightOrDownOnly {
	void solve(Scanner in, PrintWriter out) {
		int rows = in.nextInt();
		int cols = in.nextInt();
		int[][] field = new int[rows][cols];
		for(int i = 0; i < rows; i++){
			for(int j = 0; j < cols; j++){
				field[i][j] = in.nextInt();
			}
		}
		
		int[][] cnt = new int[rows][cols];
		cnt[0][0] = 1;
		for(int i = 0; i < rows; i++){
			for(int j = 0; j < cols && !(i == rows - 1 && j == cols - 1); j++){
				if((i == rows - 1 && j == cols - 1)){
					break;
				}
				if(field[i][j] + i < rows){
					cnt[i + field[i][j]][j] += cnt[i][j];
				}
				if(field[i][j] + j < cols){
					cnt[i][j + field[i][j]] += cnt[i][j];
				}
			}
		}
		
		out.println(cnt[rows - 1][cols - 1]);
	}

	// this approach resulted in tle
	public static int pathCount(Cell[][] field){
		int n = field.length;
		int m = field[0].length;
		
		Stack<Cell> stack = new Stack<>();
		stack.add(field[0][0]);
		int cnt = 0;
		
		while(stack.size() != 0){
			Cell nextCell = stack.pop();
			if(nextCell.x == n - 1 && nextCell.y == m - 1){
				cnt++;
				continue;
			}
			if(nextCell.wt == 0){
				continue;
			}
			if(nextCell.x + nextCell.wt < n){
				stack.push(field[nextCell.x + nextCell.wt][nextCell.y]);
			}
			if(nextCell.y + nextCell.wt < m){
				stack.push(field[nextCell.x][nextCell.y + nextCell.wt]);
			}
		}
		return cnt;
	}

	public static void main(String args[]) {
		new RightOrDownOnly().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
	
	public static class Cell{
		int x;
		int y;
		int wt;
	}
}
