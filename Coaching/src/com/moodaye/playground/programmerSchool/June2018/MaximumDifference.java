package com.moodaye.playground.programmerSchool.June2018;

import java.util.Scanner;
import java.util.Arrays;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #32 */
public class MaximumDifference {

	void solve(Scanner in, PrintWriter out) {
		int a = in.nextInt();
		int b = in.nextInt();
		
		int[] aArr = toArray(a);
		int[] bArr = toArray(b);
		
	
		Arrays.sort(aArr);
		if(a > 0){
			reverse(aArr);
		}
		moveZero(aArr);
				
		Arrays.sort(bArr);
		if(b < 0){
			reverse(bArr);
		}
		moveZero(bArr);
		
		out.println(toInt(aArr) * (a < 0 ? -1 : 1) - toInt(bArr) * (b < 0 ? -1 : 1));
	}
	
	public static int[] toArray(int a){
		if(a == 0){
			int[] aArr = {0};
			return aArr;
		}
		a = a < 0 ? a * -1 : a;
		
		int temp = a;
		int len = 0;
		
		while(temp != 0){
			temp /= 10;
			len++;
		}
		
		int[] aArr = new int[len];
	
		int idx = len - 1;
		while(a != 0){
			aArr[idx--] = a % 10;
			a /= 10;
		}
		return aArr;
	}
	
	public static int toInt(int[] aArr){
		int mult = 1;
		int a = 0;
		for(int i = aArr.length - 1; i >= 0; i--){
			a += aArr[i] * mult;
			mult *= 10;
		}
		return a;
	}
	
	public static void moveZero(int[] a){
		if(a[0] == 0 && a.length > 1){
			int idx = 1;
			while(a[idx] == 0){
				idx++;
			}
			int temp = a[0];
			a[0] = a[idx];
			a[idx] = temp;
		}
	}
	
	public static void reverse(int[] a){
		for(int i = 0; i < a.length / 2; i++){
			int temp = a[i];
			a[i] = a[a.length - i - 1];
			a[a.length - i - 1] = temp;
		}
	}

	public static void main(String args[]) {
		new MaximumDifference().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
