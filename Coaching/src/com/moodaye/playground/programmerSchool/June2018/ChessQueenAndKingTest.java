package com.moodaye.playground.programmerSchool.June2018;

import static org.junit.Assert.*;

import org.junit.AfterClass;
import org.junit.Test;

import static com.moodaye.playground.programmerSchool.June2018.ChessQueenAndKing.*;
public class ChessQueenAndKingTest {

	@Test
	public void test() {
		// Same Row King blocking ( bk wk wq )
		assertEquals("NO", check("c4", "d4", "b4"));
		// Same Row King blocking ( wq wk bk )
		assertEquals("NO", check("c4", "b4", "d4"));
		// Same Row King not blocking: ( bk wq wk )
		assertEquals("YES", check("d4", "c4", "b4"));
		// Same Row King not blocking: ( wk wq bk )
		assertEquals("YES", check("b4", "c4", "d4"));
		// Same Row King not blocking: ( wq bk wk )
		assertEquals("YES", check("d4", "b4", "c4"));
		// Same Col King blocking: b5 b6 b4  
		assertEquals("NO", check("b5", "b6", "b4"));
		// Same Col King not blocking: b6 b5 b4
		assertEquals("YES", check("b6", "b5", "b4"));
		// Same Diag King blocking: c4 d5 b6
		assertEquals("NO", check("c4", "d5", "b6"));
		// Same Diag King not blocking: d4 c6 b5	
		assertEquals("YES", check("d4", "c6", "b5"));
	
		// c4 e6 d3
		assertEquals("YES", check("c3", "b4", "d6"));
		
		// c1 b2 d4
		assertEquals("YES", check("c1", "b2", "d4"));
		assertEquals("YES", check("a8", "c6", "h1"));
		assertEquals("NO", check("c6", "a8", "h1"));
	
		//FM test cases
		assertEquals("YES", check("a8", "f3", "c6"));
		assertEquals("YES", check("h8", "c3", "f6"));
		assertEquals("YES", check("h1", "c6", "f3"));
		assertEquals("YES", check("s1", "f6", "c3"));
	}

}
