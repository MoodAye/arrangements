package com.moodaye.playground.programmerSchool.June2018;

import java.util.Scanner;
import java.util.Stack;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #185 */
// Getting TLE on test 12.
public class HorseRacing {
	void solveRecursive(Scanner in, PrintWriter out){
		int n = in.nextInt();
		int h = in.nextInt();
		List<List<Integer>> adj = new ArrayList<>();
		for(int i = 1; i <= n; i++){
			adj.add(new ArrayList<Integer>());
		}
		
		boolean[] mark = new boolean[n + 1];
		
		while(true){
			int horse1 = in.nextInt();
			if(horse1 == 0){
				break;
			}
			int horse2 = in.nextInt();
			if(horse2 == h){
				out.println("No");
				return;
			}
			adj.get(horse1 - 1).add(horse2);
		}
		
		dfs(h, adj, mark);
		
		for(int i = 1; i <= n; i++){
			if(!mark[i]){
				out.println("No");
				return;
			}
		}
		out.println("Yes");
	}
	
	void dfs(int h, List<List<Integer>> adj, boolean[] mark){
		if(mark[h] == true){
			return;
		}
		mark[h] = true;
		Iterator<Integer> iter = adj.get(h - 1).iterator();
		 while(iter.hasNext()){
			 int hor = iter.next();
			 dfs(hor, adj, mark);
		 }
	}
	
	
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int h = in.nextInt();
		
		List<List<Integer>> adj = new ArrayList<>();
		for(int i = 1; i <= n; i++){
			adj.add(new ArrayList<Integer>());
		}
		boolean[] mark = new boolean[n + 1];
		
		while(true){
			int horse1 = in.nextInt();
			if(horse1 == 0){
				break;
			}
			int horse2 = in.nextInt();
			if(horse2 == h){
				out.println("No");
				return;
			}
			adj.get(horse1 - 1).add(horse2);
		}
		
		Stack<Integer> stack = new Stack<>();
		stack.push(h);
		mark[h] = true;
		while(stack.size() > 0){
			int horse = stack.pop();
			 Iterator<Integer> iter = adj.get(horse - 1).iterator();
			 while(iter.hasNext()){
				 int hor = iter.next();
				 if(mark[hor]) {
					 continue;
				 }
				 mark[hor] = true;
				 stack.push(hor);
			 }
		}
		
		for(int i = 1; i <= n; i++){
			if(!mark[i]){
				out.println("No");
				return;
			}
		}
		out.println("Yes");
	}
	

	public static void main(String args[]) {
		new HorseRacing().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solveRecursive(in, out);
		}
	}
}