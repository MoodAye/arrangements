package com.moodaye.playground.programmerSchool.June2018;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #456 */
// Start Time = 739pm Pause = 8:39 (time spent = 1hr)
// Wrong Answer test 11
public class ChestBillyBones {
	void solve(Scanner in, PrintWriter out) {
		// for each year - we end up with an equation of the form
		// ax - by
		// for the nth year - if we have c coins then
		//	ax - by = c ---> y = (ax - c) / b
		// we find y by trial and error
		
		// since max years = 21
		int[] a = new int[21];
		int[] b = new int[21];   
		a[1] = 1;
		b[1] = 0;
		a[2] = 1;
		b[2] = 1;
		
		int n = in.nextInt();
		int coins = in.nextInt();

		for(int i = 3; i <= n; i++){
			a[i] = a[i - 1] + a[i - 2];
			b[i] = b[i - 1] + b[i - 2];
		}
		
		int x = 1; // x cannot be zero since final coin count is non - zero
		while((a[n] * x - coins) % b[n] != 0 || (a[n] * x - coins) / b[n] <= 0){
			x++;
		}
		
//		out.printf("a = %d, b = %d%n", a[n], b[n]);
		out.printf("%d %d%n", x, x - (a[n] * x - coins) / b[n]);
	}

	public static void main(String args[]) {
		new ChestBillyBones().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
