package com.moodaye.playground.programmerSchool.June2018;

import java.util.Scanner;
import java.util.Arrays;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School # */
// Start Time = 7:18 Finish = 7:58pm Time Taken = 40 min
public class Poker {

	void solve2(Scanner in, PrintWriter out) {
		int[] cards = new int[5];
		for (int i = 0; i < 5; i++) {
			cards[i] = in.nextInt();
		}

		int[] cardCnt = new int[14];
		for (int c : cards) {
			cardCnt[c]++;
		}

		int[] cnt = new int[6];
		for (int i = 1; i <= 13; i++) {
			cnt[cardCnt[i]]++;
		}

		if (cnt[5] == 1) {
			out.println("Impossible");
			return;
		}
		else if (cnt[4] == 1) {
			out.println("Four of a Kind");
			return;
		}
		else if (cnt[3] == 1) {
			if (cnt[2] == 1) {
				out.println("Full House");
			} else {
				out.println("Three of a Kind");
			}
		}
		else if (cnt[2] == 2) {
			out.println("Two Pairs");
		}
		else if (cnt[2] == 1) {
			out.println("One Pair");
		}
		else if (cnt[1] == 5) {
			Arrays.sort(cards);
			if(cards[4] - cards[0] == 4){
				out.println("Straight");
			}
			else{
				out.println("Nothing");
			}
		}
	}

	void solve(Scanner in, PrintWriter out) {
		int[] cards = new int[5];
		for (int i = 0; i < 5; i++) {
			cards[i] = in.nextInt();
		}

		Arrays.sort(cards);

		if (cards[0] == cards[1] && cards[1] == cards[2] && cards[2] == cards[3] && cards[3] == cards[4]) {
			out.println("Impossible");
			return;
		}

		if (cards[0] == cards[1] && cards[1] == cards[2] && cards[2] == cards[3]
				|| cards[3] == cards[4] && cards[1] == cards[2] && cards[2] == cards[3]) {
			out.println("Four of a Kind");
			return;
		}

		if (cards[0] == cards[1] && cards[3] == cards[4]) {
			if (cards[1] == cards[2] || cards[2] == cards[3]) {
				out.println("Full House");
			} else {
				out.println("Two Pairs");
			}
			return;
		}

		if ((cards[0] == cards[1] && cards[1] == cards[2]) || (cards[1] == cards[2] && cards[2] == cards[3])
				|| (cards[2] == cards[3] && cards[3] == cards[4])) {
			out.println("Three of a Kind");
			return;
		}

		if ((cards[0] == cards[1] && cards[2] == cards[3]) || (cards[1] == cards[2] && cards[3] == cards[4])) {
			out.println("Two Pairs");
			return;
		}

		if (cards[0] - cards[1] == 0 || cards[1] - cards[2] == 0 || cards[2] - cards[3] == 0
				|| cards[3] - cards[4] == 0) {
			out.println("One Pair");
			return;
		}

		if (cards[0] - cards[1] == -1 && cards[1] - cards[2] == -1 && cards[2] - cards[3] == -1
				&& cards[3] - cards[4] == -1) {
			out.println("Straight");
			return;
		}
		out.println("Nothing");
	}

	public static void main(String args[]) {
		new Poker().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve2(in, out);
		}
	}
}
