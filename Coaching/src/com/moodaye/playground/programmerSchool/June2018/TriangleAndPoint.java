package com.moodaye.playground.programmerSchool.June2018;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #102 */
// uses area = abs(a.x(b.y - c.y) + b.x(c.y - a.y) + c.x(a.y - b.y))
public class TriangleAndPoint {
	void solve(Scanner in, PrintWriter out) {
		Point a = new Point();
		Point b = new Point();
		Point c = new Point();
		Point p = new Point();
		a.x = in.nextInt();
		a.y = in.nextInt();
		b.x = in.nextInt();
		b.y = in.nextInt();
		c.x = in.nextInt();
		c.y = in.nextInt();
		p.x = in.nextInt();
		p.y = in.nextInt();
		
		int area1 = triangleArea(a, b, c);
		int area2 = triangleArea(p, b, c);
		int area3 = triangleArea(a, p, c);
		int area4 = triangleArea(a, b, p);
		
		if(area1 == area2 + area3 + area4){
			out.println("In");
		}
		else{
			out.println("Out");
		}
	}

	// this will always return an int - but the area could be double
	// since area = 1/2 base x height ?? 
	public static int triangleArea(Point a, Point b, Point c){
		return Math.abs(a.x * (b.y - c.y) + b.x * (c.y - a.y) + c.x * (a.y - b.y));
	}

	public static void main(String args[]) {
		new TriangleAndPoint().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}

	static class Point {
		int x;
		int y;
	}
}
