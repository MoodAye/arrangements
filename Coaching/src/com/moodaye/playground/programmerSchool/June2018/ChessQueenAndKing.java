package com.moodaye.playground.programmerSchool.June2018;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #412 */
public class ChessQueenAndKing {
	void solve(Scanner in, PrintWriter out) {
		String wk = in.next();
		String wq = in.next();
		String bk = in.next();
		out.println(check(wk, wq, bk));
	}
	
	public static String check(String wk, String wq, String bk){
		if (sameRow(wq, bk) || sameCol(wq, bk) || sameDiag(wq, bk)) {
			if (!between(wq, bk, wk)) {
				return "YES";
			}
		}
		return "NO";
	}

	static boolean sameRow(String p1, String p2) {
		return p1.charAt(1) == p2.charAt(1);
	}

	static boolean sameCol(String p1, String p2) {
		return p1.charAt(0) == p2.charAt(0);
	}

	static boolean sameDiag(String p1, String p2) {
		int dx = p1.charAt(1) - p2.charAt(1);
		int dy = p1.charAt(0) - p2.charAt(0);
		return dx * dx == dy * dy;
	}

	/* is wk between wq and bk */
	static boolean between(String wq, String bk, String wk) {
		return ((sameRow(wq, bk) && sameRow(wq, wk)) && (inOrder(wq.charAt(0), wk.charAt(0), bk.charAt(0)))) ||
				((sameCol(wq, bk) && sameCol(wq, wk)) && (inOrder(wq.charAt(1), wk.charAt(1), bk.charAt(1)))) ||
				((sameDiag(wq, bk) && sameDiag(wq, wk) && sameDiag(bk, wk)) && (inOrder(wq.charAt(0), wk.charAt(0), bk.charAt(0))));
	}

	/* checks if c is between a and b */
	static boolean inOrder(char a, char b, char c){
			return (a < b && b < c) || (c < b && b < a);
	}

	public static void main(String args[]) {
		new ChessQueenAndKing().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
