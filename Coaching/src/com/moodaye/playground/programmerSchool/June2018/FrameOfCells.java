package com.moodaye.playground.programmerSchool.June2018;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #310 */
// Start Time = 9:44am Pause = 9:57am
public class FrameOfCells {
	void solve(Scanner in, PrintWriter out) {
		int t = in.nextInt();
		for (int i = 0; i < t; i++) {
			out.print(cover(in.nextInt(), in.nextInt(), in.nextInt()));
		}
	}

	public static int cover(int w, int h, int a) {
		if (w % a == 0 && (h - 2) % a == 0) {
			return 1;
		}
		if (h % a == 0 && (w - 2) % a == 0) {
			return 1;
		}
		if ((w - 1) % a == 0 && h % a == 0 && (h - 2) % a == 0) {
			return 1;
		}
		if ((h - 1) % a == 0 && w % a == 0 && (w - 2) % a == 0) {
			return 1;
		}
		
		if ((w - 1) % a == 0 && (h - 1) % a == 0) {
			return 1;
		}
		return 0;
	}

	public static void main(String args[]) {
		new FrameOfCells().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
