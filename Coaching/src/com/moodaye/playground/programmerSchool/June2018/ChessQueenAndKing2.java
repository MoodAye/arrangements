package com.moodaye.playground.programmerSchool.June2018;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #412 */
/* Alternate Approach */
public class ChessQueenAndKing2 {
	void solve(Scanner in, PrintWriter out) {
		String wk = in.next();
		String wq = in.next();
		String bk = in.next();
		out.println(check(wk, wq, bk));
	}

	/* walk white queen - if it meets black king without meeting white king first - return YES */
	public static String check(String wk, String wq, String bk){
		char wkh = wk.charAt(0);
		char wkv = wk.charAt(1);
		char bkh = bk.charAt(0);
		char bkv = bk.charAt(1);
		char wqh = wq.charAt(0);
		char wqv = wq.charAt(1);
		
		char temph= wqh;
		char tempv = wqv;
		
		while(wqh <= 'h'){
			if(wqv == wkv && wqh == wkh){
				break;
			}
			if(wqv == bkv && wqh == bkh){
				return "YES";
			}
			wqh++;
		}
		
		wqh = temph;
		while(wqh >= 'a'){
			if(wqv == wkv && wqh == wkh){
				break;
			}
			if(wqv == bkv && wqh == bkh){
				return "YES";
			}
			wqh--;
		}
		
		wqh = temph;
		wqv = tempv;
		while(wqv >= '1'){
			if(wqv == wkv && wqh == wkh){
				break;
			}
			if(wqv == bkv && wqh == bkh){
				return "YES";
			}
			wqv--;
		}
		
		wqh = temph;
		wqv = tempv;
		while(wqv <= '8'){
			if(wqv == wkv && wqh == wkh){
				break;
			}
			if(wqv == bkv && wqh == bkh){
				return "YES";
			}
			wqv++;
		}
		
		wqh = temph;
		wqv = tempv;
		while(wqv >= '1'){
			if(wqv == wkv && wqh == wkh){
				break;
			}
			if(wqv == bkv && wqh == bkh){
				return "YES";
			}
			wqv--;
		}
		
		wqh = temph;
		wqv = tempv;
		while(wqv <= '8' && wqh >= 'a'){
			if(wqv == wkv && wqh == wkh){
				break;
			}
			if(wqv == bkv && wqh == bkh){
				return "YES";
			}
			wqv++;
			wqh--;
		}
		
		
		wqh = temph;
		wqv = tempv;
		while(wqv >= '1' && wqh >= 'a'){
			if(wqv == wkv && wqh == wkh){
				break;
			}
			if(wqv == bkv && wqh == bkh){
				return "YES";
			}
			wqv--;
			wqh--;
		}
		
		
		wqh = temph;
		wqv = tempv;
		while(wqv <= '8' && wqh <= 'h'){
			if(wqv == wkv && wqh == wkh){
				break;
			}
			if(wqv == bkv && wqh == bkh){
				return "YES";
			}
			wqv++;
			wqh++;
		}
		
		wqh = temph;
		wqv = tempv;
		while(wqv >= '1' && wqh <= 'h'){
			if(wqv == wkv && wqh == wkh){
				break;
			}
			if(wqv == bkv && wqh == bkh){
				return "YES";
			}
			wqv--;
			wqh++;
		}
		return "NO";
	}

	public static void main(String args[]) {
		new ChessQueenAndKing2().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
