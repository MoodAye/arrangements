package com.moodaye.playground.programmerSchool;

import static org.junit.Assert.*;

import org.junit.Test;

public class GCDTest {

	@Test
	public void test1() {
		assertEquals(12, GCD.gcd(12, 12));
		assertEquals(2, GCD.gcd(2, 200000));
		assertEquals(17, GCD.gcd(51, 340));
		assertEquals(6, GCD.gcd(12, 42));
		assertEquals(1, GCD.gcd(1, 1));
		assertEquals(1, GCD.gcd(1, 99));
		assertEquals(1, GCD.gcd(7, 5));
	}

	@Test
	public void test2() {
		assertEquals(12, GCD.gcd2(12, 12));
		assertEquals(2, GCD.gcd2(2, 200000));
		assertEquals(17, GCD.gcd2(51, 340));
		assertEquals(6, GCD.gcd2(12, 42));
		assertEquals(1, GCD.gcd2(1, 1));
		assertEquals(1, GCD.gcd2(1, 99));
		assertEquals(1, GCD.gcd2(7, 5));
	}
}
