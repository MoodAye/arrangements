package com.moodaye.playground.programmerSchool;

import static org.junit.Assert.*;

import org.junit.Test;

public class LastDigitAPowerOfBTest {

	@Test
	public void test() {
		assertEquals(0, LastDigitAPowerOfB.lastDigit(0, 1));
		assertEquals(0, LastDigitAPowerOfB.lastDigit(0, 2));
		
		
		assertEquals(1, LastDigitAPowerOfB.lastDigit(1, 1));
		
		assertEquals(2, LastDigitAPowerOfB.lastDigit(2, 1));
		assertEquals(4, LastDigitAPowerOfB.lastDigit(2, 2));
		
		assertEquals(3, LastDigitAPowerOfB.lastDigit(3, 1));
		assertEquals(7, LastDigitAPowerOfB.lastDigit(3, 7));
		
		assertEquals(4, LastDigitAPowerOfB.lastDigit(4, 1));
		assertEquals(4, LastDigitAPowerOfB.lastDigit(24, 9));
		
		assertEquals(5, LastDigitAPowerOfB.lastDigit(5, 1));
		assertEquals(6, LastDigitAPowerOfB.lastDigit(6, 1));
		assertEquals(7, LastDigitAPowerOfB.lastDigit(7, 1));
		assertEquals(8, LastDigitAPowerOfB.lastDigit(8, 1));
		assertEquals(9, LastDigitAPowerOfB.lastDigit(9, 1));
	}
	
	@Test
	public void test2() {
		assertEquals(0, LastDigitAPowerOfB.lastDigitSimpler(0, 1));
		assertEquals(0, LastDigitAPowerOfB.lastDigitSimpler(0, 2));
		
		
		assertEquals(1, LastDigitAPowerOfB.lastDigitSimpler(1, 1));
		
		assertEquals(2, LastDigitAPowerOfB.lastDigitSimpler(2, 1));
		assertEquals(4, LastDigitAPowerOfB.lastDigitSimpler(2, 2));
		
		assertEquals(3, LastDigitAPowerOfB.lastDigitSimpler(3, 1));
		assertEquals(7, LastDigitAPowerOfB.lastDigitSimpler(3, 7));
		
		assertEquals(4, LastDigitAPowerOfB.lastDigitSimpler(4, 1));
		assertEquals(4, LastDigitAPowerOfB.lastDigitSimpler(24, 9));
		
		assertEquals(5, LastDigitAPowerOfB.lastDigitSimpler(5, 1));
		assertEquals(6, LastDigitAPowerOfB.lastDigitSimpler(6, 1));
		assertEquals(7, LastDigitAPowerOfB.lastDigitSimpler(7, 1));
		assertEquals(8, LastDigitAPowerOfB.lastDigitSimpler(8, 1));
		assertEquals(9, LastDigitAPowerOfB.lastDigitSimpler(9, 1));
	}
}
