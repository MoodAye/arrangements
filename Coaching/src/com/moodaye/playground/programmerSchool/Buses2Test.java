package com.moodaye.playground.programmerSchool;
import static org.junit.Assert.*;

import org.junit.Test;

public class Buses2Test {

	@Test
	public void test2() {
		assertEquals(2, Buses2.buses(10, 4, 7));
		assertEquals(0, Buses2.buses(10, 4, 5));
		assertEquals(0, Buses2.buses(1, 1, 1));
		assertEquals(0, Buses2.buses(1, 2, 1));
		assertEquals(1, Buses2.buses(1, 2, 99));
		assertEquals(0, Buses2.buses(20, 200, 2));
		assertEquals(0, Buses2.buses(20, 8, 2));
		assertEquals(7, Buses2.buses(21, 14, 5));
		assertEquals(2, Buses2.buses(10000, 10000, 10000));
	}
}
