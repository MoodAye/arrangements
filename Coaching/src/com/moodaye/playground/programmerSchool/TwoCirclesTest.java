package com.moodaye.playground.programmerSchool;

import static org.junit.Assert.*;

import org.junit.Test;

public class TwoCirclesTest {

	@Test
	public void testIsBetween() {
		assertTrue(TwoCircles.isBetween(1, 1, 2));
		assertTrue(TwoCircles.isBetween(100,100,100));
		assertTrue(TwoCircles.isBetween(-100,-200,2));
	}
	
	@Test
	public void testTwoCircles(){
		assertEquals(true, TwoCircles.commonPoint2(0,0,2,0,3,2));
		assertEquals(false, TwoCircles.commonPoint2(1,1,1,4,4,1));
		assertEquals(true, TwoCircles.commonPoint2(0,0,2,4,0,2));
		assertEquals(true, TwoCircles.commonPoint2(0,0,2,0,0,2));
		assertEquals(false, TwoCircles.commonPoint2(0,0,2,10,10,2));
		assertEquals(false, TwoCircles.commonPoint2(0,0,2,10,10,2));
		assertEquals(false, TwoCircles.commonPoint2(0,0,2,0,10,2));
    	assertEquals(true, TwoCircles.commonPoint2(0,0,2,1,-1,2));
    	assertEquals(false, TwoCircles.commonPoint2(0,0,100, 100, -100, 1));
		
		
		/**
		 *           x1left - - - x - - - x1right
		 */
		assertEquals("YES", TwoCircles.commonPoint(0,0,2,-4,0,2));
	}
	
	@Test
	public void testTwoCircles2(){
		assertEquals("YES", TwoCircles.commonPoint(0,0,2,0,3,2));
		assertEquals("NO", TwoCircles.commonPoint(1,1,1,4,4,1));
		assertEquals("YES", TwoCircles.commonPoint(0,0,2,4,0,2));
		assertEquals("YES", TwoCircles.commonPoint(0,0,2,0,0,2));
		assertEquals("NO", TwoCircles.commonPoint(0,0,2,10,10,2));
		assertEquals("NO", TwoCircles.commonPoint(0,0,2,10,10,2));
		assertEquals("NO", TwoCircles.commonPoint(0,0,2,0,10,2));
    	assertEquals("YES", TwoCircles.commonPoint(0,0,2,1,-1,2));
    	assertEquals("YES", TwoCircles.commonPoint(0,0,2,1,-1,2));
    	assertEquals("NO", TwoCircles.commonPoint(0,0,100, 100, -100, 1));
		
		
		
		/**
		 *           x1left - - - x - - - x1right
		 */
		assertEquals("YES", TwoCircles.commonPoint(0,0,2,-4,0,2));
	}
}
