package com.moodaye.playground.programmerSchool.June2019;

import java.util.Scanner;
import java.util.Arrays;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #32 */
// 921am - 935am
public class MaximumDifference {
	void solve(Scanner in, PrintWriter out) {
		int a = in.nextInt();
		int b = in.nextInt();
		out.println(maximum(a) - minimum(b));
	}

	int maximum(int a) {
		if (a < 0) {
			return -1 * (minimum(a * -1));
		}

		char[] ca = String.valueOf(a).toCharArray();
		Arrays.sort(ca);
		reverse(ca);
		return Integer.valueOf(String.valueOf(ca));
	}

	int minimum(int b) {
		if (b > 0) {
			char[] cb = String.valueOf(b).toCharArray();
			Arrays.sort(cb);
			if (cb[0] == '0') {
				int idx = 0;
				while (cb[idx] == '0') {
					idx++;
				}
				cb[0] = cb[idx];
				cb[idx] = '0';
			}
			return Integer.valueOf(String.valueOf(cb));
		}

		return -1 * maximum(b * -1);
	}

	void reverse(char[] ca) {
		for (int i = 0; i < ca.length / 2; i++) {
			char temp = ca[i];
			ca[i] = ca[ca.length - i - 1];
			ca[ca.length - i - 1] = temp;
		}
	}

	public static void main(String args[]) {
		new MaximumDifference().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}

}
