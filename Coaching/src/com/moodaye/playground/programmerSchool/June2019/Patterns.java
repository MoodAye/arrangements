package com.moodaye.playground.programmerSchool.June2019;

import java.util.Scanner;
import java.util.Arrays;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #49 */
// 755am - 810am (15min)
public class Patterns {
	void solve(Scanner in, PrintWriter out) {
		char[] p1 = in.next().toCharArray();
		char[] p2 = in.next().toCharArray();
		
		int cnt = 1;
		for(int i = 0; i < p1.length; i++) {
			cnt *= matchCount(p1[i], p2[i]);
		}
		out.println(cnt);
	}
	
	static String[] ps = new String[255];
	static {
		ps['?'] = "0123456789";
		ps['0'] = "0";
		ps['1'] = "1";
		ps['2'] = "2";
		ps['3'] = "3";
		ps['4'] = "4";
		ps['5'] = "5";
		ps['6'] = "6";
		ps['7'] = "7";
		ps['8'] = "8";
		ps['9'] = "9";
		ps['a'] = "0123";
		ps['b'] = "1234";
		ps['c'] = "2345";
		ps['d'] = "3456";
		ps['e'] = "4567";
		ps['f'] = "5678";
		ps['g'] = "6789";
	}
	
	int matchCount(char c1, char c2) {
		char[] sc1 = ps[c1].toCharArray();
		char[] sc2 = ps[c2].toCharArray();
		int cnt = 0;
		for(char c : sc1) {
			for(int i = 0; i < sc2.length; i++) {
				if(c == sc2[i]) {
					cnt++;
				}
			}
		}
		return cnt;
	}

	public static void main(String args[]) {
		new Patterns().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
