package com.moodaye.playground.programmerSchool.June2019;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #102 */
// 9:42pm - 9:56pm (14 min)
public class TriangleAndPoint {
	void solve(Scanner in, PrintWriter out) {
		int x1 = in.nextInt();
		int y1 = in.nextInt();
		int x2 = in.nextInt();
		int y2 = in.nextInt();
		int x3 = in.nextInt();
		int y3 = in.nextInt();
		int x4 = in.nextInt();
		int y4 = in.nextInt();
		
		int a123 = twiceAreaOfTriangle(x1, y1, x2, y2, x3, y3);
		int a234 = twiceAreaOfTriangle(x2, y2, x3, y3, x4, y4);
		int a341 = twiceAreaOfTriangle(x3, y3, x4, y4, x1, y1);
		int a412 = twiceAreaOfTriangle(x4, y4, x1, y1, x2, y2);
		if(a123 == (a234 + a341 + a412)){
			out.println("In");
		}
		else{
			out.println("Out");
		}
	}

	// twice so we don't need to worry about dividing by 2 and ending up with decimal
	int twiceAreaOfTriangle(int x1, int y1, int x2, int y2, int x3, int y3){
		return Math.abs(x1 * (y2 - y3) + x2 * (y3 - y1) + x3 * (y1 - y2));
	}

	public static void main(String args[]) {
		new TriangleAndPoint().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
