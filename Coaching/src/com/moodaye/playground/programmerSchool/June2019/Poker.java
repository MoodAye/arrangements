package com.moodaye.playground.programmerSchool.June2019;

import java.util.Scanner;
import java.util.Arrays;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #347 */
// 10:03pm - 10:12pm (9min)
public class Poker {
	void solve(Scanner in, PrintWriter out) {
		int[] cards = new int[5];
		int[] cnt = new int[14];
		for(int i = 0; i < 5; i++) {
			cards[i] = in.nextInt();
			cnt[cards[i]]++;
		}
		
		Arrays.sort(cards);
		
		int[] mults = new int[6];
		for(int i : cnt) {
			mults[i]++;
		}
		
		if(mults[5] == 1) {
			out.println("Impossible");
		}
		else if(mults[4] == 1) {
			out.println("Four of a Kind");
		}
		else if(mults[3] == 1 && mults[2] == 1) {
			out.println("Full House");
		}
		else if(mults[3] == 1) {
			out.println("Three of a Kind");
		}
		else if(mults[2] == 2) {
			out.println("Two Pairs");
		}
		else if(mults[2] == 1) {
			out.println("One Pair");
		}
		else if(cards[4] - cards[0] == 4) {
			out.println("Straight");
		}
		else {
			out.println("Nothing");
		}
	}

	public static void main(String args[]) {
		new Poker().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}

}
