package com.moodaye.playground.programmerSchool.June2019;

import java.util.Scanner;
import java.util.Arrays;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #292 */
// 7min
public class PrimeDigitalRoot {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		while(true){
			if(isPrime(n)){
				out.println(n);
				return;
			}
			if(n < 10){
				out.println(0);
				return;
			}
			n = sumOfDigits(n);
		}
	}
	
	int sumOfDigits(int n){
		int sum = 0;
		while(n > 0){
			sum += n % 10;
			n /= 10;
		}
		return sum;
	}
	
	boolean isPrime(int n){
		if(n == 1){
			return false;
		}
		for(int d = 2; d <= n / d; d++){
			if(n % d == 0){
				return false;
			}
		}
		return true;
	}

	public static void main(String args[]) {
		new PrimeDigitalRoot().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
