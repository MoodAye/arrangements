package com.moodaye.playground.programmerSchool.June2019;

import java.util.Arrays;
import java.util.Locale;
import java.io.InputStream;
import java.io.PrintWriter;

/** Programmer's School #360 */
// 1115pm - 1138pm (23min)
public class MaximumTriple {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int[][] lines = new int[3][n];
		Arrays.fill(lines[0], -500);
		Arrays.fill(lines[1], -500);
		Arrays.fill(lines[2], -500);
		
		int max = Integer.MIN_VALUE;
		for(int r = 0; r < n; r++) {
			int lastRow = r % 3;
			for(int c = 0; c < n; c++) {
				lines[lastRow][c] = in.nextInt();
			}
			// check hor
			for(int c = 0; c < n - 2 && n > 2; c++) {
				max = Math.max(max, lines[lastRow][c] + lines[lastRow][c + 1] + lines[lastRow][c + 2]);
			}
			
			// check vert
			for(int c = 0; c < n && n > 2; c++) {
				max = Math.max(max, lines[0][c] + lines[1][c] + lines[2][c]);
			}
			
			// check box
			int prevRow = (lastRow + 2) % 3;
			for(int c = 0; c < n - 1; c++) {
				max = Math.max(max, lines[lastRow][c] + lines[lastRow][c + 1] + lines[prevRow][c + 1]);
				max = Math.max(max, lines[lastRow][c + 1] + lines[prevRow][c + 1] + lines[prevRow][c]);
				max = Math.max(max, lines[prevRow][c + 1] + lines[prevRow][c] + lines[lastRow][c]);
				max = Math.max(max, lines[prevRow][c] + lines[lastRow][c] + lines[lastRow][c + 1]);
			}
		}
		out.println(max);
	}
	
	public static void main(String[] args) {
		new MaximumTriple().run();
	}
	
	void run() {
		Locale.setDefault(Locale.US);
		try(Scanner in = new Scanner(System.in); 
				PrintWriter out = new PrintWriter(System.out)){
			solve(in, out);
		}
	}
	static class Scanner implements AutoCloseable {

	    InputStream is;
	    byte buffer[] = new byte[1 << 16];
	    int size = 0;
	    int pos = 0;

	    Scanner(InputStream is) {
	        this.is = is;
	    }

	    int nextChar() {
	        if (pos >= size) {
	            try {
	                size = is.read(buffer);
	            } catch (java.io.IOException e) {
	                throw new java.io.IOError(e);
	            }
	            pos = 0;
	            if (size == -1) {
	                return -1;
	            }
	        }
	        Assert.check(pos < size);
	        int c = buffer[pos] & 0xFF;
	        pos++;
	        return c;
	    }

	    int nextInt() {
	        int c = nextChar();
	        while (c == ' ' || c == '\r' || c == '\n' || c == '\t') {
	            c = nextChar();
	        }
	        if (c == '-') {
	            c = nextChar();
	            Assert.check('0' <= c && c <= '9');
	            int n = -(c - '0');
	            c = nextChar();
	            while ('0' <= c && c <= '9') {
	                int d = c - '0';
	                c = nextChar();
	                Assert.check(n > Integer.MIN_VALUE / 10 || n == Integer.MIN_VALUE / 10 && d <= -(Integer.MIN_VALUE % 10));
	                n = n * 10 - d;
	            }
	            return n;
	        } else {
	            Assert.check('0' <= c && c <= '9');
	            int n = c - '0';
	            c = nextChar();
	            while ('0' <= c && c <= '9') {
	                int d = c - '0';
	                c = nextChar();
	                Assert.check(n < Integer.MAX_VALUE / 10 || n == Integer.MAX_VALUE / 10 && d <= Integer.MAX_VALUE % 10);
	                n = n * 10 + d;
	            }
	            return n;
	        }
	    }

	    @Override
	    public void close() {
	    }
	}

	static class Assert {
	    static void check(boolean e) {
	        if (!e) {
	            throw new AssertionError();
	        }
	    }
	}	
}
