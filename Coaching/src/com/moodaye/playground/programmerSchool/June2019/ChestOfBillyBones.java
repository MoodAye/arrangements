package com.moodaye.playground.programmerSchool.June2019;

import java.util.Scanner;
import java.util.Arrays;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #456 */
// 916pm - 945pm = 29min
// take some out and put some int?
public class ChestOfBillyBones {
	void solve(Scanner in, PrintWriter out) {
		int x = in.nextInt();
		int y = in.nextInt();
		
		// 1: start
		// 2: start - removed
		// 3: 2.start - removed
		// 4: 3.start - 2.removed
		// 5: 5.start - 3.removed
		// 6: 8.start - 5.removed
		// 7: 13.start - 8.removed
		//
		//
		// start series is: 2 , 3, 5, 8 , 13 --> or a fib series
		// same for removed but 1 behind
		// based on year we can get coeff of start and removed
		// y = fib(x)*start - fib(x-1)*removed
		// then guess start and removed to make equation work. 
		// removed = (fib(x) * start - y) / fib(x-1)
		// where  (fib(x) * start - y) % fib(x-1) = 0
		// and  (fib(x) * start > y; 
		// or start > y / fib(x)
		// 5 
		// 2
		// 8
		// 11
		// 19
		// 30
		
		int[] fib = new int[21];
		fib[0] = 0;
		fib[1] = 1;
		fib[2] = 1;
		for(int i = 3; i < 21; i++) {
			fib[i] = fib[i - 1] + fib[i - 2];
		}
		
		int start = (y + fib[x] - 1) / fib[x];
		int removed = 0;
		while(true){
			if(fib[x] * start > y) {
				if(((fib[x] * start - y) % fib[x-1]) == 0){
					removed = (fib[x] * start - y) / fib[x - 1];
					break;
				}
			}
			start++;
		}
		out.println(start + " " + (start - removed));
	}
	
	
	public static void main(String[] args) {
		new ChestOfBillyBones().run();
	}
	
	void run() {
		Locale.setDefault(Locale.US);
		try(Scanner in = new Scanner(System.in);
			PrintWriter out = new PrintWriter(System.out)){
				solve(in, out);
			}
	}
}
