package com.moodaye.playground.programmerSchool.June2019;

import java.util.Scanner;
import java.util.Arrays;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #412 */
// 12:40 1:01 am (21 min)
public class ChessKingAndQueen {
	void solve(Scanner in, PrintWriter out) {
		char[] wk = in.next().toCharArray();
		char[] wq = in.next().toCharArray();
		char[] bk = in.next().toCharArray();

		for (int dx = -1; dx <= 1; dx++) {
			for (int dy = -1; dy <= 1; dy++) {
				if(dx == 0 && dy == 0) {
					continue;
				}
				char[] wqm = Arrays.copyOf(wq, 2);
				wqm[0] += (char) dx;
				wqm[1] += (char) dy;
				while(isInBoard(wqm)) {
					if(wqm[0] == bk[0] && wqm[1] == bk[1]) {
						out.println("YES");
						return;
					}
					if(wqm[0] == wk[0] && wqm[1] == wk[1]) {
						break;
					}
				wqm[0] += (char) dx;
				wqm[1] += (char) dy;
				}
			}
		}
		out.println("NO");

	}
	
	boolean isInBoard(char[] pos) {
		return 'a' <= pos[0]  && pos[0] <= 'h' && '1' <= pos[1] && pos[1] <= '8';
	}

	public static void main(String[] args) {
		new ChessKingAndQueen().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
