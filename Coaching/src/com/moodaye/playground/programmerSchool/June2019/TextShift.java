package com.moodaye.playground.programmerSchool.June2019;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #203 */
// 807am 8:11am
public class TextShift {
	void solve(Scanner in, PrintWriter out) {
		String s1 = in.next();
		String s2 = in.next();
		int idx = (s2 + s2).indexOf(s1);
		
		out.println(idx == -1 ? -1 : idx);
	}

	public static void main(String args[]) {
		new TextShift().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
