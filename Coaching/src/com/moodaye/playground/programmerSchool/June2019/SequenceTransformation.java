package com.moodaye.playground.programmerSchool.June2019;

import java.util.Locale;
import java.util.Scanner;
import java.io.InputStream;
import java.io.PrintWriter;

/** Programmer's School #178 */
// 25min
public class SequenceTransformation {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int[] numbers = new int[n];
		int[] buckets = new int[2 * 1_000_000 + 1];

		for (int i = 0; i < n; i++) {
			numbers[i] = in.nextInt();
			buckets[numbers[i] + 1_000_000]++;
		}
		
		int maxCnt = Integer.MIN_VALUE;
		int maxOccurs = Integer.MIN_VALUE;
		for(int i = 0; i < buckets.length; i++) {
			if(buckets[i] > maxCnt) {
				maxCnt = buckets[i];
				maxOccurs = i - 1_000_000;
			}
		}
		
		for(int i : numbers) {
			if(i == maxOccurs) {
				continue;
			}
			out.print(i + " ");
		}
		
		while(0 < maxCnt--) {
			out.print(maxOccurs + " ");
		}
	}

	public static void main(String args[]) {
		new SequenceTransformation().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
