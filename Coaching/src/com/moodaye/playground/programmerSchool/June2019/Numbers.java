package com.moodaye.playground.programmerSchool.June2019;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #462 */
// took a while to figure this out
public class Numbers {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		out.println(n - n / 2 - n / 3 - n / 5 + 
				n / (2 * 3) + n / (2 * 5) + n / (3 * 5) - 
				n / (2 * 3 * 5));
	}

	public static void main(String args[]) {
		new Numbers().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
