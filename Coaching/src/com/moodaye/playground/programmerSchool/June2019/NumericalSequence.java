package com.moodaye.playground.programmerSchool.June2019;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #341 */
// start 10pm - 1024pm (24min)
// 0,1,2,3,4,5,6,7,8,9,
// 10,22,30,41,50,61,70,81,90,111,
// 200,311,400,511,600,711,800,911,2000,3111,4000,5111
public class NumericalSequence {

	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		if (n <= 11) {
			out.println(n - 1);
			return;
		}
		if (n == 12) {
			out.println(22);
			return;
		}

		char first = '2';
		int len = 2;
		char next = '2';
		for (int i = 13; i <= n; i++) {
			if (first < '9') {
				first = (char) (first + 1);
			} else {
				first = next == '1' ? '2' : '1';
				len++;
			}
			next = next == '0' ? '1' : '0';
		}
		
		out.print(first);
		for(int i = 0; i < len - 1; i++){
			out.print(next);
		}
	}

	public static void main(String args[]) {
		new NumericalSequence().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}

}
