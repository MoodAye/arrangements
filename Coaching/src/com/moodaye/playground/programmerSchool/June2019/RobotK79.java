package com.moodaye.playground.programmerSchool.June2019;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #235 */
// 1014pm - 10:31pm (17min)
public class RobotK79 {
	void solve(Scanner in, PrintWriter out) {
		char[] steps = in.next().toCharArray();
		boolean[][] visited = new boolean[101][101];
		
		int spotx = 50;
		int spoty = 50;
		visited[spotx][spoty] = true;
		
		int dx = 1;
		int dy = 0;
		
		int cnt = 0;
		
		for(char s : steps) {
			switch(s) {
			case 'S': 
				cnt++;
				spotx += dx;
				spoty += dy;
				if(visited[spotx][spoty]) {
					out.println(cnt);
					return;
				}
				visited[spotx][spoty] = true;
				break;
			case 'L':
				int temp = dx;
				dx = dy;
				dy = -temp;
				break;
			case 'R':
				temp = dx;
				dx = -dy;
				dy = temp;
			}
		}
		out.println(-1);
	}

	public static void main(String args[]) {
		new RobotK79().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
