package com.moodaye.playground.programmerSchool.June2019;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #464 */
// 1012am - 10:23 (11min)
public class NumberInSequence {

	void solve(Scanner in, PrintWriter out) {
		out.print(countBits(in.nextInt() - 1) % 3);
	}

	int countBits(int n) {
		int mask = 1 << 30;
		int cnt = 0;
		while (mask != 0) {
			if ((mask & n) != 0) {
				cnt++;
			}
			mask >>= 1;
		}
		return cnt;
	}

	void solve2(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		// determine number of conversions
		int cnt = 0;
		while (n > 1) {
			n -= smallerTwoSq(n);
			cnt++;
		}
		out.println(cnt % 3);
	}

	int smallerTwoSq(int n) {
		int twoSqBit = 1 << 30;
		while (twoSqBit >= n) {
			twoSqBit >>= 1;
		}
		return twoSqBit;
	}

	public static void main(String args[]) {
		new NumberInSequence().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
