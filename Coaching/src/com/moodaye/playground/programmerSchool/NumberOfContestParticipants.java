package com.moodaye.playground.programmerSchool;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

/** Programmer School - problem 677 */
public class NumberOfContestParticipants {
	void solve(Scanner in, PrintWriter out) {
		int K = in.nextInt();
		int N = in.nextInt();
		int M = in.nextInt();
		int D = in.nextInt();

		System.out.println(computeCountOfParticipants(K, N, M, D));
	}

	/** don't need lcm! */
	public static int computeCountOfParticipantsWithoutLCM(int K, int N, int M, int D){
		int prod_KNM = K * N * M;
		int D_part = prod_KNM - (prod_KNM / K) - (prod_KNM / N) - (prod_KNM / M);
		if (D_part <= 0){
			return -1;
		}
		int numberOfParticipants = prod_KNM * D / D_part;
		
		if (numberOfParticipants % K != 0 || numberOfParticipants % N != 0 || numberOfParticipants % M != 0){
			return -1;
		}
		return numberOfParticipants;
	}

	/**
	 * add the fractions and the remaining part of the whole is equivalent to D
	 */
	public static int computeCountOfParticipants(int K, int N, int M, int D) {

		int lcm_KN = lcm(K, N);
		int lcm_KNM = lcm(lcm_KN, M);

		// part of whole that is equivalent to D
		int D_part = lcm_KNM - (lcm_KNM / K) - (lcm_KNM / N) - (lcm_KNM / M);

		// commenting out this code caused one or more of the test cases at programmers school to fail.
		// So this is needed
		// note - D cannot be zero per problem constraint
		if (D_part <= 0) {
			return -1;
		}

		// TODO - commenting out this code did not cause any test cases to fail at programmer's school
		// so understand why this is not needed.
		// make sure that the answer is a whole number - if not the secretary
		// made a mistake
	/*	if ((lcm_KNM * D) % D_part != 0) {
			return -1;
		}*/
		
		int numberOfParticipants = (lcm_KNM * D) / (D_part); 
		
		// the participant portions need to be whole numbers
		if (numberOfParticipants % K != 0 || numberOfParticipants % N != 0 || numberOfParticipants % M != 0){
			return -1;
		}
		
		return numberOfParticipants;
	}

	/**
	 * uses well know formula leveraging gcd since for this problem a and b are
	 * small enough - no danger of exceeding integer max during the
	 * multiplication - or we could divide by gcd first
	 * TODO - make this work for -ve integers - how about gcd?
	 */
	public static int lcm(int a, int b) {
		return (a * b) / gcd(a, b);
	}

	/** compute lcm of any number of integers 
	 *  TODO - write test cases to confirm this works as expected.... how about -ve integers?
	 * 
	 */
	public static int lcm(int a[]){
		int lcm = a[0];
		if (a.length == 1) return lcm;
		for (int i = 1; i < a.length; i++) {
			lcm = lcm(lcm, a[i]);
		}
		return lcm;
	}

	// From GCD.java
	private static int gcd(int a, int b) {
		while (b != 0) {
			int t = a % b;
			a = b;
			b = t;
		}
		return a;
	}

	public static void main(String args[]) {
		new NumberOfContestParticipants().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
