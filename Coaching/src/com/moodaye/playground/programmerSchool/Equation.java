package com.moodaye.playground.programmerSchool;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;

/** Programmer's School - Problem 10 */
public class Equation {
	
	void solve(Scanner in, PrintWriter out){
		long A = in.nextInt();
		long B = in.nextInt();
		long C = in.nextInt();
		long D = in.nextInt();
		
		Set<Integer> roots = getRootsCubicEquation(A,B,C,D);
		roots.stream().forEach(root -> System.out.print( root + " "));
	}

	/** Actual solution by FM -- much simpler! 
	 *  Also - consider making A,B,C,D longs - not required
	 *  for the problem solution * */
	public static void getRootsCubicEquationSimpleVersion(long A, long B, long C, long D){
		for(int x = -100; x <= 100; x++){
			if (A * x  * x * x + B * x * x + C * x + D == 0 ){
				System.out.print(" " + x);
			}
		}
	}
	
	public static Set<Integer> getRootsCubicEquation(long A, long B, long C, long D){
		Set<Integer> roots = new TreeSet<>();
	
		if(D == 0){
			roots.add(0);
		}
		
		// cubic equation will have max 3 roots
		for (int x = 1; x <= 100 && roots.size() < 3; x++){
			
			long xSquared = x * x;
			long xCubed = xSquared * x;
			long firstFactor = A * xCubed;
			long secondFactor = B * xSquared;
			long thirdFactor = C * x;
			
			if(firstFactor + secondFactor + thirdFactor + D == 0){
				roots.add(x);
			}
			
			if ( -1 * firstFactor + secondFactor + -1 * thirdFactor + D == 0){
				roots.add(-1 * x);
			}
		}
		return roots;
	}
	
	public static void main(String args[]){
		new Equation().run();
	}
	
	void run(){
		Locale.setDefault(Locale.US);
		try( Scanner in  = new Scanner(System.in);
				PrintWriter out = new PrintWriter(System.out)){
			solve(in, out);
		}
	}
}
