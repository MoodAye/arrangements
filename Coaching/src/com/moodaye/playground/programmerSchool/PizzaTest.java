package com.moodaye.playground.programmerSchool;

import static org.junit.Assert.*;

import org.junit.Test;

public class PizzaTest {

	@Test
	public void test() {
		assertEquals(2, Pizza.fib(1));
		assertEquals(4, Pizza.fib(2));
		assertEquals(7, Pizza.fib(3));
		assertEquals(11, Pizza.fib(4));
		assertEquals(16, Pizza.fib(5));
		assertEquals(37, Pizza.fib(8));
	}

}
