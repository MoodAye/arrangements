package com.moodaye.playground.programmerSchool.July2019;

import java.util.Scanner;
import java.util.Arrays;
import java.util.Locale;
import java.io.PrintWriter;

 /** Programmer's School #500 */
// 919pm - 931pm ; 12min
public class Agent {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		Student[] students = new Student[n];
		for(int i = 0; i < n; i++) {
			students[i] = new Student();
			students[i].age = in.nextInt();
			students[i].risk = in.nextInt();
		}
		
		Arrays.sort(students);
		
		int[] totalRisk = new int[n];
		totalRisk[1] = students[1].risk;
		if(n == 2) {
			out.println(totalRisk[1]);
			return;
		}
		
		totalRisk[2] = students[2].risk + students[1].risk;
		if(n == 3) {
			out.println(totalRisk[2]);
			return;
		}
		
		totalRisk[3] = students[3].risk + students[1].risk;
		if(n == 4) {
			out.println(totalRisk[3]);
			return;
		}
		
		for(int i = 4; i < n; i++) {
			int risk1 = students[i].risk + students[i - 1].risk + totalRisk[i- 3];
			int risk2 = students[i].risk + totalRisk[i- 2];
			totalRisk[i] = Math.min(risk1, risk2);
		}
		
		out.println(totalRisk[n - 1]);
	}

	public static void main(String args[]) {
		new Agent().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
	
	static class Student implements Comparable<Student>{
		int age;
		int risk;
		
		@Override
		public int compareTo(Student o) {
			return age - o.age;
		}
	}
}
