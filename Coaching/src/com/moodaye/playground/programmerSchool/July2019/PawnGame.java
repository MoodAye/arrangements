package com.moodaye.playground.programmerSchool.July2019;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #787 */
// Start:632am   End:659am   Time Taken:27min

public class PawnGame {
void solve(Scanner in, PrintWriter out) {
	int n = in.nextInt();
	int[] diag = new int[n];
	for (int i = 0; i < n; i++) {
		diag[i] = in.nextInt();
	}
	if(n == 1) {
		out.println(diag[0]);
		return;
	}
	
	int gvalue = 0;
	if(n % 2 == 0){
		gvalue = Math.max(diag[n / 2], diag[n / 2 - 1]);
	}
	else {
		gvalue = Math.min(diag[n / 2], Math.max(diag[n / 2 - 1], diag[n / 2 + 1]));
	}
	out.println(gvalue);
}

public static void main(String args[]) {
	new PawnGame().run();
}

void run() {
	Locale.setDefault(Locale.US);
	try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
		solve(in, out);
	}
}
}
