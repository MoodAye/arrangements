package com.moodaye.playground.programmerSchool.July2019;

import java.util.Scanner;
import java.util.Arrays;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #171 */
// How would you do this the brute force way?
// start = 844pm - 905pm  (21 min)
public class Nails {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int[] x = new int[n];
		for (int i = 0; i < n; i++) {
			x[i] = in.nextInt();
		}
		
		Arrays.sort(x);
		
		int[] minLen = new int[n];
		minLen[1] = x[1] - x[0];
		if(n == 2) {
			out.println(minLen[1]);
			return;
		}
		
		minLen[2] = x[2] - x[0];
		if(n == 3) {
			out.println(minLen[2]);
			return;
		}
		
		minLen[3] = x[1] - x[0] + x[3] - x[2];
		if(n == 4) {
			out.println(minLen[3]);
			return;
		}
		
		for(int i = 4; i < n; i++) {
			int option1 = x[i] - x[i - 1] + x[i - 1] - x[i - 2] + minLen[i - 3];
			int option2 = x[i] - x[i - 1] + minLen[i - 2];
			minLen[i] = Math.min(option1, option2);
		}
		out.println(minLen[n - 1]);
	}

	public static void main(String args[]) {
		new Nails().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
