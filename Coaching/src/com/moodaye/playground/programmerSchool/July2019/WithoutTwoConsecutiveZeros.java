package com.moodaye.playground.programmerSchool.July2019;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #114 */
// Time = ~10min
public class WithoutTwoConsecutiveZeros {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int k = in.nextInt();
		int count[] = new int[n + 1];

		count[2] = (k - 1) * k;
		if (n >= 3) {
			count[3] = (k - 1) * (k - 1) * (k - 1) + (k - 1) * (k - 1) + (k - 1) * (k - 1);
		}

		// general case
		// x0......; x......
		for (int i = 4; i <= n; i++) {
			count[i] = (k - 1) * (count[i - 2] + count[i - 1]);
		}

		out.println(count[n]);
	}
	
	public static void main(String args[]) {
		new WithoutTwoConsecutiveZeros().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
