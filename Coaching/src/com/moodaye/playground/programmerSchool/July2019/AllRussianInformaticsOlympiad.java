package com.moodaye.playground.programmerSchool.July2019;

import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Locale;
import java.util.Scanner;

public class AllRussianInformaticsOlympiad {

	void solve2(Scanner in, PrintWriter out) {

		int monLen = in.nextInt();
		int olymLen = in.nextInt();
		int wkLen = in.nextInt();
		int badWkDayCnt = in.nextInt();
		int firstDay = in.nextInt();
		int[] badWkDays = new int[badWkDayCnt];
		for (int i = 0; i < badWkDayCnt; i++) {
			badWkDays[i] = in.nextInt();
		}
		int badMonDayCnt = in.nextInt();
		int[] badMonDays = new int[badMonDayCnt];
		for (int i = 0; i < badMonDayCnt; i++) {
			badMonDays[i] = in.nextInt();
		}

		int consecDays = 0;
		int cnt = 0;

		for (int day = 1; day <= monLen; day++) {
			if (!holiday(day)) {
				consecDays++;
				if (consecDays >= olymLen) {
					cnt++;
				}
			} else {
				consecDays = 0;
			}
		}
	}

	boolean holiday(int day) {
		return false;
	}

	void solve(Scanner in, PrintWriter out) {
		int monLen = in.nextInt();
		int olymLen = in.nextInt();
		int wkLen = in.nextInt();
		int badWkDayCnt = in.nextInt();
		int firstDay = in.nextInt();
		int[] badWkDays = new int[badWkDayCnt];
		for (int i = 0; i < badWkDayCnt; i++) {
			badWkDays[i] = in.nextInt();
		}
		int badMonDayCnt = in.nextInt();
		int[] badMonDays = new int[badMonDayCnt];
		for (int i = 0; i < badMonDayCnt; i++) {
			badMonDays[i] = in.nextInt();
		}

		boolean[] goodDaysInMonth = new boolean[monLen + 1];
		Arrays.fill(goodDaysInMonth, true);
		goodDaysInMonth[0] = false;

		for (int day : badWkDays) {
			if (day < firstDay) {
				day += wkLen;
			}

			int adjustedDay = day - firstDay + 1;
			while (adjustedDay <= monLen) {
				goodDaysInMonth[adjustedDay] = false;
				adjustedDay += wkLen;
			}
		}

		for (int day : badMonDays) {
			goodDaysInMonth[day] = false;
		}

		int consecDays = 0;
		int optionsCnt = 0;
		for (int day = 1; day <= monLen; day++) {
			consecDays =  goodDaysInMonth[day] ? consecDays + 1 : 0;
			if (consecDays >= olymLen) {
				optionsCnt++;
			}
		}
		out.println(optionsCnt);
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}

	public static void main(String[] args) {
		new AllRussianInformaticsOlympiad().run();
	}
}
