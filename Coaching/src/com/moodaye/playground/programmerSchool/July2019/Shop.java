package com.moodaye.playground.programmerSchool.July2019;

import java.util.Scanner;
import java.util.Arrays;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #169 */
// Time Taken > 1hr (Non recursive solution)
public class Shop {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int k = in.nextInt();
		int[][] nk = new int[k + n + 1][k + n + 1];
		for (int i = 0; i <= n + k; i++) {
			if(i != 0) {
				Arrays.fill(nk[i], -1);
			}
			for (int j = 0; j <= n + k; j++) {
				if (i == j) {
					nk[i][j] = 1;
				}
				if (i > j) {
					nk[i][j] = 0;
				}
			}
		}

		for (int j = 1; j <= k; j++) {
			for (int i = 1; i <= (n + k) - j; i++) {
				if (nk[i][j] != -1) {
					continue;
				}
				nk[i][j] = nk[i - 1][j - 1] + nk[i + 1][j - 1];
			}
		}
		out.println(nk[n][k]);
	}
	
	public static void main(String args[]) {
		new Shop().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
