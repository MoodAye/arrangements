package com.moodaye.playground.programmerSchool.July2019;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #908 */
// Start:850pm   End:920pm   Time Taken:30min
public class Number3 {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int[] cnt = new int[3 * (n + 1)];

		int next = 1;
		if (n == 1) {
			out.println(0);
			return;
		}

		while (next < n) {
			if (cnt[next] + 1 < cnt[next + 1] || cnt[next + 1] == 0) {
				cnt[next + 1] = cnt[next] + 1;
			}
			if (cnt[next] + 1 < cnt[next * 2] || cnt[next * 2] == 0) {
				cnt[next * 2] = cnt[next] + 1;
			}
			if (cnt[next] + 1 < cnt[next * 3] || cnt[next * 3] == 0) {
				cnt[next * 3] = cnt[next] + 1;
			}
			next++;
		}
		out.println(cnt[n]);
	}

	public static void main(String args[]) {
		new Number3().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
