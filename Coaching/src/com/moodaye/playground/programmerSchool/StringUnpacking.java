package com.moodaye.playground.programmerSchool;

import java.util.Scanner;
import java.io.PrintWriter;
import java.util.Locale;

/** Programmer's School #231 */
public class StringUnpacking {
	void solve(Scanner in, PrintWriter out) {
		String input = in.next();
		StringBuilder output = new StringBuilder();
		int counter = 0;
		for(int i = 0; i < input.length(); ){
			int n;
			if(isLetter(input.charAt(i))){
				n = 1;
			}
			else if(!isLetter(input.charAt(i + 1))){
				n = Integer.parseInt(input.substring(i, i + 2));
				i +=  2;
			}
			else{
				n = Integer.parseInt(input.substring(i, i + 1));
				i++;
			}
			char c = input.charAt(i);
		
			for(int k = 0; k < n; k++){
				if((counter != 0) && (counter  % 40 == 0)){
					output.append(System.lineSeparator());
				}
				counter++;
				output.append(c);
			}
			i++;
		}
		out.println(output);
	}
	
	public static boolean isLetter(char c){
		return (c >= 'A' && c <= 'Z');
	}
	
	public static void main(String[] args) {
		new StringUnpacking().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}

}
