package com.moodaye.playground.programmerSchool;

import static org.junit.Assert.*;

import org.junit.Test;

public class PrinceAndDragonTest {

	@Test
	public void test() {
		assertEquals("4", PrinceAndDragon.numberOfBlows(3, 6, 2));
		assertEquals("1", PrinceAndDragon.numberOfBlows(4, 4, 5));
		assertEquals("NO", PrinceAndDragon.numberOfBlows(5, 10, 6));
		assertEquals("NO", PrinceAndDragon.numberOfBlows(5, 10, 5));
		assertEquals("4", PrinceAndDragon.numberOfBlows(5, 10, 3));
		assertEquals("4", PrinceAndDragon.numberOfBlows(5, 10, 3));
		assertEquals("1", PrinceAndDragon.numberOfBlows(100_000, 90_000, 1));
		assertEquals("101", PrinceAndDragon.numberOfBlows(1_000, 100_000, 1));
	}

}
