package com.moodaye.playground.programmerSchool;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

/** Programmer's School #398 */
//TODO Try recursion?? (problem category indicates)
public class Sum_2 {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
//		out.println(approach1(n));
//		out.println(approach2(n));
//		out.println(approach3(n));
//		out.println(approach4(n));
		out.println(approach5(n));
	}	

	
	int approach5(int n){
		long st = System.currentTimeMillis();
		int cnt = 0;

		for (int i = n - 3; i >= (n + 3) / 4; i--) {
			for (int j = i; j >= (n - i + 2) / 3; j--) {
				if (i + j + 2 > n) {
					continue;
				}
				for (int k = j; k >= (n - i - j + 1) / 2; k--) {
					if (i + j + k >= n) {
						continue;
					}
					int m = n - (i + j + k);
					if (m > k) continue;
					else{
						cnt++;
					}
				}
			}
		}
		long et = System.currentTimeMillis();
		long tt = (et - st);
//		System.out.println("#5 ---> Time = " + tt);
		return cnt;
	}
	
	int approach4(int n){
		long st = System.currentTimeMillis();
		int cnt = 0;
		for (int i = n - 3; i >= (n + 3) / 4; i--) {
			for (int j = i; j >= (n - i + 2) / 3; j--) {
				for (int k = j; k >= (n - i - j + 1) / 2; k--) {
					if (k >= n -i -j || 
							2 * k < n - i - j){
						continue;
					}
					else{
						cnt++;
					}
				}
			}
		}
		long et = System.currentTimeMillis();
		long tt = (et - st);
		System.out.println("#4 --> Time = " + tt);
		return cnt;
	}
	
	/** further optimizations to reduce the number of time the for loops are executed */
	/** Complexity  = O(n^3) */
	int approach3(int n){
		long st = System.currentTimeMillis();
		int cnt = 0;
		for (int i = n - 3; i >= 1; i--){
			for (int j = 1; j <= n / 2 && j <= i; j++){ 
				for (int k = 1; k <= n / 3 && k <= j; k++){
					if (k >= n - i - j || 
							2 * k < n - i - j){
						continue;
					}
					else{
						cnt++;
					}
				}
			}
		}
		long et = System.currentTimeMillis();
		long tt = (et - st);
		System.out.println("#3 --> Time = " + tt);
		return cnt;
	}
	
	int approach2(int n){
		long st = System.currentTimeMillis();
		int cnt = 0;
		for (int i = n; i >= 1; i--) {
			for (int j = i; j >= 1; j--) {
				for (int k = j; k >= 1; k--) {
					if (k >= n -i -j || 
							2 * k < n - i - j){
						continue;
					}
					else{
						cnt++;
					}
				}
			}
		}
		long et = System.currentTimeMillis();
		long tt = (et - st);
		System.out.println("#2 ---> Time = " + tt);
		return cnt;
	}
	
	int approach1(int n){
		long st = System.currentTimeMillis();
		int cnt = 0;

		for (int i = n - 3; i >= 1; i--) {
			for (int j = i; j >= 1; j--) {
				if (i + j + 2 > n) {
					continue;
				}
				for (int k = j; k >= 1; k--) {
					if (i + j + k >= n) {
						continue;
					}
					int m = n - (i + j + k);
					if (m > k) continue;
					else{
						cnt++;
					}
				}
			}
		}
		long et = System.currentTimeMillis();
		long tt = (et - st);
		System.out.println("#1 ---> Time = " + tt);
		return cnt;
	}
	
	


	public static void main(String[] args) {
		new Sum_2().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
