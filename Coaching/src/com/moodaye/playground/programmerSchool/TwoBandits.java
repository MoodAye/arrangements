package com.moodaye.playground.programmerSchool;

import java.util.Scanner;

/** Problem # 33 https://acmp.ru/index.asp?main=task&id_task=33 */
public class TwoBandits {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		Integer first = in.nextInt();
		Integer second = in.nextInt();
		System.out.format("%d %d", second - 1,first - 1);
	}
}
