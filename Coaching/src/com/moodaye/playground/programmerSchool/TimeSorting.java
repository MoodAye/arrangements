package com.moodaye.playground.programmerSchool;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

/** Problem 119 - Programmer's School */
public class TimeSorting {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();
		List<Integer> list = new ArrayList<>();
		for(int i =0; i < n; i++){
			int time = in.nextInt()*10000 + in.nextInt()*100 + in.nextInt();
			list.add(time);
		}
		list.sort(Integer::compareTo);
		for(int t : list){
			int hr = t / 10000;
			int min = (t /100) % 100;
			int sec =  t % 100;
			System.out.println( hr + " " + min + " " + sec);
		}
	}
		
	public static void usingStrings(String[] args) {
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();
		List<String> list = new ArrayList<>();
		for (int i = 0; i < n ; i++){
			String hr =  pad(in.next());
			String min = pad(in.next());
			String sec = pad(in.next());
			list.add(hr+min+sec);
		}
		list.sort(String::compareTo);
		for(String s : list){
			System.out.println(unPad(s.substring(0, 2)) + " " + 
							   unPad(s.substring(2, 4)) + " " + 
							   unPad(s.substring(4, 6)));
		}
	}
	
	private static String pad(String s){
		if(s.length() == 1){
			return "0" + s;
		}
		return s;
	}
	
	private static String unPad(String s){
		if(s.charAt(0) == '0'){
			return s.substring(1);
		}
		return s;
	}
}
