package com.moodaye.playground.programmerSchool;

import static org.junit.Assert.*;

import org.junit.Test;

public class ElevatorTest {

	@Test
	public void test() {
		assertEquals(3, Elevator.floors("11"));
		assertEquals(2, Elevator.floors("21212"));
		assertEquals(6, Elevator.floors("1221221221221"));
		assertEquals(2, Elevator.floors("1"));
		assertEquals(1, Elevator.floors(""));
	}

}
