package com.moodaye.playground.programmerSchool.July2018;

import java.util.Scanner;
import java.util.Arrays;
import java.util.Locale;
import java.io.InputStream;
import java.io.PrintWriter;

/** Programmer's School #178 */
// Array of ints - 2xe6 x 4 bytes = 8MB.
// Start = 10:22pm  End = 10:53 Time Taken = 31min
public class SequenceTransformation {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int[] seq = new int[n];
		int[] counter = new int[2_000_001];

		int max = 0;
		int maxN = -1;
		for (int i = 0; i < n; i++) {
			seq[i] = in.nextInt();
			counter[seq[i] + 1_000_000]++;
			if (counter[seq[i] + 1_000_000] > max || (counter[seq[i] + 1_000_000] == max && seq[i] < maxN)) {
				max = counter[seq[i] + 1_000_000];
				maxN = seq[i];
			}
		}

		for (int i = 0; i < n; i++) {
			if (seq[i] != maxN) {
				out.print(seq[i] + " ");
			} 
		}
		for (int i = 0; i < counter[maxN + 1_000_000]; i++) {
			out.print(maxN + " ");
		}
	}

	public static void main(String args[]) {
		new SequenceTransformation().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}

	static class Scanner implements AutoCloseable {

		InputStream is;
		byte buffer[] = new byte[1 << 16];
		int size = 0;
		int pos = 0;

		Scanner(InputStream is) {
			this.is = is;
		}

		int nextChar() {
			if (pos >= size) {
				try {
					size = is.read(buffer);
				} catch (java.io.IOException e) {
					throw new java.io.IOError(e);
				}
				pos = 0;
				if (size == -1) {
					return -1;
				}
			}
			Assert.check(pos < size);
			int c = buffer[pos] & 0xFF;
			pos++;
			return c;
		}

		int nextInt() {
			int c = nextChar();
			while (c == ' ' || c == '\r' || c == '\n' || c == '\t') {
				c = nextChar();
			}
			if (c == '-') {
				c = nextChar();
				Assert.check('0' <= c && c <= '9');
				int n = -(c - '0');
				c = nextChar();
				while ('0' <= c && c <= '9') {
					int d = c - '0';
					c = nextChar();
					Assert.check(n > Integer.MIN_VALUE / 10
							|| n == Integer.MIN_VALUE / 10 && d <= -(Integer.MIN_VALUE % 10));
					n = n * 10 - d;
				}
				return n;
			} else {
				Assert.check('0' <= c && c <= '9');
				int n = c - '0';
				c = nextChar();
				while ('0' <= c && c <= '9') {
					int d = c - '0';
					c = nextChar();
					Assert.check(
							n < Integer.MAX_VALUE / 10 || n == Integer.MAX_VALUE / 10 && d <= Integer.MAX_VALUE % 10);
					n = n * 10 + d;
				}
				return n;
			}
		}

		@Override
		public void close() {
		}
	}

	static class Assert {
		static void check(boolean e) {
			if (!e) {
				throw new AssertionError();
			}
		}
	}
}
