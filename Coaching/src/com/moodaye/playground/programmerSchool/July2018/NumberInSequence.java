package com.moodaye.playground.programmerSchool.July2018;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #464 */
public class NumberInSequence {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		// out.println(genSeq(n).charAt(n - 1));
		// out.println(genSeqDivideAndConquer(n));
		int cnt = numberTransforms(n) - 1;
		if (cnt % 3 == 2) {
			out.println(2);
		}
		else if (cnt % 3 == 1) {
			out.println(1);
		}
		else{
			out.println(0);
		}
	}

	/*
	 * 1. Find the highest power of 2 smaller than n. 
	 * 2. n = n - power of 2 
	 * 3. Repeat until n is equal to 1 
	 * 4. Number of transforms = number of iterations
	 * 
	 * (t = transforms; n = index; t = number of transformations)
	 
		n=	1	2	3	4	5	6	7	8	9	10	
		t=	0	1	1	2	1	2	2	3	1	2


		n=	11	12	13	14	15	16	17	18	19	20	
		t=	2	3	2	3	3	4	1	2	2	3	


		n=	21	22	23	24	25	26	27	28	29	30	
		t=	2	3	3	4	2	3	3	4	3	4	


		n=	31	32  ...
		t=	4	5   ...
	*/
	public static int numberTransforms(int n) {
		if(n == 1){
			return 1;
		}
		
		int power2 = 1 << 30;
		while ((power2 & n) == 0) {
			power2 >>= 1;
		}

		if (n == power2) {
			power2 >>= 1;
		}
		
		n -= power2;

		return numberTransforms(n) + 1;
	}

	public static void main(String args[]) {
		new NumberInSequence().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
