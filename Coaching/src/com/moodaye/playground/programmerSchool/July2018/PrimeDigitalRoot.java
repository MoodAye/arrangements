package com.moodaye.playground.programmerSchool.July2018;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #292 */
// Test 2^31 - 1 (2147483647)
public class PrimeDigitalRoot {

	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		out.println(pdr(n));
	}

	public static int pdr(int n) {
		if (isPrime(n)) {
			return n;
		}
		if (n <= 9) {
			return 0;
		}
		return pdr(sumOfDigits(n));
	}

	public static boolean isPrime(int n) {
		if (n <= 1) {
			return false;
		}

		for (int i = 2; i <= Math.sqrt(n); i++) {
			if (n % i == 0) {
				return false;
			}
		}
		return true;
	}

	public static void mark(boolean[] notPrime, int p) {
		for (int i = 2 * p; i < notPrime.length; i += p) {
			notPrime[i] = true;
		}
	}

	public static int sumOfDigits(int n) {
		int sum = 0;
		while (n > 0) {
			sum += n % 10;
			n /= 10;
		}
		return sum;
	}

	public static void main(String[] args) {
		new PrimeDigitalRoot().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
