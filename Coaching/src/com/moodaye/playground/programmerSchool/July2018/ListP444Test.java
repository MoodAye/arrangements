package com.moodaye.playground.programmerSchool.July2018;

import static org.junit.Assert.*;
import static com.moodaye.playground.programmerSchool.July2018.ListP444.*;
import java.util.Arrays;

import org.junit.Test;

public class ListP444Test {

	@Test
	public void test() {
		int[] a = {1, 3, 5, -1, 3, 4, 6};
		Arrays.sort(a);
		assertEquals(getStringList(a),"-1, 1, 3, ..., 6");
		
		int[] a1 = {1};
		Arrays.sort(a1);
		assertEquals(getStringList(a1),"1");
		
		int[] a2 = {1,2,3};
		Arrays.sort(a2);
		assertEquals(getStringList(a2),"1, 2, 3");
		
		int[] a3 = {-1, -2, -3};
		Arrays.sort(a3);
		assertEquals(getStringList(a3),"-3, -2, -1");
//		assertEquals(getStringList(a3),"-3, ..., -1");
		
		int[] a4 = {-11, -10, -9};
		Arrays.sort(a4);
//		assertEquals(getStringList(a4),"-11, -10, -9");
		assertEquals(getStringList(a4),"-11, ..., -9");
		
		int[] a5 = {11, 10, 9};
		Arrays.sort(a5);
		assertEquals(getStringList(a5),"9, 10, 11");
//		assertEquals(getStringList(a5),"9, ..., 11");
		
		int[] a6 = {1,10,20,30,32,33,33,34,35,36,36,36};
		Arrays.sort(a6);
		assertEquals(getStringList(a6),"1, 10, 20, 30, 32, ..., 36");
		
		int[] a7 = {-1,-10,-20,-30,-32,-33,-33,-34,-35,-36,-36,-36,-100};
		Arrays.sort(a7);
		assertEquals(getStringList(a7),"-100, -36, ..., -32, -30, -20, -10, -1");
		
		int[] a8 = {-10000, -9000, -11, -10, -9};
		Arrays.sort(a8);
		assertEquals(getStringList(a8),"-10000, -9000, -11, ..., -9");
		
		int[] a91 = {0, 1, 5};
		Arrays.sort(a91);
		assertEquals(getStringList(a91),"0, 1, 5");
		
		int[] a90 = {-9, 0, 1, 5, 6, 7, 8};
		Arrays.sort(a90);
		assertEquals(getStringList(a90),"-9, 0, 1, 5, ..., 8");
		
		int[] a92 = {-9, 0, 1, 5};
		Arrays.sort(a92);
		assertEquals(getStringList(a92),"-9, 0, 1, 5");
		
		int[] a9 = {-10000, -9000, -11, -10, -9, 0, 1, 5, 6, 7, 8, 10, 11, 12, 13};
		Arrays.sort(a9);
		assertEquals(getStringList(a9),"-10000, -9000, -11, ..., -9, 0, 1, 5, ..., 8, 10, ..., 13");
		
		int[] a10 = {1000, 1001, 1002};
		Arrays.sort(a10);
//		assertEquals(getStringList(a10),"1000, 1001, 1002");
		assertEquals(getStringList(a10),"1000, ..., 1002");
		
		int[] a11 = {2, 2, 2};
		Arrays.sort(a11);
		assertEquals(getStringList(a11),"2");
		
		int[] a12 = {1, 2, 2, 3};
		Arrays.sort(a12);
		assertEquals(getStringList(a12),"1, 2, 3");
		
		int[] a13 = {1, 2, 3, 3};
		Arrays.sort(a13);
		assertEquals(getStringList(a13),"1, 2, 3");
		
		
	}
}

/* Tests
 *	 list ending with <, d, ..., e>  OK
 *	 list starting with <d, ..., e, > OK
 *	 list having only <d, ..., e> OK
 *	 list crossing 0  <-d, ..., e, > OK
 *	 list crossing digit sizes 
 */