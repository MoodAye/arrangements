package com.moodaye.playground.programmerSchool.July2018;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #462 */
// Start Time = 10:40am  End Time = 11:00am
// Initial Intuition is to use formula:
// 1/2 of numbers are divisible by 2
// of the remaining : 1/6 are divisible by 3
// of the remaining : 1/15 are divisible by 5.
public class Numbers {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
//		out.println(bruteForce(n));
//		out.println(formula(n));
		
	/* A U B U C = A + B + C - A int B - B int C - C int A + A U B U C */
		out.println(n - n / 2 - n / 3 - n / 5 + n / 6 + n / 15 + n / 10 - n / 30);
		
	}

	
	public static int formula(int n){
		int n1 = n + 1;
		while(n1 % 3 != 0){
			n1++;
		}
		
		int n2 = n + 1;
		while(n2 % 5 != 0){
			n2++;
		}
				
		return n - n /2 - n1 / 6 - n2 / 15;
	}
	
	public static int bruteForce(int n){
		int cnt = 0;
		for(int i = 1; i <= n ; i += 2){
			if(i % 3 == 0 || i % 5 == 0){
				continue;
			}
			cnt++;
		}
		return cnt;
	}

	public static void main(String args[]) {
		new Numbers().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
