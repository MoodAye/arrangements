package com.moodaye.playground.programmerSchool.July2018;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #170 */
// Start = 1045pm  End = 11;21  Time Taken = 36min
// 25: 25, 12+13, 3+4+5+6+7
public class NumberDecomposition {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int start = 1;
		int skip = 1;
		int max = 1;
		for(int i = 2; start + skip <= n; i++){
			start += i;
			skip++;
			if(((n - start) % skip) == 0){
				max = skip;
//				out.printf("start = %d, skip = %d%n", start, skip);
			}
		}
		out.println(max);
	}

	public static void main(String args[]) {
		new NumberDecomposition().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
