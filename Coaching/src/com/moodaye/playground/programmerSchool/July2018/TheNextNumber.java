package com.moodaye.playground.programmerSchool.July2018;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #318 */
// ~1.5 hours
// With Brute force - where we count the "1" in the binary representation and then increment n 
// until we find the next number with the same "1" count - would take 2e31 operations.
//
// This solution finds the first "1" from the left - flips it to a "0".
// Then it find the nest "0" to the left of the flipped digit, shifting '
// digits to the right until the "0" is found.  E.g.,
// 32767 =  011 1111 1111 1111 
// Step 1 = 011 1111 1111 1110
// Step 2 = 011 1111 1111 1101
// Step 3 = 011 1111 1111 1011
// Step 4 = 011 1111 1111 0111
// ...
// Answer = 101 1111 1111 1111 (49151)

public class TheNextNumber {
	void solve(Scanner in, PrintWriter out) {
		long n = in.nextLong();
		long mask = 1;
		
		// find the first "1" and flip it.
		while((mask & n) == 0){
			mask <<= 1;
		}
		n ^= mask;
		
		//while moving left until you find a "0"
		//move any "1" right. 
		//flip the 0
		mask <<= 1;
		while((mask & n) != 0){
			n = moveRight(n, mask);
			mask <<= 1;
		}
		n |= mask;
		out.println(n);
	}	
		
	public static long bruteForce(long n){
		int nNext = 0;
		int nOnes = ones(n);
		while(nOnes != nNext){
			n++;
			nNext = ones(n);
		}
		return n;
	}
	
	/* moves the "1" in the mask position to the last "0" before the next "1" 
	   This method requires mask to be in the position of the "1" being moved.*/
	private static long moveRight(long n, long mask){
		// flip 1 to 0 
		n = mask ^ n;
		
		// find the rightmost 0
		while(mask > 1){
			mask >>= 1;
			if((mask & n) == mask){
				mask <<= 1;
				break;
			}
		}
		
		n = mask ^ n;
		return n;
	}
	
	/* used in brute force solution, returns number of "1"s */
	public static int ones(long n){
		long mask = 1;
		int cnt = 0;
		while(mask <= n){
			if((mask & n) != 0){
				cnt++;
			}
			mask <<= 1;
		}
		return cnt;
	}

	public static void main(String args[]) {
		new TheNextNumber().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
