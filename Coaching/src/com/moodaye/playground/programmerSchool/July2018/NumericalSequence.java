package com.moodaye.playground.programmerSchool.July2018;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #341 */
// Time Taken = 72min
// Pattern:
/*
 * 0 1 2 3 4 5 6 7 8 9 ----> n = 10 
 * 10 21 30 41 50 61 70 81 90 ----> n = 19 
 * 111 200 311 400 511 600 711 800 911 ----> n = 28 
 * 2000 3111 4000 5111 6000 7111 8000 9111 ----> n = 28 + 8 
 * 20000 31111 40000 51111 60000 71111 80000 91111 -----> n = 28 + 8 + 8 
 * 200000 311111 400000 511111 600000 711111 800000 911111 -----> n = 28 + 8 + 8
 * Max length = (500 - 28) / 8 + 3 = 472 / 8 + 3 = 62. We can use an array of 70 ints.
 */

public class NumericalSequence {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
//		for (n = 0; n <= 500; n++) {
			int[] a = new int[70];
			int idx = 69; // to track the current length
			for (int i = 2; i <= n; i++) {

				// first 10
				if (i <= 10) {
					a[idx]++;
					continue;
				}

				// a[idx]
				if (a[idx] == 9) {
					idx--;
					if (i > 28) {
						a[idx] = 2;
					} else {
						a[idx] = 1;
					}
				} else {
					a[idx]++;
				}

				if (i <= 19) {
					a[idx + 1] = i % 2 == 1 ? 0 : 1;
					if(i == 12){
						a[idx + 1] = 2;
					}
					continue;
				}

				for (int j = idx + 1; j < 70; j++) {
					a[j] = a[70 - 1] == 1 ? 0 : 1;
				}
			}

			// print out sequence starting with idx'
		//	out.print(n + ",");
			for (int i = idx; i < 70; i++) {
				out.print(a[i]);
			}
		//	out.println();
//		}
	}

	public static void main(String args[]) {
		new NumericalSequence().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
