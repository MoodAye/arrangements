package com.moodaye.playground.programmerSchool.July2018;

import java.util.Scanner;
import java.util.Arrays;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #49 */
// Time Taken = 28min
public class Patterns {
	void solve(Scanner in, PrintWriter out) {
		char[] p1 = in.next().toCharArray();
		char[] p2 = in.next().toCharArray();
		int cnt = 1;
		for (int i = 0; i < p1.length; i++) {
			cnt *= getCommonCnt(p1[i], p2[i]);
		}
		out.println(cnt);
	}

	public static int getCommonCnt(char c1, char c2) {
		boolean[] cs1 = getCharSet(c1);
		boolean[] cs2 = getCharSet(c2);
		int cnt = 0;
		for(int i = 0; i < cs1.length; i++){
			if(cs1[i] && cs2[i]){
				cnt++;
			}
		}
		return cnt;
	}

	public static boolean[] getCharSet(char c) {
		boolean[] cs = new boolean[10];
		if ('0' <= c && c <= '9') {
			cs[c - '0'] = true;
		} else if (c == 'a') {
			cs[0] = true;
			cs[1] = true;
			cs[2] = true;
			cs[3] = true;
		} else if (c == 'a') {
			cs[0] = true;
			cs[1] = true;
			cs[2] = true;
			cs[3] = true;
		} else if (c == 'b') {
			cs[1] = true;
			cs[2] = true;
			cs[3] = true;
			cs[4] = true;
		} else if (c == 'c') {
			cs[2] = true;
			cs[3] = true;
			cs[4] = true;
			cs[5] = true;
		} else if (c == 'd') {
			cs[3] = true;
			cs[4] = true;
			cs[5] = true;
			cs[6] = true;
		} else if (c == 'e') {
			cs[4] = true;
			cs[5] = true;
			cs[6] = true;
			cs[7] = true;
		} else if (c == 'f') {
			cs[5] = true;
			cs[6] = true;
			cs[7] = true;
			cs[8] = true;
		} else if (c == 'g') {
			cs[6] = true;
			cs[7] = true;
			cs[8] = true;
			cs[9] = true;
		} else if (c == '?') {
			Arrays.fill(cs, true);
		}
		return cs;
	}

	public static void main(String args[]) {
		new Patterns().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
