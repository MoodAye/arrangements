package com.moodaye.playground.programmerSchool.July2018;

import java.util.Scanner;
import java.util.Arrays;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #444 */
// Time Taken ~3 hrs (testing took time --- found quite a few issues)
// WA on test 3
public class ListP444 {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int[] list = new int[n];

		for (int i = 0; i < n; i++) {
			list[i] = in.nextInt();
		}

		Arrays.sort(list);
		out.println(getStringList(list));
	}

	public static String getStringList(int[] list) {
		StringBuilder sb = new StringBuilder();
		StringBuilder cn = new StringBuilder();
		int n = list.length;
		int cnt = 0;
		sb.append(list[0]);
		for (int i = 1; i < n; i++) {
			if (list[i] == list[i - 1]) {
				continue;
			} else if (list[i] - list[i - 1] == 1) {
				cn.append(", ").append(list[i]);
				cnt++;
				continue;
			} else if (cnt == 0) {
				sb.append(", ").append(list[i]);
			} else if(cn.length() - (String.valueOf(list[i - 1]).length() + 2) <= 4){
				sb.append(cn.toString());
				cn.delete(0, cn.length());
				sb.append(", ").append(list[i]);
			}
			else {
				sb.append(", ..., ").append(list[i - 1]).append(", ").append(list[i]);
				cn.delete(0, cn.length());
			}
			cnt = 0;
		}
		if (cnt != 0) {
			if (cnt == 1 || cn.length() - (String.valueOf(list[n - 1]).length() + 2) <= 4){
				sb.append(cn.toString());
			} else {
				sb.append(", ..., ").append(list[n - 1]);
			}
		}
		return sb.toString();
	}

	public static void main(String args[]) {
		new ListP444().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
