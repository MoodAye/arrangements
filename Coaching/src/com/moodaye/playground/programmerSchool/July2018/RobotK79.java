package com.moodaye.playground.programmerSchool.July2018;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #235` */
//TIme Taken = ~1 hr 15min
public class RobotK79 {
	void solve(Scanner in, PrintWriter out) {
		String cmds = in.next();
		Step step = new Step();
		int cnt = 0;
		boolean[][] visited = new boolean[101][101];
		visited[50][50] = true;
		int x = 50;
		int y = 50;
		for (char c : cmds.toCharArray()) {
			if (c == 'S') {
				cnt++;
				if (step.getTowards() == Direction.UP) {
					x--;
				} else if (step.getTowards() == Direction.DOWN) {
					x++;
				} else if (step.getTowards() == Direction.RIGHT) {
					y++;
				} else if (step.getTowards() == Direction.LEFT) {
					y--;
				}

				if (visited[x][y]) {
					out.println(cnt);
					return;
				}
				visited[x][y] = true;
			} else {
				step.turn(c);
			}
		}
		out.println(-1);
	}

	public static void main(String args[]) {
		new RobotK79().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}

enum Direction {
	UP, RIGHT, DOWN, LEFT
}

class Step {
	Direction towards = Direction.UP;

	void turn(char dir) {
		switch (dir) {
		case 'L':
			switch (towards) {
			case UP:
				towards = Direction.LEFT;
				break;
			case RIGHT:
				towards = Direction.UP;
				break;
			case DOWN:
				towards = Direction.RIGHT;
				break;
			case LEFT:
				towards = Direction.DOWN;
				break;
			}
			break;
		case 'R':
			switch (towards) {
			case UP:
				towards = Direction.RIGHT;
				break;
			case RIGHT:
				towards = Direction.DOWN;
				break;
			case DOWN:
				towards = Direction.LEFT;
				break;
			case LEFT:
				towards = Direction.UP;
				break;
			}
			break;
		}
	}

	Direction getTowards() {
		return towards;
	}

}
