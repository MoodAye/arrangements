package com.moodaye.playground.programmerSchool;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

/** Programmer's School - #623 */
/** Problem reduces to finding a repetition pattern for the last digit of
 *  Fibonacci numbers.   Since each digit depends on the preceeding
 *  two (order matters) - the maximum cycle is 90 (99 - 10 + 1).
 *  E.g., 2 is preceeded by 1,1 (11).  3 is preceeded by 1,2 (12).  
 *  5 is preceeded by 2,3 (23)
 * @author Rajiv
 */
public class FibonacciAgain {
	void solve(Scanner in, PrintWriter out) {
		int fibN = in.nextInt();
//		out.println(lastDigitCyclePattern()[fibN % 60]);
		out.println(simpleLastDigit(fibN));
	}
	
	public static int simpleLastDigit(int n){
		int sld = 1;
		int n_2 = 1;
		int n_1 = 1;
		for(int i = 2; i <= n; i++){
			sld = (n_2 + n_1) % 10;
			n_2 = n_1;
			n_1 = sld;
		}
		return sld;
	}
	
	public static int[] lastDigitCyclePattern(){
		int[] mem = new int[100];
		int[] pattern = new int[100];
		int num1 = 1;
		int num2 = 1;
		mem[0] = 1;
		mem[1] = 1;
		int len = 2;
		do{
			pattern[num1 * 10 + num2] = 1;
			int nextFib = num1 + num2;
			num1 = num2;
			num2 = nextFib % 10;
			mem[len] = num2;
			len++;
		} while(pattern[num1 * 10 + num2] != 1);
		return mem;
	}

	public static void main(String[] args) {
		new FibonacciAgain().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
