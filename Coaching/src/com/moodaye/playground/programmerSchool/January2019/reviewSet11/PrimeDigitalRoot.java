package com.moodaye.playground.programmerSchool.January2019.reviewSet11;

import java.util.Scanner;
import java.util.Arrays;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #292 */
// ~25min
public class PrimeDigitalRoot {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		out.println(pdr(n));
	}

	public static int pdr(int n) {
		if (isPrime(n)) {
			return n;
		}
		if (n < 10) {
			return 0;
		}
		return pdr(sumDigits(n));
	}

	public static int sumDigits(int n) {
		char[] cn = String.valueOf(n).toCharArray();
		int sum = 0;
		for (char c : cn) {
			sum += c - '0';
		}
		return sum;
	}

	public static boolean isPrime(int n) {
		if (n == 1) {
			return false;
		}
		boolean[] isPrime = new boolean[(int) Math.sqrt(n) + 1];
		Arrays.fill(isPrime, true);
		isPrime[0] = false;
		isPrime[1] = false;
		for (int i = 2; i <= Math.sqrt(n); i++) {
			if (isPrime[i]) {
				if (n % i == 0) {
					return false;
				}
				mark(isPrime, i);
			}
		}
		return true;
	}
	private static void mark(boolean[] isPrime, int p){
		for(int i = p * p; i < isPrime.length; i += p){
			isPrime[i] = false;
		}
	}

	public static void main(String args[]) {
		new PrimeDigitalRoot().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
