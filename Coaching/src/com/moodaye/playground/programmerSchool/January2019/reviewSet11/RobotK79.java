package com.moodaye.playground.programmerSchool.January2019.reviewSet11;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #235 */
// ~30min
public class RobotK79 {
	void solve(Scanner in, PrintWriter out) {
		char[] path = in.next().toCharArray();
		Point[] points = new Point[path.length + 1];
		Point current = new Point(0, 0, Point.DIR.E);
		int idx = 0;
		points[idx] = current;
		
		for(char c : path){
			current = new Point(points[idx].x, points[idx].y, points[idx].d);
			if(c == 'S'){
				idx++;
				if(current.d == Point.DIR.E){
					current.x += 1;
				}
				else if(current.d == Point.DIR.W){
					current.x -= 1;
				}
				else if(current.d == Point.DIR.N){
					current.y += 1;
				}
				else{
					current.y -= 1;
				}
			}
			else if(c == 'L'){
				if(current.d == Point.DIR.E){
					current.d = Point.DIR.N;
				}
				else if(current.d == Point.DIR.W){
					current.d = Point.DIR.S;
				}
				else if(current.d == Point.DIR.N){
					current.d = Point.DIR.W;
				}
				else{
					current.d = Point.DIR.E;
				}
			}
			else if(c == 'R'){
				if(current.d == Point.DIR.E){
					current.d = Point.DIR.S;
				}
				else if(current.d == Point.DIR.W){
					current.d = Point.DIR.N;
				}
				else if(current.d == Point.DIR.N){
					current.d = Point.DIR.E;
				}
				else{
					current.d = Point.DIR.W;
				}
			}
			if(visited(current, points, idx)){
				out.println(idx);
				return;
			}
			points[idx] = current;
		}
		out.println(-1);
	}
	
	public boolean visited(Point current, Point[] points, int idx){
		for(int i = 0; i < idx; i++){
			if(current.equals(points[i])){
				return true;
			}
		}
		return false;
	}

	public static void main(String args[]) {
		new RobotK79().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}

class Point{
	enum DIR {E, S, W, N};
	
	int x;
	int y;
	DIR d;
	
	Point(int x, int y, DIR d){
		this.x = x;
		this.y = y;
		this.d = d;
	}
	boolean equals(Point p){
		return (p.x == x && p.y == y);
	}
}
