package com.moodaye.playground.programmerSchool.January2019.reviewSet11;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #49 */
// ~30min
public class Patterns {
	void solve(Scanner in, PrintWriter out) {
		char[] p1 = in.next().toCharArray();
		char[] p2 = in.next().toCharArray();
		int combos = 1;
		for (int i = 0; i < p1.length; i++) {
			combos *= intersect(p1[i], p2[i]);
		}
		out.println(combos);
		int xx = Integer.MAX_VALUE;
	}

	int intersect(char x, char y) {
		int[] cnt = new int[10];
		updateCnt(cnt, x);
		updateCnt(cnt, y);
		int n = 0;
		for(int i = 0; i < 10; i++){
			if(cnt[i] == 2){
				n++;
			}
		}
		return n;
	}

	void updateCnt(int[] cnt, char x) {
		if ('0' <= x && x <= '9') {
			cnt[x - '0']++;
		}
		else if (x == '?') {
			for (int i = 0; i <= 9; i++) {
				cnt[i]++;
			}
		}
		else {
			for (int i = 0 + x - 'a'; i <= x - 'a' + 3; i++) {
				cnt[i]++;
			}
		}
	}

	public static void main(String args[]) {
		new Patterns().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
