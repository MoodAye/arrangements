package com.moodaye.playground.programmerSchool.January2019.reviewSet4;

import java.util.Scanner;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;

/** Programmer's School #408 */
// ~20 min
public class Letter {
	void solve(Scanner in, PrintWriter out) {
		int w = in.nextInt();
		int n = in.nextInt();
		in.nextLine();
		List<String> sList = new LinkedList<>();
		for (int i = 0; i < n; i++) {
			String s = in.nextLine().trim();

			if (s.length() > w) {
				out.println("Impossible.");
				return;
			} else {
				int sp = (w - s.length());
				String preSpace = "";
				for (int k = 0; k < sp / 2; k++) {
					preSpace += " ";
				}
				String postSpace = preSpace;
				if (sp % 2 != 0) {
					postSpace += " ";
				}
				sList.add(preSpace + s + postSpace);
			}
		}
		for (String sf : sList) {
			out.println(sf);
		}
	}

	public static void main(String args[]) {
		new Letter().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in, "cp866");
				PrintWriter out = new PrintWriter(new OutputStreamWriter(System.out, "cp866"))) {
			solve(in, out);
		}
		catch(Exception e){
			
		}
	}
}
