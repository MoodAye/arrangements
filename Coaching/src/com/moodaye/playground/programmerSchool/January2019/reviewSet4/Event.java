package com.moodaye.playground.programmerSchool.January2019.reviewSet4;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #339 */
// 25min
public class Event {
	final static int[] daysInMnth = { 0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

	void solve(Scanner in, PrintWriter out) {
		in.useDelimiter("\\.|\\s+");
		int sd = in.nextInt();
		int sm = in.nextInt();
		int sy = in.nextInt();
		int ed = in.nextInt();
		int em = in.nextInt();
		int ey = in.nextInt();
		int start = daysSince0(sd, sm, sy);
		int end = daysSince0(ed, em, ey);

		out.println(end - start + 1);
	}

	private static int daysSince0(int d, int m, int y) {
		int days = 0;
		for (int i = 0; i < y; i++) {
			if ((i % 4 == 0 && i % 100 != 0) || (i % 400 == 0)) {
				days += 366;
			} else {
				days += 365;
			}
		}
		days += d;
		
		for(int i = 1; i < m; i++){
			days += daysInMnth[i];
		}
		if (m > 2) {
			if ((y % 4 == 0 && y % 100 != 0) || (y % 400 == 0)) {
				days += 1;
			}
		}
		
		return days;
	}

	public static void main(String args[]) {
		new Event().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}

}
