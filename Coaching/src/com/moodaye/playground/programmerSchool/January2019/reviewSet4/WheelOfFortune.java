package com.moodaye.playground.programmerSchool.January2019.reviewSet4;

import java.util.Locale;
import java.util.Scanner;
import java.io.PrintWriter;

/** Programmer's School #17 */
public class WheelOfFortune {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int[] numbs = new int[n - 1]; // skip last no. as it is repeat
		for (int i = 0; i < n - 1; i++) {
			numbs[i] = in.nextInt();
		}
		for (int i = 0; i < n - 1; i++) {
			if (numbs[i] == numbs[n - 2]) { 
				if ((n - 1) % (i + 1) == 0) { // optimization
					if (repeats(numbs, i)) {
						out.println(i + 1);
						return;
					}
				}
			}
		}
	}

	public static boolean repeats(int[] numbs, int idx) {
		if (idx == numbs.length - 1) {
			return true;
		}
		int j = 0;
		for (int i = idx + 1; i < numbs.length; i++, j++) {
			if (j == idx + 1) { // loop through the first occurence of pattern
				j = 0;
			}
			if (numbs[i] != numbs[j]) {
				return false;
			}
		}
		return j == idx + 1; // Additional check - has j reached end of pattern?
	}

	public static void main(String args[]) {
		new WheelOfFortune().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}