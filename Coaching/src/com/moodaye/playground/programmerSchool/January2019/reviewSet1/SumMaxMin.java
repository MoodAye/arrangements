package com.moodaye.playground.programmerSchool.January2019.reviewSet1;

import java.util.Scanner;
import java.util.StringTokenizer;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #272 */
// Test 11 Mem Limit exceeded - fixed by using StringTokenizer.
// Not sure why mem was exceeded.   200_000 ints ~ 800_000 bytes.  Plus the string which is 200_000 bytes.  
// The limit is 32 Mb. 
// Time taken ~35min
public class SumMaxMin {
	void solve(Scanner in, PrintWriter out) {
		String strn = in.nextLine();
		StringTokenizer st = new StringTokenizer(strn);
		boolean odd = true;
		int max = Integer.MIN_VALUE;
		int min = Integer.MAX_VALUE;
		while(st.hasMoreTokens()){
			int n = Integer.valueOf(st.nextToken());
			if(odd){
				if(n < min){
					min = n;
				}
			}
			else{
				if(n > max){
					max = n;
				}
			}
			odd = !odd;
		}
		out.println(max + min);
	}

	public static void main(String args[]) {
		new SumMaxMin().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
