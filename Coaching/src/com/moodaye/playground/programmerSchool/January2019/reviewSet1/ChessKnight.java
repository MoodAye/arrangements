package com.moodaye.playground.programmerSchool.January2019.reviewSet1;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School # */
public class ChessKnight {
	void solve(Scanner in, PrintWriter out) {
		String start = in.next();
		int sr = start.charAt(1) - '1';
		int sc = start.charAt(0) - 'a';
		for (int i = 0; i < 8; i++) {
			for (int j = 0; j < 8; j++) {
				int dx = (sr - i);
				int dy = (sc - j);
				if (dx * dx + dy * dy == 5) {
					out.println((char) ('a' + sc - dy) + "" + (char) ('1' + sr - dx));
				}
			}
		}
	}

	public static void main(String args[]) {
		new ChessKnight().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
