package com.moodaye.playground.programmerSchool.January2019.reviewSet1;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #44 */
// 6:56pm // 10min + 20 min to fix issue.
public class Arrows {
	void solve(Scanner in, PrintWriter out) {
		int cnt = 0;
		if (in.hasNext()) {
			char[] arr = in.next().toCharArray();
			for (int i = 0; i < arr.length - 4; i++) {
				if (arr[i] == '<' && arr[i + 1] == '-' && arr[i + 2] == '-' && arr[i + 3] == '<' && arr[i + 4] == '<') {
					cnt++;
				} else if (arr[i] == '>' && arr[i + 1] == '>' && arr[i + 2] == '-' && arr[i + 3] == '-'
						&& arr[i + 4] == '>') {
					cnt++;
				}
			}
		}

		out.println(cnt);
	}

	public static void main(String args[]) {
		new Arrows().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
