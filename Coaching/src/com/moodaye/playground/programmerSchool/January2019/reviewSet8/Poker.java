package com.moodaye.playground.programmerSchool.January2019.reviewSet8;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #347 */
// ~30 min
// WA -- test 5?
public class Poker {
	void solve(Scanner in, PrintWriter out) {
		int[] cnts = new int[14];
		for(int i = 0; i < 5; i++){
			cnts[in.nextInt()]++;
		}
		boolean three = false;
		boolean pair = false;
		for(int i : cnts){
			if(i == 5){
				out.println("Impossible");
				return;
			}
			if(i == 4){
				out.println("Four of a Kind");
				return;
			}
			if(i == 3){
				if(pair){
					out.println("Full House");
					return;
				}
				three = true;
				continue;
			}
			if(i == 2){
				if(pair){
					out.println("Two Pairs");
					return;
				}
				if(three){
					out.println("Full House");
					return;
				}
				pair = true;
				continue;
			}
		}
		if(pair){
			out.println("One Pair");
			return;
		}
		if(three){
			out.println("Three of a Kind");
			return;
		}
		for(int i = 1; i <= 9; i++){
			if(cnts[i] == 1){
				boolean straight = true;
				for(int j = 1; j <= 4; j++){
					if(cnts[i + j] != 1){
						straight = false;
						break;
					}
				}
				if(straight){
					out.println("Straight");
					return;
				}
			}
		}
		out.println("Nothing");
	}

	public static void main(String args[]) {
		new Poker().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}

}
