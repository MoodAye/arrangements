package com.moodaye.playground.programmerSchool.January2019.reviewSet8;

import java.util.Locale;
import java.io.InputStream;
import java.io.PrintWriter;

/** Programmer's School #360 */
// ~1hr
public class MaximumTriple {
	// Codify one step left, right, up or down.
	// E.g., (x + dx[0], y + dy[0]) represent going left from (x,y)
	private static short[] dx = { -1, +1, 0, 0 };
	private static short[] dy = { 0, 0, -1, +1 };

	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		short[][] table = new short[n][n];
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				table[i][j] = (short) in.nextInt();
			}
		}

		short max = Short.MIN_VALUE;
		short sum = (short) 0;
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				for (int step1 = 0; step1 < 4; step1++) {
					if (inTable(table, i, j, step1)) {
						// Why does this not work? o
						// sum = table[i][j] + table[i + dx[step1]][j + dy[step1]];
						sum = table[i][j];
						sum += table[i + dx[step1]][j + dy[step1]];
					} else {
						continue;
					}
					for (int step2 = 0; step2 < 4; step2++) {
						int temp = sum;
						if (inTable(table, i + dx[step1], j + dy[step1], step2)) {
							// cannot use the first element (i, j) again
							if (i + dx[step1] + dx[step2] == i && j + dy[step1] + dy[step2] == j) {
								continue;
							}
							temp += table[i + dx[step1] + dx[step2]][j + dy[step1] + dy[step2]];
							max = (short) Math.max((int) max, (int) temp);
						} else {
							continue;
						}
					}
				}
			}
		}
		out.println(max);
	}

	private static boolean inTable(short[][] table, int i, int j, int step) {
		int len = table.length;
		return (0 <= i + dx[step] && i + dx[step] < len && 0 <= j + dy[step] && j + dy[step] < len);
	}

	public static void main(String args[]) {
		new MaximumTriple().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}

class Scanner implements AutoCloseable {
	InputStream is;
	int pos = 0;
	int size = 0;
	byte[] buffer = new byte[1024];

	Scanner(InputStream is) {
		this.is = is;
	}

	int nextChar() {
		if (pos >= size) {
			try {
				size = is.read(buffer);
			} catch (java.io.IOException e) {
				throw new java.io.IOError(e);
			}
			pos = 0;
			if (size == -1) {
				return -1;
			}
		}
		Assert.check(pos < size);
		int c = buffer[pos] & 0xFF;
		pos++;
		return c;
	}

	int nextInt() {
		int c = nextChar();
		while (c == ' ' || c == '\r' || c == '\n' || c == '\t') {
			c = nextChar();
		}
		if (c == '-') {
			c = nextChar();
			Assert.check('0' <= c && c <= '9');
			int n = -(c - '0');
			c = nextChar();
			while ('0' <= c && c <= '9') {
				int d = c - '0';
				c = nextChar();
				Assert.check(
						n > Integer.MIN_VALUE / 10 || n == Integer.MIN_VALUE / 10 && d <= -(Integer.MIN_VALUE % 10));
				n = n * 10 - d;
			}
			return n;
		} else {
			Assert.check('0' <= c && c <= '9');
			int n = c - '0';
			c = nextChar();
			while ('0' <= c && c <= '9') {
				int d = c - '0';
				c = nextChar();
				Assert.check(n < Integer.MAX_VALUE / 10 || n == Integer.MAX_VALUE / 10 && d <= Integer.MAX_VALUE % 10);
				n = n * 10 + d;
			}
			return n;
		}
	}

	@Override
	public void close() {
	}
}

class Assert {
	static void check(boolean e) {
		if (!e) {
			throw new AssertionError();
		}
	}
}