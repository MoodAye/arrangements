package com.moodaye.playground.programmerSchool.January2019.reviewSet8;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #310 */
// ~15 min
public class FrameOfCells {
	void solve(Scanner in, PrintWriter out) {
		int t = in.nextInt();
		for(int i = 0; i < t; i++){
			int x = in.nextInt();
			int y = in.nextInt();
			int a = in.nextInt();
		
			if( ( x % a == 0 && (y - 2) % a == 0) ||
				((x - 2) % a == 0 && y % a == 0) ||
				((x - 1) % a == 0 && (y - 1) % a == 0) ||
				((x - 1) % a == 0 && y % a == 0 && (y - 2) % a ==  0) ||
				((x - 2) % a == 0 && x % a == 0 && (y - 1) % a ==  0)){
				out.print(1);
			}
			else{
				out.print(0);
			}
		}
	}

	public static void main(String args[]) {
		new FrameOfCells().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
