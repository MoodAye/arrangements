package com.moodaye.playground.programmerSchool.January2019.reviewSet6;

import java.util.Scanner;
import java.util.Arrays;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #39 */
// Time = 20min (original) +10 for optimization
public class HairyBusiness {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int[] costs = new int[n];
		for(int i = 0; i < n; i++){
			costs[i] += in.nextInt();
		}
		
		// Scan from right to left ... picking the max 
		int max = costs[n - 1];
		int sum = 0;
		for(int i = n - 1; i >= 0; i--){
			max = Math.max(max, costs[i]);
			sum += max;
		}
		out.println(sum);
	}

	public static void main(String args[]) {
		new HairyBusiness().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
