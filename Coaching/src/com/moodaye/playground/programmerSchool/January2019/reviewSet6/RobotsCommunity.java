package com.moodaye.playground.programmerSchool.January2019.reviewSet6;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #166 */
public class RobotsCommunity {
	void solve(Scanner in, PrintWriter out) {
		int k = in.nextInt();
		int n = in.nextInt();
		int[] ageCnt = new int[4]; // ageCnt[3] = cnt of 3 year old robots
		ageCnt[0] = k;
		n--;
		while (n > 0) {
			n--;
			// shift ages
			ageCnt[3] = ageCnt[2];
			ageCnt[2] = ageCnt[1];
			ageCnt[1] = ageCnt[0];

			// make new robots
			int newRobots = 0;
			int groupOf5 = k / 5;
			int groupOf3 = (k % 5) / 3;
			newRobots = groupOf5 * 9 + groupOf3 * 5;

			for (int t = 1; t <= 3; t++) {
				groupOf5--;
				if(groupOf5 < 0){
					break;
				}
				groupOf3 = (k - (groupOf5 * 5)) / 3;
				newRobots = Math.max(groupOf5 * 9 + groupOf3 * 5, newRobots);
			}
			
			k += newRobots;
			ageCnt[0] = newRobots;

			// deduct dead robots
			k -= ageCnt[3];
			
		}
		out.println(k);
	}

	public static void main(String args[]) {
		new RobotsCommunity().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
