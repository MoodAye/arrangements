package com.moodaye.playground.programmerSchool.January2019.reviewSet6;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #376 */
// Time Taken = 18min
public class Birthday2 {
	void solve(Scanner in, PrintWriter out) {
		int bd = in.nextInt();
		int bm = in.nextInt();
		int cd = in.nextInt();
		int cm = in.nextInt();
		int cy = in.nextInt();
		
		int days = 0;
		while(bd != cd || bm != cm){
			days++;
			cd++;
			if((cd == 32 && (cm == 1 || cm == 3 || cm == 5 || cm == 7 || cm == 8 || cm == 10 || cm == 12))
			|| (cd == 31 && (cm == 4 || cm == 6 || cm == 9 || cm == 11)) 
			|| (cd == 30 && cm == 2 && leap(cy))
			|| (cd == 29 && cm == 2 && !leap(cy))){
				cd = 1;
				cm++;
				if(cm == 13){
					cm = 1;
					cy++;
				}
			}
		}
		out.println(days);
	}
	
	public static boolean leap(int cy){
		return (cy % 400 == 0 || ((cy % 4 == 0) && (cy % 100 != 0)));
	}

	public static void main(String args[]) {
		new Birthday2().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}

}
