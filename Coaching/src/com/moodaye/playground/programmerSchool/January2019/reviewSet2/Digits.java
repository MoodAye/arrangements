package com.moodaye.playground.programmerSchool.January2019.reviewSet2;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #303 */
// Time Taken ~25min
public class Digits {
	void solve(Scanner in, PrintWriter out) {
		String s = in.next();
		char[] an = s.toCharArray();
		int max = Integer.MIN_VALUE;
		for(int i = 0; i < an.length; i++){
			int sum = doSum(an, i);
			if(sum > max){
				max = sum;
			}
		}
		out.println(max);
	}
	
	private static int doSum(char[] a, int skip){
		int sign = 1;
		int sum = 0;
		for(int i = 0; i < a.length; i++){
			if (i == skip){
				continue;
			}
			sum += sign * (a[i] - '0');
			sign *= -1;
		}
		return sum;
	}
	
	private static int[] toArray(int n){
		int len = 0;
		int temp = n;
		while(temp > 0){
			len++;
			temp /= 10;
		}
		
		int[] an = new int[len];
		for(int i = len - 1; i >= 0; i--){
			an[i] = n % 10;
			n /= 10;
		}
		
		return an;
	}

	public static void main(String args[]) {
		new Digits().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}