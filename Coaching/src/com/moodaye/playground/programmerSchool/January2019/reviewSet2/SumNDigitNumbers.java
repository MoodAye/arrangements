package com.moodaye.playground.programmerSchool.January2019.reviewSet2;

import java.util.Scanner;
import java.util.Arrays;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #682 */
// Time taken < 30 min.
// n = 4; (1000 + 9999) * 9000 / 2 = 10999 * 4500
public class SumNDigitNumbers {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		if(n == 1){
			out.println((0 + 9) * 10 / 2);
			return;
		}
		int[] f1 = new int[n + 1]; // 10999
		Arrays.fill(f1, 9);
		f1[0] = 1;
		f1[1] = 0;
		
		int[] f2 = new int[n];  // 4500
		f2[0] = 4;
		f2[1] = 5;
		int[] sum = mult(f1, f2);
		out.println(toString(sum));
	}
	
	public static String toString(int[] sum){
		StringBuilder sb = new StringBuilder();
		for(int i = 0; i < sum.length; i++){
			sb.append(sum[i]);
		}
		return sb.toString();
	}

	public static int[] mult(int[] a, int[] b) {
		if (a.length < b.length) {
			int[] temp = a;
			a = b;
			b = temp;
		}

		int[] prod = new int[a.length + b.length];
		int carry = 0;
		for (int i = b.length - 1; i >= 0; i--) {
			for(int j = a.length - 1; j >= 0; j--){
				int pi = i + j + 1;
				prod[pi] += a[j] * b[i] + carry;
				carry = prod[pi] / 10;
				prod[pi] %= 10;
			}
			int p = prod.length - a.length;
			while(carry > 0){
				prod[p--] = carry % 10;
				carry /= 10;
			}
		}

		return pruneZeros(prod);
	}
	
	public static int[] pruneZeros(int[] prod){
		int cnt = 0;
		for(int i = 0; i < prod.length; i++){
			if(prod[i] > 0){
				break;
			}
			cnt++;
		}
		
		int[] pruned = new int[prod.length - cnt];
		for(int i = 0; i < pruned.length; i++){
			pruned[i] = prod[i + cnt];
		}
		return pruned;
	}

	public static void main(String args[]) {
		new SumNDigitNumbers().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
