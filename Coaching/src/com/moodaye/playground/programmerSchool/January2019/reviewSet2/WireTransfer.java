package com.moodaye.playground.programmerSchool.January2019.reviewSet2;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #316 */
// 9:47pm - 9:55pm ... time taken = 8min
public class WireTransfer {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int max = 0;
		int cost = 0;
		while(n >= 107){
			n -= 107;
			max += 100;
			cost += 7;
		}
		if(n > 7){
			cost += 7;
			max += n - 7;
		}
		out.printf("%d %d", max, cost);
	}

	public static void main(String args[]) {
		new WireTransfer().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
