package com.moodaye.playground.programmerSchool.January2019.reviewSet10;

import java.util.Scanner;
import java.util.Arrays;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #32 */
// ~30min
public class MaximumDifference {
	void solve(Scanner in, PrintWriter out) {
		int a = in.nextInt();
		int b = in.nextInt();
		int[] arrA = toArray(Math.abs(a));
		int[] arrB = toArray(Math.abs(b));
		int signA = a < 0 ? -1 : 1;
		int signB = b < 0 ? -1 : 1;

		if (a < 0) {
			getMinVersion(arrA);
		} else {
			getMaxVersion(arrA);
		}

		if (b > 0) {
			getMinVersion(arrB);
		} else {
			getMaxVersion(arrB);
		}

		out.println(signA * toInt(arrA) - signB * toInt(arrB));
	}
	
	private static void getMinVersion(int[] a){
		Arrays.sort(a);
		moveLeadingZeros(a);
	}
	
	private static void getMaxVersion(int[] a){
		Arrays.sort(a);
		reverse(a);
	}

	private static void reverse(int[] a) {
		for (int i = 0; i < a.length / 2; i++) {
			exch(a, i, a.length - i - 1);
		}
	}

	private static void moveLeadingZeros(int[] a) {
		int idx = 0;
		while (a[idx] == 0) {
			idx++;
		}
		exch(a, 0, idx);
	}

	private static int toInt(int[] a) {
		int n = 0;
		for (int i : a) {
			n = n * 10 + i;
		}
		return n;
	}

	private static void exch(int[] a, int i, int j) {
		int temp = a[i];
		a[i] = a[j];
		a[j] = temp;
	}

	private static int[] toArray(int a) {
		char[] arrAc = String.valueOf(a).toCharArray();
		int[] arrA = new int[arrAc.length];
		for(int i = 0; i < arrAc.length; i++){
			arrA[i] = arrAc[i] - '0';
		}
		return arrA;
	}

	public static void main(String args[]) {
		new MaximumDifference().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
