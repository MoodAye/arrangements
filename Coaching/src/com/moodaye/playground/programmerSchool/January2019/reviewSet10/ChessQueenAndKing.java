package com.moodaye.playground.programmerSchool.January2019.reviewSet10;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #412 */
// time = 28min
public class ChessQueenAndKing {
	void solve(Scanner in, PrintWriter out) {
		String wk = in.next();
		String wq = in.next();
		String bk = in.next();
		char wkx = wk.charAt(0);
		char wky = wk.charAt(1);
		char wqx = wq.charAt(0);
		char wqy = wq.charAt(1);
		char bkx = bk.charAt(0);
		char bky = bk.charAt(1);
		
		for(int dx = -1; dx <= 1; dx++){
			for(int dy = -1; dy <= 1; dy++){
				if(dx * dx + dy * dy > 0){
					char x = wqx;
					char y = wqy;
					while('a' <= x && x <= 'h' && '1' <= y && y <= '8'){
						x += dx;
						y += dy;
						if(x == wkx && y == wky){
							break;
						}
						if(x == bkx && y == bky){
							out.println("YES");
							return;
						}
					}
				}
			}
		}
		out.println("NO");
	}
	
	void solve2(Scanner in, PrintWriter out) {
		String wk = in.next();
		String wq = in.next();
		String bk = in.next();
		char wkx = wk.charAt(0);
		char wky = wk.charAt(1);
		char wqx = wq.charAt(0);
		char wqy = wq.charAt(1);
		char bkx = bk.charAt(0);
		char bky = bk.charAt(1);
		
		for(char x = wqx; x <= 'h' && bky == wqy; x++){
			if(x == wkx && wqy == wky){
				break;
			}
			if(x == bkx){
				out.println("YES");
				return;
			}
		}
		
		for(char x = wqx; x >= 'a' && bky == wqy; x--){
			if(x == wkx && wqy == wky){
				break;
			}
			if(x == bkx){
				out.println("YES");
				return;
			}
		}
		
		for(char y = wqy; y <= '8' && bkx == wqx; y++){
			if(y == wky && wqx == wkx){
				break;
			}
			if(y == bky){
				out.println("YES");
				return;
			}
		}
		
		for(char y = wqy; y >= '1' && bkx == wqx; y--){
			if(y == wky && wqx == wkx){
				break;
			}
			if(y == bky){
				out.println("YES");
				return;
			}
		}
		
		char x = wqx;
		char y = wqy;
		while(x < 'h' && y < '8'){
			x++;
			y++;
			if(y == wky && x == wkx){
				break;
			}
			if(y == bky && x == bkx){
				out.println("YES");
				return;
			}
		}
		
		x = wqx;
		y = wqy;
		while(x > 'a' && y > '1'){
			x--;
			y--;
			if(y == wky && x == wkx){
				break;
			}
			if(y == bky && x == bkx){
				out.println("YES");
				return;
			}
		}
		
		x = wqx;
		y = wqy;
		while(x > 'a' && y < '8'){
			x--;
			y++;
			if(y == wky && x == wkx){
				break;
			}
			if(y == bky && x == bkx){
				out.println("YES");
				return;
			}
		}
		
		x = wqx;
		y = wqy;
		while(x < 'h' && y > '1'){
			x++;
			y--;
			if(y == wky && x == wkx){
				break;
			}
			if(y == bky && x == bkx){
				out.println("YES");
				return;
			}
		}
		out.println("NO");
	}
	
	public static void main(String args[]) {
		new ChessQueenAndKing().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
