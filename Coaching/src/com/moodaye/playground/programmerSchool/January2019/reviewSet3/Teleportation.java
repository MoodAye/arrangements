package com.moodaye.playground.programmerSchool.January2019.reviewSet3;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School 330 */
// ~15min
public class Teleportation {
	void solve(Scanner in, PrintWriter out) {
		int x1 = in.nextInt();
		int y1 = in.nextInt();
		int x2 = in.nextInt();
		int y2 = in.nextInt();
		int dx = Math.abs(x1 - x2);
		int dy = Math.abs(y1 - y2);
		
		if((dx + dy) % 2 == 1){
			out.println(0);
		}
		else if(dx == dy){
			out.println(1);
		}
		else{ 
			out.println(2);
		}
	}

	public static void main(String args[]) {
		new Teleportation().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}

}
