package com.moodaye.playground.programmerSchool.January2019.reviewSet3;

import java.util.Scanner;
import java.util.Arrays;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #314 */
// Complexity = O(nlogn) for sorting and O(n) for search.
public class LexicographicOrder {
	void solve2(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int k = in.nextInt();
		String[] sa = new String[n];
		for(int i = 1; i <= n; i++){
			sa[i - 1] = String.valueOf(i);
		}
		
		Arrays.sort(sa, String::compareTo);
		String sk = String.valueOf(k);
		for(int i = 0; i < sa.length; i++){
			if(sk.equals(sa[i])){
				out.println(i + 1);
			}
		}
	}

	/* Refined solution - performance - worst case is 10_000 * n */
	// this is significantly faster -- not clear why though
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int k = in.nextInt();
		String[] sa = new String[n];
		for(int i = 1; i <= n; i++){
			sa[i - 1] = String.valueOf(i);
		}
		String sk = String.valueOf(k);
		int cntLess = 0;
		for(int i = 0; i < n; i++){
			if(sa[i].compareTo(sk) < 0){
				cntLess++;
			}
		}
		out.println(cntLess + 1);
	}
	
	public static void main(String args[]) {
		new LexicographicOrder().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}

}
