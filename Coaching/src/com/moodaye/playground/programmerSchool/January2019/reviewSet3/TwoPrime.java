package com.moodaye.playground.programmerSchool.January2019.reviewSet3;

import java.util.Scanner;
import java.util.Arrays;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #516 */
// Time taken = 24 minutes
public class TwoPrime {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int[] arrayN = toArray(n);
		Arrays.sort(arrayN);
		int[] revArrayN = new int[arrayN.length];
		for(int i = 0; i < arrayN.length; i++){
			revArrayN[i] = arrayN[arrayN.length - i - 1]; 
		}
		if(isPrime(arrayN) && isPrime(revArrayN)){
			out.println("Yes");
		}
		else{
			out.println("No");
		}
	}
	public static boolean isPrime(int[] a){
		int n = 0;
		for(int i = 0; i < a.length; i++){
			n = a[i] + 10 * n;
		}
		return isPrime(n);
	}
	
	
	public static int[] toArray(int n){
		int len = 0;
		int temp = n;
		while(temp > 0){
			len++;
			temp /= 10;
		}
		int[] arrayN = new int[len];
		while(n > 0){
			arrayN[len-- - 1] = n % 10;
			n /= 10;
		}
		return arrayN;
	}
	
	public static boolean isPrime(int n){
		if(n == 1){
			return false;
		}
		for(int i = 2; i * i <= n; i++){
			if(n % i == 0){
				return false;
			}
		}
		return true;
	}

	public static void main(String args[]) {
		new TwoPrime().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}