package com.moodaye.playground.programmerSchool.January2019.reviewSet5;
import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #282 */
// Time taken ~30min
public class Rectangles {
	void solve(Scanner in, PrintWriter out) {
		int w = in.nextInt();
		int h = in.nextInt();
		long cnt = 0;
		for(int wi = 1; wi <= w; wi++){
			for(int hi = 1; hi <= h; hi++){
				cnt += (h - hi + 1) * (w - wi + 1); // why does this not overflow? 
			}
		}
		out.println(cnt);
		
	}

	public static void main(String args[]) {
		new Rectangles().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}

}
