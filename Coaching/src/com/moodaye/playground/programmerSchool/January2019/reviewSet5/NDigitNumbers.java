package com.moodaye.playground.programmerSchool.January2019.reviewSet5;

import java.util.Scanner;
import java.util.Arrays;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #672 */
// ~1hr -- took a while to figure out how to write the allPerms logic
public class NDigitNumbers {
	private static int cnt = 0;
	private static int min = -1;
	
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		
		if(n == 1){
			out.println("10 0");
			return;
		}

		int[] an = new int[n];
		Arrays.fill(an, 1);
		allPerms(an, 0);
		out.println(cnt + " " + min);
	}
	
	private static boolean isSpecial(int[] n){
		int sum = 0;
		int prod = 1;
		for(int i : n){
			sum += i;
			prod *= i;
		}
		return sum == prod;
	}
	
	private static int toInt(int[] an){
		int n = 0;
		for(int i : an){
			n = n * 10 + i;
		}
		return n;
	}

	/* returns cnt of all permutations of the digits of n */
	private static int getCnt(int[] n){
		int cnt = java.util.stream.IntStream.rangeClosed(2, n.length).reduce(1, (a, b) -> a * b);
		int[] digits = new int[10];
		for(int i : n){
			digits[i]++;
		}
		for(int i : digits){
			if(i > 1){
				cnt /= java.util.stream.IntStream.rangeClosed(2, i).reduce(1, (a, b) -> a * b);
			}
		}
		
		return cnt;
	}

	public static void allPerms(int[] n, int idx) {
		if (idx == n.length) {
			if(isSpecial(n)){
				cnt += getCnt(n);
				if(min == -1) min = toInt(n);
			}
			return;
		}
		int i;
		if (idx == 0)
			i = 1;
		else
			i = n[idx - 1];
		for (; i <= 9; i++) {
			n[idx] = i;
			allPerms(n, idx + 1);
		}
	}

	public static void main(String args[]) {
		new NDigitNumbers().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
