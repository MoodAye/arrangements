package com.moodaye.playground.programmerSchool.January2019.reviewSet5;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #203 */
// WA 18
public class TextShift {
	void solve(Scanner in, PrintWriter out){
		String a = in.next();
		String b = in.next();
		String dblA = a + a;
		int idx = dblA.lastIndexOf(b);
		out.println(idx <= 0 ? idx : a.length() - idx);
	}
	
	
	void solve2(Scanner in, PrintWriter out) {
		char[] a = in.next().toCharArray();
		char[] b = in.next().toCharArray();
		char[] dblA = new char[a.length * 2];
		int min = Integer.MAX_VALUE;
		for(int i = 0; i < dblA.length; i++){
			dblA[i] = a[i == 0 ? 0 : (i % a.length)];
		}
		for(int i = 0; i < a.length; i++){
			if(dblA[i] == b[0]){
				boolean match = true;
				for(int j = i + 1; j < b.length; j++){
					if(dblA[j] != b[j - i]){
						match = false;
					}
				}
				if(match == true){
					min = Math.min(min, i == 0 ? 0 : a.length - i);
				}
			}
		}
		out.println(min == Integer.MAX_VALUE ? -1 : min);
	}
	
	

	public static void main(String args[]) {
		new TextShift().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
