package com.moodaye.playground.programmerSchool.January2019.reviewSet9;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #456 */
// Took a long time - initial time spent = 1.5 hr... then time to fix bugs (3 iterations)
public class ChestOfBillyBones {
	void solve(Scanner in, PrintWriter out) {
		int x = in.nextInt();
		int y = in.nextInt();

		// guess coins year x -1
		int gcoins2 = y;
	
		int coins = 0; // current year
		int coins1 = 0; // next year (curr year + 1)
		int coins2 = 0; // curr year + 2
	
		// go backwards from x starting with current year = x - 3
		do{
			coins2 = y;
			coins1 = y - gcoins2;
			for (int i = x - 2; i >= 1; i--) {
				coins = coins2 - coins1;
				if(coins < 0){
					break;
				}
				coins2 = coins1;
				coins1 = coins;
			}
			if(coins > 0 && coins > coins2){
				break;
			}
			gcoins2--;
		}while(true);
		out.printf("%d %d", coins, coins2);
	}

	public static void main(String args[]) {
		new ChestOfBillyBones().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
