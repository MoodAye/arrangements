package com.moodaye.playground.programmerSchool.January2019.reviewSet9;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #102 */
// Time taken - 10 min
public class TriangleAndPoint {
	void solve(Scanner in, PrintWriter out) {
		int x1 = in.nextInt();
		int y1 = in.nextInt();
		int x2 = in.nextInt();
		int y2 = in.nextInt();
		int x3 = in.nextInt();
		int y3 = in.nextInt();
		int x4 = in.nextInt();
		int y4 = in.nextInt();
		
		// Inside if sum of area of three triangles made with point equals the area of the triangle
		int a124 = Math.abs(x1 * (y2 - y4) + x2 * (y4 - y1) + x4 * (y1 - y2));
		int a234 = Math.abs(x3 * (y2 - y4) + x2 * (y4 - y3) + x4 * (y3 - y2));
		int a314 = Math.abs(x3 * (y1 - y4) + x1 * (y4 - y3) + x4 * (y3 - y1));
		int a123 = Math.abs(x3 * (y1 - y2) + x1 * (y2 - y3) + x2 * (y3 - y1));
		
		if(a123 == (a124 + a234 + a314)){
			out.println("In");
		}
		else{
			out.println("Out");
		}
	}

	public static void main(String args[]) {
		new TriangleAndPoint().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
