package com.moodaye.playground.programmerSchool.January2019.reviewSet9;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #602 */
// ~30min
public class PointsOnLine {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int minStart = in.nextInt();
		int minEnd = in.nextInt();
		int currStart = minStart;
		int currEnd = minEnd;
		for (int i = 2; i < n; i++) {
			currStart = currEnd;
			currEnd = in.nextInt();
			if(currEnd - currStart < minEnd - minStart){
				minStart = currStart;
				minEnd = currEnd;
			}
		}
		out.printf("%d %d", minStart, minEnd);
	}

	public static void main(String args[]) {
		new PointsOnLine().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
