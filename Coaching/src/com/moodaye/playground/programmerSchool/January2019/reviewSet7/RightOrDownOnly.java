package com.moodaye.playground.programmerSchool.January2019.reviewSet7;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #165 */
public class RightOrDownOnly {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int m = in.nextInt();
		int[][] field = new int[n][m];
		for(int i = 0; i < n; i++){
			for(int j = 0; j < m; j++){
				field[i][j] = in.nextInt();
			}
		}
		
		// cnt of paths to cell
		int[][] ptc = new int[n][m]; 
		ptc[0][0] = 1;
		
		for(int r = 0; r < n; r++){
			for(int c = 0; c < m; c++){
				if(r == n - 1 && c == m - 1){
					break;
				} 
			    if(ptc[r][c] == 0){ // (optional) no path to this cell 
			    	continue;
			    }
				if(r + field[r][c] < n){
					ptc[r + field[r][c]][c] += ptc[r][c];
				}
				if(c + field[r][c] < m){
					ptc[r][c + field[r][c]] += ptc[r][c];
				}
			}
		}
		out.println(ptc[n - 1][m - 1]);
	}
	
	
	public static void main(String args[]) {
		new RightOrDownOnly().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
