package com.moodaye.playground.programmerSchool.January2019.reviewSet7;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #120 */
// Use greedy approach ... starting with target (bottom right cell)
// > 2hrs.  Took time figuring out code for parsing through grid.
public class MinimumPathInTable {
	void solve(Scanner in, PrintWriter out) {
		int ht = in.nextInt();
		int wd = in.nextInt();
		int[][] g = new int[ht][wd];
		for (int r = 0; r < ht; r++) {
			for (int c = 0; c < wd; c++) {
				g[r][c] = in.nextInt();
			}
		}
	
		// make 0 indexed
		int h = ht - 1;
		int w = wd - 1;
		
		// bottom border
		for(int i = w - 1; i >= 0; i--){
			g[h][i] += g[h][i + 1];
		}
		
		// right border
		for(int i = h - 1; i >= 0; i--){
			g[i][w] += g[i + 1][w];
		}
		
		while(h != 0 || w != 0){
			h = h == 0 ? 0: --h;
			w = w == 0 ? 0: --w;
			g[h][w] += Math.min(g[h == ht - 1 ? h : h + 1][w], g[h][w == wd  - 1? w : w + 1]);
			for(int i = h - 1; i >= 0; i--){
				if(w == 0){
					break;
				}
				g[i][w] += Math.min(g[i + 1][w], g[i][w + 1]);
			}
			for(int i = w - 1; i >= 0; i--){
				if(h == 0){
					break;
				}
				g[h][i] += Math.min(g[h][i + 1], g[h + 1][i]);
			}
		}
		out.println(g[0][0]);
	}

	public static void main(String args[]) {
		new MinimumPathInTable().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
