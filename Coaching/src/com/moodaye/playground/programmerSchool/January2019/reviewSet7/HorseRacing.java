package com.moodaye.playground.programmerSchool.January2019.reviewSet7;

import java.util.Scanner;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School #185 */
// ~50 min
//MLE - test 12 with original approach using graph algorithm
// simpler approach passed all tests
public class HorseRacing {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int k = in.nextInt();
		boolean[] slowerHorses = new boolean[n + 1];
		boolean kAppears = false;
		while(true){
			int h1 = in.nextInt();
			if(h1 == 0){
				break;
			}
			if(h1 == k){
				kAppears = true;
			}
			int h2 = in.nextInt();
			if(h2 == k){
				out.println("No");
				return;
			}
			slowerHorses[h2] = true;
		}
		for(int h = 1; h <= n; h++){
			if(h == k){
				continue;
			}
			if(!slowerHorses[h]){
				out.println("No");
				return;
			}
		}
		out.println("Yes");
	}
	
	public static void main(String args[]) {
		new HorseRacing().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
