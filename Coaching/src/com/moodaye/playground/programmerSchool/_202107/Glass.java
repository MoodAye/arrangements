package com.moodaye.playground.programmerSchool._202107;

import java.io.PrintWriter;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.Locale;
import java.util.Scanner;

// Problem 
public class Glass {
	
	void solve3(Scanner in, PrintWriter out) {
		out.println(solve2(in.nextInt()));
//		for(int i = 0; i < 10_000; i++) {
//			if(solve2(i) != solve1(i)) {
//				out.println(i);
//				out.printf("%d %d", 1, solve1(i));
//				out.printf("%d %d", 2, solve2(i));
//				return;
//			}
//		}
	}
	
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		solve2(n);
		solve1(n);
	}
	
	
	long solve2(int n) {
		long[] cnt1 = new long[n + 1];
		long[] cnt2 = new long[n + 1];
		long[] cnt3 = new long[n + 1];
		
		Arrays.fill(cnt1, 0L);
		Arrays.fill(cnt2, 0L);
		Arrays.fill(cnt3, 0L);
		
		if(n <= 9 || (n >= 13 && n <= 19)) {
			return 0;
		}
		
		if(n == 10 || n == 11 || n == 12) {
			return 2;
		}
		
		cnt1[10] = 2L;
		cnt2[11] = 2L;
		cnt3[12] = 2L;
		
		for(int i = 20; i <= n; i++) {
			cnt1[i] = (cnt1[i - 10] + cnt2[i - 10] + cnt3[i - 10]) % 1_000_000L;
			cnt2[i] = cnt1[i - 1];
			cnt3[i] = cnt2[i - 1];
		}
		
		return (cnt1[n] + cnt2[n] + cnt3[n]) % 1000000L;
		
	}
	
	long solve1(int n) {
		BigInteger[] cnt1 = new BigInteger[n + 1];
		BigInteger[] cnt2 = new BigInteger[n + 1];
		BigInteger[] cnt3 = new BigInteger[n + 1];
		
		Arrays.fill(cnt1, BigInteger.ZERO);
		Arrays.fill(cnt2, BigInteger.ZERO);
		Arrays.fill(cnt3, BigInteger.ZERO);
		
		if(n <= 9 || (n >= 13 && n <= 19)) {
			return 0;
		}
		
		if(n == 10 || n == 11 || n == 12) {
			return 2;
		}
		
		cnt1[10] = BigInteger.TWO;
		cnt2[11] = BigInteger.TWO;
		cnt3[12] = BigInteger.TWO;
		
		for(int i = 20; i <= n; i++) {
			cnt1[i] = cnt1[i - 10].add(cnt2[i - 10]).add(cnt3[i - 10]);
			cnt2[i] = cnt1[i - 1];
			cnt3[i] = cnt2[i - 1];
		}
		
		return Long.valueOf(cnt1[n].add(cnt2[n]).add(cnt3[n]).mod(BigInteger.valueOf(1000000L)).toString());
		
	}

	public static void main(String[] args) {
		new Glass().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve3(in, out);
		}
	}
}
