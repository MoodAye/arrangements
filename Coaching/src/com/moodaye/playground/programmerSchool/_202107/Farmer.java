package com.moodaye.playground.programmerSchool._202107;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

// Problem 
public class Farmer {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int[][] plot = new int[n + 1][n + 1];
		for (int i = 1; i <= n; i++) {
			char[] line = in.next().toCharArray();
			for (int j = 1; j <= n; j++) {
				plot[i][j] = line[j - 1] - '0';
			}
		}
		
		int max = 0;
		for (int i = 1; i <= n; i++) {
			for (int j = 1; j <= n; j++) {
				if(plot[i][j] == 0) {
					continue;
				}
				plot[i][j] = Math.min(plot[i -1][j - 1], 
						Math.min(plot[i - 1][j], plot[i][j -1])) + 1;
				max = Math.max(max, plot[i][j]);
			}
		}
		out.println(max * max);
	}

	public static void main(String[] args) {
		new Farmer().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
