package com.moodaye.playground.programmerSchool._202107;

import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Locale;
import java.util.Scanner;

// Problem 
public class Route {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int[][] nums = new int[n + 1][n + 1];
		char[][] path = new char[n + 1][n + 1];

		Arrays.fill(nums[0], Integer.MAX_VALUE);
		for (int row = 0; row <= n; row++) {
			nums[row][0] = Integer.MAX_VALUE;
		}

		for (int i = 1; i <= n; i++) {
			char[] line = in.next().toCharArray();
			for (int j = 1; j <= n; j++) {
				nums[i][j] = line[j - 1] - '0';
			}
		}

		for (int i = 1; i <= n; i++) {
			for (int j = 1; j <= n; j++) {
				if(j == 1 && i == 1) continue;
				if (nums[i - 1][j] < nums[i][j - 1]) {
					nums[i][j] += nums[i - 1][j];
					path[i][j] = 'T';
				} else {
					nums[i][j] += nums[i][j - 1];
					path[i][j] = 'L';
				}
			}
		}

		int i = n;
		int j = n;
		char next = '.';

		while (i != 1 || j != 1) {
			next = path[i][j];
			path[i][j] = '#';
			if (next == 'L') {
				j--;
			} else {
				i--;
			}
		}

		path[1][1] = '#';
		
		for (i = 1; i <= n; i++) {
			for (j = 1; j <= n; j++) {
				out.print(path[i][j] == '#' ? '#' : '.');
			}
			out.println();
		}
	}

	public static void main(String[] args) {
		new Route().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
