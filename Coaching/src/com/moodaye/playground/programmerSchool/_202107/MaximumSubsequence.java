package com.moodaye.playground.programmerSchool._202107;

import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Locale;
import java.util.Scanner;

// Problem 
public class MaximumSubsequence {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int[] nums = new int[n];
		for (int i = 0; i < nums.length; i++) {
			nums[i] = in.nextInt();
		}
		
		int[] cnts = new int[n];
		Arrays.fill(cnts, 1);
		
		for(int i = 0; i < n; i++) {
			for(int j = i - 1; j >= 0; j--) {
				if(nums[i] > nums[j]) {
					cnts[i] = Math.max(cnts[i], cnts[j] + 1);
				}
			}
		}
		out.println(Arrays.stream(cnts).max().getAsInt());
	}

	public static void main(String[] args) {
		new MaximumSubsequence().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
