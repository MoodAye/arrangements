package com.moodaye.playground.programmerSchool._202107;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

// Problem 
public class ComputerGame {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int[] hts = new int[n];
		for (int i = 0; i < hts.length; i++) {
			hts[i] = in.nextInt();
		}

		int prev = 0;
		int prev_1 = 0;
		int minE = 0;
		for (int i = 1; i < hts.length; i++) {
			int step1 = Math.abs(hts[i] - hts[i - 1]);
			int step2 = Integer.MAX_VALUE;
			if (i > 1) {
				step2 = 3 * Math.abs(hts[i] - hts[i - 2]);
			}
			minE = Math.min(prev_1 + step2, prev + step1);
			prev_1 = prev;
			prev = minE;
		}
		out.println(minE);
	}

	public static void main(String[] args) {
		new ComputerGame().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
