package com.moodaye.playground.programmerSchool._202107;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

// Problem 
public class BallOnStair {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		
		long currStepCnt = 1;
		long step_1Cnt = 1;
		long step_2Cnt = 0;
		long step_3Cnt = 0;
		
		for(int step = n; step > 0; step--) {
			currStepCnt = step_1Cnt + step_2Cnt + step_3Cnt;
			step_3Cnt = step_2Cnt;
			step_2Cnt = step_1Cnt;
			step_1Cnt = currStepCnt;
		}
		out.println(currStepCnt);
	}

	public static void main(String[] args) {
		new BallOnStair().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
