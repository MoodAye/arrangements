package com.moodaye.playground.programmerSchool._202107;

import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Locale;
import java.util.Scanner;

// Problem 
public class MinimumCostOfTravel {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int[][] costs = new int[n + 1][n + 1];
		for (int start = 0; start <= n - 1; start++) {
			for (int end = start + 1; end <= n ; end++) {
				costs[start][end] = in.nextInt();
			}
		}
		
		int[] minCosts = new int[n + 1];
		Arrays.fill(minCosts, Integer.MAX_VALUE);
		minCosts[0] = 0;
		
		for (int end = 1; end <= n; end++) {
			for (int start = 0; start <= end - 1 ; start++) {
				minCosts[end] = Math.min(minCosts[end], minCosts[start] + costs[start][end]);
			}
		}
		
		out.println(minCosts[n]);
		
	}
	
	/*          station 0:   7 10 20
	 * 			station 1:      4  8
	 * 			station 2:         2	
	 * 
	 */
	
	public static void main(String[] args) {
		new MinimumCostOfTravel().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
