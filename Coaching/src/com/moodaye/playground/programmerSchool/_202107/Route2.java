package com.moodaye.playground.programmerSchool._202107;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

// Problem 
public class Route2 {

	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int k = in.nextInt();
		int[][] grid = new int[n + 2][n + 2];
		for (int i = 1; i <= n; i++) {
			for (int j = 1; j <= n; j++) {
				grid[i][j] = in.nextInt();
			}
		}

		// create grids to track sums and current step locations
		int[][] maxSum = new int[n + 2][n + 2];
		boolean[][] currStep = new boolean[n + 2][n + 2];

		// seed step 1
		maxSum[1][1] = grid[1][1];
		currStep[1][1] = true;

		for (int stepNumber = 2; stepNumber <= k; stepNumber++) {
			boolean[][] nextStep = new boolean[n + 2][n + 2];
			for (int row = 1; row <= n; row++) {
				for (int col = 1; col <= n; col++) {
					int maxValue = 0;
					if (currStep[row - 1][col]) {
						maxValue = Math.max(maxValue, maxSum[row - 1][col]);
						nextStep[row][col] = true;
					}
					if (currStep[row + 1][col]) {
						maxValue = Math.max(maxValue, maxSum[row + 1][col]);
						nextStep[row][col] = true;
					}
					if (currStep[row][col - 1]) {
						maxValue = Math.max(maxValue, maxSum[row][col - 1]);
						nextStep[row][col] = true;
					}
					if (currStep[row][col + 1]) {
						maxValue = Math.max(maxValue, maxSum[row][col + 1]);
						nextStep[row][col] = true;
					}
					if(nextStep[row][col]) {
						maxSum[row][col] = maxValue + grid[row][col];
					}
				}
			}
			currStep = nextStep;
		}
		
		int max = 0;
		for (int row = 1; row <= n; row++) {
			for (int col = 1; col <= n; col++) {
				max = Math.max(max, maxSum[row][col]);
			}
		}
		
		out.println(max);
	}

	public static void main(String[] args) {
		new Route2().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
