package com.moodaye.playground.programmerSchool._202107;

import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Scanner;

// Problem 674   745am - 758
public class DeviceSelection {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		
	 /*
	  * 2 = 0
	  * 3 = 1
	  * 4 = 0
	  * 5 = 3 + 2 = n(3) + n(2) = 0
	  * 6 = 3 + 3 = n(3) + n(3)
	  * 7 = 4 + 3 = n(4) + n(3)
	  * 8 = 4 + 4 = 0
	  * 9 = 5 + 4 = n(3)
	  * 10 = 5 + 5 = 2*N(3)
	  * 
	  * 100 = 50 + 50.
	  * 50 = 25 + 25
	  * 25 = 13 + 12
	  * 13 = 7 + 6
	  * 6 = 3 + 3 = 2
	  * 7 = 4 + 3 + 1
	  * 13 = 3
	  * 12 = 6 + 6 = 4
	  * 25 = 7
	  * */
		
		Map<Integer, Integer> mem = new HashMap<Integer, Integer>();
		mem.put(0, 0);
		mem.put(1, 0);
		mem.put(2, 0);
		mem.put(3, 1);
		out.println(doDiv(n, mem));
			
	}
	
	private int doDiv(int n, Map<Integer, Integer> memo) {
		if(n == 3) {
			return 1;
		}
		if(n <= 2) {
			return 0;
		}
		if(memo.containsKey(n)) {
			return memo.get(n);
		}
		
		int first = n / 2;
		int second = (n + 1) / 2;
		
		memo.put(n, doDiv(first, memo) + doDiv(second, memo));
		return memo.get(n);
	}

	public static void main(String[] args) {
		new WordChain().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
