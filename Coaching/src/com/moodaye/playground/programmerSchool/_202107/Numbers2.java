package com.moodaye.playground.programmerSchool._202107;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

// Problem 
// TLE
public class Numbers2 {
	
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int m = in.nextInt();
		int len = in.nextInt();
		int mod = (int) Math.pow(10, len);
		
		
		String line = in.next();
		int[] digits = new int[n];
		for (int i = 0; i < n ; i++) {
			digits[i] = line.charAt(i) - '0';
		}
		
		long[] cnts = new long[n];
		for (int i = 1; i < cnts.length; i++) {
			int multiplier = 1;
			long numberFormed = 0;
			
			if(digits[i - 1] == 0) {
				cnts[i] = cnts[i - 1];
			}
			else {
				cnts[i] = 2 * cnts[i -1];
			}
			
			if(i >= len && cnts[i - len] != 0)  {
				cnts[i] -= cnts[i - len];
			}
			
			
			for(int j = i; j >= 0; j--) {
				// number cannot begin with 0
				if(j != i && digits[j] == 0) {
					multiplier *= 10;
					continue;
				}
				numberFormed = multiplier * digits[j] + numberFormed;
				if(numberFormed > m) {
					break;
				}
				cnts[i] += (j == 0 ? 1L :cnts[j - 1]) % mod;
				multiplier *= 10;
			}
		}
		out.println(cnts[n - 1]);
	}

	public static void main(String[] args) {
		new Numbers2().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
