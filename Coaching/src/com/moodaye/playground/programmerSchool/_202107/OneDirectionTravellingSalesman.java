package com.moodaye.playground.programmerSchool._202107;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

// Problem 
public class OneDirectionTravellingSalesman {
	void solve(Scanner in, PrintWriter out) {
		int h = in.nextInt();
		int w = in.nextInt();
		int[][] wts = new int[h + 2][w + 1];

		int[][] paths = new int[h + 2][w + 1];

		for (int edge = 0; edge <= w; edge++) {
			wts[0][edge] = Integer.MAX_VALUE;
			wts[h + 1][edge] = Integer.MAX_VALUE;
		}
		for (int edge = 0; edge <= h; edge++) {
			wts[edge][0] = Integer.MAX_VALUE;
		}

		for (int r = 1; r <= h; r++) {
			for (int c = 1; c <= w; c++) {
				wts[r][c] = in.nextInt();
			}
		}

		for (int cols = 2; cols <= w; cols++) {
			for (int rows = 1; rows <= h; rows++) {
				if (wts[rows - 1][cols - 1] <= wts[rows][cols - 1]) {
					if (wts[rows - 1][cols - 1] <= wts[rows + 1][cols - 1]) {
						paths[rows][cols] = rows - 1;
						wts[rows][cols] += wts[rows - 1][cols - 1];
					} else {
						paths[rows][cols] = rows + 1;
						wts[rows][cols] += wts[rows + 1][cols - 1];
					}
				}
				else if (wts[rows][cols - 1] <= wts[rows + 1][cols - 1]) {
					paths[rows][cols] = rows;
					wts[rows][cols] += wts[rows][cols - 1];
				} else {
					paths[rows][cols] = rows + 1;
					wts[rows][cols] += wts[rows + 1][cols - 1];
				}
			}
		}

		List<Integer> minRows = new ArrayList<>();
		int minWt = Integer.MAX_VALUE;

		for (int row = 1; row <= h; row++) {
			minWt = Math.min(minWt, wts[row][w]);
		}
		for (int row = 1; row <= h; row++) {
			if (wts[row][w] == minWt) {
				minRows.add(row);
			}
		}

		int[][] path = new int[minRows.size()][w + 1];
		for (int i = 0; i < minRows.size(); i++) {
			path[i][w] = minRows.get(i);
			for (int j = w - 1; j >= 0; j--) {
				path[i][j] = paths[path[i][j + 1]][j + 1];
			}
		}
		
		StringBuilder sbBest = null;
		for (int p = 0; p < path.length; p++) {
			StringBuilder sbCurr = new StringBuilder();
			for (int e = 1; e < path[0].length; e++) {
				sbCurr.append(path[p][e]).append(" ");
			}
			if(sbBest == null || sbBest.toString().compareTo(sbCurr.toString()) > 0) {
				sbBest = sbCurr;
			}
		}
		
		System.out.println(sbBest.toString());
		out.println(minWt);
	}

	public static void main(String[] args) {
		new OneDirectionTravellingSalesman().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
