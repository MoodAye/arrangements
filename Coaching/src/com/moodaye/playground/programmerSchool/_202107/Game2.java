package com.moodaye.playground.programmerSchool._202107;

import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Scanner;

// Problem 
public class Game2 {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int[] nums = new int[n];
		for (int i = 0; i < nums.length; i++) {
			nums[i] = in.nextInt();
		}
		
		int winAmt = winAmount(nums, 0, n - 1, new HashMap<String, Integer>());
		out.println(winAmt > 0 ? 1 : winAmt < 0 ? 2 : 0);
	}
	
	private int winAmount(int[] a, int startIdx, int endIdx, Map<String, Integer> memo) {
		String key = startIdx + "," + endIdx;
		if(memo.containsKey(key)) {
			return memo.get(key);
		}
		
		if(startIdx == endIdx) {
			memo.put(key, a[startIdx]);
			return a[startIdx];
		}
		
		if(startIdx + 1 == endIdx) {
			memo.put(key, Math.abs(a[startIdx] - a[endIdx]));
			return Math.abs(a[startIdx] - a[endIdx]);
		}
		
		int winAmt1 = a[startIdx] - winAmount(a, startIdx + 1, endIdx, memo);
		int winAmt2 = a[endIdx] - winAmount(a, startIdx, endIdx - 1, memo);
		
		int winAmt = Math.max(winAmt1, winAmt2);
		memo.put(key, winAmt);
		return winAmt;
	}

	public static void main(String[] args) {
		new Game2().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
