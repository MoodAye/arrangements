package com.moodaye.playground.programmerSchool._202107;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;

// Problem 
public class Sums {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int[] nums = new int[n];
		for (int i = 0; i < nums.length; i++) {
			nums[i] = in.nextInt();
		}
		
		Set<Integer> sums = new TreeSet<>();
		sums.add(0);
		doSums(nums, 0, sums);
		out.println(sums.size());
	}
	
	
	private void doSums(int[] nums, int idx, Set<Integer> sums) {
		if(idx == nums.length) {
			return;
		}
		
		// add element at idx
		Set<Integer> temp = new TreeSet<>();
		for(int sum : sums) {
			temp.add(sum + nums[idx]);
		}
		sums.addAll(temp);
		doSums(nums, idx + 1, sums);
	}

	public static void main(String[] args) {
		new Sums().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
