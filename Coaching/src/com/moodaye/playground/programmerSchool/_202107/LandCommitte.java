package com.moodaye.playground.programmerSchool._202107;

import java.io.InputStream;
import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;


// Problem 
public class LandCommitte {
	void solve(FastScanner in, PrintWriter out) {
		int h = in.nextInt();
		int w = in.nextInt();
		int n = in.nextInt();
		int[][] cost = new int[h + 1][w + 1];
		int[][] tot = new int[h + 1][w + 1];
		for (int i = 1; i <= h; i++) {
			for (int j = 1; j <= w; j++) {
				cost[i][j] = in.nextInt();
				tot[i][j] = cost[i][j] + tot[i - 1][j] + tot[i][j - 1] - tot[i - 1][j - 1];
			}
		}

		for (int i = 0; i < n; i++) {
			int x1 = in.nextInt();
			int y1 = in.nextInt();
			int x2 = in.nextInt();
			int y2 = in.nextInt();
			out.println(tot[x2][y2] + tot[x1 - 1][y1 - 1] - tot[x1 - 1][y2] - tot[x2][y1 - 1]);
		}
	}

	public static void main(String[] args) {
		new LandCommitte().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (FastScanner in = new FastScanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}

	static class FastScanner implements AutoCloseable {

		InputStream is;
		byte buffer[] = new byte[1 << 16];
		int size = 0;
		int pos = 0;

		FastScanner(InputStream is) {
			this.is = is;
		}

		int nextChar() {
			if (pos >= size) {
				try {
					size = is.read(buffer);
				} catch (java.io.IOException e) {
					throw new java.io.IOError(e);
				}
				pos = 0;
				if (size == -1) {
					return -1;
				}
			}
			Assert.check(pos < size);
			int c = buffer[pos] & 0xFF;
			pos++;
			return c;
		}

		int nextInt() {
			int c = nextChar();
			while (c == ' ' || c == '\r' || c == '\n' || c == '\t') {
				c = nextChar();
			}
			if (c == '-') {
				c = nextChar();
				Assert.check('0' <= c && c <= '9');
				int n = -(c - '0');
				c = nextChar();
				while ('0' <= c && c <= '9') {
					int d = c - '0';
					c = nextChar();
					Assert.check(n > Integer.MIN_VALUE / 10
							|| n == Integer.MIN_VALUE / 10 && d <= -(Integer.MIN_VALUE % 10));
					n = n * 10 - d;
				}
				return n;
			} else {
				Assert.check('0' <= c && c <= '9');
				int n = c - '0';
				c = nextChar();
				while ('0' <= c && c <= '9') {
					int d = c - '0';
					c = nextChar();
					Assert.check(
							n < Integer.MAX_VALUE / 10 || n == Integer.MAX_VALUE / 10 && d <= Integer.MAX_VALUE % 10);
					n = n * 10 + d;
				}
				return n;
			}
		}

		@Override
		public void close() {
		}
	}

	static class Assert {
		static void check(boolean e) {
			if (!e) {
				throw new AssertionError();
			}
		}
	}
}