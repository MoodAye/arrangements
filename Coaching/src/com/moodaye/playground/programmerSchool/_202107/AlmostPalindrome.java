package com.moodaye.playground.programmerSchool._202107;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

// Problem 
// TLE
public class AlmostPalindrome {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int k = in.nextInt();
		String word = in.next();
		
		int cnt = 0;
		for(int len = 1; len <= word.length(); len++) {
			for(int idx = 0; idx + len <= word.length(); idx++) {
				if(isAlmostPalindrome(word.substring(idx, idx + len), k)) {
					cnt++;
				}
			}
		}
		out.println(cnt);
	}
	
	private boolean isAlmostPalindrome(String word, int k) {
		for(int i = 0; i < word.length() / 2; i++) {
			if(word.charAt(i) != word.charAt(word.length() - (i + 1))) {
				k--;
			}
		}
		return k >= 0 ? true : false;
	}
			

	public static void main(String[] args) {
		new AlmostPalindrome().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
