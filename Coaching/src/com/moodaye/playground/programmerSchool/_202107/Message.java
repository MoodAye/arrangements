package com.moodaye.playground.programmerSchool._202107;

import java.io.PrintWriter;
import java.math.BigInteger;
import java.util.Locale;
import java.util.Scanner;

// Problem 
public class Message {
void solve(Scanner in, PrintWriter out) {
	char[] line = in.next().toCharArray();
	
	BigInteger bicnt = BigInteger.ONE;
	BigInteger biprevCnt = BigInteger.ONE;
	BigInteger biprevPrevCnt = BigInteger.ZERO;
	
	for (int i = 1; i < line.length; i++) {
		biprevPrevCnt = biprevCnt;
		biprevCnt = bicnt;
		if(line[i -1] == '1' || line[i - 1] == '2' || 
				(line[i -1] == '3' && line[i] <= '3')){
			bicnt = bicnt.add(biprevPrevCnt);
		}
	}
	
	out.printf(bicnt.toString());
}

public static void main(String[] args) {
	new Message().run();
}

void run() {
	Locale.setDefault(Locale.US);
	try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
		solve(in, out);
	}
}
}
