package com.moodaye.playground.programmerSchool._202107;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

// Problem 
public class ChocolateBar {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		long[] combos = new long[n + 1];
		
		if(n % 2 == 1) {
			out.println(0);
			return;
		}
		
		if(n == 2) {
			out.println(3);
			return;
		}
		
		combos[2] = 3L;
		combos[4] = 11L;
		
		for(int i = 6; i <= n; i++) {
			combos[i] = combos[i - 2] * 4L - combos[i - 4];
		}
		
		out.println(combos[n]);
		
	}

	public static void main(String[] args) {
		new ChocolateBar().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
