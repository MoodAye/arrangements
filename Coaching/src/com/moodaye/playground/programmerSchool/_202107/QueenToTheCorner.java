package com.moodaye.playground.programmerSchool._202107;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;

// Problem 
public class QueenToTheCorner {
	void solve(Scanner in, PrintWriter out) {
		int m = in.nextInt();
		int n = in.nextInt();
		
		// make a list of rows, columns, and diags that will
		// determine whether the first player wins.  
		// These are rows, columns, diags that allow the player
		// to reach a square that ensures victory.  The first square 
		// that ensures victory is 1,1.  Subsequent squares are those that will 
		// not allow the second player to reach 1,1 and by extension a square 
		// that ensures victory for player to play first.
		
		Set<Integer> winRows = new TreeSet<Integer>();
		Set<Integer> winCols = new TreeSet<Integer>();
		
		/* diags = row - col */
		Set<Integer> winDiags = new TreeSet<Integer>();
		
		// start by adding row, col, and diag for square 1,1
		winRows.add(1);
		winCols.add(1);
		winDiags.add(0);
		
		for(int k = 2; k < Math.max(m, n); k++) {
			for(int d = 1; d <= k; d++) {
				if(!winCols.contains(d) && !winDiags.contains(k - d)) {
					// cell (k, d) is a loser
					winCols.add(d);
					winRows.add(k);
					winDiags.add(k - d);
				}
				if(!winRows.contains(d) && !winDiags.contains(d - k)) {
					// cell (d, k) is a loser
					winRows.add(d);
					winCols.add(k);
					winDiags.add(d - k);
				}
			}
		}
		
		if(winRows.contains(m) || winCols.contains(n) || winDiags.contains(m - n)) {
			out.println(1);
		}
		else {
			out.println(2);
		}
	}

	public static void main(String[] args) {
		new QueenToTheCorner().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
