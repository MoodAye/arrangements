package com.moodaye.playground.programmerSchool._202107;

import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Locale;
import java.util.Scanner;

// Problem  1251am
public class Change {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int[] coins = new int[n + 1];
		for (int i = 1; i <= n; i++) {
			coins[i] = in.nextInt();
		}

		int k = in.nextInt();
		int[] vals = new int[k];
		for (int i = 0; i < vals.length; i++) {
			vals[i] = in.nextInt();
		}

		int maxAmt = Arrays.stream(vals).max().getAsInt();
		boolean[][] canMake = new boolean[coins.length][maxAmt + 1];

		for (int coinIdx = 1; coinIdx < coins.length; coinIdx++) {
			for (int amt = 0; amt <= maxAmt; amt++) {
				if (amt == 0) {
					canMake[coinIdx][amt] = true;
					continue;
				}
				if (canMake[coinIdx - 1][amt]) {
						canMake[coinIdx][amt] = true;
				}
				if (amt - coins[coinIdx] >= 0 && 
					canMake[coinIdx][amt - coins[coinIdx]]) {
						canMake[coinIdx][amt] = true;
				}
			}
		}
		
		for(int i = 0; i < k; i++) {
			out.printf("%d ", canMake[n][vals[i]] ? 1 : 0);
		}
	}

	public static void main(String[] args) {
		new Change().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
