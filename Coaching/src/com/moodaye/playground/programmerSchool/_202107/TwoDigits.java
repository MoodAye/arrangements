package com.moodaye.playground.programmerSchool._202107;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

// Problem 
public class TwoDigits {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		long sd = 2;
		long dd = 0;
		
		for(int i = 2; i <= n; i++) {
			long temp = dd;
			dd = sd;
			sd += temp;
		}
		
		out.println(sd + dd);
	}

	public static void main(String[] args) {
		new TwoDigits().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
