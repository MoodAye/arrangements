package com.moodaye.playground.programmerSchool._202107;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Map;
import java.util.Scanner;
import java.util.stream.IntStream;
import java.util.Arrays;
import java.util.HashMap;

// Problem 
public class ThreePrimeNumbers {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();

		boolean[] prime = threeDigitPrimes();
		
		if(n == 3) {
			out.println(IntStream.range(101, 1000).filter(i -> prime[i]).count());
			return;
		}
		

		Map<Integer, Long> twoDigitEndings = new HashMap<>();
		for (int i = 101; i <= 999; i++) {
			if(i % 100 < 10) {
				continue;
			}
			if (prime[i]) {
				twoDigitEndings.merge(i % 100, 1L, Long::sum);
			}
		}

		for (int i = 4; i <= n; i++) {
			
			// for each item in twoDigitEndings - add an odd number and see if it makes
			// a 3 digit prime. If so, add it's last two digits to the new map
			Map<Integer, Long> tempMap = new HashMap<>();
			
			for (int number : twoDigitEndings.keySet()) {
				for (int k = 1; k <= 9; k += 2) {
					int newNumber = Integer.valueOf(number * 10 + k);
					if(prime[newNumber]) {
						tempMap.merge(newNumber % 100, twoDigitEndings.get(number), 
								(a, b) -> {
									long sum = 1L * (a + b) % (1_000_000_000L + 9L);
									return sum;
								});
					}
				}
			}
			twoDigitEndings = tempMap;
		}
		
		out.println(twoDigitEndings.values().stream().reduce(0L, Long::sum) % 1_000_000_009L);
	}

	private boolean[] threeDigitPrimes() {
		boolean seive[] = new boolean[1000];
		Arrays.fill(seive, true);
		seive[0] = false;
		seive[1] = false;
		for (int i = 2; i < seive.length; i++) {
			if (seive[i]) {
				mask(seive, i);
			}
		}

		return seive;
	}

	private void mask(boolean[] seive, int p) {
		for (int i = p; i * p < seive.length; i++) {
			seive[i * p] = false;
		}
	}

	public static void main(String[] args) {
		new ThreePrimeNumbers().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
