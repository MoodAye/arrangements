package com.moodaye.playground.programmerSchool._202107;

import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Scanner;

// Problem 
public class RollingDie {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int q = in.nextInt();

		Map<Integer, Double> probs = new HashMap<Integer, Double>();
		for (int i = 1; i <= 6; i++) {
			probs.put(i, 1.0 / 6);
		}

		for (int i = 2; i <= n; i++) {
			for (int sum : probs.keySet()) {
				probs.put(sum, probs.get(sum) / 6);
			}
			Map<Integer, Double> probsTemp = new HashMap<Integer, Double>();
			for (int die = 1; die <= 6; die++) {
				for (int sum : probs.keySet()) {
					probsTemp.merge(sum + die, probs.get(sum), Double::sum);
				}
			}
			probs = probsTemp;
		}
		out.printf("%.7f%n", probs.containsKey(q) ? probs.get(q) : 0);
	}

	public static void main(String[] args) {
		new RollingDie().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
