package com.moodaye.playground.programmerSchool._202107;

import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Locale;
import java.util.Scanner;

// Problem 
public class SumPowersTwo {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		
		long[] curr = new long[n + 1];
		long[] prev = new long[n + 1];
		
		Arrays.fill(curr, 1);
		
		int pow = 2;
		
		while(pow <= n) {
			long[] temp = curr;
			curr = prev;
			prev = temp;
			Arrays.fill(curr, 0);
			for(int i = 0; i <= n; i++) {
				if(i - pow < 0) {
					curr[i] = prev[i];
				}
				else {
					curr[i] = prev[i] + curr[i - pow];
				}
			}
			pow *= 2;
		}
		out.println(curr[n]);
	}
	
	public static void main(String[] args) {
		new SumPowersTwo().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
