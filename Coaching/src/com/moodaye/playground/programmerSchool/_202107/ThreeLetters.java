package com.moodaye.playground.programmerSchool._202107;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

// Problem 
public class ThreeLetters {
	void solve(Scanner in, PrintWriter out) {
		if (in.hasNext()) {
			char[] seq = in.next().toCharArray();

			long a = 0L;
			long ab = 0L;
			long abc = 0L;

			for (int i = 0; i < seq.length; i++) {
				if (seq[i] == 'a') {
					a++;
				} else if (seq[i] == 'b') {
					ab += a;
				} else if (seq[i] == 'c') {
					abc += ab;
				}
			}
			out.println(abc);
		} else {
			out.println(0);
		}
	}

	public static void main(String[] args) {
		new ThreeLetters().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
