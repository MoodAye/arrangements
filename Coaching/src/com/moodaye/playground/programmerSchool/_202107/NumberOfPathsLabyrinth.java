package com.moodaye.playground.programmerSchool._202107;

import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Locale;
import java.util.Scanner;

// Problem 
public class NumberOfPathsLabyrinth {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int k = in.nextInt();
		int laby[][] = new int[n + 2][n + 2];

		for (int i = 1; i <= n; i++) {
			char[] line = in.next().toCharArray();
			for (int j = 1; j <= n; j++) {
				laby[i][j] = line[j - 1] - '0';
			}
		}

		int[][] cnts = new int[n + 2][n + 2];
		int[][] tmp = new int[n + 2][n + 2];
		
		cnts[1][1] = 1;

		while (k-- > 0) {
			clear(tmp);
			for (int i = 1; i <= n; i++) {
				for (int j = 1; j <= n; j++) {
					if(laby[i][j] == 1) {
						continue;
					}
					tmp[i][j] = cnts[i - 1][j] +
							cnts[i + 1][j] + 
							cnts[i][j - 1] +
							cnts[i][j + 1];
				}
			}
			int[][] tmp2 = cnts;
			cnts = tmp;
			tmp = tmp2;
		}
		out.println(cnts[n][n]);
	}
	
	private void clear(int[][] a) {
		for (int i = 0; i < a.length; i++) {
			Arrays.fill(a[i], 0);
		}
	}

	public static void main(String[] args) {
		new NumberOfPathsLabyrinth().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
