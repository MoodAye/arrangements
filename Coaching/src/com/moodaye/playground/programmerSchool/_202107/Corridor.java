package com.moodaye.playground.programmerSchool._202107;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

// Problem 
public class Corridor {
void solve(Scanner in, PrintWriter out) {
	int m = in.nextInt();
	int n = in.nextInt();
	long[] cnt = new long[n + 1];
	
	for(int i = 0; i <= n; i++) {
		if(i < m) {
			cnt[i] = 1L;
			continue;
		}
		cnt[i] = cnt[i - 1] + cnt[i - m];
	}
	out.println(cnt[n]);
}

public static void main(String[] args) {
	new Corridor().run();
}

void run() {
	Locale.setDefault(Locale.US);
	try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
		solve(in, out);
	}
}
}
