package com.moodaye.playground.programmerSchool._202107;

import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Locale;
import java.util.Scanner;

// Problem  121 731am - 742am (11min)
public class Nails {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int[] nums = new int[n];
		for (int i = 0; i < nums.length; i++) {
			nums[i] = in.nextInt();
		}
		
		Arrays.sort(nums);
		
		int sumN_2 = 0;
		int sumN_1 = nums[1] - nums[0];
		int sumN = sumN_1;
		
		for(int i = 2; i < n; i++) {
			sumN_2 = sumN_1;
			sumN_1 = sumN;
			sumN = nums[i] - nums[i - 1] + Math.min(sumN_1, sumN_2);
		}
		out.println(sumN);
	}

	public static void main(String[] args) {
		new Nails().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
