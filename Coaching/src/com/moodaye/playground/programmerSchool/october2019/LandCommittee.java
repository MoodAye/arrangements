package com.moodaye.playground.programmerSchool.october2019;

import java.io.InputStream;
import java.io.PrintWriter;
import java.util.Locale;

// Problem 470
// Time Taken ~30min
public class LandCommittee {
	
	// Solution with O(1) complexity in the main loop.
	void solve(Scanner in, PrintWriter out) {
		int h = in.nextInt();
		int w = in.nextInt();
		int n = in.nextInt();
		
		int[][] cost = new int[h + 1][w + 1];
		for (int i = 1; i <= h; i++) {
			for (int j = 1; j <= w; j++) {
				cost[i][j] = in.nextInt() + cost[i - 1][j] + cost[i][j - 1] - cost[i - 1][j - 1];
			}
		}
		
		for (int i = 0; i < n; i++) {
			int a = in.nextInt();
			int b = in.nextInt();
			int c = in.nextInt();
			int d = in.nextInt();
			out.println(cost[c][d] - cost[a - 1][d] - cost[c][b - 1] + cost[a - 1][b - 1]);
		}
	}
	

	void solve2(Scanner in, PrintWriter out) {
		int h = in.nextInt();
		int w = in.nextInt();
		int n = in.nextInt();

		// cumulative costs - left to right
		int[][] cost = new int[h + 1][w + 1];
		for (int i = 1; i <= h; i++) {
			for (int j = 1; j <= w; j++) {
				cost[i][j] = in.nextInt() + cost[i][j - 1];
				if (j == 1) {
					cost[i][j] += cost[i - 1][w];
				}
			}
		}

		for (int i = 0; i < n; i++) {
			int a = in.nextInt();
			int b = in.nextInt();
			int c = in.nextInt();
			int d = in.nextInt();

			int plotCost = 0;
			for (int row = a; row <= c; row++) {
				if(b == 1) {
					plotCost += cost[row][d] - cost[row - 1][w];
				}
				else{
					plotCost += cost[row][d] - cost[row][b - 1];
				}
			}
			out.println(plotCost);

		}
		/*
		 * for (int r = 0; r <= h; r++) { out.println(); for (int k = 0; k <= w; k++) {
		 * out.printf("%d ", cost[r][k]); } }
		 */
	}

	// Complexity = O(nhw)
	// worst case count of operations has order of magnitude 1e6 x 1e2 x 1e2 = 1e8.
	// therefore we can do this brute force in the 5s allocated time.
	void solveBruteForce(Scanner in, PrintWriter out) {
		int h = in.nextInt();
		int w = in.nextInt();
		int n = in.nextInt();
		int[][] cost = new int[h + 1][w + 1];
		for (int i = 1; i <= h; i++) {
			for (int j = 1; j <= w; j++) {
				cost[i][j] = in.nextInt();
			}
		}

		for (int i = 0; i < n; i++) {
			int a = in.nextInt();
			int b = in.nextInt();
			int c = in.nextInt();
			int d = in.nextInt();

			int plotCost = 0;
			for (int j = a; j <= c; j++) {
				for (int k = b; k <= d; k++) {
					plotCost += cost[j][k];
				}
			}
			out.println(plotCost);
		}
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}

	public static void main(String[] args) {
		new LandCommittee().run();
	}

	static class Scanner implements AutoCloseable {

		InputStream is;
		byte buffer[] = new byte[1 << 16];
		int size = 0;
		int pos = 0;

		Scanner(InputStream is) {
			this.is = is;
		}

		int nextChar() {
			if (pos >= size) {
				try {
					size = is.read(buffer);
				} catch (java.io.IOException e) {
					throw new java.io.IOError(e);
				}
				pos = 0;
				if (size == -1) {
					return -1;
				}
			}
			Assert.check(pos < size);
			int c = buffer[pos] & 0xFF;
			pos++;
			return c;
		}

		int nextInt() {
			int c = nextChar();
			while (c == ' ' || c == '\r' || c == '\n' || c == '\t') {
				c = nextChar();
			}
			if (c == '-') {
				c = nextChar();
				Assert.check('0' <= c && c <= '9');
				int n = -(c - '0');
				c = nextChar();
				while ('0' <= c && c <= '9') {
					int d = c - '0';
					c = nextChar();
					Assert.check(n > Integer.MIN_VALUE / 10
							|| n == Integer.MIN_VALUE / 10 && d <= -(Integer.MIN_VALUE % 10));
					n = n * 10 - d;
				}
				return n;
			} else {
				Assert.check('0' <= c && c <= '9');
				int n = c - '0';
				c = nextChar();
				while ('0' <= c && c <= '9') {
					int d = c - '0';
					c = nextChar();
					Assert.check(
							n < Integer.MAX_VALUE / 10 || n == Integer.MAX_VALUE / 10 && d <= Integer.MAX_VALUE % 10);
					n = n * 10 + d;
				}
				return n;
			}
		}

		@Override
		public void close() {
		}
	}

	static class Assert {
		static void check(boolean e) {
			if (!e) {
				throw new AssertionError();
			}
		}
	}
}
