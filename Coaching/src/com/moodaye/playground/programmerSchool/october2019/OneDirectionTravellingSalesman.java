package com.moodaye.playground.programmerSchool.october2019;

import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Locale;
import java.util.Scanner;
import java.util.stream.IntStream;

// Problem  687
// 641pm - 6:53pm (reused previous solution - therefore quite fast)
public class OneDirectionTravellingSalesman {
	
	void solve2(Scanner in, PrintWriter out) {
		int rowCnt = in.nextInt();
		int colCnt = in.nextInt();
		long[][] m = new long[rowCnt][colCnt];

		for (int r = 0; r < rowCnt; r++) {
			for (int c = 0; c < colCnt; c++) {
				m[r][c] = in.nextLong();
			}
		}
		
		if(rowCnt == 1) {
			IntStream.range(1, colCnt + 1).forEach((x) -> out.print(x + " "));
			out.println();
			out.println(Arrays.stream(m[0]).sum());
			return;
		}

		int[][] path = new int[rowCnt][colCnt];

		for (int c = colCnt - 2; c >= 0; c--) {
			for (int r = 0; r < rowCnt; r++) {
				int minR = 0;
				if (r == 0) {
					minR = minRow(m, c + 1, 0, 1);
				} else if (r == rowCnt - 1) {
					minR = minRow(m, c + 1, rowCnt - 2, rowCnt - 1);
				} else {
					minR = minRow(m, c + 1, r - 1, r, r + 1);
				}
				path[r][c] = minR;
				m[r][c] += m[minR][c + 1];
			}
		}

		// get min from first column
		int minRow = 0;
		long min = m[0][0];
		for (int r = 1; r < rowCnt; r++) {
			if (m[r][0] < min) {
				minRow = r;
				min = m[r][0];
			}
		}
		
		out.print((minRow + 1) + " ");
		for(int c = 0; c < colCnt - 1; c++) {
			minRow = path[minRow][c];
			out.print((minRow + 1) +  " ");
		}
		out.println();
		out.println(min);
	}
	

	int minRow(long[][] m, int c, int... rows) {
		int minR = rows[0];
		for (int r = 1; r < rows.length; r++) {
			if (m[rows[r]][c] < m[minR][c]) {
				minR = rows[r];
			}
		}
		return minR;
	}

	public static void main(String[] args) {
		new OneDirectionTravellingSalesman().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve2(in, out);
		}
	}
}
