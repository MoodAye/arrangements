package com.moodaye.playground.programmerSchool.october2019;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

// Problem 781
// 24min
public class TwoDigits {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		
		if(n <= 1) {
			out.println(2 * n);
			return;
		}
		
		int cnt55 = 1;
		int cnt59 = 1;
		int cnt95 = 1;
		int cnt99 = 1;
		
		for(int d = 3; d <= n; d++) {
			int tmpCnt55 = cnt95;
			int tmpCnt59 = cnt55 + cnt95;
			int tmpCnt95 = cnt59 + cnt99;
			int tmpCnt99 = cnt59;
			
			cnt55 = tmpCnt55;
			cnt59 = tmpCnt59;
			cnt95 = tmpCnt95;
			cnt99 = tmpCnt99;
		}
		
		out.println(cnt55 + cnt59 + cnt95 + cnt99);
	}

	public static void main(String[] args) {
		new TwoDigits().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
