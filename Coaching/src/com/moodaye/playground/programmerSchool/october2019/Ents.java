package com.moodaye.playground.programmerSchool.october2019;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

// Problem 183
// start 828am - 854am + 10 ~36min
// WA on test 10
public class Ents {
	private static int cnts;
	
	void solve(Scanner in, PrintWriter out) {
		int k = in.nextInt();
		int p = in.nextInt();
		
		if(k == 1) {
			out.println(0);
			return;
		}
		
		int[] memo = new int[k + 1];
		memo[2] = 1 % p;
		
		for(int i = 3; i <= k; i++) {
			memo[i] = (memo[i] + memo[i - 1]) % p;
			if(i % 2 == 0) {
				memo[i] = (memo[i] + memo[i / 2]) % p;
			}
		}
		out.println(memo[k]);
	}
	
	public static void main(String[] args) {
		new Ents().run();
	}
	
	void run() {
		Locale.setDefault(Locale.US);
		try(Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)){
			solve(in, out);
		}
	}
}
