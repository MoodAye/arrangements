package com.moodaye.playground.programmerSchool.october2019;

import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Locale;
import java.util.Scanner;

// Problem 334
// Problem reduces to finding the min of the sums of the differences between each time and all the other times.
// This difference of each time with the other times can be derived from the sums of all times and the 
// hour (12) for starting back from zero.
public class ChineseWatches {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		long[] times = new long[n];
	
		in.useDelimiter(":|\\s+");
		for(int i = 0; i < n; i++) {
			int h = in.nextInt();
			int m = in.nextInt();
			int s = in.nextInt();
			times[i] = 1L * (h * 3600 + m * 60 + s);
		}
	
		Arrays.sort(times);
		long sum = Arrays.stream(times).sum();
		long rollTimeAt = 12 * 3600;
		
		long minTime = 0;
		long min = Long.MAX_VALUE;
		for(int i = 0; i < n; i++) {
			long diff = n * times[i] - sum + (n - i + 1) * rollTimeAt;
			if(diff < min) {
				minTime = times[i];
				min = diff;
			}
		}
		long h = minTime / 3600;
		long m = (minTime % 3600) / 60;
		long s = (minTime % 3600) % 60;
		out.printf("%d:%02d:%02d", h, m, s);
	}
	

	public static void main(String[] args) {
		new ChineseWatches().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
