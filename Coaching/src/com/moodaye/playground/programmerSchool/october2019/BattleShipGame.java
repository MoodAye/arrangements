package com.moodaye.playground.programmerSchool.october2019;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

// Problem 305
public class BattleShipGame {
	void solve(Scanner in, PrintWriter out) {
		int h = in.nextInt();
		int w = in.nextInt();
		int shipsPlaced = in.nextInt();

		int[][] avail = new int[h + 2][w + 2];

		for (int i = 0; i < shipsPlaced; i++) {
			int uleftx = in.nextInt();
			int ulefty = in.nextInt();
			int brightx = in.nextInt();
			int brighty = in.nextInt();
			for (int j = uleftx; j <= brightx; j++) {
				for (int k = ulefty; k <= brighty; k++) {
					avail[j][k] = 1;
					avail[j - 1][k] = 1;
					avail[j + 1][k] = 1;
					avail[j][k + 1] = 1;
					avail[j][k - 1] = 1;
				}
			}
		}
		
		for(int i = 1; i <= h; i++) {
			for(int j = 1; j <= w; j++) {
				avail[i][j] = avail[i][j] + avail[i - 1][j] + avail[i][j - 1] - avail[i - 1][j - 1];
			}
		}
	
		int max = 0;
		for(int i = 1; i <= h; i++) {
			for(int j = 1; j <= w; j++) {
				for(int k = i; k <= h; k++) {
					for(int m = j; m <= w; m++) {
						max = Math.max(max, size(i, j, k, m, avail));
					}
				}
				
			}
		}
		
		out.println(max);
	}
	
	int size(int uleftx, int ulefty, int brightx, int brighty, int[][] avail) {
		if(avail[brightx][brighty] - avail[brightx][ulefty - 1] - avail[uleftx - 1][brighty] + 
				avail[uleftx - 1][ulefty - 1] != 0) {
			return 0;
		}
		return (brightx - uleftx + 1) * (brighty - ulefty + 1);
	}

	public static void main(String[] args) {
		new BattleShipGame().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
