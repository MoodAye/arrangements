package com.moodaye.playground.programmerSchool.october2019;

import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Locale;
import java.util.Scanner;

// Problem 335
public class ThreePrimeNumbers {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		final int MASK = 1_000_000_000 + 9;

		int[] primes3Digit = seive();
		int[] cntsLast2Digits = new int[100];
		int[] prevCntsLast2Digits = new int[100];

		// e.g., 613 ---> 6131, 6137, 6139
		// or 13 --> 3 digits primes 131, 137, 139 --> 31, 37, 39
		int[][] mapLast2Next2 = new int[100][4];

		for (int i = 100; i < 1000; i++) {
			if (primes3Digit[i] == 1) {
				// e.g., if 613 then cnstsLast2Digits[13]++
				int last2 = i % 100;

				// e.g. ,101 is prime ; last2 = 1;
				if (last2 < 10) {
					continue;
				}

				cntsLast2Digits[last2]++;

				// find 3 digit primes that begin with last2
				int k = 0;
				for (int j = (last2 * 10 + 1); j <= (last2 * 10 + 9); j = j + 2) {
					if (primes3Digit[j] == 1) {
						mapLast2Next2[last2][k++] = j % 100;
					}
				}
			}
		}

		if (n == 3) {
			int cnt = 0;
			for(int i = 100; i < 1000; i++) {
				if(primes3Digit[i] == 1) {
					cnt++;
				}
			}
			out.println(cnt);
			return;
		}

		for (int i = 4; i <= n; i++) {
			int[] temp = prevCntsLast2Digits;
			prevCntsLast2Digits = cntsLast2Digits;
			cntsLast2Digits = temp;
			Arrays.fill(cntsLast2Digits, 0);
			for (int j = 10; j <= 99; j++) {
				if (prevCntsLast2Digits[j] != 0) {
					for (int k = 0; k < 4; k++) {
						if (mapLast2Next2[j][k] == 0) {
							break;
						}
						int idx = mapLast2Next2[j][k];
						cntsLast2Digits[idx] = (cntsLast2Digits[idx] + prevCntsLast2Digits[j]) % MASK;
					}
				}
			}
		}
		int cnt = 0;
		for (int i = 10; i <= 99; i++) {
			cnt = (cnt + cntsLast2Digits[i]) % MASK;
		}
		out.println(cnt);
	}

	public static void main(String[] args) {
		new ThreePrimeNumbers().run();
	}

	// all 3 digit prime numbers
	private int[] seive() {
		int[] isPrime = new int[1000];
		Arrays.fill(isPrime, 1);

		isPrime[0] = 0;
		isPrime[1] = 0;

		for (int i = 2; i < 1000; i++) {
			if (isPrime[i] == 1) {
				for (int j = i * i; j < 1000; j += i) {
					isPrime[j] = 0;
				}
			}
		}
		return isPrime;
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
