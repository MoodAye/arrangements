package com.moodaye.playground.programmerSchool.october2019;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

// Problem 619
// Time thus far = 1 hr
// Complexity = O(n*q);  n * q = 1_500_000 < 1e8.  1 sec is enough time. 
public class RollingDie {
	// using 1d array
	void solve2(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int q = in.nextInt();

		double[] p = new double[q + 1];

		// there is one way to get to a sum of zero without rolling any dice.
		p[0] = 1.0;

		for (int turn = 1; turn <= n; turn++) {
			for (int sum = Math.min(q, turn * 6); sum >= 0; sum--) {
				p[sum] = 0;
				for (int d = 1; d <= 6 && (sum - d) >= 0; d++) {
					p[sum] += p[sum - d];
				}
				p[sum] /= 6;
			}
		}
		out.printf("%.6f", p[q]);
	}

	public static void main(String[] args) {
		new RollingDie().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve2(in, out);
		}
	}
}
