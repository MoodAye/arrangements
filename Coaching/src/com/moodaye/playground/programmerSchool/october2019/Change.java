package com.moodaye.playground.programmerSchool.october2019;

import java.io.PrintWriter;
import java.util.Arrays;
import java.util.BitSet;
import java.util.Locale;
import java.util.Scanner;

// Problem 944
public class Change {
	
	// uses Boolean Array
	void solve2(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int[] coin = new int[n];
		for (int i = 0; i < n; i++) {
			coin[i] = in.nextInt();
		}

		int k = in.nextInt();
		int[] amount = new int[k];
		for (int i = 0; i < k; i++) {
			amount[i] = in.nextInt();
		}

		int maxAmt = Arrays.stream(amount).max().getAsInt();
		boolean[] perms = new boolean[maxAmt + 1];
		perms[0] = true;

		for (int c = 1; c <= n; c++) {
			for (int amt = coin[c - 1]; amt <= maxAmt; amt++) {
				perms[amt] = perms[amt] || perms[amt - coin[c - 1]];
			}
		}

		for (int i : amount) {
			out.print(perms[i] ? "1 " : "0 ");
		}
	}
	
	// Uses BitSet
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int[] coin = new int[n];
		for (int i = 0; i < n; i++) {
			coin[i] = in.nextInt();
		}

		int k = in.nextInt();
		int[] amount = new int[k];
		for (int i = 0; i < k; i++) {
			amount[i] = in.nextInt();
		}

		int maxAmt = Arrays.stream(amount).max().getAsInt();
		BitSet perms = new BitSet(maxAmt - 1);
		perms.set(0);

		for (int c = 1; c <= n; c++) {
			for (int amt = coin[c - 1]; amt <= maxAmt; amt++) {
				if (perms.get(amt) || perms.get(amt - coin[c - 1])) {
					perms.set(amt);
				}
			}
		}

		for (int i : amount) {
			out.print(perms.get(i) ? "1 " : "0 ");
		}
	}

	public static void main(String[] args) {
		new Change().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}

}
