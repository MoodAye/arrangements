package com.moodaye.playground.programmerSchool.october2019;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

// Problem 510.  Time > 1hr
public class ChocolateBar {
	
	// O(n) implementation
	void solve2(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		if(n % 2 == 1) {
			out.println(0);
			return;
		}
		
		// keeps count of previous levels
		int[] memo1 = new int[n + 1];
		
		// seed for algo to work
		memo1[0] = 1; 
		memo1[2] = 3; 
		
		// keeps count of sums of (n-4) levels x 2
		int[] memo2 = new int[n + 1];
		
		
		for(int i = 4; i <= n; i += 2) {
			memo2[i] = memo2[i - 2] + memo1[i - 4] * 2;
			memo1[i] = memo1[i - 2] * 3 + memo2[i];
		}
		
		out.println(memo1[n]);
	//	out.println(cnt);
		
		
		
	}
	void solve(Scanner in, PrintWriter out){
		int n = in.nextInt();
		int[] memo = new int[n + 1];
		if(n % 2 == 1) {
			out.println(0);
			return;
		}
		
		memo[0] = 1;
		memo[2] = 3;
	
		// The first set of ways to get the nth row is from a row below 
		// that is complete is
		// (i - 2) * 3
		// The second set of ways to get the nth row is from a row below 
		// that is complete is
		// (i - 4) * 2
		// (i - 6) * 2
		// ...
		// For alogrithm to work - we seed m[0] = 1
		for(int i = 4; i <= n; i += 2) {
			memo[i] = memo[i - 2] * 3;
			for(int j = i - 4; j >= 0; j -= 2) {
				memo[i] += memo[j] * 2;
			}
		}
		out.println(memo[n]);
	}
	
	public static void main(String[] args) {
		new ChocolateBar().run();
	}
	void run() {
		Locale.setDefault(Locale.US);
		try(Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)){
			solve2(in, out);
		}
	}
}
