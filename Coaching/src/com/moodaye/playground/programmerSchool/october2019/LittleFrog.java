package com.moodaye.playground.programmerSchool.october2019;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

// Problem 502
public class LittleFrog {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int[][] a = new int[n + 1][n + 1];
		
		for(int i = 1; i <= n; i++) {
			for(int j = 1; j <= n; j++) {
				a[i][j] = in.nextInt();
			}
		}
		
		int[][] maxWeight = new int[n + 1][n + 1];
		// init
		for(int c = 1; c <= n; c++) {
			maxWeight[1][c] = a[c][1];
		}
		
		for(int nCol = 2; nCol <= n; nCol++) {
			for(int rowSum = 1; rowSum <= n; rowSum++) {
				maxWeight[nCol][rowSum] = maxWeight[nCol - 1][rowSum];
				for(int prevRowSum = rowSum - 1; prevRowSum >= 0; prevRowSum--) {
					maxWeight[nCol][rowSum] = Math.max(maxWeight[nCol][rowSum], 
							maxWeight[nCol - 1][prevRowSum] + a[rowSum - prevRowSum][nCol]);
				}
			}
		}
		
		out.println(maxWeight[n][n]);
		
	}
	
	void run() {
		Locale.setDefault(Locale.US);
		try(Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)){
			solve(in, out);
		}
	}
	
	public static void main(String[] args) {
		new LittleFrog().run();
	}
}
