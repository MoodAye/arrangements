package com.moodaye.playground.programmerSchool;

import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;

/** Programmer's School # */
public class EquationFifthGrade {
	void solve(Scanner in, PrintWriter out) {
		String e = in.next();
		out.println(getAnswer(e));
	}
	
	static int getAnswer(String e){
		int a = e.charAt(0);
		int b = e.charAt(2);
		int c = e.charAt(4);
		int pm = e.charAt(1); // plus or minus (+/-)
		
		// 48 normalizes the integer value 
		// (44 - pm) returns +1 or -1
		if(c == 'x'){
			return (a - 48) + (44 - pm) * (b - 48);
		}
		// e.g., 6-x=4
		else if(b == 'x' && pm == '-'){ 
			return (a - c);
		}
		else{
			return (c - 48) - (44 - pm) * (a + b - 'x' - 48);
		}
	}

	public static void main(String args[]) {
		new EquationFifthGrade().run();

	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
