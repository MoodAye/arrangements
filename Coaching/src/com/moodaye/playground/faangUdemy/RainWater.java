package com.moodaye.playground.faangUdemy;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

// Problem 
public class RainWater {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int[] wallHt = new int[n];
		for (int i = 0; i < wallHt.length; i++) {
			wallHt[i] = in.nextInt();
		}
	
		int sum = 0;
		
		for(int i = 0; i  < n; i++) {
			
			if(wallHt[i] == 0) {
				continue;
			}
				
			// scan forward for wall that is max of smaller walls
			// or ht equal to greater - if equal or greater break
			// keep track of blocks
			// if you find next wall has ht = max of prev min - keep goindA
			
			int maxOfSmallerWalls = Integer.MAX_VALUE;
			int sumWallHtsPassed = 0;
			
			for(int k = i + 1; k < n; k++) {
				if(wallHt[k] >= wallHt[i]) {
					sum += wallHt[i] * (k - i - 1) - sumWallHtsPassed;
					break;
				}
			}
		}
	}

	public static void main(String[] args) {
		new RainWater().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
