package com.moodaye.playground.combinations;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

/**
 * 
 * Brute force.
 * Todo:
 * 1. Fix brute force - only printing one solution
 * 2. Understand complexity
 *
 */
public class LangfordSequences {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();

		int[] trg = new int[2 * n];
		//patternedCombo1(n, new HashSet<Integer>(), trg, 0);
		Set<int[]> solutions = langford2(n);
		int cnt = 0;
		for(int[] s : solutions) {
			cnt++;
			Arrays.stream(s).forEach((x) -> System.out.print(x + ","));
			System.out.println();
			if(cnt == 10) {
				break;
			}
		}
	}
	
	
	/**
	 * Backtracking
	 * Zero marks unused spaces in array
	 * 
	 * Start with 1 in spot idx = 0; Then 2 .....
	 * As you put numbers in spots - fill in the corresponding spot per Langford pattern
	 * 
	 * Recurse - passing in numbers already spotted.
	 * 
	 * Max recursions for each trial is n - 1
	 * 
	 * @param n
	 */
	public static Set<int[]> langford2(int n) {
		Set<int[]> solutions = new HashSet<>();
		langford2(n, new int[2 * n], new HashSet<Integer>(), solutions);
		return solutions;
	}
	
	public static void langford2(int n, int[] a, Set<Integer> used, Set<int[]> solutions) {
		if(used.size() == n) {
			solutions.add(a.clone());
//			Arrays.stream(a).forEach((i) -> System.out.print(i + ","));
//			System.out.println();
			return;
		}
		
		for(int i = 1; i <= n; i++) {
			if(used.contains(i)) {
				continue;
			}
			
			for(int j = 0; j < a.length; j++) {
				if(j + i + 1 >= a.length) {
					return;
				}
				if(a[j] != 0 || a[j + i + 1] != 0) {
					continue;
				}
				a[j] = i;
				a[j + i + 1] = i;
				used.add(i);
				langford2(n, a, used, solutions);
				used.remove(i);
				a[j] = 0;
				a[j + i + 1] = 0;
			}
		}
	}
	
	
	public static boolean found(int[] target, Set<int[]> solutions) {
		for(int[] soln : solutions) {
			for(int i = 0; i < target.length; i++) {
				if(soln[i] != target[i]) {
					break;
				}
			}
			return true;
		}
		return false;
	}

	public static void patternedCombo1(int n, Set<Integer> idxUsed, int[] trg, int trgIdx) {
		int[] src = new int[2 * n];
		for(int i = 0; i < n; i++) {
			src[2 * i] = i + 1;
			src[2 * i + 1] = i + 1;
		}
		patternedCombo1(src, idxUsed, trg, trgIdx, new HashSet<Integer>(), new HashSet<int[]>());
	}

	/**
	 * Will implement a rule of how to fill the target array. In this case we are
	 * going to follow the rule that given a source with numbers 1..2n, any number k
	 * must have k digits between its 2 occurrences in the array.
	 * 
	 * @param src     Source array with numbers 1...2n (e.g., 1,1,2,2,3,3,4,4 where
	 *                n = 4)
	 * @param idxUsed Tracks which indexes have been used
	 * @param trg     Target array we are populating to try and fit the pattern
	 * @param trgIdx  Current idx of the target
	 */
	public static Set<int[]> patternedCombo1(int[] src, Set<Integer> idxUsed, int[] trg, int trgIdx, Set<Integer> firstElementInTarget, Set<int[]> solutions) {
		if (trgIdx == trg.length) {
			if (langfordPatternCheck(trg) && !found(trg, solutions)) {
				solutions.add(trg.clone());
				Arrays.stream(trg).forEach((i) -> System.out.print(i + ","));
				System.out.println();
			}
		}
		
		for (int i = 0; i < src.length; i++) {
			if (idxUsed.contains(i)) {
				continue;
			}
			idxUsed.add(i);
			
			trg[trgIdx] = src[i];
			patternedCombo1(src, idxUsed, trg, trgIdx + 1, firstElementInTarget, solutions);
			idxUsed.remove(i);
		}
		
		return solutions;
	}
	
	public static boolean langfordPatternCheck(int[] a) {
		for(int i = 1; i <= a.length / 2; i++) {
			for(int j = 0; j < a.length; j++) {
				if(a[j] == 0) {
					return false;
				}
				if(a[j] == i) {
					if(j + i + 1 >= a.length || a[j + i + 1] != i) {
						return false;
					}
					else {
						break;
					}
				}
			}
		}
		return true;
	}

	public static void allCombos(int[] src, Set<Integer> idxUsed, int[] trg, int trgIdx) {
		if (trgIdx == trg.length) {
			Arrays.stream(trg).forEach((i) -> System.out.print(i + ","));
			System.out.println();
		}

		for (int i = 0; i < src.length; i++) {
			if (idxUsed.contains(i)) {
				continue;
			}
			idxUsed.add(i);
			trg[trgIdx] = src[i];
			allCombos(src, idxUsed, trg, trgIdx + 1);
			idxUsed.remove(i);
		}
	}
}
