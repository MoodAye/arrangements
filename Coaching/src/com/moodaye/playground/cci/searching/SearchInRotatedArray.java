package com.moodaye.playground.cci.searching;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

// Problem 10.3 - CCI (Cracking The Coding Interview)
// 8:57am
public class SearchInRotatedArray {
	/*
	 * Given sorted array of n integers that has been rotated - write code to find an element. Array was in increasing order
	 */
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int[] values = new int[n];
		for(int i = 0; i < n; i++) {
			values[i] = in.nextInt();
		}
		//out.println(find(in.nextInt(), values));
		out.println(find(in.nextInt(), values));
	}

	/* from book - more consise than the one i wrote - but mine was similar */
	int binarySearch2(int x, int[] v) {
		int low = 0;
		int high = v.length - 1;
		int mid;
		while(low <= high) {
			mid = (low + high) / 2;
			if(v[mid] < x) {
				low = mid + 1;
			}
			else if (v[mid] > x) {
				high = mid - 1;
			}
			else {
				return mid;
			}
		}
		return -1;
	}
	
	
	/* 
	 * [1,2,3,4,5,6] --> [4,5,6,1,2,3]
	 *
	 */
	int find(int x, int[] v) {
		// approach after peeking at solution
		// compare left most element with middle element to determine which half of the array is sorted.
		// then compare the 2 end elements of that half to determine if x is in that half or the other half.
		
		// leveraging binary solution code
		int low = 0;
		int high = v.length - 1;
		int mid;
		while(low <= high) {
			mid = (low + high) / 2;
			if(x == v[mid]) {
				return mid;
			}
			
			// which half is sorted?
			// and which side contains x?
			if(v[low] <= v[mid]) {
				if(v[low] <= x && x < v[mid]) {
					high = mid - 1;
				}
				else {
					low = mid + 1;
				}
			}
			else {
				if(v[mid] < x && x <= v[high]) {
					low = mid + 1;
				}
				else {
					high = mid - 1;
				}
			}
		}
		return -1;
	}

	public static void main(String[] args) {
		new SearchInRotatedArray().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
