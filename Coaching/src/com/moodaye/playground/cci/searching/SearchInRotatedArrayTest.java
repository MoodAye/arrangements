package com.moodaye.playground.cci.searching;

import static org.junit.Assert.*;
import static com.moodaye.playground.cci.searching.SortedSearchNoSize.*;

import java.util.Arrays;
import java.util.Random;

import org.junit.Test;
import static com.moodaye.playground.cci.searching.SearchInRotatedArray.*;

public class SearchInRotatedArrayTest {

	static int[] randArr() {
		Random rand = new Random();
		int n = rand.nextInt(1_000);
		int[] v = new int[n];
		for (int j = 0; j < n; j++) {
			v[j] = rand.nextInt();
		}
		Arrays.sort(v);
		return v;
	}

	//@Test
	public void test() {
		Random rand = new Random();
		for (int i = 0; i < 10_000; i++) {
			int[] v = randArr();
			int n = v.length;
			int shift = rand.nextInt();
			int[] temp = v.clone();
			for (int j = 0; j < n; j++) {
				// todo - do shift in place
				temp[(j + shift) % n] = v[j];
			}
			for (int j = 0; j < n; j++) {
			//	assertEquals(find(temp[j], temp), j);
			}
		}
	}
	
	@Test
	public void testSortedSearchNoSize() {
		int[] v = {1,2,3,4};
		assertEquals(ssns(1,v),0);
		assertEquals(ssns(2,v),1);
		assertEquals(ssns(3,v),2);
		assertEquals(ssns(4,v),3);
		assertEquals(ssns(10,v),-1);
		
		int[] v2 = {1};
		assertEquals(ssns(10,v),-1);
		assertEquals(ssns(1,v), 0);
		
		
		int[] v3 = {1,2,3};
		assertEquals(ssns(1,v3),0);
		assertEquals(ssns(2,v3),1);
		assertEquals(ssns(3,v3),2);
		assertEquals(ssns(-1,v3),-1);
		assertEquals(ssns(100,v3),-1);
	}

}
