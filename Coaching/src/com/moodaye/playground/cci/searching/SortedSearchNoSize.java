package com.moodaye.playground.cci.searching;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

// Problem 10.4 CCI
// 825am - 8:50am
public class SortedSearchNoSize {
	void solve(Scanner in, PrintWriter out) {
		
		
	}
	
	/* find index without using length of array */
	static int ssns(int x, int[] v) {
		
		int left = 0;
		int right = 1;
		
		while(true) {
			if(right >= v.length) { // simulates returning -1
				right = (left + right) / 2;
			}
			else if(x < v[right]) {
				return binarySearch(x, left, right, v);
			}
			else if(x == v[right]) {
				return right;
			}
			else if(left == right) {
				return -1;
			}
			else {
				left = right;
				right = 2 * left;
			}
		}
	}
	
	static int binarySearch(int x, int left, int right, int[] v) {
		int mid = (left + right) / 2;
		
		while(left <= right) {
			if(x == v[mid]) {
				return mid;
			}
			else if(x < v[mid]) {
				right = mid - 1;
			}
			else {
				left = mid + 1;
			}
		}
		return -1;
	}

	public static void main(String[] args) {
		new SortedSearchNoSize().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
