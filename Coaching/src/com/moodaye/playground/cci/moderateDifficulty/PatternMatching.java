package com.moodaye.playground.cci.moderateDifficulty;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

// Problem 16.18
// 434pm
// Need to complete this - write the translate method.
// Approach is to determine all possible values of a and b based on value string.
// Then translate the pattern using this value of a and b.
// You have a match if the translated pattern equals value.
public class PatternMatching {
	void solve(Scanner in, PrintWriter out) {
		String pattern = in.next();
		String value = in.next();

		if (pattern.equals("a") || pattern.equals("ab") || pattern.equals("b")) {
			out.println("true");
			return;
		}

		for (int i = 0; i < value.length(); i++) {
			String a = value.substring(0, i + 1);
			for (int j = i + 1; j < value.length(); j++) {
				for (int k = j; k < value.length(); k++) {
					String b = value.substring(j, k);
					String patternToValue = translate(a, b, pattern);
					if (patternToValue.equals(value)) {
						out.println("true");
						return;
					}
				}
			}
		}
		out.println("false");
	}

	/*
	 * Creates and returns string based on the pattern. The pattern is of the form
	 * [a,b]*
	 */
	private String translate(String a, String b, String pattern) {
		char[] cp = pattern.toCharArray();
		StringBuilder sb = new StringBuilder();

		// a and b are the strings based on the value input by user. a is always before
		// b.
		// By exchanging them in this case - we are enforcing that the first character
		// (either a or b)
		// from the pattern matches the string a.
		if (cp[0] == 'b') {
			String temp = a;
			a = b;
			b = temp;
		}

		for (int i = 0; i < cp.length; i++) {
			if (cp[i] == 'a') {
				sb.append(a);
			} else {
				sb.append(b);
			}
		}
		return sb.toString();
	}

	public static void main(String[] args) {
		new PatternMatching().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
