package com.moodaye.playground.cci.moderateDifficulty;

import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Scanner;

// Problem cci 16.19
// Complexity of processing is O(wh).  Complexity of hashmap is O(c)
public class PondSizes {
	void solve(Scanner in, PrintWriter out) {
		int h = in.nextInt();
		int w = in.nextInt();
		int[][] plot = new int[h + 2][w + 2];
		for (int r = 1; r <= h; r++) {
			for (int c = 1; c <= w; c++) {
				plot[r][c] = in.nextInt();
			}
		}

		int nextId = -1;
		Map<Integer, Integer> cnts = new HashMap<>();
		for (int r = 1; r <= h; r++) {
			for (int c = 1; c <= w; c++) {
				if (plot[r][c] > 0) {
					continue;
				}
				if (plot[r][c] == 0) {
					plot[r][c] = nextId;
					cnts.put(nextId, 1);
					nextId--;
				}
				markNeighbors(plot, r, c, r, c + 1, cnts);
				markNeighbors(plot, r, c, r + 1, c - 1, cnts);
				markNeighbors(plot, r, c, r + 1, c, cnts);
				markNeighbors(plot, r, c, r + 1, c + 1, cnts);
			}
		}
		out.println(cnts.values());
	}

	private void markNeighbors(int[][] plot, int a, int b, int na, int nb, Map<Integer, Integer> cnts) {
		if(na == 0 || nb == 0 || na == plot.length - 1 || nb == plot[0].length - 1) {
			return;
		}
		
		if (plot[na][nb] == 0) {
			plot[na][nb] = plot[a][b];
			cnts.put(plot[a][b], cnts.get(plot[a][b]) + 1);
		}
	}

	public static void main(String[] args) {
		new PondSizes().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
