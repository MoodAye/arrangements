package com.moodaye.playground.cci.moderateDifficulty;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

// Problem cci - 16.17
public class ContiguousSubsequence {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int[] a = new int[n];
		for(int i = 0; i < n; i++) {
			a[i] = in.nextInt();
		}
		
		int maxSum = a[0];
		int contSum = a[0];
		for(int i = 1; i < n; i++) {
			contSum = Math.max(a[i], contSum + a[i]);
			maxSum = Math.max(maxSum, contSum);
		}
		out.println(maxSum);
	}

	public static void main(String[] args) {
		new ContiguousSubsequence().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
