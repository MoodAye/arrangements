package com.moodaye.playground.cci.moderateDifficulty;

import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.Scanner;

// Problem 16.8
// 813am - 8:59
/*
 * Need more tests
 * Support for -ve integers
 */
public class EnglishInt {

	// todo - could use enum or array here
	private static final Map<Integer, String> ones = getOnesMap();
	private static final Map<Integer, String> tens = getTensMap();
	private static final Map<Integer, String> teens = getTeensMap();
	private static final Map<Integer, String> sizeValue = getSizeValue();
	
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		Random x = new Random();
		for(int i = 0; i < 10; i++) {
			n = x.nextInt() % 10_000;
			out.printf("%,d = %s%n", n, wp(n));
		}
		
	}
	
	private String wp(int n) {
		StringBuilder sb = new StringBuilder();
		if(n < 0) {
			n *= -1;
			sb.append("Minus ");
		}
		char[] cn = String.valueOf(n).toCharArray();
		int rem = 0;
		if(cn.length > 9) {
			sb.append(wp(n / 1_000_000_000));
			sb.append(" ").append(sizeValue.get(10)).append(" ");
			rem = n % 1_000_000_000;
		}
		else if(cn.length > 6) {
			sb.append(wp(n / 1_000_000));
			sb.append(" ").append(sizeValue.get(7)).append(" ");
			rem = n % 1_000_000;
		}
		else if(cn.length > 3) {
			// TODO - no null pointer exception if null - 
			// e.g., sb.append(tens.get(101001));
			sb.append(wp(n / 1_000));
			sb.append(" ").append(sizeValue.get(4)).append(" ");
			rem = n % 1_000;
		}
		else if(cn.length > 2) {
			sb.append(ones.get(n / 100));
			sb.append(" ").append(sizeValue.get(3)).append(" ");
			rem = n % 100;
		}
		else if(cn.length == 2) {
			if(n > 19 && n % 10 == 0) {
				sb.append(tens.get(n / 10));
			}
			else if(10 <= n && n <= 19) {
				sb.append(teens.get(n));
			}
			else {
				sb.append(tens.get(n / 10)).append(" ");
				rem = n % 10;
			}
		}
		else {
			sb.append(ones.get(n));
		}
		if(rem > 0) {
			sb.append(wp(rem));
		}
		return sb.toString();
	}	
		
	
	private static Map<Integer, String> getSizeValue(){
		Map<Integer, String> sv = new HashMap<>();
		sv.put(3, "Hundred");
		sv.put(4, "Thousand");
		sv.put(5, "Thousand");
		sv.put(6, "Thousand");
		sv.put(7, "Million");
		sv.put(8, "Million");
		sv.put(9, "Million");
		sv.put(10, "Billion");
		return sv;
	}
	
	private static Map<Integer, String> getTensMap() {
		Map<Integer, String> tens = new HashMap<>();
		tens.put(1, "Ten");
		tens.put(2, "Twenty");
		tens.put(3, "Thirty");
		tens.put(4, "Forty");
		tens.put(5, "Fifty");
		tens.put(6, "Sixty");
		tens.put(7, "Seventy");
		tens.put(8, "Eighty");
		tens.put(9, "Ninety");
		return tens;
	}
	
	private static Map<Integer, String> getTeensMap() {
		Map<Integer, String> teens = new HashMap<>();
		tens.put(10, "Ten");
		tens.put(11, "Eleven");
		tens.put(12, "Twelve");
		tens.put(13, "Thirteen");
		tens.put(14, "Fourteen");
		tens.put(15, "Fifteen");
		tens.put(16, "Sixteen");
		tens.put(17, "Seventeen");
		tens.put(18, "Eighteen");
		tens.put(19, "Nineteen");
		return tens;
	}

	private static Map<Integer, String> getOnesMap() {
		Map<Integer, String> ones = new HashMap<>();
		ones.put(0, "Zero");
		ones.put(1, "One");
		ones.put(2, "Two");
		ones.put(3, "Three");
		ones.put(4, "Four");
		ones.put(5, "Five");
		ones.put(6, "Six");
		ones.put(7, "Seven");
		ones.put(8, "Eight");
		ones.put(9, "Nine");

		return ones;
	}

	

	public static void main(String[] args) {
		new EnglishInt().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
