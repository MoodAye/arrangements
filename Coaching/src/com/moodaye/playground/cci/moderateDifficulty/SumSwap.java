package com.moodaye.playground.cci.moderateDifficulty;

import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Locale;
import java.util.Scanner;

// Problem 16.21
// Simple enough - we can sort the arrays - make it somewhat more efficient
// We can use counting sort to make perf complexity linear - but size can become an issue
// Finally we can use a hashset to make this linear.  
public class SumSwap {
	void solve(Scanner in, PrintWriter out) {
		int n1 = in.nextInt();
		int n2 = in.nextInt();
		int[] a1 = new int[n1];
		int[] a2 = new int[n2];
		for(int i = 0; i < n1; i++) {
			a1[i] = in.nextInt();
		}
		for(int i = 0; i < n2; i++) {
			a2[i] = in.nextInt();
		}
		
		// sum each array - add them together. 
		// so we need to make each array add to the total sum / 2.
		// if total sum is odd - there is no solution
		
		// e.g., 
		// 4,1,2,1,1,2 and 3,6,3,3
		// sum1 = 11;  sum2 = 15.  sum1 + sum2 = 26.  Each array should equal to 13
		// so find elements that have a difference of 2.
		
		int sum1 = Arrays.stream(a1).sum();
		int sum2 = Arrays.stream(a2).sum();
		
		int diff = sum1 - sum2;
		
		for(int e1 : a1) {
			for(int e2 : a2) {
				// sum1 - sum2 = diff;    sum1-e1+e2 = sum2+e1-e2; sum1 - sum2 = 2(e1-e2) = diff;  
				if(2*(e1 - e2) == diff) {
					out.printf("%d %d%n", e1, e2);
					return;
				}
			}
		}
		out.println("No Solution");
	}

	public static void main(String[] args) {
		new SumSwap().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
