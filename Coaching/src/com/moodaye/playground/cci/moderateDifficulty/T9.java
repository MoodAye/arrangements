package com.moodaye.playground.cci.moderateDifficulty;

import java.io.PrintWriter;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.Stack;
import java.util.TreeSet;

// Problem cci 16.20 - T9 Telephone numeric keypad.
public class T9 {
	void solve(Scanner in, PrintWriter out) {
		// input sequence
		int n = in.nextInt();
		char[] cn = String.valueOf(n).toCharArray();
		
		// dictionary
		int cntWords = in.nextInt();
		Set<String> dict = new TreeSet<>();
		for(int i = 0; i < cntWords; i++) {
			String word = in.next();
			dict.add(word);
		}
		
		Map<Integer, List<Character>> digitToChar = initList();
		out.println(cntWords(dict, digitToChar, cn, new StringBuilder(), 0, 0, new ArrayList<String>()));
		
		/* Option 2 
		 * - pre-computing the codes for dictionary */
		Map<Integer, List<String>> wordToInt	= getWordCodes(dict);
		out.println(wordToInt.get(n));
	}
	
	private Map<Integer, List<String>> getWordCodes(Set<String> dict){
		int num;
		Map<Integer, List<String>> wordCodes = new HashMap<>();
		for(String word : dict) {
			char[] cword = word.toCharArray();
			num = 0; 
			int mult = 1;
			for(char c : cword) {
				num = num * mult + code(c);
				mult *= 10;
			}
			List<String> words = wordCodes.get(num);
			if(words == null) {
				words = new ArrayList<>();
			}
			words.add(word);
			wordCodes.put(num, words);
		}
		return wordCodes;
	}
	
	private int code(char c) {
		int cn = 0;
		if(c == 'a' || c == 'b' || c == 'c') cn = 2;
		if(c == 'd' || c == 'e' || c == 'f') cn = 3;
		if(c == 'g' || c == 'h' || c == 'i') cn = 4;
		if(c == 'j' || c == 'k' || c == 'l') cn = 5;
		if(c == 'm' || c == 'n' || c == 'o') cn = 6;
		if(c == 'p' || c == 'q' || c == 'r' || c == 's') cn = 7;
		if(c == 't' || c == 'u' || c == 'v') cn = 8;
		if(c == 'w' || c == 'x' || c == 'y' || c == 'z') cn = 9;
		return cn;
	}
	private List<String> cntWords(Set<String> dict, Map<Integer, List<Character>> digitToChar, 
			char[] digits, StringBuilder wordThusFar, int idx, int cnt, List<String> words) {
		if(idx == digits.length) {
			String word = wordThusFar.toString();
			if(dict.contains(word)) {
				words.add(word);
			}
			return words;
		}
		List<Character> chars = digitToChar.get(digits[idx] - '0');
		for(char c : chars) {
			StringBuilder next = new StringBuilder(wordThusFar.toString()).append(c);
			cntWords(dict, digitToChar, digits, next, idx + 1, cnt, words);
		}
		return words;
	}
	
	private Map<Integer, List<Character>> initList() {
		Map<Integer, List<Character>> digitToChar = new HashMap<>();
		List<Character> list2 = new ArrayList<>();
		list2.add('a'); 
		list2.add('b'); 
		list2.add('c');
		digitToChar.put(2, list2);
		List<Character> list3 = new ArrayList<>();
		list3.add('d');
		list3.add('e');
		list3.add('f');
		digitToChar.put(3, list3);
		List<Character> list4 = new ArrayList<>();
		list4.add('g');
		list4.add('h');
		list4.add('i');
		digitToChar.put(4, list4);
		List<Character> list5 = new ArrayList<>();
		list5.add('j');
		list5.add('k');
		list5.add('l');
		digitToChar.put(5, list5);
		List<Character> list6 = new ArrayList<>();
		list6.add('m');
		list6.add('n');
		list6.add('o');
		digitToChar.put(6, list6);
		List<Character> list7 = new ArrayList<>();
		list7.add('p');
		list7.add('q');
		list7.add('r');
		list7.add('s');
		digitToChar.put(7, list7);
		List<Character> list8 = new ArrayList<>();
		list8.add('t');
		list8.add('u');
		list8.add('v');
		digitToChar.put(8, list8);
		List<Character> list9 = new ArrayList<>();
		list9.add('w');
		list9.add('x');
		list9.add('y');
		list9.add('z');
		digitToChar.put(9, list8);
		return digitToChar;
	}

	public static void main(String[] args) {
		new T9().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
	
}
