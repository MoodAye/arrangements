package com.moodaye.playground.cci.moderateDifficulty;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

// Problem 16.16 
// 841am - 9am
public class SubSort {
	void solve(Scanner in, PrintWriter out) {
		int n = in.nextInt();
		int[] list = new int[n];
		for(int i = 0; i < n; i++) {
			list[i] = in.nextInt();
		}
		
		int lft = -1;
		int rgt = -1;
		int curr = list[0];
		int max = curr;
		for(int i = 1; i < n; i++) {
			int next = list[i];
			if(next < max) {
				rgt = i;
				lft = scanForLeft(list, lft, i);
			}
			else {
				max = list[i];
			}
			curr = next;
		}
		out.printf("%d %d", lft, rgt);
	}
	
	// todo - do we need to check for start < 0?
	private int scanForLeft(int[] list, int lft, int cidx) {
		int start = lft == -1 ? cidx - 1 : lft - 1;
		while(start >= 0 && list[cidx] < list[start]) {
			start--;
		}
		return start + 1;
	}

	public static void main(String[] args) {
		new SubSort().run();
	}

	void run() {
		Locale.setDefault(Locale.US);
		try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
			solve(in, out);
		}
	}
}
/*
 * Tests:
 * 1 2 3 :  -1 - 1
 * 3 : -1 -1
 * 3 2  :  0, 1
 * 3 2 1 :  0, 2
 */