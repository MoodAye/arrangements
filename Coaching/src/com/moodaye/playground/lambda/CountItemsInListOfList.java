package com.moodaye.playground.lambda;

import java.util.ArrayList;
import java.util.List;

public class CountItemsInListOfList {
	public static void main(String[] args) {
		List<List<SpecialClass>> parentList = new ArrayList<>();
		
		List<SpecialClass> scList = new ArrayList<>();
		SpecialClass sc1 = new SpecialClass(true, "Rajiv");
		scList.add(sc1);
		SpecialClass sc2 = new SpecialClass(true, "Kavita");
		scList.add(sc2);
		SpecialClass sc3 = new SpecialClass(false, "Ronnie");
		scList.add(sc3);
		SpecialClass sc4 = new SpecialClass(true, "Tasha");
		scList.add(sc4);
		SpecialClass sc5 = new SpecialClass(true, "Alisha");
		scList.add(sc5);
		
		System.out.println(scList.stream().filter((a) -> a.isSigned).count());
	}
	
	// count the occurences of children have special attribute (isSigned)
	int countSc(List<List<SpecialClass>> parentList) {
		return (int) parentList.stream().count();
	}
}

class SpecialClass{
	boolean isSigned;
	String name;
	public SpecialClass(boolean isSigned, String name) {
		super();
		this.isSigned = isSigned;
		this.name = name;
	}
}

