package com.moodaye.util;

import java.util.stream.IntStream;

public class UsefulLambdas {
	
	public static void main(String[] args) {
//		IntStream.range(101, 1000).filter(UsefulLambdas::isPrime).forEach(
//				i -> System.out.println(i));
		System.out.println(
				IntStream.range(101, 1000).filter(UsefulLambdas::isPrime).count());
	}
	
	
	/* not optimal - but quick to write */
	public static boolean isPrime(int n) {
		int rootn = (int) Math.sqrt(n);
		return IntStream.rangeClosed(2, rootn).noneMatch(i -> n % i == 0);
	}
}
